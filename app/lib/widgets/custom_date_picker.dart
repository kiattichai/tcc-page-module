import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class CustomDatePicker extends DatePickerModel {
  CustomDatePicker(
      {DateTime? currentTime,
        DateTime? maxTime,
        DateTime? minTime,
        LocaleType? locale})
      : super(
      locale: locale,
      maxTime: maxTime,
      minTime: minTime,
      currentTime: currentTime);

  @override
  String? leftStringAtIndex(int index) {
    if (index >= 0 && index < leftList.length) {
      return (int.parse(leftList[index]) + 543).toString();
    } else {
      return null;
    }
  }
}