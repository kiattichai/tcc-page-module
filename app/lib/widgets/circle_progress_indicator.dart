import 'package:flutter/material.dart';

class CircleLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Color(0xffda3534)),
    );
  }
}
