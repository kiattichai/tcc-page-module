import 'package:app/utils/screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TimePickerWidget extends StatefulWidget {
  TimeOfDay timeOfDay;

  TimePickerWidget(this.timeOfDay);

  @override
  _TimePickerWidgetState createState() => _TimePickerWidgetState();
}

class _TimePickerWidgetState extends State<TimePickerWidget> {
  int currentTimeInHour = 0;
  int currentTimeInMin = 0;

  @override
  void initState() {
    currentTimeInHour = widget.timeOfDay.hour;
    currentTimeInMin = widget.timeOfDay.minute;
    super.initState();
  }

  Widget durationPicker({bool inMinutes = false, int picked = 0}) {
    return CupertinoPicker(
      scrollController: FixedExtentScrollController(initialItem: picked),
      onSelectedItemChanged: (x) {
        if (inMinutes) {
          currentTimeInMin = x;
        } else {
          currentTimeInHour = x;
        }
        setState(() {});
      },
      children: List.generate(
          inMinutes ? 60 : 24,
              (index) =>
              Align(
                alignment: Alignment.center,
                child: Text(
                  index.toString().padLeft(2, '0'),
                  style: TextStyle(color: Colors.black),
                ),
              )),
      itemExtent: 35,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(color: Colors.black),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text(
                      'ยกเลิก',
                      style: textStyle(),

                    )),
                TextButton(
                    onPressed: () => Navigator.of(context).pop(
                        TimeOfDay(hour:currentTimeInHour,minute:currentTimeInMin )
                    ),
                    child: Text(
                      'ยืนยัน',
                      style: textStyle(),

                    )),
              ],
            ),
            Container(
              color: Colors.white,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              child: Center(
                child: Container(
                    width: Screen.width * 0.6,
                    height: Screen.height * 0.45,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child:
                            durationPicker(picked: widget.timeOfDay.hour)),
                        Text(":"),
                        Expanded(
                            child: durationPicker(
                                inMinutes: true,
                                picked: widget.timeOfDay.minute)),
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet',
      color: Colors.blue,
      fontSize: 16,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
    );
  }
}
