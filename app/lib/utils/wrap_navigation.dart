import 'package:app/env_config.dart';
import 'package:app/module/share_module/mockup_native_screen.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;

class WrapNavigation {
  static WrapNavigation get instance => WrapNavigation();
  factory WrapNavigation() => _singleton;
  static final WrapNavigation _singleton = WrapNavigation._init();

  WrapNavigation._init();

  void pop<T extends Object?>(BuildContext context, {T? data}) {
    if (EnvConfig.isDevelopment) {
      TixNavigate.instance.pop(data: data);
    } else {
      Navigator.pop(context, data);
    }
  }

  Future<T?> pushNamed<T extends Object?>(BuildContext context, TixRoute route,
      {Object? arguments}) async {
    if (EnvConfig.isDevelopment) {
      if (route == null) return null;
      return await TixNavigate.instance.navigateTo(route, data: arguments);
    } else {
      return await Navigator.pushNamed(context, route.buildPath(), arguments: arguments);
    }
  }

  void nativePushNamed(BuildContext context, String name, {Object? arguments}) {
    if (EnvConfig.isDevelopment) {
      TixNavigate.instance.navigateTo(MockUpNativeScreenDataReceive(), data: arguments);
    } else {
      Navigator.of(context).nativePushNamed(name, arguments: arguments);
    }
  }

  bool canPop(BuildContext context) {
    if (EnvConfig.isDevelopment) {
      return TixNavigate.instance.navigatorKey?.currentState?.canPop() ?? false;
    } else {
      return Navigator.canPop(context);
    }
  }

  Future<T?> pushAndRemoveUntil<T extends Object?>(
      BuildContext context, Route<T> newRoute, RoutePredicate predicate) async {
    if (EnvConfig.isDevelopment) {
      if (newRoute.settings.name == null) return null;
      return await TixNavigate.instance.navigatorKey?.currentState
          ?.pushNamedAndRemoveUntil(newRoute.settings.name ?? '', predicate);
    } else {
      return await Navigator.of(context).pushAndRemoveUntil(newRoute, predicate);
    }
  }
}
