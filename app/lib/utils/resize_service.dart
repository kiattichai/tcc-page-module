import 'package:app/env_config.dart';
import 'package:app/model/images_entity.dart';
import 'package:app/utils/mock_data.dart';

class ResizeService {
  static String resize(int? width, int? height, Map<String, dynamic>? model) {
    if (model == null) {
      return DEFAULT_AVATAR;
    }
    try {
      final image = ImagesEntity.fromJson(model);
      if (image.id == null) {
        return DEFAULT_AVATAR;
      }
      if (width != null && height != null) {
        return "${EnvConfig.resUrl}/w_$width,h_$height,c_thump/${image.id}/${image.name}";
      } else if (width != null && height == null) {
        return "${EnvConfig.resUrl}/w_$width,c_thump/${image.id}/${image.name}";
      } else {
        return "${EnvConfig.resUrl}/c_thump/${image.id}/${image.name}";
      }
    } catch (e) {
      return '';
    }
  }

  static double getRatioImage(Map<String, dynamic> model) {
    if (model == null) {
      return 1.0;
    }
    try {
      final image = ImagesEntity.fromJson(model);
      if (image.id == null) {
        return 1.0;
      }
      // print("use resize service");
      // print("width ${image.width} height ${image.height}");
      // print("ratio ${(image.width / image.height)}");
      return (image.width! / image.height!);
    } catch (e) {
      return 1.0;
    }
  }

  static String resizeCrop(
      int? width, int? height, Map<String, dynamic>? model) {
    if (model == null) {
      return '';
    }
    try {
      final image = ImagesEntity.fromJson(model);
      if (width != null && height != null) {
        return "${EnvConfig.resUrl}/w_$width,h_$height,c_crop/${image.id}/${image.name}";
      } else {
        return "${EnvConfig.resUrl}/w_$width,c_crop/${image.id}/${image.name}";
      }
    } catch (e) {
      return '';
    }
  }
}
