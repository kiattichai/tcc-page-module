import 'dart:convert';
import 'dart:io';

import 'package:app/model/images_entity.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path/path.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

const IMAGE_QUALITY = 90;

enum ImageResolution { Landscape, Portrait, Square }

class ImagePickerUtils {
  static Future<List<AssetEntity>?> multiImagePicker(String gallery,
      {int currentImageInList = 0, BuildContext? context}) async {
    return await AssetPicker.pickAssets(context!,
        maxAssets: 10 - currentImageInList, textDelegate: ThaiTextDelegate());
//     return await MultiImagePicker.pickImages(
//         maxImages: 10 - currentImageInList,
//         materialOptions: MaterialOptions(
//           allViewTitle: gallery,
//           actionBarColor: "#4da4ad",
//           actionBarTitleColor: "#ffffff",
//           lightStatusBar: false,
//           statusBarColor: '#4da4ad',
//           startInAllView: true,
// //          selectionLimitReachedText: "You can't select any more.",
//         ));
  }

  static Future<String?> pickImageWithCropper() async {
    final picker = ImagePicker();
    String? result;
    var pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1400,
        maxWidth: 1400,
        imageQuality: IMAGE_QUALITY);
    if (pickedFile != null) {
      File? croppedFile = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(
          ratioX: 1.0,
          ratioY: 1.0,
        ),
        maxWidth: 1400,
        maxHeight: 1400,
        iosUiSettings: IOSUiSettings(title: "แก้ไขรูปภาพ"),
        androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'แก้ไขรูปภาพ',
        ),
      );
      if (croppedFile != null) {
        result = croppedFile.path;
      }
    }
    return result;
  }

  static Future<String?> pickImage() async {
    final picker = ImagePicker();
    String? result;
    var pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1400,
        maxWidth: 1400,
        imageQuality: IMAGE_QUALITY);
    if (pickedFile != null) {
      result = pickedFile.path;
    }
    return result;
  }

  static Future<String?> pickImageCover() async {
    final picker = ImagePicker();
    String? result;
    var pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1400,
        maxWidth: 1400,
        imageQuality: IMAGE_QUALITY);
    if (pickedFile != null) {
      File? croppedFile = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16, ratioY: 9),
        iosUiSettings:
            IOSUiSettings(minimumAspectRatio: 1.77, title: "แก้ไขรูปภาพ"),
        androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'แก้ไขรูปภาพ',
        ),
      );

      if (croppedFile != null) {
        result = croppedFile.path;
      }
    }
    return result;
  }

  static Future<String?> takePicture({bool crop = true}) async {
    final picker = ImagePicker();
    String? result;
    var pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1400,
        maxWidth: 1400,
        imageQuality: IMAGE_QUALITY);
    if (pickedFile != null) {
      if (crop) {
        File? croppedFile = await ImageCropper.cropImage(
          sourcePath: pickedFile.path,
          maxWidth: 1400,
          maxHeight: 1400,
          iosUiSettings: IOSUiSettings(title: "แก้ไขรูปภาพ"),
          androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'แก้ไขรูปภาพ',
          ),
        );
        if (croppedFile != null) {
          result = croppedFile.path;
        }
      } else {
        result = pickedFile.path;
      }
    }

    return result;
  }

  static Future<String?> takePictureCover() async {
    final picker = ImagePicker();
    String? result;
    var pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1400,
        maxWidth: 1400,
        imageQuality: IMAGE_QUALITY);
    if (pickedFile != null) {
      File? croppedFile = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16, ratioY: 9),
        iosUiSettings:
            IOSUiSettings(minimumAspectRatio: 1.77, title: "แก้ไขรูปภาพ"),
        androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'แก้ไขรูปภาพ',
        ),
      );
      if (croppedFile != null) {
        result = croppedFile.path;
      }
    }

    return result;
  }

  static Future<ImageUploadRequest> getImageUploadRequest(String path) async {
    final file = File(path);
    String imageName = basename(file.path);
    String? mimeType = mime(file.path);
    String? fileFormat = getFileFormat(mimeType!);
    String name = imageName.split('.').first.toLowerCase() + '.jpg';
    List<int> imageData = await file.readAsBytes();
    String fileData = base64Encode(imageData);
    print("getImageUploadRequest");
    print(fileFormat);
    print(mimeType);
    print(name);
    print(imageName);
    return ImageUploadRequest(name, fileData);
  }

  static String? getFileFormat(String fileName) {
    int lastDot = fileName.lastIndexOf('/', fileName.length - 1);
    if (lastDot != -1) {
      String result = fileName.substring(lastDot + 1);
      if (result == "jpeg") {
        return "jpg";
      } else {
        return result;
      }
    } else
      return null;
  }

  static Future<ImageResolution> getImageResolution(
      {String? path, ImagesEntity? imageModel}) async {
    if (path != null) {
      File image = new File(path); // Or any other way to get a File instance.
      var decodedImage = await decodeImageFromList(image.readAsBytesSync());
      if (decodedImage != null) {
        if (decodedImage.width == decodedImage.height) {
          return ImageResolution.Square;
        } else if (decodedImage.width > decodedImage.height) {
          return ImageResolution.Landscape;
        } else {
          return ImageResolution.Portrait;
        }
      } else {
        return ImageResolution.Square;
      }
    }
    if (imageModel != null) {
      if (imageModel.width == imageModel.height) {
        return ImageResolution.Square;
      } else if (imageModel.width! > imageModel.height!) {
        return ImageResolution.Landscape;
      } else {
        return ImageResolution.Portrait;
      }
    }
    return ImageResolution.Square;
  }
}

class ImageUploadRequest {
  String fileName;
  String fileData;

  ImageUploadRequest(this.fileName, this.fileData);
}

/// [AssetsPickerTextDelegate] implements with English.
class ThaiTextDelegate implements AssetsPickerTextDelegate {
  factory ThaiTextDelegate() => _instance;

  ThaiTextDelegate._internal();

  static final ThaiTextDelegate _instance = ThaiTextDelegate._internal();

  @override
  String confirm = 'ยืนยัน';

  @override
  String cancel = 'ยกเลิก';

  @override
  String edit = 'แก้ไข';

  @override
  String gifIndicator = 'GIF';

  @override
  String heicNotSupported = 'Unsupported HEIC asset type.';

  @override
  String loadFailed = 'โหลดไม่สำเร็จ';

  @override
  String original = 'Origin';

  @override
  String preview = 'พรีวิว';

  @override
  String select = 'เลือก';

  @override
  String unSupportedAssetType = 'Unsupported HEIC asset type.';

  @override
  String get emptyList => 'Empty list';

  @override
  String get unableToAccessAll => 'Unable to access all assets on the device';

  @override
  String get viewingLimitedAssetsTip =>
      'Only view assets and albums accessible to app.';

  @override
  String get changeAccessibleLimitedAssets =>
      'Update limited access assets list';

  @override
  String get accessAllTip => 'App can only access some assets on the device. '
      'Go to system settings and allow app to access all assets on the device.';

  @override
  String get goToSystemSettings => 'Go to system settings';

  @override
  String get accessLimitedAssets => 'Continue with limited access';

  @override
  String get accessiblePathName => 'Accessible assets';

  @override
  String durationIndicatorBuilder(Duration duration) {
    const String separator = ':';
    final String minute = duration.inMinutes.toString().padLeft(2, '0');
    final String second =
        ((duration - Duration(minutes: duration.inMinutes)).inSeconds)
            .toString()
            .padLeft(2, '0');
    return '$minute$separator$second';
  }
}
