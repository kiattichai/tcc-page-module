class ThaiIdValidator {


  static clean(String personalId) {
    return personalId.trim().replaceAll('-', '');
  }


  static bool validate(String personalId) {

    if(personalId.length != 13){
      return false;
    }

    List<String> charList = ThaiIdValidator.clean(personalId).split('');

    // 1st step
    List<int> digitList = charList.map((e) => int.parse(e)).toList();

    var lastDigit = digitList[12];
    digitList.removeLast();

    // 2nd step
    var position = 13;

    for (var index = 0; index < 12; index++) {
      digitList[index] = digitList[index] * position;
      --position;

    }

    // 3rd step
    var sum = digitList.reduce((first, second) => first + second);

    // 4th step
    var mod = sum % 11;

    // 5th step
    var rawValue = 11 - mod;

    // 6th step
    var checkingValue;
    if (rawValue.toString().length == 1) {
      checkingValue = rawValue;
    } else {
      checkingValue = int.parse(rawValue.toString().split('')[0]);
    }

    // let's validate
    if (checkingValue == lastDigit) {
      return true;
    } else {
      return false;
    }
  }
}