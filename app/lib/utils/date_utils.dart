import 'package:intl/intl.dart';

class DateFormatUtils {
  static String format(DateTime time) {
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(time);
  }
}
