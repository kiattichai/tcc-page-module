const String MOCK_PROFILE =
    "https://res.theconcert.com/w_110,h_110,c_crop/1cd2ac33df5158ad80f5481efe9a4a586/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2pvLzAvdWQvNDgwLzI0MDE3MzMvZ3JvdXB3aXRobG9nby5qcGc.jpg";

const String DEFAULT_AVATAR = "images/default-avatar.png";

const String DEFAULT_GROUP = "images/cover-group-default.png";
