import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_setting_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_user_profile_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class AgentWithdrawApi {
  final CoreApi api;
  final String pathUser = '/users';
  final String pathProfile = '/profile';
  final String pathSetting = '/settings';
  final String pathAgents = '/agents';
  final String pathWithdraw = '/withdraw';

  AgentWithdrawApi(this.api);

  Future<GetUserProfileEntity> getUserProfile() async {
    Response response = await api.get(pathUser + pathProfile,
        null, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'}, hasPermission: true);
    return GetUserProfileEntity.fromJson(response.data);
  }

  Future<GetAgentWithdrawSettingEntity> getAgentSetting() async {
    Response response = await api.get(pathSetting,
        {'code':'config','key':'agent_withdrawal_settings'}, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'});
    return GetAgentWithdrawSettingEntity.fromJson(response.data);
  }

  Future<GetAgentWithdrawEntity> getWithdraw({required int page,required int limit,required String status}) async {
    Response response = await api.get(pathAgents + pathWithdraw,
        {'status':status,'page':page,'limit':limit, 'sort': 'desc', 'order': 'id'}, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'}, hasPermission: true);
    return GetAgentWithdrawEntity.fromJson(response.data);
  }

  Future<BaseResponseEntity> withdraw(int amount) async {
    Response response = await api.post(pathAgents + pathWithdraw,
        { "amount": amount}, BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return BaseResponseEntity.fromJson(response.data);
  }

}