import 'package:app/core/base_repository.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/module/agent_withdraw_module/data/agent_withdraw_api.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_setting_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_user_profile_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class AgentWithdrawRepository extends BaseRepository {
  final AgentWithdrawApi api;

  AgentWithdrawRepository(this.api);

  Future<GetUserProfileData?> getUserProfile() async {
    final result = await api.getUserProfile();
    return result.data;
  }

  Future<GetAgentWithdrawSettingData?> getAgentSetting() async {
    final result = await api.getAgentSetting();
    return result.data;
  }

  Future<GetAgentWithdrawData?> getWithdraw({required int page,required int limit,required String status}) async {
    final result = await api.getWithdraw(page:page,limit:limit,status: status);
    return result.data;
  }

  Future<BaseResponseEntity> withdraw(int amount) async {
    final result = await api.withdraw(amount);
    return result;
  }
}