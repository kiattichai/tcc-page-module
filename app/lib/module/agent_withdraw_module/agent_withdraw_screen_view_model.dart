import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_setting_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

import 'model/get_user_profile_entity.dart';

class AgentWithdrawScreenViewModel extends BaseViewModel {
  GetUserProfileData? getUserProfileData;
  GetAgentWithdrawSettingData? getAgentWithdrawSettingData;
  GetAgentWithdrawDataRecord? waitWithdraw;
  TextEditingController withdrawController = TextEditingController();
  bool isActive = false;
  Function(BaseError)? showAlertError;

  @override
  void postInit() {
    super.postInit();
    setLoading(true);
    getAllData();
  }

  void getAllData() {
    catchError(() async {
      setLoading(true);
      waitWithdraw = null;
      getUserProfileData = await di.agentWithdrawRepository.getUserProfile();
      getAgentWithdrawSettingData = await di.agentWithdrawRepository.getAgentSetting();
      var data = await di.agentWithdrawRepository.getWithdraw(limit: 1, page: 1, status: 'waiting,pending');
      if ((data?.record ?? []).isNotEmpty) {
        waitWithdraw = data?.record?.first;
      }
      checkValidate();
      setLoading(false);
    });
  }

  String? validate(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกจำนวนเงินให้ถูกต้อง';
    }

    var min = getAgentWithdrawSettingData
            ?.record?.first.value?.minmaxWithdrawal?.minimumValue ??
        0;
    var max = getAgentWithdrawSettingData
            ?.record?.first.value?.minmaxWithdrawal?.maximumValue ??
        0;
    var minFormatted = NumberFormat('#,###').format(min);
    var maxFormatted = NumberFormat('#,###').format(max);
    if (int.parse(value) < min) {
      return 'กรุณากรอกจำนวนเงินไม่น้อยกว่า $minFormatted';
    }
    if (int.parse(value) > max) {
      return 'กรุณากรอกจำนวนเงินไม่เกิน $maxFormatted';
    }
    var money = (getUserProfileData?.wallet?.amount ?? 0) /
        (getAgentWithdrawSettingData?.record?.first.value?.coinsToThbRate ?? 1);
    if (int.parse(value) > money) {
      return 'จำนวนเงินไม่เพียงพอ';
    }
    return null;
  }

  void checkValidate() {
    isActive = validate(withdrawController.text) == null &&
        (getUserProfileData?.agent?.status == true.toString() &&
            getUserProfileData?.agent?.documentStatus?.id == 2)
         && (waitWithdraw == null);
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }
}
