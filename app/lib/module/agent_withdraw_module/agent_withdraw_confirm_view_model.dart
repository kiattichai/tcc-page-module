import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_setting_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_user_profile_entity.dart';

class AgentWithdrawConfirmViewModel extends BaseViewModel{


  GetUserProfileData? getUserProfileData;
  GetAgentWithdrawSettingData? getAgentWithdrawSettingData;
  final int amount;
  bool isActive = false;
  Function(BaseError)? showAlertError;
  Function()? navigateToHistory;

  AgentWithdrawConfirmViewModel(this.amount);

  @override
  void postInit() {
    super.postInit();
    setLoading(true);
    getUserProfile();
    getAgentSetting();
  }

  void getUserProfile() {
    catchError(() async {
      setLoading(true);
      getUserProfileData = await di.agentWithdrawRepository.getUserProfile();
      setLoading(false);
    });
  }

  void getAgentSetting() {
    catchError(() async {
      setLoading(true);
      getAgentWithdrawSettingData = await di.agentWithdrawRepository.getAgentSetting();
      setLoading(false);
    });
  }

  void withdraw() {
    catchError(() async {
      setLoading(true);
      var result = await di.agentWithdrawRepository
          .withdraw(amount);
      navigateToHistory!();
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }
}