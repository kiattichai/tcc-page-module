import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_confirm_view_model.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_history.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AgentWithdrawConfirm extends StatefulWidget with TixRoute {
  final int? amount;

  AgentWithdrawConfirm({
    Key? key,
    this.amount,
  }) : super(key: key);

  @override
  _AgentWithdrawConfirmState createState() => _AgentWithdrawConfirmState();

  @override
  String buildPath() {
    return '/agent_withdraw_confirm';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => AgentWithdrawConfirm(
              amount: data,
            ));
  }
}

class _AgentWithdrawConfirmState
    extends BaseStateProvider<AgentWithdrawConfirm, AgentWithdrawConfirmViewModel> {
  @override
  void initState() {
    viewModel = AgentWithdrawConfirmViewModel(widget.amount!);
    viewModel.showAlertError = showAlertError;
    viewModel.navigateToHistory = navigateToHistory;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
        onWillPop: () async {
          WrapNavigation.instance.pop(context);
          return false;
        },
        child: BaseWidget<AgentWithdrawConfirmViewModel>(
            model: viewModel,
            builder: (context, model, child) {
              var amountFormatted = NumberFormat('#,##0.00').format(viewModel.amount);
              var withdrawalFeeFormatted = NumberFormat('#,##0.00').format(
                  viewModel.getAgentWithdrawSettingData?.record?.first.value?.withdrawalFee ?? 0);
              var sum = (viewModel.amount) -
                  (viewModel.getAgentWithdrawSettingData?.record?.first.value?.withdrawalFee ?? 0);
              var sumFormatted = NumberFormat('#,##0.00').format(sum);
              return ProgressHUD(
                  opacity: 1.0,
                  color: Colors.white,
                  progressIndicator: CircleLoading(),
                  inAsyncCall: viewModel.loading,
                  child: Scaffold(
                    appBar: AgentAppBar(
                      onBack: () {
                        WrapNavigation.instance.pop(context);
                      },
                      name: 'ตรวจสอบการโอน',
                    ),
                    body: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(left: 16, right: 16),
                            padding: const EdgeInsets.only(top: 20, bottom: 20),
                            decoration: BoxDecoration(
                                border:
                                    Border(bottom: BorderSide(color: Color(0xfff4f4f4), width: 1))),
                            child: // กรุณาตรวจสอบข้อมูลให้
                                Text("กรุณาตรวจสอบข้อมูลให้ถูกต้อง",
                                    style: const TextStyle(
                                        color: const Color(0xff757575),
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "Thonburi-Bold",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 14.0),
                                    textAlign: TextAlign.center),
                          ),
                          Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16, right: 16),
                              padding: const EdgeInsets.only(top: 20, bottom: 20),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(color: Color(0xffe0e0e0), width: 1))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // โอนเข้า
                                  Text("โอนเข้า",
                                      style: const TextStyle(
                                          color: const Color(0xff333333),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Thonburi-Bold",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 14.0)),
                                  SizedBox(
                                    width: 25,
                                  ),
                                  viewModel.getUserProfileData?.agent?.bank?.logo == null
                                      ? CircleAvatar(
                                          radius: 21,
                                          backgroundColor: Colors.grey,
                                        )
                                      : CircleAvatar(
                                          radius: 21,
                                          backgroundColor: Colors.transparent,
                                          backgroundImage: NetworkImage(
                                              viewModel.getUserProfileData?.agent?.bank?.logo ??
                                                  ''),
                                        ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${viewModel.getUserProfileData?.agent?.bank?.name ?? ''}",
                                          style: const TextStyle(
                                              color: Color(0xff4a4a4a),
                                              fontWeight: FontWeight.w700,
                                              fontFamily: "Thonburi-Bold",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0),
                                        ),
                                        Text(
                                          "สาขา ${viewModel.getUserProfileData?.agent?.bank?.branch ?? ''}",
                                          style: const TextStyle(
                                              color: Color(0xff4a4a4a),
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "Thonburi-Bold",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0),
                                        ),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "ชื่อบัญชี",
                                              style: const TextStyle(
                                                  color: Color(0xff4a4a4a),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 16.0),
                                            ),
                                            Expanded(
                                              child: Text(
                                                " ${viewModel.getUserProfileData?.agent?.bank?.accountName ?? ''}",
                                                style: const TextStyle(
                                                    color: Color(0xff4a4a4a),
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: "Thonburi-Bold",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "เลขที่บัญชี ",
                                              style: const TextStyle(
                                                  color: Color(0xff4a4a4a),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 16.0),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(top: 4.0),
                                                child: Text(
                                                  "${viewModel.getUserProfileData?.agent?.bank?.accountNo ?? ''}",
                                                  style: const TextStyle(
                                                      color: Color(0xff4a4a4a),
                                                      fontWeight: FontWeight.w700,
                                                      fontFamily: "SFUIText",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize: 16.0),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )),
                          Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16, right: 16),
                              padding: const EdgeInsets.only(top: 20, bottom: 20),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(color: Color(0xffe0e0e0), width: 1))),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      // จำนวน
                                      Text("จำนวน",
                                          style: const TextStyle(
                                              color: const Color(0xff333333),
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "Thonburi-Bold",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 14.0)),
                                      // ฿ 500.00
                                      Text("฿$amountFormatted",
                                          style: const TextStyle(
                                              color: const Color(0xff4a4a4a),
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "SFUIText",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0))
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      // ค่าธรรมเนียมการทำธุร
                                      Text("ค่าธรรมเนียมการทำธุรกรรม",
                                          style: const TextStyle(
                                              color: const Color(0xff333333),
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "Thonburi-Bold",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 14.0)),
                                      // ฿ 30.00
                                      Text("-฿$withdrawalFeeFormatted",
                                          style: const TextStyle(
                                              color: const Color(0xffda3534),
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "SFUIText",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0))
                                    ],
                                  )
                                ],
                              )),
                          Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16, right: 16),
                              padding: const EdgeInsets.only(top: 18, bottom: 18),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(color: Color(0xff1f1f1f), width: 1))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  // รวมทั้งสิ้น
                                  Text("รวมทั้งสิ้น",
                                      style: const TextStyle(
                                          color: const Color(0xff333333),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Thonburi-Bold",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 14.0)),
                                  // ฿ 470.00
                                  Text("฿$sumFormatted",
                                      style: const TextStyle(
                                          color: const Color(0xff4a4a4a),
                                          fontWeight: FontWeight.w700,
                                          fontFamily: "SFUIText",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 18.0))
                                ],
                              )),
                          Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(left: 16, right: 16),
                            padding: const EdgeInsets.only(top: 2),
                            decoration: BoxDecoration(
                                border:
                                    Border(bottom: BorderSide(color: Color(0xff1f1f1f), width: 1))),
                          )
                        ],
                      ),
                    ),
                    bottomNavigationBar: Padding(
                      padding: EdgeInsets.only(
                          left: 16.0,
                          right: 16.0,
                          bottom: 10.0 + MediaQuery.of(context).padding.bottom,
                          top: 10),
                      child: InkWell(
                        child: Container(
                          width: double.infinity,
                          height: 39,
                          decoration: new BoxDecoration(
                              color: Color(0xffda3534), borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: // ลิงก์และวิธีการขาย
                                  // ยืนยัน
                                  Text("ยืนยัน",
                                      style: const TextStyle(
                                          color: const Color(0xffffffff),
                                          fontWeight: FontWeight.w700,
                                          fontFamily: "Thonburi-Bold",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 14.0),
                                      textAlign: TextAlign.center)),
                        ),
                        onTap: () {
                          viewModel.withdraw();
                        },
                      ),
                    ),
                  ));
            }));
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void navigateToHistory() async {
    await WrapNavigation.instance.pushNamed(context, AgentWithdrawHistory(), arguments: null);
    WrapNavigation.instance.pop(context);
  }
}
