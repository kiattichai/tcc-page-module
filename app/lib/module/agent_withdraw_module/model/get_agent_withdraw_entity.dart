import 'package:app/generated/json/get_agent_withdraw_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAgentWithdrawEntity {
  GetAgentWithdrawEntity();

  factory GetAgentWithdrawEntity.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawEntityToJson(this);

  GetAgentWithdrawData? data;
  GetAgentWithdrawBench? bench;
}

@JsonSerializable()
class GetAgentWithdrawData {
  GetAgentWithdrawData();

  factory GetAgentWithdrawData.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawDataFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawDataToJson(this);

  GetAgentWithdrawDataPagination? pagination;
  List<GetAgentWithdrawDataRecord>? record;
}

@JsonSerializable()
class GetAgentWithdrawDataPagination {
  GetAgentWithdrawDataPagination();

  factory GetAgentWithdrawDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetAgentWithdrawDataRecord {
  GetAgentWithdrawDataRecord();

  factory GetAgentWithdrawDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawDataRecordToJson(this);

  int? id;
  @JSONField(name: "transaction_no")
  String? transactionNo;
  GetAgentWithdrawDataRecordType? type;
  String? sof;
  int? amount;
  @JSONField(name: "amount_with_service_fee")
  int? amountWithServiceFee;
  @JSONField(name: "transfer_status")
  GetAgentWithdrawDataRecordTransferStatus? transferStatus;
  @JSONField(name: "transfer_at")
  GetAgentWithdrawDataRecordTransferAt? transferAt;
  GetAgentWithdrawDataRecordStatus? status;
  GetAgentWithdrawDataRecordUser? user;
  dynamic? staff;
  @JSONField(name: "store_bank")
  dynamic? storeBank;
  dynamic? image;
  @JSONField(name: "created_at")
  GetAgentWithdrawDataRecordCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetAgentWithdrawDataRecordUpdatedAt? updatedAt;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordType {
  GetAgentWithdrawDataRecordType();

  factory GetAgentWithdrawDataRecordType.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordTypeFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawDataRecordTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordTransferStatus {
  GetAgentWithdrawDataRecordTransferStatus();

  factory GetAgentWithdrawDataRecordTransferStatus.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordTransferStatusFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawDataRecordTransferStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordTransferAt {
  GetAgentWithdrawDataRecordTransferAt();

  factory GetAgentWithdrawDataRecordTransferAt.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordTransferAtFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawDataRecordTransferAtToJson(this);
}

@JsonSerializable()
class GetAgentWithdrawDataRecordStatus {
  GetAgentWithdrawDataRecordStatus();

  factory GetAgentWithdrawDataRecordStatus.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordStatusFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawDataRecordStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordUser {
  GetAgentWithdrawDataRecordUser();

  factory GetAgentWithdrawDataRecordUser.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordUserFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawDataRecordUserToJson(this);

  int? id;
  String? username;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordCreatedAt {
  GetAgentWithdrawDataRecordCreatedAt();

  factory GetAgentWithdrawDataRecordCreatedAt.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordCreatedAtFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawDataRecordCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetAgentWithdrawDataRecordUpdatedAt {
  GetAgentWithdrawDataRecordUpdatedAt();

  factory GetAgentWithdrawDataRecordUpdatedAt.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawDataRecordUpdatedAtFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawDataRecordUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetAgentWithdrawBench {
  GetAgentWithdrawBench();

  factory GetAgentWithdrawBench.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
