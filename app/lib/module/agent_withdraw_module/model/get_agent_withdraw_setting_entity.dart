import 'package:app/generated/json/get_agent_withdraw_setting_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAgentWithdrawSettingEntity {
  GetAgentWithdrawSettingEntity();

  factory GetAgentWithdrawSettingEntity.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawSettingEntityToJson(this);

  GetAgentWithdrawSettingData? data;
  GetAgentWithdrawSettingBench? bench;
}

@JsonSerializable()
class GetAgentWithdrawSettingData {
  GetAgentWithdrawSettingData();

  factory GetAgentWithdrawSettingData.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingDataFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawSettingDataToJson(this);

  List<GetAgentWithdrawSettingDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetAgentWithdrawSettingDataRecord {
  GetAgentWithdrawSettingDataRecord();

  factory GetAgentWithdrawSettingDataRecord.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingDataRecordFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawSettingDataRecordToJson(this);

  int? id;
  String? code;
  String? key;
  GetAgentWithdrawSettingDataRecordValue? value;
  bool? serialized;
}

@JsonSerializable()
class GetAgentWithdrawSettingDataRecordValue {
  GetAgentWithdrawSettingDataRecordValue();

  factory GetAgentWithdrawSettingDataRecordValue.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingDataRecordValueFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawSettingDataRecordValueToJson(this);

  @JSONField(name: "minmax_withdrawal")
  GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal? minmaxWithdrawal;
  @JSONField(name: "receive_within_hr")
  int? receiveWithinHr;
  @JSONField(name: "coins_to_thb_rate")
  int? coinsToThbRate;
  @JSONField(name: "withdrawal_fee")
  int? withdrawalFee;
}

@JsonSerializable()
class GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal {
  GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal();

  factory GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawalFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawalToJson(this);

  @JSONField(name: "minimum_value")
  int? minimumValue;
  @JSONField(name: "maximum_value")
  int? maximumValue;
}

@JsonSerializable()
class GetAgentWithdrawSettingBench {
  GetAgentWithdrawSettingBench();

  factory GetAgentWithdrawSettingBench.fromJson(Map<String, dynamic> json) =>
      $GetAgentWithdrawSettingBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentWithdrawSettingBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
