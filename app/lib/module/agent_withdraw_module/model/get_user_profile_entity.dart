import 'package:app/generated/json/get_user_profile_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetUserProfileEntity {

	GetUserProfileEntity();

	factory GetUserProfileEntity.fromJson(Map<String, dynamic> json) => $GetUserProfileEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileEntityToJson(this);

	GetUserProfileData? data;
	GetUserProfileBench? bench;
}

@JsonSerializable()
class GetUserProfileData {

	GetUserProfileData();

	factory GetUserProfileData.fromJson(Map<String, dynamic> json) => $GetUserProfileDataFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataToJson(this);

	int? id;
	String? username;
	@JSONField(name: "country_code")
	String? countryCode;
	@JSONField(name: "display_name")
	dynamic? displayName;
	GetUserProfileDataAvatar? avatar;
	String? gender;
	@JSONField(name: "first_name")
	String? firstName;
	@JSONField(name: "last_name")
	String? lastName;
	String? email;
	String? idcard;
	String? birthday;
	@JSONField(name: "terms_accepted")
	bool? termsAccepted;
	bool? activated;
	@JSONField(name: "activated_at")
	String? activatedAt;
	bool? blocked;
	@JSONField(name: "profile_score")
	int? profileScore;
	@JSONField(name: "last_login_at")
	String? lastLoginAt;
	@JSONField(name: "last_login_ip")
	String? lastLoginIp;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	String? referral;
	String? lang;
	GetUserProfileDataWallet? wallet;
	GetUserProfileDataGroup? group;
	GetUserProfileDataAgent? agent;
	List<GetUserProfileDataConnects>? connects;
}

@JsonSerializable()
class GetUserProfileDataAvatar {

	GetUserProfileDataAvatar();

	factory GetUserProfileDataAvatar.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAvatarFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAvatarToJson(this);

	String? id;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	@JSONField(name: "resize_url")
	String? resizeUrl;
}

@JsonSerializable()
class GetUserProfileDataWallet {

	GetUserProfileDataWallet();

	factory GetUserProfileDataWallet.fromJson(Map<String, dynamic> json) => $GetUserProfileDataWalletFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataWalletToJson(this);

	int? id;
	dynamic? amount;
}

@JsonSerializable()
class GetUserProfileDataGroup {

	GetUserProfileDataGroup();

	factory GetUserProfileDataGroup.fromJson(Map<String, dynamic> json) => $GetUserProfileDataGroupFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataGroupToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetUserProfileDataAgent {

	GetUserProfileDataAgent();

	factory GetUserProfileDataAgent.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentToJson(this);

	int? id;
	String? code;
	String? status;
	@JSONField(name: "document_status")
	GetUserProfileDataAgentDocumentStatus? documentStatus;
	dynamic? remark;
	String? address;
	@JSONField(name: "zip_code")
	String? zipCode;
	GetUserProfileDataAgentBank? bank;
	GetUserProfileDataAgentCountry? country;
	GetUserProfileDataAgentProvince? province;
	GetUserProfileDataAgentCity? city;
	GetUserProfileDataAgentDistrict? district;
}

@JsonSerializable()
class GetUserProfileDataAgentDocumentStatus {

	GetUserProfileDataAgentDocumentStatus();

	factory GetUserProfileDataAgentDocumentStatus.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentDocumentStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentDocumentStatusToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetUserProfileDataAgentBank {

	GetUserProfileDataAgentBank();

	factory GetUserProfileDataAgentBank.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentBankFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentBankToJson(this);

	int? id;
	String? name;
	@JSONField(name: "account_no")
	String? accountNo;
	@JSONField(name: "account_name")
	String? accountName;
	@JSONField(name: "account_type")
	dynamic? accountType;
	String? branch;
	String? logo;
}

@JsonSerializable()
class GetUserProfileDataAgentCountry {

	GetUserProfileDataAgentCountry();

	factory GetUserProfileDataAgentCountry.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentCountryFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentCountryToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetUserProfileDataAgentProvince {

	GetUserProfileDataAgentProvince();

	factory GetUserProfileDataAgentProvince.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentProvinceFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentProvinceToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetUserProfileDataAgentCity {

	GetUserProfileDataAgentCity();

	factory GetUserProfileDataAgentCity.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentCityFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentCityToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetUserProfileDataAgentDistrict {

	GetUserProfileDataAgentDistrict();

	factory GetUserProfileDataAgentDistrict.fromJson(Map<String, dynamic> json) => $GetUserProfileDataAgentDistrictFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataAgentDistrictToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetUserProfileDataConnects {

	GetUserProfileDataConnects();

	factory GetUserProfileDataConnects.fromJson(Map<String, dynamic> json) => $GetUserProfileDataConnectsFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileDataConnectsToJson(this);

	int? id;
	String? service;
	String? uid;
	String? account;
}

@JsonSerializable()
class GetUserProfileBench {

	GetUserProfileBench();

	factory GetUserProfileBench.fromJson(Map<String, dynamic> json) => $GetUserProfileBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetUserProfileBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
