import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_history_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AgentWithdrawHistory extends StatefulWidget with TixRoute {
  @override
  _AgentWithdrawHistoryState createState() => _AgentWithdrawHistoryState();

  @override
  String buildPath() {
    return '/agent_withdraw_history';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => AgentWithdrawHistory());
  }
}

class _AgentWithdrawHistoryState
    extends BaseStateProvider<AgentWithdrawHistory, AgentWithdrawHistoryViewModel> {
  @override
  void initState() {
    viewModel = AgentWithdrawHistoryViewModel();
    viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
        onWillPop: () async {
          WrapNavigation.instance.pop(context);
          return false;
        },
        child: BaseWidget<AgentWithdrawHistoryViewModel>(
            model: viewModel,
            builder: (context, model, child) {
              return Scaffold(
                appBar: AgentAppBar(
                  onBack: () {
                    WrapNavigation.instance.pop(context);
                  },
                  name: 'ประวัติการถอน',
                ),
                body: RefreshIndicator(
                  onRefresh: () {
                    viewModel.clearPage();
                    viewModel.getAgentWithdraw();
                    return Future.value();
                  },
                  child: viewModel.getAgentWithdrawData == null
                      ? Center(child: CircleLoading())
                      : viewModel.agentWithdrawList.isNotEmpty
                          ? ListView.builder(
                              physics: AlwaysScrollableScrollPhysics(),
                              itemCount: viewModel.agentWithdrawList.length + 1,
                              controller: viewModel.scrollController,
                              itemBuilder: (context, index) {
                                if (index == 0) {
                                  return Container(
                                    margin: const EdgeInsets.only(left: 16, right: 16),
                                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom:
                                                BorderSide(color: Color(0xffe0e0e0), width: 1))),
                                    child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("รายการ",
                                              style: const TextStyle(
                                                  color: const Color(0xff333333),
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0)),
                                          Spacer(),
                                          Text("จำนวน/บาท",
                                              style: const TextStyle(
                                                  color: const Color(0xff333333),
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0))
                                        ]),
                                  );
                                } else {
                                  var item = viewModel.agentWithdrawList[index - 1];
                                  var price = item.amount ?? 0;
                                  var priceFormatted = NumberFormat('#,##0.00').format(price);
                                  var date = DateFormat('dd/MM/yyyy HH:mm').format(
                                      DateFormat("yyyy-MM-dd HH:mm:ss")
                                          .parse(item.createdAt?.value ?? ''));

                                  var fontColor = item.transferStatus?.id == 1
                                      ? Color(0xff09900e)
                                      : item.transferStatus?.id == 0
                                          ? Color(0xffc65f2f)
                                          : Color(0xffda3534);
                                  return Container(
                                      margin: const EdgeInsets.only(left: 16, right: 16),
                                      padding: const EdgeInsets.only(top: 16, bottom: 16),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom:
                                                  BorderSide(color: Color(0xffe0e0e0), width: 1))),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text("${item.transactionNo ?? ''}",
                                                    style: const TextStyle(
                                                        color: const Color(0xff333333),
                                                        fontWeight: FontWeight.w400,
                                                        fontFamily: "Thonburi-Bold",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 14.0))
                                              ],
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Text("฿$priceFormatted",
                                                  style: const TextStyle(
                                                      color: const Color(0xff333333),
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily: "Thonburi-Bold",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize: 14.0)),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text("เมื่อ $date",
                                                  style: const TextStyle(
                                                      color: const Color(0xff333333),
                                                      fontWeight: FontWeight.w400,
                                                      fontFamily: "Thonburi-Bold",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize: 14.0)),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              item.status?.id != 1
                                                  ? Container(
                                                      padding: const EdgeInsets.only(
                                                          top: 2, bottom: 2, left: 5, right: 5),
                                                      alignment: Alignment.center,
                                                      child: // รอตรวจสอบ
                                                          Text("${item.status?.text ?? ''}",
                                                              style: TextStyle(
                                                                  color: fontColor,
                                                                  fontWeight: FontWeight.w400,
                                                                  fontFamily: "Thonburi-Bold",
                                                                  fontStyle: FontStyle.normal,
                                                                  fontSize: 14.0)),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(Radius.circular(3)),
                                                          border: Border.all(
                                                              color: item.transferStatus?.id == 1
                                                                  ? Color(0xff5c935c)
                                                                  : item.transferStatus?.id == 0
                                                                      ? Color(0xfff8deb4)
                                                                      : Color(0xffd77d7d),
                                                              width: 1),
                                                          color: item.transferStatus?.id == 1
                                                              ? Color(0xffffffff)
                                                              : item.transferStatus?.id == 0
                                                                  ? Color(0xfffef7ea)
                                                                  : Color(0xffd4a4a4)))
                                                  : SizedBox(),

                                              // Container(
                                              //     padding:
                                              //         const EdgeInsets.all(4),
                                              //     alignment: Alignment.center,
                                              //     child: Text(
                                              //         "${item.transferStatus?.text ?? ''}",
                                              //         style: const TextStyle(
                                              //             color: Colors.black87,
                                              //             fontWeight:
                                              //                 FontWeight.w400,
                                              //             fontFamily:
                                              //                 "Thonburi",
                                              //             fontStyle:
                                              //                 FontStyle.normal,
                                              //             fontSize: 14.0)),
                                              //     decoration: BoxDecoration(
                                              //         borderRadius:
                                              //             BorderRadius.all(
                                              //                 Radius.circular(
                                              //                     3)),
                                              //         border: Border.all(
                                              //             color: const Color(
                                              //                 0xffe0e0e0),
                                              //             width: 1),
                                              //         color: const Color(
                                              //             0xffe0e0e0)))
                                            ],
                                          )
                                        ],
                                      ));
                                }
                              })
                          : Center(
                              child: Text("ไม่มีประวัติการถอนเงิน",
                                  style: const TextStyle(
                                      color: const Color(0xff333333),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Thonburi-Bold",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0)),
                            ),
                ),
              );
            }));
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }
}
