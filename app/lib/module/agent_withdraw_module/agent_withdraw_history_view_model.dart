import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_entity.dart';
import 'package:flutter/material.dart';

class AgentWithdrawHistoryViewModel extends BaseViewModel {
  late ScrollController scrollController;
  GetAgentWithdrawData? getAgentWithdrawData;
  List<GetAgentWithdrawDataRecord> agentWithdrawList = [];
  int limit = 10;
  int page = 1;
  bool isComplete = false;
  Function(BaseError)? showAlertError;

  AgentWithdrawHistoryViewModel(){
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0 && !isComplete) {
        getAgentWithdraw();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    getAgentWithdraw();
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }

  void getAgentWithdraw() {
    catchError(() async {
      setLoading(true);
      if (isComplete == false) {
        isComplete = true;
        getAgentWithdrawData = await di.agentWithdrawRepository.getWithdraw(limit:limit,page: page,status: 'waiting,pending,approved');
      }
      if (page == 1) {
        agentWithdrawList.clear();
      }
      if (getAgentWithdrawData?.record != null &&
          getAgentWithdrawData!.record!.length > 0 &&
          getAgentWithdrawData!.pagination!.currentPage! <= getAgentWithdrawData!.pagination!.lastPage!) {
        agentWithdrawList.addAll(getAgentWithdrawData?.record ?? []);
        if(getAgentWithdrawData!.pagination!.currentPage! != getAgentWithdrawData!.pagination!.lastPage!){
          isComplete = false;
          page++;
        }

      } else {
        isComplete = true;
      }
      setLoading(false);
    });
  }

  void clearPage() {
    page = 1;
    isComplete = false;
    notifyListeners();
  }


  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }
}