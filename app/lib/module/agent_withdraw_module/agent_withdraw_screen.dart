import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_confirm.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_history.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_screen_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/agent_bank_screen.dart';
import 'package:app/module/register_agent_module/agent_detail_screen.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class AgentWithdrawScreen extends StatefulWidget with TixRoute {
  @override
  _AgentWithdrawScreenState createState() => _AgentWithdrawScreenState();

  @override
  String buildPath() {
    return '/agent_withdraw_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => AgentWithdrawScreen());
  }
}

class _AgentWithdrawScreenState
    extends BaseStateProvider<AgentWithdrawScreen, AgentWithdrawScreenViewModel> {
  final FocusNode _nodeTextWithDrawMoney = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: false,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeTextWithDrawMoney,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 20, top: 8, bottom: 8),
                  child: Text(
                    "ตกลง",
                    textScaleFactor: MediaQuery.of(context).textScaleFactor > 1.2
                        ? 1.1
                        : MediaQuery.of(context).textScaleFactor,
                    style: TextStyle(
                      fontFamily: 'Kanit',
                      color: Color(0xffda3534),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              );
            }
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    viewModel = AgentWithdrawScreenViewModel();
    viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
        onWillPop: () async {
          SystemNavigator.pop();
          return false;
        },
        child: BaseWidget<AgentWithdrawScreenViewModel>(
            model: viewModel,
            builder: (context, model, child) {
              var money = (viewModel.getUserProfileData?.wallet?.amount ?? 0) /
                  (viewModel.getAgentWithdrawSettingData?.record?.first.value?.coinsToThbRate ?? 1);
              var coin = (viewModel.getUserProfileData?.wallet?.amount ?? 0);
              var min = viewModel.getAgentWithdrawSettingData?.record?.first.value?.minmaxWithdrawal
                      ?.minimumValue ??
                  0;
              var max = viewModel.getAgentWithdrawSettingData?.record?.first.value?.minmaxWithdrawal
                      ?.maximumValue ??
                  0;
              var waitWithdraw = (viewModel.waitWithdraw?.amountWithServiceFee ?? 0);
              var moneyFormatted = NumberFormat('#,##0.00').format(money);
              var coinFormatted = NumberFormat('#,##0.00').format(coin);
              var waitWithdrawFormatted = NumberFormat('#,##0.00').format(waitWithdraw);
              var minFormatted = NumberFormat('#,###').format(min);
              var maxFormatted = NumberFormat('#,###').format(max);
              return GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  child: ProgressHUD(
                      opacity: 1.0,
                      color: Colors.white,
                      progressIndicator: CircleLoading(),
                      inAsyncCall: viewModel.loading,
                      child: Scaffold(
                        appBar: AgentAppBar(
                          onBack: () {
                            SystemNavigator.pop();
                            appState.channel.invokeMethod('pop');
                          },
                          name: 'ถอนเงิน',
                        ),
                        body: RefreshIndicator(
                          onRefresh: () {
                            viewModel.withdrawController.text = '0';
                            viewModel.getAllData();
                            return Future.value();
                          },
                          child: KeyboardActions(
                            config: _buildConfig(context),
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, top: 20, bottom: 19),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            Text("คุณมีทั้งหมด ",
                                                style: const TextStyle(
                                                    color: const Color(0xff757575),
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: "Thonburi-Bold",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 14.0)),
                                            Expanded(
                                                child: Text("$coinFormatted Coins",
                                                    style: const TextStyle(
                                                        color: const Color(0xff989898),
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily: "Thonburi-Bold",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 14.0)))
                                          ],
                                        ),
                                        SizedBox(
                                          height: 7,
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            // คุณมีทั้งหมด เป็นเงิ
                                            Text("เป็นเงินจำนวน (บาท)",
                                                style: const TextStyle(
                                                    color: const Color(0xff757575),
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: "Thonburi-Bold",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 14.0)),
                                            Spacer(),
                                            Expanded(
                                                child: // ฿1,000.00
                                                    Text("฿$moneyFormatted",
                                                        textAlign: TextAlign.right,
                                                        style: const TextStyle(
                                                            color: Color(0xff4a4a4a),
                                                            fontWeight: FontWeight.w700,
                                                            fontFamily: "SFUIText",
                                                            fontStyle: FontStyle.normal,
                                                            fontSize: 16.0)))
                                          ],
                                        ),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom:
                                                BorderSide(color: Color(0xfff4f4f4), width: 1))),
                                  ),
                                  viewModel.waitWithdraw != null
                                      ? Container(
                                          child: Row(
                                            children: [
                                              Text("${viewModel.waitWithdraw?.status?.text ?? ''}",
                                                  style: const TextStyle(
                                                      color: const Color(0xff2b2b2b),
                                                      fontWeight: FontWeight.w400,
                                                      fontFamily: "Thonburi-Bold",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize: 14.0)),
                                              Spacer(),
                                              Expanded(
                                                  child: // ฿500.00
                                                      Text(
                                                '฿$waitWithdrawFormatted',
                                                textAlign: TextAlign.right,
                                                style: const TextStyle(
                                                    color: const Color(0xff4a4a4a),
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "Thonburi-Bold",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 16.0),
                                              ))
                                            ],
                                          ),
                                          padding: const EdgeInsets.only(
                                              left: 16, right: 16, top: 14, bottom: 14),
                                          decoration: BoxDecoration(color: const Color(0xfffef7ea)))
                                      : SizedBox(),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, top: 25, bottom: 10),
                                    child: Row(
                                      children: [
                                        Text('จำนวนเงินที่ถอน *',
                                            style: const TextStyle(
                                                color: Color(0xff4a4a4a),
                                                fontWeight: FontWeight.w700,
                                                fontFamily: "Thonburi-Bold",
                                                fontStyle: FontStyle.normal,
                                                fontSize: 14.0)),
                                        Spacer(),
                                        // ประวัติการถอน
                                        InkWell(
                                            onTap: () {
                                              WrapNavigation.instance.pushNamed(
                                                  context, AgentWithdrawHistory(),
                                                  arguments: null);
                                            },
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                Text("ประวัติการถอน",
                                                    style: const TextStyle(
                                                        color: const Color(0xff757575),
                                                        fontWeight: FontWeight.w400,
                                                        fontFamily: "Thonburi-Bold",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 14.0)),
                                                SizedBox(
                                                  width: 6,
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(bottom: 2),
                                                  child: Image(
                                                    image: AssetImage('assets/chevron_right2.png'),
                                                    width: 6,
                                                    height: 10,
                                                    color: Color(0xff989898),
                                                  ),
                                                ),
                                              ],
                                            ))
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(left: 16, right: 16),
                                    child: TextFormField(
                                      focusNode: viewModel.waitWithdraw != null
                                          ? new AlwaysDisabledFocusNode()
                                          : _nodeTextWithDrawMoney,
                                      controller: viewModel.withdrawController,
                                      validator: viewModel.validate,
                                      maxLength: 6,
                                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                      keyboardType: TextInputType.number,
                                      autovalidateMode: AutovalidateMode.onUserInteraction,
                                      onChanged: (value) {
                                        viewModel.checkValidate();
                                      },
                                      readOnly: viewModel.waitWithdraw != null,
                                      style: TextStyle(
                                          color: const Color(0xff4a4a4a),
                                          fontWeight: FontWeight.w700,
                                          fontFamily: "SFUIText",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 18.0),
                                      decoration: InputDecoration(
                                        counterText: '',
                                        prefixIcon: SizedBox(
                                          child: Center(
                                            widthFactor: 0.0,
                                            child: Text(
                                              "฿",
                                              style: const TextStyle(
                                                  color: const Color(0xff989898),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "SFUIText",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 16.0),
                                            ),
                                          ),
                                        ),
                                        isDense: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                const BorderRadius.all(Radius.circular(4.0)),
                                            borderSide: BorderSide(
                                                color: const Color(0xffe0e0e0), width: 1)),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, top: 10, bottom: 10),
                                    child: Text(
                                        "ขั้นต่ำ $minFormatted บาท สูงสุดไม่เกิน $maxFormatted บาท",
                                        style: const TextStyle(
                                            color: const Color(0xff757575),
                                            fontWeight: FontWeight.w400,
                                            fontFamily: "Thonburi-Bold",
                                            fontStyle: FontStyle.normal,
                                            fontSize: 12.0)),
                                  ),
                                  viewModel.waitWithdraw != null
                                      ? Container(
                                          padding: const EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                          ),
                                          child: Text("* สามารถทำรายการได้ครั้งละ 1 รายการเท่านั้น",
                                              style: const TextStyle(
                                                  color: const Color(0xffff2626),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 12.0)),
                                        )
                                      : SizedBox(),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, top: 10, bottom: 10),
                                    child: Text("ถอนไปยังบัญชี",
                                        style: const TextStyle(
                                            color: const Color(0xff4a4a4a),
                                            fontWeight: FontWeight.w700,
                                            fontFamily: "Thonburi-Bold",
                                            fontStyle: FontStyle.normal,
                                            fontSize: 16.0)),
                                  ),
                                  // Rectangle
                                  viewModel.getUserProfileData == null
                                      ? SizedBox()
                                      : viewModel.getUserProfileData?.agent?.status ==
                                                  true.toString() &&
                                              viewModel.getUserProfileData?.agent?.documentStatus
                                                      ?.id ==
                                                  2
                                          ? Container(
                                              margin: const EdgeInsets.only(left: 16, right: 16),
                                              padding: const EdgeInsets.only(
                                                  left: 16, right: 16, top: 14, bottom: 12),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(Radius.circular(9)),
                                                  color: const Color(0xfff6f6f6)),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  viewModel.getUserProfileData?.agent?.bank?.logo ==
                                                          null
                                                      ? CircleAvatar(
                                                          radius: 21,
                                                          backgroundColor: Colors.grey,
                                                        )
                                                      : CircleAvatar(
                                                          radius: 21,
                                                          backgroundColor: Colors.transparent,
                                                          backgroundImage: NetworkImage(viewModel
                                                                  .getUserProfileData
                                                                  ?.agent
                                                                  ?.bank
                                                                  ?.logo ??
                                                              ''),
                                                        ),
                                                  SizedBox(
                                                    width: 18,
                                                  ),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          "${viewModel.getUserProfileData?.agent?.bank?.name ?? ''}",
                                                          style: const TextStyle(
                                                              color: Color(0xff4a4a4a),
                                                              fontWeight: FontWeight.w700,
                                                              fontFamily: "Thonburi-Bold",
                                                              fontStyle: FontStyle.normal,
                                                              fontSize: 16.0),
                                                        ),
                                                        Text(
                                                          "สาขา ${viewModel.getUserProfileData?.agent?.bank?.branch ?? ''}",
                                                          style: const TextStyle(
                                                              color: Color(0xff4a4a4a),
                                                              fontWeight: FontWeight.w400,
                                                              fontFamily: "Thonburi-Bold",
                                                              fontStyle: FontStyle.normal,
                                                              fontSize: 16.0),
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                              "ชื่อบัญชี",
                                                              style: const TextStyle(
                                                                  color: Color(0xff4a4a4a),
                                                                  fontWeight: FontWeight.w400,
                                                                  fontFamily: "Thonburi-Bold",
                                                                  fontStyle: FontStyle.normal,
                                                                  fontSize: 16.0),
                                                            ),
                                                            Expanded(
                                                              child: Text(
                                                                " ${viewModel.getUserProfileData?.agent?.bank?.accountName ?? ''}",
                                                                style: const TextStyle(
                                                                    color: Color(0xff4a4a4a),
                                                                    fontWeight: FontWeight.w700,
                                                                    fontFamily: "Thonburi-Bold",
                                                                    fontStyle: FontStyle.normal,
                                                                    fontSize: 16.0),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment.start,
                                                          children: [
                                                            Text(
                                                              "เลขที่บัญชี ",
                                                              style: const TextStyle(
                                                                  color: Color(0xff4a4a4a),
                                                                  fontWeight: FontWeight.w400,
                                                                  fontFamily: "Thonburi-Bold",
                                                                  fontStyle: FontStyle.normal,
                                                                  fontSize: 16.0),
                                                            ),
                                                            Expanded(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets.only(top: 4.0),
                                                                child: Text(
                                                                  "${viewModel.getUserProfileData?.agent?.bank?.accountNo ?? ''}",
                                                                  style: const TextStyle(
                                                                      color: Color(0xff4a4a4a),
                                                                      fontWeight: FontWeight.w700,
                                                                      fontFamily: "SFUIText",
                                                                      fontStyle: FontStyle.normal,
                                                                      fontSize: 16.0),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ))
                                          : Container(
                                              margin: const EdgeInsets.only(left: 16, right: 16),
                                              padding: const EdgeInsets.only(
                                                  left: 16, right: 16, top: 14, bottom: 16),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Image(
                                                        image: AssetImage('assets/group_6.png'),
                                                        width: 30,
                                                        height: 30,
                                                      ),
                                                      Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment.start,
                                                          children: [
                                                            Text("ไม่มีบัญชีธนาคาร",
                                                                style: const TextStyle(
                                                                    color: const Color(0xffc65f2f),
                                                                    fontWeight: FontWeight.w700,
                                                                    fontFamily: "Thonburi-Bold",
                                                                    fontStyle: FontStyle.normal,
                                                                    fontSize: 16.0)),
                                                            SizedBox(
                                                              height: 6,
                                                            ),
                                                            Text(
                                                                "กรุณาเพิ่มบัญชีธนาคารเพื่อทำธุรกรรม",
                                                                style: const TextStyle(
                                                                    color: const Color(0xff757575),
                                                                    fontWeight: FontWeight.w400,
                                                                    fontFamily: "Thonburi-Bold",
                                                                    fontStyle: FontStyle.normal,
                                                                    fontSize: 14.0))
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),

                                                  //TODO: Button add bank account
                                                  InkWell(
                                                    onTap: () async {
                                                      await WrapNavigation.instance.pushNamed(
                                                          context, AgentBankScreen(), arguments: {
                                                        'isUpdate': 'update'
                                                      }).then((value) async => {
                                                            if (value != null)
                                                              {
                                                                await WrapNavigation.instance
                                                                    .pushNamed(context,
                                                                        AgentDetailScreen()),
                                                                viewModel.getAllData(),
                                                              }
                                                          });
                                                    },
                                                    child: Container(
                                                      height: 40.0,
                                                      margin: const EdgeInsets.all(16),
                                                      alignment: Alignment.center,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(Radius.circular(5)),
                                                          color: const Color(0xfff8b412)),
                                                      child: // เพิ่มบัญชีธนาคาร
                                                          Text("เพิ่มบัญชีธนาคาร",
                                                              style: const TextStyle(
                                                                  color: const Color(0xffffffff),
                                                                  fontWeight: FontWeight.w700,
                                                                  fontFamily: "Thonburi-Bold",
                                                                  fontStyle: FontStyle.normal,
                                                                  fontSize: 14.0),
                                                              textAlign: TextAlign.center),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(9)),
                                                color: const Color(0xfffef7ea),
                                              )),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 16, top: 14, bottom: 10),
                                    child: Text(
                                        "หลังจากยืนยันและได้รับการตรวจสอบแล้ว เงินจะถูกโอนเข้าบัญชีของคุณภายใน ${viewModel.getAgentWithdrawSettingData?.record?.first.value?.receiveWithinHr ?? ''} ชั่วโมง\n\n\n*  อัตราแลกเปลี่ยน ${viewModel.getAgentWithdrawSettingData?.record?.first.value?.coinsToThbRate ?? ''} coins = 1 บาท\n** ค่าธรรมเนียมการทำธุรกรรมครั้งละ ${viewModel.getAgentWithdrawSettingData?.record?.first.value?.withdrawalFee ?? ''} บาท",
                                        style: const TextStyle(
                                            color: const Color(0xff757575),
                                            fontWeight: FontWeight.w400,
                                            fontFamily: "Thonburi-Bold",
                                            fontStyle: FontStyle.normal,
                                            fontSize: 14.0)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        bottomNavigationBar: Padding(
                          padding: EdgeInsets.only(
                              left: 16.0,
                              right: 16.0,
                              bottom: 10.0 + MediaQuery.of(context).padding.bottom,
                              top: 10),
                          child: InkWell(
                            child: Container(
                              width: double.infinity,
                              height: 39,
                              decoration: new BoxDecoration(
                                  color: viewModel.isActive ? Color(0xffda3534) : Color(0xffd9d9d9),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Center(
                                  child: // ลิงก์และวิธีการขาย
                                      // ยืนยัน
                                      Text("ดำเนินการ",
                                          style: const TextStyle(
                                              color: const Color(0xffffffff),
                                              fontWeight: FontWeight.w700,
                                              fontFamily: "Thonburi-Bold",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 14.0),
                                          textAlign: TextAlign.center)),
                            ),
                            onTap: () async {
                              if (viewModel.isActive) {
                                _nodeTextWithDrawMoney.unfocus();
                                await WrapNavigation.instance.pushNamed(
                                    context, AgentWithdrawConfirm(),
                                    arguments: int.parse(viewModel.withdrawController.text));
                                viewModel.withdrawController.text = '';
                                viewModel.getAllData();
                                viewModel.notifyListeners();
                              }
                            },
                          ),
                        ),
                      )));
            }));
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
