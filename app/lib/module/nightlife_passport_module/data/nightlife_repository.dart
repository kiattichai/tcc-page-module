import 'package:app/core/base_repository.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/model/setting_vaccine_limit_response_entity.dart';
import 'package:app/module/nightlife_passport_module/data/nightlife_api.dart';
import 'package:app/module/nightlife_passport_module/model/get_nightlife_passport_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_vaccine_entity.dart';
import 'package:app/module/nightlife_passport_module/model/nightlife_ticket_history_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/passport_data_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/register_nightlife_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_contents_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_qr_response_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class NightlifeRepository extends BaseRepository {
  final NightlifeApi api;

  NightlifeRepository(this.api);

  Future<GetVaccineData?> getVaccineContents() async {
    final response = await api.getVaccineContents();
    return response.data;
  }

  Future<GetNightlifePassportData?> getNightlifePassport() async {
    final response = await api.getNightlifePassport();
    return response.data;
  }

  Future<BaseResponseEntity> registerNightlife(
      RegisterNightlifeEntity nightlifeEntity) async {
    final response = await api.registerNightlife(nightlifeEntity);
    return response;
  }

  Future<BaseResponseEntity> updateNightlife(
      RegisterNightlifeEntity nightlifeEntity) async {
    final response = await api.updateNightlife(nightlifeEntity);
    return response;
  }

  Future<TicketQrResponseEntity> getNightlifeTicket(
      dynamic productId, dynamic ticketId) async {
    final response = await api.getNightlifeTicket(productId, ticketId);
    return response;
  }

  Future<PassportDataResponseEntity> getNightlifePassportPassportData() async {
    final response = await api.getNightlifePassportPassportData();
    return response;
  }

  Future<TicketContentsResponseEntity> getTicketContent() async {
    final response = await api.getTicketContent();
    return response;
  }

  Future<NightlifeTicketHistoryResponseData?> getHistory(dynamic ticketId,
      dynamic productId, dynamic productVariantId, int page, int limit) async {
    final response = await api.getHistory(
        ticketId, productId, productVariantId, page, limit);
    return response.data;
  }

  Future<SettingVaccineLimitResponseData?> getVaccineLimit() async {
    final response = await api.getVaccineLimit();
    return response.data;
  }

}
