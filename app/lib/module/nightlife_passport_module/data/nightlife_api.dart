import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/model/setting_vaccine_limit_response_entity.dart';
import 'package:app/module/log_in_module/model/get_user_authen_entity.dart';
import 'package:app/module/log_in_module/model/get_user_detail_entity.dart';
import 'package:app/module/log_in_module/model/log_in_request_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_nightlife_passport_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_vaccine_entity.dart';
import 'package:app/module/nightlife_passport_module/model/nightlife_ticket_history_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/passport_data_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/register_nightlife_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_contents_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_qr_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class NightlifeApi {
  final CoreApi api;
  final String pathContents = '/contents';
  final String pathNightlife = '/users/nightlife-passport';
  final String pathUserTicket = '/users/tickets';
  final String pathSetting = '/settings';

  NightlifeApi(this.api);

  Future<GetVaccineEntity> getVaccineContents() async {
    final response = await api.get(
        pathContents,
        {"group_code": "tcc-covid-vaccine", "status": true},
        BaseErrorEntity.badRequestToModelError);
    return GetVaccineEntity.fromJson(response.data);
  }

  Future<GetNightlifePassportEntity> getNightlifePassport() async {
    final response = await api.get(
        pathNightlife, null, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetNightlifePassportEntity.fromJson(response.data);
  }

  Future<BaseResponseEntity> registerNightlife(
      RegisterNightlifeEntity nightlifeEntity) async {
    final response = await api.post(pathNightlife, nightlifeEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return BaseResponseEntity.fromJson(response.data);
  }

  Future<BaseResponseEntity> updateNightlife(
      RegisterNightlifeEntity nightlifeEntity) async {
    final response = await api.put(pathNightlife, nightlifeEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return BaseResponseEntity.fromJson(response.data);
  }

  Future<TicketQrResponseEntity> getNightlifeTicket(
      dynamic productId, dynamic ticketId) async {
    final param = {"format": "string"};
    final response = await api.get(pathUserTicket + '/$productId/qr/$ticketId',
        param, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return TicketQrResponseEntity.fromJson(response.data);
  }

  Future<PassportDataResponseEntity> getNightlifePassportPassportData() async {
    final response = await api.get(pathNightlife + '/passport-data', null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return PassportDataResponseEntity.fromJson(response.data);
  }

  Future<TicketContentsResponseEntity> getTicketContent() async {
    final param = {
      "group_code": "widget-image",
      "category_code": "ticket-nightlife-passport",
      "status": true
    };
    final response = await api.get(
        pathContents, param, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return TicketContentsResponseEntity.fromJson(response.data);
  }

  Future<NightlifeTicketHistoryResponseEntity> getHistory(dynamic ticketId,
      dynamic productId, dynamic productVariantId, int page, int limit) async {
    final param = {
      "ticket_id": ticketId,
      "product_id": productId,
      "product_variant_id": productVariantId,
      "page": page,
      "limit": limit
    };
    final response = await api.get(pathNightlife + '/history', param,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return NightlifeTicketHistoryResponseEntity.fromJson(response.data);
  }


  Future<SettingVaccineLimitResponseEntity> getVaccineLimit() async {
    final param = {
      "code": "config",
      "key": "vaccine_limit"
    };
    final response = await api.get(
        pathSetting, param, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SettingVaccineLimitResponseEntity.fromJson(response.data);
  }
}
