import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_nightlife_passport_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_vaccine_entity.dart';
import 'package:app/module/nightlife_passport_module/model/register_nightlife_entity.dart';
import 'package:app/utils/image_picker_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';

class RegisterNightlifeScreenViewModel extends BaseViewModel {
  File? image;
  GetVaccineData? getVaccineData;
  GetNightlifePassportData? getNightlifePassportData;
  // List<GetNightlifePassportDataRecord?> record = [null, null, null];
  List<GetNightlifePassportDataRecord?> record = [];
  TextEditingController vaccine1 = TextEditingController();
  TextEditingController vaccine2 = TextEditingController();
  TextEditingController vaccine3 = TextEditingController();
  List<TextEditingController> vaccineControllers = [];
  List<Map<String, dynamic>> vaccinesList = [];
  bool isActive = false;
  int vaccineLimit = 0;
  Function(BaseError)? showAlertError;
  Function()? showAlertSuccess;

  @override
  void postInit() {
    getVaccineLimitSetting();
    getVaccine();
    getNightlifePassport();
    super.postInit();
  }

  void picImage() async {
    final pathImage = await ImagePickerUtils.pickImage();
    if (pathImage != null) {
      image = File(pathImage);
    }
    validate();
    notifyListeners();
  }

  void getVaccine() {
    catchError(() async {
      getVaccineData = await di.nightlifeRepository.getVaccineContents();
    });
  }

  void getVaccineLimitSetting() {
    catchError(() async {
      final result = await di.nightlifeRepository.getVaccineLimit();
      if (result?.record?.isNotEmpty == true) {
        final limit = result?.record
            ?.where((element) => element.key == "vaccine_limit")
            .first;
        vaccineLimit = limit?.value ?? 5;
      } else {
        vaccineLimit = 5;
      }
      notifyListeners();
    });
  }

  void setVaccine(GetVaccineDataRecord e, int index) {
    // record[index] = GetNightlifePassportDataRecord()
    //   ..vaccineId = e.id
    //   ..vaccineName = e.fields?.first.lang;
    // if (record[0] != null) {
    //   vaccine1.text = record[0]?.vaccineName ?? '';
    // }
    // if (record[1] != null) {
    //   vaccine2.text = record[1]?.vaccineName ?? '';
    // }
    // if (record[2] != null) {
    //   vaccine3.text = record[2]?.vaccineName ?? '';
    // }

    // record[index] = GetNightlifePassportDataRecord()
    //   ..vaccineId = e.id
    //   ..vaccineName = e.fields?.first.lang;
    print("record[index]?.vaccineName ${e.fields?.first.lang}");
    if (vaccineControllers.isNotEmpty) {
      vaccinesList[index]["vaccine_id"] = e.id;
      vaccinesList[index]["vaccine_name"] = e.fields?.first.lang ?? '';

      vaccineControllers[index].text = e.fields?.first.lang ?? '';
    }

    validate();
    notifyListeners();
  }

  void register() {
    catchError(() async {
      setLoading(true);

      // var nightlifeEntity = RegisterNightlifeEntity();
      // nightlifeEntity.vaccinesReceived = record
      //     .where((element) => element != null)
      //     .map((e) => e!.vaccineId!)
      //     .toList();

      vaccinesList.forEach((element) {
        print("element ${element.toString()}");
      });
      var nightlifeEntity = RegisterNightlifeEntity();
      nightlifeEntity.vaccinesReceived = vaccinesList
          .where((element) => element["vaccine_name"] != null)
          .map((e) => e["vaccine_id"])
          .cast<int>()
          .toList();

      print("nightlifeEntity ------ ${nightlifeEntity.toJson()}");

      if (image != null) {
        var evidence = RegisterNightlifeEvidence();
        var imageRequest =
            await ImagePickerUtils.getImageUploadRequest(image!.path);
        evidence.name = imageRequest.fileName;
        evidence.image = imageRequest.fileData;
        nightlifeEntity.evidence = evidence;
      }

      if (getNightlifePassportData?.record?.isEmpty ?? false) {
        var result =
            await di.nightlifeRepository.registerNightlife(nightlifeEntity);
      } else {
        var result =
            await di.nightlifeRepository.updateNightlife(nightlifeEntity);
      }

      await Future.delayed(Duration(milliseconds: 400));
      getNightlifePassport();
      showAlertSuccess!();

      setLoading(false);
    });
  }

  void validate() {
    // isActive = record.where((element) => element != null).length >= 2 && (image != null || getNightlifePassportData?.document?.id != null);
    // isActive =
    //     vaccinesList.where((element) => element["vaccine_name"] != null).length >= 2;

    isActive = vaccinesList
                .where((element) => element["vaccine_name"] != null)
                .length >=
            2 &&
        (image != null || getNightlifePassportData?.document?.id != null);
  }

  void getNightlifePassport() {
    vaccinesList.clear();
    catchError(() async {
      setLoading(true);
      getNightlifePassportData =
          await di.nightlifeRepository.getNightlifePassport();
      if (getNightlifePassportData?.record?.length == 0) {
        Map<String, dynamic> data = {
          "vaccine_id": 0,
          "vaccine_name": null,
          "vaccination_order": 1,
          "vaccination_order_title": "เข็มที่ 1 "
        };
        var textEdit = TextEditingController();

        vaccineControllers.add(textEdit);
        vaccinesList.add(data);
      } else {
        if (vaccineLimit == 0) {
          getVaccineLimitSetting();
        }

        var vaccineNo = 0;
        getNightlifePassportData?.record?.forEach((element) {
          record.add(element);
          vaccineNo = vaccineNo + 1;
          Map<String, dynamic> data = {
            "vaccine_id": element.vaccineId,
            "vaccine_name": element.vaccineName,
            "vaccination_order": element.vaccinationOrder,
            "vaccination_order_title": "เข็มที่ $vaccineNo "
          };
          var textEdit = TextEditingController();
          textEdit.text = element.vaccineName ?? '';
          vaccineControllers.add(textEdit);
          vaccinesList.add(data);
        });
      }
      // record = [null, null, null];

      // List.generate(3, (index) {
      //   if ((getNightlifePassportData?.record?.length ?? 0) >= index + 1) {
      //     record[index] = getNightlifePassportData?.record![index];
      //   } else {
      //     record.add(null);
      //   }
      // });

      // if (record[0] != null) {
      //   vaccine1.text = record[0]?.vaccineName ?? '';
      // }
      // if (record[1] != null) {
      //   vaccine2.text = record[1]?.vaccineName ?? '';
      // }
      // if (record[2] != null) {
      //   vaccine3.text = record[2]?.vaccineName ?? '';
      // }

      print("vaccinesList --- ${vaccinesList.length}");
      validate();
      setLoading(false);
      notifyListeners();
    });
  }

  void addVaccines(int index) {
    if (vaccinesList.length < vaccineLimit) {
      Map<String, dynamic> data = {
        "vaccine_id": index,
        "vaccine_name": null,
        "vaccination_order": index + 1,
        "vaccination_order_title": "เข็มที่ ${index + 1} "
      };
      print("addVaccines ${data.toString()}");
      vaccinesList.add(data);
      var textEdit = TextEditingController();

      vaccineControllers.add(textEdit);
      validate();
      notifyListeners();
    }
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }
}
