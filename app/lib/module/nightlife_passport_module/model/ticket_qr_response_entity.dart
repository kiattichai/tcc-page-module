import 'package:app/generated/json/ticket_qr_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class TicketQrResponseEntity {
  TicketQrResponseEntity();

  factory TicketQrResponseEntity.fromJson(Map<String, dynamic> json) =>
      $TicketQrResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $TicketQrResponseEntityToJson(this);

  TicketQrResponseData? data;
  TicketQrResponseBench? bench;
}

@JsonSerializable()
class TicketQrResponseData {
  TicketQrResponseData();

  factory TicketQrResponseData.fromJson(Map<String, dynamic> json) =>
      $TicketQrResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $TicketQrResponseDataToJson(this);

  @JSONField(name: "ticket_no")
  String? ticketNo;
  String? mime;
  @JSONField(name: "image_data")
  String? imageData;
  int? expired;
}

@JsonSerializable()
class TicketQrResponseBench {
  TicketQrResponseBench();

  factory TicketQrResponseBench.fromJson(Map<String, dynamic> json) =>
      $TicketQrResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $TicketQrResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
