import 'package:app/generated/json/get_vaccine_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetVaccineEntity {

	GetVaccineEntity();

	factory GetVaccineEntity.fromJson(Map<String, dynamic> json) => $GetVaccineEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineEntityToJson(this);

	GetVaccineData? data;
	GetVaccineBench? bench;
}

@JsonSerializable()
class GetVaccineData {

	GetVaccineData();

	factory GetVaccineData.fromJson(Map<String, dynamic> json) => $GetVaccineDataFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataToJson(this);

	GetVaccineDataPagination? pagination;
	List<GetVaccineDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetVaccineDataPagination {

	GetVaccineDataPagination();

	factory GetVaccineDataPagination.fromJson(Map<String, dynamic> json) => $GetVaccineDataPaginationFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class GetVaccineDataRecord {

	GetVaccineDataRecord();

	factory GetVaccineDataRecord.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordToJson(this);

	int? id;
	dynamic? slug;
	GetVaccineDataRecordGroup? group;
	GetVaccineDataRecordCategory? category;
	List<GetVaccineDataRecordFields>? fields;
	List<GetVaccineDataRecordImages>? images;
	int? viewed;
	@JSONField(name: "viewed_text")
	int? viewedText;
	bool? status;
	@JSONField(name: "share_url")
	String? shareUrl;
	@JSONField(name: "webview_url")
	String? webviewUrl;
	@JSONField(name: "total_user")
	int? totalUser;
	@JSONField(name: "published_at")
	String? publishedAt;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
}

@JsonSerializable()
class GetVaccineDataRecordGroup {

	GetVaccineDataRecordGroup();

	factory GetVaccineDataRecordGroup.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordGroupFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordGroupToJson(this);

	int? id;
	String? code;
	String? name;
	String? description;
}

@JsonSerializable()
class GetVaccineDataRecordCategory {

	GetVaccineDataRecordCategory();

	factory GetVaccineDataRecordCategory.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordCategoryFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordCategoryToJson(this);

	GetVaccineDataRecordCategoryCurrent? current;
	List<GetVaccineDataRecordCategoryItems>? items;
}

@JsonSerializable()
class GetVaccineDataRecordCategoryCurrent {

	GetVaccineDataRecordCategoryCurrent();

	factory GetVaccineDataRecordCategoryCurrent.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordCategoryCurrentFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordCategoryCurrentToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
	dynamic? description;
	@JSONField(name: "meta_title")
	dynamic? metaTitle;
	@JSONField(name: "meta_description")
	dynamic? metaDescription;
	@JSONField(name: "meta_keyword")
	dynamic? metaKeyword;
}

@JsonSerializable()
class GetVaccineDataRecordCategoryItems {

	GetVaccineDataRecordCategoryItems();

	factory GetVaccineDataRecordCategoryItems.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordCategoryItemsFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordCategoryItemsToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
}

@JsonSerializable()
class GetVaccineDataRecordFields {

	GetVaccineDataRecordFields();

	factory GetVaccineDataRecordFields.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordFieldsFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordFieldsToJson(this);

	String? label;
	String? name;
	GetVaccineDataRecordFieldsType? type;
	@JSONField(name: "has_lang")
	bool? hasLang;
	bool? require;
	dynamic? options;
	String? lang;
}

@JsonSerializable()
class GetVaccineDataRecordFieldsType {

	GetVaccineDataRecordFieldsType();

	factory GetVaccineDataRecordFieldsType.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordFieldsTypeFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordFieldsTypeToJson(this);

	int? id;
	String? name;
	String? value;
}

@JsonSerializable()
class GetVaccineDataRecordImages {

	GetVaccineDataRecordImages();

	factory GetVaccineDataRecordImages.fromJson(Map<String, dynamic> json) => $GetVaccineDataRecordImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineDataRecordImagesToJson(this);

	String? id;
	String? tag;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	int? position;
}

@JsonSerializable()
class GetVaccineBench {

	GetVaccineBench();

	factory GetVaccineBench.fromJson(Map<String, dynamic> json) => $GetVaccineBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetVaccineBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
