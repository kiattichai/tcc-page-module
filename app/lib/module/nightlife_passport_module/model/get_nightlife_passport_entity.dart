import 'package:app/generated/json/get_nightlife_passport_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetNightlifePassportEntity {
  GetNightlifePassportEntity();

  factory GetNightlifePassportEntity.fromJson(Map<String, dynamic> json) =>
      $GetNightlifePassportEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetNightlifePassportEntityToJson(this);

  GetNightlifePassportData? data;
  GetNightlifePassportBench? bench;
}

@JsonSerializable()
class GetNightlifePassportData {
  GetNightlifePassportData();

  factory GetNightlifePassportData.fromJson(Map<String, dynamic> json) =>
      $GetNightlifePassportDataFromJson(json);

  Map<String, dynamic> toJson() => $GetNightlifePassportDataToJson(this);

  List<GetNightlifePassportDataRecord>? record;
  @JSONField(name: "user_data")
  GetNightlifePassportDataUserData? userData;
  GetNightlifePassportDataDocument? document;
  bool? cache;
}

@JsonSerializable()
class GetNightlifePassportDataRecord {
  GetNightlifePassportDataRecord();

  factory GetNightlifePassportDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetNightlifePassportDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetNightlifePassportDataRecordToJson(this);

  @JSONField(name: "vaccine_id")
  int? vaccineId;
  @JSONField(name: "vaccine_name")
  String? vaccineName;
  @JSONField(name: "vaccination_order")
  int? vaccinationOrder;
}

@JsonSerializable()
class GetNightlifePassportDataUserData {
  GetNightlifePassportDataUserData();

  factory GetNightlifePassportDataUserData.fromJson(
          Map<String, dynamic> json) =>
      $GetNightlifePassportDataUserDataFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetNightlifePassportDataUserDataToJson(this);

  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "mobile_phone")
  String? mobilePhone;
  @JSONField(name: "full_name")
  String? fullName;
  String? email;
  @JSONField(name: "verify_status")
  int? verifyStatus;
  @JSONField(name: "verify_remark")
  String? verifyRemark;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
}

@JsonSerializable()
class GetNightlifePassportDataDocument {
  GetNightlifePassportDataDocument();

  factory GetNightlifePassportDataDocument.fromJson(
          Map<String, dynamic> json) =>
      $GetNightlifePassportDataDocumentFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetNightlifePassportDataDocumentToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  String? url;
}

@JsonSerializable()
class GetNightlifePassportBench {
  GetNightlifePassportBench();

  factory GetNightlifePassportBench.fromJson(Map<String, dynamic> json) =>
      $GetNightlifePassportBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetNightlifePassportBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
