import 'package:app/generated/json/passport_data_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class PassportDataResponseEntity {
  PassportDataResponseEntity();

  factory PassportDataResponseEntity.fromJson(Map<String, dynamic> json) =>
      $PassportDataResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $PassportDataResponseEntityToJson(this);

  PassportDataResponseData? data;
  PassportDataResponseBench? bench;
}

@JsonSerializable()
class PassportDataResponseData {
  PassportDataResponseData();

  factory PassportDataResponseData.fromJson(Map<String, dynamic> json) =>
      $PassportDataResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $PassportDataResponseDataToJson(this);

  PassportDataResponseDataRecord? record;
}

@JsonSerializable()
class PassportDataResponseDataRecord {
  PassportDataResponseDataRecord();

  factory PassportDataResponseDataRecord.fromJson(Map<String, dynamic> json) =>
      $PassportDataResponseDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $PassportDataResponseDataRecordToJson(this);

  int? id;
  @JSONField(name: "product_id")
  int? productId;
  @JSONField(name: "product_variant_id")
  int? productVariantId;
  @JSONField(name: "ticket_no")
  String? ticketNo;
  @JSONField(name: "order_no")
  String? orderNo;
}

@JsonSerializable()
class PassportDataResponseBench {
  PassportDataResponseBench();

  factory PassportDataResponseBench.fromJson(Map<String, dynamic> json) =>
      $PassportDataResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $PassportDataResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
