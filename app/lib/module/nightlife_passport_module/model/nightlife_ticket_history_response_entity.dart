import 'package:app/generated/json/nightlife_ticket_history_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class NightlifeTicketHistoryResponseEntity {

	NightlifeTicketHistoryResponseEntity();

	factory NightlifeTicketHistoryResponseEntity.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseEntityToJson(this);

	NightlifeTicketHistoryResponseData? data;
	NightlifeTicketHistoryResponseBench? bench;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseData {

	NightlifeTicketHistoryResponseData();

	factory NightlifeTicketHistoryResponseData.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataToJson(this);

	NightlifeTicketHistoryResponseDataRecord? record;
	bool? cache;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseDataRecord {

	NightlifeTicketHistoryResponseDataRecord();

	factory NightlifeTicketHistoryResponseDataRecord.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataRecordToJson(this);

	NightlifeTicketHistoryResponseDataRecordPagination? pagination;
	List<NightlifeTicketHistoryResponseDataRecordData>? data;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseDataRecordPagination {

	NightlifeTicketHistoryResponseDataRecordPagination();

	factory NightlifeTicketHistoryResponseDataRecordPagination.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataRecordPaginationFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataRecordPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseDataRecordData {

	NightlifeTicketHistoryResponseDataRecordData();

	factory NightlifeTicketHistoryResponseDataRecordData.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataRecordDataFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataRecordDataToJson(this);

	int? id;
	@JSONField(name: "user_id")
	int? userId;
	@JSONField(name: "ticket_id")
	int? ticketId;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "store_name")
	String? storeName;
	@JSONField(name: "product_id")
	int? productId;
	@JSONField(name: "product_variant_id")
	int? productVariantId;
	@JSONField(name: "created_at")
	NightlifeTicketHistoryResponseDataRecordDataCreatedAt? createdAt;
	@JSONField(name: "updated_at")
	NightlifeTicketHistoryResponseDataRecordDataUpdatedAt? updatedAt;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseDataRecordDataCreatedAt {

	NightlifeTicketHistoryResponseDataRecordDataCreatedAt();

	factory NightlifeTicketHistoryResponseDataRecordDataCreatedAt.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataRecordDataCreatedAtFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataRecordDataCreatedAtToJson(this);

	String? value;
	String? date;
	String? time;
}

@JsonSerializable()
class NightlifeTicketHistoryResponseDataRecordDataUpdatedAt {

	NightlifeTicketHistoryResponseDataRecordDataUpdatedAt();

	factory NightlifeTicketHistoryResponseDataRecordDataUpdatedAt.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseDataRecordDataUpdatedAtFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseDataRecordDataUpdatedAtToJson(this);


}

@JsonSerializable()
class NightlifeTicketHistoryResponseBench {

	NightlifeTicketHistoryResponseBench();

	factory NightlifeTicketHistoryResponseBench.fromJson(Map<String, dynamic> json) => $NightlifeTicketHistoryResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $NightlifeTicketHistoryResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
