import 'package:app/generated/json/ticket_contents_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class TicketContentsResponseEntity {

	TicketContentsResponseEntity();

	factory TicketContentsResponseEntity.fromJson(Map<String, dynamic> json) => $TicketContentsResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseEntityToJson(this);

	TicketContentsResponseData? data;
	TicketContentsResponseBench? bench;
}

@JsonSerializable()
class TicketContentsResponseData {

	TicketContentsResponseData();

	factory TicketContentsResponseData.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataToJson(this);

	TicketContentsResponseDataPagination? pagination;
	List<TicketContentsResponseDataRecord>? record;
}

@JsonSerializable()
class TicketContentsResponseDataPagination {

	TicketContentsResponseDataPagination();

	factory TicketContentsResponseDataPagination.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataPaginationFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class TicketContentsResponseDataRecord {

	TicketContentsResponseDataRecord();

	factory TicketContentsResponseDataRecord.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordToJson(this);

	int? id;
	dynamic? slug;
	TicketContentsResponseDataRecordGroup? group;
	TicketContentsResponseDataRecordCategory? category;
	List<TicketContentsResponseDataRecordFields>? fields;
	List<TicketContentsResponseDataRecordImages>? images;
	int? viewed;
	@JSONField(name: "viewed_text")
	int? viewedText;
	bool? status;
	@JSONField(name: "share_url")
	String? shareUrl;
	@JSONField(name: "webview_url")
	String? webviewUrl;
	@JSONField(name: "total_user")
	int? totalUser;
	@JSONField(name: "published_at")
	String? publishedAt;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
}

@JsonSerializable()
class TicketContentsResponseDataRecordGroup {

	TicketContentsResponseDataRecordGroup();

	factory TicketContentsResponseDataRecordGroup.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordGroupFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordGroupToJson(this);

	int? id;
	String? code;
	String? name;
	String? description;
}

@JsonSerializable()
class TicketContentsResponseDataRecordCategory {

	TicketContentsResponseDataRecordCategory();

	factory TicketContentsResponseDataRecordCategory.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordCategoryFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordCategoryToJson(this);

	TicketContentsResponseDataRecordCategoryCurrent? current;
	List<TicketContentsResponseDataRecordCategoryItems>? items;
}

@JsonSerializable()
class TicketContentsResponseDataRecordCategoryCurrent {

	TicketContentsResponseDataRecordCategoryCurrent();

	factory TicketContentsResponseDataRecordCategoryCurrent.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordCategoryCurrentFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordCategoryCurrentToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
	dynamic? description;
	@JSONField(name: "meta_title")
	dynamic? metaTitle;
	@JSONField(name: "meta_description")
	dynamic? metaDescription;
	@JSONField(name: "meta_keyword")
	dynamic? metaKeyword;
}

@JsonSerializable()
class TicketContentsResponseDataRecordCategoryItems {

	TicketContentsResponseDataRecordCategoryItems();

	factory TicketContentsResponseDataRecordCategoryItems.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordCategoryItemsFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordCategoryItemsToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
}

@JsonSerializable()
class TicketContentsResponseDataRecordFields {

	TicketContentsResponseDataRecordFields();

	factory TicketContentsResponseDataRecordFields.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordFieldsFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordFieldsToJson(this);

	String? label;
	String? name;
	TicketContentsResponseDataRecordFieldsType? type;
	@JSONField(name: "has_lang")
	bool? hasLang;
	bool? require;
	dynamic? options;
	String? lang;
}

@JsonSerializable()
class TicketContentsResponseDataRecordFieldsType {

	TicketContentsResponseDataRecordFieldsType();

	factory TicketContentsResponseDataRecordFieldsType.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordFieldsTypeFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordFieldsTypeToJson(this);

	int? id;
	String? name;
	String? value;
}

@JsonSerializable()
class TicketContentsResponseDataRecordImages {

	TicketContentsResponseDataRecordImages();

	factory TicketContentsResponseDataRecordImages.fromJson(Map<String, dynamic> json) => $TicketContentsResponseDataRecordImagesFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseDataRecordImagesToJson(this);

	String? id;
	String? tag;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	int? position;
}

@JsonSerializable()
class TicketContentsResponseBench {

	TicketContentsResponseBench();

	factory TicketContentsResponseBench.fromJson(Map<String, dynamic> json) => $TicketContentsResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $TicketContentsResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
