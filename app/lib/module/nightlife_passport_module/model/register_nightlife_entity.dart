import 'package:app/generated/json/register_nightlife_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class RegisterNightlifeEntity {

	RegisterNightlifeEntity();

	factory RegisterNightlifeEntity.fromJson(Map<String, dynamic> json) => $RegisterNightlifeEntityFromJson(json);

	Map<String, dynamic> toJson() => $RegisterNightlifeEntityToJson(this);

	@JSONField(name: "vaccines_received")
	List<int>? vaccinesReceived;
	RegisterNightlifeEvidence? evidence;
}

@JsonSerializable()
class RegisterNightlifeEvidence {

	RegisterNightlifeEvidence();

	factory RegisterNightlifeEvidence.fromJson(Map<String, dynamic> json) => $RegisterNightlifeEvidenceFromJson(json);

	Map<String, dynamic> toJson() => $RegisterNightlifeEvidenceToJson(this);

	String? name;
	String? image;
}
