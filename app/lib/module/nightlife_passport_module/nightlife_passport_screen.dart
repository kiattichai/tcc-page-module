import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/nightlife_passport_module/nightlife_passport_screen_view_model.dart';
import 'package:app/module/nightlife_passport_module/register_nightlife_screen.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/utils/resize_service.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tix_navigate/tix_navigate.dart';

class NightlifePassportScreen extends StatefulWidget with TixRoute {
  NightlifePassportScreen({Key? key}) : super(key: key);

  @override
  _NightlifePassportScreenState createState() =>
      _NightlifePassportScreenState();

  @override
  String buildPath() {
    return '/nightlife_passport_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => NightlifePassportScreen());
  }
}

class _NightlifePassportScreenState extends BaseStateProvider<
    NightlifePassportScreen,
    NightlifePassportScreenViewModel> with TickerProviderStateMixin {
  late TabController tabController;
  int currentPage = 0;
  @override
  void initState() {
    super.initState();
    viewModel = NightlifePassportScreenViewModel();
    viewModel.showAlertError = showAlertError;
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(setActiveTabIndex);
    currentPage = tabController.index;
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<NightlifePassportScreenViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.transparent,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    appBar: AppBarWidget(
                      onBack: () {
                        WrapNavigation.instance.pop(context);
                      },
                      name: 'VACCINE PASSPORT',
                    ),
                    body: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 48,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: Color(0xffe0e0e0), width: 1))),
                          child: TabBar(
                            indicatorColor: Color(0xffda3534),
                            unselectedLabelColor: Color(0xff6d6d6d),
                            labelColor: Color(0xffda3534),
                            labelStyle: TextStyle(
                                color: const Color(0xff757575),
                                fontWeight: FontWeight.w700,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 18.0),
                            tabs: [
                              Tab(
                                text: 'บัตร',
                              ),
                              Tab(
                                text: 'ประวัติการใช้',
                              ),
                            ],
                            controller: tabController,
                          ),
                        ),
                        Expanded(
                          child: TabBarView(children: [
                            VaccinePassportTicketWidget(viewModel: viewModel),
                            Container(
                              child: Builder(builder: (context) {
                                return RefreshIndicator(
                                  color: Color(0xffda3534),
                                  key: model.refreshHistoryKey,
                                  onRefresh: () async {
                                    viewModel.clearPage();
                                    viewModel.getHistory();
                                    return Future.value();
                                  },
                                  child: viewModel.isHistoryEmpty
                                      ? ListView(
                                          physics:
                                              const AlwaysScrollableScrollPhysics(),
                                          controller: model.scrollController,
                                          children: <Widget>[
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 35,
                                                    ),
                                                    Text("ไม่มีข้อมูล",
                                                        style: const TextStyle(
                                                            color: const Color(
                                                                0xff676767),
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontFamily:
                                                                "Thonburi",
                                                            fontStyle: FontStyle
                                                                .normal,
                                                            fontSize: 18.0))
                                                  ],
                                                ),
                                              )
                                            ])
                                      : ListView.builder(
                                          physics:
                                              AlwaysScrollableScrollPhysics(),
                                          itemCount:
                                              viewModel.historysArray.length,
                                          controller: model.scrollController,
                                          itemBuilder: (context, index) {
                                            final model =
                                                viewModel.historysArray[index];
                                            return InkWell(
                                                onTap: () {
                                                  viewModel.clearPage();
                                                  viewModel.getHistory();
                                                },
                                                child: Container(
                                                  margin: const EdgeInsets.only(
                                                      left: 16, right: 16),
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 16, bottom: 16),
                                                  decoration: BoxDecoration(
                                                      border: Border(
                                                          bottom: BorderSide(
                                                              color: Color(
                                                                  0xffdedede)))),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      // Expanded(
                                                      //   child: Column(
                                                      //     crossAxisAlignment:
                                                      //         CrossAxisAlignment
                                                      //             .start,
                                                      //     children: [
                                                      //       // ร้านเรื่องเหล้าเช้านี้
                                                      //       Text(
                                                      //           model.storeName ??
                                                      //               '',
                                                      //           style: const TextStyle(
                                                      //               color: const Color(
                                                      //                   0xff333333),
                                                      //               fontWeight:
                                                      //                   FontWeight
                                                      //                       .w400,
                                                      //               fontFamily:
                                                      //                   "Thonburi",
                                                      //               fontStyle:
                                                      //                   FontStyle
                                                      //                       .normal,
                                                      //               fontSize:
                                                      //                   18.0)),
                                                      //       SizedBox(
                                                      //         height: 8,
                                                      //       ),
                                                      //       SizedBox(
                                                      //         height: 14,
                                                      //       )
                                                      //       // Text("${model.ticketId ?? 0}",
                                                      //       //     style: const TextStyle(
                                                      //       //         color: const Color(
                                                      //       //             0xff757575),
                                                      //       //         fontWeight:
                                                      //       //             FontWeight
                                                      //       //                 .w400,
                                                      //       //         fontFamily:
                                                      //       //             "SFUIText",
                                                      //       //         fontStyle:
                                                      //       //             FontStyle
                                                      //       //                 .normal,
                                                      //       //         fontSize: 12.0))
                                                      //     ],
                                                      //   ),
                                                      // ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(
                                                            model.storeName ??
                                                                '',
                                                            style: const TextStyle(
                                                                color: const Color(
                                                                    0xff333333),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "Thonburi",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize:
                                                                    18.0)),
                                                      ),
                                                      Expanded(
                                                          // flex: 1,
                                                          child: Text(
                                                              '${model.createdAt?.date ?? ''} ${model.createdAt?.time ?? ''}',
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                              style: const TextStyle(
                                                                  color: const Color(
                                                                      0xff757575),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontFamily:
                                                                      "SFUIText",
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal,
                                                                  fontSize:
                                                                      12.0)))
                                                    ],
                                                  ),
                                                ));
                                          }),
                                );
                              }),
                            )
                          ], controller: tabController),
                        )
                      ],
                    ),
                  )));
        });
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void setActiveTabIndex() {
    currentPage = tabController.index;
    print('tab index : $currentPage');
    if (currentPage == 1) {
      viewModel.clearPage();
      viewModel.getHistory();
    }
  }
}

class VaccinePassportTicketWidget extends StatelessWidget {
  final NightlifePassportScreenViewModel viewModel;

  const VaccinePassportTicketWidget({Key? key, required this.viewModel})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Expanded(
            child: Container(
              width: Screen.width,
              alignment: Alignment.center,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(6.0),
                child: Image.network(
                    ResizeService.resize(
                        null, null, viewModel.ticketBanner?.toJson()),
                    //viewModel.ticketBanner ?? '',
                    fit: BoxFit.cover,
                    width: Screen.width,
                    errorBuilder: (context, url, error) => Container()),
              ),
              decoration: BoxDecoration(
                  // color: const Color(0xffda3534),
                  borderRadius: BorderRadius.circular(6),
                  border: Border.all(color: Color(0xffdedede))),
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(13, 6, 15, 6),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(color: Color(0xffdedede))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            style: TextStyle(
                                color: Color(0xff333333).withOpacity(0.6),
                                fontWeight: FontWeight.w400,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 16.0),
                            text: "ชื่อ : "),
                        TextSpan(
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w700,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 18.0),
                            text: viewModel.getNightlifePassportData?.userData
                                    ?.fullName ??
                                ''),
                      ])),
                      SizedBox(
                        height: 9,
                      ),
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            style: TextStyle(
                                color: Color(0xff333333).withOpacity(0.6),
                                fontWeight: FontWeight.w400,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 16.0),
                            text: "เบอร์โทร : "),
                        TextSpan(
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w700,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 18.0),
                            text: viewModel.getNightlifePassportData?.userData
                                    ?.mobilePhone ??
                                ''),
                      ])),
                      SizedBox(height: 6),
                      viewModel.userHasVaccineMoreThan3()
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text("ข้อมูลการฉีดวัคซีน : ",
                                        style: TextStyle(
                                            color: Color(0xff333333)
                                                .withOpacity(0.6),
                                            fontWeight: FontWeight.w400,
                                            fontFamily: "Thonburi",
                                            fontStyle: FontStyle.normal,
                                            fontSize: 16.0)),
                                    Container(
                                      padding: EdgeInsets.only(right: 4),
                                      child: Text(
                                          "${viewModel.getNightlifePassportData?.record?.length ?? 0} เข็ม",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: const Color(0xff333333),
                                              fontWeight: FontWeight.w700,
                                              fontFamily: "Thonburi",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0)),
                                    )
                                  ],
                                ),
                                SizedBox(height: 6),
                                InkWell(
                                  onTap: () {
                                    WrapNavigation.instance.pushNamed(
                                        context, RegisterNightlifeScreen());
                                  },
                                  child: new Container(
                                      margin:
                                          EdgeInsets.only(left: 16, top: 16),
                                      alignment: Alignment.center,
                                      child: new Text("ดูข้อมูลวัคซีน",
                                          style: TextStyle(
                                            fontFamily: 'Thonburi',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      width: 129,
                                      height: 30,
                                      decoration: new BoxDecoration(
                                          border: Border.all(
                                              color: Color(0xffe0e0e0)),
                                          color: Color(0xfff4f4f4),
                                          borderRadius:
                                              BorderRadius.circular(5))),
                                )
                              ],
                              mainAxisSize: MainAxisSize.min,
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("ข้อมูลการฉีดวัคซีน :",
                                    style: TextStyle(
                                        color:
                                            Color(0xff333333).withOpacity(0.6),
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "Thonburi",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 16.0)),
                                SizedBox(height: 6),
                                Container(
                                  padding: EdgeInsets.only(right: 4),
                                  child: Text(viewModel.vaccines,
                                      maxLines: 5,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          color: const Color(0xff333333),
                                          fontWeight: FontWeight.w700,
                                          fontFamily: "Thonburi",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 16.0)),
                                ),
                                SizedBox(height: 8),
                                viewModel.enableButtonAddVaccine()
                                    ? InkWell(
                                        onTap: () {
                                          WrapNavigation.instance.pushNamed(
                                              context,
                                              RegisterNightlifeScreen());
                                        },
                                        child: new Container(
                                            alignment: Alignment.center,
                                            child: new Text("เพิ่มข้อมูลวัคซีน",
                                                style: TextStyle(
                                                  fontFamily: 'Thonburi',
                                                  color: Color(0xff6d6d6d),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w700,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                            width: 129,
                                            height: 30,
                                            decoration: new BoxDecoration(
                                                border: Border.all(
                                                    color: Color(0xffe0e0e0)),
                                                color: Color(0xfff4f4f4),
                                                borderRadius:
                                                    BorderRadius.circular(5))),
                                      )
                                    : SizedBox(height: 8)
                              ],
                              mainAxisSize: MainAxisSize.min,
                            ),
                    ],
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        viewModel.qrCodeData.length == 0
                            ? Container(
                                width: Screen.width / 2.884,
                                height: Screen.width / 2.884,
                                decoration: BoxDecoration(color: Colors.white))
                            : Container(
                                width: Screen.width / 2.884,
                                height: Screen.width / 2.884,
                                child: Center(
                                    child: QrImage(
                                  data: viewModel.qrCodeData,
                                  version: QrVersions.auto,
                                  // size: Screen.width /
                                  //     2.884,
                                  padding: EdgeInsets.zero,
                                )),
                              ),
                        Text("แสดง QR code นี้\nเมื่อต้องการใช้สิทธิ์",
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w700,
                                fontFamily: "Thonburi",
                                fontStyle: FontStyle.normal,
                                fontSize: 14.0))
                      ],
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
