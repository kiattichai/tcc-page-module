import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/nightlife_passport_module/component/nightlife_register_success_dialog.dart';
import 'package:app/module/nightlife_passport_module/register_nightlife_screen_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tix_navigate/tix_navigate.dart';

import 'component/how_to_register_nightlife_dialog.dart';

class RegisterNightlifeScreen extends StatefulWidget with TixRoute {
  final bool? forceFocus;
  RegisterNightlifeScreen({Key? key, this.forceFocus}) : super(key: key);

  @override
  _RegisterNightlifeScreenState createState() =>
      _RegisterNightlifeScreenState();

  @override
  String buildPath() {
    return '/register_nightlife_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => RegisterNightlifeScreen(forceFocus: data));
  }
}

class _RegisterNightlifeScreenState extends BaseStateProvider<
    RegisterNightlifeScreen, RegisterNightlifeScreenViewModel> {
  @override
  void initState() {
    viewModel = RegisterNightlifeScreenViewModel();
    viewModel.showAlertError = showAlertError;
    viewModel.showAlertSuccess = showAlertSuccess;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<RegisterNightlifeScreenViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return WillPopScope(
              child: ProgressHUD(
                  opacity: 1.0,
                  color: Colors.transparent,
                  progressIndicator: CircleLoading(),
                  inAsyncCall: viewModel.loading,
                  child: Scaffold(
                    appBar: AppBarWidget(
                      name: 'VACCINE PASSPORT',
                      onBack: widget.forceFocus == false ||
                              widget.forceFocus == null
                          ? () {
                              WrapNavigation.instance.pop(context);
                            }
                          : null,
                    ),
                    body: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Image.asset('assets/banner_tcc_passport.png'),
                          InkWell(
                            onTap: () {
                              showAlertSuccess();
                            },
                            child: Image.asset('assets/vaccine_banner.png'),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(16, 18, 16, 8),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xfff5f5f5), width: 9))),
                            child: RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  style: const TextStyle(
                                      color: const Color(0xff333333),
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Thonburi-bold",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0),
                                  text: "Vaccine Passport "),
                              TextSpan(
                                  style: const TextStyle(
                                      color: const Color(0xff333333),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Thonburi",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0),
                                  text:
                                      "พาสปอร์ตที่ช่วยคุณในการยืนยันตัวตนจากการรับวัคซีน ใช้งานง่าย เพียงแค่เปิดผ่าน The Concert Application\n\n"
                                      "ใช้ได้ทั้งงาน Concert , Music Festival หรือสำหรับร้านค้าต่างๆ ตามสไตล์ New Nightlife สังคมกลางคืนไร้โควิด\n\n"
                                      "เพียงแค่สมัครสมาชิก และยืนยันหลักฐานการรับวัคซีนครบ 2 เข็ม หรือกรณีการบูสเตอร์เพิ่มเติม เพียงเท่านี้ คุณก็จะได้รับ Vaccine passport"
                                      "เพื่อใช้เช็คอินเข้างานคอนเสิร์ต และร้านค้าต่างๆสำหรับเก็บ Tracking ข้อมูลงานที่เข้าร่วม และร้านค้าที่คุณไปเที่ยว\n\n"
                                      "ไม่ต้องจำ ไม่ต้องลืม ระบบช่วยจำให้คุณเอง พร้อม Notification แจ้งเตือนหากคุณเข้าใกล้พื้นที่เสี่ยง และสิทธิประโยชน์อีกมายมายจาก The Concert"),
                            ])),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(16, 13, 16, 20),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xfff5f5f5), width: 9))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                viewModel.getNightlifePassportData?.userData
                                            ?.verifyStatus ==
                                        2
                                    ? Container(
                                        width: double.infinity,
                                        padding: const EdgeInsets.all(8),
                                        margin:
                                            const EdgeInsets.only(bottom: 8),
                                        child: Text(
                                          viewModel.getNightlifePassportData
                                                  ?.userData?.verifyRemark ??
                                              '',
                                          style: const TextStyle(
                                              color: const Color(0xffd90011),
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Thonburi",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 18.0),
                                        ),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(3)),
                                            border: Border.all(
                                                color: const Color(0xfff5c6bd),
                                                width: 1),
                                            color: Color(0xfffdede9)),
                                      )
                                    : SizedBox(),
                                Text("ระบุวัคซีนที่คุณได้รับ",
                                    style: const TextStyle(
                                        color: const Color(0xff333333),
                                        fontWeight: FontWeight.w700,
                                        fontFamily: "Thonburi",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 18.0)),
                                // Container(
                                //   padding:
                                //       const EdgeInsets.fromLTRB(14, 20, 10, 0),
                                //   child: Row(
                                //     children: [
                                //       Text(
                                //         "เข็มที่ 1 ",
                                //         style: const TextStyle(
                                //             color: const Color(0xff333333),
                                //             fontWeight: FontWeight.w400,
                                //             fontFamily: "Thonburi",
                                //             fontStyle: FontStyle.normal,
                                //             fontSize: 16),
                                //       ),
                                //       Expanded(
                                //           child: TextFormField(
                                //         onTap: () {
                                //           showModalCategoryAction(0);
                                //         },
                                //         controller: viewModel.vaccine1,
                                //         readOnly: true,
                                //         decoration: InputDecoration(
                                //             isDense: true,
                                //             suffixIconConstraints:
                                //                 BoxConstraints(maxHeight: 20),
                                //             contentPadding: EdgeInsets.fromLTRB(
                                //                 0, 10, 0, 10),
                                //             suffixIcon: Icon(
                                //               Icons.arrow_forward_ios_sharp,
                                //               size: 15,
                                //               color: Color(0xff545454),
                                //             ),
                                //             border: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             enabledBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             focusedBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             )),
                                //       )),
                                //     ],
                                //   ),
                                // ),
                                // Container(
                                //   padding:
                                //       const EdgeInsets.fromLTRB(14, 20, 10, 0),
                                //   child: Row(
                                //     children: [
                                //       // เข็มที่ 1
                                //       Text(
                                //         "เข็มที่ 2 ",
                                //         style: const TextStyle(
                                //             color: const Color(0xff333333),
                                //             fontWeight: FontWeight.w400,
                                //             fontFamily: "Thonburi",
                                //             fontStyle: FontStyle.normal,
                                //             fontSize: 16),
                                //       ),
                                //       Expanded(
                                //           child: TextFormField(
                                //         onTap: () {
                                //           showModalCategoryAction(1);
                                //         },
                                //         controller: viewModel.vaccine2,
                                //         readOnly: true,
                                //         decoration: InputDecoration(
                                //             isDense: true,
                                //             suffixIconConstraints:
                                //                 BoxConstraints(maxHeight: 20),
                                //             contentPadding: EdgeInsets.fromLTRB(
                                //                 0, 10, 0, 10),
                                //             suffixIcon: Icon(
                                //               Icons.arrow_forward_ios_sharp,
                                //               size: 15,
                                //               color: Color(0xff545454),
                                //             ),
                                //             border: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             enabledBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             focusedBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             )),
                                //       )),
                                //     ],
                                //   ),
                                // ),
                                // Container(
                                //   padding:
                                //       const EdgeInsets.fromLTRB(14, 20, 10, 0),
                                //   child: Row(
                                //     children: [
                                //       // เข็มที่ 1
                                //       Text(
                                //         "เข็มที่ 3 ",
                                //         style: const TextStyle(
                                //             color: const Color(0xff333333),
                                //             fontWeight: FontWeight.w400,
                                //             fontFamily: "Thonburi",
                                //             fontStyle: FontStyle.normal,
                                //             fontSize: 16),
                                //       ),
                                //       Expanded(
                                //           child: TextFormField(
                                //         controller: viewModel.vaccine3,
                                //         readOnly: true,
                                //         onTap: () {
                                //           showModalCategoryAction(2);
                                //         },
                                //         decoration: InputDecoration(
                                //             isDense: true,
                                //             suffixIconConstraints:
                                //                 BoxConstraints(maxHeight: 20),
                                //             contentPadding: EdgeInsets.fromLTRB(
                                //                 0, 10, 0, 10),
                                //             suffixIcon: Icon(
                                //               Icons.arrow_forward_ios_sharp,
                                //               size: 15,
                                //               color: Color(0xff545454),
                                //             ),
                                //             border: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             enabledBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             ),
                                //             focusedBorder: UnderlineInputBorder(
                                //               borderSide: BorderSide(
                                //                   color: Color(0xffe0e0e0),
                                //                   width: 1),
                                //             )),
                                //       )),
                                //     ],
                                //   ),
                                // ),

                                Container(
                                  child: Column(
                                    children: renderVacceines(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(16, 13, 16, 8),
                            width: Screen.width,
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xfff5f5f5), width: 9))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("อัปโหลดข้อมูลการฉีดวัคซีนของคุณ",
                                    style: const TextStyle(
                                        color: const Color(0xff333333),
                                        fontWeight: FontWeight.w700,
                                        fontFamily: "Thonburi",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 18.0)),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Image.asset(
                                      'assets/info.png',
                                      width: 16,
                                      height: 16,
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Expanded(
                                      child: InkWell(
                                        onTap: () {
                                          openDialog();
                                        },
                                        child: RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                              style: const TextStyle(
                                                  color:
                                                      const Color(0xff333333),
                                                  fontWeight: FontWeight.w400,
                                                  decoration:
                                                      TextDecoration.underline,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 16.0),
                                              text:
                                                  "ดูขั้นตอนการอัปโหลดข้อมูล"),
                                          TextSpan(
                                              style: const TextStyle(
                                                  color:
                                                      const Color(0xff333333),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 16.0),
                                              text: 'การฉีดวัคซีน'),
                                        ])),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                InkWell(
                                  onTap: () {
                                    viewModel.picImage();
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    height: (viewModel.image != null ||
                                            viewModel.getNightlifePassportData
                                                    ?.document !=
                                                null)
                                        ? 160
                                        : 70,
                                    child: DottedBorder(
                                      borderType: BorderType.RRect,
                                      radius: Radius.circular(6),
                                      dashPattern: [8, 2],
                                      // padding: EdgeInsets.all(6),
                                      color: Color(0xff757575),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6)),
                                        child: viewModel.image != null
                                            ? Container(
                                                width: double.infinity,
                                                height: 150,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(6)),
                                                    image: DecorationImage(
                                                        image: FileImage(
                                                            viewModel.image!))),
                                              )
                                            : viewModel.getNightlifePassportData
                                                        ?.document?.url !=
                                                    null
                                                ? Container(
                                                    width: double.infinity,
                                                    height: 150,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    6)),
                                                        image: DecorationImage(
                                                            image: NetworkImage(
                                                                viewModel
                                                                        .getNightlifePassportData
                                                                        ?.document
                                                                        ?.url ??
                                                                    ''))),
                                                  )
                                                : Container(
                                                    alignment: Alignment.center,
                                                    width: double.infinity,
                                                    padding: EdgeInsets.all(6),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Image.asset(
                                                          'assets/upload.png',
                                                          width: 16,
                                                          height: 16,
                                                        ),
                                                        SizedBox(
                                                          width: 15,
                                                        ),
                                                        Text("คลิกเพื่ออัปโหลด",
                                                            style: const TextStyle(
                                                                color: const Color(
                                                                    0xff333333),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                                fontFamily:
                                                                    "Thonburi",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: 18.0))
                                                      ],
                                                    )),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    bottomNavigationBar: Padding(
                      padding: EdgeInsets.only(
                          left: 16.0,
                          right: 16.0,
                          bottom: 10.0 + MediaQuery.of(context).padding.bottom,
                          top: 10),
                      child: InkWell(
                        child: Container(
                          width: double.infinity,
                          height: 39,
                          alignment: Alignment.center,
                          decoration: new BoxDecoration(
                              color: viewModel.isActive
                                  ? Color(0xffda3534)
                                  : Color(0xffe0e0e0),
                              borderRadius: BorderRadius.circular(5)),
                          child: Text("ดำเนินการ",
                              style: const TextStyle(
                                  color: const Color(0xffffffff),
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Thonburi-Bold",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 14.0),
                              textAlign: TextAlign.center),
                        ),
                        onTap: () {
                          if (viewModel.isActive) {
                            viewModel.register();
                          }
                        },
                      ),
                    ),
                  )),
              onWillPop: () async {
                return widget.forceFocus == true ? false : true;
              });
        });
  }

  openDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return HowToRegisterNightlifeDialog();
        });
  }

  showModalCategoryAction(int index) {
    showModalBottomSheet(
        context: TixNavigate.instance.navigatorKey?.currentContext ?? context,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewPadding.bottom),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8, right: 8),
                          child: Image.asset(
                            'assets/x_copy.png',
                            color: Color(0xff1f1f1f),
                            width: 28,
                            height: 28,
                          ),
                        ),
                      )
                    ],
                  ),
                  ...(viewModel.getVaccineData?.record ?? []).map((e) {
                    return InkWell(
                      onTap: () {
                        viewModel.setVaccine(e, index);
                        Navigator.pop(context);
                      },
                      child: Stack(
                        children: [
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 16, right: 16, top: 12, bottom: 12),
                              width: Screen.width,
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Color(0xffeeeeee), width: 1))),
                              child: Center(
                                child: Text("${e.fields?.first.lang ?? ''}",
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Color(0xff424242),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ))
                        ],
                      ),
                    );
                  })
                ],
              ),
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(14),
                      topRight: Radius.circular(14))));
        });
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  showAlertSuccess() {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return NightlifeRegisterSuccessDialog(
            onTap: () {
              if (widget.forceFocus == true) {
                WrapNavigation.instance
                    .nativePushNamed(this.context, ACTION_OPEN_MAIN_SCREEN);
              } else {
                WrapNavigation.instance.pop(this.context);
              }
            },
          );
        });
  }

  List<Widget> renderVacceines() {
    List<Widget> columnOfVacceines = [];
    for (var i = 0; i < viewModel.vaccinesList.length; i++) {
      columnOfVacceines.add(Container(
        padding: const EdgeInsets.fromLTRB(14, 20, 10, 0),
        child: Row(
          children: [
            Text(
              // "เข็มที่ 1 ",
              viewModel.vaccinesList[i]["vaccination_order_title"],
              style: const TextStyle(
                  color: const Color(0xff333333),
                  fontWeight: FontWeight.w400,
                  fontFamily: "Thonburi",
                  fontStyle: FontStyle.normal,
                  fontSize: 16),
            ),
            Expanded(
                child: TextFormField(
              onTap: () {
                showModalCategoryAction(i);
              },
              controller: viewModel.vaccineControllers[i],
              readOnly: true,
              decoration: InputDecoration(
                  hintText: "กรุณาเลือกวัคซีน",
                  isDense: true,
                  suffixIconConstraints: BoxConstraints(maxHeight: 20),
                  contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  suffixIcon: Icon(
                    Icons.arrow_forward_ios_sharp,
                    size: 15,
                    color: Color(0xff545454),
                  ),
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffe0e0e0), width: 1),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffe0e0e0), width: 1),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffe0e0e0), width: 1),
                  )),
            )),
          ],
        ),
      ));
    }

    if (viewModel.vaccinesList.length < viewModel.vaccineLimit) {
      print("list --- ${viewModel.vaccinesList}");
      columnOfVacceines.add(InkWell(
          onTap: () {
            viewModel.addVaccines(viewModel.vaccinesList.length);
          },
          child: Container(
              padding: const EdgeInsets.fromLTRB(14, 20, 10, 0),
              width: Screen.width,
              child: Row(
                children: [
                  SizedBox(
                    width: 45,
                  ),
                  Expanded(
                      child: Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset('assets/icon_plus_circle.svg'),
                              SizedBox(
                                width: 10,
                              ),
                              Text("เพิ่ม",
                                  style: const TextStyle(
                                      color: const Color(0xff6d6d6d),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Thonburi",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0))
                            ],
                          ),
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              border: Border.all(
                                  color: const Color(0xffe0e0e0), width: 1),
                              color: const Color(0xffffffff))))
                ],
              ))));
    } else {
      columnOfVacceines.add(SizedBox());
    }

    return columnOfVacceines;
  }
}
