import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_nightlife_passport_entity.dart';
import 'package:app/module/nightlife_passport_module/model/get_vaccine_entity.dart';
import 'package:app/module/nightlife_passport_module/model/nightlife_ticket_history_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/passport_data_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/register_nightlife_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_contents_response_entity.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_qr_response_entity.dart';
import 'package:flutter/material.dart';

class NightlifePassportScreenViewModel extends BaseViewModel {
  GetNightlifePassportData? getNightlifePassportData;
  GlobalKey<RefreshIndicatorState>? refreshHistoryKey;
  bool isHistoryEmpty = false;
  String vaccines = '';
  late ScrollController scrollController;
  String qrCodeData = '';
  TicketContentsResponseDataRecordImages? ticketBanner;
  String? ticketName;
  int page = 1;
  int perPage = 20;
  bool isComplete = false;
  bool loadMore = false;

  int? productId;
  int? ticketId;
  int? productVariantId;
  // late TabController tabController;
  // int currentPage = 0;

  int vaccineLimit = 5;

  List<NightlifeTicketHistoryResponseDataRecordData> historysArray = [];

  Function(BaseError)? showAlertError;

  PassportDataResponseData? passPortData;
  TicketQrResponseData? ticketQrCode;

  NightlifePassportScreenViewModel() {
    refreshHistoryKey = GlobalKey<RefreshIndicatorState>();
    scrollController = new ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0 && !isComplete) {
        setPage(page + 1);
        print(page);
        getHistory();
      }
    });
  }

  @override
  void postInit() {
    getNightlifePassport();
    getPassportData();
    getVaccineLimit();
    super.postInit();
  }

  void getVaccineLimit() {
    catchError(() async {
      final result = await di.nightlifeRepository.getVaccineLimit();
      if (result?.record?.isNotEmpty == true) {
        final limit = result?.record
            ?.where((element) => element.key == "vaccine_limit")
            .first;
        vaccineLimit = limit?.value ?? 5;
        print('vaccineLimit $vaccineLimit');
      }
      notifyListeners();
    });
  }

  void getNightlifePassport() {
    catchError(() async {
      setLoading(true);
      getNightlifePassportData =
          await di.nightlifeRepository.getNightlifePassport();

      vaccines = getNightlifePassportData?.record
              ?.map((e) =>
                  'เข็มที่ ${((getNightlifePassportData?.record?.indexOf(e) ?? 0) + 1).toString()} ${e.vaccineName}')
              .join("\n") ??
          '';
      setLoading(false);
    });
  }

  void getPassportData() {
    catchError(() async {
      final result =
          await di.nightlifeRepository.getNightlifePassportPassportData();
      passPortData = result.data;
      if (passPortData != null) {
        productId = passPortData?.record?.productId;
        ticketId = passPortData?.record?.id;
        productVariantId = passPortData?.record?.productVariantId;
        final ticketData = await di.nightlifeRepository
            .getNightlifeTicket(productId ?? 0, ticketId ?? 0);
        ticketQrCode = ticketData.data;
        //print("ticketQrCode ${ticketQrCode?.imageData}");
        final ticketContent = await di.nightlifeRepository.getTicketContent();
        final ticketContentImage =
            ticketContent.data?.record!.map((e) => e.images).first;
        final ticketContentName = ticketContent.data?.record!
            .map((e) => e.fields)
            .map((e) => e?.first.lang)
            .first;
        ticketBanner = ticketContentImage?.first;
        ticketName = ticketContentName;
        qrCodeData = ticketQrCode?.imageData ?? '';
        getHistory();
      }
      notifyListeners();
    });
  }

  //getHistory
  void getHistory() {
    print("getHistory page ===== $page");
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }
    catchError(() async {
      NightlifeTicketHistoryResponseData? tmp;
      if (isComplete == false) {
        tmp = await di.nightlifeRepository
            .getHistory(ticketId, productId, productVariantId, page, perPage);
      }

      if (tmp != null &&
          tmp.record?.data != null &&
          (tmp.record?.data?.length ?? 0) > 0 &&
          (tmp.record?.pagination?.currentPage ?? 0) <=
              (tmp.record?.pagination?.lastPage ?? 0)) {
        historysArray..addAll(tmp.record?.data ?? []);

        if (historysArray.length == 0) {
          isHistoryEmpty = true;
        } else {
          isHistoryEmpty = false;
        }
      } else {
        if (historysArray.length == 0) {
          isHistoryEmpty = true;
        } else {
          isHistoryEmpty = false;
        }
        setIsComplete(true);
      }

      setLoading(false);
      setLoadMore(false);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    isComplete = value;
  }

  void setLoadMore(bool value) {
    loadMore = value;
  }

  void clearPage() {
    page = 1;
    isComplete = false;
    historysArray.clear();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError?.call(error);
    }
  }

  bool enableButtonAddVaccine() {
    if (getNightlifePassportData == null) {
      return false;
    }
    return true;
    return (getNightlifePassportData?.record?.length ?? 0) <= vaccineLimit;
  }

  bool userHasVaccineMoreThan3() {
    return (getNightlifePassportData?.record?.length ?? 0) > 3;
  }
}
