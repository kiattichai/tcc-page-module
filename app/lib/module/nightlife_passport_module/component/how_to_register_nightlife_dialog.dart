import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HowToRegisterNightlifeDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(left: 6, right: 6),
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
          child: Image.asset('assets/how_to_upload.png'),
        ),
        Positioned(
            child: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
                child: Image.asset(
              'assets/x.png',
              width: 26,
              height: 26,
            )),
            right: 0,
            top: 0)
      ],
    );
  }
}
