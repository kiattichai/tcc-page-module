import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NightlifeRegisterSuccessDialog extends StatelessWidget {
  final Function() onTap;

  const NightlifeRegisterSuccessDialog({Key? key, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(left: 6, right: 6),
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: const Color(0xffffffff)),
          margin:
              const EdgeInsets.only(top: 14, right: 10, left: 10, bottom: 25),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 16,
              ),
              Image.asset(
                'assets/tcc_passport_icon.png',
                width: 80,
                height: 95,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                  "The Concert ได้รับข้อมูลของคุณแล้ว ขณะนี้อยู่ระหว่างการตรวจสอบ  ท่านจะได้รับข้อความแจ้งเตือน  เมื่อVACCINE PASSPORT ส่งถึงคุณ",
                  style: const TextStyle(
                      color: const Color(0xff333333),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Thonburi",
                      fontStyle: FontStyle.normal,
                      fontSize: 16.0),
                  textAlign: TextAlign.center),
              SizedBox(
                height: 25,
              ),
              InkWell(
                onTap: () {
                  onTap();
                  WrapNavigation.instance.pop(context);
                },
                child: Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.fromLTRB(16, 0, 16, 25),
                    padding: const EdgeInsets.fromLTRB(10, 9, 10, 9),
                    child: // ตกลง
                        Text("ตกลง",
                            style: const TextStyle(
                                color: const Color(0xffffffff),
                                fontWeight: FontWeight.w700,
                                fontFamily: "HelveticaNeue",
                                fontStyle: FontStyle.normal,
                                fontSize: 16.0)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: const Color(0xffda3534),
                    )),
              )
            ],
          ),
        ),
      ],
    );
  }
}
