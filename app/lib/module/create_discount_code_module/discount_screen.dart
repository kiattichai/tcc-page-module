import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/create_discount_code_module/create_discount_screen.dart';
import 'package:app/module/create_discount_code_module/discount_screen_viewmodel.dart';
import 'package:app/module/create_discount_code_module/manage_code_scren.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:intl/intl.dart';

class DiscountScreen extends StatefulWidget with TixRoute {
  int? concertId;
  String? storeId;

  DiscountScreen({Key? key, this.concertId, this.storeId}) : super(key: key);

  @override
  String buildPath() {
    return '/discount_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) =>
            DiscountScreen(concertId: data['concertId'], storeId: data['storeId']));
  }

  @override
  _DiscountScreenState createState() => _DiscountScreenState();
}

class _DiscountScreenState extends BaseStateProvider<DiscountScreen, DiscountScreenViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = DiscountScreenViewModel(widget.storeId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DiscountScreenViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.red),
                titleSpacing: 0.0,
                title: Text(
                  "โค้ดส่วนลด",
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff4a4a4a),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                actions: [
                  Center(
                    child: InkWell(
                      onTap: () async {
                        //final result = await TixNavigate.instance.navigateTo(CreateDiscountScreen(), data: {'concertId': widget.concertId, 'storeId': widget.storeId, 'discountRecord': null});
                        final result = await WrapNavigation.instance.pushNamed(
                            context, CreateDiscountScreen(), arguments: {
                          'concertId': widget.concertId,
                          'storeId': widget.storeId,
                          'discountRecord': null
                        });
                        if (result != null) {
                          viewModel.getDiscount();
                          //TixNavigate.instance.navigateTo(ManageCodeScreen(),data: {'storeId':viewModel.storeId,'discountId':result});
                          WrapNavigation.instance.pushNamed(context, ManageCodeScreen(),
                              arguments: {'storeId': viewModel.storeId, 'discountId': result});
                        } else {
                          viewModel.getDiscount();
                        }
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 16.0),
                        child: Text(
                          "สร้าง",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff4a4a4a),
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: RefreshIndicator(
                onRefresh: () {
                  viewModel.discountRecord.clear();
                  viewModel.getDiscount();
                  return Future.value();
                },
                child: viewModel.discountRecord.length != 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: viewModel.discountRecord.length,
                        itemBuilder: (context, index) {
                          var item = viewModel.discountRecord[index];
                          DateTime dt = DateTime.parse('${item.promotionTime!.endDate}');
                          String showTime = DateFormat('dd MMM yyyy HH:mm', 'th').format(dt);

                          return Container(
                            margin: EdgeInsets.only(left: 16.0, right: 16.0),
                            padding: EdgeInsets.only(bottom: 15.0),
                            decoration: BoxDecoration(
                                border: Border(
                              bottom: BorderSide(color: Colors.grey.shade300),
                            )),
                            child: Column(
                              children: [
                                //TODO title
                                index == 0
                                    ? Container(
                                        padding: EdgeInsets.only(top: 19.0, bottom: 6.0),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(color: Colors.grey.shade300))),
                                        child: Row(
                                          children: [
                                            Text(
                                              "ชื่อส่วนลด",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet',
                                                color: Color(0xff6d6d6d),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            ),
                                            Spacer(),
                                            Text(
                                              "การเปิดใช้งาน",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet',
                                                color: Color(0xff6d6d6d),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                Row(
                                  children: [
                                    Text(
                                      item.title ?? '',
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff08080a),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.edit,
                                      ),
                                      iconSize: 18.0,
                                      onPressed: () async {
                                        //final result = await TixNavigate.instance.navigateTo(CreateDiscountScreen(), data: {'concertId': widget.concertId, 'storeId': widget.storeId, 'discountRecord':viewModel.discountRecord[index]});
                                        final result = await WrapNavigation.instance
                                            .pushNamed(context, CreateDiscountScreen(), arguments: {
                                          'concertId': widget.concertId,
                                          'storeId': widget.storeId,
                                          'discountRecord': viewModel.discountRecord[index]
                                        });
                                        if (result == true) {
                                          viewModel.getDiscount();
                                        }
                                      },
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.switched[index],
                                      activeColor: Colors.red,
                                      onChanged: (value) {
                                        viewModel.setSwitch(index, value, item.id ?? 0);
                                      },
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        // TixNavigate.instance.navigateTo(ManageCodeScreen(),data: {'storeId':viewModel.storeId,'discountId':item.id}).then((value){
                                        //   viewModel.getDiscount();
                                        // });
                                        WrapNavigation.instance.pushNamed(
                                            context, ManageCodeScreen(), arguments: {
                                          'storeId': viewModel.storeId,
                                          'discountId': item.id
                                        }).then((value) {
                                          viewModel.getDiscount();
                                        });
                                      },
                                      child: Container(
                                        width: 61,
                                        height: 23,
                                        decoration: new BoxDecoration(
                                          color: Color(0xffffffff),
                                          border: Border.all(color: Colors.black),
                                          borderRadius: BorderRadius.circular(4),
                                        ),
                                        child: Center(
                                          child: new Text(
                                            "จัดการโค้ด",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 8.0,
                                    ),
                                    Text(
                                      "วันหมดอายุ ${showTime} น. ",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet',
                                        color: Color(0xff6d6d6d),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    : Container(
                        child: Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50.0,
                              ),
                              Icon(
                                Icons.turned_in_not,
                                size: 30.0,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "ยังไม่มีโค้ดส่วนลด",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Colors.grey,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              ),
            ),
          );
        },
        model: viewModel);
  }
}
