import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ConfirmDialog extends StatelessWidget with TixRoute {
  String? title;
  String? description;

  ConfirmDialog({Key? key, this.title, this.description}) : super(key: key);

  @override
  String buildPath() {
    return '/confirm_dialog';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => ConfirmDialog(
              title: data['title'],
              description: data['description'],
            ));
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Dialog(
      insetPadding: EdgeInsets.only(left: 16.0, right: 16.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: Container(
        padding: EdgeInsets.only(top: 20.0, bottom: 21.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              child: new Text(
                "${title}",
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  color: Color(0xff000000),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.4000000059604645,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 35.0),
              child: new Text(
                "${description}",
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  color: Color(0xff000000),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    //TixNavigate.instance.pop(data: false);
                    WrapNavigation.instance.pop(context, data: false);
                  },
                  child: Container(
                    width: 137,
                    height: 43,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.red),
                    ),
                    child: Center(
                      child: Text(
                        'ยกเลิก',
                        style: TextStyle(
                          fontFamily: 'sukhumvitSet-Text',
                          color: Colors.red,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 21.0,
                ),
                InkWell(
                  onTap: () {
                    //TixNavigate.instance.pop(data: true);
                    WrapNavigation.instance.pop(context, data: true);
                  },
                  child: Container(
                    width: 137,
                    height: 43,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.red,
                      border: Border.all(color: Colors.red),
                    ),
                    child: Center(
                      child: Text(
                        'ยืนยัน',
                        style: TextStyle(
                          fontFamily: 'sukhumvitSet-Text',
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
