import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/create_discount_code_module/confirm_dialog.dart';
import 'package:app/module/create_discount_code_module/generate_code_dialog.dart';
import 'package:app/module/create_discount_code_module/manage_code_viewmodel.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ManageCodeScreen extends StatefulWidget with TixRoute {
  String? storeId;
  int? discountId;

  ManageCodeScreen({Key? key, this.storeId, this.discountId}) : super (key: key);

  @override
  _ManageCodeScreenState createState() => _ManageCodeScreenState();

  @override
  String buildPath() {
    return '/manage_code';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder)=>ManageCodeScreen(storeId: data['storeId'], discountId: data['discountId'],));
  }
}

class _ManageCodeScreenState extends BaseStateProvider<ManageCodeScreen,ManageCodeViewModel> {

  @override
  void initState() {
    super.initState();
    viewModel = ManageCodeViewModel(widget.storeId, widget.discountId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ManageCodeViewModel>(builder: (context,model,child){
      return ProgressHUD(
        opacity: 1.0,
        color: Colors.white,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          appBar: AppBar(
            titleSpacing: 0.0,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.red),
            title: new Text("จัดการโค้ด",
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff4a4a4a),
                fontSize: 18,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            ),
            actions: [
              Center(
                child: InkWell(
                  onTap: ()async{
                    final result = await  showDialog(context: context, builder: (_)=> GenerateCode());
                    print(result);
                    viewModel.generateCode(result);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 16.0),
                    child: new Text("สร้าง",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff4a4a4a),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          body: RefreshIndicator(
            onRefresh: (){
              viewModel.codeRecord.clear();
              viewModel.getCode();
              return Future.value();
            },
            child: viewModel.codeRecord.length != 0 ? Container(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: viewModel.codeRecord.length,
                itemBuilder: (context,index){
                  var item = viewModel.codeRecord[index];
                  return Container(
                    margin: EdgeInsets.only(left: 16.0, right: 16.0),
                    padding: EdgeInsets.only(bottom: 15.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Colors.grey.shade300)
                        )
                    ),
                    child: Column(
                      children: [
                        index == 0 ? Container(
                          padding: EdgeInsets.only(top: 18.0,bottom: 6.0),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey.shade300)
                              )
                          ),
                          child: Row(
                            children: [
                              new Text("โค้ด",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              Spacer(),
                              new Text("การเปิดใช้งาน",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                        ) : SizedBox(),
                        Row(
                          children: [
                            Text(item.code ?? '',
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            Spacer(),
                            Switch(
                              value: viewModel.switched[index],
                              activeColor: Colors.red,
                              onChanged: (value){
                                viewModel.setStatus(item.id ?? 0, index, value);
                              },),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                                text: new TextSpan(
                                    children: [
                                      new TextSpan(
                                          text: "ใช้แล้ว:",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )
                                      ),
                                      new TextSpan(
                                          text: " ${item.used} ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )
                                      ),
                                      new TextSpan(
                                          text: "ครั้ง | เป็นเงิน",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )
                                      ),
                                      new TextSpan(
                                          text: " ฿${item.sumDiscount?.toDouble().toStringAsFixed(2)}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )
                                      ),
                                    ]
                                )
                            ),
                            InkWell(
                              onTap: ()async{
                                final result = await showDialog(context: context, builder: (BuildContext context){
                                  return ConfirmDialog(title: 'ลบโค้ดส่วนลด',description: 'คุณต้องการที่จะลบโค้ดส่วนลดนี้หรือไม่',);
                                });
                                if(result == true) viewModel.deleteCode(item.id ?? 0);
                              },
                              child: Row(
                                children: [
                                  SizedBox(width: 9.0,),
                                  Icon(Icons.delete_outline, color: Colors.grey,size: 16.0,),
                                  new Text("ลบโค้ดนี้",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  );
                },
              ),
            ) : Container(
              child: Center(
                child: Column(
                  children: [
                    SizedBox(height: 100.0,),
                    Icon(Icons.turned_in_not,color: Colors.grey.shade300,size: 50.0,),
                    Text('ยังไม่มีโค้ดส่วนลด',
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff6d6d6d),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),


          ),


        ),
      );
    }, model: viewModel);
  }
}
