import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class GenerateCode extends StatelessWidget with TixRoute {
  TextEditingController codes = TextEditingController();

  @override
  String buildPath() {
    return '/generate_code';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => GenerateCode());
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(left: 16.0, right: 16.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: Container(
        padding: EdgeInsets.only(bottom: 21.0, left: 24.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  //TixNavigate.instance.pop();
                  WrapNavigation.instance.pop(context);
                },
                icon: Icon(Icons.close),
              ),
            ),
            Container(
              child: new Text(
                "สร้างโค้ด",
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  color: Color(0xff000000),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.4000000059604645,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 16.0),
              child: new Text(
                "โค้ดส่วนลดต้องเป็นอักษรอังกฤษหรือตัวเลข",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff6d6d6d),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 24.0, bottom: 16.0),
              child: Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: TextFormField(
                  maxLength: 10,
                  controller: codes,
                  keyboardType: TextInputType.text,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]|[0-9]')),
                  ],
                  validator: (value) {
                    if (value!.length < 4) {
                      return 'ต้องมากกว่า 4 ตัวอักษร';
                    }
                    return null;
                  },
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff6d6d6d),
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6.0),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6.0),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    //TixNavigate.instance.pop();
                    WrapNavigation.instance.pop(context);
                  },
                  child: Center(
                    child: Text(
                      'ยกเลิก',
                      style: TextStyle(
                        fontFamily: 'sukhumvitSet-Text',
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 21.0,
                ),
                InkWell(
                  onTap: () {
                    if (codes.text.isNotEmpty) {
                      //TixNavigate.instance.pop(data: codes.text);
                      WrapNavigation.instance.pop(context, data: codes.text);
                    }
                  },
                  child: Center(
                    child: Text(
                      'ยืนยัน',
                      style: TextStyle(
                        fontFamily: 'sukhumvitSet-Text',
                        color: Colors.red,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 24.0,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
