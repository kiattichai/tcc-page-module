import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_discount_entity.dart';
import 'package:app/model/response_result_entity.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class DiscountScreenViewModel extends BaseViewModel {
  String? storeId;
  GetDiscountData? getDiscountData;
  List<GetDiscountDataRecord> discountRecord = [];
  List<bool> switched = [];
  ResponseResultData? resultData;
  DiscountScreenViewModel(this.storeId);

  @override
  void postInit() {
    super.postInit();
    getDiscount();
  }

  void getDiscount() {
    discountRecord.clear();
    switched.clear();
    catchError(()async{
      setLoading(true);
      getDiscountData = await di.concertRepository.getDiscount(storeId!);
      setLoading(false);
      if(getDiscountData != null){
        discountRecord.addAll(getDiscountData?.record ?? []);
        for(int i=0; i<discountRecord.length;i++){
          switched.add(discountRecord[i].status ?? false);
        }
        notifyListeners();
      }
    });
  }
  void setSwitch(int index, bool value, int discountId) {
    switched[index] = value;
    catchError(()async{
      resultData = await di.concertRepository.setStatus(discountId, value, storeId!);
    });
    notifyListeners();
  }
  
  @override
  void onError(error) {
    super.onError(error);
  }


}

