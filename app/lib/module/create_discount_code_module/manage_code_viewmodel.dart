import 'package:app/core/base_view_model.dart';
import 'package:app/model/generate_code_entity.dart';
import 'package:app/model/get_code_entity.dart';
import 'package:app/model/response_result_entity.dart';

class ManageCodeViewModel extends BaseViewModel {
  String? storeId;
  int? discountId;
  GetCodeData? getCodeData;
  List<GetCodeDataRecord> codeRecord = [];
  List<bool> switched = [];
  ResponseResultData? resultData;
  GenerateCodeData? generateCodeData;


  ManageCodeViewModel(this.storeId, this.discountId);

  @override
  void postInit() {
    super.postInit();
    getCode();
  }

  void getCode() {
    codeRecord.clear();
    catchError(()async{
      setLoading(true);
      getCodeData = await di.concertRepository.getCode(discountId!);
      setLoading(false);
      if(getCodeData != null){
        codeRecord.addAll(getCodeData?.record ?? []);
        for(int i=0;i<codeRecord.length;i++){
          switched.add(codeRecord[i].status ?? false);
        }
      }
      notifyListeners();
    });
  }

  @override
  void onError(error) {
    // TODO: implement onError
    super.onError(error);
  }

  void setStatus(int codeId, int index, bool value) {
    switched[index] = value;
    catchError(()async{
      resultData = await di.concertRepository.setStatusCode(discountId!, codeId, value);
    });
    notifyListeners();
  }

  void deleteCode(int codeId){
    catchError(()async{
      setLoading(true);
      resultData = await di.concertRepository.deleteCode(discountId!, codeId);
      if(resultData != null){
        getCode();
      }
      setLoading(false);
    });
    notifyListeners();
  }

  void generateCode(String code) {
    List item = [{
      "uuid": "",
      "code": "${code}",
      "status": "true",
      "used": 0
    }];
    catchError(()async{
      generateCodeData = await di.concertRepository.generateCode(discountId!, item);
      if(generateCodeData != null){
        getCode();
      }
    });
    
  }
}