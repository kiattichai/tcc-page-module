import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_discount_entity.dart';
import 'package:app/model/get_ticket_entity.dart';
import 'package:app/module/create_discount_code_module/confirm_dialog.dart';
import 'package:app/module/create_discount_code_module/create_discount_viewmodel.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/custom_date_picker.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateDiscountScreen extends StatefulWidget with TixRoute {
  int? concertId;
  String? storeId;
  GetDiscountDataRecord? discountDetail;

  CreateDiscountScreen(
      {Key? key, this.concertId, this.storeId, this.discountDetail})
      : super(key: key);

  @override
  _CreateDiscountScreenState createState() => _CreateDiscountScreenState();

  @override
  String buildPath() {
    return '/create_discount';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => CreateDiscountScreen(
              concertId: data['concertId'],
              storeId: data['storeId'],
              discountDetail: data['discountRecord'],
            ));
  }
}

class _CreateDiscountScreenState
    extends BaseStateProvider<CreateDiscountScreen, CreateDiscountViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = CreateDiscountViewModel(
        widget.concertId, widget.storeId, widget.discountDetail);
  }

  void _showDatePicker(DateTime time, bool isStart) {
    DatePicker.showPicker(context,
        showTitleActions: true,
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ), onConfirm: (date) {
      if (isStart) {
        viewModel.startDateTime = DateTime(date.year, date.month, date.day,
            viewModel.startDateTime.hour, viewModel.startDateTime.minute);
      } else {
        viewModel.endDateTime = DateTime(date.year, date.month, date.day,
            viewModel.endDateTime.hour, viewModel.endDateTime.minute);
      }
      viewModel.notifyListeners();
      formatDate();
      viewModel.checkValidate();
      //check();
    },
        pickerModel: CustomDatePicker(
            minTime: DateTime.now(),
            currentTime: time,
            locale: LocaleType.th,
            maxTime: viewModel.maxDateTime),
        locale: LocaleType.th);
  }

  void _showTimePicker(DateTime time, bool isStart) {
    DatePicker.showTimePicker(context,
        showTitleActions: true,
        showSecondsColumn: false,
        currentTime: time, onConfirm: (date) {
      if (isStart) {
        viewModel.startDateTime = DateTime(
            viewModel.startDateTime.year,
            viewModel.startDateTime.month,
            viewModel.startDateTime.day,
            date.hour,
            date.minute);
      } else {
        viewModel.endDateTime = DateTime(
            viewModel.endDateTime.year,
            viewModel.endDateTime.month,
            viewModel.startDateTime.day,
            date.hour,
            date.minute);
      }
      viewModel.notifyListeners();
      formatDate();
      viewModel.checkValidate();
    },
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ),
        locale: LocaleType.th);
  }

  void formatDate() {
    viewModel.startDate.text =
        DateFormat('dd/MM/yy').format(viewModel.startDateTime);
    viewModel.startTime.text =
        DateFormat('HH:mm', 'th').format(viewModel.startDateTime);
    viewModel.endDate.text =
        DateFormat('dd/MM/yy').format(viewModel.endDateTime);
    viewModel.endTime.text =
        DateFormat('HH:mm', 'th').format(viewModel.endDateTime);
    viewModel.notifyListeners();
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Colors.black,
      fontSize: fontSize,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  labelStyle(){
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  hintStyle(){
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateDiscountViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBar(
                titleSpacing: 0.0,
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.red),
                title: Text(
                  "สร้างโค้ดส่วนลด",
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff4a4a4a),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //TODO ชื่อส่วนลด
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, top: 16.0, right: 16.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.title,
                          keyboardType: TextInputType.text,
                          inputFormatters: <TextInputFormatter>[],
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนชื่อโค้ดส่วนลด';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            hintText: 'ชื่อส่วนลด',
                            labelText: 'ชื่อส่วนลด',
                            contentPadding: EdgeInsets.all(15.0),
                            labelStyle: labelStyle(),
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //TODO วันที่เริ่ม
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: TextFormField(
                        onChanged: (text) {
                          viewModel.checkValidate();
                        },
                        autovalidateMode: AutovalidateMode.always,
                        autofocus: false,
                        controller: viewModel.startDate,
                        maxLength: 200,
                        style: textStyle(),
                        readOnly: true,
                        validator: null,
                        onTap: () =>
                            _showDatePicker(viewModel.startDateTime, true),
                        decoration: InputDecoration(
                          labelText: 'เริ่มเมื่อ',
                          contentPadding: EdgeInsets.all(15.0),
                          suffixIcon: Icon(
                            Icons.calendar_today,
                            size: 20,
                            color: Colors.grey,
                          ),
                          isDense: true,
                          labelStyle: labelStyle(),
                          border: OutlineInputBorder(),
                          counterText: '',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    //TODO เวลาเริ่มต้น
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: TextFormField(
                        onChanged: (text) {
                          viewModel.checkValidate();
                        },
                        autovalidateMode: AutovalidateMode.always,
                        autofocus: false,
                        controller: viewModel.startTime,
                        maxLength: 200,
                        style: textStyle(),
                        readOnly: true,
                        validator: null,
                        onTap: () =>
                            _showTimePicker(viewModel.startDateTime, true),
                        decoration: InputDecoration(
                          labelText: 'เวลาเริ่มต้น',
                          contentPadding: EdgeInsets.all(15.0),
                          suffixIcon: Icon(
                            Icons.timer,
                            color: Colors.grey,
                            size: 20,
                          ),
                          isDense: true,
                          labelStyle: labelStyle(),
                          border: OutlineInputBorder(),
                          counterText: '',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    //TODO วันที่สิ้นสุด
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: TextFormField(
                        onChanged: (text) {
                          viewModel.checkValidate();
                        },
                        autovalidateMode: AutovalidateMode.always,
                        autofocus: false,
                        controller: viewModel.endDate,
                        maxLength: 200,
                        style: textStyle(),
                        readOnly: true,
                        validator: (value) {},
                        onTap: () =>
                            _showDatePicker(viewModel.endDateTime, false),
                        decoration: InputDecoration(
                          labelText: 'สิ้นสุดเมื่อ',
                          contentPadding: EdgeInsets.all(15.0),
                          suffixIcon: Icon(
                            Icons.calendar_today,
                            color: Colors.grey,
                            size: 20,
                          ),
                          isDense: true,
                          labelStyle: labelStyle(),
                          border: OutlineInputBorder(),
                          counterText: '',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    //TODO เวลาสิ้นสุด
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: TextFormField(
                        onChanged: (text) {
                          viewModel.checkValidate();
                        },
                        autovalidateMode: AutovalidateMode.always,
                        autofocus: false,
                        controller: viewModel.endTime,
                        maxLength: 200,
                        style: textStyle(),
                        readOnly: true,
                        validator: (value) {
                          if (viewModel.checkDate()) {
                            return null;
                          } else {
                            return 'กรุณาป้อนข้อมูลให้ถูกต้อง';
                          }
                        },
                        onTap: () {
                          _showTimePicker(viewModel.endDateTime, false);
                        },
                        decoration: InputDecoration(
                          labelText: 'เวลาสิ้นสุด',
                          contentPadding: EdgeInsets.all(15.0),
                          suffixIcon: Icon(
                            Icons.timer,
                            color: Colors.grey,
                            size: 20,
                          ),
                          isDense: true,
                          labelStyle: labelStyle(),
                          border: OutlineInputBorder(),
                          counterText: '',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),

                    //TODO เงื่อนไขโปรโมชัน
                    Container(
                      margin: EdgeInsets.only(left: 16.0),
                      child: new Text(
                        "เงื่อนไขโปรโมชัน",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet',
                          color: Color(0xff08080a),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.0),
                      child: new Text("ประเภทส่วนลด",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                    ),

                    //TODO ลดเป็นเปอร์เซ็นต์
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 0.0),
                      horizontalTitleGap: 0.0,
                      title: new Text("ลดเป็นเปอร์เซ็นต์",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                      leading: Radio(
                        value: 1,
                        groupValue: viewModel.discountType,
                        activeColor: Colors.red,
                        onChanged: (value) {
                          int val = int.parse(value.toString());
                          print(val);
                          viewModel.setDiscountType(val);
                          viewModel.checkValidate();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.persenPurchased,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          enabled: viewModel.discountType == 1 ? true : false,
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนข้อมูล';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'ซื้อครบ(บาท)',
                            labelText: 'ซื้อครบ(บาท)',
                            labelStyle: labelStyle(),
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.persenDiscount,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          enabled: viewModel.discountType == 1 ? true : false,
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนข้อมูล';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'ลดเพิ่ม(%)',
                            labelText: 'ลดเพิ่ม(%)',
                            labelStyle: labelStyle(),
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),

                    //TODO ลดเป็นบาท
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 0.0),
                      horizontalTitleGap: 0.0,
                      title: new Text("ลดเป็นบาท",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                      leading: Radio(
                        value: 2,
                        groupValue: viewModel.discountType,
                        activeColor: Colors.red,
                        onChanged: (value) {
                          int val = int.parse(value.toString());
                          print(val);
                          viewModel.setDiscountType(val);
                          viewModel.checkValidate();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.bathPurchased,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          enabled: viewModel.discountType == 2 ? true : false,
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนข้อมูล';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'ซื้อครบ(บาท)',
                            labelText: 'ซื้อครบ(บาท)',
                            labelStyle: labelStyle(),
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.bathDiscount,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          enabled: viewModel.discountType == 2 ? true : false,
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนข้อมูล';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'ลดเพิ่ม(บาท)',
                            labelText: 'ลดเพิ่ม(บาท)',
                            labelStyle: labelStyle(),
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),

                    //TODO รับส่วนลดสูงสุด
                    Container(
                      margin: EdgeInsets.only(left: 16.0),
                      child: new Text(
                        "รับส่วนลดสูงสุดไม่เกิน (บาท)",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff08080a),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.0, right: 16.0, top: 10.0, bottom: 16.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          controller: viewModel.discountLimit,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          validator: (value) {
                            if (value!.isNotEmpty) {
                              return null;
                            } else {
                              return 'กรุณาป้อนข้อมูล';
                            }
                          },
                          onChanged: (value) {
                            viewModel.checkValidate();
                          },
                          style: TextStyle(),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            suffixText: '฿',
                            suffixStyle: TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                            ),
                            hintText: 'ระบุจำนวนส่วนลดสูงสุด',
                            hintStyle: hintStyle(),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),

                    //TODO ประเภทส่วนลด
                    Container(
                      margin: EdgeInsets.only(left: 16.0),
                      child: new Text("ประเภทของโค้ด",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.0, right: 16.0),
                      padding: EdgeInsets.only(bottom: 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(7.0)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Radio(
                                  value: 1,
                                  groupValue: viewModel.codeType,
                                  activeColor: Colors.red,
                                  onChanged: (value) {
                                    int val = int.parse(value.toString());
                                    viewModel.setCodeType(val);
                                  }),
                              Text(
                                "ใช้หลายครั้ง",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: 2,
                                  groupValue: viewModel.codeType,
                                  activeColor: Colors.red,
                                  onChanged: (value) {
                                    int val = int.parse(value.toString());
                                    viewModel.setCodeType(val);
                                  }),
                              Text(
                                "ใช้ครั้งเดียว",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),

                          //TODO จำนวนครั้งที่สามารถใช้ได้
                          viewModel.codeType == 1
                              ? Container(
                                  margin: EdgeInsets.only(left: 16.0),
                                  child: new Text("จำนวนครั้งที่สามารถใช้ได้",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff08080a),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                )
                              : SizedBox(),
                          viewModel.codeType == 1
                              ? Row(
                                  children: [
                                    Radio(
                                        value: 1,
                                        groupValue: viewModel.numberOfUse,
                                        activeColor: Colors.red,
                                        onChanged: (value) {
                                          int val = int.parse(value.toString());
                                          viewModel.setNumberOfUse(val);
                                        }),
                                    Text(
                                      "ไม่จำกัด",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff6d6d6d),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox(),
                          viewModel.codeType == 1
                              ? Row(
                                  children: [
                                    Radio(
                                        value: 2,
                                        groupValue: viewModel.numberOfUse,
                                        activeColor: Colors.red,
                                        onChanged: (value) {
                                          int val = int.parse(value.toString());
                                          viewModel.setNumberOfUse(val);
                                        }),
                                    Text(
                                      "จำกัดจำนวน",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff6d6d6d),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    Container(
                                      width: 60.0,
                                      margin: EdgeInsets.only(
                                          left: 16.0, right: 16.0),
                                      child: Form(
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction,
                                        child: TextFormField(
                                          controller: viewModel.limitTotal,
                                          keyboardType: TextInputType.number,
                                          enabled: viewModel.numberOfUse == 1
                                              ? false
                                              : true,
                                          textAlign: TextAlign.center,
                                          inputFormatters: <TextInputFormatter>[
                                            FilteringTextInputFormatter
                                                .digitsOnly,
                                          ],
                                          validator: (value) {
                                            if (value!.isNotEmpty) {
                                              return null;
                                            } else {
                                              return '';
                                            }
                                          },
                                          onChanged: (value) {
                                            viewModel.checkValidate();
                                          },
                                          style: TextStyle(),
                                          decoration: InputDecoration(
                                            isDense: true,
                                            contentPadding: EdgeInsets.all(5.0),
                                            filled: true,
                                            fillColor:
                                                viewModel.numberOfUse == 1
                                                    ? Colors.grey.shade300
                                                    : Colors.white,
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(6.0),
                                              borderSide: BorderSide(
                                                color: Colors.grey,
                                              ),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(6.0),
                                              borderSide: BorderSide(
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "ครั้ง",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff6d6d6d),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox(),

                          //TODO สามารถใช้ได้กี่ครั้งต่อ 1 ยูเซอร์
                          Container(
                            margin: EdgeInsets.only(left: 16.0, top: 16.0),
                            child: new Text("สามารถใช้ได้กี่ครั้งต่อ 1 ยูเซอร์",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: 1,
                                  groupValue: viewModel.numberOfUser,
                                  activeColor: Colors.red,
                                  onChanged: (value) {
                                    int val = int.parse(value.toString());
                                    viewModel.setNumberOfUser(val);
                                  }),
                              Text(
                                "ไม่จำกัด",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: 2,
                                  groupValue: viewModel.numberOfUser,
                                  activeColor: Colors.red,
                                  onChanged: (value) {
                                    int val = int.parse(value.toString());
                                    viewModel.setNumberOfUser(val);
                                  }),
                              Text(
                                "จำกัดจำนวน",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              Container(
                                width: 60.0,
                                margin:
                                    EdgeInsets.only(left: 16.0, right: 16.0),
                                child: Form(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  child: TextFormField(
                                    controller: viewModel.userLimitTotal,
                                    keyboardType: TextInputType.number,
                                    enabled: viewModel.numberOfUser == 1
                                        ? false
                                        : true,
                                    textAlign: TextAlign.center,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    validator: (value) {
                                      if (value!.isNotEmpty) {
                                        return null;
                                      } else {
                                        return '';
                                      }
                                    },
                                    onChanged: (value) {
                                      viewModel.checkValidate();
                                    },
                                    style: TextStyle(),
                                    decoration: InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.all(5.0),
                                      filled: true,
                                      fillColor: viewModel.numberOfUser == 1
                                          ? Colors.grey.shade300
                                          : Colors.white,
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                "ครั้ง",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    //TODO เลือกบัตร
                    Container(
                      margin: EdgeInsets.only(left: 16.0, top: 24.0),
                      child: new Text(
                        "เลือกบัตรที่สามารถใช้โค้ดนี้ได้",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet',
                          color: Color(0xff08080a),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    viewModel.getTicketRecord.length == 0
                        ? Container(
                            margin: EdgeInsets.only(
                                left: 16.0,
                                right: 16.0,
                                top: 10.0,
                                bottom: 16.0),
                            decoration: BoxDecoration(
                              color: Color(0xfffff3e0),
                              border: Border.all(color: Color(0xfffff3e0)),
                              borderRadius: BorderRadius.circular(7.0),
                            ),
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.all(16.0),
                                  child: Icon(
                                    Icons.turned_in_not,
                                    color: Color(0xffffab00),
                                    size: 50.0,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Tickets not found.',
                                      style: TextStyle(
                                          color: Color(0xffffab00),
                                          fontFamily: 'sukhumvitSet-Text',
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text.rich(
                                      TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: 'Please',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                        TextSpan(
                                          text: ' create tickets',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              print('สร้างบัตร');
                                            },
                                        ),
                                        TextSpan(
                                          text: ' before',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                      ]),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )
                        : Container(
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: viewModel.getTicketRecord.length,
                              itemBuilder: (context, index) {
                                var item = viewModel.getTicketRecord[index];
                                return ListTile(
                                    horizontalTitleGap: 0.0,
                                    leading: Checkbox(
                                        checkColor: Colors.white,
                                        activeColor: Colors.red,
                                        value: viewModel.selectTicket[index],
                                        onChanged: (value) {
                                          print(value);
                                          viewModel.setTicket(index, value!);
                                          viewModel.checkValidate();
                                        }),
                                    title: Row(
                                      children: [
                                        Text(
                                          '${item.name}' + ' | ',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                        Text(item.priceText.toString())
                                      ],
                                    ));
                              },
                            ),
                          ),

                    //TODO ลบ
                    viewModel.discountDetail != null
                        ? InkWell(
                            onTap: () async {
                              final result = await showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return ConfirmDialog(
                                      title: 'ลบส่วนลด',
                                      description:
                                          'คุณต้องการที่จะลบส่วนลดนี้หรือไม่',
                                    );
                                  });
                              if (result == true) viewModel.deleteDiscount(context);
                            },
                            child: Container(
                              height: 42,
                              margin: EdgeInsets.all(16.0),
                              decoration: new BoxDecoration(
                                  color: Color(0xffe6e7ec),
                                  borderRadius: BorderRadius.circular(21)),
                              child: Center(
                                child: new Text(
                                  "ลบ",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Colors.red,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),

                    //TODO บันทึก
                    viewModel.isActive == true
                        ? InkWell(
                            onTap: () {
                              print('make code');
                              viewModel.saveDiscount(context);
                            },
                            child: Container(
                              height: 42,
                              margin: EdgeInsets.only(
                                  left: 16.0, right: 16.0, bottom: 16.0),
                              decoration: new BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(21)),
                              child: Center(
                                child: new Text(
                                  "บันทึก",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(
                            height: 42,
                            margin: EdgeInsets.only(
                                left: 16.0, right: 16.0, bottom: 16.0),
                            decoration: new BoxDecoration(
                                color: Color(0xffe6e7ec),
                                borderRadius: BorderRadius.circular(21)),
                            child: Center(
                              child: new Text(
                                "บันทึก",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xffb2b2b2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          );
        },
        model: viewModel);
  }

  check() {
    viewModel.isActive =
        viewModel.startDateTime.isBefore(viewModel.endDateTime);
  }
}
