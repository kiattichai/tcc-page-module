import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_discount_entity.dart';
import 'package:app/model/get_ticket_entity.dart';
import 'package:app/model/get_ticket_select_entity.dart';
import 'package:app/model/response_link_id_entity.dart';
import 'package:app/model/response_result_entity.dart';
import 'package:app/model/response_save_discount_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateDiscountViewModel extends BaseViewModel {
  int discountType = 1;
  int codeType = 1;
  int numberOfUse = 1;
  int numberOfUser = 1;
  bool isActive = false;
  int? concertId;
  String? storeId;
  GetTicketData? getTicketData;
  GetDiscountDataRecord? discountDetail;
  List<GetTicketDataRecord> getTicketRecord = [];
  GetTicketSelectData? getTicketSelect;
  List<GetTicketSelectDataRecord> getTicketSelectRecord = [];
  List<bool> selectTicket = [];
  TextEditingController title = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController endTime = TextEditingController();
  TextEditingController valueStart = TextEditingController();
  TextEditingController valueEnd = TextEditingController();
  TextEditingController discount = TextEditingController();
  TextEditingController discountLimit = TextEditingController();
  TextEditingController limitTotal = TextEditingController();
  TextEditingController userLimitTotal = TextEditingController();
  TextEditingController persenPurchased = TextEditingController();
  TextEditingController persenDiscount = TextEditingController();
  TextEditingController bathPurchased = TextEditingController();
  TextEditingController bathDiscount = TextEditingController();

  ResponseSaveDiscountData? saveDiscountData;
  ResponseResultData? responseData;
  ResponseLinkIdData? linkIdData;

  CreateDiscountViewModel(this.concertId, this.storeId, this.discountDetail);

  DateTime startDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10);
  DateTime endDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 10);

  DateTime maxDateTime =
      DateTime(DateTime.now().year + 5, DateTime.now().month, DateTime.now().day + 1, 10);

  @override
  void postInit() {
    super.postInit();
    limitTotal.text = "1";
    userLimitTotal.text = "1";
    if (discountDetail != null) {
      initText();
      getTicketSelected();
    } else {
      getTicket();
    }
  }

  void checkValidate() {
    if (title.text.isNotEmpty &&
        startDate.text.isNotEmpty &&
        startTime.text.isNotEmpty &&
        endDate.text.isNotEmpty &&
        endTime.text.isNotEmpty &&
        checkDate()) {
      if (discountType == 1) {
        if (persenPurchased.text.isNotEmpty && persenDiscount.text.isNotEmpty) {
          isActive = true;
        } else {
          isActive = false;
        }
      } else {
        if (bathPurchased.text.isNotEmpty && bathDiscount.text.isNotEmpty) {
          isActive = true;
        } else {
          isActive = false;
        }
      }

      if (numberOfUse != numberOfUser) {
        if (numberOfUse == 2 && limitTotal.text.isNotEmpty ||
            numberOfUser == 2 && userLimitTotal.text.isNotEmpty) {
          isActive = true;
        } else {
          isActive = false;
        }
      }
      if (numberOfUse == numberOfUser) {
        if (numberOfUse == 2 &&
            limitTotal.text.isNotEmpty &&
            numberOfUser == 2 &&
            userLimitTotal.text.isNotEmpty) {
          print(limitTotal.text);
          print(userLimitTotal.text);
          isActive = true;
        } else if (numberOfUse == 1 && numberOfUser == 1) {
          isActive = true;
        } else {
          isActive = false;
        }
      }

      List<bool> ticket = selectTicket.where((element) {
        return element == true;
      }).toList();
      if (ticket.length != 0) {
        isActive = true;
      } else {
        isActive = false;
      }
    } else {
      isActive = false;
    }
    notifyListeners();
  }

  void saveDiscount(BuildContext context) {
    String startDate = DateFormat('yyyy-MM-dd ').format(startDateTime) +
        DateFormat('HH:mm:mm', 'th').format(startDateTime);
    String endDate = DateFormat('yyyy-MM-dd ').format(endDateTime) +
        DateFormat('HH:mm:mm', 'th').format(endDateTime);
    Map params = {
      'store_id': "${storeId}",
      'promotion_type': "promocode",
      'title': "${title.text}",
      'start_date': "${startDate}",
      'end_date': "${endDate}",
      'discount_type': discountType == 1 ? "percentage" : "amount",
      'rates': [
        if (discountType == 1)
          {
            'value_start': "${persenPurchased.text}",
            'value_end': "${persenPurchased.text}",
            'discount': "${persenDiscount.text}"
          }
        else
          {
            'value_start': "${bathPurchased.text}",
            'value_end': "${bathPurchased.text}",
            'discount': "${bathDiscount.text}"
          }
      ],
      'discount_limit': discountLimit.text,
      'used_type': codeType == 1 ? "multiple" : "unique",
      'limit_type': numberOfUse == 1 ? false : true,
      'limit_total': limitTotal.text,
      'user_limit_type': numberOfUser == 1 ? false : true,
      'user_limit_total': userLimitTotal.text,
      'status': false
    };

    //TODO ถ้าข้อมูลว่าง
    if (discountDetail == null) {
      catchError(() async {
        setLoading(true);
        saveDiscountData = await di.concertRepository.saveDiscount(params);
        setLoading(false);
        if (saveDiscountData != null) {
          int discountId = saveDiscountData?.id ?? 0;
          List items = [];
          for (int i = 0; i < selectTicket.length; i++) {
            if (selectTicket[i] == true) {
              items.add({"id": concertId, "variant_id": getTicketRecord[i].id, "group_id": 1});
            }
          }
          catchError(() async {
            setLoading(true);
            linkIdData = await di.concertRepository.linkId(discountId, items);
            setLoading(false);
            if (linkIdData != null) {
              //TixNavigate.instance.pop(data: discountId);
              WrapNavigation.instance.pop(context, data: discountId);
            }
          });
        }
      });
    } else {
      catchError(() async {
        setLoading(true);
        responseData = await di.concertRepository.updateDiscount(discountDetail!.id!, params);
        setLoading(false);
        if (responseData != null) {
          List items = [];
          for (int i = 0; i < selectTicket.length; i++) {
            if (selectTicket[i] == true) {
              items.add({"id": concertId, "variant_id": getTicketRecord[i].id, "group_id": 1});
            }
          }
          catchError(() async {
            setLoading(true);
            linkIdData = await di.concertRepository.linkId(discountDetail!.id!, items);
            setLoading(false);
            if (linkIdData != null) {
              //TixNavigate.instance.pop(data: true);
              WrapNavigation.instance.pop(context, data: true);
            }
          });
        }
      });
    }
  }

  void deleteDiscount(BuildContext context) {
    catchError(() async {
      setLoading(true);
      responseData = await di.concertRepository.deleteDiscount(discountDetail!.id!, storeId!);
      setLoading(false);
      if (responseData != null) {
        //TixNavigate.instance.pop(data: true);
        WrapNavigation.instance.pop(context, data: true);
      }
    });
  }

  void getTicket() {
    selectTicket.clear();
    catchError(() async {
      setLoading(true);
      getTicketData = await di.concertRepository.getTicket(concertId!);
      setLoading(false);
      if (getTicketData != null) {
        getTicketRecord.addAll(getTicketData?.record ?? []);
        for (int i = 0; i < getTicketRecord.length; i++) {
          selectTicket.add(false);
        }
        for (int i = 0; i < getTicketRecord.length; i++) {
          int ticketId = getTicketRecord[i].id!;
          int index = i;
          for (int i = 0; i < getTicketSelectRecord.length; i++) {
            if (ticketId == getTicketSelectRecord[i].variant?.id) {
              selectTicket[index] = true;
              //break;
            }
          }
        }
      }
      notifyListeners();
    });
  }

  void getTicketSelected() async {
    getTicketSelectRecord.clear();
    catchError(() async {
      getTicketSelect = await di.concertRepository.getTicketSelected(discountDetail!.id!);
      if (getTicketSelect != null) {
        getTicketSelectRecord.addAll(getTicketSelect?.record ?? []);
      }
      notifyListeners();
    });
    getTicket();
  }

  checkDate() {
    bool checkDate = startDateTime.isBefore(endDateTime);
    return checkDate;
  }

  setDiscountType(int val) {
    discountType = val;
    notifyListeners();
  }

  setCodeType(int val) {
    codeType = val;
    notifyListeners();
  }

  setNumberOfUse(int val) {
    numberOfUse = val;
    limitTotal.text = '1';
    notifyListeners();
  }

  setNumberOfUser(int val) {
    numberOfUser = val;
    userLimitTotal.text = '1';
    notifyListeners();
  }

  setTicket(int index, bool value) {
    selectTicket[index] = value;
    print(selectTicket);
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
  }

  void initText() {
    title.text = discountDetail?.title ?? '';
    startDate.text = converseDate(discountDetail?.promotionTime?.startDate ?? '');
    startTime.text = converseTime(discountDetail?.promotionTime?.startDate ?? '');
    endDate.text = converseDate(discountDetail?.promotionTime?.endDate ?? '');
    endTime.text = converseTime(discountDetail?.promotionTime?.endDate ?? '');
    discountType = discountDetail?.discountType == "percentage" ? 1 : 2;
    if (discountDetail?.discountType == "percentage") {
      persenPurchased.text = discountDetail!.rates![0].valueStart.toString();
      persenDiscount.text = discountDetail!.rates![0].discount.toString();
    } else {
      bathPurchased.text = discountDetail!.rates![0].valueStart.toString();
      bathDiscount.text = discountDetail!.rates![0].discount.toString();
    }
    discountLimit.text = discountDetail!.discountLimit.toString();
    codeType = discountDetail?.usedType == "multiple" ? 1 : 2;
    discountDetail?.limitType == false ? numberOfUse = 1 : numberOfUse = 2;
    if (discountDetail?.limitType == true) limitTotal.text = discountDetail!.limitTotal.toString();

    discountDetail?.userLimitType == false ? numberOfUser = 1 : numberOfUser = 2;
    if (discountDetail?.userLimitType == true)
      userLimitTotal.text = discountDetail!.userLimitTotal.toString();

    notifyListeners();
  }

  converseDate(String dateTime) {
    DateTime date = DateTime.parse('${dateTime}');
    String dateText = DateFormat('dd MMM yyyy ', 'th').format(date);
    return dateText;
  }

  converseTime(String dateTime) {
    DateTime time = DateTime.parse('${dateTime}');
    String timeText = DateFormat('HH:mm', 'th').format(time);
    return timeText;
  }
}
