import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/concert_agent_module/concert_agent_detail_screen_view_model.dart';
import 'package:app/module/concert_agent_module/widget/concert_agent_item.dart';
import 'package:app/module/concert_agent_module/widget/ticket_coin_item.dart';
import 'package:app/module/concert_agent_module/widget/ticket_link_dialog.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ConcertAgentDetailScreen extends StatefulWidget with TixRoute {
  final int? concertId;

  const ConcertAgentDetailScreen({Key? key, this.concertId}) : super(key: key);

  @override
  _ConcertAgentDetailScreenState createState() => _ConcertAgentDetailScreenState();

  @override
  String buildPath() {
    return '/concert_agent_detail_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => ConcertAgentDetailScreen(
              concertId: data,
            ));
  }
}

class _ConcertAgentDetailScreenState
    extends BaseStateProvider<ConcertAgentDetailScreen, ConcertAgentDetailScreenViewModel> {
  @override
  void initState() {
    viewModel = ConcertAgentDetailScreenViewModel(widget.concertId!);
    viewModel.showAlertError = showAlertError;
    viewModel.showLinkDialog = showLinkDialog;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
      onWillPop: () async {
        WrapNavigation.instance.pop(context);
        return false;
      },
      child: BaseWidget<ConcertAgentDetailScreenViewModel>(
          model: viewModel,
          builder: (context, model, child) {
            return ProgressHUD(
                opacity: 1.0,
                color: Colors.white,
                progressIndicator: CircleLoading(),
                inAsyncCall: viewModel.loading,
                child: Scaffold(
                    appBar: AgentAppBar(
                      onBack: () {
                        WrapNavigation.instance.pop(context);
                      },
                      name: 'สิทธิ์บัตรคอนเสิร์ตของตัวแทน',
                    ),
                    body: RefreshIndicator(
                        onRefresh: () {
                          viewModel.getConcertAgentDetail();
                          viewModel.getAgentSaleTerm();
                          viewModel.getAgentContentHow();
                          return Future.value();
                        },
                        child: viewModel.getConcertAgentDetailData == null
                            ? SizedBox()
                            : SingleChildScrollView(
                                physics: AlwaysScrollableScrollPhysics(),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ConcertAgentItem(
                                      imageUrl: viewModel.getConcertAgentDetailData?.record
                                              ?.concertImages?.first.url ??
                                          '',
                                      concertName:
                                          viewModel.getConcertAgentDetailData?.record?.name ?? '',
                                      address: viewModel
                                              .getConcertAgentDetailData?.record?.venue?.name ??
                                          '',
                                      time: viewModel.getConcertAgentDetailData?.record?.showTime
                                              ?.textShort ??
                                          '',
                                      agentName: viewModel
                                              .getConcertAgentDetailData?.record?.store?.name ??
                                          '',
                                      link: null,
                                      showMessageCopy: () {},
                                    ),
                                    Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.only(
                                            left: 16, right: 16, top: 10, bottom: 10),
                                        width: double.infinity,
                                        decoration: BoxDecoration(color: Color(0xfff0f0f0)),
                                        child: // บัตรที่เปิดขายได้ แล
                                            Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("ประเภทบัตรที่ขายได้ และค่าคอมมิชชันที่ได้รับ",
                                                style: const TextStyle(
                                                    color: const Color(0xff333333),
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: "Thonburi",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 16.0)),
                                            (viewModel.getConcertAgentDetailData?.record
                                                        ?.commissionableTickets?.isNotEmpty ??
                                                    false)
                                                ? Text(
                                                    "ระยะเวลา : ${viewModel.getConcertAgentDetailData?.record?.commissionableTickets?.first.textFull ?? ''}",
                                                    style: const TextStyle(
                                                        color: const Color(0xff333333),
                                                        fontWeight: FontWeight.w400,
                                                        fontFamily: "Thonburi",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 16.0))
                                                : SizedBox(),
                                          ],
                                        )),
                                    Container(
                                      margin: const EdgeInsets.only(left: 16, right: 16, top: 6),
                                      padding: const EdgeInsets.only(bottom: 16),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("รายการบัตร",
                                              style: const TextStyle(
                                                  color: const Color(0xff616161),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0)),
                                          Spacer(),
                                          Text("จำนวน coin ที่ได้รับ",
                                              style: const TextStyle(
                                                  color: const Color(0xff616161),
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0)),
                                        ],
                                      ),
                                    ),
                                    ...List.generate(
                                        (viewModel.getConcertAgentDetailData?.record
                                                    ?.commissionableTickets ??
                                                [])
                                            .length, (index) {
                                      var item = viewModel.getConcertAgentDetailData!.record!
                                          .commissionableTickets![index];
                                      return TicketCoinItem(
                                        ticket: item.name ?? '',
                                        price: item.price ?? 0,
                                        coin: item.commissionerReward ?? 0,
                                      );
                                    }),
                                    viewModel.getAgentSaleTermData != null
                                        ? Container(
                                            alignment: Alignment.centerLeft,
                                            padding: const EdgeInsets.only(left: 16),
                                            width: double.infinity,
                                            height: 39,
                                            decoration: BoxDecoration(color: Color(0xfff0f0f0)),
                                            child: // เงื่อนไขและข้อตกลง
                                                Text("เงื่อนไขและข้อตกลง",
                                                    style: const TextStyle(
                                                        color: const Color(0xff333333),
                                                        fontWeight: FontWeight.w400,
                                                        fontFamily: "Thonburi",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 16.0)),
                                          )
                                        : SizedBox(),
                                    viewModel.getAgentSaleTermData != null
                                        ? Container(
                                            alignment: Alignment.centerLeft,
                                            padding: const EdgeInsets.only(
                                                left: 16, right: 16, top: 11, bottom: 11),
                                            child: Html(
                                                data: viewModel.getAgentSaleTermData!.record!.first
                                                    .fields![1].lang),
                                          )
                                        : SizedBox(),
                                    !(viewModel.getConcertAgentDetailData?.record?.isCommissioner ??
                                            false)
                                        ? Container(
                                            child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Checkbox(
                                                splashRadius: 4,
                                                value: viewModel.isChecked,
                                                checkColor: Colors.white,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(4)),
                                                fillColor: MaterialStateProperty.all<Color>(
                                                    Color(0xFFDA3534)),
                                                onChanged: (bool? value) {
                                                  viewModel.setChecked(value);
                                                },
                                              ),
                                              Text("ยอมรับเงื่อนไขและเข้าร่วม",
                                                  style: const TextStyle(
                                                      color: const Color(0xff616161),
                                                      fontWeight: FontWeight.w400,
                                                      fontFamily: "Thonburi",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize: 16.0))
                                            ],
                                          ))
                                        : SizedBox()
                                  ],
                                ),
                              )),
                    bottomNavigationBar:
                        !(viewModel.getConcertAgentDetailData?.record?.isCommissioner ?? false)
                            ? Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0,
                                    right: 16.0,
                                    bottom: 16.0 + MediaQuery.of(context).padding.bottom,
                                    top: 10),
                                child: InkWell(
                                  child: Container(
                                    width: double.infinity,
                                    height: 39,
                                    decoration: new BoxDecoration(
                                        color: viewModel.isChecked
                                            ? Color(0xffda3534)
                                            : Color(0xffd9d9d9),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Center(
                                      child: // ลิงก์และวิธีการขาย
                                          Text("ยืนยันเข้าร่วม",
                                              style: const TextStyle(
                                                  color: const Color(0xffffffff),
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: "Thonburi-Bold",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0),
                                              textAlign: TextAlign.center),
                                    ),
                                  ),
                                  onTap: () {
                                    if (viewModel.isChecked) {
                                      viewModel.save();
                                    }
                                  },
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0,
                                    right: 16.0,
                                    bottom: 16.0 + MediaQuery.of(context).padding.bottom,
                                    top: 10),
                                child: InkWell(
                                  child: Container(
                                    width: double.infinity,
                                    height: 39,
                                    decoration: new BoxDecoration(
                                        color: Color(0xffda3534),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Center(
                                      child: // ลิงก์และวิธีการขาย
                                          Text("ลิงก์และวิธีการขาย",
                                              style: const TextStyle(
                                                  color: const Color(0xffffffff),
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0),
                                              textAlign: TextAlign.center),
                                    ),
                                  ),
                                  onTap: () {
                                    showLinkDialog();
                                  },
                                ),
                              )));
          }),
    );
  }

  void showLinkDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var header = viewModel.getAgentContentHowData!.record!.first.fields![0].lang!;
          var description = viewModel.getAgentContentHowData!.record!.first.fields![1].lang!;
          var link = viewModel.getConcertAgentDetailData?.record?.afLink ?? '';
          return TicketLinkDialog(
            header: header,
            description: description,
            link: link,
            showMessageCopy: showMessageCopy,
          );
        });
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void showMessageCopy() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 1),
        backgroundColor: Color(0xffda3534),
        content: new Text(
          "คัดลอกสำเร็จ",
          style: TextStyle(
            fontSize: 14,
            fontFamily: 'Kanit',
            color: Colors.white,
          ),
        ),
        // backgroundColor: Design.theme.colorPrimary,
      ),
    );
  }
}
