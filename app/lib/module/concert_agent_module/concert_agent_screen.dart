import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/concert_agent_module/concert_agent_detail_screen.dart';
import 'package:app/module/concert_agent_module/widget/concert_agent_item.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

import 'concert_agent_screen_view_model.dart';

class ConcertAgentScreen extends StatefulWidget with TixRoute {
  @override
  _ConcertAgentScreenState createState() => _ConcertAgentScreenState();

  @override
  String buildPath() {
    return '/concert_agent_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => ConcertAgentScreen());
  }
}

class _ConcertAgentScreenState
    extends BaseStateProvider<ConcertAgentScreen, ConcertAgentScreenViewModel> {
  @override
  void initState() {
    viewModel = ConcertAgentScreenViewModel();
    viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
      onWillPop: () async {
        SystemNavigator.pop();
        return false;
      },
      child: BaseWidget<ConcertAgentScreenViewModel>(
          model: viewModel,
          builder: (context, model, child) {
            return Scaffold(
                appBar: AgentAppBar(
                  onBack: () {
                    if (WrapNavigation.instance.canPop(context)) {
                      WrapNavigation.instance.pop(context);
                    } else {
                      SystemNavigator.pop();
                    }
                    appState.channel.invokeMethod('pop');
                  },
                  name: 'สิทธิ์บัตรคอนเสิร์ตของตัวแทน',
                ),
                body: RefreshIndicator(
                  onRefresh: () {
                    viewModel.clearPage();
                    viewModel.getConcertAgent();
                    return Future.value();
                  },
                  child: ListView.builder(
                      physics: AlwaysScrollableScrollPhysics(),
                      itemCount: viewModel.concertList.length,
                      controller: viewModel.scrollController,
                      itemBuilder: (context, index) {
                        var item = viewModel.concertList[index];
                        return Column(
                          children: [
                            ConcertAgentItem(
                              imageUrl: item.product?.images?.first.url ?? '',
                              concertName: item.product?.name ?? '',
                              address: item.product?.venue?.name ?? '',
                              time: item.product?.showTime?.textShort ?? '',
                              agentName: item.store?.name ?? '',
                              link: item.afLink,
                              showMessageCopy: showMessageCopy,
                            ),
                            item.product?.showTime?.status == 1
                                ? (item.isCommissioner ?? false)
                                    ? Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xfff0f0f0), width: 6))),
                                        padding:
                                            const EdgeInsets.only(bottom: 16, left: 16, right: 16),
                                        child: InkWell(
                                          onTap: () {
                                            navigateToDetail(item);
                                          },
                                          child: Container(
                                            alignment: Alignment.center,
                                            width: double.infinity,
                                            height: 37,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                                border: Border.all(
                                                    color: const Color(0xffda3534), width: 1),
                                                color: const Color(0xffffffff)),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Image(
                                                  image: AssetImage('assets/user.png'),
                                                  width: 23,
                                                  height: 19,
                                                  color: Color(0xffda3534),
                                                ),
                                                SizedBox(
                                                  width: 11,
                                                ),
                                                Text("ดูรายละเอียด",
                                                    style: const TextStyle(
                                                        color: const Color(0xffda3534),
                                                        fontWeight: FontWeight.w700,
                                                        fontFamily: "Thonburi",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 14.0),
                                                    textAlign: TextAlign.center)
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xfff0f0f0), width: 6))),
                                        padding:
                                            const EdgeInsets.only(bottom: 16, left: 16, right: 16),
                                        child: InkWell(
                                          onTap: () async {
                                            navigateToDetail(item);
                                          },
                                          child: Container(
                                            alignment: Alignment.center,
                                            width: double.infinity,
                                            height: 37,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                                border: Border.all(
                                                    color: const Color(0xffda3534), width: 1),
                                                color: const Color(0xffda3534)),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Image(
                                                  image: AssetImage('assets/user_plus.png'),
                                                  width: 23,
                                                  height: 19,
                                                  color: Color(0xffffffff),
                                                ),
                                                SizedBox(
                                                  width: 11,
                                                ),
                                                // ดูรายละเอียด
                                                Text("สนใจเข้าร่วม",
                                                    style: const TextStyle(
                                                        color: const Color(0xffffffff),
                                                        fontWeight: FontWeight.w700,
                                                        fontFamily: "Thonburi",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: 14.0),
                                                    textAlign: TextAlign.center)
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                : Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom:
                                                BorderSide(color: Color(0xfff0f0f0), width: 6))),
                                    padding: EdgeInsets.only(bottom: 10.0, left: 16, right: 16),
                                    child: Container(
                                      alignment: Alignment.center,
                                      width: double.infinity,
                                      height: 37,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(5)),
                                          border:
                                              Border.all(color: const Color(0xffb9b9b9), width: 1),
                                          color: const Color(0xffb9b9b9)),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text(item.product?.showTime?.statusText ?? '',
                                              style: const TextStyle(
                                                  color: const Color(0xffffffff),
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: "Thonburi",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: 14.0),
                                              textAlign: TextAlign.center)
                                        ],
                                      ),
                                    ),
                                  )
                          ],
                        );
                      }),
                ));
          }),
    );
  }

  void showAlertError(BaseError baseError) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void navigateToDetail(item) async {
    await WrapNavigation.instance
        .pushNamed(context, ConcertAgentDetailScreen(), arguments: item.product!.id!);
    viewModel.clearPage();
    viewModel.getConcertAgent();
  }

  void showMessageCopy() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 1),
        backgroundColor: Color(0xffda3534),
        content: new Text(
          "คัดลอกสำเร็จ",
          style: TextStyle(
            fontSize: 14,
            fontFamily: 'Kanit',
            color: Colors.white,
          ),
        ),
        // backgroundColor: Design.theme.colorPrimary,
      ),
    );
  }
}
