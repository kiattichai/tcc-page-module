import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TicketCoinItem extends StatelessWidget {
  final String ticket;
  final int coin;
  final int price;

  const TicketCoinItem(
      {Key? key, required this.ticket, required this.coin, required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var coinFormatted =  NumberFormat('#,###').format(coin);
    var priceFormatted =  NumberFormat('#,###').format(price);
    return Container(
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Color(0xfff0f0f0), width: 1))),
      margin: const EdgeInsets.only(left: 16, right: 16),
      padding: const EdgeInsets.only(top: 12, bottom: 15),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("$ticket",
                style: const TextStyle(
                    color: const Color(0xff333333),
                    fontWeight: FontWeight.w600,
                    fontFamily: "SFUIText",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0)),
            SizedBox(height: 4,),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                  style: const TextStyle(
                      color: const Color(0xff757575),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Thonburi",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0),
                  text: "มูลค่า "),
              TextSpan(
                  style: const TextStyle(
                      color: const Color(0xff757575),
                      fontWeight: FontWeight.w600,
                      fontFamily: "SFUIText",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0),
                  text: priceFormatted),
              TextSpan(
                  style: const TextStyle(
                      color: const Color(0xff757575),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Thonburi",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0),
                  text: " บาท")
            ]))
          ],
        ),
        Spacer(),
        RichText(
            text: TextSpan(children: [
          TextSpan(
              style: const TextStyle(
                  color: const Color(0xfff8b412),
                  fontWeight: FontWeight.w700,
                  fontFamily: "SFUIText",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0),
              text: coinFormatted),
          TextSpan(
              style: const TextStyle(
                  color: const Color(0xfff8b412),
                  fontWeight: FontWeight.w400,
                  fontFamily: "SFUIText",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0),
              text: " coins")
        ]))
      ]),
    );
  }
}
