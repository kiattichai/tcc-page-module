import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ConcertAgentItem extends StatelessWidget {
  final String imageUrl;
  final String concertName;
  final String address;
  final String time;
  final String agentName;
  final String? link;
  final Function() showMessageCopy;

  const ConcertAgentItem({
    Key? key,
    required this.imageUrl,
    required this.concertName,
    required this.address,
    required this.time,
    required this.agentName,
    required this.link,
    required this.showMessageCopy,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          imageUrl.isNotEmpty
              ? Container(
                  width: 100,
                  height: 133,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(imageUrl),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(4))))
              : SizedBox(),
          SizedBox(
            width: 13,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("$concertName",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: const Color(0xff333333),
                      fontWeight: FontWeight.w600,
                      fontFamily: "SFUIText",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0,
                    )),
                SizedBox(
                  height: 2,
                ),
                link != null
                    ? Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Spacer(),
                          InkWell(
                              onTap: () async {
                                await Clipboard.setData(
                                  new ClipboardData(text: link??""),
                                );
                                showMessageCopy();
                              },
                              child: Container(
                                constraints: BoxConstraints(minWidth: 86),
                                padding:
                                    const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                    border: Border.all(color: const Color(0xffe0e0e0), width: 1),
                                    color: const Color(0xffffffff)),
                                child: Row(
                                  children: [
                                    Image(
                                      image: AssetImage('assets/link.png'),
                                      width: 13,
                                      height: 13,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text("คัดลอกลิงก์",
                                        style: const TextStyle(
                                            color: const Color(0xff989898),
                                            fontWeight: FontWeight.w400,
                                            fontFamily: "Thonburi",
                                            fontStyle: FontStyle.normal,
                                            fontSize: 12.0))
                                  ],
                                ),
                              ),
                            ),
                        ],
                      )
                    : SizedBox(height: 23),
                SizedBox(
                  height: 6,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image(
                      image: AssetImage('assets/map_pin.png'),
                      width: 13,
                      height: 13,
                    ),
                    SizedBox(
                      width: 9,
                    ),
                    Expanded(
                        child: Text(address,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w400,
                                fontFamily: "SFUIText",
                                fontStyle: FontStyle.normal,
                                fontSize: 12.0)))
                  ],
                ),
                SizedBox(
                  height: 9,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image(
                      image: AssetImage('assets/calendar.png'),
                      width: 13,
                      height: 13,
                    ),
                    SizedBox(
                      width: 9,
                    ),
                    Expanded(
                        child: Text("$time",
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w400,
                                fontFamily: "SFUIText",
                                fontStyle: FontStyle.normal,
                                fontSize: 12.0)))
                  ],
                ),
                SizedBox(
                  height: 9,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image(
                      image: AssetImage('assets/user.png'),
                      width: 13,
                      height: 13,
                    ),
                    SizedBox(
                      width: 9,
                    ),
                    Expanded(
                        child: Text("$agentName",
                            style: const TextStyle(
                                color: const Color(0xff333333),
                                fontWeight: FontWeight.w400,
                                fontFamily: "SFUIText",
                                fontStyle: FontStyle.normal,
                                fontSize: 12.0)))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
