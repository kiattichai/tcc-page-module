import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';

class TicketLinkDialog extends StatelessWidget {
  final String header;
  final String description;
  final String link;
  final Function() showMessageCopy;

  const TicketLinkDialog(
      {Key? key,
      required this.header,
      required this.description,
      required this.link,
      required this.showMessageCopy})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  WrapNavigation.instance.pop(context);
                },
                child: Container(
                  width: 18,
                  height: 18,
                  margin: const EdgeInsets.only(right: 8, top: 8),
                  child: Image(
                    image: AssetImage('assets/x_copy.png'),
                    fit: BoxFit.cover,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            // Link และวิธีการขาย
            Center(
              child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: [
                    TextSpan(
                        style: const TextStyle(
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.w600,
                            fontFamily: "SFUIText",
                            fontStyle: FontStyle.normal,
                            fontSize: 16.0),
                        text: "Link "),
                    TextSpan(
                        style: const TextStyle(
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.w700,
                            fontFamily: "Thonburi",
                            fontStyle: FontStyle.normal,
                            fontSize: 16.0),
                        text: "และวิธีการขาย")
                  ])),
            ),
            SizedBox(
              height: 17,
            ),
            // สามารถคัดลอกลิงก์ด้า

            Padding(
              padding: const EdgeInsets.only(left: 21, right: 21),
              child: Text("สามารถคัดลอกลิงก์ด้านล่างนี้ เพื่อนำไปแชร์กับเพื่อนหรือโปรโมทได้",
                  style: const TextStyle(
                      color: const Color(0xff616161),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Thonburi",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0)),
            ),
            // Rectangle
            Container(
              margin: const EdgeInsets.only(left: 16, right: 16, top: 19, bottom: 19),
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(3)),
                  border: Border.all(color: const Color(0xffe0e0e0), width: 1),
                  color: const Color(0xfff9f9f9)),
              child: // https://www.theconce
                  Text("$link",
                      style: const TextStyle(
                          color: const Color(0xff333333),
                          fontWeight: FontWeight.w400,
                          fontFamily: "SFUIText",
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0)),
            ),
            Center(
              child: // Rectangle
                  InkWell(
                onTap: () async {
                  await Clipboard.setData(
                    new ClipboardData(text: link),
                  );
                  WrapNavigation.instance.pop(context);
                  showMessageCopy();
                },
                child: Container(
                  alignment: Alignment.center,
                  width: 124,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      color: const Color(0xffda3534)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage('assets/link.png'),
                        width: 15,
                        height: 15,
                        color: Colors.white,
                      ),
                      // คัดลอกลิงก์
                      Text("คัดลอกลิงก์",
                          style: const TextStyle(
                              color: const Color(0xffffffff),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Thonburi",
                              fontStyle: FontStyle.normal,
                              fontSize: 14.0),
                          textAlign: TextAlign.center)
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.only(left: 22.5, bottom: 7),
              child: // วิธีการขาย
                  Text("วิธีการขาย",
                      style: const TextStyle(
                          color: const Color(0xff1f1f1f),
                          fontWeight: FontWeight.w700,
                          fontFamily: "Thonburi",
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0)),
            ),
            Container(
                padding: const EdgeInsets.only(left: 20, bottom: 16),
                child: Html(data: description)),
          ],
        ),
      ),
    );
  }
}
