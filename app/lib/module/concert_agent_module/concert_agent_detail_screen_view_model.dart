import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/concert_agent_module/model/get_agent_content_how_entity.dart';
import 'package:app/module/concert_agent_module/model/get_agent_sale_term_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_detail_entity.dart';
import 'package:flutter/material.dart';

class ConcertAgentDetailScreenViewModel extends BaseViewModel{
  late ScrollController scrollController;
  final int concertId;
  GetConcertAgentDetailData? getConcertAgentDetailData;
  GetAgentSaleTermData? getAgentSaleTermData;
  GetAgentContentHowData? getAgentContentHowData;
  Function(BaseError)? showAlertError;
  Function()? showLinkDialog;
  bool isChecked = false;

  ConcertAgentDetailScreenViewModel(this.concertId){
    scrollController = ScrollController();
  }


  void postInit() {
    super.postInit();
    getConcertAgentDetail();
    getAgentSaleTerm();
    getAgentContentHow();
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }


  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }

  void getConcertAgentDetail() {
    catchError(() async {
      setLoading(true);
      getConcertAgentDetailData = await di.concertAgentRepository.getConcertAgentDetail(concertId);
      setLoading(false);
    });
  }

  void save(){
    catchError(() async {
      setLoading(true);
      var result = await di.concertAgentRepository.saveConcertAgent(getConcertAgentDetailData!.record!.promotionId!);
      await Future.delayed(Duration(milliseconds: 300), ()  {});
      getConcertAgentDetailData = await di.concertAgentRepository.getConcertAgentDetail(concertId);
      setLoading(false);
    });
    showLinkDialog!();
  }

  void getAgentSaleTerm() {
    catchError(() async {
      setLoading(true);
      getAgentSaleTermData = await di.concertAgentRepository.getAgentSaleTerm();
      setLoading(false);
    });
  }

  void getAgentContentHow() {
    catchError(() async {
      setLoading(true);
      getAgentContentHowData = await di.concertAgentRepository.getAgentHowTerm();
      setLoading(false);
    });
  }

  void setChecked(bool? value) {
    isChecked = value??false;
    notifyListeners();
  }
}