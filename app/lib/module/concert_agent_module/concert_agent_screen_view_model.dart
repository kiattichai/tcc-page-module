import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConcertAgentScreenViewModel extends BaseViewModel{
  late ScrollController scrollController;
  GetConcertAgentData? getConcertAgentData;
  List<GetConcertAgentDataRecord> concertList = [];
  int limit = 10;
  int page = 1;
  bool isComplete = false;
  Function(BaseError)? showAlertError;

  ConcertAgentScreenViewModel(){
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0 && !isComplete) {
        getConcertAgent();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    getConcertAgent();
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }

  void getConcertAgent() {
    catchError(() async {
      setLoading(true);
      if (isComplete == false) {
        isComplete = true;
        getConcertAgentData = await di.concertAgentRepository.getConcertAgent(limit,page: page);
      }
      if (page == 1) {
        concertList.clear();
      }
      if (getConcertAgentData?.record != null &&
          getConcertAgentData!.record!.length > 0 &&
          getConcertAgentData!.pagination!.currentPage! <= getConcertAgentData!.pagination!.lastPage!) {
        concertList.addAll(getConcertAgentData?.record ?? []);
        if(getConcertAgentData!.pagination!.currentPage! != getConcertAgentData!.pagination!.lastPage!){
          isComplete = false;
          page++;
        }

      } else {
        isComplete = true;
      }
      setLoading(false);
    });
  }

  void clearPage() {
    page = 1;
    isComplete = false;
    notifyListeners();
  }


  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }

}