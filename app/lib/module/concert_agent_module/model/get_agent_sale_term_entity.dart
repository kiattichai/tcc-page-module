import 'package:app/generated/json/get_agent_sale_term_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAgentSaleTermEntity {
  GetAgentSaleTermEntity();

  factory GetAgentSaleTermEntity.fromJson(Map<String, dynamic> json) =>
      $GetAgentSaleTermEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentSaleTermEntityToJson(this);

  GetAgentSaleTermData? data;
  GetAgentSaleTermBench? bench;
}

@JsonSerializable()
class GetAgentSaleTermData {
  GetAgentSaleTermData();

  factory GetAgentSaleTermData.fromJson(Map<String, dynamic> json) =>
      $GetAgentSaleTermDataFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentSaleTermDataToJson(this);

  GetAgentSaleTermDataPagination? pagination;
  List<GetAgentSaleTermDataRecord>? record;
}

@JsonSerializable()
class GetAgentSaleTermDataPagination {
  GetAgentSaleTermDataPagination();

  factory GetAgentSaleTermDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetAgentSaleTermDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentSaleTermDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetAgentSaleTermDataRecord {
  GetAgentSaleTermDataRecord();

  factory GetAgentSaleTermDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetAgentSaleTermDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentSaleTermDataRecordToJson(this);

  List<GetAgentSaleTermDataRecordFields>? fields;
}

@JsonSerializable()
class GetAgentSaleTermDataRecordFields {
  GetAgentSaleTermDataRecordFields();

  factory GetAgentSaleTermDataRecordFields.fromJson(
          Map<String, dynamic> json) =>
      $GetAgentSaleTermDataRecordFieldsFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAgentSaleTermDataRecordFieldsToJson(this);

  String? name;
  String? lang;
}

@JsonSerializable()
class GetAgentSaleTermBench {
  GetAgentSaleTermBench();

  factory GetAgentSaleTermBench.fromJson(Map<String, dynamic> json) =>
      $GetAgentSaleTermBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentSaleTermBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
