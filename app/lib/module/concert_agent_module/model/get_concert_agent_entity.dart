import 'package:app/generated/json/get_concert_agent_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConcertAgentEntity {
  GetConcertAgentEntity();

  factory GetConcertAgentEntity.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentEntityToJson(this);

  GetConcertAgentData? data;
  GetConcertAgentBench? bench;
}

@JsonSerializable()
class GetConcertAgentData {
  GetConcertAgentData();

  factory GetConcertAgentData.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentDataFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentDataToJson(this);

  GetConcertAgentDataPagination? pagination;
  List<GetConcertAgentDataRecord>? record;
}

@JsonSerializable()
class GetConcertAgentDataPagination {
  GetConcertAgentDataPagination();

  factory GetConcertAgentDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetConcertAgentDataRecord {
  GetConcertAgentDataRecord();

  factory GetConcertAgentDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentDataRecordToJson(this);

  int? id;
  @JSONField(name: "store_id")
  int? storeId;
  @JSONField(name: "is_commissioner")
  bool? isCommissioner;
  GetConcertAgentDataRecordProduct? product;
  @JSONField(name: "af_link")
  String? afLink;
  GetConcertAgentDataRecordStore? store;
}

@JsonSerializable()
class GetConcertAgentDataRecordProduct {
  GetConcertAgentDataRecordProduct();

  factory GetConcertAgentDataRecordProduct.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductToJson(this);

  int? id;
  String? name;
  GetConcertAgentDataRecordProductVenue? venue;
  List<GetConcertAgentDataRecordProductImages>? images;
  @JSONField(name: "show_time")
  GetConcertAgentDataRecordProductShowTime? showTime;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductVenue {
  GetConcertAgentDataRecordProductVenue();

  factory GetConcertAgentDataRecordProductVenue.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductVenueFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductVenueToJson(this);

  int? id;
  double? lat;
  double? long;
  String? name;
  dynamic? address;
  GetConcertAgentDataRecordProductVenueCountry? country;
  GetConcertAgentDataRecordProductVenueProvince? province;
  GetConcertAgentDataRecordProductVenueCity? city;
  GetConcertAgentDataRecordProductVenueDistrict? district;
  @JSONField(name: "zip_code")
  int? zipCode;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductVenueCountry {
  GetConcertAgentDataRecordProductVenueCountry();

  factory GetConcertAgentDataRecordProductVenueCountry.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductVenueCountryFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductVenueCountryToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductVenueProvince {
  GetConcertAgentDataRecordProductVenueProvince();

  factory GetConcertAgentDataRecordProductVenueProvince.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductVenueProvinceFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductVenueProvinceToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductVenueCity {
  GetConcertAgentDataRecordProductVenueCity();

  factory GetConcertAgentDataRecordProductVenueCity.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductVenueCityFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductVenueCityToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductVenueDistrict {
  GetConcertAgentDataRecordProductVenueDistrict();

  factory GetConcertAgentDataRecordProductVenueDistrict.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductVenueDistrictFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductVenueDistrictToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductImages {
  GetConcertAgentDataRecordProductImages();

  factory GetConcertAgentDataRecordProductImages.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductImagesFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductImagesToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}

@JsonSerializable()
class GetConcertAgentDataRecordProductShowTime {
  GetConcertAgentDataRecordProductShowTime();

  factory GetConcertAgentDataRecordProductShowTime.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordProductShowTimeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordProductShowTimeToJson(this);

  String? start;
  String? end;
  @JSONField(name: "text_full")
  String? textFull;
  @JSONField(name: "text_short")
  String? textShort;
  @JSONField(name: "text_short_date")
  String? textShortDate;
  int? status;
  @JSONField(name: "status_text")
  String? statusText;
}

@JsonSerializable()
class GetConcertAgentDataRecordStore {
  GetConcertAgentDataRecordStore();

  factory GetConcertAgentDataRecordStore.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordStoreFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentDataRecordStoreToJson(this);

  String? name;
  String? slug;
  GetConcertAgentDataRecordStoreSection? section;
}

@JsonSerializable()
class GetConcertAgentDataRecordStoreSection {
  GetConcertAgentDataRecordStoreSection();

  factory GetConcertAgentDataRecordStoreSection.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertAgentDataRecordStoreSectionFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertAgentDataRecordStoreSectionToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetConcertAgentBench {
  GetConcertAgentBench();

  factory GetConcertAgentBench.fromJson(Map<String, dynamic> json) =>
      $GetConcertAgentBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertAgentBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
