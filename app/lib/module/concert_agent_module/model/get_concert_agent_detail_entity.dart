import 'package:app/generated/json/get_concert_agent_detail_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConcertAgentDetailEntity {

	GetConcertAgentDetailEntity();

	factory GetConcertAgentDetailEntity.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailEntityToJson(this);

	GetConcertAgentDetailData? data;
	GetConcertAgentDetailBench? bench;
}

@JsonSerializable()
class GetConcertAgentDetailData {

	GetConcertAgentDetailData();

	factory GetConcertAgentDetailData.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataToJson(this);

	GetConcertAgentDetailDataRecord? record;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecord {

	GetConcertAgentDetailDataRecord();

	factory GetConcertAgentDetailDataRecord.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordToJson(this);

	int? id;
	String? name;
	GetConcertAgentDetailDataRecordStore? store;
	GetConcertAgentDetailDataRecordVenue? venue;
	@JSONField(name: "show_time")
	GetConcertAgentDetailDataRecordShowTime? showTime;
	@JSONField(name: "commissionable_tickets")
	List<GetConcertAgentDetailDataRecordCommissionableTickets>? commissionableTickets;
	@JSONField(name: "promotion_id")
	int? promotionId;
	@JSONField(name: "concert_images")
	List<GetConcertAgentDetailDataRecordConcertImages>? concertImages;
	@JSONField(name: "is_commissioner")
	bool? isCommissioner;
	@JSONField(name: "af_link")
	String? afLink;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordStore {

	GetConcertAgentDetailDataRecordStore();

	factory GetConcertAgentDetailDataRecordStore.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordStoreFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordStoreToJson(this);

	int? id;
	String? name;
	GetConcertAgentDetailDataRecordStoreSection? section;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordStoreSection {

	GetConcertAgentDetailDataRecordStoreSection();

	factory GetConcertAgentDetailDataRecordStoreSection.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordStoreSectionFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordStoreSectionToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordVenue {

	GetConcertAgentDetailDataRecordVenue();

	factory GetConcertAgentDetailDataRecordVenue.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordVenueFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordVenueToJson(this);

	int? id;
	double? lat;
	double? long;
	String? name;
	String? address;
	GetConcertAgentDetailDataRecordVenueCountry? country;
	GetConcertAgentDetailDataRecordVenueProvince? province;
	GetConcertAgentDetailDataRecordVenueCity? city;
	GetConcertAgentDetailDataRecordVenueDistrict? district;
	@JSONField(name: "zip_code")
	int? zipCode;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordVenueCountry {

	GetConcertAgentDetailDataRecordVenueCountry();

	factory GetConcertAgentDetailDataRecordVenueCountry.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordVenueCountryFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordVenueCountryToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordVenueProvince {

	GetConcertAgentDetailDataRecordVenueProvince();

	factory GetConcertAgentDetailDataRecordVenueProvince.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordVenueProvinceFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordVenueProvinceToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordVenueCity {

	GetConcertAgentDetailDataRecordVenueCity();

	factory GetConcertAgentDetailDataRecordVenueCity.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordVenueCityFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordVenueCityToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordVenueDistrict {

	GetConcertAgentDetailDataRecordVenueDistrict();

	factory GetConcertAgentDetailDataRecordVenueDistrict.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordVenueDistrictFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordVenueDistrictToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordShowTime {

	GetConcertAgentDetailDataRecordShowTime();

	factory GetConcertAgentDetailDataRecordShowTime.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordShowTimeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordShowTimeToJson(this);

	String? start;
	String? end;
	@JSONField(name: "text_full")
	String? textFull;
	@JSONField(name: "text_short")
	String? textShort;
	@JSONField(name: "text_short_date")
	String? textShortDate;
	int? status;
	@JSONField(name: "status_text")
	String? statusText;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordCommissionableTickets {

	GetConcertAgentDetailDataRecordCommissionableTickets();

	factory GetConcertAgentDetailDataRecordCommissionableTickets.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordCommissionableTicketsFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordCommissionableTicketsToJson(this);

	int? id;
	String? name;
	int? price;
	@JSONField(name: "commissioner_reward")
	int? commissionerReward;
	@JSONField(name: "start_date")
	String? startDate;
	@JSONField(name: "end_date")
	String? endDate;
	@JSONField(name: "text_full")
	String? textFull;
	@JSONField(name: "text_short")
	String? textShort;
}

@JsonSerializable()
class GetConcertAgentDetailDataRecordConcertImages {

	GetConcertAgentDetailDataRecordConcertImages();

	factory GetConcertAgentDetailDataRecordConcertImages.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailDataRecordConcertImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailDataRecordConcertImagesToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	String? tag;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	int? position;
}

@JsonSerializable()
class GetConcertAgentDetailBench {

	GetConcertAgentDetailBench();

	factory GetConcertAgentDetailBench.fromJson(Map<String, dynamic> json) => $GetConcertAgentDetailBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertAgentDetailBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
