import 'package:app/generated/json/get_agent_content_how_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAgentContentHowEntity {

	GetAgentContentHowEntity();

	factory GetAgentContentHowEntity.fromJson(Map<String, dynamic> json) => $GetAgentContentHowEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowEntityToJson(this);

	GetAgentContentHowData? data;
	GetAgentContentHowBench? bench;
}

@JsonSerializable()
class GetAgentContentHowData {

	GetAgentContentHowData();

	factory GetAgentContentHowData.fromJson(Map<String, dynamic> json) => $GetAgentContentHowDataFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowDataToJson(this);

	GetAgentContentHowDataPagination? pagination;
	List<GetAgentContentHowDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetAgentContentHowDataPagination {

	GetAgentContentHowDataPagination();

	factory GetAgentContentHowDataPagination.fromJson(Map<String, dynamic> json) => $GetAgentContentHowDataPaginationFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowDataPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class GetAgentContentHowDataRecord {

	GetAgentContentHowDataRecord();

	factory GetAgentContentHowDataRecord.fromJson(Map<String, dynamic> json) => $GetAgentContentHowDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowDataRecordToJson(this);

	List<GetAgentContentHowDataRecordFields>? fields;
}

@JsonSerializable()
class GetAgentContentHowDataRecordFields {

	GetAgentContentHowDataRecordFields();

	factory GetAgentContentHowDataRecordFields.fromJson(Map<String, dynamic> json) => $GetAgentContentHowDataRecordFieldsFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowDataRecordFieldsToJson(this);

	String? name;
	String? lang;
}

@JsonSerializable()
class GetAgentContentHowBench {

	GetAgentContentHowBench();

	factory GetAgentContentHowBench.fromJson(Map<String, dynamic> json) => $GetAgentContentHowBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetAgentContentHowBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
