import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/module/concert_agent_module/model/get_agent_content_how_entity.dart';
import 'package:app/module/concert_agent_module/model/get_agent_sale_term_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_detail_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class ConcertAgentApi {
  final CoreApi api;
  final String pathAgents = '/agents';
  final String pathPromotions = '/promotions';
  final String pathProducts = '/products';
  final String pathContent = '/contents';

  ConcertAgentApi(this.api);

  Future<GetConcertAgentEntity> getConcertAgent(int limit, int page) async {
    Response response = await api.get(pathAgents + pathPromotions,
        {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'}, hasPermission: true);
    return GetConcertAgentEntity.fromJson(response.data);
  }

  Future<GetConcertAgentDetailEntity> getConcertAgentDetail(
      int concertId) async {
    Response response = await api.get(pathAgents + pathProducts + '/$concertId',
        null, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'}, hasPermission: true);
    return GetConcertAgentDetailEntity.fromJson(response.data);
  }

  Future<BaseResponseEntity> saveConcertAgent(int promotionId) async {
    Response response = await api.post(pathAgents + pathPromotions,
        {"promotion_id": promotionId}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return BaseResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/contents?group_code=page&slug=tcc-agent-sales&limit=1&page=1&status=true&fields=fields(name,lang)
  Future<GetAgentSaleTermEntity> getAgentSaleTerm() async {
    Response response = await api.get(
        pathContent,
        {
          'group_code': 'page',
          'slug': 'tcc-agent-sales',
          'limit': 1,
          'page': 1,
          'status': true,
          'fields': 'fields(name,lang)'
        },
        BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'});
    return GetAgentSaleTermEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/contents?group_code=page&slug=tcc-agent-how&limit=1&page=1&status=true&fields=fields(name,lang)
  Future<GetAgentContentHowEntity> getAgentHowTerm() async {
    Response response = await api.get(
        pathContent,
        {
          'group_code': 'page',
          'slug': 'tcc-agent-how',
          'limit': 1,
          'page': 1,
          'status': true,
          'fields': 'fields(name,lang)'
        },
        BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'});
    return GetAgentContentHowEntity.fromJson(response.data);
  }
}
