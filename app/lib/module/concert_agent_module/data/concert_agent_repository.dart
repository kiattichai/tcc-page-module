import 'package:app/core/base_repository.dart';
import 'package:app/model/base_response_entity.dart';
import 'package:app/module/concert_agent_module/data/concert_agent_api.dart';
import 'package:app/module/concert_agent_module/model/get_agent_content_how_entity.dart';
import 'package:app/module/concert_agent_module/model/get_agent_sale_term_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_detail_entity.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class ConcertAgentRepository extends BaseRepository {
  final ConcertAgentApi api;

  ConcertAgentRepository(this.api);

  Future<GetConcertAgentData?> getConcertAgent( int limit, {int page = 1}) async {
    final result = await api.getConcertAgent(limit, page);
    return result.data;
  }

  Future<GetConcertAgentDetailData?> getConcertAgentDetail(int concertId) async {
    final result = await api.getConcertAgentDetail(concertId);
    return result.data;
  }

  Future<BaseResponseData?> saveConcertAgent(int promotionId) async {
    final result = await api.saveConcertAgent(promotionId);
    return result.data;
  }

  Future<GetAgentSaleTermData?> getAgentSaleTerm() async {
    final result = await api.getAgentSaleTerm();
    return result.data;
  }

  Future<GetAgentContentHowData?> getAgentHowTerm() async {
    final result = await api.getAgentHowTerm();
    return result.data;
  }

}