import 'package:app/api/core_api.dart';
import 'package:app/module/share_module/model/refresh_token_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import '../../../globals.dart' as globals;

@singleton
class RefreshTokenApi {
  final CoreApi coreApi;
  final String pathRefreshToken = "/users/refresh-token";

  RefreshTokenApi(this.coreApi);

  Future<bool> refreshToken(String token, Function badRequestToModelError) async {
    Response response =
        await coreApi.putRefreshToken(pathRefreshToken, token, badRequestToModelError);
    final responseEntity = RefreshTokenResponseEntity.fromJson(response.data);
    if (responseEntity != null && responseEntity.data != null) {
      final data = responseEntity.data;
      globals.userAuthen!.data!.refreshToken = data!.accessToken!;
      globals.userAuthen!.data!.accessToken = data.accessToken!;
      globals.userAuthen!.data!.tokenType = data.tokenType!;
      return true;
    } else {
      return false;
    }
  }
}
