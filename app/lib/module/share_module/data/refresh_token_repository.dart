import 'package:app/core/base_repository.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/share_module/data/refresh_token_api.dart';
import 'package:injectable/injectable.dart';

@singleton
class RefreshTokenRepository extends BaseRepository {
  final RefreshTokenApi api;

  RefreshTokenRepository(this.api);

  Future<bool> refreshToken(String token) async {
    final result = await api.refreshToken(token, BaseErrorEntity.badRequestToModelError);
    return result;
  }
}
