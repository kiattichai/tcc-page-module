import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class MockUpNativeScreenDataReceive extends StatelessWidget with TixRoute {
  final Object? data;
  const MockUpNativeScreenDataReceive({Key? key, this.data}) : super(key: key);

  @override
  String buildPath() {
    return '/mockup_native_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => MockUpNativeScreenDataReceive(data: data));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'MockUpNativeScreenDataReceive',
      ),
      body: Center(
        child: Text(
          data?.toString() ?? '',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
