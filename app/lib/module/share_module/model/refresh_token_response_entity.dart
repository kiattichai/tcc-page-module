import 'package:app/generated/json/refresh_token_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class RefreshTokenResponseEntity {
  RefreshTokenResponseEntity();

  factory RefreshTokenResponseEntity.fromJson(Map<String, dynamic> json) =>
      $RefreshTokenResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $RefreshTokenResponseEntityToJson(this);

  RefreshTokenResponseData? data;
}

@JsonSerializable()
class RefreshTokenResponseData {
  RefreshTokenResponseData();

  factory RefreshTokenResponseData.fromJson(Map<String, dynamic> json) =>
      $RefreshTokenResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $RefreshTokenResponseDataToJson(this);

  @JSONField(name: "token_type")
  String? tokenType;
  @JSONField(name: "access_token")
  String? accessToken;
  @JSONField(name: "refresh_token")
  String? refreshToken;
}
