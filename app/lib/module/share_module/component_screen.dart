import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/nightlife_passport_module/nightlife_passport_screen.dart';
import 'package:app/module/nightlife_passport_module/register_nightlife_screen.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_term_and_condition.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:app/model/get_my_page_entity.dart';
import 'package:app/module/create_page_module/create_page_name.dart';
import 'package:app/module/log_in_module/login_screen.dart';
import 'package:app/module/public_page_module/home_page_screen.dart';
import 'package:app/utils/screen.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:app/globals.dart' as globals;

class ComponentPageScreen extends StatefulWidget with TixRoute {
  @override
  String buildPath() {
    return '/component_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => ComponentPageScreen());
  }

  @override
  _ComponentPageScreen createState() => _ComponentPageScreen();
}

class _ComponentPageScreen extends BaseStateProvider<ComponentPageScreen,
    ComponentPageScreenViewModel> {
  @override
  void initState() {
    viewModel = ComponentPageScreenViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);

    return BaseWidget<ComponentPageScreenViewModel>(
      builder: (context, model, child) {
        return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Text("Feature",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff4a4a4a),
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      globals.userDetail != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: ClipOval(
                                child: Image.network(
                                  '${globals.userDetail?.data?.avatar?.url?.replaceAll('jpeg', 'jpg')}',
                                  fit: BoxFit.cover,
                                  width: 79,
                                  height: 79,
                                  errorBuilder: (context, error, stackTrace) =>
                                      Image(
                                    width: 79,
                                    height: 79,
                                    image: AssetImage("assets/no_image.jpeg"),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      globals.userDetail?.data?.avatar?.url != null
                          ? Container(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 10),
                              child: Text(
                                '${globals.userDetail?.data?.firstName} ${globals.userDetail?.data?.lastName}',
                                style: TextStyle(fontSize: 18),
                              ))
                          : SizedBox(),
                      ...(viewModel.myPageEntity?.data?.map((e) {
                            return Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  e.avatar != null
                                      ? CircleAvatar(
                                          radius: 18,
                                          backgroundImage: NetworkImage(
                                              'https://alpha-res.theconcert.com/c_thumb/${e.avatar?.id}/${e.avatar?.name}'),
                                        )
                                      : CircleAvatar(
                                          radius: 18,
                                          backgroundImage: AssetImage(
                                              'assets/image-empty.png'),
                                        ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      WrapNavigation.instance.pushNamed(
                                          context, MyHomePage(), arguments: {
                                        'storeId': e.storeId,
                                        'isOwner': true
                                      });
                                    },
                                    child: Text(
                                      e.displayName ?? '',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ),
                                ],
                              ),
                              width: Screen.width,
                            );
                          }).toList() ??
                          []),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            WrapNavigation.instance.pushNamed(
                                context, MyHomePage(), arguments: {
                              'storeId': 5000986,
                              'isOwner': false
                            });
                          },
                          child: Text(
                            'Public Page',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            //TixNavigate.instance.navigateTo(CreatePageName());
                            WrapNavigation.instance
                                .pushNamed(context, CreatePageName());
                          },
                          child: Text(
                            'Create Page',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            viewModel.navigateLogin(context);
                          },
                          child: Text(
                            'Login Page',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            // WrapNavigation.instance
                            //     .pushNamed(context, NightlifePassportScreen());
                            WrapNavigation.instance
                                .pushNamed(context, RegisterNightlifeScreen());
                          },
                          child: Text(
                            'WebView',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            // WrapNavigation.instance
                            //     .pushNamed(context, NightlifePassportScreen());
                            WrapNavigation.instance.pushNamed(
                                context, RegisterNightlifeScreen(),
                                arguments: true);
                          },
                          child: Text(
                            'VACCINE PASSPORT',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      ),
                      Container(
                        child: TextButton(
                          onPressed: () {
                            WrapNavigation.instance
                                .pushNamed(context, NightlifePassportScreen());
                          },
                          child: Text(
                            'VACCINE PASSPORT REGISTERD',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        width: Screen.width,
                      )
                    ],
                  ),
                ),
              ),
            ));
      },
      model: viewModel,
    );
  }
}

class ComponentPageScreenViewModel extends BaseViewModel {
  GetMyPageEntity? myPageEntity;

  @override
  void postInit() {
    // TODO: implement postInit
    super.postInit();
    getToken();
  }

  void getToken() async {
    catchError(() async {
      if (globals.userAuthen != null) {
        globals.userDetail = await di.logInRepository.getUserDetail();
      }
      if (globals.userDetail != null) {
        getMyPage();
      }
    });
  }

  void getMyPage() {
    catchError(() async {
      setLoading(true);
      myPageEntity = await di.pageRepository.getMyPage(100, 1);
      setLoading(false);
    });
  }

  void navigateLogin(BuildContext context) {
    catchError(() async {
      //final result = await TixNavigate.instance.navigateTo(LoginScreen());
      final result =
          await WrapNavigation.instance.pushNamed(context, LoginScreen());
      if (result != null && result == true) {
        getMyPage();
      }
    });
  }
}

// class TestFlutterBoost extends StatefulWidget {
//   const TestFlutterBoost({Key? key}) : super(key: key);
//
//   @override
//   _TestFlutterBoostState createState() => _TestFlutterBoostState();
// }
//
// class _TestFlutterBoostState extends State<TestFlutterBoost> {
//   static Map<String, FlutterBoostRouteFactory> routerMap = {
//     '/': (settings, uniqueId) {
//       return PageRouteBuilder<dynamic>(
//           settings: settings, pageBuilder: (_, __, ___) => ComponentPageScreen());
//     },
//     '/home_page_screen': (settings, id) {
//       return PageRouteBuilder<dynamic>(
//           pageBuilder: (_, __, ___) {
//             final data = settings.arguments as Map<String, dynamic>;
//             final model = data['data'] as NearByStoreEntity?;
//             return MyHomePage(storeId: model?.id, isOwner: false);
//           },
//           settings: settings);
//     },
//   };
//
//   Route<dynamic>? routeFactory(RouteSettings settings, String uniqueId) {
//     FlutterBoostRouteFactory? func = routerMap[settings.name];
//     if (func == null) {
//       return null;
//     }
//     return func(settings, uniqueId);
//   }
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return FlutterBoostApp(routeFactory);
//   }
// }
