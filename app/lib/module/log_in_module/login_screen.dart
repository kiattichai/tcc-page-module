import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/main.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

import '../../../globals.dart' as globals;

class LoginScreen extends StatefulWidget with TixRoute {
  @override
  _LoginScreen createState() => _LoginScreen();

  @override
  String buildPath() {
    return '/login';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => LoginScreen());
  }
}

class _LoginScreen extends BaseStateProvider<LoginScreen, LoginScreenViewModel> {
  TextStyle style = TextStyle(fontFamily: 'SukhumvitSet-Text', fontSize: 16.0);

  @override
  void initState() {
    viewModel = LoginScreenViewModel();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      obscureText: false,
      controller: viewModel.username,
      style: style,
      decoration: InputDecoration(
        hintText: "เบอร์โทรศัพท์",
        isDense: true,
        border: OutlineInputBorder(),
      ),
    );
    final passwordField = TextField(
      obscureText: true,
      controller: viewModel.password,
      style: style,
      decoration: InputDecoration(
        hintText: "รหัสผ่าน",
        isDense: true,
        border: OutlineInputBorder(),
      ),
    );
    final loginButon = Container(
      width: double.infinity,
      height: 48,
      child: ElevatedButton(
          child: Text(
            "เข้าสู่ระบบ",
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xffffffff),
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor: MaterialStateProperty.all<Color>(Color(0xffda3534)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ))),
          onPressed: () => viewModel.login(context)),
    );

    return BaseWidget<LoginScreenViewModel>(
      builder: (context, model, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 155.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 45.0),
                  emailField,
                  SizedBox(height: 25.0),
                  passwordField,
                  SizedBox(
                    height: 35.0,
                  ),
                  loginButon,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
      model: viewModel,
    );
  }
}

class LoginScreenViewModel extends BaseViewModel {
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();

  void login(BuildContext context) {
    catchError(() async {
      globals.userAuthen = await di.logInRepository.login(username.text, password.text);
      if (globals.userAuthen != null) {
        globals.userDetail = await di.logInRepository.getUserDetail();
      }
      //TixNavigate.instance.pop(data: true);
      WrapNavigation.instance.pop(context, data: true);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}
