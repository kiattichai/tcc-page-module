import 'package:app/generated/json/log_in_request_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class LogInRequestEntity {

	LogInRequestEntity();

	factory LogInRequestEntity.fromJson(Map<String, dynamic> json) => $LogInRequestEntityFromJson(json);

	Map<String, dynamic> toJson() => $LogInRequestEntityToJson(this);

	String? username;
	String? password;
	@JSONField(name: "device_id")
	String? deviceId;
	@JSONField(name: "notification_token")
	String? notificationToken;
	@JSONField(name: "device_type")
	String? deviceType;
	@JSONField(name: "ip_address")
	String? ipAddress;
	@JSONField(name: "country_code")
	String? countryCode;
}
