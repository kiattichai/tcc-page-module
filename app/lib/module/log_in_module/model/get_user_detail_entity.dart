import 'package:app/generated/json/get_user_detail_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetUserDetailEntity {
  GetUserDetailEntity();

  factory GetUserDetailEntity.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailEntityToJson(this);

  GetUserDetailData? data;
  GetUserDetailBench? bench;
}

@JsonSerializable()
class GetUserDetailData {
  GetUserDetailData();

  factory GetUserDetailData.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataToJson(this);

  int? id;
  String? username;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "display_name")
  dynamic? displayName;
  GetUserDetailDataAvatar? avatar;
  String? gender;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  String? email;
  dynamic? idcard;
  String? birthday;
  @JSONField(name: "terms_accepted")
  bool? termsAccepted;
  bool? activated;
  @JSONField(name: "activated_at")
  String? activatedAt;
  bool? blocked;
  @JSONField(name: "profile_score")
  int? profileScore;
  @JSONField(name: "last_login_at")
  String? lastLoginAt;
  @JSONField(name: "last_login_ip")
  String? lastLoginIp;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  String? referral;
  String? lang;
  GetUserDetailDataWallet? wallet;
  GetUserDetailDataGroup? group;
  GetUserDetailDataAgent? agent;
  List<GetUserDetailDataConnects>? connects;
}

@JsonSerializable()
class GetUserDetailDataAvatar {
  GetUserDetailDataAvatar();

  factory GetUserDetailDataAvatar.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataAvatarFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataAvatarToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}

@JsonSerializable()
class GetUserDetailDataWallet {
  GetUserDetailDataWallet();

  factory GetUserDetailDataWallet.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataWalletFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataWalletToJson(this);

  int? id;
  int? amount;
}

@JsonSerializable()
class GetUserDetailDataGroup {
  GetUserDetailDataGroup();

  factory GetUserDetailDataGroup.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataGroupFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataGroupToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetUserDetailDataAgent {
  GetUserDetailDataAgent();

  factory GetUserDetailDataAgent.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataAgentFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataAgentToJson(this);
}

@JsonSerializable()
class GetUserDetailDataConnects {
  GetUserDetailDataConnects();

  factory GetUserDetailDataConnects.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailDataConnectsFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailDataConnectsToJson(this);

  int? id;
  String? service;
  String? uid;
  String? account;
}

@JsonSerializable()
class GetUserDetailBench {
  GetUserDetailBench();

  factory GetUserDetailBench.fromJson(Map<String, dynamic> json) =>
      $GetUserDetailBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetUserDetailBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
