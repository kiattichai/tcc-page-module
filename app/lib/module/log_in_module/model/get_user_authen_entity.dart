import 'package:app/generated/json/get_user_authen_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetUserAuthenEntity {
  GetUserAuthenEntity();

  factory GetUserAuthenEntity.fromJson(Map<String, dynamic> json) =>
      $GetUserAuthenEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetUserAuthenEntityToJson(this);

  GetUserAuthenData? data;
  GetUserAuthenBench? bench;
}

@JsonSerializable()
class GetUserAuthenData {
  GetUserAuthenData();

  factory GetUserAuthenData.fromJson(Map<String, dynamic> json) =>
      $GetUserAuthenDataFromJson(json);

  Map<String, dynamic> toJson() => $GetUserAuthenDataToJson(this);

  @JSONField(name: "token_type")
  String? tokenType;
  @JSONField(name: "access_token")
  String? accessToken;
  @JSONField(name: "refresh_token")
  String? refreshToken;
}

@JsonSerializable()
class GetUserAuthenBench {
  GetUserAuthenBench();

  factory GetUserAuthenBench.fromJson(Map<String, dynamic> json) =>
      $GetUserAuthenBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetUserAuthenBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
