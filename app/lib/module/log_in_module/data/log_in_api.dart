import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/log_in_module/model/get_user_authen_entity.dart';
import 'package:app/module/log_in_module/model/get_user_detail_entity.dart';
import 'package:app/module/log_in_module/model/log_in_request_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class LogInApi {

  final String pathUsers = '/users';
  final String pathAuthen = '/authen';
  final String pathProfile = '/profile';

  final CoreApi api;

  LogInApi(this.api);

  Future<GetUserAuthenEntity> LogIn(String username, String password) async {
    LogInRequestEntity logInRequestEntity = new LogInRequestEntity();
    logInRequestEntity.username = username;
    logInRequestEntity.password = password;
    logInRequestEntity.deviceId = "random";
    logInRequestEntity.notificationToken = "random";
    logInRequestEntity.deviceType = "android";
    logInRequestEntity.ipAddress = "0.0.0.0";
    logInRequestEntity.countryCode = "TH";
    Response response = await api.post(
        pathUsers + pathAuthen, logInRequestEntity.toJson(), BaseErrorEntity.badRequestToModelError,);
    return GetUserAuthenEntity.fromJson(response.data);
  }

  Future<GetUserDetailEntity> getUserDetail() async {
    Response response = await api.get(
        pathUsers + pathProfile, null, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'},hasPermission: true);
    return GetUserDetailEntity.fromJson(response.data);
  }

}

