import 'package:app/core/base_repository.dart';
import 'package:app/module/log_in_module/model/get_user_authen_entity.dart';
import 'package:app/module/log_in_module/data/log_in_api.dart';
import 'package:app/module/log_in_module/model/get_user_detail_entity.dart';
import 'package:injectable/injectable.dart';


@singleton
class LogInRepository extends BaseRepository {
  final LogInApi api;

  LogInRepository(this.api);

  Future<GetUserAuthenEntity?> login(String username, String password) async {
    final result = await api.LogIn(username,password);
    return result;
  }

  Future<GetUserDetailEntity> getUserDetail() async {
    final result = await api.getUserDetail();
    return result;
  }

}