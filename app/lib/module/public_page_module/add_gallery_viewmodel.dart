import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/save_gallery_entity.dart';
import 'package:app/model/save_gallery_response_entity.dart';
import 'package:app/utils/image_picker_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as p;
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class AddGalleryViewModel extends BaseViewModel {
  String storeId;
  TextEditingController textDescription = TextEditingController();
  List<File> images = <File>[];
  List<TextEditingController> textDescriptions = <TextEditingController>[];
  List<String> description = [];

  AddGalleryViewModel(this.storeId);

  Future<void> loadAssets(BuildContext context) async {
    List<File> resultList = <File>[];
    catchError(() async {
      String error = 'No Error Detected';
      final pathImages = await ImagePickerUtils.multiImagePicker("เลือกรูปภาพ",
          currentImageInList: images.length, context: context);
      if (pathImages != null) {
        for (AssetEntity asset in pathImages) {
          final filePath = await asset.file;
          if (filePath == null) return;
          resultList.add(filePath);
        }
      }
      images = resultList;
      textDescriptions.addAll(List.generate(images.length, (index) => new TextEditingController()));
      notifyListeners();
    });
  }

  void removeImage(int index) {
    images.removeAt(index);
    textDescriptions.removeAt(index);
    notifyListeners();
  }

  void save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      for (var i = 0; i < images.length; i++) {
        final bytes = await images[i].readAsBytes();
        final b64 = base64.encode(bytes);
        SaveGalleryResponseData? responseData =
            await di.galleryRepository.saveGallery(SaveGalleryEntity()
              ..description = textDescriptions[i].text
              ..fileData = b64
              ..storeId = storeId
              ..fileName = p.basename(images[i].path));
      }
      setLoading(false);
      //TixNavigate.instance.pop(data: true);
      WrapNavigation.instance.pop(context, data: true);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
  }
}
