import 'dart:core';

import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreatePageScreen extends StatefulWidget with TixRoute {
  const CreatePageScreen({Key? key}) : super(key: key);

  @override
  _CreatePageScreen createState() => _CreatePageScreen();

  @override
  String buildPath() {
    return '/create_page';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreatePageScreen());
  }
}

class _CreatePageScreen extends State<CreatePageScreen> {
  final formKey = GlobalKey<FormState>();
  bool _passwordVisible = false;
  bool _confirmPasswordVisible = false;
  int pageNameLength = 0;
  final password = TextEditingController();
  final phone = TextEditingController();
  final email = TextEditingController();
  final address = TextEditingController();
  final pageName = TextEditingController();
  final about = TextEditingController();
  final username = TextEditingController();
  final confirmController = TextEditingController();
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(name: 'บันทึกข้อมูล'),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Form(
            key: formKey,
            child: ListView(children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.number,
                controller: phone,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                autovalidateMode: AutovalidateMode.onUserInteraction,
                autofocus: false,
                maxLength: 10,
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                    labelText: 'เบอร์โทรศัพท์', border: OutlineInputBorder(), counterText: ''),
                validator: validatePhoneNumber,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                autofocus: false,
                controller: email,
                keyboardType: TextInputType.emailAddress,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                maxLength: 100,
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                    labelText: 'อีเมล', border: OutlineInputBorder(), counterText: ''),
                validator: validateEmail,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                autofocus: false,
                controller: address,
                maxLength: 200,
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                    labelText: 'ตำแหน่งที่ตั้ง', border: OutlineInputBorder(), counterText: ''),
                validator: validateAddress,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  autofocus: false,
                  controller: pageName,
                  decoration: InputDecoration(
                      labelText: 'เพจ',
                      border: OutlineInputBorder(),
                      suffixText: "$pageNameLength / 52",
                      suffixStyle: TextStyle(color: Colors.grey),
                      counterText: ''),
                  maxLength: 52,
                  validator: validatePage,
                  onChanged: (text) {
                    setState(() {
                      pageNameLength = text.length;
                    });
                  }),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                autofocus: false,
                controller: about,
                keyboardType: TextInputType.multiline,
                maxLength: 250,
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                    alignLabelWithHint: true,
                    labelText: 'เกี่ยวกับเพจ',
                    border: OutlineInputBorder(),
                    counterText: ''),
                maxLines: 3,
                validator: validateAbout,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                autofocus: false,
                controller: username,
                maxLength: 30,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]')),
                ],
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                    labelText: 'บัญชีผู้ใช้', border: OutlineInputBorder(), counterText: ''),
                validator: validateUsername,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  autofocus: false,
                  controller: password,
                  obscureText: !_passwordVisible,
                  keyboardType: TextInputType.text,
                  validator: validatePassword,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9!@#$%^&*(),.?":{}|<>]')),
                  ],
                  maxLength: 15,
                  onChanged: (text) {
                    setState(() {
                      check();
                    });
                  },
                  decoration: InputDecoration(
                    labelText: 'รหัสผ่าน',
                    errorMaxLines: 2,
                    border: OutlineInputBorder(),
                    counterText: '',
                    suffixIcon: IconButton(
                      icon: Icon(
                        _passwordVisible ? Icons.visibility : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                  )),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                obscureText: !_confirmPasswordVisible,
                autofocus: false,
                keyboardType: TextInputType.text,
                validator: validateConfirmPassword,
                maxLength: 15,
                onChanged: (text) {
                  setState(() {
                    check();
                  });
                },
                decoration: InputDecoration(
                  labelText: 'ยืนยันรหัสผ่าน',
                  border: OutlineInputBorder(),
                  counterText: '',
                  suffixIcon: IconButton(
                    icon: Icon(
                      _confirmPasswordVisible ? Icons.visibility : Icons.visibility_off,
                    ),
                    onPressed: () {
                      setState(() {
                        _confirmPasswordVisible = !_confirmPasswordVisible;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              ElevatedButton(
                  child: Text(
                    "ถัดไป",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet',
                      color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                      shape:
                          MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(21),
                      ))),
                  onPressed: () => isActive ? _save(context) : null)
            ])),
      ),
    );
  }

  String? validatePhoneNumber(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกเบอร์โทรศัพท์';
    }
    if (value.length < 10) {
      return 'กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก';
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกรหัสผ่าน';
    }
    bool hasUppercase = value.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = value.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = value.contains(new RegExp(r'[a-z]'));
    bool hasSpecialCharacters = value.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = value.length >= 8;
    bool hasMaxLength = value.length <= 15;
    if (!(hasUppercase &&
        hasDigits &&
        hasLowercase &&
        hasSpecialCharacters &&
        hasMinLength &&
        hasMaxLength)) {
      return 'รหัสผ่านต้องมีตัวอักษรพิมพ์เล็ก,พิมพ์ใหญ่,ตัวเลข,อักขระพิเศษ อย่างน้อย 8 ไม่เกิน 15';
    }
    return null;
  }

  String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกอีเมล';
    }
    if (!EmailValidator.validate(value)) {
      return 'กรุณากรอกอีเมลให้ถูกต้อง';
    }
    return null;
  }

  String? validateAddress(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกตำแหน่งที่ตั้ง';
    }
    return null;
  }

  String? validatePage(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อเพจ';
    }
    return null;
  }

  String? validateUsername(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกบัญชีผู้ใช้';
    }
    if (value.length < 2) {
      return 'กรุณากรอกบัญชีผู้ใช้มากกว่า 2 ตัวอักษร';
    }
    return null;
  }

  String? validateAbout(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกข้อมูลเกี่ยวกับเพจ';
    }
    return null;
  }

  String? validateConfirmPassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณายืนยันรหัสผ่าน';
    }
    if (password.text != value) {
      return 'กรุณายืนยันรหัสผ่านให้ถูกต้อง';
    }
    return null;
  }

  void check() {
    isActive = (validatePhoneNumber(phone.text) == null) &&
        (validatePassword(password.text) == null) &&
        (validateEmail(email.text) == null) &&
        (validateAddress(address.text) == null) &&
        (validatePage(pageName.text) == null) &&
        (validateUsername(username.text) == null) &&
        (validateAbout(about.text) == null) &&
        (validateConfirmPassword(confirmController.text) == null);
  }

  void _save(BuildContext context) {
    WrapNavigation.instance.pop(context);
  }
}
