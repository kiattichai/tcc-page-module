import 'dart:io';

import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AddProductScreen extends StatefulWidget with TixRoute {
  const AddProductScreen({Key? key}) : super(key: key);

  @override
  _AddProductScreen createState() => _AddProductScreen();

  @override
  String buildPath() {
    return '/add_product';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => AddProductScreen());
  }
}

class _AddProductScreen extends State<AddProductScreen> {
  List<File> images = [];
  bool isActive = false;

  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        images.add(File(pickedFile.path));
        setActive();
      }
    });
  }

  void removeImage(int index) {
    setState(() {
      images.removeAt(index);
      setActive();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: "เพิ่มสินค้า",
      ),
      body: Container(
          padding: const EdgeInsets.only(top: 16, left: 17, right: 17, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 205,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: images.length + 1,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return index != (images.length)
                          ? Container(
                              padding: index != 0 ? const EdgeInsets.only(left: 10) : null,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Image.file(
                                        images[index],
                                        height: 205,
                                        width: 145,
                                        fit: BoxFit.cover,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(right: 8, top: 8),
                                        child: InkWell(
                                          child: Image(
                                            image: AssetImage('assets/remove.png'),
                                            width: 16,
                                            height: 16,
                                          ),
                                          onTap: () {
                                            removeImage(index);
                                          },
                                        ),
                                      )
                                    ],
                                  )))
                          : images.length != 10
                              ? Container(
                                  padding: index != 0 ? const EdgeInsets.only(left: 10) : null,
                                  child: InkWell(
                                    child: Container(
                                        width: 145,
                                        height: 205,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Image(
                                              width: 18,
                                              height: 18,
                                              image: AssetImage("assets/add.png"),
                                            ),
                                            SizedBox(
                                              width: 6,
                                            ),
                                            Text("เพิ่มรูปภาพ",
                                                style: TextStyle(
                                                  fontFamily: 'SukhumvitSet-Text',
                                                  color: Color(0xff4a4a4a),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                ))
                                          ],
                                        ),
                                        decoration: new BoxDecoration(
                                            border: Border.all(color: Color(0xFFE0E0E0), width: 1),
                                            color: Color(0xffffffff),
                                            borderRadius: BorderRadius.circular(6))),
                                    onTap: getImage,
                                  ))
                              : SizedBox();
                    }),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10),
                child: RichText(
                    text: new TextSpan(children: [
                  new TextSpan(
                      text: "รูปภาพ : ${images.length}/10",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      )),
                  new TextSpan(
                      text: " เลือกรูปภาพหลักของรายการสินค้าของคุณ",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff6d6d6d),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      )),
                ])),
              ),
              Spacer(),
              ConstrainedBox(
                constraints: const BoxConstraints(minWidth: double.infinity),
                child: ElevatedButton(
                    child: Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(21),
                        ))),
                    onPressed: () => isActive ? _save(context) : null),
              )
            ],
          )),
    );
  }

  void setActive() {
    if (images.length > 0) {
      isActive = true;
    } else {
      isActive = false;
    }
  }

  void _save(BuildContext context) {
    WrapNavigation.instance.pop(context);
  }
}
