import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/add_gallery_viewmodel.dart';
import 'package:app/module/public_page_module/widget/add_gallery_widget.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AddGalleryScreen extends StatefulWidget with TixRoute {
  String? storeId;

  AddGalleryScreen({this.storeId});

  @override
  _AddGalleryScreenState createState() => _AddGalleryScreenState();

  @override
  String buildPath() {
    return '/add_gallery';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => AddGalleryScreen(storeId: data));
  }
}

class _AddGalleryScreenState
    extends BaseStateProvider<AddGalleryScreen, AddGalleryViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = AddGalleryViewModel(widget.storeId!);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddGalleryViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(color: Color(0xff323232)),
              backgroundColor: Colors.white,
              title: Text(
                "เพิ่มรูป",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff323232),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.16,
                ),
              ),
              actions: [
                InkWell(
                  child: Container(
                    padding: EdgeInsets.only(right: 15.0, top: 15.0),
                    child: Text("เรียบร้อย",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color:viewModel.images.isNotEmpty ?  Color(0xFF0CAF12): Color(0xff323232),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.16,
                        )),
                  ),
                  onTap: () {
                    viewModel.images.isNotEmpty ?  viewModel.save(context):null;
                  },

                )
              ],
            ),
            body: AddGalleryWidget(viewModel),
            bottomNavigationBar: InkWell(
              onTap: () {
                viewModel.loadAssets(context);
              },
              child: Container(
                padding: EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 15.0, bottom: 19.0),
                child: Container(
                  width: 343.0,
                  height: 37.0,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(12)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.camera_alt_outlined,
                        color: Color(0xff333333),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Text("เพิ่มรูป",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff333333),
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ))
                    ],
                  ),
                ),
              ),
            ),
          ));
        },
        model: viewModel);
  }
}
