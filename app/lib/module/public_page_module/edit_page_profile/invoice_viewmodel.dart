import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/model/get_district_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/invoice_response_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class InvoiceViewModel extends BaseViewModel {
  TextEditingController company = TextEditingController();
  TextEditingController major = TextEditingController();
  TextEditingController taxId = TextEditingController();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController citizen = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController zipCode = TextEditingController();
  String? storeId;
  String provinceText = 'จังหวัด';
  String cityText = 'อำเภอ';
  String districtsText = 'ตำบล';
  GetAddressData? getAddressData;
  GetDistrictData? getDistrictData;
  GetOrganizerStoreDetailData? getOrganizerDetail;
  InvoiceResponseData? invoiceResponseData;
  List<GetAddressDataRecord> provinceList = [];
  List<GetAddressDataRecord> citiesList = [];
  List<GetDistrictDataRecord> districtList = [];

  int? provinceId;
  int? citiesId;
  int? districtId;

  bool isActive = false;
  bool validateCompany = false;
  bool validateMajor = false;
  bool validateTax = false;
  bool validateFirstName = false;
  bool validateLastName = false;
  bool validateCitizen = false;
  bool validateAddress = false;
  bool validateProvince = false;
  bool validateCities = false;
  bool validateDistrict = false;
  bool validateZipCode = false;

  InvoiceViewModel(this.storeId);

  @override
  void postInit() {
    super.postInit();
    getStoreDetail();
    getProvince();
    notifyListeners();
  }

  void getStoreDetail() async {
    catchError(() async {
      setLoading(true);
      getOrganizerDetail = await di.pageRepository.getOrganizerStore(storeId!);
      print(getOrganizerDetail?.billing?.address);
      if (getOrganizerDetail != null) {
        company.text = getOrganizerDetail?.corporate?.name ?? '';
        major.text = getOrganizerDetail?.corporate?.branch ?? '';
        taxId.text = getOrganizerDetail?.corporate?.taxId ?? '';
        firstName.text = getOrganizerDetail?.contact?.firstName ?? '';
        lastName.text = getOrganizerDetail?.contact?.lastName ?? '';
        citizen.text = getOrganizerDetail?.contact?.citizenId ?? '';
        address.text = getOrganizerDetail?.billing?.address ?? '';

        provinceId = getOrganizerDetail?.billing?.province["id"];
        provinceText = getOrganizerDetail?.billing?.province["name"];

        citiesId = getOrganizerDetail?.billing?.city["id"];
        cityText = getOrganizerDetail?.billing?.city["name"];

        districtId = getOrganizerDetail?.billing?.district["id"];
        districtsText = getOrganizerDetail?.billing?.district["name"];

        zipCode.text = getOrganizerDetail?.billing?.zipcode.toString() ?? '';
        getCities(provinceId!);
        getDistrict(citiesId!);
        setLoading(false);
        notifyListeners();
      }
    });
  }

  void provinceIndex(int index, int province_Id) {
    provinceId = province_Id;
    provinceText = provinceList[index].name?.th ?? '';
    cityText = '';
    citiesList.clear();
    districtsText = '';
    districtList.clear();
    zipCode.text = '';
    notifyListeners();

    if (province_Id != -1) {
      catchError(() async {
        setLoading(true);
        getAddressData = await di.getAddressRepository.getCities(province_Id);
        if (getAddressData != null) {
          citiesList.addAll(getAddressData?.record ?? []);
        }
        setLoading(false);
      });
    }
  }

  void cityIndex(int index, int cityId) {
    print(index);
    print(cityId);
    citiesId = cityId;
    cityText = citiesList[index].name?.th ?? '';
    districtsText = '';
    zipCode.text = '';
    districtList.clear();

    notifyListeners();
    if (cityId != -1) {
      catchError(() async {
        setLoading(true);
        getDistrictData = await di.getAddressRepository.getDistrict(cityId);
        if (getDistrictData != null) {
          districtList.addAll(getDistrictData?.record ?? []);
        }
        setLoading(false);
      });
    }
  }

  void districtIndex(int index, int district_Id) {
    districtId = district_Id;
    districtsText = districtList[index].name?.th ?? '';
    zipCode.text = districtList[index].zipCode!;
    checkText();
    notifyListeners();
  }

  void getProvince() async {
    catchError(() async {
      setLoading(true);
      getAddressData = await di.getAddressRepository.getProvince(209);
      if (getAddressData?.record != null) {
        provinceList.clear();
        provinceList.addAll(getAddressData?.record ?? []);
      }
      setLoading(false);
      notifyListeners();
    });
  }

  void getCities(int provinceId) {
    print(provinceId);
    catchError(() async {
      setLoading(true);
      getAddressData = await di.getAddressRepository.getCities(provinceId);
      if (getAddressData != null) {
        citiesList.clear();
        citiesList.addAll(getAddressData?.record ?? []);
      }
      setLoading(false);
      notifyListeners();
    });
  }

  void getDistrict(int cityId) {
    catchError(() async {
      setLoading(true);
      getDistrictData = await di.getAddressRepository.getDistrict(cityId);
      if (getDistrictData != null) {
        districtList.clear();
        districtList.addAll(getDistrictData?.record ?? []);
      }
      setLoading(false);
      notifyListeners();
    });
  }

  void checkText() {
    if (company.text.isNotEmpty &&
        major.text.isNotEmpty &&
        taxId.text.isNotEmpty &&
        firstName.text.isNotEmpty &&
        lastName.text.isNotEmpty &&
        citizen.text.isNotEmpty &&
        address.text.isNotEmpty &&
        zipCode.text.isNotEmpty &&
        provinceText != 'จังหวัด' &&
        provinceText != '' &&
        cityText != 'อำเภอ' &&
        cityText != '' &&
        districtsText != 'ตำบล' &&
        districtsText != '') {
      isActive = true;
    } else {
      isActive = false;
    }
    notifyListeners();
  }

  void updateInvoice(BuildContext context) {
    Map params = {
      "address": address.text,
      "citizen_id": "${citizen.text}",
      "city_id": "${citiesId.toString()}",
      "corporate_branch": major.text,
      "corporate_name": company.text,
      "country_id": 209,
      "district_id": "${districtId.toString()}",
      "first_name": firstName.text,
      "last_name": lastName.text,
      "province_id": "${provinceId.toString()}",
      "store_id": 5000810,
      "tax_id": "${taxId.text}",
      "type": "corporate",
      "zipcode": "${zipCode.text}"
    };
    catchError(() async {
      setLoading(true);
      invoiceResponseData = await di.getAddressRepository.updateInvoice(params);
      if (invoiceResponseData != null) {
        WrapNavigation.instance.pop(context);
      }
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
  }
}
