import 'package:app/core/base_view_model.dart';
import 'package:app/model/publish_page_entity.dart';

class EditPublishPageViewModel extends BaseViewModel{

  final String storeId;
  bool? status;

  EditPublishPageViewModel(this.storeId,this.status);

  setStatus(bool status){
    this.status = status;
    publishPage();
  }

  publishPage() {
    catchError(() async {
      setLoading(true);
      PublishPageEntity publishPageEntity = PublishPageEntity()..status = this.status
        ..storeId = storeId;
      final result = await di.pageRepository.savePublish(publishPageEntity);

      setLoading(false);
    });
  }


  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}