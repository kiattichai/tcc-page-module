import 'package:app/module/public_page_module/edit_page_profile/document/document_ticket_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/result_dialog.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_gallery_image_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_profile_page.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_publish_page.dart';
import 'package:app/module/public_page_module/edit_page_profile/invoice_screen.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditProfilePageMenu extends StatelessWidget with TixRoute {
  String? storeId;
  bool? status;

  EditProfilePageMenu({this.storeId, this.status});

  @override
  String buildPath() {
    return '/edit_profile_page_menu_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditProfilePageMenu(
              storeId: data['storeId'],
              status: data['status'],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'จัดการเพจโปรไฟล์',
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 16, right: 16, top: 5),
          child: Column(children: [
            menu('assets/info.png', 'แก้ไขข้อมูลเพจ', navigateToEditProfile(context)),
            menu('assets/camera.png', 'ภาพแกลเลอรี่', navigateToEditGalleryImage(context)),
            menu('assets/doc.png', 'เอกสารเพื่อเปิดขายบัตร', navigateToDocumentTicket(context)),
            menu('assets/bill.png', 'ใบวางบิล', navigateToInvoiceScreen(context)),
            menu('assets/list_settings_fill.png', 'ทั่วไป', navigateToEditPublish(context)),
          ]),
        ),
      ),
    );
  }

  menu(String icon, String name, Function()? onTap) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.only(top: 11.5),
        child: Column(
          children: [
            Row(
              children: [
                Image(
                  image: AssetImage(icon),
                  color: Color(0xff333333),
                  width: 16,
                  height: 16,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 19),
                  child: Text(name, style: textStyle()),
                )
              ],
            ),
            Container(
              width: Screen.width - 67,
              margin: const EdgeInsets.only(left: 35, top: 11.5),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(width: 1, color: Color(0xFFDDDDDD)))),
            )
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  navigateToEditProfile(BuildContext context) {
    //TixNavigate.instance.navigateTo(EditProfilePage(), data: {'storeId': storeId});
    WrapNavigation.instance.pushNamed(context, EditProfilePage(), arguments: {'storeId': storeId});
  }

  navigateToDocumentTicket(BuildContext context) async {
    //TixNavigate.instance.navigateTo(DocumentTicketScreen(), data: {'storeId': storeId});
    WrapNavigation.instance
        .pushNamed(context, DocumentTicketScreen(), arguments: {'storeId': storeId});
  }

  navigateToEditGalleryImage(BuildContext context) {
    //TixNavigate.instance.navigateTo(EditGalleryImageScreen(), data: {'storeId': storeId});
    WrapNavigation.instance
        .pushNamed(context, EditGalleryImageScreen(), arguments: {'storeId': storeId});
  }

  navigateToInvoiceScreen(BuildContext context) {
    //TixNavigate.instance.navigateTo(InvoiceScreen(), data: {'storeId': storeId});
    WrapNavigation.instance.pushNamed(context, InvoiceScreen(), arguments: {'storeId': storeId});
  }

  navigateToEditPublish(BuildContext context) {
    //TixNavigate.instance.navigateTo(EditPublishPage(), data: {'storeId': storeId, 'status': status});
    WrapNavigation.instance
        .pushNamed(context, EditPublishPage(), arguments: {'storeId': storeId, 'status': status});
  }

  textStyle() {
    return TextStyle(
        fontFamily: 'SukhumvitSet-Text',
        color: Color(0xff333333),
        fontSize: 16,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        letterSpacing: -0.5);
  }
}
