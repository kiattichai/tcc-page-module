import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_genre_attribute_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class SelectGenreScreenViewModel extends BaseViewModel {
  GetGenreAttributeDataRecord? getGenreAttributeDataRecord;

  List<GenreSelect> genreSelect = [];
  List<SaveEditProfilePageAttributes> attributes = [];
  bool isActive = false;

  SelectGenreScreenViewModel(List<SaveEditProfilePageAttributes> attributes) {
    this.attributes.addAll(attributes);
  }

  @override
  void postInit() {
    super.postInit();
    getGenre();
  }

  getGenre() {
    catchError(() async {
      setLoading(true);
      getGenreAttributeDataRecord = await di.pageRepository.getGenre();
      genreSelect.addAll(getGenreAttributeDataRecord?.values?.map((e) {
            if (attributes.where((element) => element.valueId == e.id).isNotEmpty) {
              return new GenreSelect(e.id, true, e);
            } else {
              return new GenreSelect(e.id, false, e);
            }
          }).toList() ??
          []);
      check();
      setLoading(false);
    });
  }

  check() {
    isActive = genreSelect.where((element) => element.checked).isNotEmpty;
    notifyListeners();
  }

  selectGenre(int index) {
    genreSelect[index].checked = !genreSelect[index].checked;
    check();
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }

  save(BuildContext context) {
    List<SaveEditProfilePageAttributes> attributes =
        genreSelect.where((element) => element.checked == true).map((e) {
      return SaveEditProfilePageAttributes()
        ..id = getGenreAttributeDataRecord?.id
        ..name = e.value?.name
        ..valueId = e.value?.id;
    }).toList();

    //TixNavigate.instance.pop(data: attributes);
    WrapNavigation.instance.pop(context, data: attributes);
  }
}

class GenreSelect {
  int? id;
  bool checked;
  GetGenreAttributeDataRecordValues? value;

  GenreSelect(this.id, this.checked, this.value);
}
