import 'package:app/model/get_document_ticket_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/image_dialog.dart';
import 'package:flutter/material.dart';

class FormUploadDocument extends StatelessWidget {
  final String title;
  final String? fileName;
  final String? url;
  final String? documentStatus;
  final Function()? pickImage;
  final GetDocumentTicketDataRecord? record;

  const FormUploadDocument(
      {Key? key,
      required this.title,
      this.fileName,
      this.url,
      this.documentStatus,
      this.pickImage,
      this.record})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool rejectStatus = record?.status?.id == 2;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
          child: new Text(
            title,
            style: TextStyle(
              fontFamily: 'sukhumvitSet-Text',
              color: Color(0xff545454),
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        record == null
            ? InkWell(
                onTap: () {
                  pickImage!();
                },
                child: Container(
                  height: 57,
                  margin: EdgeInsets.only(left: 16.0, right: 16.0),
                  decoration: new BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                        style: BorderStyle.solid,
                        color: fileName != null ? Colors.green : Colors.grey, width: 0.5),
                  ),
                  child: fileName != null
                      ? Center(
                          child: Text(
                            fileName ?? '',
                            style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Colors.green,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        )
                      : Center(
                          child: Icon(
                            Icons.upload_sharp,
                            color: Colors.grey,
                          ),
                        ),
                ),
              )
            : Container(
                margin: EdgeInsets.only(left: 16.0, right: 16.0),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () async {
                        await showDialog(
                            context: context,
                            builder: (_) => ImageDialog(record?.images?[0].url ?? ''));
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Text(
                              record?.images?[0].name ?? '',
                              style: TextStyle(fontFamily: 'sukhumvitSet-Text', color: Colors.blue),
                            ),
                            Icon(
                              Icons.image_search,
                              color: Colors.blue,
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text(
                            'Document Status : ',
                            style: TextStyle(fontFamily: 'sukhumvitSet-text'),
                          ),
                          Text(
                            record?.status?.text ?? '',
                            style: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: record?.status?.id == 0
                                    ? Colors.orangeAccent
                                    : record?.status?.id == 1
                                        ? Colors.green
                                        : Colors.red,
                                fontWeight: FontWeight.w700),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
        rejectStatus
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: Row(
                      children: [
                        Text(
                          'Remark : ',
                          style: TextStyle(fontFamily: 'sukhumvitSet-text'),
                        ),
                        Text(
                          record?.remark ?? '',
                          style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Colors.red,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      pickImage!();
                    },
                    child: Container(
                      width: 335,
                      height: 49,
                      margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 4),
                      decoration: new BoxDecoration(
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            style: BorderStyle.solid,
                            color: fileName != null ? Colors.green : Colors.grey),
                      ),
                      child: fileName != null
                          ? Center(
                              child: Text(
                                fileName ?? '',
                                style: TextStyle(
                                  fontFamily: 'sukhumvitSet-Text',
                                  color: Colors.green,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            )
                          : Center(
                              child: Icon(
                                Icons.upload_sharp,
                                color: Colors.grey,
                              ),
                            ),
                    ),
                  )
                ],
              )
            : SizedBox()
      ],
    );
  }
}
