import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_document_ticket_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_bank_item.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_ticket_viewmodel.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/form_upload_document.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/image_dialog.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/result_dialog.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class DocumentTicketScreen extends StatefulWidget with TixRoute {
  final String? storeId;

  DocumentTicketScreen({Key? key, this.storeId}) : super(key: key);

  @override
  _DocumentTicketScreenState createState() => _DocumentTicketScreenState();

  @override
  String buildPath() {
    return '/document_ticker';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => DocumentTicketScreen(
              storeId: data['storeId'],
            ));
  }
}

class _DocumentTicketScreenState
    extends BaseStateProvider<DocumentTicketScreen, DocumentTicketViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = DocumentTicketViewModel(widget.storeId);
    viewModel.showResult = showResult;
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  labelStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff545454),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  hintStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DocumentTicketViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.red),
                title: new Text(
                  "เอกสารเพื่อเปิดขายบัตร",
                  style: TextStyle(
                    fontFamily: 'sukhumvitSet-Text',
                    color: Color(0xff010101),
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      viewModel.documentUpload.isEmpty
                          ? Container()
                          : viewModel.storeType == "corporate"
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    //TODO อัพโหลดหนังสือรับรองบริษัท
                                    FormUploadDocument(
                                      title:
                                          'Certificate of Company Registration / หนังสือรองรับการจดทะเบียนบริษัท',
                                      pickImage: () => viewModel.picImage(0),
                                      fileName: viewModel.documentUpload[0].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[0].record,
                                    ),

                                    //TODO อัพโหลดบัตรประจำตัวประชาชน
                                    FormUploadDocument(
                                      title: 'Director ID Cards / บัตรประจำตัวประชาชน',
                                      pickImage: () => viewModel.picImage(1),
                                      fileName: viewModel.documentUpload[1].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[1].record,
                                    ),

                                    //TODO อัพโหลด ภพ.20
                                    FormUploadDocument(
                                      title: 'Certificate of VAT Registration / ภพ.20',
                                      pickImage: () => viewModel.picImage(2),
                                      fileName: viewModel.documentUpload[2].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[2].record,
                                    ),

                                    //TODO อัพโหลดบัญชีธนาคาร
                                    FormUploadDocument(
                                      title: 'Book bank / บัญชีธนาคาร',
                                      pickImage: () => viewModel.picImage(3),
                                      fileName: viewModel.documentUpload[3].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[3].record,
                                    ),
                                  ],
                                )
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    //TODO อัพโหลดบัตรประจำตัวประชาชน record bock-bank มาก่อน
                                    FormUploadDocument(
                                      title: 'ID Cards / บัตรประจำตัวประชาชน',
                                      pickImage: () => viewModel.picImage(0),
                                      fileName: viewModel.documentUpload[0].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[0].record,
                                    ),

                                    //TODO อัพโหลดบัตรประจำตัวประชาชน
                                    FormUploadDocument(
                                      title: 'Book bank / บัญชีธนาคาร',
                                      pickImage: () => viewModel.picImage(1),
                                      fileName: viewModel.documentUpload[1].imageName,
                                      record: viewModel.documentUpload.isEmpty
                                          ? null
                                          : viewModel.documentUpload[1].record,
                                    ),
                                  ],
                                ),

                      //TODO เลือกบัญชีธนาคาร
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: new Text(
                          "Bank",
                          style: TextStyle(
                            fontFamily: 'sukhumvitSet-Text',
                            color: Color(0xff545454),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          if (viewModel.isUploaded == false) {
                            Map map = {
                              'title': 'เลือกบัญชีธนาคาร',
                              'item': viewModel.getPaymentList,
                              'selected': viewModel.bankSelected
                            };
                            //final result = await TixNavigate.instance.navigateTo(DocumentBankItem(), data: map);
                            final result = await WrapNavigation.instance
                                .pushNamed(context, DocumentBankItem(), arguments: map);
                            if (result is Map && result['id'] != -1) {
                              viewModel.setIdBank(result['id']);
                            }
                          }
                        },
                        child: Container(
                          height: 57,
                          margin: EdgeInsets.only(left: 16.0, right: 16.0),
                          decoration: new BoxDecoration(
                            color: Colors.grey.shade50,
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey.shade400, width: 0.5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.0),
                                child: Text(
                                  viewModel.bankText,
                                  style: textStyle(),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(Icons.keyboard_arrow_down_outlined),
                              ),
                            ],
                          ),
                        ),
                      ),

                      //TODO ชื่อบัญชี
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.bankName,
                            enabled: !viewModel.isUploaded,
                            keyboardType: TextInputType.text,
                            inputFormatters: <TextInputFormatter>[],
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                return null;
                              } else {
                                return 'กรุณาป้อนชื่อบัญชี';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkValidate();
                            },
                            style: textStyle(),
                            decoration: InputDecoration(
                              hintText: 'ชื่อบัญชี',
                              labelText: 'ชื่อบัญชี',
                              contentPadding: EdgeInsets.all(15.0),
                              labelStyle: labelStyle(),
                              hintStyle: hintStyle(),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      //TODO ประเภทบัญชี
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: new Text(
                          "Account Type",
                          style: TextStyle(
                            fontFamily: 'sukhumvitSet-Text',
                            color: Color(0xff545454),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          if (viewModel.isUploaded == false) {
                            Map map = {
                              'title': 'เลือกประเภทบัญชี',
                              'item': viewModel.bankType,
                              'selected': viewModel.bankTypeSelected
                            };
                            //final result = await TixNavigate.instance.navigateTo(DocumentBankItem(), data: map);
                            final result = await WrapNavigation.instance
                                .pushNamed(context, DocumentBankItem(), arguments: map);
                            if (result is Map && result['id'] != -1) {
                              viewModel.setIdType(result['id']);
                            }
                          }
                        },
                        child: Container(
                          height: 57,
                          margin: EdgeInsets.only(left: 16.0, right: 16.0),
                          decoration: new BoxDecoration(
                            color: Colors.grey.shade50,
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey.shade400, width: 0.5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.0),
                                child: Text(
                                  viewModel.bankTypeText,
                                  style: textStyle(),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(Icons.keyboard_arrow_down_outlined),
                              ),
                            ],
                          ),
                        ),
                      ),

                      //TODO หมายเลขบัญชี
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.bankNumber,
                            enabled: !viewModel.isUploaded,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            ],
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                return null;
                              } else {
                                return 'กรุณาป้อนหมายเลขบัญชี';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkValidate();
                            },
                            style: textStyle(),
                            decoration: InputDecoration(
                              hintText: 'หมายเลขบัญชี',
                              labelText: 'หมายเลขบัญชี',
                              contentPadding: EdgeInsets.all(15.0),
                              labelStyle: labelStyle(),
                              hintStyle: hintStyle(),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      //TODO สาขา
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0, bottom: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.branch,
                            enabled: !viewModel.isUploaded,
                            keyboardType: TextInputType.text,
                            inputFormatters: <TextInputFormatter>[],
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                return null;
                              } else {
                                return 'กรุณาป้อนสาขา';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkValidate();
                            },
                            style: textStyle(),
                            decoration: InputDecoration(
                              hintText: 'สาขา',
                              labelText: 'สาขา',
                              contentPadding: EdgeInsets.all(15.0),
                              labelStyle: labelStyle(),
                              hintStyle: hintStyle(),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: InkWell(
                onTap: () {
                  if (viewModel.isActive == true) {
                    viewModel.handleUploadForm(context);
                  }
                },
                child: Container(
                  width: 343,
                  height: 42,
                  margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
                  decoration: new BoxDecoration(
                      color: viewModel.isActive == false ? Color(0xff989898) : Color(0xffda3534),
                      borderRadius: BorderRadius.circular(21)),
                  child: Center(
                    child: new Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xffffffff),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
        model: viewModel);
  }

  void showResult() async {
    return showDialog(context: context, builder: (_) => ResultDialog());
  }
}
