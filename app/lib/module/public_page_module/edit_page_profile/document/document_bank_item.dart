import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_payment_bank_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

class DocumentBankItem extends StatefulWidget with TixRoute {
  Map<String, dynamic>? map;
  DocumentBankItem({Key? key, this.map}) : super(key: key);

  @override
  _DocumentBankItemState createState() => _DocumentBankItemState();

  @override
  String buildPath() {
    return '/document_bank';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => DocumentBankItem(
              map: data,
            ));
  }
}

class _DocumentBankItemState
    extends BaseStateProvider<DocumentBankItem, DocumentBankItemViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = DocumentBankItemViewModel(widget.map);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DocumentBankItemViewModel>(
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.red),
              title: new Text(
                "${widget.map?['title']}",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff323232),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.16,
                ),
              ),
            ),
            body: bodyContent(),
            bottomNavigationBar: InkWell(
              child: Container(
                width: 343,
                height: 42,
                margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
                decoration: new BoxDecoration(
                    color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                child: Center(
                  child: new Text(
                    "บันทึก",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
              onTap: () {
                Map data = {};
                if (widget.map?['title'] == 'เลือกบัญชีธนาคาร') {
                  data = {
                    'id': viewModel.bankSelected,
                  };
                } else {
                  data = {
                    'id': viewModel.bankTypeSelected,
                  };
                }

                //TixNavigate.instance.pop(data: data);
                WrapNavigation.instance.pop(context, data: data);
              },
            ),
          );
        },
        model: viewModel);
  }

  bodyContent() {
    return Container(
        child: widget.map?['title'] == 'เลือกบัญชีธนาคาร'
            ? ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: viewModel.paymentList.length,
                itemBuilder: (context, index) {
                  var item = viewModel.paymentList[index];
                  return ListTile(
                    title: Text(
                      '${item.name}',
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    trailing: viewModel.bankSelected != -1 && viewModel.bankSelected == item.id
                        ? Icon(
                            Icons.check,
                            color: Colors.green,
                          )
                        : SizedBox(),
                    onTap: () {
                      viewModel.selectItem(item.id ?? -1);
                      // viewModel.setIndex(index);
                    },
                  );
                })
            : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: viewModel.typeList.length,
                itemBuilder: (context, index) {
                  var item = viewModel.typeList[index];
                  return item != ''
                      ? ListTile(
                          title: Text(
                            '${item}',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          trailing: viewModel.bankTypeSelected != -1 &&
                                  index == viewModel.bankTypeSelected
                              ? Icon(
                                  Icons.check,
                                  color: Colors.green,
                                )
                              : SizedBox(),
                          onTap: () {
                            print(index);
                            viewModel.selectItem(index);
                            // viewModel.setIndex(index);
                          },
                        )
                      : SizedBox();
                }));
  }
}

class DocumentBankItemViewModel extends BaseViewModel {
  Map? map;
  int? bankSelected = -1;
  int? bankTypeSelected = -1;
  List<GetPaymentBankDataRecord> paymentList = [];
  List<String> typeList = [];
  DocumentBankItemViewModel(this.map);
  @override
  void postInit() {
    super.postInit();
    sortItem();
  }

  void sortItem() {
    if (map?['title'] == 'เลือกบัญชีธนาคาร') {
      paymentList.addAll(map?['item']);
      bankSelected = map?['selected'];
    } else {
      typeList.addAll(map?['item']);
      bankTypeSelected = map?['selected'];
      print(bankTypeSelected);
    }
    notifyListeners();
  }

  void selectItem(int id) {
    bankSelected = id;
    bankTypeSelected = id;
    notifyListeners();
  }
}
