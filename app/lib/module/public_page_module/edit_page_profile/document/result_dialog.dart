import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ResultDialog extends StatelessWidget with TixRoute {
  @override
  String buildPath() {
    return '/result_dialog';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => ResultDialog());
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Dialog(
      insetPadding: EdgeInsets.only(left: 16.0, right: 8.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      backgroundColor: Colors.transparent,
      child: customDialog(context),
    );
  }

  Widget customDialog(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: 11.0,
              ),
              margin: EdgeInsets.only(top: 13.0, right: 8.0),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(7.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Image.asset(
                        'assets/check_circle.png',
                        scale: 2,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20.0, top: 16.0, right: 20.0),
                    child: Center(
                      child: Text(
                        'ทำรายการสำเร็จ',
                        style: TextStyle(
                            fontFamily: 'sukhumvitSet-Text',
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      WrapNavigation.instance.pop(context);
                    },
                    child: Center(
                      child: new Container(
                        width: 285,
                        height: 40,
                        margin: EdgeInsets.only(top: 18.0, bottom: 23.0),
                        decoration: new BoxDecoration(
                            color: Color(0xffda3534), borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: new Text(
                            "ปิด",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xffffffff),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0.0,
              child: GestureDetector(
                onTap: () {
                  WrapNavigation.instance.pop(context);
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: CircleAvatar(
                    radius: 14.0,
                    backgroundColor: Colors.white,
                    child: Icon(Icons.close, color: Colors.black),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
