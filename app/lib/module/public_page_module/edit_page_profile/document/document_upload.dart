import 'dart:io';

import 'package:app/model/get_document_ticket_entity.dart';

class DocumentUpload  {
  GetDocumentTicketDataRecord? record;
  File? image;
  String? imageName;
  String? storeType;
  String? fileType;

  DocumentUpload(this.record,this.image,this.imageName,this.storeType, this.fileType);

  bool reject() {
    if (record?.status?.id == 2) {
      return true;
    }else{
      return false;
    }
  }
}