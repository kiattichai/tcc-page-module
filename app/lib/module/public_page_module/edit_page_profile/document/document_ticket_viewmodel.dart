import 'dart:convert';
import 'dart:io';
import 'package:app/model/Response_upload_bank_entity.dart';
import 'package:app/model/get_bank_data_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/response_image_upload_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_upload.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_document_ticket_entity.dart';
import 'package:app/model/get_payment_bank_entity.dart';
import 'package:image_picker/image_picker.dart';

class DocumentTicketViewModel extends BaseViewModel {
  String? storeId;
  GetOrganizerStoreDetailData? storeDetail;
  TextEditingController bankName = TextEditingController();
  TextEditingController bankNumber = TextEditingController();
  TextEditingController branch = TextEditingController();
  String? storeType;
  int bankSelected = -1;
  int bankTypeSelected = -1;
  String bankText = 'เลือกบัญชีธนาคาร';
  String bankTypeText = 'เลือกประเภทบัญชี';
  bool isActive = false;
  bool isUploaded = false;
  GetDocumentTicketData? getDocument;
  GetPaymentBankData? getPayment;
  GetBankDataData? getBank;
  List<String> bankType = ['', 'saving', 'current'];
  List<GetBankDataDataRecord> bankRecord = [];
  List<GetPaymentBankDataRecord> getPaymentList = [];
  ResponseImageUploadData? responseData;
  ResponseUploadBankData? responseBankData;
  List<DocumentUpload> documentUpload = [];
  List<String> sortCorporate = ['company', 'director_id_cards', 'certificate', 'book_bank'];
  List<String> sortIndividual = ['id_card', 'book_bank'];
  DocumentTicketViewModel(this.storeId);

  @override
  void postInit() {
    super.postInit();
    getBankList();
    getBankData();
    getOganizer();
  }

  Function()? showResult;

  void getOganizer() {
    catchError(() async {
      setLoading(true);
      storeDetail = await di.pageRepository.getOrganizerStore(storeId!);
      if (storeDetail != null) {
        storeType = storeDetail?.type?.text;
      }
      getDocumentData();
      setLoading(false);
    });
  }

  void getDocumentData() async {
    catchError(() async {
      setLoading(true);
      getDocument = await di.pageRepository.getDocument(storeId!);
      if (getDocument != null && getDocument?.record != null) {
        if (getDocument?.record?.length != 0) {
          isUploaded = true;
          getDocument?.record?.forEach((document) {
            String? name;
            if (document.status?.id == 2) {
              name = null;
            } else {
              name = document.images![0].name;
            }
            DocumentUpload doc =
                DocumentUpload(document, null, name, storeType, document.fileType ?? '');
            documentUpload.add(doc);
            print('-----------------------------------------------------------');
            print(doc.fileType);
          });
        } else {
          isUploaded = false;
          if (storeType == 'corporate') {
            documentUpload = List<DocumentUpload>.generate(4, (index) {
              return DocumentUpload(null, null, null, storeType, sortCorporate[index]);
            });
          } else {
            documentUpload = List<DocumentUpload>.generate(2, (index) {
              print(index);
              return DocumentUpload(null, null, null, storeType, sortIndividual[index]);
            });
          }
        }
      }
      print(storeType);
      print(documentUpload.length);
      sortDocument();
      setLoading(false);
    });
  }

  void sortDocument() {
    if (storeType == 'corporate') {
      List<DocumentUpload> docUpload = [];
      for (int i = 0; i < sortCorporate.length; i++) {
        String fType = sortCorporate[i];
        DocumentUpload doc = documentUpload.firstWhere((element) {
          return element.fileType == fType;
        });
        docUpload.add(doc);
      }
      documentUpload.clear();
      documentUpload.addAll(docUpload);
    } else {
      List<DocumentUpload> docUpload = [];
      for (int i = 0; i < sortIndividual.length; i++) {
        String fType = sortIndividual[i];
        DocumentUpload? doc = documentUpload.firstWhere((element) {
          return element.fileType == fType;
        });
        docUpload.add(doc);
      }
      documentUpload.clear();
      documentUpload.addAll(docUpload);
    }
    notifyListeners();
  }

  void getBankList() async {
    catchError(() async {
      setLoading(true);
      getPayment = await di.pageRepository.getPayment();
      if (getPayment != null && getPayment?.record != null) {
        getPaymentList.addAll(getPayment?.record ?? []);
      }
      setLoading(false);
    });
  }

  void getBankData() {
    catchError(() async {
      setLoading(true);
      getBank = await di.pageRepository.getBankData(storeId!);
      if (getBank != null && getBank?.record != []) {
        bankRecord.addAll(getBank?.record ?? []);

        bankSelected = bankRecord[0].paymentBank?.id ?? -1;
        bankText = bankRecord[0].paymentBank?.name ?? '';

        bankName.text = bankRecord[0].account?.name ?? '';

        bankTypeSelected = bankRecord[0].account?.type?.id ?? -1;
        bankTypeText = bankRecord[0].account?.type?.text ?? '';

        bankNumber.text = bankRecord[0].account?.number ?? '';
        branch.text = bankRecord[0].account?.branch ?? '';
      }
      setLoading(false);
      notifyListeners();
    });
  }

  void picImage(int index) async {
    print(index);
    try {
      PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
      documentUpload[index].image = File(pickedFile!.path);
      documentUpload[index].imageName = pickedFile.path.split('/').last;
      checkValidate();
    } catch (err) {
      print(err);
    }
    notifyListeners();
  }

  void setIdBank(int id) {
    print(id);
    bankSelected = id;
    bankText = getPaymentList[id - 1].name ?? '';
    notifyListeners();
    checkValidate();
  }

  void setIdType(int id) {
    print(id);
    bankTypeSelected = id;
    bankTypeText = bankType[id];
    notifyListeners();
    checkValidate();
  }

  upLoadCorporate(BuildContext context) async {
    //TODO upload image
    for (int i = 0; i < documentUpload.length; i++) {
      DocumentUpload doc = documentUpload[i];
      String title = '';
      String type = '';
      String? id;
      String? fileType;
      var bytes;

      if (doc.image == null) continue;
      if (i == 0 && doc != null) {
        bytes = await doc.image!.readAsBytes();
        title = 'Certificate of Company Registration / หนังสือรองรับการจดทะเบียนบริษัท';
        type = 'corporate';
        fileType = 'company';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }
      if (i == 1 && doc != null) {
        bytes = await doc.image!.readAsBytes();
        title = 'Director ID Cards / บัตรประจำตัวประชาชน';
        type = 'corporate';
        fileType = 'director_id_cards';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }
      if (i == 2) {
        bytes = await doc.image!.readAsBytes();
        title = 'Certificate of VAT Registration / ภพ.20';
        type = 'corporate';
        fileType = 'certificate';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }
      if (i == 3 && doc != null) {
        bytes = await doc.image!.readAsBytes();
        title = 'Book bank / บัญชีธนาคาร';
        type = 'book-bank';
        fileType = 'book_bank';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }
      if (bytes == null) continue;
      final b64 = base64Encode(bytes);

      Map params = {
        'files': [
          {'data': '${b64}', 'name': '${doc.imageName}'}
        ],
        'id': id,
        'store_id': "${storeId}",
        'title': "${title}",
        'file_type': "${fileType}",
        'type': "${type}",
      };
      print('-----------------------------------------------------------');
      print(params['file_type']);
      catchError(() async {
        setLoading(true);
        responseData = await di.pageRepository.upLoadImage(params);
        if (responseData != null) {}
        setLoading(false);
      });
    }
    //TODO upload bank
    catchError(() async {
      Map params = {
        'account_name': "${bankName.text}",
        'account_no': "${bankNumber.text}",
        'account_type': "${bankTypeText.toString()}",
        'branch': "${branch.text}",
        'payment_bank_id': bankSelected,
        'store_id': "${storeId}",
        'type': "account",
      };
      setLoading(true);
      responseBankData = await di.pageRepository.upLoadBank(params);
      if (responseBankData != null) {
        WrapNavigation.instance.pop(context);
        showResult!();
      }
      setLoading(false);
    });
  }

  upLoadIndividual(BuildContext context) async {
    for (int i = 0; i < documentUpload.length; i++) {
      DocumentUpload doc = documentUpload[i];
      String title = '';
      String type = '';
      String? fileType;
      String? id;
      var bytes;

      if (doc.image == null) continue;
      if (i == 0) {
        bytes = await doc.image!.readAsBytes();
        title = 'ID Cards / บัตรประจำตัวประชาชน';
        type = 'individual';
        fileType = 'id_card';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }
      if (i == 1) {
        bytes = await doc.image!.readAsBytes();
        title = 'Book bank / บัญชีธนาคาร';
        type = 'book-bank';
        fileType = 'book_bank';
        id = doc.record != null ? doc.record?.images![0].id : null;
      }

      final b64 = base64Encode(bytes);
      Map params = {
        'files': [
          {'data': '${b64}', 'name': '${doc.imageName}'}
        ],
        'id': id,
        'store_id': "${storeId}",
        'title': "${title}",
        'file_type': "${fileType}",
        'type': "${type}",
      };
      print('-----------------------------------------------------------');
      print(params['file_type']);
      catchError(() async {
        setLoading(true);
        responseData = await di.pageRepository.upLoadImage(params);
        setLoading(false);
      });
    }
    //TODO upload bank
    catchError(() async {
      Map params = {
        'account_name': "${bankName.text}",
        'account_no': "${bankNumber.text}",
        'account_type': "${bankTypeText.toString()}",
        'branch': "${branch.text}",
        'payment_bank_id': bankSelected,
        'store_id': "${storeId}",
        'type': "account",
      };
      setLoading(true);
      responseBankData = await di.pageRepository.upLoadBank(params);
      if (responseBankData != null) {
        WrapNavigation.instance.pop(context);
        showResult!();
      }
      setLoading(false);
    });
  }

  void checkValidate() {
    if (storeType == 'corporate') {
      if (bankSelected != -1 &&
          bankTypeSelected != -1 &&
          bankNumber.text.isNotEmpty &&
          branch.text.isNotEmpty &&
          bankName.text.isNotEmpty &&
          documentUpload[0].imageName != null &&
          documentUpload[1].imageName != null &&
          documentUpload[2].imageName != null &&
          documentUpload[3].imageName != null) {
        isActive = true;
      } else {
        isActive = false;
      }
    } else {
      //TODO Individual
      if (bankSelected != -1 &&
          bankTypeSelected != -1 &&
          bankNumber.text.isNotEmpty &&
          branch.text.isNotEmpty &&
          bankName.text.isNotEmpty &&
          documentUpload[0].imageName != null &&
          documentUpload[1].imageName != null) {
        isActive = true;
      } else {
        isActive = false;
      }
    }
    notifyListeners();
  }

  handleUploadForm(BuildContext context) {
    if (storeType == 'corporate') {
      upLoadCorporate(context);
    } else {
      upLoadIndividual(context);
    }
  }

  @override
  void onError(error) {
    super.onError(error);
  }
}
