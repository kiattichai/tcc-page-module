import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CitiesListScreen extends StatefulWidget with TixRoute {
  int? citiesId;
  List<GetAddressDataRecord>? citiesList;
  CitiesListScreen({Key? key, this.citiesId, this.citiesList}) : super(key: key);

  @override
  _CitiesListScreenState createState() => _CitiesListScreenState();

  @override
  String buildPath() {
    return "/cities_screen";
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => CitiesListScreen(
              citiesId: data['selected'],
              citiesList: data['citiesList'],
            ));
  }
}

class _CitiesListScreenState extends BaseStateProvider<CitiesListScreen, CitiesViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = CitiesViewModel(widget.citiesList, widget.citiesId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CitiesViewModel>(
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.red),
              title: new Text(
                "เลือกอำเภอ",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff4a4a4a),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            body: bodyContent(),
            bottomNavigationBar: InkWell(
              child: Container(
                width: 343,
                height: 42,
                margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
                decoration: new BoxDecoration(
                    color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                child: Center(
                  child: new Text(
                    "บันทึก",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
              onTap: () {
                Map data = {
                  "index": viewModel.selectedIndex,
                  "id": viewModel.citiesId,
                };
                //TixNavigate.instance.pop(data: data);
                WrapNavigation.instance.pop(context, data: data);
              },
            ),
          );
        },
        model: viewModel);
  }

  bodyContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: widget.citiesList?.length,
              itemBuilder: (context, index) {
                var item = viewModel.citiesList?[index];
                return ListTile(
                  title: Text(
                    '${item?.name?.th}',
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  trailing: viewModel.citiesId != -1 && viewModel.citiesId == item!.id
                      ? Icon(
                          Icons.check,
                          color: Colors.green,
                        )
                      : SizedBox(),
                  onTap: () {
                    viewModel.selectItem(item?.id ?? -1);
                    viewModel.setIndex(index);
                  },
                );
              }),
        ],
      ),
    );
  }
}

class CitiesViewModel extends BaseViewModel {
  List<GetAddressDataRecord>? citiesList;
  int? citiesId = -1;
  int? selectedIndex = -1;

  CitiesViewModel(
    this.citiesList,
    this.citiesId,
  );

  void selectItem(int id) {
    citiesId = id;
  }

  void setIndex(int index) {
    selectedIndex = index;
    notifyListeners();
  }
}
