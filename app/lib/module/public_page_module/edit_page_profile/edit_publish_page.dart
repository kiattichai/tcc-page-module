import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_publish_page_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditPublishPage extends StatefulWidget with TixRoute {
  String? storeId;
  bool? status;

  EditPublishPage({this.storeId, this.status});

  @override
  _EditPublishPage createState() => _EditPublishPage();

  @override
  String buildPath() {
    return '/edit_publish_page';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditPublishPage(
              storeId: data['storeId'],
              status: data['status'],
            ));
  }
}

class _EditPublishPage extends BaseStateProvider<EditPublishPage, EditPublishPageViewModel> {
  @override
  void initState() {
    viewModel = EditPublishPageViewModel(widget.storeId!, widget.status);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EditPublishPageViewModel>(
      builder: (context, model, child) {
        return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBarWidget(
                name: 'ทั่วไป',
              ),
              body: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(top: 18, right: 18, left: 18),
                  height: 249,
                  decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.circular(6),
                      border: Border.all(width: 1, color: Color(0xFFE0E0E0))),
                  child: Column(
                    children: [
                      Container(
                        height: 39,
                        decoration: new BoxDecoration(
                            color: Color(0xfff1f2f7),
                            border: Border(bottom: BorderSide(width: 1, color: Color(0xFFE0E0E0)))),
                        child: Align(
                          child: Padding(
                            child: Text(
                              "การแสดงเพจ",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            padding: const EdgeInsets.only(left: 16, right: 16),
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: Column(
                          children: [
                            new Text(
                                "เมื่อคุณเผยแพร่เพจ เพจจะแสดงต่อสาธารณะ แต่หากคุณเลือกที่จะเลิกเผยแพร่เพจ เพจจะแสดงให้เห็นเฉพาะผู้ที่มีบทบาทบนเพจเท่านั้น",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                )),
                            InkWell(
                              child: Container(
                                padding: const EdgeInsets.only(top: 12, bottom: 12),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(width: 1, color: Color(0xFFDDDDDD)))),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 21,
                                    ),
                                    Text("เผยแพร่",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    Spacer(),
                                    viewModel.status == true
                                        ? Image(
                                            image: AssetImage('assets/check.png'),
                                            width: 17,
                                            height: 12,
                                            color: Color(0xFF3FA73C),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                              onTap: () {
                                viewModel.setStatus(true);
                              },
                            ),
                            InkWell(
                              child: Container(
                                padding: const EdgeInsets.only(top: 12, bottom: 12),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(width: 1, color: Color(0xFFDDDDDD)))),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 21,
                                    ),
                                    Text("เลิกเผยแพร่",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    Spacer(),
                                    viewModel.status == false
                                        ? Image(
                                            image: AssetImage('assets/check.png'),
                                            width: 17,
                                            height: 12,
                                            color: Color(0xFF3FA73C),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                              onTap: () {
                                viewModel.setStatus(false);
                              },
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ));
      },
      model: viewModel,
    );
  }
}
