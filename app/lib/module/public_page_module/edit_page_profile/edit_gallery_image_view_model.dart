import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_organizer_store_banner_entity.dart';
import 'package:app/model/save_banner_bulk_entity.dart';
import 'package:app/model/uploads_banner_entity.dart';
import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

class EditGalleryViewModel extends BaseViewModel {
  final String storeId;
  GetOrganizerStoreBannerData? image;
  File? bannerImageFile;

  EditGalleryViewModel(this.storeId);

  @override
  void postInit() {
    super.postInit();
    getBanner();
  }

  getBanner() {
    catchError(() async {
      setLoading(true);
      image = await di.pageRepository.getBanner(storeId);
      setLoading(false);
    });
  }

  void pickBannerImage() {
    catchError(() async {
      final pickedImage =
          await ImagePicker().getImage(source: ImageSource.gallery);
      bannerImageFile = pickedImage != null ? File(pickedImage.path) : null;
      if (bannerImageFile != null) {
        cropImage();
      }
    });
  }

  void cropImage() {
    catchError(() async {
      File? croppedFile = await ImageCropper.cropImage(
          sourcePath: bannerImageFile!.path,
          aspectRatioPresets: [],
          aspectRatio: CropAspectRatio(ratioX: 16, ratioY: 9),
          androidUiSettings: AndroidUiSettings(
              hideBottomControls: true,
              toolbarTitle: 'ครอบตัด',
              toolbarColor: Colors.white,
              toolbarWidgetColor: Colors.black,
              lockAspectRatio: true),
          iosUiSettings: IOSUiSettings(
              title: 'ครอบตัด',
              aspectRatioLockEnabled: true,
              aspectRatioPickerButtonHidden: true));
      if (croppedFile != null) {
        bannerImageFile = croppedFile;
        uploadBanner();
      }
    });
  }

  void uploadBanner() {
    catchError(() async {
      setLoading(true);
      if (bannerImageFile != null) {
        final bytes = await bannerImageFile!.readAsBytes();
        final b64 = base64.encode(bytes);
        UploadsBannerEntity uploadsBannerEntity = UploadsBannerEntity()
          ..storeId = storeId
          ..files = [
            UploadsBannerFiles()
              ..position = 0
              ..name = p.basename(bannerImageFile!.path)
              ..data = b64
          ];
        await di.pageRepository.uploadBanner(uploadsBannerEntity);
        getBanner();
      }
      setLoading(false);
    });
  }

  void deleteBanner(String id) {
    catchError(() async {
      setLoading(false);
      final result = await di.pageRepository.deleteBanner(storeId, id);
      if(result!=null){
        getBanner();
      }
      setLoading(true);
    });
  }

  void saveBulk(){
    catchError(() async {
      List<SaveBannerBulkItems> items = List.generate(image?.images?.banner?.length ?? 0, (index) {
        return SaveBannerBulkItems()
          ..position = index
            ..attachmentId = image?.images?.banner![index].id!;
      });

    final save =    SaveBannerBulkEntity()
      ..fileableId = storeId
      ..fileableType = 'stores'
      ..items = items;
      final result = await di.pageRepository.saveBannerBulk(save);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }

  void updatePosition(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final  item = image?.images?.banner!.removeAt(oldIndex);
    image?.images?.banner!.insert(newIndex, item!);
    notifyListeners();
    saveBulk();
  }
}
