import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_organizer_store_banner_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_gallery_image_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditGalleryImageScreen extends StatefulWidget with TixRoute {
  String? storeId;

  EditGalleryImageScreen({this.storeId});

  @override
  _EditGalleryImageScreen createState() => _EditGalleryImageScreen();

  @override
  String buildPath() {
    return '/edit_gallery_image_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditGalleryImageScreen(storeId: data['storeId']));
  }
}

class _EditGalleryImageScreen
    extends BaseStateProvider<EditGalleryImageScreen, EditGalleryViewModel> {
  @override
  void initState() {
    viewModel = EditGalleryViewModel(widget.storeId!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EditGalleryViewModel>(
      builder: (context, model, child) {
        return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
                appBar: AppBarWidget(
                  name: 'รูปภาพ',
                ),
                body: SingleChildScrollView(
                  child: Column(children: [
                    Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: Text(
                          "ขนาดภาพแบนเนอร์ที่เหมาะสม ควรมีขนาด \n940 x 358 และเป็นภาพ .png หรือ .jpgเท่านั้น",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
                    Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: InkWell(
                          child: Container(
                              width: double.infinity,
                              height: 100,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    width: 18,
                                    height: 18,
                                    image: AssetImage("assets/add.png"),
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text("เพิ่มรูปภาพ",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff4a4a4a),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ))
                                ],
                              ),
                              decoration: new BoxDecoration(
                                  border: Border.all(color: Color(0xFFE0E0E0), width: 1),
                                  color: Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(6))),
                          onTap: () => viewModel.pickBannerImage(),
                        )),
                    SizedBox(
                      height: 16,
                    ),
                    ReorderableListView(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      children: viewModel.image?.images?.banner
                              ?.map((e) => _BodyWidget(
                                    e,
                                    viewModel.deleteBanner,
                                    key: ValueKey(e.id!),
                                  ))
                              .toList() ??
                          [],
                      onReorder: viewModel.updatePosition,
                    ),
                  ]),
                )));
      },
      model: viewModel,
    );
  }
}

class _BodyWidget extends StatelessWidget {
  GetOrganizerStoreBannerDataImagesBanner banner;
  Function(String) delete;
  final Key? key;

  _BodyWidget(this.banner, this.delete, {this.key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: key,
      height: 100,
      padding: const EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(6)),
                image: DecorationImage(image: NetworkImage(banner.url!), fit: BoxFit.cover)),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
                padding: const EdgeInsets.only(right: 6, top: 6),
                child: InkWell(
                  child: Image(
                    image: AssetImage('assets/remove.png'),
                    width: 16,
                    height: 16,
                  ),
                  onTap: () async {
                    bool result = await showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomDialog(
                            title: "ลบรูปภาพ",
                            description: 'ยืนยันลบรูปภาพนี้หรือไม่ ?',
                            dangerButtonText: 'ยกเลิก',
                            buttonText: 'ตกลง',
                          );
                        });
                    if (result == true) {
                      delete(banner.id!);
                    }
                  },
                )),
          )
        ],
      ),
    );
  }
}
