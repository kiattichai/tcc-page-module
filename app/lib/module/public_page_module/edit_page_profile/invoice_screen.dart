import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/edit_page_profile/cities_list_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/district_list_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/invoice_viewmodel.dart';
import 'package:app/module/public_page_module/edit_page_profile/province_list_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class InvoiceScreen extends StatefulWidget with TixRoute {
  String? storeId;
  InvoiceScreen({Key? key, this.storeId}) : super(key: key);

  @override
  _InvoiceScreenState createState() => _InvoiceScreenState();

  @override
  String buildPath() {
    return '/invoice';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => InvoiceScreen(
              storeId: data['storeId'],
            ));
  }
}

class _InvoiceScreenState extends BaseStateProvider<InvoiceScreen, InvoiceViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = InvoiceViewModel(widget.storeId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<InvoiceViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Color(0xffda3534)),
                title: new Text(
                  "ใบวางบิล",
                  style: TextStyle(
                    fontFamily: 'sukhumvitSet-Text',
                    color: Color(0xff4a4a4a),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0),
                        child: new Text(
                          "เพิ่มรายละเอียดใบวางบิล กรุณาใช้ข้อมูลที่ถูกต้อง",
                          style: TextStyle(
                            fontFamily: 'sukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      //TODO ชื่อบริษัท
                      viewModel.getOrganizerDetail?.type?.id == 2
                          ? Container(
                              margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                              child: Form(
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                child: TextFormField(
                                  controller: viewModel.company,
                                  validator: (value) {
                                    if (value!.isNotEmpty) {
                                      viewModel.validateCompany = true;
                                      return null;
                                    } else {
                                      viewModel.validateCompany = false;
                                      return 'กรุณาป้อนชื่อบริษัท';
                                    }
                                  },
                                  onChanged: (value) {
                                    viewModel.checkText();
                                  },
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'ชื่อบริษัท',
                                    labelText: 'ชื่อบริษัท',
                                    labelStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    hintStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),

                      //TODO ชื่อสาขา/รหัสสาขา
                      viewModel.getOrganizerDetail?.type?.id == 2
                          ? Container(
                              margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                              child: Form(
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                child: TextFormField(
                                  controller: viewModel.major,
                                  validator: (value) {
                                    if (value!.isNotEmpty) {
                                      viewModel.validateMajor = true;
                                      return null;
                                    } else {
                                      viewModel.validateMajor = false;
                                      return 'กรุณาป้อนชื่อสาขา/รหัสสาขา';
                                    }
                                  },
                                  onChanged: (value) {
                                    viewModel.checkText();
                                  },
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'ชื่อสาขา/รหัสสาขา',
                                    labelText: 'ชื่อสาขา/รหัสสาขา',
                                    labelStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    hintStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),

                      //TODO หมายเลขประจำตัวผู้เสียภาษี
                      viewModel.getOrganizerDetail?.type?.id == 2
                          ? Container(
                              margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                              child: Form(
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                child: TextFormField(
                                  controller: viewModel.taxId,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                  ],
                                  validator: (value) {
                                    if (value!.isNotEmpty) {
                                      viewModel.validateTax = true;
                                      return null;
                                    } else {
                                      viewModel.validateTax = false;
                                      return 'กรุณาป้อนหมายเลขประจำตัวผู้เสียภาษี';
                                    }
                                  },
                                  onChanged: (value) {
                                    viewModel.checkText();
                                  },
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'หมายเลขประจำตัวผู้เสียภาษี',
                                    labelText: 'หมายเลขประจำตัวผู้เสียภาษี',
                                    labelStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    hintStyle: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(6.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      //TODO ชื่อ
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.firstName,
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                viewModel.validateFirstName = true;
                                return null;
                              } else {
                                viewModel.validateFirstName = false;
                                return 'กรุณาป้อนชื่อ';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkText();
                            },
                            style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            decoration: InputDecoration(
                              hintText: 'ชื่อ',
                              labelText: 'ชื่อ',
                              labelStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              hintStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //TODO นามสกุล
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.lastName,
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                viewModel.validateLastName = true;
                                return null;
                              } else {
                                viewModel.validateLastName = false;
                                return 'กรุณาป้อนนามสกุล';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkText();
                            },
                            style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            decoration: InputDecoration(
                              hintText: 'นามสกุล',
                              labelText: 'นามสกุล',
                              labelStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              hintStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //TODO รหัสประจำตัวประชาชน
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.citizen,
                            keyboardType: TextInputType.number,
                            maxLength: 13,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              LengthLimitingTextInputFormatter(13)
                            ],
                            validator: (value) {
                              if (value!.isEmpty || value.length < 13) {
                                viewModel.validateCitizen = false;
                                return 'กรุณาป้อนรหัสประจำตัวประชาชน';
                              } else {
                                viewModel.validateCitizen = true;

                                return null;
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkText();
                            },
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            decoration: InputDecoration(
                              hintText: 'รหัสประจำตัวประชาชน',
                              labelText: 'รหัสประจำตัวประชาชน',
                              labelStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              hintStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //TODO ที่อยู่
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.address,
                            minLines: 2,
                            maxLines: 2,
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                viewModel.validateAddress = true;
                                return null;
                              } else {
                                viewModel.validateAddress = false;
                                return 'กรุณาป้อนที่อยู่';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkText();
                            },
                            style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            decoration: InputDecoration(
                              hintText: 'ที่อยู่',
                              labelText: 'ที่อยู่',
                              alignLabelWithHint: true,
                              labelStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              hintStyle: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //TODO จังหวัด
                      InkWell(
                        onTap: () async {
                          //final result = await TixNavigate.instance.navigateTo(ProvinceListScreen(), data: {'selected': viewModel.provinceId, 'provinceList': viewModel.provinceList});
                          final result = await WrapNavigation.instance.pushNamed(
                              context, ProvinceListScreen(), arguments: {
                            'selected': viewModel.provinceId,
                            'provinceList': viewModel.provinceList
                          });
                          if (result is Map) {
                            print(result);
                            viewModel.provinceIndex(result["index"], result["id"]);
                          }
                        },
                        child: Container(
                          width: 343,
                          height: 57,
                          margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.0),
                                child: Text(
                                  "${viewModel.provinceText}",
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(Icons.keyboard_arrow_down_outlined),
                              ),
                            ],
                          ),
                        ),
                      ),
                      //TODO อำเภอ
                      InkWell(
                        onTap: () async {
                          //final result = await TixNavigate.instance.navigateTo(CitiesListScreen(), data: {'selected': viewModel.citiesId, 'citiesList': viewModel.citiesList});
                          final result = await WrapNavigation.instance.pushNamed(
                              context, CitiesListScreen(), arguments: {
                            'selected': viewModel.citiesId,
                            'citiesList': viewModel.citiesList
                          });
                          print(result);
                          if (result is Map) {
                            viewModel.cityIndex(result["index"], result["id"]);
                          }
                        },
                        child: Container(
                          width: 343,
                          height: 57,
                          margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.0),
                                child: Text(
                                  "${viewModel.cityText}",
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(Icons.keyboard_arrow_down_outlined),
                              ),
                            ],
                          ),
                        ),
                      ),
                      //TODO ตำบล
                      InkWell(
                        onTap: () async {
                          print('ตำบล');
                          // final result = await TixNavigate.instance.navigateTo(DistrictListScreen(), data: {'selected': viewModel.districtId, 'districtList': viewModel.districtList});
                          final result = await WrapNavigation.instance.pushNamed(
                              context, DistrictListScreen(), arguments: {
                            'selected': viewModel.districtId,
                            'districtList': viewModel.districtList
                          });
                          if (result is Map) {
                            viewModel.districtIndex(result["index"], result["id"]);
                          }
                        },
                        child: Container(
                          width: 343,
                          height: 57,
                          margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.0),
                                child: Text(
                                  "${viewModel.districtsText}",
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xff6d6d6d),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(Icons.keyboard_arrow_down_outlined),
                              ),
                            ],
                          ),
                        ),
                      ),
                      //TODO รหัสไปรษณีย์
                      Container(
                        margin: EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            controller: viewModel.zipCode,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            ],
                            validator: (value) {
                              if (value!.isNotEmpty) {
                                viewModel.validateZipCode = true;
                                return null;
                              } else {
                                viewModel.validateZipCode = false;
                                return 'กรุณาป้อนรหัสไปรษณีย์';
                              }
                            },
                            onChanged: (value) {
                              viewModel.checkText();
                            },
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            decoration: InputDecoration(
                              hintText: 'รหัสไปรษณีย์',
                              labelText: 'รหัสไปรษณีย์',
                              labelStyle: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              hintStyle: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6.0),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      //TODO button
                      viewModel.isActive == false
                          ? Container(
                              width: 343,
                              height: 42,
                              margin:
                                  EdgeInsets.only(left: 16.0, top: 35.0, right: 16.0, bottom: 24.0),
                              decoration: new BoxDecoration(
                                  color: Color(0xffe6e7ec),
                                  borderRadius: BorderRadius.circular(21)),
                              child: Center(
                                child: Text(
                                  "บันทึก",
                                  style: TextStyle(
                                    fontFamily: 'sukhumvitSet-Text',
                                    color: Color(0xffb2b2b2),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            )
                          : InkWell(
                              onTap: () {
                                viewModel.updateInvoice(context);
                              },
                              child: Container(
                                width: 343,
                                height: 42,
                                margin: EdgeInsets.only(
                                    left: 16.0, top: 35.0, right: 16.0, bottom: 24.0),
                                decoration: new BoxDecoration(
                                    color: Color(0xffda3534),
                                    borderRadius: BorderRadius.circular(21)),
                                child: Center(
                                  child: Text(
                                    "บันทึก",
                                    style: TextStyle(
                                      fontFamily: 'sukhumvitSet-Text',
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
        model: viewModel);
  }
}
