import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ProvinceListScreen extends StatefulWidget with TixRoute {
  List<GetAddressDataRecord>? provinceList;
  int? provinceId;
  ProvinceListScreen({Key? key, this.provinceId, this.provinceList}) : super(key: key);

  @override
  _ProvinceListScreenState createState() => _ProvinceListScreenState();

  @override
  String buildPath() {
    return '/province_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => ProvinceListScreen(
              provinceId: data['selected'],
              provinceList: data['provinceList'],
            ));
  }
}

class _ProvinceListScreenState extends BaseStateProvider<ProvinceListScreen, ProvinceViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = ProvinceViewModel(widget.provinceList, widget.provinceId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProvinceViewModel>(
        builder: (context, model, child) {
          return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.red),
                title: new Text(
                  "เลือกจังหวัด",
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff4a4a4a),
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              body: bodyContent(),
              bottomNavigationBar: InkWell(
                child: Container(
                  width: 343,
                  height: 42,
                  margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
                  decoration: new BoxDecoration(
                      color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                  child: Center(
                    child: new Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xffffffff),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  Map data = {"index": viewModel.selectedIndex, "id": viewModel.provinceId};
                  //TixNavigate.instance.pop(data: data);
                  WrapNavigation.instance.pop(context, data: data);
                },
              ));
        },
        model: viewModel);
  }

  bodyContent() {
    return viewModel.provinceList != null
        ? SingleChildScrollView(
            child: Column(
              children: [
                ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.provinceList?.length,
                    itemBuilder: (context, index) {
                      var item = viewModel.provinceList?[index];
                      return ListTile(
                        title: Text(
                          '${item?.name?.th}',
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        trailing: viewModel.provinceId != -1 && viewModel.provinceId == item!.id
                            ? Icon(
                                Icons.check,
                                color: Colors.green,
                              )
                            : SizedBox(),
                        onTap: () {
                          viewModel.selectedItem(item?.id ?? -1);
                          viewModel.setIndex(index);
                        },
                      );
                    })
              ],
            ),
          )
        : SizedBox();
  }
}

class ProvinceViewModel extends BaseViewModel {
  List<GetAddressDataRecord>? provinceList;
  int? provinceId = -1;
  int? selectedIndex = -1;
  ProvinceViewModel(this.provinceList, this.provinceId);

  @override
  void postInit() {
    super.postInit();
  }

  selectedItem(int id) {
    print(id);
    provinceId = id;
    notifyListeners();
  }

  setIndex(int index) {
    print(index);
    selectedIndex = index;
  }
}
