import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/constants.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_profile_page_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditProfilePage extends StatefulWidget with TixRoute {
  String? storeId;

  EditProfilePage({this.storeId});

  @override
  _EditProfilePage createState() => _EditProfilePage();

  @override
  String buildPath() {
    return '/edit_profile_page_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => EditProfilePage(storeId: data['storeId']));
  }
}

class _EditProfilePage extends BaseStateProvider<EditProfilePage, EditProfilePageViewModel> {
  @override
  void initState() {
    viewModel = EditProfilePageViewModel(widget.storeId!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var countryDropDown = Container(
      alignment: Alignment.centerLeft,
      decoration: new BoxDecoration(
        border: Border(
          right: BorderSide(width: 0.5, color: Colors.grey),
        ),
      ),
      width: 170,
      child: Text(HOST_URL,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
          style: textStyle()),
    );

    return BaseWidget<EditProfilePageViewModel>(
      builder: (context, model, child) {
        return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBarWidget(name: 'แก้ไขข้อมูลเพจ'),
              body: SafeArea(
                  child: CustomScrollView(slivers: [
                title("ข้อมูลทั่วไป"),
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autofocus: false,
                      style: textStyle(),
                      controller: viewModel.pageName,
                      validator: viewModel.validatePage,
                      decoration: InputDecoration(
                          labelText: 'ชื่อเพจ',
                          border: OutlineInputBorder(),
                          isDense: true,
                          labelStyle: textStyle(),
                          suffixText: "${viewModel.pageNameLength} / 50",
                          suffixStyle: suffixTextStyle(),
                          counterText: ''),
                      maxLength: 50,
                      onChanged: (text) {
                        viewModel.check();
                        viewModel.setPageNameLength(text.length);
                      }),
                )),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                        child: DropdownButtonFormField(
                          value: [
                            'บุคคลธรรมดา',
                            'นิติบุคคล'
                          ][(viewModel.storeDetail?.type?.id ?? 2) - 1],
                          style: textStyle(),
                          decoration: InputDecoration(
                            labelText: 'ประเภทธุรกิจ',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                          ),
                          isDense: true,
                          onChanged: (String? newValue) {
                            if (newValue == 'บุคคลธรรมดา') {
                              viewModel.setType('individual');
                            } else {
                              viewModel.setType('corporate');
                            }

                            viewModel.check();
                          },
                          items: <String>['บุคคลธรรมดา', 'นิติบุคคล']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ))),
                SliverToBoxAdapter(
                  child: Container(
                    padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: new TextFormField(
                      onChanged: (text) {
                        viewModel.check();
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autofocus: false,
                      maxLength: 50,
                      controller: viewModel.url,
                      validator: viewModel.validateUrl,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]')),
                      ],
                      decoration: new InputDecoration(
                          border: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: const Color(0xFFE0E0E0), width: 0.1)),
                          fillColor: Colors.white,
                          prefixIcon: countryDropDown,
                          hintText: '5000809',
                          hintStyle: textStyle(),
                          labelText: 'URL Page'),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                    onChanged: (text) {
                      viewModel.check();
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    style: textStyle(),
                    controller: viewModel.about,
                    maxLines: 3,
                    decoration: InputDecoration(
                      labelText: 'เกี่ยวกับ',
                      border: OutlineInputBorder(),
                      isDense: true,
                      alignLabelWithHint: true,
                      hintText: 'เขียนสั้นๆเกี่ยวกับเพจของคุณ',
                      hintStyle: textStyle(),
                      labelStyle: textStyle(),
                    ),
                  ),
                )),
                viewModel.storeDetail?.section?.id == 2
                    ? SliverToBoxAdapter(
                        child: Container(
                            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                            child: TextFormField(
                              onChanged: (text) {
                                viewModel.check();
                              },
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              autofocus: false,
                              style: textStyle(),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              maxLength: 10,
                              controller: viewModel.park,
                              decoration: InputDecoration(
                                  labelText: 'จำนวนที่จอดรถ',
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  border: OutlineInputBorder(),
                                  counterText: ''),
                            )))
                    : SliverToBoxAdapter(
                        child: Container(
                        child: SizedBox(),
                      )),
                viewModel.storeDetail?.section?.id == 2
                    ? SliverToBoxAdapter(
                        child: Container(
                            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                            child: TextFormField(
                              onChanged: (text) {
                                viewModel.check();
                              },
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              autofocus: false,
                              style: textStyle(),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              maxLength: 15,
                              controller: viewModel.seat,
                              decoration: InputDecoration(
                                  labelText: 'จำนวนที่นั่ง',
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  border: OutlineInputBorder(),
                                  counterText: ''),
                            )))
                    : SliverToBoxAdapter(
                        child: Container(
                        child: SizedBox(),
                      )),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          controller: viewModel.genre,
                          autofocus: false,
                          readOnly: true,
                          style: textStyle(),
                          onTap: () => viewModel.navigateToSelectGenre(context),
                          decoration: InputDecoration(
                              labelText: 'เลือกแนวเพลง',
                              isDense: true,
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                size: 25,
                                color: Colors.black,
                              ),
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                title("ข้อมูลทั่วไป"),
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                    onChanged: (text) {
                      viewModel.check();
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: viewModel.phone,
                    autofocus: false,
                    style: textStyle(),
                    validator: viewModel.validatePhoneNumber,
                    maxLength: 10,
                    decoration: InputDecoration(
                      labelText: 'เบอร์โทรศัพท์',
                      border: OutlineInputBorder(),
                      counterText: '',
                      isDense: true,
                      labelStyle: textStyle(),
                    ),
                  ),
                )),
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9!@#$%^&*(),.?":{}|<>]')),
                    ],
                    onChanged: (text) {
                      viewModel.check();
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    style: textStyle(),
                    controller: viewModel.email,
                    validator: viewModel.validateEmail,
                    decoration: InputDecoration(
                      labelText: 'อีเมล',
                      border: OutlineInputBorder(),
                      isDense: true,
                      labelStyle: textStyle(),
                    ),
                  ),
                )),
                title("ตำแหน่งที่ตั้ง"),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          controller: viewModel.address,
                          maxLength: 200,
                          style: textStyle(),
                          readOnly: true,
                          validator: viewModel.validateAddress,
                          onTap: () => viewModel.navigateMap(context),
                          decoration: InputDecoration(
                              labelText: 'ตำแหน่งที่ตั้ง',
                              suffixIcon: Icon(
                                Icons.location_on,
                                size: 25,
                              ),
                              isDense: true,
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                    onChanged: (text) {
                      viewModel.check();
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    style: textStyle(),
                    controller: viewModel.addressDetail,
                    validator: viewModel.validateAddressDetail,
                    maxLines: 2,
                    decoration: InputDecoration(
                      labelText: 'รายละเอียดที่อยู่',
                      border: OutlineInputBorder(),
                      isDense: true,
                      alignLabelWithHint: true,
                      hintText: 'ที่อยู่(อาคาร,ตึก,ซอย,ถนน)',
                      hintStyle: textStyle(),
                      labelStyle: textStyle(),
                    ),
                  ),
                )),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          controller: viewModel.province,
                          style: textStyle(),
                          readOnly: true,
                          decoration: InputDecoration(
                              labelText: 'จังหวัด',
                              isDense: true,
                              enabled: false,
                              filled: true,
                              fillColor: Color(0xFFE9ECEF),
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          controller: viewModel.city,
                          autofocus: false,
                          style: textStyle(),
                          readOnly: true,
                          decoration: InputDecoration(
                              labelText: 'อำเภอ',
                              enabled: false,
                              isDense: true,
                              filled: true,
                              fillColor: Color(0xFFE9ECEF),
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          controller: viewModel.district,
                          style: textStyle(),
                          readOnly: true,
                          decoration: InputDecoration(
                              enabled: false,
                              labelText: 'ตำบล',
                              isDense: true,
                              fillColor: Color(0xFFE9ECEF),
                              filled: true,
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          onChanged: (text) {
                            viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          keyboardType: TextInputType.number,
                          controller: viewModel.zipcode,
                          style: textStyle(),
                          maxLength: 5,
                          validator: viewModel.validateZipCode,
                          decoration: InputDecoration(
                              labelText: 'รหัสไปรษณีย์',
                              isDense: true,
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                viewModel.storeDetail?.section?.id == 2
                    ? title("เวลาทำการ")
                    : SliverToBoxAdapter(
                        child: Container(
                        child: SizedBox(),
                      )),
                viewModel.storeDetail?.section?.id == 2
                    ? SliverToBoxAdapter(
                        child: Container(
                        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          style: textStyle(),
                          onTap: () => viewModel.navigateToEditTime(context),
                          readOnly: true,
                          controller: viewModel.timeController,
                          decoration: InputDecoration(
                              labelText: 'เวลาทำการ',
                              isDense: true,
                              suffixIcon: Icon(
                                Icons.more_time,
                                size: 25,
                              ),
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ),
                      ))
                    : SliverToBoxAdapter(
                        child: Container(
                        child: SizedBox(),
                      )),
                viewModel.storeDetail?.section?.id == 2
                    ? SliverToBoxAdapter(
                        child: Container(
                        padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
                        child: Column(
                            children: viewModel.times?.map((e) {
                                  return Row(
                                    children: [
                                      Text(
                                        "${DAY_THAI[e.day! - 1]}",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      Spacer(),
                                      Text(
                                        "${e.open} - ${e.close}",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      )
                                    ],
                                  );
                                }).toList() ??
                                []),
                      ))
                    : SliverToBoxAdapter(
                        child: Container(
                        child: SizedBox(),
                      )),
                SliverFillRemaining(
                    hasScrollBody: false,
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                          width: double.infinity,
                          height: 42,
                          child: ElevatedButton(
                              child: Text(
                                "เรียบร้อย",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: viewModel.isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              style: ButtonStyle(
                                  foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                  backgroundColor: MaterialStateProperty.all<Color>(
                                      viewModel.isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(21),
                                  ))),
                              onPressed: () => viewModel.isActive ? viewModel.save() : null),
                        )))
              ])),
            ));
      },
      model: viewModel,
    );
  }

  suffixTextStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 14,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
    );
  }

  title(String title) {
    return SliverToBoxAdapter(
        child: Container(
            padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
            child: new Text(title,
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff08080a),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ))));
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
