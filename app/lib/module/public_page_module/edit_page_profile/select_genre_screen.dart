import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/select_genre_screen_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class SelectGenreScreen extends StatefulWidget with TixRoute {
  List<SaveEditProfilePageAttributes>? attributes;

  SelectGenreScreen({this.attributes});

  @override
  _SelectGenreScreen createState() => _SelectGenreScreen();

  @override
  String buildPath() {
    return '/select_genre_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => SelectGenreScreen(attributes: data));
  }
}

class _SelectGenreScreen extends BaseStateProvider<SelectGenreScreen, SelectGenreScreenViewModel> {
  @override
  void initState() {
    viewModel = SelectGenreScreenViewModel(widget.attributes ?? []);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectGenreScreenViewModel>(
      builder: (context, model, child) {
        return ProgressHUD(
            opacity: 1.0,
            color: Colors.white,
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.loading,
            child: Scaffold(
              appBar: AppBarWidget(
                name: 'เลือกแนวเพลง',
              ),
              body: SafeArea(
                  child: CustomScrollView(slivers: [
                ...List.generate(viewModel.getGenreAttributeDataRecord?.values?.length ?? 0,
                    (index) {
                  return checkGenre(
                      viewModel.getGenreAttributeDataRecord?.values?[index].name ?? '', index);
                }),
              ])),
              bottomNavigationBar: Container(
                margin: const EdgeInsets.only(
                    top: 5, left: 16, right: 16, bottom: 5),
                width: double.infinity,
                height: 42,
                child: ElevatedButton(
                    child: Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: viewModel.isActive
                            ? Color(0xffffffff)
                            : Color(0xffb2b2b2),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor:
                        MaterialStateProperty.all<Color>(
                            Colors.white),
                        backgroundColor: MaterialStateProperty
                            .all<Color>(viewModel.isActive
                            ? Color(0xffda3534)
                            : Color(0xffe6e7ec)),
                        shape:
                        MaterialStateProperty.all<
                            RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius
                                  .circular(21),
                            ))),
                    onPressed: () =>  viewModel.isActive ? viewModel.save(context)  : null),
              ),
            ));
      },
      model: viewModel,
    );
  }

  Widget checkGenre(String name, int index) {
    return SliverToBoxAdapter(
        child: Container(
            padding: const EdgeInsets.only(top: 13, right: 16, left: 16, bottom: 13),
            child: InkWell(
              child: Row(
                children: [
                  Image(
                    image: AssetImage('assets/music.png'),
                    width: 16,
                    height: 15,
                  ),
                  SizedBox(
                    width: 19,
                  ),
                  Text(name,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      )),
                  Spacer(),
                  viewModel.genreSelect[index].checked
                      ? Image(
                          image: AssetImage('assets/check.png'),
                          width: 17,
                          height: 12,
                          color: Color(0xFF3FA73C),
                        )
                      : SizedBox()
                ],
              ),
              onTap: () => viewModel.selectGenre(index),
            )));
  }
}
