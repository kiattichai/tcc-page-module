import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/model/save_venue_entity.dart';
import 'package:app/module/create_page_module/map_info.dart';
import 'package:app/module/create_page_module/model/get_locally_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_time_open.dart';
import 'package:app/module/public_page_module/edit_page_profile/select_genre_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditProfilePageViewModel extends BaseViewModel {
  String storeId; //"5000090"
  GetOrganizerStoreDetailData? storeDetail;
  GetLocallyData? getLocallyData;
  bool isActive = false;
  List<SaveEditProfilePageAttributes> attributes = [];
  List<SaveEditProfilePageTimes>? times;

  TextEditingController pageName = TextEditingController();
  TextEditingController url = TextEditingController();
  TextEditingController about = TextEditingController();
  TextEditingController park = TextEditingController();
  TextEditingController seat = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController province = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController district = TextEditingController();
  TextEditingController zipcode = TextEditingController();
  TextEditingController addressDetail = TextEditingController();
  TextEditingController genre = TextEditingController();
  TextEditingController email = TextEditingController();

  LatLng? latLng;
  int? cityId;
  int? countryId;
  int? districtId;
  TextEditingController address = TextEditingController();
  TextEditingController timeController = TextEditingController();

  int pageNameLength = 0;
  String? type;

  @override
  void postInit() {
    super.postInit();
    getOrganizerStoreDetail();
  }

  EditProfilePageViewModel(this.storeId);

  void getOrganizerStoreDetail() {
    catchError(() async {
      setLoading(true);
      storeDetail = await di.pageRepository.getOrganizerStore(storeId);
      pageName.text = storeDetail?.name ?? '';
      pageNameLength = pageName.text.length;
      phone.text = storeDetail?.contact?.phone ?? '';
      type = storeDetail?.type?.text;
      url.text = storeDetail?.slug ?? '';
      about.text = storeDetail?.description ?? '';
      park.text = (storeDetail?.parking?.total ?? '').toString();
      seat.text = (storeDetail?.seats?.min ?? '').toString();
      email.text = storeDetail?.contact?.email ?? '';
      addressDetail.text = storeDetail?.venue?.address ?? '';
      province.text = storeDetail?.venue?.province?.name ?? '';
      city.text = storeDetail?.venue?.city?.name ?? '';
      district.text = storeDetail?.venue?.district?.name ?? '';
      zipcode.text = (storeDetail?.venue?.zipCode ?? '').toString();

      province.text = storeDetail?.venue?.address ?? '';
      getLocallyData = GetLocallyData()
        ..provinceId = storeDetail?.venue?.province?.id
        ..cityId = storeDetail?.venue?.city?.id
        ..districtId = storeDetail?.venue?.district?.id
        ..zipCode = storeDetail?.venue?.zipCode?.toString();

      if (storeDetail?.venue?.lat != null && storeDetail?.venue?.long != null) {
        latLng = LatLng(storeDetail!.venue!.lat!, storeDetail!.venue!.long!);
      }

      attributes.clear();
      if (storeDetail?.attributes?.where((element) => element.code == 'genre').isNotEmpty ??
          false) {
        var genre = storeDetail?.attributes?.firstWhere((element) => element.code == 'genre');
        var genres = genre?.items?.map((e) {
          return SaveEditProfilePageAttributes()
            ..id = genre.id
            ..name = e.name
            ..valueId = e.id;
        }).toList();
        attributes.addAll(genres ?? []);
        this.genre.text = attributes.map((e) => e.name).join(", ");
      }
      List<SaveEditProfilePageTimes>? data;
      if (storeDetail?.times != null) {
        times = storeDetail?.times?.where((element) => element.status == true).map((e) {
          return SaveEditProfilePageTimes()
            ..day = e.day
            ..close = e.close
            ..open = e.open;
        }).toList();
      }
      check();
      setLoading(false);
    });
  }

  void getLocally() {
    catchError(() async {
      getLocallyData =
          await di.locationRepository.getLocally(city.text, district.text, province.text);
      zipcode.text = getLocallyData?.zipCode ?? '';
      notifyListeners();
    });
  }

  void navigateMap(BuildContext context) {
    catchError(() async {
      //var data = await TixNavigate.instance.navigateTo(MapInfo(), data: latLng);
      var data = await WrapNavigation.instance.pushNamed(context, MapInfo(), arguments: latLng);
      if (data is Map) {
        address.text = data['address'] ?? '';
        latLng = data['latLng'];
        province.text = data['province'] ?? ''; //จังหวัด
        district.text = data['district'] ?? ''; //ตำบล
        city.text = data['city'] ?? ''; //อำเภอ
        this.getLocally();
      }
    });
  }

  String? validateUrl(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกข้อมูล URL';
    }
    if (value.length <= 3) {
      return 'ข้อมูล URL ต้องมีค่าไม่น้อยกว่า 3 ตัวอักษร';
    }
    return null;
  }

  String? validatePage(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อเพจ';
    }
    if (value.length <= 2) {
      return 'กรุณากรอกชื่อเพจไม่ต่ำกว่า 2 ตัวอักษร';
    }
    return null;
  }

  String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกอีเมล';
    }
    if (!EmailValidator.validate(value)) {
      return 'กรุณากรอกอีเมลให้ถูกต้อง';
    }
    return null;
  }

  String? validateAddress(String? value) {
    if (latLng == null) {
      return 'กรุณาระบุตำแหน่งที่ตั้ง';
    }
  }

  String? validateAddressDetail(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกรายละเอียดที่อยู่';
    }
    if (value.length <= 2) {
      return 'กรุณากรอกรายละเอียดที่อยู่ไม่ต่ำกว่า 2 ตัวอักษร';
    }
  }

  String? validateZipCode(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกรหัสไปรษณีย์';
    }
    if (value.length < 5) {
      return 'กรุณากรอกรหัสไปรษณีย์่ให้ครบถ้วน';
    }
  }

  String? validateGenre(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณาระบุแนวเพลง';
    }
  }

  String? validatePhoneNumber(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกเบอร์โทรศัพท์';
    }
    if (value.length < 10) {
      return 'กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก';
    }
    return null;
  }

  void check() {
    isActive = (validatePage(pageName.text) == null &&
        validateUrl(url.text) == null &&
        validatePhoneNumber(phone.text) == null &&
        validateEmail(email.text) == null &&
        validateGenre(genre.text) == null &&
        validateAddressDetail(addressDetail.text) == null &&
        validateZipCode(zipcode.text) == null);
    notifyListeners();
  }

  void setPageNameLength(int length) {
    pageNameLength = length;
    notifyListeners();
  }

  navigateToSelectGenre(BuildContext context) {
    catchError(() async {
      //final result = await TixNavigate.instance.navigateTo(SelectGenreScreen(), data: attributes);
      final result = await WrapNavigation.instance
          .pushNamed(context, SelectGenreScreen(), arguments: attributes);
      if (result is List<SaveEditProfilePageAttributes>) {
        attributes.clear();
        attributes.addAll(result);
        genre.text = attributes.map((e) => e.name).join(", ");
        notifyListeners();
      }
    });
  }

  save() {
    catchError(() async {
      setLoading(true);
      SaveEditProfilePageEntity saveEditProfilePageEntity = SaveEditProfilePageEntity()
        ..attributes = attributes
        ..countryCode = 'th'
        ..description = about.text
        ..email = email.text
        ..name = pageName.text
        ..parking = int.parse(park.text)
        ..seatMin = int.parse(seat.text)
        ..slug = url.text
        ..type = type
        ..storeId = storeId
        ..phone = phone.text
        ..times = times;
      SaveVenueAddress saveVenueAddress = SaveVenueAddress()
        ..en = addressDetail.text
        ..th = addressDetail.text;
      SaveVenueName saveVenueName = SaveVenueName()
        ..en = pageName.text
        ..th = pageName.text;

      SaveVenueEntity saveVenueEntity = SaveVenueEntity()
        ..address = saveVenueAddress
        ..cityId = getLocallyData?.cityId
        ..countryId = storeDetail?.venue?.country?.id
        ..xDefault = true
        ..districtId = getLocallyData?.districtId
        ..id = storeDetail?.venue?.id
        ..lat = latLng?.latitude.toString()
        ..long = latLng?.longitude.toString()
        ..name = saveVenueName
        ..provinceId = getLocallyData?.provinceId
        ..status = storeDetail?.status
        ..storeId = storeDetail?.id.toString()
        ..zipCode = getLocallyData?.zipCode;
      final resultVenue = await di.pageRepository
          .updateVenue(storeDetail!.venue!.id!, storeDetail!.id!, saveVenueEntity);

      final result = await di.pageRepository.saveEditProfile(saveEditProfilePageEntity);
      setLoading(false);
    });
  }

  navigateToEditTime(BuildContext context) {
    catchError(() async {
      //List<SaveEditProfilePageTimes>? data = await TixNavigate.instance.navigateTo(EditTimeOpen(), data: times);
      List<SaveEditProfilePageTimes>? data =
          await WrapNavigation.instance.pushNamed(context, EditTimeOpen(), arguments: times);
      if (data != null) {
        times = data;
        notifyListeners();
      }
    });
  }

  void setType(String? newValue) {
    type = newValue;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}
