import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/model/get_district_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class DistrictListScreen extends StatefulWidget with TixRoute {
  List<GetDistrictDataRecord>? districtList;
  int? districtId;
  DistrictListScreen({Key? key, this.districtId, this.districtList}) : super(key: key);

  @override
  _DistrictListScreenState createState() => _DistrictListScreenState();

  @override
  String buildPath() {
    return '/district_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => DistrictListScreen(
              districtId: data['selected'],
              districtList: data['districtList'],
            ));
  }
}

class _DistrictListScreenState extends BaseStateProvider<DistrictListScreen, DistrictViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = DistrictViewModel(widget.districtList, widget.districtId);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DistrictViewModel>(
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.red),
              title: new Text(
                "เลือกตำบล",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff4a4a4a),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            body: bodyContent(),
            bottomNavigationBar: InkWell(
              child: Container(
                width: 343,
                height: 42,
                margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
                decoration: new BoxDecoration(
                    color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                child: Center(
                  child: new Text(
                    "บันทึก",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
              onTap: () {
                Map data = {
                  "index": viewModel.selectedIndex,
                  "id": viewModel.districtId,
                };
                //TixNavigate.instance.pop(data: data);
                WrapNavigation.instance.pop(context, data: data);
              },
            ),
          );
        },
        model: viewModel);
  }

  bodyContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: widget.districtList?.length,
              itemBuilder: (context, index) {
                var item = viewModel.districtList?[index];
                return ListTile(
                  title: Text(
                    '${item?.name?.th}',
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  trailing: viewModel.districtId != -1 && viewModel.districtId == item!.id
                      ? Icon(
                          Icons.check,
                          color: Colors.green,
                        )
                      : SizedBox(),
                  onTap: () {
                    viewModel.selectedItem(item?.id ?? -1);
                    viewModel.setIndex(index);
                  },
                );
              })
        ],
      ),
    );
  }
}

class DistrictViewModel extends BaseViewModel {
  List<GetDistrictDataRecord>? districtList;
  int? districtId = -1;
  int? selectedIndex = -1;
  DistrictViewModel(this.districtList, this.districtId);

  void selectedItem(int id) {
    districtId = id;
    notifyListeners();
  }

  void setIndex(int index) {
    selectedIndex = index;
  }
}
