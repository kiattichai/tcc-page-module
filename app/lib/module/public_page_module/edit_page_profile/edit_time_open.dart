import 'package:app/model/constants.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/time_picker_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditTimeOpen extends StatefulWidget with TixRoute {
  List<SaveEditProfilePageTimes>? data;

  EditTimeOpen({this.data});

  @override
  _EditTimeOpen createState() => _EditTimeOpen();

  @override
  String buildPath() {
    return '/edit_time_open';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditTimeOpen(
              data: data,
            ));
  }
}

class _EditTimeOpen extends State<EditTimeOpen> {
  bool isInstructionView = false;
  bool isActive = false;
  List<SaveEditProfilePageTimes> pageTime = [];

  List<TimeOpen> times = [];
  List<TextEditingController> openControllers = [];
  List<TextEditingController> closeController = [];

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() {
    pageTime.addAll(widget.data ?? []);
    openControllers.addAll(List.generate(7, (index) => TextEditingController()));
    closeController.addAll(List.generate(7, (index) => TextEditingController()));
    times.addAll(List.generate(7, (index) {
      TimeOpen timeOpen = TimeOpen(index + 1);

      if (pageTime.any((element) => element.day == index + 1)) {
        var old = pageTime.firstWhere((element) => element.day == index + 1);
        timeOpen = TimeOpen(index + 1)
          ..isOpen = true
          ..open = TimeOfDay(
              hour: int.parse(old.open!.substring(0, 2)),
              minute: int.parse(old.open!.substring(3, 5)))
          ..closed = TimeOfDay(
              hour: int.parse(old.close!.substring(0, 2)),
              minute: int.parse(old.close!.substring(3, 5)));
      }
      ;

      openControllers[index].text = formatTime(timeOpen.open);
      closeController[index].text = formatTime(timeOpen.closed);
      return timeOpen;
    }));
    check();
  }

  formatTime(TimeOfDay timeOfDay) {
    final now = new DateTime.now();
    final date = DateTime(now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
    return DateFormat('HH:mm').format(date);
  }

  Future<void> _selectOpenTime(int index) async {
    final TimeOfDay? picked = await showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) => TimePickerWidget(
            TimeOfDay(hour: times[index].open.hour, minute: times[index].open.minute)));
    if (picked != null) {
      setState(() {
        times[index].open = picked;
        openControllers[index].text = formatTime(picked);
        check();
      });
    }
  }

  Future<void> _selectClosedTime(int index) async {
    final TimeOfDay? picked = await showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) => TimePickerWidget(
            TimeOfDay(hour: times[index].closed.hour, minute: times[index].closed.minute)));
    if (picked != null) {
      setState(() {
        times[index].closed = picked;
        closeController[index].text = formatTime(picked);
        check();
      });
    }
  }

  void reset(int index) {
    TimeOpen timeOpen = TimeOpen(index + 1);
    times[index] = timeOpen;
    openControllers[index].text = formatTime(timeOpen.open);
    closeController[index].text = formatTime(timeOpen.closed);
    check();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'เลือกเวลาเปิดทำการ',
      ),
      body: SafeArea(
          child: CustomScrollView(slivers: [
        ...List.generate(7, (index) => dayOpenPick(index, DAY_THAI[index])),
        SliverFillRemaining(
            hasScrollBody: false,
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                  width: double.infinity,
                  height: 42,
                  child: ElevatedButton(
                      child: Text(
                        "เรียบร้อย",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(21),
                          ))),
                      onPressed: () => isActive ? save() : null),
                )))
      ])),
    );
  }

  Widget dayOpenPick(int index, String day) {
    return SliverToBoxAdapter(
        child: Container(
            padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
            child: Column(
              children: [
                Row(
                  children: [
                    Switch(
                      value: times[index].isOpen,
                      onChanged: (bool isOn) {
                        setState(() {
                          times[index].isOpen = isOn;
                          if (isOn == false) {
                            reset(index);
                          }
                        });
                      },
                      activeColor: Color(0xFFDA3534),
                      inactiveTrackColor: Colors.grey,
                      inactiveThumbColor: Colors.grey,
                    ),
                    Text(
                      '$day',
                      style: textStyle(),
                    ),
                  ],
                ),
                times[index].isOpen
                    ? Container(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextFormField(
                              autovalidateMode: AutovalidateMode.always,
                              controller: openControllers[index],
                              autofocus: false,
                              readOnly: true,
                              style: textStyle(),
                              onTap: () => _selectOpenTime(index),
                              validator: (value) {
                                return validateTime(times[index].open, times[index].closed);
                              },
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(5),
                                  labelText: 'เวลาเปิด',
                                  suffixIcon: Icon(
                                    Icons.keyboard_arrow_down,
                                    size: 14,
                                  ),
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  border: OutlineInputBorder(),
                                  counterText: ''),
                            )),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                                child: TextFormField(
                              autovalidateMode: AutovalidateMode.always,
                              autofocus: false,
                              controller: closeController[index],
                              style: textStyle(),
                              onTap: () => _selectClosedTime(index),
                              validator: (value) {
                                return validateTime(times[index].open, times[index].closed);
                              },
                              readOnly: true,
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(5),
                                  labelText: 'เวลาปิด',
                                  suffixIcon: Icon(
                                    Icons.keyboard_arrow_down,
                                    size: 14,
                                  ),
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  border: OutlineInputBorder(),
                                  counterText: ''),
                            ))
                          ],
                        ))
                    : SizedBox()
              ],
            ),
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(width: 1, color: Color(0xFFDDDDDD))))));
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  save() {
    List<SaveEditProfilePageTimes> data = times.where((element) => element.isOpen).map((e) {
      return SaveEditProfilePageTimes()
        ..day = e.day
        ..open = formatTime(e.open)
        ..close = formatTime(e.closed);
    }).toList();
    //TixNavigate.instance.pop(data: data);
    WrapNavigation.instance.pop(context, data: data);
  }

  String? validateTime(TimeOfDay open, TimeOfDay close) {
    var now = DateTime.now();
    var openDate = DateTime(now.year, now.month, now.day, open.hour, open.minute);
    var closeDate = DateTime(now.year, now.month, now.day, close.hour, close.minute);
    if (openDate.compareTo(closeDate) > 0) {
      return 'กรุณาระบุเวลาให้ถูกต้อง';
    }
    return null;
  }

  void check() {
    isActive = times
        .where((element) => element.isOpen)
        .where((element) => validateTime(element.open, element.closed) != null)
        .isEmpty;
    setState(() {});
  }
}

class TimeOpen {
  bool isOpen = false;
  final int day;
  TimeOfDay open = TimeOfDay(hour: 10, minute: 00);
  TimeOfDay closed = TimeOfDay(hour: 23, minute: 30);

  TimeOpen(this.day);
}
