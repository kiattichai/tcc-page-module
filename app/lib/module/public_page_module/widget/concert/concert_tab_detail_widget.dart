import 'package:app/model/concert.dart';
import 'package:app/model_from_native/app_constant.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;

class ConcertTabDetail extends StatelessWidget {
  final List<Concert> items;
  final String withOut;
  ConcertTabDetail({Key? key, required this.items, required this.withOut}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (items.isEmpty) {
      return Container(
        padding: const EdgeInsets.only(top: 16, bottom: 34),
        child: new Text("${withOut}",
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff6d6d6d),
              fontSize: 14,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: -0.5,
            )),
      );
    } else {
      return ListView.builder(
        itemCount: items.length,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          final data = items[index];
          return InkWell(
            onTap: () async {
              WrapNavigation.instance.nativePushNamed(context, ACTION_OPEN_CONCERT_DETAIL,
                  arguments: {'id': '${data.id}'});
            },
            child: Container(
              padding: const EdgeInsets.only(top: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: Image.network(
                      items[index].imageUrl,
                      height: 120,
                      width: 90,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image(
                              width: 12,
                              height: 12,
                              image: AssetImage("assets/icons_calendar.png"),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 5),
                              child: new Text(
                                items[index].date,
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff676767),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0.2333333333333333,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.visible,
                              ),
                            )
                          ],
                        ),
                        Container(
                          width: width - 138,
                          child: new Text(
                            items[index].detail,
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff000000),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            overflow: TextOverflow.clip,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
  }
}
