import 'package:app/model/concert.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_management.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ConcertMainWidget extends StatelessWidget {
  final List<Concert> items;

  const ConcertMainWidget({Key? key, required this.items}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (items.isEmpty) {
      return SizedBox();
    } else {
      return Container(
       child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 16.0,left: 16.0,right: 16.0),
              decoration: BoxDecoration(
                border: Border(top: BorderSide(
                  width: 3.0, color: Colors.grey.shade300,
                ))
              ),
              child: Row(
                children: [
                  Text("คอนเสิร์ตที่กำลังมาถึง",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff333333),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        letterSpacing: -0.5714285714285714,
                      )
                  ),
                  Spacer(),
                  InkWell(
                    onTap: ()async{

                    },
                    child: Row(
                      children: [
                        Text("ดูทั้งหมด",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff08080a),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                        ),
                        SizedBox(width: 10.0,),
                        Image(
                          width: 6,
                          height: 10,
                          image: AssetImage("assets/chevron_right2.png"),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(left: 16.0,right: 16.0,bottom: 16.0),
              child: ListView.builder(
                itemCount: items.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    padding: const EdgeInsets.only(top: 16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(6),
                          child: Image.network(
                            items[index].imageUrl,
                            height: 120,
                            width: 90,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Image(
                                    width: 12,
                                    height: 12,
                                    image: AssetImage("assets/icons_calendar.png"),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(left: 5),
                                    child: new Text(
                                      items[index].date,
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff676767),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.2333333333333333,
                                      ),
                                      maxLines: 1,
                                      overflow: TextOverflow.visible,
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                width: width - 138,
                                child: new Text(
                                  items[index].detail,
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xff000000),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  overflow: TextOverflow.clip,
                                  maxLines: 3,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      );
    }
  }
}
