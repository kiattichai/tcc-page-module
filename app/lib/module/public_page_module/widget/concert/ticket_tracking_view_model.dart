import 'dart:convert';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_ticket_setting_entity.dart';
import 'package:app/model/save_ticket_setting_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TicketTrackingViewModel extends BaseViewModel{
  final int concertId;
  final String storeId;

  TextEditingController google = TextEditingController();
  TextEditingController facebook = TextEditingController();

  GetTicketSettingData? getTicketSettingData;

  TicketTrackingViewModel(this.concertId, this.storeId);

  void getSetting(){
    catchError(() async{
      getTicketSettingData = await di.concertRepository.getConcertSetting(concertId, storeId);
      var settingValue = getTicketSettingData?.record?.firstWhere((element) => element.key == 'setting').value;
      facebook.text = settingValue?.fbPixel??'';
      google.text = settingValue?.ga?.replaceFirst('UA-', '')??'';
    });
  }

  @override
  void postInit() {
    super.postInit();
    getSetting();
  }


  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }


  save() {

    catchError(() async {
      setLoading(true);
      var settingValue = getTicketSettingData?.record?.firstWhere((element) => element.key == 'setting').value;
      settingValue!.fbPixel = facebook.text;
      settingValue.ga = google.text;
      var methodValueSave = json.encode(settingValue);

      SaveTicketSettingSettings method = SaveTicketSettingSettings()
        ..key = 'setting'
        ..code = 'payment'
        ..value = methodValueSave;


      SaveTicketSettingEntity save =  SaveTicketSettingEntity();
      save.storeId = storeId;
      save.settings = [method];

      var result  = await di.concertRepository.updateConcertTicketSetting(concertId, save);
      setLoading(false);
    });


  }

}