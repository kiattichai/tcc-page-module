import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/get_concert_by_id_entity.dart';
import 'package:app/model/get_venue_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/module/create_concert_module/add_venue_screen.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_entity.dart';
import 'package:app/module/create_concert_module/select_artist.dart';
import 'package:app/module/create_concert_module/select_concert_place.dart';
import 'package:app/module/public_page_module/edit_page_profile/select_genre_screen.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/utils/date_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/custom_date_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;
import 'package:path/path.dart' as p;

class EditConcertViewModel extends BaseViewModel {
  final int concertId;
  GetConcertByIdData? getConcertByIdData;
  File? concertImage;
  bool isActive = false;
  DateTime startDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10);
  DateTime endDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 10);

  DateTime maxDateTime =
      DateTime(DateTime.now().year + 5, DateTime.now().month, DateTime.now().day + 1, 10);
  List<SaveEditProfilePageAttributes> attributes = [];
  List<GetArtistDataRecord> artists = [];
  TextEditingController place = TextEditingController(text: 'สถานที่');
  GetVenueDataRecord? getVenueDataRecord;

  final genre = TextEditingController(text: 'เลือกแนวเพลง');
  final artist = TextEditingController(text: 'ศิลปิน');

  TextEditingController startDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController endTime = TextEditingController();
  TextEditingController concertName = TextEditingController();
  TextEditingController description = TextEditingController();

  Function()? showSaveSuccess;

  EditConcertViewModel(this.concertId);

  void pickProfileImage() {
    catchError(() async {
      final pickedImage = await ImagePicker().getImage(source: ImageSource.gallery);
      concertImage = pickedImage != null ? File(pickedImage.path) : null;
      check();
      notifyListeners();
    });
  }

  check() {
    isActive = validateDescription(concertName.text) == null &&
        validateDescription(description.text) == null &&
        startDateTime.isBefore(endDateTime) &&
        attributes.isNotEmpty &&
        artists.isNotEmpty &&
        validate(place.text) == null;
  }

  String? validateStartDate(String? value) {
    if (!startDateTime.isBefore(endDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  String? validateEndDate(String? value) {
    if (!startDateTime.isBefore(endDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  String? validateConcertName(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อคอนเสิร์ต';
    }
    if (value.length <= 3) {
      return 'กรุณากรอกชื่อคอนเสิร์ตมากกว่า 3 ตัวอักษร';
    }
    return null;
  }

  String? validateDescription(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกรายละเอียด';
    }
    if (value.length <= 3) {
      return 'กรุณากรอกรายละเอียดมากกว่า 3 ตัวอักษร';
    }
    return null;
  }

  @override
  void postInit() {
    super.postInit();
    getConcert();
  }

  void getConcert() {
    catchError(() async {
      setLoading(true);
      getConcertByIdData = await di.concertRepository.getConcertById(concertId);
      concertName.text = getConcertByIdData?.name ?? '';
      description.text = getConcertByIdData?.description ?? '';
      description.text = getConcertByIdData?.description ?? '';
      startDateTime = DateTime.parse(getConcertByIdData?.showTime?.start ?? '');
      endDateTime = DateTime.parse(getConcertByIdData?.showTime?.end ?? '');
      formatDate();
      if ((getConcertByIdData?.attributes ?? [])
          .where((element) => element.code == 'artist')
          .isNotEmpty) {
        var artist = getConcertByIdData?.attributes
            ?.firstWhere((element) => element.code == 'artist')
            .items
            ?.map((e) {
          var image = GetArtistDataRecordImage()..url = e.image?.url ?? '';
          return GetArtistDataRecord()
            ..image = image
            ..id = e.id
            ..name = e.name ?? '';
        }).toList();
        this.artists.addAll(artist ?? []);
      }
      if ((getConcertByIdData?.attributes ?? [])
          .where((element) => element.code == 'genre')
          .isNotEmpty) {
        var genre = getConcertByIdData?.attributes
            ?.firstWhere((element) => element.code == 'genre')
            .items
            ?.map((e) {
          return SaveEditProfilePageAttributes()
            ..valueId = e.id
            ..id = 1
            ..name = e.name ?? '';
        }).toList();
        this.attributes.addAll(genre ?? []);
      }

      this.place.text = getConcertByIdData?.venue?.name ?? '';
      this.getVenueDataRecord = GetVenueDataRecord()
        ..id = getConcertByIdData?.venue?.id!
        ..name = getConcertByIdData?.venue?.name;
      setLoading(false);
      check();
    });
  }

  void formatDate() {
    startDate.text = DateFormat('dd MMM ', 'th').format(startDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(startDateTime)) + 543).toString();
    startTime.text = DateFormat('HH:mm', 'th').format(startDateTime);
    endDate.text = DateFormat('dd MMM ', 'th').format(endDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(endDateTime)) + 543).toString();
    endTime.text = DateFormat('HH:mm', 'th').format(endDateTime);
    notifyListeners();
  }

  void showDatePicker(BuildContext context, bool isStart) {
    DateTime time = isStart ? startDateTime : endDateTime;
    DatePicker.showPicker(context,
        showTitleActions: true,
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ), onConfirm: (date) {
      if (isStart) {
        startDateTime =
            DateTime(date.year, date.month, date.day, startDateTime.hour, startDateTime.minute);
      } else {
        endDateTime =
            DateTime(date.year, date.month, date.day, endDateTime.hour, endDateTime.minute);
      }
      formatDate();
      check();
      notifyListeners();
    },
        pickerModel: CustomDatePicker(
            minTime: DateTime.now(),
            currentTime: time,
            locale: LocaleType.th,
            maxTime: maxDateTime),
        locale: LocaleType.th);
  }

  void showTimePicker(BuildContext context, bool isStart) {
    DateTime time = isStart ? startDateTime : endDateTime;
    DatePicker.showTimePicker(context,
        showTitleActions: true, showSecondsColumn: false, currentTime: time, onConfirm: (date) {
      if (isStart) {
        startDateTime = DateTime(
            startDateTime.year, startDateTime.month, startDateTime.day, date.hour, date.minute);
      } else {
        endDateTime = DateTime(
            endDateTime.year, endDateTime.month, startDateTime.day, date.hour, date.minute);
      }
      formatDate();
      check();
      notifyListeners();
    },
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ),
        locale: LocaleType.th);
  }

  navigateToSelectGenre(BuildContext context) {
    catchError(() async {
      //final result = await TixNavigate.instance.navigateTo(SelectGenreScreen(), data: attributes);
      final result = await WrapNavigation.instance
          .pushNamed(context, SelectGenreScreen(), arguments: attributes);
      if (result is List<SaveEditProfilePageAttributes>) {
        attributes.clear();
        attributes.addAll(result);
        check();
        notifyListeners();
      }
    });
  }

  String? validateGenre(String? value) {
    if (attributes.isEmpty) {
      return 'กรุณาเลือกแนวเพลง';
    }
    return null;
  }

  String? validateArtist(String? value) {
    if (artists.isEmpty) {
      return 'กรุณาเลือกศิลปิน';
    }
    return null;
  }

  navigateToSelectArtist(BuildContext context) async {
    catchError(() async {
      //List<GetArtistDataRecord>? result = await TixNavigate.instance.navigateTo(SelectArtist(), data: artists);
      List<GetArtistDataRecord>? result =
          await WrapNavigation.instance.pushNamed(context, SelectArtist(), arguments: artists);
      if (result != null) {
        artists.clear();
        artists.addAll(result);
        check();
        notifyListeners();
      }
    });
  }

  navigateToSelectConcertPlace(BuildContext context) {
    catchError(() async {
      //GetVenueDataRecord? result = await TixNavigate.instance.navigateTo(SelectConcertPlace(), data: {'storeId':getConcertByIdData?.store?.id!.toString(),'selectedId':getConcertByIdData?.venue?.id});
      GetVenueDataRecord? result = await WrapNavigation.instance.pushNamed(
          context, SelectConcertPlace(), arguments: {
        'storeId': getConcertByIdData?.store?.id!.toString(),
        'selectedId': getConcertByIdData?.venue?.id
      });
      if (result != null) {
        place.text = result.name!;
        getVenueDataRecord = result;
        check();
        notifyListeners();
      }
    });
  }

  String? validate(String? value) {
    if (getVenueDataRecord == null) {
      return 'กรุณาระบุสถานที่';
    }
    return null;
  }

  navigateToCreateVenue(BuildContext context) {
    catchError(() async {
      //final result = await TixNavigate.instance.navigateTo(AddVenueScreen(), data: {'storeId': getConcertByIdData?.store?.id!.toString()});
      final result = await WrapNavigation.instance.pushNamed(context, AddVenueScreen(),
          arguments: {'storeId': getConcertByIdData?.store?.id!.toString()});
    });
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  delete(BuildContext context) {
    catchError(() async {
      setLoading(true);
      bool result = await showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomDialog(
              title: 'ยืนยันลบ "${getConcertByIdData?.name}" หรือไม่ ?',
              description: '',
              dangerButtonText: 'ยกเลิก',
              buttonText: 'ตกลง',
            );
          });
      if (result == true) {
        final resultSave = await di.concertRepository
            .deleteConcertById(concertId, getConcertByIdData!.store!.id!.toString());
        //TixNavigate.instance.navigateTo(ConcertListScreen(), data: {'storeId': getConcertByIdData!.store!.id!.toString(), 'storeName': getConcertByIdData!.store!.name!, 'isOwner': true});
        WrapNavigation.instance.pushNamed(context, ConcertListScreen(), arguments: {
          'storeId': getConcertByIdData!.store!.id!.toString(),
          'storeName': getConcertByIdData!.store!.name!,
          'isOwner': true
        });
      }
      setLoading(false);
    });
  }

  save() {
    catchError(() async {
      setLoading(true);
      SaveConcertEntity saveConcertEntity = SaveConcertEntity();
      saveConcertEntity.storeId = getConcertByIdData?.store?.id!.toString();
      saveConcertEntity.name = SaveConcertName()
        ..th = concertName.text
        ..en = concertName.text;
      saveConcertEntity.description = SaveConcertDescription()
        ..th = description.text
        ..en = description.text;
      saveConcertEntity.showStart = DateFormatUtils.format(startDateTime);
      saveConcertEntity.showEnd = DateFormatUtils.format(endDateTime);
      saveConcertEntity.attributes = [
        ...artists
            .map((e) => SaveConcertAttributes()
              ..valueId = e.id
              ..id = 2)
            .toList(),
        ...attributes
            .map((e) => SaveConcertAttributes()
              ..valueId = e.valueId
              ..id = e.id)
            .toList()
      ];
      saveConcertEntity.type = "event";
      saveConcertEntity.createdBy = globals.userDetail!.data!.id;
      saveConcertEntity.seoDescription = SaveConcertSeoDescription()
        ..th = ''
        ..en = '';
      saveConcertEntity.seoName = SaveConcertSeoName()
        ..th = ''
        ..en = '';
      saveConcertEntity.venueId = getVenueDataRecord?.id!;
      if (concertImage != null) {
        final bytes = await concertImage!.readAsBytes();
        final b64 = base64.encode(bytes);
        UploadConcertImageEntity uploadConcertImageEntity = UploadConcertImageEntity()
          ..storeId = saveConcertEntity.storeId
          ..status = 'default'
          ..secure = false
          ..files = [
            UploadConcertImageFiles()
              ..position = 0
              ..data = b64
              ..name = p.basename(concertImage!.path)
          ];
        final result = await di.createConcertRepository.saveConcertImage(uploadConcertImageEntity);
        saveConcertEntity.images = List.generate(
            result?.uploads?.length ?? 0,
            (index) => SaveConcertImages()
              ..id = result?.uploads![index].id
              ..position = index
              ..tag = 'logo');
      }
      final resultSave = await di.concertRepository.updateConcertById(concertId, saveConcertEntity);
      showSaveSuccess!();
      setLoading(false);
    });
  }
}
