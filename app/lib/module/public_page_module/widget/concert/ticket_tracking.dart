import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_tracking_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TicketTracking extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;

  TicketTracking({Key? key, this.concertId, this.storeId}) : super(key: key);

  @override
  _TicketTracking createState() => _TicketTracking();

  @override
  String buildPath() {
    return '/ticket_tracking';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => TicketTracking(
              concertId: data['concertId'],
              storeId: data['storeId'],
            ));
  }
}

class _TicketTracking
    extends BaseStateProvider<TicketTracking, TicketTrackingViewModel> {
  @override
  void initState() {
    viewModel = TicketTrackingViewModel(widget.concertId!, widget.storeId!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TicketTrackingViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'การวิเคราะห์และติดตาม',
                ),
                body: SafeArea(
                  child: CustomScrollView(
                    slivers: [
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            child: new Text("Google",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ))),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 3, left: 16, right: 16),
                            child: new Text("Google Analytics Tracking",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ))),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 16, left: 16, right: 16),
                            child: TextFormField(
                              onChanged: (text) {},
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              controller: viewModel.google,
                              autofocus: false,
                              maxLength: 50,
                              decoration: new InputDecoration(
                                  border: new OutlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: const Color(0xFFE0E0E0),
                                          width: 0.1)),
                                  fillColor: Colors.white,
                                  prefixIcon: Container(
                                    margin: const EdgeInsets.only(right: 5),
                                    alignment: Alignment.center,
                                    decoration: new BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                            width: 0.5, color: Colors.grey),
                                      ),
                                    ),
                                    width: 30,
                                    child: Text('UA-',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                        style: textStyle()),
                                  ),
                                  counterText: '',
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  labelText: 'Google Analytics tracking ID'),
                            )),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            child: new Text("Facebook",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ))),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 3, left: 16, right: 16),
                            child: new Text("Facebook Pixel ID",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ))),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 16, left: 16, right: 16),
                            child: TextFormField(
                              onChanged: (text) {},
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              autofocus: false,
                              maxLength: 50,
                              controller: viewModel.facebook,
                              decoration: new InputDecoration(
                                  border: new OutlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: const Color(0xFFE0E0E0),
                                          width: 0.1)),
                                  fillColor: Colors.white,
                                  prefixIcon: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(right: 5),
                                    decoration: new BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                            width: 0.5, color: Colors.grey),
                                      ),
                                    ),
                                    width: 30,
                                    child: Text('ID',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                        style: textStyle()),
                                  ),
                                  counterText: '',
                                  isDense: true,
                                  labelStyle: textStyle(),
                                  labelText: 'Facebook Pixel ID'),
                            )),
                      ),
                      SliverFillRemaining(
                          hasScrollBody: false,
                          child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: const EdgeInsets.only(
                                    top: 20, left: 16, right: 16, bottom: 24),
                                width: double.infinity,
                                height: 42,
                                child: ElevatedButton(
                                    child: Text(
                                      "บันทึกข้อมูล",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: true
                                            ? Color(0xffffffff)
                                            : Color(0xffb2b2b2),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    style: ButtonStyle(
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.white),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                true
                                                    ? Color(0xffda3534)
                                                    : Color(0xffe6e7ec)),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(21),
                                        ))),
                                    onPressed: () => viewModel.save()),
                              )))
                    ],
                  ),
                ),
              ));
        });
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
