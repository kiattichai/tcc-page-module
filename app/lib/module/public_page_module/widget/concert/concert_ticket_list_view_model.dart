import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_concert_tickets_entity.dart';
import 'package:app/model/update_ticket_status_entity.dart';
import 'package:app/module/public_page_module/widget/concert/concert_ticket_list.dart';
import 'package:app/module/public_page_module/widget/concert/create_concert_ticket.dart';
import 'package:app/module/public_page_module/widget/concert/set_fee_concert.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ConcertTicketListViewModel extends BaseViewModel {
  final int concertId;
  final String storeId;

  GetConcertTicketsData? concertTicketsData;

  ConcertTicketListViewModel(this.concertId, this.storeId);

  @override
  void postInit() {
    super.postInit();
    getTickets();
  }

  navigateToSetFee(BuildContext context) {
    catchError(() async {
      if (concertTicketsData?.record?.isEmpty ?? false) {
        //var result = await TixNavigate.instance.navigateTo(SetFeeConcert(), data: {'concertId': concertId,'storeId':storeId,'isPop':false});
        var result = await WrapNavigation.instance.pushNamed(context, SetFeeConcert(),
            arguments: {'concertId': concertId, 'storeId': storeId, 'isPop': false});
      } else {
        //var result = await TixNavigate.instance.navigateTo(CreateConcertTicket(), data: {'concertId': concertId,'storeId':storeId});
        var result = await WrapNavigation.instance.pushNamed(context, CreateConcertTicket(),
            arguments: {'concertId': concertId, 'storeId': storeId});
      }

      getTickets();
    });
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  void getTickets() {
    catchError(() async {
      setLoading(true);
      concertTicketsData = await di.concertRepository.getConcertTicket(concertId);
      setLoading(false);
    });
  }

  editTicket(GetConcertTicketsDataRecord ticket, BuildContext context) {
    catchError(() async {
      //var result = await TixNavigate.instance.navigateTo(CreateConcertTicket(), data: {'concertId': concertId,'storeId':storeId,'ticketId':ticket.id});
      var result = await WrapNavigation.instance.pushNamed(context, CreateConcertTicket(),
          arguments: {'concertId': concertId, 'storeId': storeId, 'ticketId': ticket.id});
      getTickets();
    });
  }

  updateStatus(GetConcertTicketsDataRecord ticket, bool status) {
    catchError(() async {
      ticket.status = status;
      notifyListeners();
      var update = UpdateTicketStatusEntity()
        ..storeId = storeId
        ..status = status.toString();
      await di.concertRepository.updateConcertTicketStatus(concertId, ticket.id!, update);
    });
  }
}
