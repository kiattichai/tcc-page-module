import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_concert_entity.dart';
import 'package:app/model/update_concert_status_entity.dart';
import 'package:flutter/material.dart';

class ConcertListScreenViewModel extends BaseViewModel {
  final String storeId;

  GetConcertData? getConcertData;

  late ScrollController concertController;
  List<GetConcertDataRecord> concertList = [];
  int limit = 10;
  int page = 1;
  bool isComplete = false;

  ConcertListScreenViewModel(this.storeId) {
    concertController = new ScrollController();
  }

  void getConcert() {
    catchError(() async {
      setLoading(true);
      getConcertData = await di.pageRepository.getConcertInStore(storeId, 1, 3);
      setLoading(false);
    });
  }

  @override
  void postInit() {
    super.postInit();
    clearCheckInPage();
    getCheckIn();
  }

  void getCheckIn() {
    catchError(() async {
      setLoading(true);
      if (getConcertData?.pagination?.lastPage != page) {
        isComplete = false;
      } else {
        isComplete = true;
      }
      if (page == 1) {
        concertList.clear();
      }
      getConcertData = await di.pageRepository.getConcertInStore(storeId, page, limit);
      if (getConcertData?.record != null) {
        concertList.addAll(getConcertData?.record ?? []);
      }

      setLoading(false);
    });
  }

  clearCheckInPage() {
    page = 1;
    isComplete = false;
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }

  void published(GetConcertDataRecord concertList, bool isOn) {
    catchError(() async {
      concertList.status = isOn;
      notifyListeners();
      var update = UpdateConcertStatusEntity()
        ..storeId = storeId
        ..status = isOn.toString();
      await di.concertRepository.updateConcertStatus(concertList.id!, update);
    });
  }
}
