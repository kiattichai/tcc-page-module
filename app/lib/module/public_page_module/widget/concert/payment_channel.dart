import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/payment_channel_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class PaymentChannel extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;

  const PaymentChannel({Key? key, this.concertId, this.storeId})
      : super(key: key);

  @override
  _PaymentChannel createState() => _PaymentChannel();

  @override
  String buildPath() {
    return '/payment_channel';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) =>
            PaymentChannel(
                concertId: data['concertId'], storeId: data['storeId']));
  }
}

class _PaymentChannel extends BaseStateProvider<PaymentChannel, PaymentChannelViewModel>  {

  @override
  void initState() {
    super.initState();
    viewModel = PaymentChannelViewModel(widget.concertId!, widget.storeId!);
  }

  @override
  Widget build(BuildContext context) {
   return BaseWidget<PaymentChannelViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child:Scaffold(
              appBar: AppBarWidget(name: 'ตั้งค่าช่องทางการชำระเงิน'),
              body: SafeArea(
                  child: CustomScrollView(slivers: [
                    SliverToBoxAdapter(
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Text("ช่องทางการชำระเงินสำหรับการจำหน่ายบัตร",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )
                          ),
                        )),
                    SliverToBoxAdapter(
                        child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFFE0E0E0),
                                        width: 1))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    new Text("บัตรเครดิตและบัตรเดบิต",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.ccw,
                                      onChanged: (bool isOn) {
                                        viewModel.setCcw(isOn);
                                      },
                                      activeColor: Color(0xFFDA3534),
                                      inactiveTrackColor: Color(
                                          0xFFCACACA),
                                      inactiveThumbColor: Color(
                                          0xFFF4F4F4),
                                    )
                                  ],
                                ),
                                Container(
                                    width: 132,
                                    height: 39,
                                    decoration: new BoxDecoration(
                                      image: DecorationImage(image: AssetImage('assets/ccw-img.jpeg')),
                                    )
                                ),
                                new Text("รองรับการชำระเงินผ่านเครือข่ายบัตรเครดิตชั้นนำทั่วโลก เพียงแค่กรอกหมายเลขบัตรและรายละเอียดที่จำเป็นอีกเล็กน้อย ผู้ซื้อก็สามารถรับชำระค่าสินค้าได้ทันที สะดวก ปลอดภัย และง่ายสุดๆ",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            )
                        )),
                    SliverToBoxAdapter(
                        child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFFE0E0E0),
                                        width: 1))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    new Text("อินเทอร์เน็ตแบงก์กิ้ง",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.iBanking,
                                      onChanged: (bool isOn) {
                                        viewModel.setIBanking(isOn);
                                      },
                                      activeColor: Color(0xFFDA3534),
                                      inactiveTrackColor: Color(
                                          0xFFCACACA),
                                      inactiveThumbColor: Color(
                                          0xFFF4F4F4),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    iBankingImage('assets/_internet_banking_bay.png'),
                                    iBankingImage('assets/_internet_banking_bbl.png'),
                                    iBankingImage('assets/_internet_banking_scb.png'),
                                    iBankingImage('assets/_internet_banking_ktb.png'),
                                  ],
                                ),
                                new Text("อินเทอร์เน็ตแบงก์กิ้งเป็นบริการธนาคารออนไลน์ที่เปิดให้เจ้าของบัญชีสามารถทำธุรกรรมออนไลน์ผ่านบัญชีธนาคารบนมือถือได้ ปัจจุบันได้รับความนิยมอย่างมาก เพราะสามารถทำธุรกรรมได้ตลอดเวลา ไม่จำเป็นต้องเดินทางออกจากบ้านไปไหน ช่วยให้กลุ่มผู้ซื้อที่ไม่ถือบัตรสามารถซื้อสินค้าและชำระเงินผ่านช่องทางออนไลน์ได้สะดวก",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            )
                        )),
                    SliverToBoxAdapter(
                        child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFFE0E0E0),
                                        width: 1))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    new Text("ชำระเงินผ่านบิล",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.bill,

                                      onChanged: (bool isOn) {
                                        viewModel.setBill(isOn);
                                      },
                                      activeColor: Color(0xFFDA3534),
                                      inactiveTrackColor: Color(
                                          0xFFCACACA),
                                      inactiveThumbColor: Color(
                                          0xFFF4F4F4),
                                    )
                                  ],
                                ),
                                Container(
                                    width: 50,
                                    height: 26,
                                    decoration: new BoxDecoration(
                                      image: DecorationImage(image: AssetImage('assets/tesco_lotus.png')),
                                    )
                                ),
                                new Text("การรับชำระเงินผ่านร้านสะดวกซื้อเปิดให้ผู้บริโภคชำระเงินค่าสินค้าหรือบริการที่ทำการสั่งซื้อผ่านช่องทางออนไลน์ โดยใช้บาร์โค้ดหรือรหัสชำระเงินที่ทางร้านค้าออกให้ นำไปชำระด้วยเงินสดผ่านเคาน์เตอร์บริการ",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            )
                        )),
                    SliverToBoxAdapter(
                        child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFFE0E0E0),
                                        width: 1))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    new Text("พร้อมเพย์",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.promptPay,
                                      onChanged: (bool isOn) {
                                        viewModel.setPromptPay(isOn);
                                      },
                                      activeColor: Color(0xFFDA3534),
                                      inactiveTrackColor: Color(
                                          0xFFCACACA),
                                      inactiveThumbColor: Color(
                                          0xFFF4F4F4),
                                    )
                                  ],
                                ),
                                Container(
                                    width: 75,
                                    height: 29,
                                    decoration: new BoxDecoration(
                                      image: DecorationImage(image: AssetImage('assets/_prompt_pay.png')),
                                    )
                                ),
                                new Text("วิธีการรับชำระเงินค่าสินค้าและบริการรูปแบบใหม่ สามารถชำระค่าสินค้าและบริการต่างๆ ผ่านทางแอปโมบายแบงก์กิ้งต่างๆ เพียงสแกน QR Code ก็สามารถจ่ายเงินค่า บัตร/สินค้า ต่างๆ ได้เลย",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            )
                        )),
                    SliverToBoxAdapter(
                        child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Color(0xFFE0E0E0),
                                        width: 1))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    new Text("พร้อมเพย์",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )
                                    ),
                                    Spacer(),
                                    Switch(
                                      value: viewModel.installment,
                                      onChanged: (bool isOn) {
                                        viewModel.setInstallment(isOn);
                                      },
                                      activeColor: Color(0xFFDA3534),
                                      activeTrackColor: Color(0xfff5c5c4),
                                      inactiveTrackColor: Color(
                                          0xFFCACACA),
                                      inactiveThumbColor: Color(
                                          0xFFF4F4F4),
                                    )
                                  ],
                                ),
                                Container(
                                    width: 182,
                                    height: 31,
                                    decoration: new BoxDecoration(
                                      image: DecorationImage(image: AssetImage('assets/_installment.png')),
                                    )
                                ),
                                new Text("การชำระเงินแบบผ่อนชำระผ่านบัตรเครดิต เป็นอีกหนึ่งช่องทางการชำระเงินที่อำนวยความสะดวกในการจ่ายเงินให้กับคุณ โดยคุณสามารถเปลี่ยนยอดใช้จ่ายบริการจากยอดเต็มจำนวนเป็นการผ่อนชำระแบบรายเดือน",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            )
                        )),
                    SliverFillRemaining(
                        hasScrollBody: false,
                        child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  top: 16, left: 16, right: 16, bottom: 24),
                              width: double.infinity,
                              height: 42,
                              child: ElevatedButton(
                                  child: Text(
                                    "ยืนยัน",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: true
                                          ? Color(0xffffffff)
                                          : Color(0xffb2b2b2),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  style: ButtonStyle(
                                      foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                      backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          true
                                              ? Color(0xFFDA3534)
                                              : Color(0xffe6e7ec)),
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(21),
                                          ))),
                                  onPressed: () => viewModel.save(context)),
                            )))
                  ])))
          );}
    );

  }

  Widget iBankingImage(String image){
    return Container(
        margin: const EdgeInsets.only(right: 4),
        width: 28,
        height: 28,
        decoration: new BoxDecoration(
            image: DecorationImage(image: AssetImage(image)),
            borderRadius: BorderRadius.circular(2)
        )
    );
  }
}
