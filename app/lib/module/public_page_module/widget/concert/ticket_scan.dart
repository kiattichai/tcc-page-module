import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/create_ticket_scan_dialog.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_scan_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/utils/date_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TicketScan extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;

  TicketScan({Key? key, this.concertId, this.storeId}) : super(key: key);

  @override
  _TicketScan createState() => _TicketScan();

  @override
  String buildPath() {
    return '/ticket_scan';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => TicketScan(
              concertId: data['concertId'],
              storeId: data['storeId'],
            ));
  }
}

class _TicketScan extends BaseStateProvider<TicketScan, TicketScanViewModel> {
  @override
  void initState() {
    viewModel = TicketScanViewModel(widget.concertId!, widget.storeId!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TicketScanViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBarWidget(
              name: 'ระแบบสแกนบัตรเข้างาน',
              actionButton: TextButton(
                onPressed: () async {
                  final result = await showDialog(
                      context: context,
                      builder: (_) => CreateTicketScanDialog());
                  if (result != null) {
                    viewModel.saveTicketScan(result);
                  }
                },
                child: new Text(" สร้าง",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff4a4a4a),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    )),
              ),
            ),
            body: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Container(
                    padding:
                        const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child:
                        new Text("ปัดไปทางซ้ายเพื่อเพิกถอนระบบสแกนบัตรเข้างาน",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20, left: 16, right: 16),
                    padding: const EdgeInsets.only(bottom: 6),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 1, color: Color(0xFFE0E0E0)))),
                    child: Row(
                      children: [
                        new Text("Login ID",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            )),
                        Spacer(),
                        new Text("Admin/Gate No.",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    ),
                  ),
                  ListView.builder(
                      itemCount: viewModel.ticketScanList.length,
                      controller: viewModel.scrollController,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        var create = DateFormat('dd MMM yyyy HH:mm น.', 'th')
                            .format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(
                                viewModel.ticketScanList[index].createdAt ??
                                    ''));
                        return Slidable(
                          startActionPane: ActionPane(
                            motion: const DrawerMotion(),
                            extentRatio: 0.20,
                            children: [
                              // SlidableAction(
                              //   label: 'Archive',
                              //   backgroundColor: Colors.blue,
                              //   icon: Icons.archive,
                              //   onPressed: (context) {},
                              // ),
                            ],
                          ),
                          endActionPane: ActionPane(
                            motion: const DrawerMotion(),
                            extentRatio: 0.20,
                            children: [
                              SlidableAction(
                                label: 'เพิกถอน',
                                backgroundColor: Color(0xFFDA3534),
                                icon: Icons.delete,
                                onPressed: (context) async {
                                  final result = await showDialog(
                                      context: context,
                                      builder: (_) => CustomDialog(
                                            title:
                                                'ลบ Login ID : ${viewModel.ticketScanList[index].token} ?',
                                            description: '',
                                            dangerButtonText: 'ยกเลิก',
                                            buttonText: 'ตกลง',
                                          ));
                                  if (result != null) {
                                    viewModel.deleteTicketScan(
                                        viewModel.ticketScanList[index].id!);
                                  }
                                },
                              ),
                            ],
                          ),
                          child: InkWell(
                            onTap: () async {
                              final result = await showDialog(
                                  context: context,
                                  builder: (_) => CreateTicketScanDialog(
                                        note: viewModel
                                            .ticketScanList[index].note,
                                      ));
                              if (result != null) {
                                viewModel.updateTicketScan(
                                    viewModel.ticketScanList[index].id!,
                                    result);
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.only(
                                  top: 14, left: 16, right: 16),
                              padding: const EdgeInsets.only(bottom: 6),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 1, color: Color(0xFFE0E0E0)))),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      new Text(
                                          viewModel.ticketScanList[index]
                                                  .token ??
                                              '',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Spacer(),
                                      new Text(
                                          viewModel
                                                  .ticketScanList[index].note ??
                                              '',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  ),
                                  Row(children: [
                                    new Text('สร้างเมื่อ: $create',
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff6d6d6d),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ])
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ])),
          );
        });
  }
}
