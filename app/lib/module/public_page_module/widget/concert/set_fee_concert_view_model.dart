import 'dart:convert';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_calculate_ticket_entity.dart';
import 'package:app/model/get_ticket_setting_entity.dart';
import 'package:app/model/save_ticket_setting_entity.dart';
import 'package:app/model/ticket_payer.dart';
import 'package:app/module/public_page_module/widget/concert/create_concert_ticket.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';

enum FeeType { customer, organizer }

class SetFeeConcertViewModel extends BaseViewModel {
  final int concertId;
  final String storeId;
  bool isPop = false;
  FeeType paymentFee = FeeType.customer;
  FeeType serviceFee = FeeType.customer;
  final int paymentFeeCharge = 3;
  final int serviceFeeCharge = 3;
  bool ccw = false;
  bool iBanking = false;
  bool bill = false;
  bool promptPay = false;
  bool installment = false;
  bool isChecked = false;

  GetTicketSettingData? getTicketSettingData;
  GetCalculateTicketData? calculateTicketData;

  TextEditingController price = TextEditingController(text: '100');
  TextEditingController amount = TextEditingController(text: '1');

  TicketPayer paymentFeeCal = TicketPayer.seller;
  TicketPayer serviceFeeCal = TicketPayer.buyer;

  SetFeeConcertViewModel(this.concertId, this.storeId, this.isPop);

  @override
  void postInit() {
    super.postInit();
    getConcertSetting();
    calculate();
  }

  void getConcertSetting() {
    catchError(() async {
      setLoading(true);
      getTicketSettingData = await di.concertRepository.getConcertSetting(concertId, storeId);
      var feeValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'fee').value;
      var methodValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'method').value;
      if ((feeValue?.paymentFee?.organizerPay ?? 0) > 0) {
        paymentFee = FeeType.organizer;
      }
      if ((feeValue?.serviceFee?.organizerPay ?? 0) > 0) {
        paymentFee = FeeType.organizer;
      }
      ccw = methodValue?.ccw?.status ?? false;
      iBanking = methodValue?.ibanking?.status ?? false;
      bill = methodValue?.bill?.status ?? false;
      promptPay = methodValue?.promptpay?.status ?? false;
      installment = methodValue?.installment?.status ?? false;
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  void setPaymentFee(FeeType? newValue) {
    paymentFee = newValue ?? FeeType.organizer;
    notifyListeners();
  }

  void setServiceFee(FeeType? newValue) {
    serviceFee = newValue ?? FeeType.organizer;
    notifyListeners();
  }

  void setCcw(bool value) {
    ccw = value;
    notifyListeners();
  }

  void setIBanking(bool value) {
    iBanking = value;
    notifyListeners();
  }

  void setBill(bool value) {
    bill = value;
    notifyListeners();
  }

  void setPromptPay(bool value) {
    promptPay = value;
    notifyListeners();
  }

  void setInstallment(bool value) {
    installment = value;
    notifyListeners();
  }

  void setChecked(bool? value) {
    isChecked = value ?? false;
    notifyListeners();
  }

  void calculate() {
    catchError(() async {
      if (this.price.text.isNotEmpty && this.amount.text.isNotEmpty) {
        var price = double.tryParse(this.price.text);
        var amount = int.tryParse(this.amount.text);
        calculateTicketData = await di.concertRepository
            .getTicketPriceCalculate(serviceFeeCal, paymentFeeCal, price ?? 0, amount ?? 0);
        notifyListeners();
      }
    });
  }

  save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      var feeValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'fee').value;
      var methodValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'method').value;

      var serviceFee = GetTicketSettingDataRecordValueServiceFee();
      if (this.serviceFee == FeeType.customer) {
        serviceFee.customerPay = paymentFeeCharge;
        serviceFee.organizerPay = 0;
      }
      if (this.serviceFee == FeeType.organizer) {
        serviceFee.organizerPay = paymentFeeCharge;
        serviceFee.customerPay = 0;
      }
      var paymentFee = GetTicketSettingDataRecordValuePaymentFee();
      if (this.paymentFee == FeeType.customer) {
        paymentFee.customerPay = paymentFeeCharge;
        paymentFee.organizerPay = 0;
      }
      if (this.paymentFee == FeeType.organizer) {
        paymentFee.organizerPay = paymentFeeCharge;
        paymentFee.customerPay = 0;
      }

      methodValue!.ccw!.status = ccw;
      methodValue.ibanking!.status = iBanking;
      methodValue.bill!.status = bill;
      methodValue.promptpay!.status = promptPay;
      methodValue.installment!.status = installment;
      var methodValueSave = json.encode(methodValue);

      GetTicketSettingDataRecordValue value = GetTicketSettingDataRecordValue()
        ..serviceFee = serviceFee
        ..paymentFee = paymentFee
        ..taxFee = feeValue?.taxFee!;
      var feeValueSave = json.encode(value);

      SaveTicketSettingSettings fee = SaveTicketSettingSettings()
        ..key = 'fee'
        ..code = 'payment'
        ..value = feeValueSave;
      SaveTicketSettingSettings method = SaveTicketSettingSettings()
        ..key = 'method'
        ..code = 'payment'
        ..value = methodValueSave;

      SaveTicketSettingEntity save = SaveTicketSettingEntity();
      save.storeId = storeId;
      save.settings = [method, fee];

      var result = await di.concertRepository.updateConcertTicketSetting(concertId, save);
      if (isPop) {
        if (result != null) {
          //TixNavigate.instance.pop(data:result );
          WrapNavigation.instance.pop(context, data: result);
        }
      } else {
        //var resultTicket = await TixNavigate.instance.navigateTo(CreateConcertTicket(), data: {'concertId': concertId,'storeId':storeId});
        var resultTicket = await WrapNavigation.instance.pushNamed(context, CreateConcertTicket(),
            arguments: {'concertId': concertId, 'storeId': storeId});
        if (resultTicket != null) {
          //TixNavigate.instance.pop(data:resultTicket );
          WrapNavigation.instance.pop(context, data: resultTicket);
        }
      }

      setLoading(false);
    });
  }

  void setPaymentFeeCal(int index) {
    if (index == 0) {
      paymentFeeCal = TicketPayer.seller;
    } else {
      paymentFeeCal = TicketPayer.buyer;
    }
    calculate();
    notifyListeners();
  }

  void setServiceFeeCal(int index) {
    if (index == 0) {
      serviceFeeCal = TicketPayer.seller;
    } else {
      serviceFeeCal = TicketPayer.buyer;
    }
    calculate();
    notifyListeners();
  }
}
