import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_concert_by_id_entity.dart';
import 'package:app/module/public_page_module/widget/concert/concert_ticket_list.dart';
import 'package:app/model/get_ticket_entity.dart';
import 'package:app/module/create_discount_code_module/discount_screen.dart';
import 'package:app/module/public_page_module/widget/concert/edit_concert.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';

import 'manage_concert_menu.dart';

class TicketManagementViewModel extends BaseViewModel {
  final int concertId;
  final String storeId;
  GetConcertByIdData? getConcertByIdData;
  GetTicketData? getTicketData;
  List<GetTicketDataRecord>? getTicketDataRecord;

  TicketManagementViewModel(this.concertId, this.storeId);

  @override
  void postInit() {
    super.postInit();
    getConcert();
    getTicket();
    //getDiscountCode();
  }

  void getConcert() {
    catchError(() async {
      setLoading(true);
      getConcertByIdData = await di.concertRepository.getConcertById(concertId);
      setLoading(false);
    });
  }

  void getTicket() {
    catchError(() async {
      setLoading(true);
      getTicketData = await di.concertRepository.getTicket(concertId);
      if (getTicketData != null) {
        getTicketDataRecord?.addAll(getTicketData?.record ?? []);
      }
      setLoading(false);
    });
  }

  void getDiscountCode() {
    catchError(() async {});
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  navigateToEditConcert(BuildContext context) {
    catchError(() async {
      //var result = await TixNavigate.instance.navigateTo(EditConcert(), data: {'concertId': concertId});
      var result = await WrapNavigation.instance
          .pushNamed(context, EditConcert(), arguments: {'concertId': concertId});
      getConcert();
    });
  }

  navigateToTicketList(BuildContext context) {
    catchError(() async {
      //var result = await TixNavigate.instance.navigateTo(ConcertTicketList(), data: {'concertId': concertId,'storeId':getConcertByIdData?.store?.id!.toString()});
      var result = await WrapNavigation.instance.pushNamed(context, ConcertTicketList(),
          arguments: {
            'concertId': concertId,
            'storeId': getConcertByIdData?.store?.id!.toString()
          });
      getConcert();
    });
  }

  navigateToDiscountScreen(BuildContext context) {
    catchError(() async {
      //var result = await TixNavigate.instance.navigateTo(DiscountScreen(), data: {'concertId': concertId,'storeId':storeId});
      var result = await WrapNavigation.instance.pushNamed(context, DiscountScreen(),
          arguments: {'concertId': concertId, 'storeId': storeId});
    });
  }

  navigateToManage(BuildContext context) {
    catchError(() async {
      //var result = await TixNavigate.instance.navigateTo(ManageConcertMenu(), data: {'concertId': concertId,'storeId':storeId});
      var result = await WrapNavigation.instance.pushNamed(context, ManageConcertMenu(),
          arguments: {'concertId': concertId, 'storeId': storeId});
    });
  }
}
