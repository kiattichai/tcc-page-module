import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/create_concert_ticket.dart';
import 'package:app/module/public_page_module/widget/concert/set_fee_concert_view_model.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_term_and_condition.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:toggle_switch/toggle_switch.dart';

class SetFeeConcert extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;
  final bool? isPop;

  const SetFeeConcert({Key? key, this.concertId, this.storeId, this.isPop}) : super(key: key);

  @override
  _SetFeeConcert createState() => _SetFeeConcert();

  @override
  String buildPath() {
    return '/set_fee_concert';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => SetFeeConcert(
            concertId: data['concertId'], storeId: data['storeId'], isPop: data['isPop']));
  }
}

class _SetFeeConcert extends BaseStateProvider<SetFeeConcert, SetFeeConcertViewModel> {
  int index1 = 0;
  int index2 = 0;

  @override
  void initState() {
    super.initState();
    viewModel = SetFeeConcertViewModel(widget.concertId!, widget.storeId!, widget.isPop ?? false);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SetFeeConcertViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'ตั้งค่าธรรมเนียมก่อนเปิดขายบัตร',
                ),
                body: SafeArea(
                    child: CustomScrollView(slivers: [
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                          child: RichText(
                              text: new TextSpan(children: [
                            new TextSpan(
                                text: "คุณสามารถเลือกได้ว่า",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                            new TextSpan(
                                text: "คุณหรือลูกค้าของคุณ(ผู้ซื้อ)",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                            new TextSpan(
                                text: "เป็นผู้ชำระค่าธรรมเนียมต่อไปนี้",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                          ])))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                          child: Table(
                            border: TableBorder.all(color: Color(0xFFE0E0E0)),
                            columnWidths: const <int, TableColumnWidth>{
                              0: FlexColumnWidth(8),
                              1: FlexColumnWidth(6),
                              2: FlexColumnWidth(8),
                            },
                            children: [
                              TableRow(
                                children: [
                                  header('ประเภท'),
                                  header('ค่าธรรมเนียม'),
                                  header('เลือกผู้จ่าย'),
                                ],
                              ),
                              TableRow(children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8, top: 8),
                                  alignment: Alignment.center,
                                  child: RichText(
                                      text: new TextSpan(children: [
                                    new TextSpan(
                                        text: "ค่าธรรมเนียมชำระเงิน",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new TextSpan(
                                        text: "(VAT 7% incl.)",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ])),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.only(left: 8, top: 8),
                                  child: RichText(
                                      text: new TextSpan(children: [
                                    new TextSpan(
                                        text: "3% ",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new TextSpan(
                                        text: "ต่อใบ",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ])),
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Radio<FeeType>(
                                            fillColor:
                                                MaterialStateProperty.all<Color>(Color(0xFFDA3534)),
                                            groupValue: viewModel.paymentFee,
                                            value: FeeType.organizer,
                                            onChanged: (FeeType? newValue) {
                                              viewModel.setPaymentFee(newValue);
                                            },
                                          ),
                                          Text('ผุู้ขาย'),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Radio<FeeType>(
                                            fillColor:
                                                MaterialStateProperty.all<Color>(Color(0xFFDA3534)),
                                            groupValue: viewModel.paymentFee,
                                            value: FeeType.customer,
                                            onChanged: (FeeType? newValue) {
                                              viewModel.setPaymentFee(newValue);
                                            },
                                          ),
                                          Text('ผู้ซื้อ'),
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              ]),
                              TableRow(children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8, top: 8),
                                  alignment: Alignment.center,
                                  child: RichText(
                                      text: new TextSpan(children: [
                                    new TextSpan(
                                        text: "ค่าบริการ",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new TextSpan(
                                        text: "(VAT 7% incl.)",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ])),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.only(left: 8, top: 8),
                                  child: RichText(
                                      text: new TextSpan(children: [
                                    new TextSpan(
                                        text: "3% ",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new TextSpan(
                                        text: "ต่อใบ",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ])),
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Radio<FeeType>(
                                            fillColor:
                                                MaterialStateProperty.all<Color>(Color(0xFFDA3534)),
                                            groupValue: viewModel.serviceFee,
                                            value: FeeType.organizer,
                                            onChanged: (FeeType? newValue) {
                                              viewModel.setServiceFee(newValue);
                                            },
                                          ),
                                          Text('ผุู้ขาย'),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Radio<FeeType>(
                                            fillColor:
                                                MaterialStateProperty.all<Color>(Color(0xFFDA3534)),
                                            groupValue: viewModel.serviceFee,
                                            value: FeeType.customer,
                                            onChanged: (FeeType? newValue) {
                                              viewModel.setServiceFee(newValue);
                                            },
                                          ),
                                          Text('ผู้ซื้อ'),
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              ])
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                          child: new Text(
                              "การตั้งค่าเหล่านี้จะมีผลต่อยอดรวมการสั่งซื้อและรายได้จากการสั่งซื้อ",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(top: 12, left: 16, right: 16),
                          child: new Text("วิธีคำนวณค่าบริการ",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          margin: const EdgeInsets.only(top: 10, left: 16, right: 16),
                          height: 176,
                          child: Container(
                            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: TextFormField(
                                      controller: viewModel.price,
                                      autovalidateMode: AutovalidateMode.always,
                                      autofocus: false,
                                      maxLength: 20,
                                      style: textStyle(),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.allow(RegExp(r'\d+(\.)?'))
                                      ],
                                      onChanged: (value) {
                                        viewModel.calculate();
                                      },
                                      decoration: InputDecoration(
                                          labelText: 'ราคาบัตร(THB)',
                                          isDense: true,
                                          filled: true,
                                          fillColor: Colors.white,
                                          labelStyle: textStyle(fontSize: 12),
                                          border: OutlineInputBorder(),
                                          counterText: ''),
                                    )),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        controller: viewModel.amount,
                                        onChanged: (value) {
                                          viewModel.calculate();
                                        },
                                        keyboardType: TextInputType.number,
                                        autovalidateMode: AutovalidateMode.always,
                                        autofocus: false,
                                        maxLength: 200,
                                        style: textStyle(),
                                        onTap: () {},
                                        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                        decoration: InputDecoration(
                                            labelText: 'จำนวน',
                                            isDense: true,
                                            filled: true,
                                            fillColor: Colors.white,
                                            labelStyle: textStyle(fontSize: 12),
                                            border: OutlineInputBorder(),
                                            counterText: ''),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: const EdgeInsets.only(top: 16),
                                  child: Row(
                                    children: [
                                      RichText(
                                          text: new TextSpan(children: [
                                        new TextSpan(
                                            text: "ค่าบริการ",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        new TextSpan(
                                            text: "(3% VAT incl.)",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ])),
                                      Spacer(),
                                      Text(
                                          "฿${viewModel.calculateTicketData?.fees?.serviceFee ?? 0}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  ),
                                ),
                                Container(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: Row(children: [
                                      RichText(
                                          text: new TextSpan(children: [
                                        new TextSpan(
                                            text: "ค่าธรรมเนียมชำระเงิน",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        new TextSpan(
                                            text: "(3% VAT incl.)",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ])),
                                      Spacer(),
                                      new Text(
                                          "฿${viewModel.calculateTicketData?.fees?.paymentFee ?? 0}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]))
                              ],
                            ),
                          ),
                          decoration: new BoxDecoration(
                              color: Color(0xFFF4F4F4), borderRadius: BorderRadius.circular(4)))),
                  SliverToBoxAdapter(
                      child: Container(
                          decoration: new BoxDecoration(
                              color: Color(0xFFF4F4F4), borderRadius: BorderRadius.circular(4)),
                          margin: const EdgeInsets.only(top: 10, left: 16, right: 16),
                          height: 176,
                          child: Container(
                              padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Column(
                                      children: [
                                        new Text("ค่าบริการ",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        ToggleSwitch(
                                          minWidth: (Screen.width - 80) / 4,
                                          cornerRadius: 4,
                                          activeBgColor: [Color(0xff6d6d6d), Color(0xff6d6d6d)],
                                          inactiveBgColor: Color(0xffe6e7ec),
                                          activeFgColor: Colors.white,
                                          animate: true,
                                          initialLabelIndex: viewModel.paymentFeeCal.index,
                                          totalSwitches: 2,
                                          labels: ['Absorb', 'Pass on'],
                                          customTextStyles: [
                                            TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              fontSize: 14,
                                              color: viewModel.paymentFeeCal.index == 0
                                                  ? Colors.white
                                                  : Color(0xFF6D6D6D),
                                              fontWeight: FontWeight.w500,
                                              fontStyle: FontStyle.normal,
                                            ),
                                            TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              fontSize: 14,
                                              color: viewModel.paymentFeeCal.index == 1
                                                  ? Colors.white
                                                  : Color(0xFF6D6D6D),
                                              fontWeight: FontWeight.w500,
                                              fontStyle: FontStyle.normal,
                                            )
                                          ],
                                          onToggle: (index) {
                                            viewModel.setPaymentFeeCal(index);
                                          },
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Column(
                                      children: [
                                        new Text("ค่าบริการชำระเงิน",
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff08080a),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        ToggleSwitch(
                                          minWidth: (Screen.width - 84) / 4,
                                          cornerRadius: 4,
                                          activeBgColor: [Color(0xff6d6d6d), Color(0xff6d6d6d)],
                                          inactiveBgColor: Color(0xffe6e7ec),
                                          animate: true,
                                          initialLabelIndex: viewModel.serviceFeeCal.index,
                                          totalSwitches: 2,
                                          labels: ['Absorb', 'Pass on'],
                                          customTextStyles: [
                                            TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              fontSize: 14,
                                              color: viewModel.serviceFeeCal.index == 0
                                                  ? Colors.white
                                                  : Color(0xFF6D6D6D),
                                              fontWeight: FontWeight.w500,
                                              fontStyle: FontStyle.normal,
                                            ),
                                            TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              fontSize: 14,
                                              color: viewModel.serviceFeeCal.index == 1
                                                  ? Colors.white
                                                  : Color(0xFF6D6D6D),
                                              fontWeight: FontWeight.w500,
                                              fontStyle: FontStyle.normal,
                                            )
                                          ],
                                          onToggle: (index) {
                                            viewModel.setServiceFeeCal(index);
                                          },
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                Container(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: Row(children: [
                                      new Text("ยอดเงินที่ผู้ซื้อบัตรต้องจ่าย",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Spacer(),
                                      new Text(
                                          "฿${viewModel.calculateTicketData?.amounts?.customerPay ?? 0}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ])),
                                Container(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: Row(children: [
                                      new Text("ยอดเงินที่คุณจะได้รับ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Spacer(),
                                      new Text(
                                          "฿${viewModel.calculateTicketData?.amounts?.organizerReceive ?? 0}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]))
                              ])))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: RichText(
                        text: new TextSpan(children: [
                      new TextSpan(
                          text:
                              "ไม่มีค่าใช้จ่ายสำหรับบัตรฟรี ไม่มีค่าแรกเข้าหรือค่าใช้จ่ายในการใช้งาน",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff6d6d6d),
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                      new TextSpan(
                          text:
                              "เราคิดค่าธรรมเนียมจากยอดขายจริงของอีเว้นท์เท่านั้น ซึ่งสามารถให้ผู้ซื้อบัตรเป็นผู้จ่ายได้",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                    ])),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Color(0xFFE0E0E0), width: 1)),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(top: 14),
                                child: new Text("ตั้งค่าการชำระเงิน",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff08080a),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ),
                              Container(
                                  alignment: Alignment.center,
                                  child: new Text("ช่องทางการชำระเงินสำหรับการจำหน่ายบัตร",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff08080a),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ))),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(color: Color(0xFFE0E0E0), width: 1))),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          new Text("บัตรเครดิตและบัตรเดบิต",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Spacer(),
                                          Switch(
                                            value: viewModel.ccw,
                                            onChanged: (bool isOn) {
                                              viewModel.setCcw(isOn);
                                            },
                                            activeColor: Color(0xFFDA3534),
                                            inactiveTrackColor: Color(0xFFCACACA),
                                            inactiveThumbColor: Color(0xFFF4F4F4),
                                          )
                                        ],
                                      ),
                                      new Text(
                                          "รองรับการชำระเงินผ่านเครือข่ายบัตรเครดิตชั้นนำทั่วโลก",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  )),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(color: Color(0xFFE0E0E0), width: 1))),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          new Text("อินเทอร์เน็ตแบงก์กิ้ง",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Spacer(),
                                          Switch(
                                            value: viewModel.iBanking,
                                            onChanged: (bool isOn) {
                                              viewModel.setIBanking(isOn);
                                            },
                                            activeColor: Color(0xFFDA3534),
                                            inactiveTrackColor: Color(0xFFCACACA),
                                            inactiveThumbColor: Color(0xFFF4F4F4),
                                          )
                                        ],
                                      ),
                                      new Text("รองรับการทำธุรกรรมออนไลน์ผ่านบัญชีธนาคารบนมือถือ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  )),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(color: Color(0xFFE0E0E0), width: 1))),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          new Text("ชำระเงินผ่านบิล",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Spacer(),
                                          Switch(
                                            value: viewModel.bill,
                                            onChanged: (bool isOn) {
                                              viewModel.setBill(isOn);
                                            },
                                            activeColor: Color(0xFFDA3534),
                                            inactiveTrackColor: Color(0xFFCACACA),
                                            inactiveThumbColor: Color(0xFFF4F4F4),
                                          )
                                        ],
                                      ),
                                      new Text(
                                          "ชำระเงินผ่านร้านสะดวกซื้ออย่างเทสโก้โลตัสด้วยบาร์โค้ด",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  )),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(color: Color(0xFFE0E0E0), width: 1))),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          new Text("พร้อมเพย์",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Spacer(),
                                          Switch(
                                            value: viewModel.promptPay,
                                            onChanged: (bool isOn) {
                                              viewModel.setPromptPay(isOn);
                                            },
                                            activeColor: Color(0xFFDA3534),
                                            inactiveTrackColor: Color(0xFFCACACA),
                                            inactiveThumbColor: Color(0xFFF4F4F4),
                                          )
                                        ],
                                      ),
                                      new Text(
                                          "ชำระเงินผ่านแอปโมบายแบงก์กิ้งต่างๆ เพียงสแกน QR Code ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  )),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(
                                      top: 16, left: 16, right: 16, bottom: 14),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          new Text("ผ่อนชำระ",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Spacer(),
                                          Switch(
                                            value: viewModel.installment,
                                            onChanged: (bool isOn) {
                                              viewModel.setInstallment(isOn);
                                            },
                                            activeColor: Color(0xFFDA3534),
                                            activeTrackColor: Color(0xfff5c5c4),
                                            inactiveTrackColor: Color(0xFFCACACA),
                                            inactiveThumbColor: Color(0xFFF4F4F4),
                                          )
                                        ],
                                      ),
                                      new Text("การชำระเงินแบบผ่อนชำระผ่านบัตรเครดิต ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  ))
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
                          child: Row(
                            children: [
                              Checkbox(
                                splashRadius: 4,
                                value: viewModel.isChecked,
                                checkColor: Colors.white,
                                shape:
                                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                fillColor: MaterialStateProperty.all<Color>(Color(0xFFDA3534)),
                                onChanged: (bool? value) {
                                  viewModel.setChecked(value);
                                },
                              ),
                              Expanded(
                                  child: InkWell(
                                //onTap: ()=> TixNavigate.instance.navigateTo(TicketTermAndCondition()),
                                onTap: () => WrapNavigation.instance
                                    .pushNamed(context, TicketTermAndCondition()),
                                child: RichText(
                                    text: new TextSpan(children: [
                                  new TextSpan(
                                      text: "รับทราบและยอมรับเงื่อนไข ",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff6d6d6d),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      )),
                                  new TextSpan(
                                      text: "อ่านรายละเอียดข้อตกลง",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Colors.blue,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ])),
                              ))
                            ],
                          ))),
                  SliverFillRemaining(
                      hasScrollBody: false,
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 24),
                            width: double.infinity,
                            height: 42,
                            child: ElevatedButton(
                                child: Text(
                                  "ยืนยัน",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color:
                                        viewModel.isChecked ? Color(0xffffffff) : Color(0xffb2b2b2),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                style: ButtonStyle(
                                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                    backgroundColor: MaterialStateProperty.all<Color>(
                                        viewModel.isChecked
                                            ? Color(0xFFDA3534)
                                            : Color(0xffe6e7ec)),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(21),
                                    ))),
                                onPressed: () =>
                                    viewModel.isChecked ? viewModel.save(context) : null),
                          )))
                ])),
              ));
        });
  }

  Widget header(String header) {
    return Container(
        height: 37,
        child: new Text(header,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff6d6d6d),
              fontSize: 14,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            )),
        alignment: Alignment.center,
        decoration: new BoxDecoration(color: Color(0xFFF4F4F4)));
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
