import 'dart:convert';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_ticket_setting_entity.dart';
import 'package:app/model/save_ticket_setting_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class PaymentChannelViewModel extends BaseViewModel {
  final int concertId;
  final String storeId;

  bool ccw = false;
  bool iBanking = false;
  bool bill = false;
  bool promptPay = false;
  bool installment = false;
  bool isChecked = false;

  GetTicketSettingData? getTicketSettingData;

  PaymentChannelViewModel(this.concertId, this.storeId);

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  @override
  void postInit() {
    super.postInit();
    getConcertSetting();
  }

  void setCcw(bool value) {
    ccw = value;
    notifyListeners();
  }

  void setIBanking(bool value) {
    iBanking = value;
    notifyListeners();
  }

  void setBill(bool value) {
    bill = value;
    notifyListeners();
  }

  void setPromptPay(bool value) {
    promptPay = value;
    notifyListeners();
  }

  void setInstallment(bool value) {
    installment = value;
    notifyListeners();
  }

  void getConcertSetting() {
    catchError(() async {
      setLoading(true);
      getTicketSettingData = await di.concertRepository.getConcertSetting(concertId, storeId);
      var methodValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'method').value;
      ccw = methodValue?.ccw?.status ?? false;
      iBanking = methodValue?.ibanking?.status ?? false;
      bill = methodValue?.bill?.status ?? false;
      promptPay = methodValue?.promptpay?.status ?? false;
      installment = methodValue?.installment?.status ?? false;
      setLoading(false);
    });
  }

  save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      var methodValue =
          getTicketSettingData?.record?.firstWhere((element) => element.key == 'method').value;
      methodValue!.ccw!.status = ccw;
      methodValue.ibanking!.status = iBanking;
      methodValue.bill!.status = bill;
      methodValue.promptpay!.status = promptPay;
      methodValue.installment!.status = installment;
      var methodValueSave = json.encode(methodValue);

      SaveTicketSettingSettings method = SaveTicketSettingSettings()
        ..key = 'method'
        ..code = 'payment'
        ..value = methodValueSave;

      SaveTicketSettingEntity save = SaveTicketSettingEntity();
      save.storeId = storeId;
      save.settings = [method];

      var result = await di.concertRepository.updateConcertTicketSetting(concertId, save);

      if (result != null) {
        //TixNavigate.instance.pop(data:result );
        WrapNavigation.instance.pop(context, data: result);
      }
      setLoading(false);
    });
  }
}
