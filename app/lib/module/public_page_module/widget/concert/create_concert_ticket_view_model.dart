import 'package:app/core/base_view_model.dart';
import 'package:app/model/save_concert_ticket_entity.dart';
import 'package:app/utils/date_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

class CreateConcertTicketViewModel extends BaseViewModel {
  final int concertId;
  final String storeId;
  final int? ticketId;

  TextEditingController ticketName = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController price = TextEditingController();
  TextEditingController salePrice = TextEditingController();
  TextEditingController amount = TextEditingController();
  TextEditingController lowAmount = TextEditingController();
  TextEditingController highAmount = TextEditingController();
  TextEditingController perPackage = TextEditingController();
  bool isActive = false;
  bool isPackage = false;
  bool hasTicket = false;
  String? ticket;

  DateTime saleStartDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10);
  DateTime saleEndDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 10);

  DateTime concertStartDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10);
  DateTime concertEndDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 10);

  DateTime maxDateTime =
      DateTime(DateTime.now().year + 5, DateTime.now().month, DateTime.now().day + 1, 10);

  TextEditingController saleStartDate = TextEditingController();
  TextEditingController saleStartTime = TextEditingController();
  TextEditingController saleEndDate = TextEditingController();
  TextEditingController saleEndTime = TextEditingController();
  TextEditingController concertStartDate = TextEditingController();
  TextEditingController concertStartTime = TextEditingController();
  TextEditingController concertEndDate = TextEditingController();
  TextEditingController concertEndTime = TextEditingController();
  int type = 0;

  CreateConcertTicketViewModel(this.concertId, this.storeId, this.ticketId);

  @override
  void postInit() {
    super.postInit();
    if (ticketId != null) {
      getTicket();
    }
    formatDate();
  }

  check() {
    isActive = validateTicketName(ticketName.text) == null &&
        validateSalePrice(salePrice.text) == null &&
        validateAmount(amount.text) == null &&
        validateLowAmount(lowAmount.text) == null &&
        validateHighAmount(highAmount.text) == null &&
        validateSaleStartDate(null) == null &&
        validateConcertStartDate(null) == null &&
        validatePerPackage(perPackage.text) == null;
    notifyListeners();
  }

  String? validateConcertStartDate(String? value) {
    if (!concertStartDateTime.isBefore(concertEndDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  String? validateSaleStartDate(String? value) {
    if (!saleStartDateTime.isBefore(saleEndDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  String? validateSale(String? value) {
    if (value == null || value.isEmpty) {
      return null;
    }
    if (value.isNotEmpty) {
      if (salePrice.text.isNotEmpty) {
        if (int.parse(value) < int.parse(salePrice.text)) {
          return 'กรุณากรอกราคาตั้งมากกว่าราคาขาย';
        }
      }
      if (int.parse(value) < 0) {
        return 'กรุณากรอกราคาให้ถูกต้อง';
      }
    }

    return null;
  }

  String? validateTicketName(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อบัตร';
    }
    return null;
  }

  String? validateHighAmount(String? value) {
    if (value != null && value.isNotEmpty && lowAmount.text.isNotEmpty) {
      if (int.parse(value) < int.parse(lowAmount.text)) {
        return 'จำนวนสูงสุด ต้อมีค่ามากกว่า';
      }
    }
    return validateInt(value, 'จำนวนซื้อสูงสุด');
  }

  String? validateLowAmount(String? value) {
    return validateInt(value, 'จำนวนซื้อต่ำสุด');
  }

  String? validateAmount(String? value) {
    return validateInt(value, 'จำนวน');
  }

  String? validateSalePrice(String? value) {
    return validateInt(value, 'ราคาขาย');
  }

  String? validatePerPackage(String? value) {
    if (isPackage == false) {
      return null;
    }
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกจำนวนบัตร';
    }
    if (int.parse(value) < 1) {
      return 'กรุณาจำนวนบัตรมากกว่า 0';
    }
    return null;
  }

  String? validateInt(String? value, String wording) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอก$wording';
    }
    if (int.parse(value) < 0) {
      return 'กรุณากรอก$wordingให้ถูกต้อง';
    }
    return null;
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  setSaleDatePicker(DateTime time, bool isStart) {
    print('sss');
    if (isStart) {
      saleStartDateTime = DateTime(
          time.year, time.month, time.day, saleStartDateTime.hour, saleStartDateTime.minute);
    } else {
      saleEndDateTime =
          DateTime(time.year, time.month, time.day, saleEndDateTime.hour, saleEndDateTime.minute);
    }
    formatDate();
    check();
    notifyListeners();
  }

  setSaleTimePicker(DateTime time, bool isStart) {
    if (isStart) {
      saleStartDateTime = DateTime(saleStartDateTime.year, saleStartDateTime.month,
          saleStartDateTime.day, time.hour, time.minute);
    } else {
      saleEndDateTime = DateTime(
          saleEndDateTime.year, saleEndDateTime.month, saleEndDateTime.day, time.hour, time.minute);
    }
    formatDate();
    check();
    notifyListeners();
  }

  setConcertDatePicker(DateTime time, bool isStart) {
    if (isStart) {
      concertStartDateTime = DateTime(
          time.year, time.month, time.day, concertStartDateTime.hour, concertStartDateTime.minute);
    } else {
      concertEndDateTime = DateTime(
          time.year, time.month, time.day, concertEndDateTime.hour, concertEndDateTime.minute);
    }
    formatDate();
    check();
    notifyListeners();
  }

  setConcertTimePicker(DateTime time, bool isStart) {
    if (isStart) {
      concertStartDateTime = DateTime(concertStartDateTime.year, concertStartDateTime.month,
          concertStartDateTime.day, time.hour, time.minute);
    } else {
      concertEndDateTime = DateTime(concertEndDateTime.year, concertEndDateTime.month,
          concertEndDateTime.day, time.hour, time.minute);
    }
    formatDate();
    check();
    notifyListeners();
  }

  void formatDate() {
    saleStartDate.text = DateFormat('dd MMM ', 'th').format(saleStartDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(saleStartDateTime)) + 543).toString();
    saleStartTime.text = DateFormat('HH:mm', 'th').format(saleStartDateTime);
    saleEndDate.text = DateFormat('dd MMM ', 'th').format(saleEndDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(saleEndDateTime)) + 543).toString();
    saleEndTime.text = DateFormat('HH:mm', 'th').format(saleEndDateTime);

    concertStartDate.text = DateFormat('dd MMM ', 'th').format(concertStartDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(concertStartDateTime)) + 543).toString();
    concertStartTime.text = DateFormat('HH:mm', 'th').format(concertStartDateTime);
    concertEndDate.text = DateFormat('dd MMM ', 'th').format(concertEndDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(concertEndDateTime)) + 543).toString();
    concertEndTime.text = DateFormat('HH:mm', 'th').format(concertEndDateTime);
    notifyListeners();
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      SaveConcertTicketEntity save = SaveConcertTicketEntity()
        ..storeId = storeId
        ..productId = concertId.toString()
        ..name = ticketName.text
        ..description = description.text
        ..compareAtPrice = price.text
        ..price = salePrice.text
        ..quantity = '+' + amount.text
        ..stock = '+' + amount.text
        ..allowOrderMin = lowAmount.text
        ..allowOrderMax = highAmount.text
        ..package = isPackage
        ..perPackage = isPackage ? int.parse(perPackage.text) : 0
        ..hasTicket = false
        ..serviceCharge = false
        ..status = false
        ..serviceFee = '0.00'
        ..sku = ''
        ..createdBy = globals.userDetail!.data!.id
        ..publishedStart = DateFormatUtils.format(saleStartDateTime)
        ..publishedEnd = DateFormatUtils.format(saleEndDateTime)
        ..gateOpen = DateFormatUtils.format(concertStartDateTime)
        ..gateClose = DateFormatUtils.format(concertEndDateTime);
      var result;
      if (ticketId != null) {
        result = await di.concertRepository.updateConcertTicket(concertId, ticketId!, save);
      } else {
        result = await di.concertRepository.saveConcertTicket(concertId, save);
      }

      //TixNavigate.instance.pop(data: result);
      WrapNavigation.instance.pop(context, data: result);
      setLoading(false);
    });
  }

  void setTicketType(String? newValue) {
    if (newValue == 'บัตรทั่วไป') {
      isPackage = false;
    }
    if (newValue == 'บัตรแพ็คเกจ') {
      isPackage = true;
    }
    notifyListeners();
  }

  void getTicket() {
    catchError(() async {
      setLoading(true);
      var ticket = await di.concertRepository.getConcertTicketById(concertId, ticketId!, storeId);
      type = (ticket?.package ?? false) ? 1 : 0;
      ticketName.text = ticket?.name ?? '';
      description.text = ticket?.description ?? '';
      if ((ticket?.costPrice ?? 0) > 0) {
        price.text = ticket?.costPrice?.toString() ?? '';
      }
      salePrice.text = ticket?.price?.toString() ?? '';
      amount.text = ticket?.quantity?.toString() ?? '';
      lowAmount.text = ticket?.allowOrderMin?.toString() ?? '';
      highAmount.text = ticket?.allowOrderMax?.toString() ?? '';
      perPackage.text = ticket?.perPackage?.toString() ?? '';
      perPackage.text = ticket?.perPackage?.toString() ?? '';
      hasTicket = ticket?.hasTicket ?? false;
      saleStartDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:sss")
          .parse(ticket?.publishedStart ?? '')
          .add(Duration(hours: 7));
      saleEndDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:sss")
          .parse(ticket?.publishedEnd ?? '')
          .add(Duration(hours: 7));
      concertStartDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:sss")
          .parse(ticket?.gateOpen ?? '')
          .add(Duration(hours: 7));
      concertEndDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:sss")
          .parse(ticket?.gateClose ?? '')
          .add(Duration(hours: 7));
      check();
      setLoading(false);
    });
  }

  delete(BuildContext context) {
    catchError(() async {
      setLoading(true);
      var result = await di.concertRepository.deleteConcertTicket(concertId, ticketId!, storeId);
      //TixNavigate.instance.pop(data: result);
      WrapNavigation.instance.pop(context, data: result);
      setLoading(false);
    });
  }
}
