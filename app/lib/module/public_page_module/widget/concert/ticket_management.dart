import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_management_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TicketManagement extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;

  TicketManagement({Key? key, this.concertId, this.storeId}) : super(key: key);

  @override
  _TicketManagement createState() => _TicketManagement();

  @override
  String buildPath() {
    return '/ticket_management';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => TicketManagement(
              concertId: data['concertId'],
              storeId: data['storeId'],
            ));
  }
}

class _TicketManagement
    extends BaseStateProvider<TicketManagement, TicketManagementViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = TicketManagementViewModel(widget.concertId!,widget.storeId!);

  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TicketManagementViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          var image = viewModel.getConcertByIdData?.images
              ?.firstWhere((element) => element.tag == 'logo');
          var artist = '';
          if((viewModel.getConcertByIdData?.attributes??[]).where((element) => element.code == 'artist').isNotEmpty){
            artist = viewModel.getConcertByIdData?.attributes
                ?.firstWhere((element) => element.code == 'artist')
                .items
                ?.map((e) => e.name)
                .join(', ')??'';
          }
          var genre = '';
          if((viewModel.getConcertByIdData?.attributes??[]).where((element) => element.code == 'genre').isNotEmpty){
               genre = viewModel.getConcertByIdData?.attributes
                ?.firstWhere((element) => element.code == 'genre')
                .items
                ?.map((e) => e.name)
                .join(', ')??'';
          }

          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    appBar: AppBarWidget(
                      name: viewModel.getConcertByIdData?.name ?? '',
                    ),
                    body: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 48,
                          child: TabBar(
                            indicatorColor: Color(0xffda3534),
                            labelColor: Color(0xff6d6d6d),
                            labelStyle: TextStyle(
                              fontFamily: 'SukhumvitSet',
                              color: Color(0xff6d6d6d),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                            tabs: [
                              Tab(
                                text: 'คอนเสิร์ต',
                              ),
                              Tab(
                                text: 'ยอดขายและรายงาน',
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: TabBarView(
                            children: [
                              CustomScrollView(slivers: [
                                SliverToBoxAdapter(
                                    child: Container(
                                  width: Screen.width,
                                  height: 350,
                                  child: Image(
                                    image: NetworkImage(image?.url ?? ''),
                                    fit: BoxFit.cover,
                                      height: 350,
                                    errorBuilder:
                                        (context, error, stackTrace) =>
                                        Image(
                                            width: 36,
                                            height: 36,
                                            image: AssetImage(
                                                "assets/no_image.jpeg"))
                                  ),
                                )),
                                SliverToBoxAdapter(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xFFE0E0E0),
                                                    width: 1))),
                                        padding: EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                            top: 16,
                                            bottom: 9),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                viewModel.getConcertByIdData
                                                        ?.showTime?.textShort ??
                                                    '',
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SukhumvitSet-Text',
                                                  color: Color(0xffda3534),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                            new Text(
                                                viewModel.getConcertByIdData
                                                        ?.name ??
                                                    '',
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SukhumvitSet-Text',
                                                  color: Color(0xff08080a),
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w700,
                                                  fontStyle: FontStyle.normal,
                                                ))
                                          ],
                                        ))),
                                SliverToBoxAdapter(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xFFE0E0E0),
                                                    width: 1))),
                                        padding: EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                            top: 16,
                                            bottom: 6),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Image(
                                              image: AssetImage(
                                                  'assets/marker.png'),
                                              width: 15,
                                              height: 18,
                                            ),
                                            SizedBox(
                                              width: 12,
                                            ),
                                            Expanded(
                                                child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                  new Text("สถานที่",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff333333),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                  new Text(
                                                      viewModel
                                                              .getConcertByIdData
                                                              ?.venue
                                                              ?.name ??
                                                          '',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff555555),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                ]))
                                          ],
                                        ))),
                                SliverToBoxAdapter(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xFFE0E0E0),
                                                    width: 1))),
                                        padding: EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                            top: 16,
                                            bottom: 6),
                                        child: Row(
                                          children: [
                                            Image(
                                              image: AssetImage(
                                                  'assets/users.png'),
                                              width: 15,
                                              height: 15,
                                            ),
                                            SizedBox(
                                              width: 12,
                                            ),
                                            Expanded(
                                                child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                  new Text("ศิลปิน",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff333333),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                  new Text(artist,
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff555555),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                ]))
                                          ],
                                        ))),
                                SliverToBoxAdapter(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xFFE0E0E0),
                                                    width: 1))),
                                        padding: EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                            top: 16,
                                            bottom: 6),
                                        child: Row(
                                          children: [
                                            Image(
                                              image: AssetImage(
                                                  'assets/music.png'),
                                              width: 15,
                                              height: 15,
                                            ),
                                            SizedBox(
                                              width: 12,
                                            ),
                                            Expanded(
                                                child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                  new Text("แนวเพลง",
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff333333),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                  new Text(genre,
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'SukhumvitSet-Text',
                                                        color:
                                                            Color(0xff555555),
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        letterSpacing: -0.5,
                                                      )),
                                                ]))
                                          ],
                                        ))),
                                SliverToBoxAdapter(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Color(0xFFE0E0E0),
                                                    width: 1))),
                                        padding: EdgeInsets.only(
                                            left: 16,
                                            right: 16,
                                            top: 16,
                                            bottom: 6),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            new Text("รายละเอียด",
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SukhumvitSet-Text',
                                                  color: Color(0xff08080a),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                            new Text(
                                                viewModel.getConcertByIdData
                                                        ?.description ??
                                                    '',
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SukhumvitSet-Text',
                                                  color: Color(0xff6d6d6d),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                ))
                                          ],
                                        )))
                              ]),
                              Icon(Icons.directions_transit),
                            ],
                          ),
                        )
                      ],
                    ),
                    bottomNavigationBar: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    color: Color(0xFFE0E0E0), width: 1))),
                        padding: const EdgeInsets.only(
                            top: 5, left: 16, bottom: 5, right: 16),
                        child: Row(children: [
                          buttonNavigate(
                              'assets/ticket_icon.png', 'เปิดขายบัตร', viewModel.navigateToTicketList(context)),
                          buttonNavigate('assets/pencil.png', 'แก้ไข', viewModel.navigateToEditConcert(context)),
                          buttonNavigate('assets/tag_icon.png', 'โค้ดส่วนลด',viewModel.navigateToDiscountScreen(context)),
                          buttonNavigate(
                              'assets/dot-menu.png', 'ตั้งค่า', viewModel.navigateToManage(context)),
                        ])),
                  ))
          );
        });
  }

  Widget buttonNavigate(String icon, String detail, Function()? onTap) {
    return Expanded(
        child: InkWell(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircleAvatar(
            backgroundColor: Color(0xFFE0E0E0),
            child: Image(
              image: AssetImage(icon),
              color: Colors.black,
              width: 18,
              height: 17,
            ),
          ),
          Text(detail,
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff333333),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.5,
              ))
        ],
      ),
    ));
  }
}
