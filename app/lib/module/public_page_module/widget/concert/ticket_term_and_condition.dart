import 'dart:async';
import 'dart:io';

import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TicketTermAndCondition extends StatefulWidget with TixRoute {
  @override
  _TicketTermAndCondition createState() => _TicketTermAndCondition();

  @override
  String buildPath() {
    return '/ticket_term_and_condition';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => TicketTermAndCondition());
  }
}

class _TicketTermAndCondition extends State<TicketTermAndCondition> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Screen.width,
        child: Expanded(
          child: Column(
            children: [
              Expanded(
                child: Container(
                  child: InAppWebView(
                    initialUrlRequest: URLRequest(
                        url: Uri.parse("https://alpha-www.theconcert.com/page/ticket-policy")),
                    initialOptions: InAppWebViewGroupOptions(crossPlatform: InAppWebViewOptions()),
                    onWebViewCreated: (InAppWebViewController controller) {},
                    onLoadStart: (InAppWebViewController controller, Uri? url) {},
                    onLoadStop: (InAppWebViewController controller, Uri? url) async {},
                    onProgressChanged: (InAppWebViewController controller, int progress) {},
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
