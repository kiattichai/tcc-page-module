import 'package:app/module/public_page_module/widget/concert/payment_channel.dart';
import 'package:app/module/public_page_module/widget/concert/set_fee_concert.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_scan.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_tracking.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ManageConcertMenu extends StatelessWidget with TixRoute {
  int? concertId;
  String? storeId;
  ManageConcertMenu({Key? key, this.concertId, this.storeId}) : super(key: key);

  @override
  String buildPath() {
    return '/mange_concert_menu_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => ManageConcertMenu(
              storeId: data['storeId'],
              concertId: data['concertId'],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'ตั้งค่า',
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 16, right: 16, top: 5),
          child: Column(children: [
            menu('assets/icon-fee.png', 'ตั้งค่าธรรมเนียมบัตร', navigateToSetFee(context)),
            menu('assets/icon-card.png', 'ตั้งค่าช่องทางการชำระเงิน',
                navigateToPaymentChannel(context)),
            menu('assets/icon-gate.png', 'สร้างระบบสแกนบัตรเข้างาน (Gate App)',
                navigateToTicketScan(context)),
            menu('assets/icon-analytics.png', 'การวิเคราะห์และติดตาม',
                navigateToTicketTracking(context)),
          ]),
        ),
      ),
    );
  }

  menu(String icon, String name, Function()? onTap) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.only(top: 11.5),
        child: Column(
          children: [
            Row(
              children: [
                Image(
                  image: AssetImage(icon),
                  color: Color(0xff333333),
                  width: 16,
                  height: 16,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 19),
                  child: Text(name, style: textStyle()),
                )
              ],
            ),
            Container(
              width: Screen.width - 67,
              margin: const EdgeInsets.only(left: 35, top: 11.5),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(width: 1, color: Color(0xFFDDDDDD)))),
            )
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  navigateToSetFee(BuildContext context) async {
    //var result = await TixNavigate.instance.navigateTo(SetFeeConcert(), data: {'concertId': concertId,'storeId':storeId,'isPop':true});
    var result = await WrapNavigation.instance.pushNamed(context, SetFeeConcert(),
        arguments: {'concertId': concertId, 'storeId': storeId, 'isPop': true});
  }

  navigateToPaymentChannel(BuildContext context) async {
    //var result = await TixNavigate.instance.navigateTo(PaymentChannel(), data: {'concertId': concertId,'storeId':storeId});
    var result = await WrapNavigation.instance.pushNamed(context, PaymentChannel(),
        arguments: {'concertId': concertId, 'storeId': storeId});
  }

  navigateToTicketScan(BuildContext context) async {
    //var result = await TixNavigate.instance.navigateTo(TicketScan(), data: {'concertId': concertId,'storeId':storeId});
    var result = await WrapNavigation.instance
        .pushNamed(context, TicketScan(), arguments: {'concertId': concertId, 'storeId': storeId});
  }

  navigateToTicketTracking(BuildContext context) async {
    //var result = await TixNavigate.instance.navigateTo(TicketTracking(), data: {'concertId': concertId,'storeId':storeId});
    var result = await WrapNavigation.instance.pushNamed(context, TicketTracking(),
        arguments: {'concertId': concertId, 'storeId': storeId});
  }

  textStyle() {
    return TextStyle(
        fontFamily: 'SukhumvitSet-Text',
        color: Color(0xff333333),
        fontSize: 16,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        letterSpacing: -0.5);
  }
}
