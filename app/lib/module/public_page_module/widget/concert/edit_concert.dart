import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/edit_concert_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/review/create_review_sucess_dialog.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditConcert extends StatefulWidget with TixRoute {
  final int? concertId;

  EditConcert({Key? key, this.concertId}) : super(key: key);

  @override
  _EditConcert createState() => _EditConcert();

  @override
  String buildPath() {
    return '/edit_concert';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditConcert(
              concertId: data['concertId'],
            ));
  }
}

class _EditConcert
    extends BaseStateProvider<EditConcert, EditConcertViewModel> {
  @override
  void initState() {
    viewModel = EditConcertViewModel(widget.concertId!);
    viewModel.showSaveSuccess = showSaveSuccess;
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EditConcertViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          var image = viewModel.getConcertByIdData?.images
              ?.firstWhere((element) => element.tag == 'logo');

          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'จัดการคอนเสิร์ต',
                ),
                body: SafeArea(
                    child: CustomScrollView(slivers: [
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: new Text("เลือกภาพสำหรับคอนเสิร์ต",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 10, left: 16, right: 16, bottom: 10),
                          child: new Text(
                              "เพิ่มภาพที่น่าสนใจเกี่ยวกับคอนเสิร์ตของคุณ เพื่อดึงดูดให้คนอยากไปเข้าร่วม ขนาดที่แนะนำ 800 x 1200 px",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                    decoration: BoxDecoration(
                      image: viewModel.concertImage != null
                          ? DecorationImage(
                              image: FileImage(viewModel.concertImage!),
                              alignment: Alignment.center)
                          : DecorationImage(
                              image: NetworkImage(image?.url ?? ''),
                              alignment: Alignment.center),
                      color: Color(0xFFE0E0E0),
                    ),
                    height: 350,
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: InkWell(
                            onTap: () => viewModel.pickProfileImage(),
                            child: Container(
                                margin: const EdgeInsets.only(bottom: 16),
                                width: 119,
                                height: 37,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: AssetImage('assets/gallery.png'),
                                        width: 18,
                                        height: 18,
                                        color: Colors.black,
                                      ),
                                      SizedBox(
                                        width: 11,
                                      ),
                                      Text("เลือกภาพ",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff4a4a4a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                decoration: new BoxDecoration(
                                  color: Color(0xFFF4F4F4),
                                  borderRadius: BorderRadius.circular(5),
                                )),
                          ),
                        )
                      ],
                    ),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: new Text("รายละเอียดคอนเสิร์ต",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        controller: viewModel.concertName,
                        style: textStyle(),
                        validator: viewModel.validateConcertName,
                        decoration: InputDecoration(
                            labelText: 'ชื่อคอนเสิร์ต',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 100,
                        onChanged: (text) {
                          viewModel.check();
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        maxLines: 4,
                        controller: viewModel.description,
                        validator: viewModel.validateDescription,
                        style: textStyle(),
                        decoration: InputDecoration(
                            alignLabelWithHint: true,
                            labelText: 'รายละเอียด',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 300,
                        onChanged: (text) {
                          viewModel.check();
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: new Text("วันและเวลาแสดง",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 10, left: 16, right: 16),
                          child: new Text(
                              "ระบุวันที่และเวลาแสดงให้ชัดเจน เพื่อให้ทุกคนทราบวันและเวลาแสดงที่ถูกต้อง",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    onChanged: (text) {
                                      // viewModel.check();
                                    },
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.startDate,
                                    maxLength: 200,
                                    style: textStyle(),
                                    readOnly: true,
                                    validator: viewModel.validateStartDate,
                                    onTap: () =>
                                        viewModel.showDatePicker(context, true),
                                    decoration: InputDecoration(
                                        labelText: 'เริ่มเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  onChanged: (text) {
                                    // viewModel.check();
                                  },
                                  autovalidateMode: AutovalidateMode.always,
                                  autofocus: false,
                                  controller: viewModel.startTime,
                                  maxLength: 200,
                                  style: textStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateStartDate,
                                  onTap: () =>
                                      viewModel.showTimePicker(context, true),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาเริ่มต้น',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    onChanged: (text) {
                                      // viewModel.check();
                                    },
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.endDate,
                                    maxLength: 200,
                                    style: textStyle(),
                                    readOnly: true,
                                    validator: viewModel.validateEndDate,
                                    onTap: () => viewModel.showDatePicker(
                                        context, false),
                                    decoration: InputDecoration(
                                        labelText: 'สิ้นสุดเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  onChanged: (text) {
                                    // viewModel.check();
                                  },
                                  autovalidateMode: AutovalidateMode.always,
                                  autofocus: false,
                                  controller: viewModel.endTime,
                                  maxLength: 200,
                                  style: textStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateEndDate,
                                  onTap: () =>
                                      viewModel.showTimePicker(context, false),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาสิ้นสุด',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: new Text("ศิลปินและแนวเพลง",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 10, left: 16, right: 16),
                          child: new Text(
                              "ให้ข้อมูลเพิ่มเติมเกี่ยวกับคอนเสิร์ตนี้ว่าศิลปินคือใคร และเพลงแนวไหน",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            autofocus: false,
                            readOnly: true,
                            style: textStyle(),
                            validator: viewModel.validateGenre,
                            controller: viewModel.genre,
                            onTap: () => viewModel.navigateToSelectGenre(context),
                            decoration: InputDecoration(
                                isDense: true,
                                suffixIcon: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 25,
                                  color: Colors.black,
                                ),
                                border: OutlineInputBorder(),
                                counterText: ''),
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
                    child: Column(
                        children: viewModel.attributes.map((e) {
                      return Row(
                        children: [
                          Icon(
                            Icons.music_note,
                            size: 20,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "${e.name}",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      );
                    }).toList()),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            autofocus: false,
                            readOnly: true,
                            style: textStyle(),
                            controller: viewModel.artist,
                            validator: viewModel.validateArtist,
                            onTap: () => viewModel.navigateToSelectArtist(context),
                            decoration: InputDecoration(
                                isDense: true,
                                suffixIcon: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 25,
                                  color: Colors.black,
                                ),
                                border: OutlineInputBorder(),
                                counterText: ''),
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
                    child: Column(
                        children: viewModel.artists.map((e) {
                      return Container(
                        padding: const EdgeInsets.only(top: 5),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 15,
                              backgroundImage: NetworkImage(e.image?.url ?? ''),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Text(
                                "${e.name}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }).toList()),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              new Text("สถานที่แสดงคอนเสิร์ต",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xff08080a),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  )),
                              Spacer(),
                              TextButton(
                                  onPressed: () =>
                                      viewModel.navigateToCreateVenue(context),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.add_location,
                                        size: 20,
                                        color: Colors.blue,
                                      ),
                                      Text("เพิ่มสถานที่",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Colors.blue,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  )),
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 10, left: 16, right: 16),
                          child: new Text(
                              "เพิ่มตำแหน่งที่คอนเสิร์ตทำการแสดงสำหรับผู้ที่สนใจ คอนเสิร์ตของคุณ",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            autofocus: false,
                            readOnly: true,
                            style: textStyle(),
                            controller: viewModel.place,
                            onTap: () =>
                                viewModel.navigateToSelectConcertPlace(context),
                            validator: viewModel.validate,
                            decoration: InputDecoration(
                                isDense: true,
                                suffixIcon: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 25,
                                  color: Colors.black,
                                ),
                                border: OutlineInputBorder(),
                                counterText: ''),
                          ))),
                  SliverFillRemaining(
                      hasScrollBody: false,
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16),
                            width: double.infinity,
                            height: 42,
                            child: ElevatedButton(
                                child: Text(
                                  "ลบคอนเสิร์ต",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xffda3534),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Color(0xffe6e7ec)),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(21),
                                    ))),
                                onPressed: () => viewModel.isActive
                                    ? viewModel.delete(context)
                                    : null),
                          ))),
                  SliverFillRemaining(
                      hasScrollBody: false,
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.only(
                                top: 20, left: 16, right: 16, bottom: 24),
                            width: double.infinity,
                            height: 42,
                            child: ElevatedButton(
                                child: Text(
                                  "บันทึกข้อมูล",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: viewModel.isActive
                                        ? Color(0xffffffff)
                                        : Color(0xffb2b2b2),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            viewModel.isActive
                                                ? Color(0xffda3534)
                                                : Color(0xffe6e7ec)),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(21),
                                    ))),
                                onPressed: () => viewModel.isActive
                                    ? viewModel.save()
                                    : null),
                          )))
                ])),
              ));
        });
  }

  void showSaveSuccess() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SaveSuccessDialog('shape.png', 'แก้ไขข้อมูลสำเร็จ', '');
        });
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
