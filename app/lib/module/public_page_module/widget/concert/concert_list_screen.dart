import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen_view_model.dart';
import 'package:app/module/public_page_module/widget/concert/manage_concert_menu.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_management.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;

class ConcertListScreen extends StatefulWidget with TixRoute {
  final String? storeId;
  final String? storeName;
  final bool? isOwner;

  ConcertListScreen({Key? key, this.storeId, this.storeName, this.isOwner = false})
      : super(key: key);

  @override
  _ConcertListScreen createState() => _ConcertListScreen();

  @override
  String buildPath() {
    return '/concert_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => ConcertListScreen(
            storeId: data['storeId'], storeName: data['storeName'], isOwner: data['isOwner']));
  }
}

class _ConcertListScreen extends BaseStateProvider<ConcertListScreen, ConcertListScreenViewModel> {
  @override
  void initState() {
    viewModel = ConcertListScreenViewModel("${widget.storeId}");
    // viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ConcertListScreenViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBarWidget(
              name: widget.storeName,
            ),
            body: RefreshIndicator(
                onRefresh: () {
                  viewModel.clearCheckInPage();
                  viewModel.getCheckIn();
                  return Future.value();
                },
                child: ListView.builder(
                  itemCount: viewModel.concertList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final data = viewModel.concertList[index];
                    return InkWell(
                      onTap: () async {
                        WrapNavigation.instance.nativePushNamed(
                          context,
                          ACTION_OPEN_CONCERT_DETAIL,
                          arguments: {'id': '${data.id}'},
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
                        decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1, color: Color(0xFFE0E0E0)))),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Image.network(
                                viewModel.concertList[index].images?.first.url ?? '',
                                height: 120,
                                width: 90,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 16),
                              height: 120,
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Image(
                                        width: 12,
                                        height: 12,
                                        image: AssetImage("assets/icons_calendar.png"),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 5),
                                        child: new Text(
                                          viewModel.concertList[index].showTime?.textShortDate ??
                                              '',
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff676767),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: 0.2333333333333333,
                                          ),
                                          maxLines: 1,
                                          overflow: TextOverflow.visible,
                                        ),
                                      )
                                    ],
                                  ),
                                  Container(
                                    width: Screen.width - 138,
                                    child: new Text(
                                      viewModel.concertList[index].name ?? '',
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff000000),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                      overflow: TextOverflow.clip,
                                      maxLines: 3,
                                    ),
                                  ),
                                  Spacer(),
                                  widget.isOwner!
                                      ? Container(
                                          width: Screen.width - 139,
                                          child: Row(
                                            children: [
                                              InkWell(
                                                  //onTap: () => TixNavigate.instance.navigateTo(TicketManagement(), data: {'concertId': viewModel.concertList[index].id, 'storeId': viewModel.storeId}),
                                                  onTap: () => WrapNavigation.instance.pushNamed(
                                                          context, TicketManagement(), arguments: {
                                                        'concertId':
                                                            viewModel.concertList[index].id,
                                                        'storeId': viewModel.storeId
                                                      }),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(2),
                                                    decoration: new BoxDecoration(
                                                        color: Color(0xffe6e7ec),
                                                        borderRadius: BorderRadius.circular(6)),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Icon(
                                                          Icons.settings,
                                                          size: 15,
                                                          color: Colors.black87,
                                                        ),
                                                        Text("จัดการคอนเสิร์ต",
                                                            style: TextStyle(
                                                              fontFamily: 'SukhumvitSet-Text',
                                                              color: Colors.black87,
                                                              fontSize: 12,
                                                              fontWeight: FontWeight.w400,
                                                              fontStyle: FontStyle.normal,
                                                            ))
                                                      ],
                                                    ),
                                                  )),
                                              Spacer(),
                                              Text("เผยแพร่ : ",
                                                  style: TextStyle(
                                                    fontFamily: 'SukhumvitSet-text',
                                                    color: Color(0xff6d6d6d),
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    fontStyle: FontStyle.normal,
                                                  )),
                                              Switch(
                                                value: viewModel.concertList[index].status ?? false,
                                                onChanged: (bool isOn) {
                                                  viewModel.published(
                                                      viewModel.concertList[index], isOn);
                                                },
                                                activeColor: Color(0xFFDA3534),
                                                activeTrackColor: Color(0xfff5c5c4),
                                                inactiveTrackColor: Color(0xFFCACACA),
                                                inactiveThumbColor: Color(0xFFF4F4F4),
                                              )
                                            ],
                                          ),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                )),
          );
        });
  }
}
