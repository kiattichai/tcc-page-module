import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/create_concert_ticket_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/custom_date_picker.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertTicket extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;
  final int? ticketId;

  const CreateConcertTicket({Key? key, this.concertId, this.storeId, this.ticketId})
      : super(key: key);

  @override
  _CreateConcertTicket createState() => _CreateConcertTicket();

  @override
  String buildPath() {
    return '/create_concert_ticket';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => CreateConcertTicket(
            concertId: data['concertId'], storeId: data['storeId'],ticketId:data['ticketId']));
  }
}

class _CreateConcertTicket extends BaseStateProvider<CreateConcertTicket,
    CreateConcertTicketViewModel> {
  @override
  void initState() {
    viewModel =
        CreateConcertTicketViewModel(widget.concertId!, widget.storeId!,widget.ticketId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  BaseWidget<CreateConcertTicketViewModel>(
            model: viewModel,
            builder: (context, model, child) {
              return ProgressHUD(
                  opacity: 1.0,
                  color: Colors.white,
                  progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'สร้างบัตร',
                ),
                body: SafeArea(
                    child: CustomScrollView(slivers: [
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Text("ข้อมูลทั่วไป",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 3, left: 16, right: 16),
                          child: RichText(
                              text: new TextSpan(children: [
                            new TextSpan(
                                text: "สามารถดูตัวอย่างการคำนวณค่าธรรมเนียม ",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                            new TextSpan(
                                text: "คลิกที่นี่",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                          ])))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              left: 16, top: 16, right: 16),
                          child: DropdownButtonFormField(
                            value: ['บัตรทั่วไป', 'บัตรแพ็คเกจ'][viewModel.type],
                            style: inputTextStyle(),
                            decoration: InputDecoration(
                              labelText: 'ประเภทบัตร',
                              border: OutlineInputBorder(),

                              isDense: true,
                              labelStyle: textStyle(),
                            ),
                            isDense: true,
                            onChanged: (String? newValue) {
                              viewModel.setTicketType(newValue);
                            },
                            items: <String>['บัตรทั่วไป', 'บัตรแพ็คเกจ']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        style: inputTextStyle(),
                        controller: viewModel.ticketName,
                        validator: viewModel.validateTicketName,

                        decoration: InputDecoration(

                            labelText: 'ชื่อบัตร',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          viewModel.check();
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                      onChanged: (text) {
                        viewModel.check();
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autofocus: false,
                      style: inputTextStyle(),
                      controller: viewModel.description,
                      maxLines: 3,
                      maxLength: 300,
                      decoration: InputDecoration(
                          labelText: 'คำอธิบาย',
                          border: OutlineInputBorder(),
                          isDense: true,
                          alignLabelWithHint: true,
                          hintText: 'เขียนคำอธิบายสั้นๆ',
                          counterText: '',
                          hintStyle: textStyle(),
                          labelStyle: textStyle(),
                          helperText: 'ระบุหรือไม่ก็ได้',
                          helperStyle: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff6d6d6d),
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 4, left: 16, right: 16),
                          child: new Text("ราคาและจำนวนบัตร",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 3, left: 16, right: 16),
                          child: new Text(
                              "ราคาตั้งไม่ต้องระบุก็ได้ แต่ถ้าระบุต้องมากกว่ารายขาย",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.always,
                        autofocus: false,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        controller: viewModel.price,
                        keyboardType: TextInputType.number,
                        validator: viewModel.validateSale,
                        style: inputTextStyle(),
                        decoration: InputDecoration(
                            labelText: 'ราคาตั้ง',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            helperText: 'ระบุหรือไม่ก็ได้',
                            helperStyle: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff6d6d6d),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          // viewModel.check();
                          // viewModel.setPageNameLength(text.length);
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        style: inputTextStyle(),
                        keyboardType: TextInputType.number,
                        controller: viewModel.salePrice,
                        validator: viewModel.validateSalePrice,
                        decoration: InputDecoration(
                            labelText: 'ราคาขาย',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          viewModel.check();
                          // viewModel.setPageNameLength(text.length);
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        style: inputTextStyle(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        controller: viewModel.amount,
                        validator: viewModel.validateAmount,
                        decoration: InputDecoration(
                            labelText: 'จำนวน/ใบ',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          // viewModel.check();
                          // viewModel.setPageNameLength(text.length);
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        style: inputTextStyle(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        controller: viewModel.lowAmount,
                        validator: viewModel.validateLowAmount,
                        decoration: InputDecoration(
                            labelText: 'จำนวนซื้อขั้นต่ำ',
                            border: OutlineInputBorder(),
                            isDense: true,
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          // viewModel.check();
                          // viewModel.setPageNameLength(text.length);
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        autofocus: false,
                        style: inputTextStyle(),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        controller: viewModel.highAmount,
                        validator: viewModel.validateHighAmount,
                        decoration: InputDecoration(
                            labelText: 'จำนวนซื้อสูงสุด/ครั้ง',
                            border: OutlineInputBorder(),
                            isDense: true,
                            labelStyle: textStyle(),
                            counterText: ''),
                        maxLength: 50,
                        onChanged: (text) {
                          viewModel.check();
                          // viewModel.setPageNameLength(text.length);
                        }),
                  )),
                  SliverToBoxAdapter(
                      child: viewModel.isPackage
                          ? Container(
                              padding: const EdgeInsets.only(
                                  left: 16, top: 16, right: 16),
                              child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  autofocus: false,
                                  style: inputTextStyle(),
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  keyboardType: TextInputType.number,
                                  controller: viewModel.perPackage,
                                  validator: viewModel.validatePerPackage,
                                  decoration: InputDecoration(
                                      labelText: 'จำนวนบัตร/บัตรแพ็คเกจ',
                                      border: OutlineInputBorder(),
                                      isDense: true,
                                      labelStyle: textStyle(),
                                      counterText: ''),
                                  maxLength: 50,
                                  onChanged: (text) {
                                    viewModel.check();
                                    // viewModel.setPageNameLength(text.length);
                                  }),
                            )
                          : SizedBox()),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 4, left: 16, right: 16),
                          child: new Text("ระยะเวลาที่เริ่มขายบัตร",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 3, left: 16, right: 16),
                          child: new Text(
                              "วันและเวลาเริ่มขายต้องมาก่อนวันและเวลาสิ้นสุด",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.saleStartDate,
                                    maxLength: 200,
                                    style: inputTextStyle(),
                                    readOnly: true,
                                    validator: viewModel.validateSaleStartDate,
                                    onTap: () => _showDatePicker(
                                        viewModel.saleStartDateTime,
                                        true,
                                        viewModel.maxDateTime,
                                        viewModel.setSaleDatePicker),
                                    decoration: InputDecoration(
                                        labelText: 'เริ่มเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  autovalidateMode: AutovalidateMode.always,
                                  autofocus: false,
                                  controller: viewModel.saleStartTime,
                                  maxLength: 200,
                                  style: inputTextStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateSaleStartDate,
                                  onTap: () => _showTimePicker(
                                      viewModel.saleStartDateTime,
                                      true,
                                      viewModel.setSaleTimePicker),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาเริ่มต้น',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.saleEndDate,
                                    maxLength: 200,
                                    style: inputTextStyle(),
                                    readOnly: true,
                                    validator: viewModel.validateSaleStartDate,
                                    onTap: () => _showDatePicker(
                                        viewModel.saleEndDateTime,
                                        false,
                                        viewModel.maxDateTime,
                                        viewModel.setSaleDatePicker),
                                    decoration: InputDecoration(
                                        labelText: 'สิ้นสุดเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  autofocus: false,
                                  controller: viewModel.saleEndTime,
                                  maxLength: 200,
                                  style: inputTextStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateSaleStartDate,
                                  onTap: () => _showDatePicker(
                                      viewModel.saleEndDateTime,
                                      false,
                                      viewModel.maxDateTime,
                                      viewModel.setSaleTimePicker),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาสิ้นสุด',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 4, left: 16, right: 16),
                          child: new Text("วันและเวลาเข้างาน",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 3, left: 16, right: 16),
                          child: new Text(
                              "วันและเวลาเริ่มเข้างานต้องมาก่อนวันและเวลาสิ้นสุด",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.concertStartDate,
                                    maxLength: 200,
                                    style: inputTextStyle(),
                                    readOnly: true,
                                    validator:
                                        viewModel.validateConcertStartDate,
                                    onTap: () => _showDatePicker(
                                        viewModel.concertStartDateTime,
                                        true,
                                        viewModel.maxDateTime,
                                        viewModel.setConcertDatePicker),
                                    decoration: InputDecoration(
                                        labelText: 'เริ่มเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  autovalidateMode: AutovalidateMode.always,
                                  autofocus: false,
                                  controller: viewModel.concertStartTime,
                                  maxLength: 200,
                                  style: inputTextStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateConcertStartDate,
                                  onTap: () => _showTimePicker(
                                      viewModel.concertStartDateTime,
                                      true,
                                      viewModel.setConcertTimePicker),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาเริ่มต้น',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode: AutovalidateMode.always,
                                    autofocus: false,
                                    controller: viewModel.concertEndDate,
                                    maxLength: 200,
                                    style: inputTextStyle(),
                                    readOnly: true,
                                    validator:
                                        viewModel.validateConcertStartDate,
                                    onTap: () => _showDatePicker(
                                        viewModel.concertEndDateTime,
                                        false,
                                        viewModel.maxDateTime,
                                        viewModel.setConcertDatePicker),
                                    decoration: InputDecoration(
                                        labelText: 'สิ้นสุดเมื่อ',
                                        suffixIcon: Icon(
                                          Icons.calendar_today,
                                          size: 20,
                                        ),
                                        isDense: true,
                                        labelStyle: textStyle(),
                                        border: OutlineInputBorder(),
                                        counterText: ''),
                                  )),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  autovalidateMode: AutovalidateMode.always,
                                  autofocus: false,
                                  controller: viewModel.concertEndTime,
                                  maxLength: 200,
                                  style: inputTextStyle(),
                                  readOnly: true,
                                  validator: viewModel.validateConcertStartDate,
                                  onTap: () => _showTimePicker(
                                      viewModel.concertStartDateTime,
                                      false,
                                      viewModel.setConcertTimePicker),
                                  decoration: InputDecoration(
                                      labelText: 'เวลาสิ้นสุด',
                                      suffixIcon: Icon(
                                        Icons.timer,
                                        size: 20,
                                      ),
                                      isDense: true,
                                      labelStyle: textStyle(fontSize: 12),
                                      border: OutlineInputBorder(),
                                      counterText: ''),
                                ),
                              )
                            ],
                          ))),
                      SliverFillRemaining(
                          hasScrollBody: false,
                          child: viewModel.ticketId != null  && !viewModel.hasTicket ? Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: const EdgeInsets.only(
                                    top: 20, left: 16, right: 16),
                                width: double.infinity,
                                height: 42,
                                child: ElevatedButton(
                                    child: Text(
                                      "ลบบัตร",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xffda3534),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    style: ButtonStyle(
                                        foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                        backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Color(0xffe6e7ec)),
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(21),
                                            ))),
                                    onPressed: () => viewModel.isActive
                                        ? delete()
                                        : null),
                              )):SizedBox()),
                  SliverFillRemaining(
                      hasScrollBody: false,
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.only(
                                top: 16, left: 16, right: 16, bottom: 24),
                            width: double.infinity,
                            height: 42,
                            child: ElevatedButton(
                                child: Text(
                                  "ยืนยัน",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: viewModel.isActive
                                        ? Color(0xffffffff)
                                        : Color(0xffb2b2b2),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            viewModel.isActive
                                                ? Color(0xFFDA3534)
                                                : Color(0xffe6e7ec)),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(21),
                                    ))),
                                onPressed: () => viewModel.isActive
                                    ? viewModel.save(context)
                                    : null),
                          )))
                ])),)
              );
            });
  }

  delete() async {

      bool result = await showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomDialog(
              title: 'ยืนยันลบ "${viewModel.ticketName.text}" หรือไม่ ?',
              description: '',
              dangerButtonText: 'ยกเลิก',
              buttonText: 'ตกลง',
            );
          });
      if(result == true){
        viewModel.delete(context);
      }
  }

  void _showDatePicker(DateTime time, bool isStart, DateTime maxDateTime,
      Function(DateTime time, bool isStart) function) {
    DatePicker.showPicker(context,
        showTitleActions: true,
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ), onConfirm: (time) {
      function(time, isStart);
    },
        pickerModel: CustomDatePicker(
            minTime: DateTime.now(),
            currentTime: time,
            locale: LocaleType.th,
            maxTime: maxDateTime),
        locale: LocaleType.th);
  }

  void _showTimePicker(DateTime time, bool isStart,
      Function(DateTime time, bool isStart) function) {
    DatePicker.showTimePicker(context,
        showTitleActions: true,
        showSecondsColumn: false,
        currentTime: time, onConfirm: (time) {
      function(time, isStart);
    },
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ),
        locale: LocaleType.th);
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
  inputTextStyle({double fontSize = 16}) {
    return    TextStyle(
    fontFamily: 'SukhumvitSet-Text',
    color: Color(0xff08080a),
    fontSize: fontSize,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,


    );
  }
}
