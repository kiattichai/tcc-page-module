import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/concert/concert_ticket_list_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ConcertTicketList extends StatefulWidget with TixRoute {
  final int? concertId;
  final String? storeId;

  const ConcertTicketList({Key? key, this.concertId, this.storeId})
      : super(key: key);

  @override
  _ConcertTicketList createState() => _ConcertTicketList();

  @override
  String buildPath() {
    return '/concert_ticket_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => ConcertTicketList(
            concertId: data['concertId'], storeId: data['storeId']));
  }
}

class _ConcertTicketList
    extends BaseStateProvider<ConcertTicketList, ConcertTicketListViewModel> {
  @override
  void initState() {
    viewModel = ConcertTicketListViewModel(widget.concertId!, widget.storeId!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ConcertTicketListViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'บัตรทั้งหมด',
                  actionButton: TextButton(
                    child: new Text(" สร้างบัตร",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff4a4a4a),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        )),
                    onPressed: viewModel.navigateToSetFee(context),
                  ),
                ),
                body: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16, bottom: 0),
                          child: new Text(
                              "สามารถคลิกดูรายละเอียดหรือแก้ไขบัตรแต่ละรายการได้ตลอดเวลาที่บัตรยังไม่ถูกซื้อ",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff6d6d6d),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ))),
                      ...(viewModel.concertTicketsData?.record ?? []).map((e) {
                        var publishedStart =
                            DateFormat('d-MMM-yyyy, HH:mm น.', 'th').format(
                                DateFormat("yyyy-MM-dd'T'HH:mm:sss")
                                    .parse(e.publishedStart ?? '')
                                    .add(Duration(hours: 7)));
                        return InkWell(
                          onTap: ()=> viewModel.editTicket(e, context),
                          child: Container(
                            margin: const EdgeInsets.only(
                                left: 16, top: 16, right: 16),
                            padding: const EdgeInsets.only(bottom: 5),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 1, color: Color(0xFFE0E0E0)))),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                        text: new TextSpan(children: [
                                      new TextSpan(
                                          text: "${e.name} |",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      new TextSpan(
                                          text: " ฿${e.price}",
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff08080a),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    ])),
                                    Text("$publishedStart",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-text',
                                          color: Color(0xff6d6d6d),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ],
                                ),
                                Spacer(),
                                Switch(
                                  value: e.status!,

                                  onChanged: (bool isOn) {
                                   viewModel.updateStatus(e,isOn);
                                  },
                                  activeColor: Color(0xFFDA3534),
                                  activeTrackColor: Color(0xfff5c5c4),
                                  inactiveTrackColor: Color(0xFFCACACA),
                                  inactiveThumbColor: Color(0xFFF4F4F4),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                      (viewModel.concertTicketsData?.record?.isEmpty ?? false)
                          ? Container(
                            height: Screen.height - 200,
                            child: Center(
                                child:  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                  Image(
                                    image:
                                        AssetImage('assets/ticket_outline.png'),
                                    width: 25,
                                    height: 25,
                                  ),
                                  SizedBox(width:10),
                                  Text(
                                    'ยังไม่มีบัตรคอนเสิร์ต',
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Colors.grey,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ]),
                              ),
                          )
                          : SizedBox()
                    ],
                  ),
                ),
              ));
        });
  }
}
