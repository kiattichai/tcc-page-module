import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_ticket_scan_entity.dart';
import 'package:app/model/save_ticket_scan_entity.dart';
import 'package:flutter/material.dart';

class TicketScanViewModel extends BaseViewModel{

  final int concertId;
  final String storeId;
  ScrollController scrollController = ScrollController();
  int pageLimit = 15;
  int page = 1;
  bool isComplete = false;

  GetTicketScanData? ticketScanData;
  List<GetTicketScanDataRecord> ticketScanList = [];

  TicketScanViewModel(this.concertId, this.storeId){
    scrollController.addListener(() {
      if(scrollController.position.extentAfter == 0 && !isComplete){
        page++;
        getTicketScan();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    getTicketScan();
  }

  void getTicketScan() async {
    catchError(() async{
      if (ticketScanData?.pagination?.lastPage != page) {
        isComplete = false;
      } else {
        isComplete = true;
      }
      ticketScanData = await di.concertRepository.getTicketScan(concertId,storeId,page,pageLimit);
      if (ticketScanData?.record != null) {
        ticketScanList.addAll(ticketScanData?.record??[]);
      }
      notifyListeners();
    });
  }

  saveTicketScan(String note){
    catchError(() async{
    var save=  SaveTicketScanEntity()
      ..storeId = storeId
      ..note = note;
     var result = await di.concertRepository.saveTicketScan(concertId,save);
     if(result != null){
       cleatTicketList();
       getTicketScan();
     }
    });
  }

  updateTicketScan(int ticketId,String note){
    catchError(() async{
      var save=  SaveTicketScanEntity()
        ..storeId = storeId
        ..note = note;
      var result = await di.concertRepository.updateTicketScan(concertId,ticketId,save);
      if(result != null){
        cleatTicketList();
        getTicketScan();
      }
    });
  }

  deleteTicketScan(int ticketId){
    catchError(() async{
      var result = await di.concertRepository.deleteTicketScan(concertId,ticketId,storeId);
      if(result != null){
        cleatTicketList();
        getTicketScan();
      }
    });
  }

  cleatTicketList(){
    ticketScanList.clear();
    page = 1;
    isComplete = false;
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

}