import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/utils/screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../../../../../globals.dart' as globals;

class BannerWidget extends StatelessWidget {
  HomeViewModel viewModel;

  BannerWidget(this.viewModel);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return (viewModel.storeDetail?.images?.banner ?? []).isEmpty
        ? Container(
            width: Screen.width,
            height: 211,
            child: Image(
              image: AssetImage('assets/default-page-screen.jpeg'),
              fit: BoxFit.cover,
            ))
        : Stack(
            children: [
              Container(
                  width: Screen.width,
                  height: 211,
                  decoration: BoxDecoration(),
                  child: CarouselSlider(
                    options: CarouselOptions(
                        height: 400.0,
                        autoPlay: true,
                        viewportFraction: 1,
                        scrollDirection: Axis.horizontal,
                        onPageChanged: (index, reason) {
                          viewModel.imageIndex(index);
                        }),
                    items:
                        (viewModel.storeDetail?.images?.banner ?? []).map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: 375.0,
                            margin: EdgeInsets.symmetric(horizontal: 0.0),
                            decoration: BoxDecoration(color: Colors.black12),
                            child: Image.network(
                              '${i.url}',
                              fit: BoxFit.cover,
                            ),
                          );
                        },
                      );
                    }).toList(),
                  )),
              Positioned(
                  bottom: 16.0,
                  right: 16.0,
                  child: viewModel.isOwner
                      ?  Container(
                    width: 61,
                    height: 23,
                          decoration: new BoxDecoration(
                              color: Color(0xff424242),
                              borderRadius: BorderRadius.circular(11.5)),
                          child: InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.camera_alt,
                                  size: 18,
                                  color: Colors.white,
                                ),
                                new Text("เพิ่มรูป",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xffffffff),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )
                                )
                              ],
                            ),onTap: () => viewModel.pickBannerImage(),
                          ),
                        )
                      : Text(
                          "${viewModel.getImageIndex()}/${(viewModel.storeDetail?.images?.banner ?? []).length}",
                          style: TextStyle(
                            fontFamily: 'SFUIText',
                            color: Color(0xffffffff),
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
            ],
          );
  }
}
