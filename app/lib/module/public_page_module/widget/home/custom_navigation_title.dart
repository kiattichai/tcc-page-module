import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:app/utils/screen.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CustomNavigationTitle extends StatelessWidget implements PreferredSizeWidget {
  final String name;
  final double height;
  final bool showActionButton;
  final Widget actionButton;
  final Function? onBack;
  final Key? keyBack;
  

  const CustomNavigationTitle(
      {Key? key, required this.height, required this.name, this.showActionButton = false,
      this.actionButton = const SizedBox(),
      this.onBack, this.keyBack})
      : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          border: Border(
            bottom: BorderSide(width: 1, color: Colors.black.withOpacity(0.17)),
          ),
          boxShadow: [
            BoxShadow(
                color: Color(0x20000000), offset: Offset(0, 1), blurRadius: 3, spreadRadius: 0)
          ],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: Stack(
            children: <Widget>[
              InkWell(
                key: keyBack,
                onTap: () {
                  if (onBack != null) {
                    onBack!();
                  } else {
                    TixNavigate.instance.pop();
                  }
                },
                child: Container(
                  height: Screen.convertHeightSize(55),
                  width: 50,
                  child: Center(
                    child: Container(
                      height: 20,
                      child: Image(
            image: AssetImage("assets/path.png"),
            width: 18,
            height: 14,
          ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, 0),
                child: Text(name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff4a4a4a),
              fontSize: 18,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
                    )),
              )
            ],
          ),
        ));
  }
}
