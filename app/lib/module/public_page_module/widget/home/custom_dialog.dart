import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String description;
  final String? buttonText;
  final String? dangerButtonText;

  const CustomDialog(
      {Key? key,
      required this.title,
      required this.description,
      this.buttonText,
      this.dangerButtonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
        child: Container(
            width: 315,
            padding: const EdgeInsets.all(24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FittedBox(
                  child: Text(title,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      )),
                  fit: BoxFit.contain,
                ),
                Container(
                  padding: const EdgeInsets.only(top: 13),
                  child: Text(
                    description,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Container(
                    padding: const EdgeInsets.only(top: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        buttonText != null
                            ? InkWell(
                                onTap: () => WrapNavigation.instance
                                    .pop(context, data: true), // Handle your callback
                                child: new Text(buttonText ?? '',
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Colors.blue,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )),
                                splashColor: Colors.white30)
                            : SizedBox(),
                        buttonText != null && dangerButtonText != null
                            ? SizedBox(
                                width: 39,
                              )
                            : SizedBox(),
                        dangerButtonText != null
                            ? InkWell(
                                onTap: () => WrapNavigation.instance
                                    .pop(context, data: true), // Handle your callback
                                child: Text(dangerButtonText ?? '',
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xffda3534),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )),
                                splashColor: Colors.white30)
                            : SizedBox(),
                      ],
                    ))
              ],
            ),
            decoration: new BoxDecoration(
                color: Color(0xffffffff), borderRadius: BorderRadius.circular(6))));
  }
}
