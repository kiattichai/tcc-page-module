import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:tix_navigate/tix_navigate.dart';

class DialogStoreVerify extends StatefulWidget {
  final String? storeId;
  DialogStoreVerify({Key? key, this.storeId}) : super(key: key);

  @override
  _DialogStoreVerifyState createState() => _DialogStoreVerifyState();
}

class _DialogStoreVerifyState extends State<DialogStoreVerify> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Dialog(
      child: Container(
        width: Screen.width,
        child: Container(
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset('assets/tcc_store_verify.png'),
                  Positioned(
                    top: 5,
                    right: 5,
                    child: InkWell(
                        onTap: () {
                          //TixNavigate.instance.pop();
                          WrapNavigation.instance.pop(context, data: false);
                        },
                        child: Container(child: Icon(Icons.close))),
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  child: InAppWebView(
                    initialUrlRequest: URLRequest(
                        url: Uri.parse(
                            "https://alpha-www.theconcert.com/store/${widget.storeId ?? ''}/covid?webview=1")),
                    initialOptions: InAppWebViewGroupOptions(
                        crossPlatform: InAppWebViewOptions()),
                    onWebViewCreated: (InAppWebViewController controller) {},
                    onLoadStart:
                        (InAppWebViewController controller, Uri? url) {},
                    onLoadStop:
                        (InAppWebViewController controller, Uri? url) async {},
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {},
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
