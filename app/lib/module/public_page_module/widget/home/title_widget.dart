import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleWidget extends StatelessWidget {
  final HomeViewModel viewModel;
  final Function()? onTapVerify;
  TitleWidget(this.viewModel, this.onTapVerify);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Container(
          padding:
              const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(children: [
                ClipOval(
                  child: Image.network(
                    '${viewModel.storeDetail?.images?.logo?.url}',
                    fit: BoxFit.cover,
                    width: 68,
                    height: 68,
                    errorBuilder: (context, error, stackTrace) => Image(
                      width: 68,
                      height: 68,
                      image: AssetImage("assets/image-empty.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                viewModel.isOwner
                    ? Positioned(
                        right: 0.0,
                        bottom: 0.0,
                        child: InkWell(
                          child: Container(
                            width: 19,
                            height: 19,
                            decoration: BoxDecoration(
                              color: Color(0xffb2b2b2),
                              border: Border.all(
                                color: Color(0xFFFFFFFF),
                              ),
                              shape: BoxShape.circle,
                            ),
                            child: Icon(
                              Icons.camera_alt,
                              size: 11,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            viewModel.pickProfileImage();
                          },
                        ),
                      )
                    : SizedBox()
              ]),
              Container(
                padding: const EdgeInsets.only(left: 16),
                width: width - 179,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${viewModel.storeDetail?.name ?? ''}',
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text('${viewModel.storeDetail?.section?.textFull ?? ''}',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        )),
                    Container(
                      height: 20,
                      width: 50,
                      decoration: new BoxDecoration(
                          color: Color(0xffda3534),
                          borderRadius: BorderRadius.circular(3)),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Icon(
                              Icons.star,
                              color: Colors.white,
                              size: 13,
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(left: 7),
                              child: Text(
                                  '${viewModel.storeDetail?.averageRating?.toStringAsFixed(1) ?? ''}',
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xffffffff),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ))),
                        ],
                      ),
                    ),
                    viewModel.storeDetail?.section?.id == 2 &&
                            viewModel.storeDetail?.isShaPlus == true
                        ? Container(
                            child: Image.asset('assets/sha_plus_logo.png',
                                width: 60),
                            margin: EdgeInsets.only(top: 6),
                          )
                        : SizedBox(height: 12)
                  ],
                ),
              ),
              Spacer(),
              !viewModel.isOwner &&
                      viewModel.storeDetail?.section?.text == 'nightclub'
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        new Text("${viewModel.distance} กม.",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff555555),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                                "${viewModel.storeDetail?.reviewTotal ?? ''} รีวิว",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                            Padding(
                              padding: const EdgeInsets.only(left: 12),
                              child: Image(
                                width: 6,
                                height: 10,
                                image: AssetImage("assets/chevron_right.png"),
                                color: Colors.black,
                              ),
                            )
                          ],
                        )
                      ],
                    )
                  : SizedBox()
            ],
          ),
        ),
        viewModel.storeDetail?.section?.id == 2 &&
                viewModel.storeDetail?.covidVerify == true
            ? Container(
                child: InkWell(
                  onTap: () {
                    onTapVerify?.call();
                  },
                  child: Image.asset('assets/tcc_store_verify.png'),
                ),
              )
            : SizedBox()
      ],
    );
  }
}
