import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabBarWidget extends StatelessWidget {
  final TabController? tabController;

  TabBarWidget({Key? key, required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: tabController == null
          ? SizedBox()
          : tabController?.length == 4
              ? TabBar(
                  controller: tabController,
                  labelColor: Colors.red,
                  indicatorColor: Colors.red,
                  unselectedLabelColor: Color(0xff08080a),
                  //isScrollable: true,
                  labelPadding: EdgeInsets.symmetric(horizontal: 1.0),
                  tabs: [
                    Tab(
                      child: Text("หน้าหลัก",
                          maxLines: 1,
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    Tab(
                      child: Text("คอนเสิร์ต",
                          maxLines: 1,
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    Tab(
                      child: Text("โปรโมชัน",
                          maxLines: 1,
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    // Tab(
                    //   child: Text("สินค้า",
                    //       style: TextStyle(
                    //         fontFamily: 'SukhumvitSet-Text',
                    //         fontSize: 14,
                    //         fontWeight: FontWeight.w500,
                    //         fontStyle: FontStyle.normal,
                    //       )),
                    // ),
                    Tab(
                      child: Text("รีวิว",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                  ],
                )
              : tabController?.length == 5
                  ? TabBar(
                      controller: tabController,
                      labelColor: Colors.red,
                      indicatorColor: Colors.red,
                      unselectedLabelColor: Color(0xff08080a),
                      //isScrollable: true,
                      labelPadding: EdgeInsets.symmetric(horizontal: 1.0),
                      tabs: [
                        Tab(
                          child: Text("หน้าหลัก",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Tab(
                          child: Text("เมนู",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Tab(
                          child: Text("คอนเสิร์ต",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Tab(
                          child: Text("โปรโมชัน",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        // Tab(
                        //   child: Text("สินค้า",
                        //       style: TextStyle(
                        //         fontFamily: 'SukhumvitSet-Text',
                        //         fontSize: 14,
                        //         fontWeight: FontWeight.w500,
                        //         fontStyle: FontStyle.normal,
                        //       )),
                        // ),
                        Tab(
                          child: Text("รีวิว",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                      ],
                    )
                  : TabBar(
                      controller: tabController,
                      labelColor: Colors.red,
                      indicatorColor: Colors.red,
                      unselectedLabelColor: Color(0xff08080a),
                      //isScrollable: true,
                      labelPadding: EdgeInsets.symmetric(horizontal: 1.0),
                      tabs: [
                        Tab(
                          child: Text("หน้าหลัก",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Tab(
                          child: Text("คอนเสิร์ต",
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        // Tab(
                        //   child: Text("โปรโมชัน",
                        //       maxLines: 1,
                        //       style: TextStyle(
                        //         fontFamily: 'SukhumvitSet-Text',
                        //         fontSize: 14,
                        //         fontWeight: FontWeight.w500,
                        //         fontStyle: FontStyle.normal,
                        //       )),
                        // ),
                        // Tab(
                        //   child: Text("สินค้า",
                        //       style: TextStyle(
                        //         fontFamily: 'SukhumvitSet-Text',
                        //         fontSize: 14,
                        //         fontWeight: FontWeight.w500,
                        //         fontStyle: FontStyle.normal,
                        //       )),
                        // ),
                        Tab(
                          child: Text("รีวิว",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                      ],
                    ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.black12,
            width: 3.0,
          ),
          bottom: BorderSide(
            color: Colors.black12,
            width: 2.0,
          ),
        ),
      ),
    );
  }
}
