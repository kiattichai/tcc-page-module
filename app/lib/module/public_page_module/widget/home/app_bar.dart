import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String? name;
  final bool showActionButton;
  final Widget actionButton;
  final Function? onBack;

  const AppBarWidget(
      {Key? key,
      required this.name,
      this.showActionButton = false,
      this.actionButton = const SizedBox(),
      this.onBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: onBack == null ? true : false,
      titleSpacing: 0,
      brightness: Brightness.light,
      backgroundColor: Colors.white,
      leading: onBack != null
          ? GestureDetector(
              onTap: () {
                if (onBack != null) {
                  onBack?.call();
                } else {
                  WrapNavigation.instance.pop(context);
                }
              },
              child: Image(
                image: AssetImage("assets/path.png"),
                width: 18,
                height: 14,
              ),
            )
          : null,
      title: new Text(name ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'SukhumvitSet-Text',
            color: Color(0xff4a4a4a),
            fontSize: 18,
            fontWeight: FontWeight.w700,
            fontStyle: FontStyle.normal,
          )),
      actions: <Widget>[actionButton],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight - 15);
}
