import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogResult extends StatelessWidget {
  String? url;
  String? name;
  DialogResult(this.url, this.name);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.only(top: 32.0),
            child: CircleAvatar(
              radius: 30.0,
              child: Stack(
                children: [
                  Container(
                    height: 100.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child: Image.network(
                        '${url}',
                        fit: BoxFit.cover,
                        width: 68,
                        height: 68,
                        errorBuilder: (context, error, stackTrace) => Image(
                          width: 68,
                          height: 68,
                          image: AssetImage("assets/image-empty.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    right: 0.0,
                    child: Container(
                      height: 15.0,
                      width: 15.0,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(30.0),
                        border: Border.all(color: Colors.white),
                      ),
                      child: Icon(
                        Icons.location_pin,
                        color: Colors.white,
                        size: 10.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, top: 16.0, right: 20.0),
            child: Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              width: 238.0,
              child: Text(
                'เช็คอินที่ ${name} เรียบร้อยแล้ว',
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Center(
            child: InkWell(
              onTap: () {
                WrapNavigation.instance.pop(context);
              },
              child: new Container(
                width: 285,
                height: 40,
                margin: EdgeInsets.only(top: 18.0, bottom: 23.0, left: 16.0, right: 16.0),
                decoration: new BoxDecoration(
                    color: Color(0xffda3534), borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: new Text(
                    "ปิด",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
