import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/checkIn/check_in_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CheckInScreen extends StatefulWidget with TixRoute {
  String? storeId;
  String? storeName;

  CheckInScreen({this.storeId, this.storeName});

  @override
  _CheckInScreen createState() => _CheckInScreen();

  @override
  String buildPath() {
    return 'check_in_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => CheckInScreen(
            storeId: data['storeId'], storeName: data['storeName']));
  }
}

class _CheckInScreen
    extends BaseStateProvider<CheckInScreen, CheckInViewModel> {
  @override
  void initState() {
    viewModel = CheckInViewModel("${widget.storeId}");
    // viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CheckInViewModel>(
      builder: (context, model, child) {
        return  Scaffold(
              appBar: AppBarWidget(
                name: widget.storeName,
              ),
              body: RefreshIndicator(
                  onRefresh: () {
                    viewModel.clearCheckInPage();
                    viewModel.getCheckIn();
                    return Future.value();
                  },
                  child: ListView.builder(
                      itemCount: viewModel.checkInDataList.length,
                       controller: viewModel.checkInController,
                      itemBuilder: (context, index) {
                       var item = viewModel.checkInDataList[index];
                        return Container(
                          padding: const EdgeInsets.only(
                              left: 16, right: 16, top: 20, bottom: 20),
                          child:  Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                          Container(
                          padding: const EdgeInsets.only(bottom: 10),
                          child:Row(
                                    children: [
                                      CircleAvatar(
                                        radius: 18,
                                        backgroundImage: NetworkImage(
                                            item.user?.image?.resizeUrl ?? ''),
                                      ),
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${item.user?.firstName}  ' +
                                                '${item.user?.lastName}',
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff4a4a4a),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          Text(
                                            '${item.createdAt?.date} ' +
                                                '${item.createdAt?.time}',
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff4a4a4a),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w300,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 1,
                                          color: Color(0xFFF0F0F0))))),
                                  (item.description ??'').isNotEmpty ?  Container(
                                    padding: const EdgeInsets.only(top:10,bottom: 20),
                                    child:   Text(
                                      item.description ?? '',
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xff4a4a4a),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ) ,
                                  ) : SizedBox()
                                ],

                              ),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 6, color: Color(0xFFF0F0F0)))),
                        );
                      })),
            );
      },
      model: viewModel,
    );
  }
}
