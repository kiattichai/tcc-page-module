import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_check_in_entity.dart';
import 'package:app/model/get_check_in_user_entity.dart';
import 'package:flutter/material.dart';

class CheckInViewModel extends BaseViewModel {

  final String storeId;
  GetCheckInData? checkInData;

  late ScrollController checkInController;
  List<GetCheckInDataRecord> checkInDataList = [];
  int reviewLimit = 10;
  int checkInPage = 1;
  bool isReviewComplete = false;

  CheckInViewModel(this.storeId){
    checkInController = new ScrollController();
    checkInController.addListener(() {
      if (checkInController.position.extentAfter == 0 && !isReviewComplete) {
        checkInPage++;
        getCheckIn();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    clearCheckInPage();
    getCheckIn();
  }

  void getCheckIn() {
    catchError(() async {
      setLoading(true);
      if (checkInData?.pagination?.lastPage != checkInPage) {
        isReviewComplete = false;
      } else {
        isReviewComplete = true;
      }
      if (checkInPage == 1) {
        checkInDataList.clear();
      }
      checkInData = await di.checkInRepository.getStoreCheckInById(storeId, reviewLimit, page: checkInPage);
      if (checkInData?.record != null) {
        checkInDataList.addAll(checkInData?.record ?? []);
      }

      notifyListeners();
      setLoading(false);
    });
  }

  clearCheckInPage() {
    checkInPage = 1;
    isReviewComplete = false;
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }

}