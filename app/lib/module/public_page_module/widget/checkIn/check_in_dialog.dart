import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/checkIn/dialog_result.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CheckInDialog extends StatelessWidget {
  HomeViewModel viewModel;

  CheckInDialog(this.viewModel);

  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(left: 16.0, right: 8.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      backgroundColor: Colors.transparent,
      child: customDialog(context),
    );
  }

  Widget customDialog(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: 11.0,
              ),
              margin: EdgeInsets.only(top: 13.0, right: 8.0),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(7.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 18.0, top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(36.0),
                            child: Image.network(
                              '${viewModel.storeDetail?.images?.logo?.url}',
                              fit: BoxFit.cover,
                              width: 68,
                              height: 68,
                              errorBuilder: (context, error, stackTrace) => Image(
                                width: 68,
                                height: 68,
                                image: AssetImage("assets/image-empty.png"),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 15.0),
                          child: new Text(
                            'เช็คอินที่ ${viewModel.storeDetail!.name}',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20.0, top: 16.0, right: 20.0),
                    child: TextField(
                      minLines: 8,
                      maxLines: 8,
                      controller: _controller,
                      decoration: InputDecoration(
                        hintText: 'อยากเขียนอะไรที่เกี่ยวกับ ${viewModel.storeDetail!.name}',
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(
                            color: Colors.grey,
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: InkWell(
                      onTap: () {
                        viewModel.checkIns(_controller.text, context);
                      },
                      child: new Container(
                        width: 285,
                        height: 40,
                        margin: EdgeInsets.only(top: 18.0, bottom: 23.0),
                        decoration: new BoxDecoration(
                            color: Color(0xffda3534), borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: new Text(
                            "เช็คอิน",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xffffffff),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0.0,
              child: GestureDetector(
                onTap: () {
                  WrapNavigation.instance.pop(context);
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: CircleAvatar(
                    radius: 14.0,
                    backgroundColor: Colors.white,
                    child: Icon(Icons.close, color: Colors.black),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showResultCheckIns() async {}
}
