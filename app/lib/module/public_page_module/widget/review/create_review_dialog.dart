import 'dart:io';

import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/create_review.dart';
import 'package:app/utils/image_picker_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class CreateReviewDialog extends StatefulWidget {
  @override
  _CreateReviewDialog createState() => _CreateReviewDialog();
}

class _CreateReviewDialog
    extends BaseStateProvider<CreateReviewDialog, CreateReviewDialogViewModel> {
  @override
  void initState() {
    viewModel = CreateReviewDialogViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateReviewDialogViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7),
              ),
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.only(left: 24, right: 19),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(children: [
                  Container(
                    margin: const EdgeInsets.only(top: 12, right: 5),
                    width: double.infinity,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 30),
                          child: Center(
                            child: Text("ให้คะแนนร้านนี้",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xFF4A4A4A),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Center(
                              child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: List<Widget>.generate(
                                      5,
                                      (int index) => (index + 1) <= viewModel.rating
                                          ? InkWell(
                                              onTap: () {
                                                viewModel.setRating(index + 1);
                                                viewModel.check();
                                              },
                                              child: Icon(Icons.star,
                                                  color: Color(0xFFDA3534), size: 28))
                                          : InkWell(
                                              onTap: () {
                                                viewModel.setRating(index + 1);
                                                viewModel.check();
                                              },
                                              child: Icon(Icons.star,
                                                  color: Color(0xFF989898), size: 28)))),
                            )),
                        Container(
                          padding: const EdgeInsets.only(top: 26, left: 20, right: 20),
                          child: Container(
                              padding: const EdgeInsets.only(left: 11, right: 11),
                              child: Column(
                                children: [
                                  TextFormField(
                                      keyboardType: TextInputType.multiline,
                                      maxLines: 6,
                                      maxLength: 250,
                                      controller: viewModel.description,
                                      onChanged: (String value) {
                                        viewModel.setDescriptionLength(value.length);
                                        viewModel.check();
                                      },
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        counterText: '',
                                        hintText: 'เขียนรีวิวสั้นๆ อย่างน้อย 10 ตัวอักษร',
                                        hintStyle: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xFF4A4A4A),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                        isDense: true,
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 11),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          child: Image(
                                            image: AssetImage('assets/camera.png'),
                                            color: Color(0xFF545454),
                                            width: 18,
                                            height: 15,
                                          ),
                                          onTap: () => viewModel.loadAssets(context),
                                        ),
                                        Spacer(),
                                        Text(
                                          '${viewModel.descriptionLength}/250',
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                            fontFamily: 'SukhumvitSet-Text',
                                            color: Color(0xff6d6d6d),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  border: Border.all(
                                      color: viewModel.showDescriptionError
                                          ? Color(0xFFDA3534)
                                          : Color(0xFFEEEEEE),
                                      width: 1))),
                        ),
                        viewModel.showDescriptionError
                            ? Container(
                                padding: const EdgeInsets.only(left: 20, top: 7, right: 20),
                                child: Text(
                                  '* เขียนรีวิวสั้นๆ อย่างน้อย 10 ตัวอักษร',
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xFFDA3534),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ))
                            : SizedBox(),
                        viewModel.images.length > 0
                            ? Container(
                                padding: const EdgeInsets.only(top: 12, left: 20, right: 20),
                                height: 79,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: viewModel.images.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {
                                      return Container(
                                          padding:
                                              index != 0 ? const EdgeInsets.only(left: 10) : null,
                                          child: ClipRRect(
                                              borderRadius: BorderRadius.all(Radius.circular(6)),
                                              child: Stack(
                                                alignment: Alignment.topRight,
                                                children: [
                                                  Image.file(
                                                    viewModel.images[index],
                                                    height: 79,
                                                    width: 79,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(right: 6, top: 6),
                                                    child: InkWell(
                                                      child: Image(
                                                        image: AssetImage('assets/x_copy.png'),
                                                        width: 18,
                                                        height: 18,
                                                      ),
                                                      onTap: () {
                                                        viewModel.removeImage(index);
                                                      },
                                                    ),
                                                  )
                                                ],
                                              )));
                                    }),
                              )
                            : SizedBox(),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin:
                                  const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                              width: double.infinity,
                              height: 40,
                              child: ElevatedButton(
                                  child: Text(
                                    "โพสต์",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: viewModel.isActive
                                          ? Color(0xffffffff)
                                          : Color(0xffb2b2b2),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(Colors.white),
                                      backgroundColor: MaterialStateProperty.all<Color>(
                                          viewModel.isActive
                                              ? Color(0xffda3534)
                                              : Color(0xffe6e7ec)),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ))),
                                  onPressed: () => viewModel.isActive
                                      ? WrapNavigation.instance.pop(context,
                                          data: CreateReview(viewModel.description.text,
                                              viewModel.rating, viewModel.images))
                                      : null),
                            ))
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(7),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                          child: Image(
                            image: AssetImage('assets/close_button.png'),
                            width: 30,
                            height: 30,
                          ),
                          onTap: () => WrapNavigation.instance.pop(context)),
                    ],
                  ),
                ]),
              ));
        });
  }
}

class CreateReviewDialogViewModel extends BaseViewModel {
  List<File> images = <File>[];
  String _error = 'No Error Dectected';
  bool isActive = false;
  bool isAction = false;
  bool showDescriptionError = false;
  int rating = 0;
  TextEditingController description = TextEditingController();
  int descriptionLength = 0;

  void loadAssets(BuildContext context) {
    catchError(() async {
      List<File> resultList = <File>[];
      String error = 'No Error Detected';
      final pathImages = await ImagePickerUtils.multiImagePicker("เลือกภาพ",
          currentImageInList: images.length, context: context);
      if (pathImages != null) {
        for (AssetEntity asset in pathImages) {
          final filePath = await asset.file;
          if (filePath == null) return;
          // File compressedFile =
          // await FlutterNativeImage.compressImage(filePath.path, quality: 75, percentage: 75);
          resultList.add(filePath);
        }
      }

      images = resultList;
      notifyListeners();
    });
  }

  void check() {
    if (description.text.length > 0) {
      isAction = true;
    }
    if (isAction && description.text.length < 10) {
      showDescriptionError = true;
    } else {
      showDescriptionError = false;
    }
    isActive = validateDescription() && validateRating();
    notifyListeners();
  }

  bool validateDescription() {
    if (description.text.isEmpty || description.text.length < 10) {
      return false;
    }

    return true;
  }

  bool validateRating() {
    if (this.rating < 1) {
      return false;
    }
    return true;
  }

  void setDescriptionLength(int length) {
    this.descriptionLength = length;
    notifyListeners();
  }

  void setRating(int rating) {
    this.rating = rating;
    notifyListeners();
  }

  void removeImage(int index) {
    images.removeAt(index);
    notifyListeners();
  }
}
