import 'dart:convert';

import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/save_review_entity.dart';
import 'package:app/model/save_review_image_entity.dart';
import 'package:app/model/save_review_response_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/review/create_review_sucess_dialog.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

class EditReviewScreen extends StatefulWidget with TixRoute {
  String? storeName;
  int? storeId;
  int? reviewId;

  EditReviewScreen({this.storeName, this.storeId, this.reviewId});

  @override
  _EditReviewScreen createState() => _EditReviewScreen();

  @override
  String buildPath() {
    return '/edit_review_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => EditReviewScreen(
            storeName: data['storeName'], storeId: data['storeId'], reviewId: data['reviewId']));
  }
}

class _EditReviewScreen extends BaseStateProvider<EditReviewScreen, EditReviewScreenViewModel> {
  @override
  void initState() {
    viewModel = EditReviewScreenViewModel(
        widget.storeId ?? 0, widget.reviewId ?? 0, widget.storeName ?? '');
    viewModel.showSaveReviewSuccess = showSaveReviewSuccess;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EditReviewScreenViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: widget.storeName,
                ),
                body: SafeArea(
                    child: CustomScrollView(slivers: [
                  SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.only(top: 30),
                      child: Center(
                        child: Text("ให้คะแนนร้านนี้",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xFF4A4A4A),
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                      child: Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Center(
                            child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: List<Widget>.generate(
                                    5,
                                    (int index) => (index + 1) <= viewModel.rating
                                        ? InkWell(
                                            onTap: () {
                                              viewModel.setRating(index + 1);
                                              viewModel.check();
                                            },
                                            child: Icon(Icons.star,
                                                color: Color(0xFFDA3534), size: 28))
                                        : InkWell(
                                            onTap: () {
                                              viewModel.setRating(index + 1);
                                              viewModel.check();
                                            },
                                            child: Icon(Icons.star,
                                                color: Color(0xFF989898), size: 28)))),
                          ))),
                  SliverToBoxAdapter(
                      child: Container(
                    padding: const EdgeInsets.only(top: 26, left: 20, right: 20),
                    child: Container(
                        padding: const EdgeInsets.only(left: 11, right: 11),
                        child: Column(
                          children: [
                            TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: 6,
                                maxLength: 250,
                                controller: viewModel.description,
                                onChanged: (String value) {
                                  viewModel.setDescriptionLength(value.length);
                                  viewModel.check();
                                },
                                decoration: InputDecoration(
                                  focusColor: Colors.transparent,
                                  focusedBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  counterText: '',
                                  hintText: 'เขียนรีวิวสั้นๆ อย่างน้อย 10 ตัวอักษร',
                                  hintStyle: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xFF4A4A4A),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  isDense: true,
                                )),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 11),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  InkWell(
                                    child: Image(
                                      image: AssetImage('assets/camera.png'),
                                      color: Color(0xFF545454),
                                      width: 18,
                                      height: 15,
                                    ),
                                    onTap: () => viewModel.loadAssets(),
                                  ),
                                  Spacer(),
                                  Text(
                                    '${viewModel.descriptionLength}/250',
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(
                                color: viewModel.showDescriptionError
                                    ? Color(0xFFDA3534)
                                    : Color(0xFFEEEEEE),
                                width: 1))),
                  )),
                  SliverToBoxAdapter(
                      child: viewModel.showDescriptionError
                          ? Container(
                              padding: const EdgeInsets.only(left: 20, top: 7, right: 20),
                              child: Text(
                                '* เขียนรีวิวสั้นๆ อย่างน้อย 10 ตัวอักษร',
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xFFDA3534),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ))
                          : SizedBox()),
                  SliverToBoxAdapter(
                      child: viewModel.imageWithType.length > 0
                          ? Container(
                              padding: const EdgeInsets.only(top: 12, left: 20, right: 20),
                              height: 79,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: viewModel.imageWithType.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    var image = viewModel.imageWithType[index];
                                    return Container(
                                        margin: index != 0 ? const EdgeInsets.only(left: 10) : null,
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(6)),
                                            child: Stack(
                                              alignment: Alignment.topRight,
                                              children: [
                                                image.imageType == ImageType.network
                                                    ? Image(
                                                        image: NetworkImage(
                                                          image.url ?? '',
                                                        ),
                                                        fit: BoxFit.cover,
                                                        height: 79,
                                                        width: 69,
                                                      )
                                                    : AssetThumb(
                                                        asset: image.asset ??
                                                            Asset(null, null, null, null),
                                                        height: 79,
                                                        width: 79,
                                                      ),
                                                Padding(
                                                  padding: const EdgeInsets.only(right: 6, top: 6),
                                                  child: InkWell(
                                                    child: Image(
                                                      image: AssetImage('assets/x_copy.png'),
                                                      width: 18,
                                                      height: 18,
                                                    ),
                                                    onTap: () {
                                                      viewModel.removeImage(index);
                                                    },
                                                  ),
                                                )
                                              ],
                                            )));
                                  }),
                            )
                          : SizedBox()),
                  SliverFillRemaining(
                      hasScrollBody: false,
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                            width: double.infinity,
                            height: 40,
                            child: ElevatedButton(
                                child: Text(
                                  "อัปเดต",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color:
                                        viewModel.isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                style: ButtonStyle(
                                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                    backgroundColor: MaterialStateProperty.all<Color>(
                                        viewModel.isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ))),
                                onPressed: () => viewModel.isActive ? viewModel.update() : null),
                          )))
                ])),
              ));
        });
  }

  void showSaveReviewSuccess() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SaveSuccessDialog('shape.png', 'เรียบร้อยแล้ว', 'ขอบคุณสำหรับคะแนนรีวิว');
        });
  }
}

class EditReviewScreenViewModel extends BaseViewModel {
  List<Asset> images = <Asset>[];
  List<ImageWithType> imageWithType = <ImageWithType>[];
  List<String> deleteImageIds = <String>[];
  bool isActive = false;
  bool isAction = false;
  bool showDescriptionError = false;
  int rating = 0;
  TextEditingController description = TextEditingController();
  int descriptionLength = 0;
  final int storeId;
  final int reviewId;
  final String storeName;
  GetReviewDataRecord? reviewByIdData;
  Function()? showSaveReviewSuccess;

  EditReviewScreenViewModel(this.storeId, this.reviewId, this.storeName);

  @override
  void postInit() {
    super.postInit();
    getReview();
  }

  void getReview() {
    catchError(() async {
      setLoading(true);
      reviewByIdData = await di.reviewRepository.getStoreReviewById(storeId, reviewId);
      description.text = reviewByIdData?.description ?? '';
      rating = reviewByIdData?.rating ?? 0;
      initImage();
      check();
      setLoading(false);
    });
  }

  void loadAssets() {
    catchError(() async {
      List<Asset> resultList = <Asset>[];
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10 - imageWithType.length,
        enableCamera: true,
        selectedAssets: images,
        materialOptions: MaterialOptions(
          actionBarColor: "#ed0000",
          actionBarTitle: "เลือกรูปภาพ",
          allViewTitle: "รูปภาพทั้งหมด",
          useDetailsView: false,
          selectCircleStrokeColor: "#ed0000",
        ),
      );

      images = resultList;
      imageWithType.where((element) => element.imageType == ImageType.network);
      imageWithType.addAll(images.map((e) {
        return ImageWithType(ImageType.asset)..asset = e;
      }));
      notifyListeners();
    });
  }

  void update() {
    catchError(() async {
      setLoading(true);
      SaveReviewResponseData? saveReviewResponseData = await di.reviewRepository.updateReview(
          reviewId,
          SaveReviewEntity()
            ..description = description.text
            ..rating = rating
            ..storeId = storeId);
      if (saveReviewResponseData != null) {
        if (deleteImageIds.isNotEmpty) {
          await di.reviewRepository.deleteImageReview(reviewId, deleteImageIds);
        }
        for (Asset asset in (imageWithType
            .where((element) => element.asset != null)
            .map((e) => e.asset ?? Asset(null, null, null, null))
            .toList())) {
          ByteData byteData = await asset.getByteData();
          List<int> imageData = byteData.buffer.asUint8List();
          String b64 = base64Encode(imageData);
          await di.reviewRepository.saveReviewImage(
              saveReviewResponseData.id ?? 0,
              SaveReviewImageEntity()
                ..data = b64
                ..name = asset.name?.toLowerCase());
        }
      }

      showSaveReviewSuccess!();
      setLoading(false);
    });
  }

  void check() {
    if (description.text.length > 0) {
      isAction = true;
    }
    if (isAction && description.text.length < 10) {
      showDescriptionError = true;
    } else {
      showDescriptionError = false;
    }
    isActive = validateDescription() && validateRating();
    notifyListeners();
  }

  bool validateDescription() {
    if (description.text.isEmpty || description.text.length < 10) {
      return false;
    }

    return true;
  }

  void initImage() {
    imageWithType.addAll(reviewByIdData?.images?.map((e) {
          return ImageWithType(ImageType.network)
            ..url = e.url
            ..id = e.id;
        }) ??
        []);
    notifyListeners();
  }

  bool validateRating() {
    if (this.rating < 1) {
      return false;
    }
    return true;
  }

  void setDescriptionLength(int length) {
    this.descriptionLength = length;
    notifyListeners();
  }

  void setRating(int rating) {
    this.rating = rating;
    notifyListeners();
  }

  void removeImage(int index) {
    if (imageWithType[index].id != null) {
      deleteImageIds.add(imageWithType[index].id ?? '');
    }

    imageWithType.removeAt(index);
    notifyListeners();
  }
}

enum ImageType { asset, network }

class ImageWithType {
  ImageType imageType;
  String? id;
  String? url;
  Asset? asset;

  ImageWithType(this.imageType);
}
