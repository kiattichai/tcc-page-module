import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/get_store_detail_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/public_page_module/widget/review/review_detail.dart';
import 'package:app/module/public_page_module/widget/review/review_screen_viewmodel.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

import '../../../../core/base_state_ful.dart';
import '../../../../core/base_view_model.dart';
import '../../../../core/base_widget.dart';
import '../../../../errors/base_error_entity.dart';
import '../../../../globals.dart' as globals;
import 'edit_review_screen.dart';

class ReviewScreen extends StatefulWidget with TixRoute {
  final GetOrganizerStoreDetailData? storeDetailData;

  const ReviewScreen({Key? key, this.storeDetailData}) : super(key: key);

  @override
  _ReviewScreen createState() => _ReviewScreen();

  @override
  String buildPath() {
    return '/review';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => ReviewScreen(
              storeDetailData: data,
            ));
  }
}

class _ReviewScreen extends BaseStateProvider<ReviewScreen, ReviewViewModel> with TixNavigateMixin {
  @override
  void initState() {
    viewModel = ReviewViewModel("${widget.storeDetailData?.id}");
    viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ReviewViewModel>(
      builder: (context, model, child) {
        return Scaffold(
            appBar: AppBarWidget(name: widget.storeDetailData?.name),
            body: Container(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Container(
                  padding: const EdgeInsets.only(top: 13, bottom: 13),
                  child: Text("รีวิวจากผู้ใช้บริการ",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff4a4a4a),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      )),
                ),
                Expanded(
                    child: RefreshIndicator(
                  onRefresh: () {
                    viewModel.clearReviewPage();
                    viewModel.getReview();
                    return Future.value();
                  },
                  child: ListView.builder(
                      itemCount: viewModel.getReviewList()?.length ?? 0,
                      controller: viewModel.reviewScrollController,
                      itemBuilder: (context, index) {
                        var item = viewModel.reviewDataList[index];
                        return Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 17.5),
                                width: MediaQuery.of(context).size.width,
                                height: 1,
                                decoration: new BoxDecoration(color: Colors.grey.shade300),
                              ),
                              Row(
                                children: [
                                  Container(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(30.0),
                                      child: CircleAvatar(
                                        //child: Image.asset(reviews[index]["img"]),
                                        child: Image.network(
                                          '${item.user?.image?.resizeUrl}',
                                          fit: BoxFit.cover,
                                          width: 68,
                                          height: 68,
                                          errorBuilder: (context, error, stackTrace) => Image(
                                            width: 68,
                                            height: 68,
                                            image: AssetImage("assets/image-empty.png"),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 12.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${item.user?.firstName}  ' +
                                                  '${item.user?.lastName}',
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff4a4a4a),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            ),
                                            globals.userDetail?.data?.id == item.user?.id
                                                ? InkWell(
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          'แก้ไข',
                                                          style: TextStyle(
                                                            fontFamily: 'SukhumvitSet-Text',
                                                            color: Color(0xff4a4a4a),
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w400,
                                                            fontStyle: FontStyle.normal,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 4,
                                                        ),
                                                        Image(
                                                          image: AssetImage('assets/pencil.png'),
                                                          width: 10,
                                                          height: 10,
                                                          color: Color(0xFF545454),
                                                        )
                                                      ],
                                                    ),
                                                    onTap: () {
                                                      // TixNavigate.instance.navigateTo(EditReviewScreen(),data: {
                                                      //   'storeName':widget.storeDetailData?.name,
                                                      //   'storeId':widget.storeDetailData?.id,
                                                      //   'reviewId':item.id,
                                                      // }).then((value) => viewModel.getReviewByIndex(index));

                                                      WrapNavigation.instance.pushNamed(
                                                          context, EditReviewScreen(),
                                                          arguments: {
                                                            'storeName':
                                                                widget.storeDetailData?.name,
                                                            'storeId': widget.storeDetailData?.id,
                                                            'reviewId': item.id,
                                                          }).then((value) {
                                                        viewModel.getReviewByIndex(index);
                                                      });
                                                    },
                                                  )
                                                : SizedBox()
                                          ],
                                        )),
                                  ),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Container(
                                          child: RatingBar.builder(
                                            initialRating: (item.rating ?? 0.0).toDouble(),
                                            minRating: 1,
                                            ignoreGestures: false,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemSize: 20.0,
                                            itemPadding: EdgeInsets.only(right: 0.0),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Color(0xffda3534),
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            '${item.createdAt?.date} ' + '${item.createdAt?.time}',
                                            style: TextStyle(
                                              fontFamily: 'SukhumvitSet-Text',
                                              color: Color(0xff4a4a4a),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 18.0, bottom: 16.0),
                                child: new Text(
                                  '${item.description}',
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xff555555),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              (item.images ?? []).isNotEmpty
                                  ? Container(
                                      margin: EdgeInsets.only(bottom: 14.0),
                                      height: 100,
                                      child: ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: item.images?.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            return Container(
                                                padding: index != 0
                                                    ? const EdgeInsets.only(left: 10)
                                                    : null,
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.all(Radius.circular(6)),
                                                    child: Image(
                                                        width: 100,
                                                        height: 100,
                                                        fit: BoxFit.cover,
                                                        image: NetworkImage(
                                                            item.images?[index].url ?? ''))));
                                          }),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        );
                      }),
                ))
              ]),
            ));
      },
      model: viewModel,
    );
  }

  void showAlertError(BaseError baseError) {
    if (showing == true) return;

    showing = true;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }
}
