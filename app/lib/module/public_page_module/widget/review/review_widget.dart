import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/review/edit_review_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

import 'review_screen.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/review/edit_review_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

import 'review_screen.dart';

class ReviewWidget extends StatelessWidget {
  HomeViewModel viewModel;
  Function(bool force) reloadReview;
  Function(int index) reloadReviewByIndex;

  ReviewWidget(this.viewModel, this.reloadReview, this.reloadReviewByIndex);

  @override
  Widget build(BuildContext context) {
    return (viewModel.reviewData?.pagination?.total ?? 0) > 0
        ? Container(
            // decoration: BoxDecoration(
            //   border: Border(top: BorderSide(width: 3.0, color: Colors.grey.shade300)),
            // ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.5, left: 16.0),
                        child: Text(
                          "คะแนนรีวิว",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff08080a),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                            child: Text(
                              "${viewModel.storeDetail?.averageRating?.toDouble()}",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff08080a),
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              child: RatingBar.builder(
                                ignoreGestures: false,
                                initialRating:
                                    viewModel.storeDetail?.averageRating?.toDouble() ?? 0,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemSize: 24.0,
                                itemPadding: EdgeInsets.only(right: 0.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.black,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 16.0),
                            child: GestureDetector(
                              onTap: () {
                                navigateToReview(context);
                              },
                              child: Row(
                                children: [
                                  new Text(
                                    "ดูทั้งหมด",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff08080a),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0, top: 3.0),
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      size: 10.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 67.0, bottom: 12.5),
                        child: new Text(
                          "${viewModel.storeDetail?.reviewTotal} " + "รีวิว",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet',
                            color: Color(0xff08080a),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      Container(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: viewModel.reviewData?.record?.length ?? 0,
                            itemBuilder: (context, index) {
                              var item = viewModel.reviewData?.record?[index];
                              return Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(bottom: 17.5),
                                      width: MediaQuery.of(context).size.width,
                                      height: 1,
                                      decoration: new BoxDecoration(color: Colors.grey.shade300),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 16.0),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(30.0),
                                            child: CircleAvatar(
                                              //child: Image.asset(reviews[index]["img"]),
                                              child: Image.network(
                                                '${item?.user?.image?.url}',
                                                errorBuilder: (context, error, stackTrace) => Image(
                                                  width: 68,
                                                  height: 68,
                                                  image: AssetImage("assets/image-empty.png"),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(left: 12.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '${item?.user?.firstName}  ' +
                                                        '${item?.user?.lastName}',
                                                    style: TextStyle(
                                                      fontFamily: 'SukhumvitSet',
                                                      color: Color(0xff4a4a4a),
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w700,
                                                      fontStyle: FontStyle.normal,
                                                    ),
                                                  ),
                                                  globals.userDetail?.data?.id == item?.user?.id
                                                      ? InkWell(
                                                          child: Row(
                                                            children: [
                                                              Text(
                                                                'แก้ไข',
                                                                style: TextStyle(
                                                                  fontFamily: 'SukhumvitSet',
                                                                  color: Color(0xff4a4a4a),
                                                                  fontSize: 12,
                                                                  fontWeight: FontWeight.w400,
                                                                  fontStyle: FontStyle.normal,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 4,
                                                              ),
                                                              Image(
                                                                image:
                                                                    AssetImage('assets/pencil.png'),
                                                                width: 10,
                                                                height: 10,
                                                                color: Color(0xFF545454),
                                                              )
                                                            ],
                                                          ),
                                                          onTap: () {
                                                            //TixNavigate.instance.navigateTo(EditReviewScreen(),data: {'storeName':viewModel.storeDetail?.name, 'storeId':viewModel.storeDetail?.id, 'reviewId':item?.id,}).then((value) =>  reloadReviewByIndex(index));
                                                            WrapNavigation.instance.pushNamed(
                                                                context, EditReviewScreen(),
                                                                arguments: {
                                                                  'storeName':
                                                                      viewModel.storeDetail?.name,
                                                                  'storeId':
                                                                      viewModel.storeDetail?.id,
                                                                  'reviewId': item?.id,
                                                                }).then((value) {
                                                              reloadReviewByIndex(index);
                                                            });
                                                          },
                                                        )
                                                      : SizedBox()
                                                ],
                                              )),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(right: 16.0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                child: RatingBar.builder(
                                                  initialRating: (item?.rating ?? 0.0).toDouble(),
                                                  minRating: 1,
                                                  direction: Axis.horizontal,
                                                  allowHalfRating: true,
                                                  itemCount: 5,
                                                  itemSize: 20.0,
                                                  itemPadding: EdgeInsets.only(right: 0.0),
                                                  itemBuilder: (context, _) => Icon(
                                                    Icons.star,
                                                    color: Color(0xffda3534),
                                                  ),
                                                  onRatingUpdate: (rating) {
                                                    print(rating);
                                                  },
                                                ),
                                              ),
                                              Container(
                                                child: new Text(
                                                  '${item?.createdAt?.date} ' +
                                                      '${item?.createdAt?.time}',
                                                  style: TextStyle(
                                                    fontFamily: 'SukhumvitSet',
                                                    color: Color(0xff4a4a4a),
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: 16.0, right: 16.0, top: 18.0, bottom: 14.0),
                                      child: new Text(
                                        '${item?.description}',
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet',
                                          color: Color(0xff555555),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    (item?.images ?? []).isNotEmpty
                                        ? Container(
                                            margin: EdgeInsets.only(left: 16.0, bottom: 27.0),
                                            height: 100,
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: item?.images?.length,
                                                scrollDirection: Axis.horizontal,
                                                itemBuilder: (context, index) {
                                                  final data = item?.images?[index];
                                                  return Container(
                                                      padding: index != 0
                                                          ? const EdgeInsets.only(left: 10)
                                                          : null,
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.all(Radius.circular(6)),
                                                        child: Image.network(
                                                          data?.url ?? '',
                                                          fit: BoxFit.cover,
                                                          width: 68,
                                                          height: 68,
                                                          errorBuilder:
                                                              (context, error, stackTrace) => Image(
                                                            width: 68,
                                                            height: 68,
                                                            image: AssetImage(
                                                                "assets/image-empty.png"),
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                      ));
                                                }),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                              );
                            }),
                      )
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    navigateToReview(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 9.5,
                      bottom: 8,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "ดูรีวิวทั้งหมด",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet',
                              color: Color(0xff08080a),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 13.0, top: 3.0),
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 10.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  shape: Border(
                    top: BorderSide(color: Colors.grey.shade300, width: 1),
                    // bottom: BorderSide(
                    //     color: Colors.grey.shade300, width: 3),
                  ),
                ),
              ],
            ),
          )
        : SizedBox();
  }

  navigateToReview(BuildContext context) {
    // TixNavigate.instance.navigateTo(ReviewScreen(), data: viewModel.storeDetail).then((value) {
    //   reloadReview();
    // });
    WrapNavigation.instance
        .pushNamed(context, ReviewScreen(), arguments: viewModel.storeDetail)
        .then((value) {
      reloadReview(true);
    });
  }
}
