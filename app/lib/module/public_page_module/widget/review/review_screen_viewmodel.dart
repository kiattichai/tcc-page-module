import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/review.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class ReviewViewModel extends BaseViewModel {
  final String storeId;

  GetReviewData? reviewData;

  late ScrollController reviewScrollController;
  List<GetReviewDataRecord> reviewDataList = [];
  int reviewLimit = 10;
  int reviewPage = 1;
  bool isReviewComplete = false;
  bool noInternet = false;
  Function(BaseError)? showAlertError;

  @override
  void postInit() {
    super.postInit();
    clearReviewPage();
    getReview();
    // showAlertError!(BaseError()..message ='ทดสอบ');
  }

  ReviewViewModel(this.storeId) {
    reviewScrollController = new ScrollController();
    reviewScrollController.addListener(() {
      if (reviewScrollController.position.extentAfter == 0 && !isReviewComplete) {
        reviewPage++;
        getReview();
      }
    });
  }

  void getReview() {
    catchError(() async {
      setLoading(true);
      if (reviewData?.pagination?.lastPage != reviewPage) {
        isReviewComplete = false;
      } else {
        isReviewComplete = true;
      }
      if (reviewPage == 1) {
        reviewDataList.clear();
      }
      reviewData = await di.pageRepository.getStoreReview(storeId, reviewLimit, page: reviewPage);
      if (reviewData?.record != null) {
        reviewDataList.addAll(reviewData?.record ?? []);
      }

      notifyListeners();
      setLoading(false);
    });
  }

  void getReviewByIndex(index) {
    catchError(() async {
      var review =  reviewDataList[index];
      var result =  await di.reviewRepository.getStoreReviewById(int.parse(storeId), review.id ?? 0);
      if(result != null){
        reviewDataList[index] = result;
        notifyListeners();
      }
    });
  }

  List<Review>? getReviewList() {
    try {
      return reviewDataList
          .map((e) => Review(
          e.user?.image?.resizeUrl ?? '',
          '${e.user?.firstName} ${e.user?.lastName}',
          e.rating ?? 0,
          DateFormat('dd-MMM-yyyy – HH:mm น.', 'th')
              .format(DateFormat('dd-MM-yyyy HH:mm').parse(e.createdAt?.value ?? '')),
          e.description ?? '',
      user: e.user))
          .toList();
    } catch (e) {
      return null;
    }
  }

  clearReviewPage() {
    reviewPage = 1;
    isReviewComplete = false;
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      if (error.code == 0) {
        setNoInternet(true);
      }
      showAlertError!(error);
    }
  }

  void setNoInternet(bool noInternet) {
    this.noInternet = noInternet;
    notifyListeners();
  }
}