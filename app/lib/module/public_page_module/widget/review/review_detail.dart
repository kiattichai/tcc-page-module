import 'package:app/model/review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../../globals.dart' as globals;

class ReviewDetail extends StatelessWidget {
  final Review? review;

  final bool isLast;

  const ReviewDetail({Key? key, required this.review, required this.isLast})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: EdgeInsets.only(top: 17.5),
            child: Row(
              children: [
                ClipOval(
                  child: Image.network(review?.imageUrl ?? '',
                      fit: BoxFit.cover,
                      width: 36,
                      height: 36,
                      errorBuilder: (context, error, stackTrace) => Image(
                            width: 36,
                            height: 36,
                            image: AssetImage("assets/no_image.jpeg"),
                          )),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(review?.name ?? '',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            )),
                        globals.userDetail?.data?.id == review?.user?.id ? Row(
                          children: [
                            Text(
                              'แก้ไข',
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xff4a4a4a),
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),SizedBox(width: 4,) ,
                            Image(image: AssetImage('assets/pencil.png'),width: 10,height: 10,color: Color(0xFF545454),)
                          ],
                        )  : SizedBox()
                      ],
                    )),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    getRating(Color(0xffda3534), Color(0xff989898),
                        review?.rating.toDouble() ?? 0, 13),
                    new Text(review?.time ?? '',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff4a4a4a),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ))
                  ],
                )
              ],
            )),
        Container(
          padding: EdgeInsets.only(top: 18),
          child: new Text(
            review?.comment ?? '',
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff555555),
              fontSize: 14,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        isLast
            ? Container()
            : Padding(
                padding: EdgeInsets.only(top: 14),
                child: Container(
                  height: 1,
                  decoration: BoxDecoration(
                      border: Border(
                    top: BorderSide(
                      color: Color(0xFFF0F0F0),
                      width: 2.0,
                    ),
                  )),
                ),
              )
      ],
    );
  }

  static Widget getRating(
      Color ratingColor, Color iconColor, double rating, double size) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: List<Widget>.generate(
          5,
          (int index) => (index + 1) <= rating.floor()
              ? Icon(Icons.star, color: ratingColor, size: size)
              : Icon(Icons.star, color: iconColor, size: size)),
    );
  }
}
