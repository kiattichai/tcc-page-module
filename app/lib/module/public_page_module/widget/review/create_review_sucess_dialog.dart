import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SaveSuccessDialog extends StatelessWidget {
  String assetImage;
  String title;
  String subTitle;
  String closeButton;

  SaveSuccessDialog(this.assetImage, this.title, this.subTitle, {this.closeButton = 'ปิด'});

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        backgroundColor: Colors.white,
        insetPadding: const EdgeInsets.only(left: 24, right: 19),
        child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 30),
                  child: Image(
                    image: AssetImage('assets/$assetImage'),
                    width: 28,
                    height: 28,
                    color: Color(0xFF0CAF12),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 18),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xFF4A4A4A),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 7),
                  child: Text(
                    subTitle,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xFF4A4A4A),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 59, left: 16, right: 16, bottom: 22),
                  width: double.infinity,
                  height: 42,
                  child: ElevatedButton(
                    child: Text(
                      closeButton,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: true ? Color(0xffffffff) : Color(0xffb2b2b2),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            true ? Color(0xffda3534) : Color(0xffe6e7ec)),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ))),
                    onPressed: () => WrapNavigation.instance.pop(context),
                  ),
                )
              ],
            )));
  }
}
