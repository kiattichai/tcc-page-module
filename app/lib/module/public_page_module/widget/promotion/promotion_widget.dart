import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;

class PromotionWidget extends StatelessWidget {
  final HomeViewModel viewModel;

  const PromotionWidget({Key? key, required this.viewModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return (viewModel.promotionData?.pagination?.total ?? 0) > 0
        ? Container(
            child: Column(
              children: [
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 16.0, top: 19.5),
                          child: new Text(
                            "โปรโมชัน",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff333333),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.5714285714285714,
                            ),
                          ),
                        ),
                        // Container(
                        //   padding: EdgeInsets.only(top: 15.5, right: 16.0),
                        //   child: GestureDetector(
                        //     child: Row(
                        //       children: [
                        //         new Text(
                        //           "ดูทั้งหมด",
                        //           style: TextStyle(
                        //             fontFamily: 'SukhumvitSet-Text',
                        //             color: Color(0xff08080a),
                        //             fontSize: 14,
                        //             fontWeight: FontWeight.w400,
                        //             fontStyle: FontStyle.normal,
                        //           ),
                        //         ),
                        //         SizedBox(
                        //           width: 10.0,
                        //         ),
                        //         Image(
                        //           width: 6,
                        //           height: 10,
                        //           image: AssetImage("assets/chevron_right2.png"),
                        //         )
                        //       ],
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                GridView.builder(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 19.5),
                    itemCount: viewModel.promotionData?.record?.length ?? 0,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 15.0,
                      mainAxisSpacing: 15.0,
                      childAspectRatio: 164 / 277,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      final item = viewModel.promotionData?.record?[index];
                      return InkWell(
                        child: Container(
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x26000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 4,
                                  spreadRadius: 0),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 164.0,
                                width: 164.0,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(3.0),
                                    topRight: Radius.circular(3.0),
                                  ),
                                  // child: Image.asset(
                                  //   'images/concert1.jpg',fit: BoxFit.fill,),
                                  child: Image.network(
                                    '${viewModel.promotionData?.record?[index].images?.first.url}',
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 8.0,
                                  right: 11.0,
                                ),
                                child: Text(
                                  "${viewModel.promotionData?.record?[index].name}",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontFamily: 'SukhumvitSet',
                                      color: Color(0xff120e0e),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      height: 1.1),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 7.0,
                                ),
                                child: Text(
                                  "${viewModel.promotionData?.record?[index].description}",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet',
                                    color: Color(0xff989898),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 9.0),
                                    child: new Text(
                                      "${viewModel.promotionData?.record?[index].showTime?.textShortDate}",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet',
                                        color: Color(0xff808080),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding:
                                  //   const EdgeInsets.only(
                                  //       right: 11.0),
                                  //   child: new Text("฿${promotion[index]["price"]}",
                                  //       style: TextStyle(
                                  //         fontFamily: 'SFProText',
                                  //         color:
                                  //         Color(0xff08080a),
                                  //         fontSize: 16,
                                  //         fontWeight:
                                  //         FontWeight.w700,
                                  //         fontStyle:
                                  //         FontStyle.normal,
                                  //       )),
                                  // )
                                ],
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          WrapNavigation.instance.nativePushNamed(
                              context, ACTION_OPEN_VOUCHER_DETAIL,
                              arguments: {'id': '${item?.id}'});
                        },
                      );
                    }),
              ],
            ),
          )
        : Container(
            padding: const EdgeInsets.only(top: 16, bottom: 34),
            child: new Text("ยังไม่มีโปรโมชันในขณะนี้",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff6d6d6d),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.5,
                )),
          );
    ;
  }
}
