import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;

class PromotionRecommendWidget extends StatelessWidget {
  final HomeViewModel viewModel;

  const PromotionRecommendWidget({Key? key, required this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (viewModel.promotionData!.record!.isEmpty) {
      return Container(
        padding: const EdgeInsets.only(top: 16, bottom: 34),
        child: new Text("ไม่มีโปรโมชั่นแนะนำ",
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff6d6d6d),
              fontSize: 14,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: -0.5,
            )),
      );
    } else {
      return Container(
        margin: const EdgeInsets.only(top: 14, left: 15.0, bottom: 16.0),
        height: 360.0,
        child: ListView.builder(
          itemCount: viewModel.promotionData?.record?.length ?? 0,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            var item = viewModel.promotionData?.record![index];
            return InkWell(
                onTap: () {
                  WrapNavigation.instance.nativePushNamed(context, ACTION_OPEN_VOUCHER_DETAIL,
                      arguments: {'id': '${item?.id}'});
                },
                child: Card(
                  child: Container(
                    width: 228.0,
                    height: 349.0,
                    decoration: BoxDecoration(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(6)),
                          child: Image.network(
                            '${item?.images![0].url}',
                            height: 228.0,
                            width: double.infinity,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 16.0),
                          child: Text(
                            '${item?.name}',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff120e0e),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 3.0, left: 10.0),
                          child: Text(
                            '${item?.description}',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff545454),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10.0),
                                child: new Text("${item?.showTime?.textShortDate}",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff808080),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10.0),
                                child: item?.price?.max != 0
                                    ? Text(
                                        "฿${item?.price?.max}",
                                        style: TextStyle(
                                          fontFamily: 'SFProText',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      )
                                    : Text(
                                        "฿ฟรี",
                                        style: TextStyle(
                                          fontFamily: 'SFProText',
                                          color: Color(0xff08080a),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ));
          },
        ),
      );
    }
  }
}
