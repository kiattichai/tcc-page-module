import 'dart:io';

import 'package:app/module/public_page_module/add_gallery_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class AddGalleryWidget extends StatelessWidget {
  AddGalleryViewModel viewModel;

  AddGalleryWidget(this.viewModel);

  @override
  Widget build(BuildContext context) {
    if (viewModel.images.isEmpty) {
      return Container(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 100.0,
              ),
              Icon(
                Icons.image_not_supported_rounded,
                size: 50.0,
                color: Colors.grey.shade300,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'ยังไม่มีรูปภาพ',
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Colors.grey.shade400,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: ListView.builder(
          itemCount: viewModel.images.length,
          itemBuilder: (context, index) {
            File file = viewModel.images[index];
            TextEditingController  controller = viewModel.textDescriptions[index];
            return Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        padding:
                            EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.file(
                            file,
                            width: 343,
                            height: 343,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 30.0,
                        right: 30.0,
                        child: InkWell(
                          onTap: () {
                            viewModel.removeImage(index);
                          },
                          child: Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Color(0x66120e0e),
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: TextField(
                      controller: controller,
                      decoration: InputDecoration(
                        hintText: 'เพิ่มคำบรรยาย...',
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );
    }
  }
}
