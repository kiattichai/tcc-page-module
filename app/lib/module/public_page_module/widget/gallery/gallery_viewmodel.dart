import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:flutter/cupertino.dart';

class GalleryViewModel extends BaseViewModel{
  ScrollController scrollController = ScrollController();
  final String storeId;
  int imageLimit = 10;
  int imagePage = 1;
  bool isComplete = false;
  int _imageIndex = 1;

  GetGalleryData? galleryData;
  List<GetGalleryDataRecord> galleryDataList = [];

  GalleryViewModel(this.storeId){
    scrollController.addListener(() {
      if(scrollController.position.extentAfter == 0 && !isComplete){
        getImage();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    //clearGallerPage();
    getImage();
  }

  void clearGallerPage() {
    imagePage = 1;
    galleryDataList.clear();
    isComplete = false;
  }

  @override
  void onError(error) {
    super.onError(error);
  }

  void getImage() async {
    catchError(() async{
      if(imagePage == 1){
        galleryDataList.clear();
      }
      setLoading(true);
      galleryData = await di.pageRepository.getStoreGallery(storeId, imageLimit, imagePage);
      if(galleryData == null || galleryData?.record == null) return;

      if(galleryData != null && galleryData!.record!.length > 0 && galleryData!.pagination!.currentPage! <= galleryData!.pagination!.lastPage!){
        galleryDataList.addAll(galleryData?.record ?? []);
        imagePage++;
        isComplete = false;
      } else {
        isComplete = true;
      }
      setLoading(false);
    });
  }


  void imageIndex(int index){
    _imageIndex = index + 1;
    notifyListeners();
  }

  getImageIndex(){
    return _imageIndex;
  }
}