import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_edit_viewmodel.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class GalleryEditDialog extends StatefulWidget {
  Map data;

  GalleryEditDialog(this.data);

  @override
  _GalleryEditDialogState createState() => _GalleryEditDialogState();
}

class _GalleryEditDialogState extends BaseStateProvider<GalleryEditDialog, GalleryEditViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = GalleryEditViewModel(widget.data);
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<GalleryEditViewModel>(
        builder: (context, model, child) {
          return Dialog(
            insetPadding: EdgeInsets.only(left: 16.0, right: 8.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(7.0),
            ),
            backgroundColor: Colors.transparent,
            child: customDialog(context),
          );
        },
        model: viewModel);
  }

  Widget customDialog(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 11.0,
              ),
              margin: EdgeInsets.only(top: 13.0, right: 8.0),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(7.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 13.0),
                    child: new Text("แก้ไข",
                        style: TextStyle(
                          fontFamily: 'sukhumvitSet-Text',
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  Stack(
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 16.0, left: 13.0, right: 13.0),
                          width: 301,
                          height: 301,
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(7.0),
                            child: Image.network(
                              viewModel.url,
                              fit: BoxFit.cover,
                            ),
                          )),
                      Positioned(
                        top: 20.0,
                        right: 20.0,
                        child: InkWell(
                          onTap: () {
                            //TixNavigate.instance.pop();
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return ConfirmDialog(context);
                                });
                          },
                          child: new Container(
                            width: 35,
                            height: 35,
                            decoration: new BoxDecoration(
                              color: Color(0x7f545454),
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: Image.asset('assets/trash.png'),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 0.0, bottom: 15.0, left: 13.0, right: 13.0),
                    child: TextFormField(
                        controller: viewModel.textDescription,
                        decoration: InputDecoration(
                            border:
                                UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                              color: Colors.grey,
                            )))),
                  ),
                  Center(
                    child: InkWell(
                      onTap: () {
                        viewModel.updateDescription(context);
                      },
                      child: Container(
                        width: 285,
                        height: 40,
                        margin: EdgeInsets.only(left: 13.0, right: 13.0, bottom: 15.0),
                        decoration: new BoxDecoration(
                            color: Color(0xffda3534), borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: new Text(
                            "บันทึก",
                            style: TextStyle(
                              fontFamily: 'sukhumvitSet-Text',
                              color: Color(0xffffffff),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0.0,
              child: GestureDetector(
                onTap: () {
                  WrapNavigation.instance.pop(context);
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: CircleAvatar(
                    radius: 14.0,
                    backgroundColor: Colors.white,
                    child: Icon(Icons.close, color: Colors.black),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget ConfirmDialog(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.only(left: 16.0, right: 16.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: Container(
        padding: EdgeInsets.only(top: 20.0, bottom: 21.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              child: new Text(
                "ลบรูปนี้",
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  color: Color(0xff000000),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.4000000059604645,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 35.0),
              child: new Text(
                "คุณต้องการลบรูปนี้ของคุณ?",
                style: TextStyle(
                  fontFamily: 'sukhumvitSet-Text',
                  color: Color(0xff000000),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print('ยกเลิก');
                    WrapNavigation.instance.pop(context);
                  },
                  child: Container(
                    width: 137,
                    height: 43,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.red),
                    ),
                    child: Center(
                      child: Text(
                        'ยกเลิก',
                        style: TextStyle(
                          fontFamily: 'sukhumvitSet-Text',
                          color: Colors.red,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 21.0,
                ),
                InkWell(
                  onTap: () {
                    viewModel.deleteImage(context);
                    //TixNavigate.instance.pop();
                  },
                  child: Container(
                    width: 137,
                    height: 43,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.red,
                      border: Border.all(color: Colors.red),
                    ),
                    child: Center(
                      child: Text(
                        'ยืนยัน',
                        style: TextStyle(
                          fontFamily: 'sukhumvitSet-Text',
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
