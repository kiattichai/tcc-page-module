import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_edit_dialog.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_slide.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_viewmodel.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class GalleryScreen extends StatefulWidget with TixRoute {
  String? storeId;
  String? storeName;
  late GalleryViewModel? viewModel;

  GalleryScreen({Key? key, this.storeId, this.storeName}) : super(key: key);

  @override
  _GalleryScreenState createState() => _GalleryScreenState();

  @override
  String buildPath() {
    return '/gallery_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => GalleryScreen(
              storeId: data['storeId'],
              storeName: data['storeName'],
            ));
  }
}

class _GalleryScreenState extends BaseStateProvider<GalleryScreen, GalleryViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = GalleryViewModel(widget.storeId ?? '');
  }

  @override
  void dispose() {
    viewModel.scrollController.removeListener(() {});
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.storeId);
    return BaseWidget<GalleryViewModel>(
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(
            title: new Text(
              widget.storeName ?? '',
              style: TextStyle(
                fontFamily: 'sukhumvitSet-Text',
                color: Color(0xff323232),
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.16,
              ),
            ),
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: RefreshIndicator(
            onRefresh: () {
              viewModel.clearGallerPage();
              viewModel.getImage();
              return Future.value();
            },
            child: SingleChildScrollView(
              controller: viewModel.scrollController,
              child: Container(
                child: GridView.builder(
                    itemCount: viewModel.galleryDataList.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 2.0,
                      mainAxisSpacing: 2.0,
                      childAspectRatio: 187 / 187,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      var item = viewModel.galleryDataList[index];
                      return GestureDetector(
                        onTap: () async {
                          Map<String, dynamic> data = {
                            "imgList": viewModel.galleryDataList,
                            "index": index,
                            "total": viewModel.galleryData?.pagination?.total,
                            "imagePage": viewModel.imagePage,
                            "storeId": viewModel.storeId,
                          };
                          //final result = await TixNavigate.instance.navigateTo(GallerySlide(), data: data,);
                          final result = await WrapNavigation.instance
                              .pushNamed(context, GallerySlide(), arguments: data);
                          if (result != null) {
                            showDialogEdit(result);
                          }
                        },
                        child: Container(
                          height: 187.0,
                          width: 187.0,
                          child: Image.network(
                            '${item.image?.url}',
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    }),
              ),
            ),
          ),
        );
      },
      model: viewModel,
    );
  }

  showDialogEdit(result) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          Map data = {
            "recordId": viewModel.galleryDataList[result].id,
            "userId": viewModel.galleryDataList[result].user?.id,
            "url": viewModel.galleryDataList[result].image?.url,
            "description": viewModel.galleryDataList[result].description,
            "storeId": widget.storeId,
          };
          return GalleryEditDialog(data);
        }).then((value) {
      if (value != null && value == true) {
        viewModel.clearGallerPage();
        viewModel.galleryDataList.clear();
        viewModel.getImage();
      } else {}
    });
  }
}
