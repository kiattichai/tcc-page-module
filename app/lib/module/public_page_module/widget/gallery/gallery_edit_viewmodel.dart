import 'dart:io';
import 'package:app/core/base_view_model.dart';
import 'package:app/model/gallery_update_response_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';

class GalleryEditViewModel extends BaseViewModel {
  Map data;
  TextEditingController textDescription = TextEditingController();
  String storeId = '';
  int recordId = 0;
  int userId = 0;
  String url = '';
  String description = '';
  File? imageFile;
  GalleryUpdateResponseData? galleryUpdateData;

  GalleryEditViewModel(this.data) {
    mapData();
  }

  void mapData() {
    storeId = data["storeId"];
    recordId = data["recordId"];
    userId = data["userId"];
    url = data["url"];
    textDescription.text = data["description"];
  }

  @override
  void postInit() {
    super.postInit();
  }

  void updateDescription(BuildContext context) async {
    catchError(() async {
      galleryUpdateData =
          await di.galleryRepository.updateDescription(recordId, textDescription.text);
      if (galleryUpdateData != null) {
        //TixNavigate.instance.pop(data: true);
        WrapNavigation.instance.pop(context, data: true);
      } else {
        //TixNavigate.instance.pop(data: false);
        WrapNavigation.instance.pop(context, data: false);
      }
    });
  }

  void deleteImage(BuildContext context) async {
    catchError(() async {
      galleryUpdateData = await di.galleryRepository.deleteImage(recordId);
      if (galleryUpdateData != null) {
        // TixNavigate.instance.pop(data: true);
        // TixNavigate.instance.pop(data: true);
        WrapNavigation.instance.pop(context, data: true);
        WrapNavigation.instance.pop(context, data: true);
      } else {
        //TixNavigate.instance.pop(data: false);
        WrapNavigation.instance.pop(context, data: false);
      }
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    print(error);
  }
}
