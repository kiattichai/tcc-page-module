import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_gallery_entity.dart';
import '../../../../globals.dart' as globals;

class GallerySlideViewModel extends BaseViewModel{
  Map? data;
  List<GetGalleryDataRecord> galleryDataList = [];
  GetGalleryData? galleryData;
  String storeId = "";
  int indexPage = 0;
  int? total;
  int? imagePage = 1;
  int imageLimit = 10;
  bool isComplete = false;
  bool isEdit = false;



  GallerySlideViewModel(this.data){
    mapData();
  }

  @override
  void postInit() {
    super.postInit();
  }

  void mapData(){
    galleryDataList.addAll(data!["imgList"] ?? []);
    total = data?["total"];
    storeId = data?["storeId"];
    imagePage = data?["imagePage"];
    indexPage = data?["index"];
    notifyListeners();
  }

  void mapStoreId() {
    storeId = data?["storeId"];
  }

  void pageIndex(int index) {
    print(index);
    indexPage = index++;
    notifyListeners();
  }

  getPageIndex(){
    return indexPage;
  }

  void mapTotal() {
    total = data?["total"];
    notifyListeners();
  }

  void mapImage() {
    galleryDataList.addAll(data?["imgList"] ?? []);
    notifyListeners();
  }

  void mapImagePage() {
    imagePage = data?["imagePage"];
    print(imagePage);
  }

  void getImage() async {
    catchError(() async{
      setLoading(true);

      galleryData = await di.pageRepository.getStoreGallery(storeId, imageLimit, imagePage!);
      if(galleryData == null || galleryData?.record == null) return;
      if(galleryData != null && galleryData!.record!.length > 0 && galleryData!.pagination!.currentPage! <= galleryData!.pagination!.lastPage!){
        galleryDataList.addAll(galleryData?.record ?? []);
        imagePage = imagePage! + 1;
        isComplete = false;
      } else {
        isComplete = true;
      }
      setLoading(false);
    });
  }


  @override
  void onError(error) {
    super.onError(error);
  }


}