import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_slide_viewmodel.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

class GallerySlide extends StatefulWidget with TixRoute {
  final Map? data;
  GallerySlide({Key? key, this.data}) : super(key: key);

  @override
  _GallerySlideState createState() => _GallerySlideState();

  @override
  String buildPath() {
    return '/gallery_slide';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => GallerySlide(data: data));
  }
}

class _GallerySlideState extends BaseStateProvider<GallerySlide, GallerySlideViewModel> {
  final CarouselController _controller = CarouselController();
  @override
  void initState() {
    super.initState();
    viewModel = GallerySlideViewModel(widget.data);
    //viewModel.pageIndex(widget.data?["index"]);
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<GallerySlideViewModel>(
        builder: (context, index, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              actions: [
                Center(
                  child: Container(
                    margin: EdgeInsets.only(right: 16.0),
                    child: new Text(
                      "${(viewModel.getPageIndex() + 1).toString() + "/" + viewModel.total.toString()}",
                      style: TextStyle(
                        fontFamily: 'SFUIText',
                        color: Color(0xffeeeeee),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: Container(
              height: Screen.height,
              decoration: BoxDecoration(color: Colors.black),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        width: Screen.width,
                        child: Stack(
                          children: [
                            Container(
                              child: CarouselSlider.builder(
                                itemCount: viewModel.galleryDataList.length,
                                carouselController: _controller,
                                options: CarouselOptions(
                                    initialPage: widget.data?["index"] ?? 0,
                                    height: Screen.height,
                                    viewportFraction: 1.0,
                                    enlargeCenterPage: false,
                                    enableInfiniteScroll: false,
                                    onPageChanged: (index, reason) {
                                      viewModel.notifyListeners();
                                      viewModel.pageIndex(index);
                                      int rang = viewModel.galleryDataList.length - index;
                                      if (rang < 5 && !viewModel.isComplete) {
                                        viewModel.getImage();
                                      }
                                    }),
                                itemBuilder: (context, index, realIdx) {
                                  var item = viewModel.galleryDataList[index];
                                  return Image.network(
                                    '${item.image?.url}',
                                  );
                                },
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: IconButton(
                                  onPressed: () {
                                    if (viewModel.indexPage != 0) {
                                      _controller.previousPage();
                                    }
                                  },
                                  icon: Icon(
                                    Icons.arrow_back_ios_sharp,
                                    color: Color(0xffeeeeee),
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                  onPressed: () {
                                    _controller.nextPage();
                                  },
                                  icon: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Color(0xffeeeeee),
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    viewModel.galleryDataList != []
                        ? Container(
                            padding: EdgeInsets.only(
                              left: 16.0,
                            ),
                            child: new Text(
                              "${viewModel.galleryDataList[viewModel.getPageIndex()].description}",
                              style: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                color: Color(0xffeeeeee),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          )
                        : SizedBox(),
                    viewModel.galleryDataList != []
                        ? Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 16.0),
                                child: RichText(
                                    text: new TextSpan(children: [
                                  new TextSpan(
                                      text:
                                          "${viewModel.galleryDataList[viewModel.getPageIndex()].user?.firstName} -",
                                      style: TextStyle(
                                        fontFamily: 'sukhumvitSet-Text',
                                        color: Color(0xffeeeeee),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                  new TextSpan(
                                      text:
                                          " ${viewModel.galleryDataList[viewModel.getPageIndex()].createdAt?.date}",
                                      style: TextStyle(
                                        fontFamily: 'sukhumvitSet-Text',
                                        color: Color(0xffeeeeee),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                  new TextSpan(
                                      text:
                                          " ${viewModel.galleryDataList[viewModel.getPageIndex()].createdAt?.time}",
                                      style: TextStyle(
                                        fontFamily: 'sukhumvitSet-Text',
                                        color: Color(0xffeeeeee),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ])),
                              ),
                              Spacer(),
                              globals.userDetail?.data?.id ==
                                      viewModel.galleryDataList[viewModel.getPageIndex()].user?.id
                                  ? InkWell(
                                      onTap: () {
                                        //TixNavigate.instance.pop(data: viewModel.getPageIndex());
                                        WrapNavigation.instance
                                            .pop(context, data: viewModel.getPageIndex());
                                      },
                                      child: Container(
                                        padding: EdgeInsets.only(right: 13.0),
                                        child: Row(
                                          children: [
                                            new Text("แก้ไข",
                                                style: TextStyle(
                                                  fontFamily: 'Thonburi',
                                                  color: Color(0xffb4b4b4),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                            Icon(
                                              Icons.edit,
                                              color: Color(0xffbababa),
                                              size: 15.0,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ),
          );
        },
        model: viewModel);
  }
}
