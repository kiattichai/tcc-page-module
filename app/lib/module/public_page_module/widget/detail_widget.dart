import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class DetailWidget extends StatelessWidget {
  final String title;
  Function()? navigateCallback;
  final String? detail;
  final Widget widget;

  DetailWidget(
      {Key? key,
      required this.title,
      required this.detail,
      required this.widget,
      this.navigateCallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 19.5),
        // decoration: BoxDecoration(
        //     border: Border(
        //       top: BorderSide(
        //         color: Colors.black12,
        //         width: 3.0,
        //       ),
        //       // bottom: BorderSide(
        //       //   color: Colors.black12,
        //       //   width: 2.0,
        //       // ),
        //     )),
        child: Column(
          children: [
            Row(
              children: [
                new Text(title,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff333333),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.5714285714285714,
                    )),
                Spacer(),
                detail != null
                    ? InkWell(
                        child: Container(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(detail ?? '',
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        onTap: navigateCallback,
                      )
                    : SizedBox(),
                detail != null
                    ? Image(
                        width: 6,
                        height: 10,
                        image: AssetImage("assets/chevron_right2.png"),
                      )
                    : SizedBox(),
              ],
            ),
            widget
          ],
        ));
  }
}
