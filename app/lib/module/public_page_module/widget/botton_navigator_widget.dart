import 'package:app/app/app_state.dart';
import 'package:app/model/create_review.dart';
import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/public_page_module/add_gallery_screen.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/checkIn/check_in_dialog.dart';
import 'package:app/module/public_page_module/widget/review/create_review_dialog.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart' as Native;
import 'package:app/globals.dart';

class BottonNavigatorWidget extends StatelessWidget {
  HomeViewModel viewModel;
  Function(CreateReview?) callback;
  Function() callbackSaveGallery;

  BottonNavigatorWidget(this.viewModel, this.callback, this.callbackSaveGallery);

  @override
  Widget build(BuildContext context) {
    final AppState appState = Provider.of(context);
    return Container(
      margin: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      padding: const EdgeInsets.only(top: 5, left: 16, bottom: 5, right: 16),
      child: Row(
        children: [
          //TODO เพิ่มรูป
          Expanded(
              child: InkWell(
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 110),
                    height: 37,
                    decoration: new BoxDecoration(
                        color: Color(0xffda3534), borderRadius: BorderRadius.circular(12)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(
                          width: 18,
                          height: 15,
                          image: AssetImage("assets/camera.png"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: new Text(
                            "เพิ่มรูป",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xffffffff),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  onTap: () async {
                    if (userAuthen?.data?.accessToken == null ||
                        userAuthen?.data?.accessToken == '') {
                      WrapNavigation.instance.nativePushNamed(context, ACTION_OPEN_LOGIN_PAGE);
                    } else {
                      final result = await WrapNavigation.instance
                          .pushNamed(context, AddGalleryScreen(), arguments: viewModel.storeId);
                      if (result != null) {
                        viewModel.getGalleryAndReState();
                        callbackSaveGallery();
                      }
                    }
                  })),
          SizedBox(
            width: 7,
          ),

          //TODO เขียนรีวิว
          Expanded(
            child: InkWell(
              onTap: () async {
                if (userAuthen?.data?.accessToken == null || userAuthen?.data?.accessToken == '') {
                  WrapNavigation.instance.nativePushNamed(context, ACTION_OPEN_LOGIN_PAGE);
                } else {
                  var result = await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CreateReviewDialog();
                      });
                  this.callback(result);
                }
              },
              child: Container(
                constraints: BoxConstraints(maxWidth: 110),
                height: 37,
                decoration: new BoxDecoration(
                    color: Color(0xffda3534), borderRadius: BorderRadius.circular(12)),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      width: 16,
                      height: 16,
                      image: AssetImage("assets/shape.png"),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: new Text(
                        "เขียนรีวิว",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xffffffff),
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 7,
          ),

          //TODO เช็คอิน
          viewModel.storeDetail?.section?.text == 'artist'
              ? SizedBox()
              : Expanded(
                  child: InkWell(
                    onTap: () async {
                      if (userAuthen?.data?.accessToken == null ||
                          userAuthen?.data?.accessToken == '') {
                        WrapNavigation.instance.nativePushNamed(context, ACTION_OPEN_LOGIN_PAGE);
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return CheckInDialog(viewModel);
                            });
                      }
                    },
                    child: Container(
                      width: 110,
                      height: 37,
                      decoration: new BoxDecoration(
                          color: Color(0xffda3534), borderRadius: BorderRadius.circular(12)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            width: 12,
                            height: 18,
                            image: AssetImage("assets/icons_marker.png"),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: new Text(
                              "เช็คอิน",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xffffffff),
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
