import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class GalleryWidget extends StatelessWidget {
  final GetGalleryData? galleryData;
  final String storeId;
  final String storeName;
  const GalleryWidget(
      {Key? key, required this.galleryData, required this.storeId, required this.storeName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if ((galleryData?.pagination?.total ?? 0) > 0) {
      if (galleryData!.record!.isNotEmpty) {
        return Container(
          padding: const EdgeInsets.only(top: 12, left: 16, right: 16, bottom: 19.5),
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(
              color: Colors.black12,
              width: 2.0,
            ),
            bottom: BorderSide(
              color: Colors.black12,
              width: 2.0,
            ),
          )),
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("ภาพที่เพิ่มโดยสมาชิก",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff4a4a4a),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
            Padding(
              padding: const EdgeInsets.only(
                top: 10,
              ),
              child: InkWell(
                onTap: () {
                  print('all gallery');
                  //TixNavigate.instance.navigateTo(GalleryScreen(), data: {'storeId':storeId, 'storeName':storeName},);
                  WrapNavigation.instance.pushNamed(context, GalleryScreen(),
                      arguments: {'storeId': storeId, 'storeName': storeName});
                },
                child: Container(
                  height: 107,
                  child: ListView.builder(
                      itemCount: galleryData?.record?.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Container(
                          padding: index != 0 ? const EdgeInsets.only(left: 10) : null,
                          child: index < 2
                              ? ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  child: Image.network(
                                    galleryData?.record?[index].image?.resizeUrl ?? '',
                                    height: 100.0,
                                    width: 100.0,
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Image.network(
                                        galleryData?.record?[index].image?.resizeUrl ?? '',
                                        fit: BoxFit.cover,
                                        height: 107,
                                        width: 107,
                                      ),
                                      Positioned.fill(
                                        child: Container(
                                          width: double.infinity,
                                          color: Color(0x66000000),
                                        ),
                                      ),
                                      Text(
                                        "+${(galleryData?.pagination?.total ?? 0) - (galleryData?.record?.length ?? 0)}",
                                        style: TextStyle(
                                          fontFamily: 'SFUIText',
                                          color: Color(0xffffffff),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        );
                      }),
                ),
              ),
            )
          ]),
        );
      } else {
        return Container(
          padding: const EdgeInsets.only(top: 16, bottom: 34),
          child: new Text("ไม่มีภาพที่เพิ่มโดยสมาชิก",
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff6d6d6d),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.5,
              )),
        );
      }
    } else {
      return SizedBox();
    }
  }
}

class CheckInUserViewModel extends BaseViewModel {
  GetGalleryData? getGalleryData;
  String storeId; //"5000090"
  int limit; //8

  CheckInUserViewModel(this.storeId, this.limit);

  @override
  void postInit() {
    super.postInit();
    getCheckInUser();
  }

  void getCheckInUser() {
    catchError(() async {
      getGalleryData = await di.pageRepository.getStoreGallery(storeId, limit, 1);
      notifyListeners();
    });
  }

  @override
  void onError(error) {
    super.onError(error);
  }
}
