import 'package:app/model/get_check_in_user_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/get_store_detail_entity.dart';
import 'package:app/module/public_page_module/widget/checkIn/check_in_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CheckInWidget extends StatelessWidget {
  final GetCheckInUserData? checkInUserData;
  final GetOrganizerStoreDetailData? storeDetail;

  const CheckInWidget({Key? key, required this.storeDetail, required this.checkInUserData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isOver = false;

    if ((checkInUserData?.pagination?.total ?? 0) > 0) {
      if ((checkInUserData?.record ?? []).isNotEmpty) {
        return Container(
          padding: const EdgeInsets.only(top: 19.5, left: 16, right: 16, bottom: 19.5),
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(
              color: Colors.black12,
              width: 3.0,
            ),
            bottom: BorderSide(
              color: Colors.black12,
              width: 2.0,
            ),
          )),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "เช็คอิน ${checkInUserData?.meta?.userTotal} คน",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff4a4a4a),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 11,
                ),
                child: Container(
                    height: 36,
                    child: ListView.builder(
                        itemCount: checkInUserData?.record?.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Container(
                              padding: index != 0 ? const EdgeInsets.only(left: 6) : null,
                              child: index < 7
                                  ? InkWell(
                                      onTap: () => navigateCheckIn(context),
                                      child: ClipOval(
                                        child: Image.network(
                                            checkInUserData
                                                    ?.record?[index].user?.image?.resizeUrl ??
                                                '',
                                            fit: BoxFit.cover,
                                            width: 36,
                                            height: 36,
                                            errorBuilder: (context, error, stackTrace) => Image(
                                                  width: 36,
                                                  height: 36,
                                                  image: AssetImage("assets/no_image.jpeg"),
                                                )),
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () => navigateCheckIn(context),
                                      child: ClipOval(
                                          child: Stack(alignment: Alignment.center, children: [
                                        Image.network(
                                            checkInUserData
                                                    ?.record?[index].user?.image?.resizeUrl ??
                                                '',
                                            fit: BoxFit.cover,
                                            width: 36,
                                            height: 36,
                                            errorBuilder: (context, error, stackTrace) => Image(
                                                  width: 36,
                                                  height: 36,
                                                  image: AssetImage("assets/no_image.jpeg"),
                                                )),
                                        Positioned.fill(
                                            child: Container(
                                          width: double.infinity,
                                          color: Color(0x66000000),
                                        )),
                                        Text(
                                            '+${(checkInUserData?.pagination?.total ?? 0) - (checkInUserData?.record?.length ?? 0)}',
                                            style: TextStyle(
                                              fontFamily: 'SFUIText',
                                              color: Color(0xffffffff),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ))
                                      ])),
                                    ));
                        })),
              )
            ],
          ),
        );
      } else {
        return Container(
          padding: const EdgeInsets.only(top: 16, bottom: 34),
          child: new Text("ยังไม่มีคนเช็คอิน",
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff6d6d6d),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.5,
              )),
        );
      }
    } else {
      return SizedBox();
    }
  }

  void navigateCheckIn(BuildContext context) {
    //TixNavigate.instance.navigateTo(CheckInScreen(), data: {'storeId': storeDetail!.id.toString(), 'storeName': storeDetail!.name});
    WrapNavigation.instance.pushNamed(context, CheckInScreen(),
        arguments: {'storeId': storeDetail!.id.toString(), 'storeName': storeDetail!.name});
  }
}
