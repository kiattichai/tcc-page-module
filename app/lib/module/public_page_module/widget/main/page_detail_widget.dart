import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageDetailWidget extends StatelessWidget {
  HomeViewModel viewModel;

  PageDetailWidget(this.viewModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 15.5, left: 16, bottom: 15.5),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        (viewModel.storeDetail?.description ?? '').isNotEmpty
            ? Text(
                '${viewModel.storeDetail?.description}',
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff08080a),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              )
            : SizedBox(),
        (viewModel.storeDetail?.description ?? '').isNotEmpty
            ? SizedBox(
                height: 12,
              )
            : SizedBox(),
        RichText(
          text: new TextSpan(
            children: [
              new TextSpan(
                  text: "ที่ตั้ง",
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff333333),
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
              viewModel.storeDetail?.venue?.address != null
                  ? TextSpan(
                      text: ": ${viewModel.storeDetail?.venue?.address} " +
                          '${viewModel.storeDetail?.venue?.district?.name} ' +
                          '${viewModel.storeDetail?.venue?.city?.name} ' +
                          '${viewModel.storeDetail?.venue?.province?.name} ' +
                          '${viewModel.storeDetail?.venue?.zipCode}',
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff555555),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    )
                  : TextSpan(
                      text: ": ยังไม่ระบุ",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff555555),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
            ],
          ),
        ),
        (viewModel.storeDetail?.timesDisplay ?? []).isNotEmpty
            ? SizedBox(
                height: 12,
              )
            : SizedBox(),
        (viewModel.storeDetail?.timesDisplay ?? []).isNotEmpty
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    Icons.schedule,
                    color: Colors.black,
                    size: 18,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 9),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("เวลาทำการ",
                            style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff333333),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                                letterSpacing: -0.5)),
                        ...viewModel.storeDetail!.timesDisplay!
                            .map((e) => Text(e,
                                style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Color(0xff555555),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: -0.5)))
                            .toList()
                      ],
                    ),
                  )
                ],
              )
            : SizedBox(),
        ...getTabDetail(viewModel.storeDetail?.contact?.phone, Icons.phone, 'เบอร์โทรศัพท์'),
        viewModel.genreList() != null
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    Icons.music_note,
                    color: Colors.black,
                    size: 18,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(left: 9),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("แนวเพลง",
                              style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff333333),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: -0.5)),
                          Text(viewModel.genreList() ?? '',
                              style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff555555),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: -0.5)),
                        ],
                      ),
                    ),
                  )
                ],
              )
            : Container(),
        viewModel.genreList() != null
            ? SizedBox(
                height: 8,
              )
            : Container(),
         ... viewModel.storeDetail?.section?.id == 2 ? getTabDetail(viewModel.storeDetail?.parking?.text,
            Icons.directions_car, 'ที่จอดรถ') :[],
        ... viewModel.storeDetail?.section?.id == 2 ? getTabDetail(
            viewModel.storeDetail?.seats?.text, Icons.person, 'จำนวนที่นั่ง'):[],



      ]),
    );
  }

  static List<Widget> getTabDetail(
      String? detail, IconData icon, String title) {
    return detail != null && detail.isNotEmpty
        ? [
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  icon,
                  color: Colors.black,
                  size: 18,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 9),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(title,
                          style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff333333),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.5)),
                      Text(detail,
                          style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff555555),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.5)),
                    ],
                  ),
                )
              ],
            ),
          ]
        : [];
  }
}
