import 'package:app/model/concert.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:flutter/material.dart';

import '../concert/concert_tab_detail_widget.dart';
import '../detail_widget.dart';

class ConcertWidget extends StatelessWidget {
  HomeViewModel viewModel;
  ConcertWidget(this.viewModel);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: concert([]),
    );
  }

  Widget concert(List<Concert> items) {
    return DetailWidget(
      title: "คอนเสิร์ตที่กำลังมาถึง",
      detail: items.isNotEmpty ? "ดูทั้งหมด" : null,
      widget: ConcertTabDetail(items: items, withOut: 'ไม่มีคอนเสิร์ตที่กำลังมาถึง',),
    );
  }
}
