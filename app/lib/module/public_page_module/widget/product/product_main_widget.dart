import 'package:app/model/product.dart';
import 'package:flutter/material.dart';

class ProductMainWidget extends StatelessWidget {

  final List<Product> items;

  const ProductMainWidget({Key? key, required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (items.isEmpty) {
      return SizedBox();
    } else {
      return Container(
        padding: const EdgeInsets.only(top: 14),
        height: 176,
        child: ListView.builder(
          itemCount: items.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              width: 118,
              padding: index != 0 ? const EdgeInsets.only(left: 7) : null,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    child: Image.network(
                      items[index].imageUrl,
                      height: 118,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 8),
                    height: 25,
                    child: Text(
                      items[index].detail,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff545454),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    height: 19,
                    child: Text("฿${items[index].price}",
                        style: TextStyle(
                          fontFamily: 'SFUIText',
                          color: Color(0xff4a4a4a),
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                  )
                ],
              ),
            );
          },
        ),
      );
    }
  }
}
