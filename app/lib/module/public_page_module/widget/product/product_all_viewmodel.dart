import 'package:app/core/base_view_model.dart';
import 'package:app/model/product_all.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductAllViewModel extends BaseViewModel {
  String storeId;
  late TabController tabController;
  PageController pageController = PageController();
  int tabIndex = 0;
  List<ProductAll> products = [];

  List<ProductAll> productAll = [
    ProductAll("แจ็คเก็ต", "assets/jacket1.jpg", "แจ็คเก็ตหรอครับ", 250, 10, 225),
    ProductAll("แจ็คเก็ต", "assets/jacket2.jpg", "แจ็คเก็ตหรือเปล่า", 300, 0, 0),
    ProductAll("แจ็คเก็ต", "assets/jacket3.jpg", "เเจ็กเก็ตแน่ๆ", 299, 0, 0),
    ProductAll("หมวก", "assets/cap1.jpg", "หมวกสีแดง", 159, 0, 0),
    ProductAll("หมวก", "assets/cap2.png", "หมวกดำ", 179, 5, 170),
    ProductAll("ป้ายไฟ", "assets/light1.jpg", "ป้ายไฟอักษรดำ", 599, 0, 0),
    ProductAll("ป้ายไฟ", "assets/light2.jpg", "ป้ายไฟเส้นสีขาว", 699, 15, 594),
    ProductAll("ป้ายไฟ", "assets/light3.jpg", "ป้ายไฟเส้นสีแดง", 899, 0, 0),
  ];

  List<Tab> tabs = <Tab>[
    Tab(
      child: Text(
        'ทั้งหมด (8)',
        style: TextStyle(
          fontFamily: 'SukhumvitSet-Text',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
    Tab(
      child: Text(
        'เสื้อแจ็คเก็ต (3)',
        style: TextStyle(
          fontFamily: 'SukhumvitSet-Text',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
    Tab(
      child: Text(
        'หมวก (2)',
        style: TextStyle(
          fontFamily: 'SukhumvitSet-Text',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
    Tab(
      child: Text(
        'ป้ายไฟ (3)',
        style: TextStyle(
          fontFamily: 'SukhumvitSet-Text',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
  ];

  ProductAllViewModel(this.storeId, TickerProvider tickerProvider) {
    tabController = TabController(vsync: tickerProvider, length: 4, initialIndex: 0);
    tabController.addListener(() {
      tabIndex = tabController.index;
      print(tabIndex);
      sortProduct(tabIndex);
    });
    if(tabIndex == 0){
      products.clear();
      products.addAll(productAll);
    }

  }

  @override
  void postInit() {
    super.postInit();
  }

  @override
  void onError(error) {
    super.onError(error);
  }

  void sortProduct(int tabIndex) {
    products.clear();

    if(tabIndex == 0){
      products.addAll(productAll);
    }

    if(tabIndex == 1){
      productAll.forEach((element) {
        if(element.type == "แจ็คเก็ต"){
          products.add(element);
        }
      });
    }
    if(tabIndex == 2){
      productAll.forEach((element) {
        if(element.type == "หมวก"){
          products.add(element);
        }
      });
    }
    if(tabIndex == 3){
      productAll.forEach((element) {
        if(element.type == "ป้ายไฟ"){
          products.add(element);
        }
      });
    }
    notifyListeners();
  }

}
