import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/tab_product_widget.dart';
import 'package:app/module/public_page_module/widget/home/tab_widget.dart';
import 'package:app/module/public_page_module/widget/product/product_all_viewmodel.dart';
import 'package:app/module/public_page_module/widget/product/tab_widget.dart';
import 'package:app/widgets/expand_page_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ProductAll extends StatefulWidget with TixRoute {
  String storeId;

  ProductAll(this.storeId);

  @override
  _ProductAllState createState() => _ProductAllState();

  @override
  String buildPath() {
    return '/product_all';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => ProductAll(data));
  }
}

class _ProductAllState
    extends BaseStateProvider<ProductAll, ProductAllViewModel>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    viewModel = ProductAllViewModel(widget.storeId, this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductAllViewModel>(builder: (context, model, child) {
          return widgetContent();
          },
        model: viewModel);
  }

  widgetContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        //TODO TAB Bar
        TabBarProductWidget(
          tabController: viewModel.tabController,
          tabs: viewModel.tabs,
        ),
        //TODO Build item
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: GridView.builder(
            padding: EdgeInsets.only(left: 16.0, right: 16.0),
            itemCount: viewModel.products.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 9.0,
              mainAxisSpacing: 9.0,
              childAspectRatio: 167 / 260,
            ),
            itemBuilder: (BuildContext context, index) {
              var item = viewModel.products[index];
              return Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: 167.0,
                          width: 167.0,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey.shade300),
                              borderRadius: BorderRadius.circular(7.0)
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            child: Image.asset(item.url,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        item.onSale != 0 ? Positioned(
                          top: 0.0,
                            right: 0.0,
                            child: Stack(
                              children: [
                                Container(
                                    child: Image.asset('assets/label.png'),
                                ),
                                Positioned.fill(
                                  top: 2.0,
                                  child:  Align(
                                    alignment: Alignment.topCenter,
                                    child: Text('${item.onSale}%',
                                      style: TextStyle(
                                        fontFamily: 'SFUIText',
                                        color: Color(0xffffffff),
                                        fontSize: 10,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0.0625,
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned.fill(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text('ลด',
                                        style: TextStyle(
                                          fontFamily: 'SFUIText',
                                          color: Color(0xffffffff),
                                          fontSize: 10,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0.0625,
                                        ),
                                      ),
                                    ),
                                ),

                              ],
                            ),
                        ) : SizedBox(),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: Text(
                        '${item.name}',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                    item.onSale == 0 ? Container(
                      padding: EdgeInsets.only(top: 5.0),
                      child: Text(
                        "฿${item.price}",
                        style: TextStyle(
                          fontFamily: 'SFUIText',
                          color: Color(0xff4a4a4a),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ) : Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Text(
                            "฿${item.priceOnSale}",
                            style: TextStyle(
                              fontFamily: 'SFUIText',
                              color: Color(0xff4a4a4a),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                        SizedBox(width: 6.0,),
                        Container(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Text(
                            "฿${item.price}",
                            style: TextStyle(
                              fontFamily: 'SFUIText',
                              color: Color(0xff9b9b9b),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              decoration: TextDecoration.lineThrough
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
