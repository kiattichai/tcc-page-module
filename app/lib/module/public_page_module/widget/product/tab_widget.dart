import 'package:app/utils/screen.dart';
import 'package:flutter/material.dart';
class TabBarProductWidget extends StatelessWidget {
  TabController tabController;
  List<Tab> tabs;

  TabBarProductWidget({Key? key,  required this.tabController, required this.tabs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
      width: Screen.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 15.0, top: 15.0),
            child: new Text("สินค้าทั้งหมด",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xff333333),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.5714285714285714,
                ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0),
            child: TabBar(
              controller: tabController,
              labelColor: Color(0xff4a4a4a),
              indicatorColor: Colors.transparent,
              isScrollable: true,
              labelPadding: EdgeInsets.only(right: 16.0),
              tabs: tabs,
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.black12,
            width: 3.0,
          ),
        ),
      ),
    );
  }
}
