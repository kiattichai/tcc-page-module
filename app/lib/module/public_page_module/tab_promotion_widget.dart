import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/detail_widget.dart';
import 'package:app/module/public_page_module/widget/promotion/promotion_recommend_widget.dart';
import 'package:app/module/public_page_module/widget/promotion/promotion_widget.dart';
import 'package:app/utils/screen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TabPromotionWidget extends StatelessWidget {
  final HomeViewModel viewModel;
  final bool isOwner;

  const TabPromotionWidget({
    Key? key,
    required this.viewModel,
    required this.isOwner,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
      child: Column(
        children: [
          isOwner
              ? Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(width: 3.0, color: Colors.grey.shade300))),
                  child: Column(
                    children: [
                      Container(
                        width: Screen.width,
                        margin: EdgeInsets.fromLTRB(22.0, 23.5, 22.0, 16.0),
                        child: Text.rich(
                          TextSpan(children: <TextSpan>[
                            TextSpan(
                              text:
                                  '    หากโปรโมชันของคุณ เปิดให้มีการซื้อผ่านระบบออนไลน์   อาจมีค่าธรรมเนียม และจำเป็นต้องส่งเอกสาร ',
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            TextSpan(
                              text: 'อ่านรายละเอียด',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xff08080a),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  print('อ่านรายละเอียด');
                                },
                            ),
                          ]),
                        ),
                      ),
                      Container(
                        width: 343,
                        height: 42,
                        margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 15.5),
                        decoration: new BoxDecoration(
                            color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                        child: Center(
                          child: new Text("สร้างโปรโมชัน",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Color(0xffffffff),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                      ),
                    ],
                  ),
                )
              : SizedBox(),
          // Container(
          //   margin: EdgeInsets.only(
          //     left: 15.0,
          //     top: 16,
          //   ),
          //   child: Row(
          //     children: [
          //       new Text("โปรโมชัน",
          //           style: TextStyle(
          //             fontFamily: 'SukhumvitSet-Text',
          //             color: Color(0xff333333),
          //             fontSize: 16,
          //             fontWeight: FontWeight.w700,
          //             fontStyle: FontStyle.normal,
          //             letterSpacing: -0.5714285714285714,
          //           ))
          //     ],
          //   ),
          // ),
          // PromotionRecommendWidget(viewModel: viewModel),
          PromotionWidget(viewModel: viewModel),
        ],
      ),
    );
  }
}
