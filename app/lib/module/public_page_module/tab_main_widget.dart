import 'package:app/model/concert.dart';
import 'package:app/model/product.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:app/module/public_page_module/widget/concert/concert_main_widget.dart';
import 'package:app/module/public_page_module/widget/concert/concert_tab_detail_widget.dart';
import 'package:app/module/public_page_module/widget/main/check_in_widget.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/main/picture_widget.dart';
import 'package:app/module/public_page_module/widget/product/product_main_widget.dart';
import 'package:app/module/public_page_module/widget/product/product_widget.dart';
import 'package:app/module/public_page_module/widget/main/concert_widget.dart';
import 'package:app/module/public_page_module/widget/main/page_detail_widget.dart';
import 'package:app/module/public_page_module/widget/promotion/promotion_widget.dart';
import 'package:app/module/public_page_module/widget/review/review_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'widget/detail_widget.dart';

class TabMainWidget extends StatelessWidget {
  HomeViewModel viewModel;
  Function() reloadReview;
  Function(int index) reloadReviewByIndex;
  TabMainWidget(this.viewModel, this.reloadReview, this.reloadReviewByIndex);

  @override
  Widget build(BuildContext context) {
    List<Concert> items = viewModel.getConcertData?.record
            ?.map((e) => Concert(
                  e.id!,
                  e.images!.first.url!,
                  e.showTime!.textShortDate!,
                  e.name!,
                ))
            .toList() ??
        [];

    return Column(
      children: [
        //TODO: Page header
        PageDetailWidget(viewModel),

        //TODO: Page Detail
        viewModel.storeDetail?.description == null && viewModel.isOwner
            ? Padding(
                padding: const EdgeInsets.only(top: 16),
                child: InkWell(
                  child: Container(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          border: Border(top: BorderSide(color: Color(0xFFDDDDDD), width: 1))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('เขียนสั้นๆ เกี่ยวกับเพจของคุณ',
                              style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff333333),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: -0.5)),
                          SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.edit,
                            size: 14.0,
                          )
                        ],
                      )),
                  onTap: () => viewModel.navigateToEditProfile(context),
                ),
              )
            : SizedBox(),

        // //TODO: Concert widget
        // viewModel.getConcertData?.record?.length != 0
        //     ? Container(
        //   decoration: BoxDecoration(
        //       border: Border(
        //           top: BorderSide(width: 3.0, color: Colors.grey.shade300)
        //       )
        //   ),
        //   child: DetailWidget(
        //     navigateCallback: ()async{
        //       await WrapNavigation.instance.of(context).pushNamed(ConcertListScreen().buildPath(), arguments: {
        //         'storeId': viewModel.storeId,
        //         'storeName': viewModel.storeDetail?.name,
        //         'isOwner': viewModel.isOwner
        //       });
        //     },
        //     title: "คอนเสิร์ตที่กำลังมาถึง",
        //     detail: items.isNotEmpty ? "ดูทั้งหมด" : null,
        //     widget: ConcertTabDetail(
        //       items: items,
        //       withOut: 'ไม่มีคอนเสิร์ตที่กำลังมาถึง',
        //     ),
        //   ),
        // )
        //     : SizedBox(),
        //
        // // ConcertMainWidget(
        // //   items: items,
        // // ),
        //
        // //TODO: Promotion widget
        // viewModel.promotionData?.record?.length != 0
        //     ? Container(
        //   decoration: BoxDecoration(
        //     border: Border(
        //       top: BorderSide(width: 3.0, color: Colors.grey.shade300)
        //     )
        //   ),
        //     child: PromotionWidget(viewModel: viewModel),
        // )
        //     : SizedBox(),
        //
        // //TODO: Product widget
        // ProductMainWidget(items: []),
        //
        // //TODO: Review widget
        // Container(
        //   decoration: BoxDecoration(
        //       border: Border(top: BorderSide(width: 3.0, color: Colors.grey.shade300))),
        //   child: ReviewWidget(viewModel, reloadReview, reloadReviewByIndex),
        // ),
        //
        // //TODO: CheckIn widget
        // CheckInWidget(
        //   storeDetail: viewModel.storeDetail,
        //   checkInUserData: viewModel.checkInUserData,
        // ),
        //
        // //TODO: Gallery widget
        // GalleryWidget(
        //   galleryData: viewModel.galleryData,
        //   storeId: viewModel.storeId,
        //   storeName: viewModel.storeDetail?.name ?? '',
        // ),
      ],
    );
  }
}
