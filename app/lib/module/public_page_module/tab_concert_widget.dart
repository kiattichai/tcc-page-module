import 'package:app/model/concert.dart';
import 'package:app/module/create_concert_module/create_concert_name.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:app/module/public_page_module/widget/concert/concert_tab_detail_widget.dart';
import 'package:app/module/public_page_module/widget/detail_widget.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class TabConcertWidget extends StatelessWidget {
  HomeViewModel viewModel;

  TabConcertWidget(this.viewModel);

  @override
  Widget build(BuildContext context) {
    List<Concert> items = viewModel.getConcertData?.record
            ?.map((e) => Concert(
                  e.id!,
                  e.images!.first.url!,
                  e.showTime!.textShortDate!,
                  e.name!,
                ))
            .toList() ??
        [];
    return Column(
      children: [
        viewModel.isOwner
            ? Container(
                child: Column(
                  children: [
                    Container(
                      width: Screen.width,
                      margin: EdgeInsets.fromLTRB(22.0, 23.5, 22.0, 16.0),
                      child: Text.rich(
                        TextSpan(children: <TextSpan>[
                          TextSpan(
                            text:
                                '    หากคอนเสิร์ตของคุณ เปิดให้มีการซื้อผ่านระบบออนไลน์   อาจมีค่าธรรมเนียม และจำเป็นต้องส่งเอกสาร ',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff08080a),
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          TextSpan(
                            text: 'อ่านรายละเอียด',
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff08080a),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                print('อ่านรายละเอียด');
                              },
                          ),
                        ]),
                      ),
                    ),
                    InkWell(
                      //onTap: () => TixNavigate.instance.navigateTo(CreateConcertName(), data: {'storeId': viewModel.storeId}),
                      onTap: () => WrapNavigation.instance.pushNamed(context, CreateConcertName(),
                          arguments: {'storeId': viewModel.storeId}),
                      child: Container(
                        width: 343,
                        height: 42,
                        margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 15.5),
                        decoration: new BoxDecoration(
                            color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
                        child: Center(
                          child: new Text("สร้างคอนเสิร์ต",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet',
                                color: Color(0xffffffff),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : SizedBox(),
        items != null
            ? DetailWidget(
                navigateCallback: () => navigateToConcertList(context),
                title: "คอนเสิร์ตที่กำลังมาถึง",
                detail: items.isNotEmpty ? "ดูทั้งหมด" : null,
                widget: ConcertTabDetail(
                  items: items,
                  withOut: 'ไม่มีคอนเสิร์ตที่กำลังมาถึง',
                ),
              )
            : SizedBox(),
      ],
    );
  }

  void navigateToConcertList(BuildContext context) async {
    await WrapNavigation.instance.pushNamed(context, ConcertListScreen(), arguments: {
      'storeId': viewModel.storeId,
      'storeName': viewModel.storeDetail?.name,
      'isOwner': viewModel.isOwner
    });
    // TixNavigate.instance.navigateTo(ConcertListScreen(), data: {
    //   'storeId': viewModel.storeId,
    //   'storeName': viewModel.storeDetail?.name,
    //   'isOwner': viewModel.isOwner
    // });
  }
}
