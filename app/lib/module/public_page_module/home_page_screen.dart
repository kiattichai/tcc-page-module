import 'package:app/model/create_review.dart';
import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/public_page_module/tab_menu_food.dart';
import 'package:app/module/public_page_module/widget/botton_navigator_widget.dart';

import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/banner_widget.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/public_page_module/tab_concert_widget.dart';
import 'package:app/module/public_page_module/tab_main_widget.dart';
import 'package:app/module/public_page_module/tab_promotion_widget.dart';
import 'package:app/module/public_page_module/tab_review_widget.dart';
import 'package:app/module/public_page_module/widget/home/dialog_store_verify.dart';
import 'package:app/module/public_page_module/widget/home/tab_widget.dart';
import 'package:app/module/public_page_module/widget/home/title_widget.dart';
import 'package:app/module/public_page_module/widget/review/create_review_sucess_dialog.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/expand_page_view.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'home_page_viewmodel.dart';
import 'widget/checkIn/dialog_result.dart';

class MyHomePage extends StatefulWidget with TixRoute {
  final String? storeId;
  final bool? isOwner;
  int? tabIndex;

  MyHomePage({Key? key, this.storeId, this.isOwner = false, this.tabIndex})
      : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();

  @override
  String buildPath() {
    return '/home_page_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => MyHomePage(
              storeId: data['storeId'].toString(),
              isOwner: data['isOwner'],
              tabIndex: data['tabIndex'],
            ));
  }
}

class _MyHomePageState extends BaseStateProvider<MyHomePage, HomeViewModel>
    with SingleTickerProviderStateMixin {
  callbackReview(CreateReview? createReview) async {
    if (createReview != null) {
      // viewModel.forceReStatePageAfterBackToScreen();
      viewModel.createReview = createReview;
      viewModel.saveReview();
    }
  }

  @override
  void initState() {
    viewModel = HomeViewModel(widget.storeId ?? '', this,
        widget.isOwner ?? false, widget.tabIndex ?? 0);
    //viewModel = HomeViewModel('5000440', this, widget.isOwner ?? false);
    viewModel.showAlertError = showAlertError;
    viewModel.showSaveReviewSuccess = showSaveReviewSuccess;
    viewModel.showSaveGallerySuccess = showSaveGallerySuccess;
    viewModel.showSaveCheckInsSuccess = showSaveCheckInsSuccess;
    viewModel.showLoading = showLoading;
    viewModel.hideLoading = hideLoading;
    super.initState();
    // WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
    //   viewModel.setLoading(true);
    // });
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    Widget publish = Container(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 12),
        decoration: new BoxDecoration(color: Color(0xffffffff)),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  width: 35,
                  height: 35,
                  image: AssetImage('assets/group_4.png'),
                ),
                SizedBox(
                  width: 12,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("ทำให้ผู้คนเห็นเพจของคุณ",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff08080a),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                    RichText(
                        text: new TextSpan(children: [
                      new TextSpan(
                          text: "The plant Cafe & Restaurant",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.normal,
                          )),
                      new TextSpan(
                          text:
                              "ยังไม่สามารถใช้ ได้ในขณะนี้ เผยแพร่เพจเพื่อให้ผู้คนเห็นได้",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff6d6d6d),
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.normal,
                          )),
                    ]))
                  ],
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 9),
              width: double.infinity,
              height: 42,
              child: ElevatedButton(
                  child: Text(
                    "เผยแพร่เพจ",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xffda3534)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(21),
                      ))),
                  onPressed: () => viewModel.publishPage()),
            )
          ],
        ));

    return WillPopScope(
        child: BaseWidget<HomeViewModel>(
            builder: (context, model, child) {
              return ProgressHUD(
                  opacity: 1.0,
                  color: Colors.white,
                  progressIndicator: CircleLoading(),
                  inAsyncCall: viewModel.loading,
                  child: DefaultTabController(
                    length: viewModel.countTabSize,
                    child: Scaffold(
                      appBar: AppBarWidget(
                        onBack: () {
                          if (WrapNavigation.instance.canPop(context)) {
                            WrapNavigation.instance.pop(context);
                          } else {
                            SystemNavigator.pop();
                          }
                          appState.channel.invokeMethod('pop');
                        },
                        name: viewModel.storeDetail?.name,
                        actionButton: viewModel.isOwner
                            ? Padding(
                                padding: new EdgeInsets.only(right: 16),
                                child: InkWell(
                                  child: Image(
                                    image: AssetImage("assets/setting.png"),
                                    width: 18,
                                    height: 18,
                                    color: Colors.black,
                                  ),
                                  onTap: () =>
                                      viewModel.navigateToEditProfile(context),
                                ))
                            : Padding(
                                padding: new EdgeInsets.only(right: 16),
                                // child: ImageIcon(
                                //   AssetImage("assets/share_forward_fill.png"),
                                //   color: Colors.black,
                                // ),
                              ),
                      ),
                      body: viewModel.noInternet
                          ? Center(
                              child: Container(
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                    Icon(
                                      Icons.info,
                                      color: Colors.black,
                                      size: 20,
                                    ),
                                    Text("ไม่สามารถเชื่อต่ออินเทอร์เน็ตได้",
                                        style: TextStyle(fontSize: 20))
                                  ])),
                            )
                          : SingleChildScrollView(
                              controller: viewModel.scrollController,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  viewModel.isOwner &&
                                          !(viewModel.storeDetail?.status ??
                                              true)
                                      ? publish
                                      : SizedBox(),
                                  BannerWidget(viewModel),
                                  TitleWidget(viewModel, onTapVerify),
                                  TabBarWidget(
                                    tabController: viewModel.tabController,
                                  ),
                                  viewModel.countTabSize == 4
                                      ? ExpandablePageView(
                                          pageController:
                                              viewModel.pageController,
                                          children: [
                                            // TODO Tab หน้าหลัก
                                            Container(
                                              child: TabMainWidget(
                                                  viewModel,
                                                  viewModel.getReview,
                                                  viewModel.getReviewByIndex),
                                            ),
                                            //TODO Tab คอนเสิร์ต
                                            Container(
                                              child:
                                                  TabConcertWidget(viewModel),
                                            ),
                                            //TODO Tab โปรโมชั่น
                                            Container(
                                              child: TabPromotionWidget(
                                                viewModel: viewModel,
                                                isOwner: viewModel.isOwner,
                                              ),
                                            ),
                                            //TODO Tab สินค้า
                                            // Container(
                                            //   child: TabProductWidget(
                                            //       productList, viewModel.storeId, viewModel.isOwner),
                                            // ),
                                            //TODO Tab รีวิว
                                            Container(
                                              child: TabReviewWidget(
                                                viewModel,
                                                (force) => viewModel.getReview(
                                                    forceTabVisible: force),
                                                viewModel.getReviewByIndex,
                                                callbackReview,
                                                showSaveGallerySuccess,
                                              ),
                                            ),
                                          ],
                                        )
                                      : viewModel.countTabSize == 5
                                          ? ExpandablePageView(
                                              pageController:
                                                  viewModel.pageController,
                                              children: [
                                                // TODO Tab หน้าหลัก
                                                Container(
                                                  child: TabMainWidget(
                                                      viewModel,
                                                      viewModel.getReview,
                                                      viewModel
                                                          .getReviewByIndex),
                                                ),
                                                //TODO Tab เมนูอาหาร
                                                Container(
                                                  child: TabMenuFood(
                                                      viewModel: viewModel),
                                                ),
                                                //TODO Tab คอนเสิร์ต
                                                Container(
                                                  child: TabConcertWidget(
                                                      viewModel),
                                                ),
                                                //TODO Tab โปรโมชั่น
                                                Container(
                                                  child: TabPromotionWidget(
                                                    viewModel: viewModel,
                                                    isOwner: viewModel.isOwner,
                                                  ),
                                                ),
                                                //TODO Tab สินค้า
                                                //TODO Tab รีวิว
                                                Container(
                                                  child: TabReviewWidget(
                                                    viewModel,
                                                    (force) =>
                                                        viewModel.getReview(
                                                            forceTabVisible:
                                                                force),
                                                    viewModel.getReviewByIndex,
                                                    callbackReview,
                                                    showSaveGallerySuccess,
                                                  ),
                                                ),
                                              ],
                                            )
                                          : ExpandablePageView(
                                              pageController:
                                                  viewModel.pageController,
                                              children: [
                                                // TODO Tab หน้าหลัก
                                                Container(
                                                  child: TabMainWidget(
                                                      viewModel,
                                                      viewModel.getReview,
                                                      viewModel
                                                          .getReviewByIndex),
                                                ),
                                                //TODO Tab คอนเสิร์ต
                                                Container(
                                                  child: TabConcertWidget(
                                                      viewModel),
                                                ),
                                                //TODO Tab โปรโมชั่น
                                                // Container(
                                                //   child: TabPromotionWidget(
                                                //     viewModel: viewModel,
                                                //     isOwner: viewModel.isOwner,
                                                //   ),
                                                // ),
                                                //TODO Tab สินค้า
                                                // Container(
                                                //   child: TabProductWidget(
                                                //       productList, viewModel.storeId, viewModel.isOwner),
                                                // ),
                                                //TODO Tab รีวิว
                                                Container(
                                                  child: TabReviewWidget(
                                                    viewModel,
                                                    (force) =>
                                                        viewModel.getReview(
                                                            forceTabVisible:
                                                                force),
                                                    viewModel.getReviewByIndex,
                                                    callbackReview,
                                                    showSaveGallerySuccess,
                                                  ),
                                                ),
                                              ],
                                            ),
                                ],
                              ),
                            ),
                      bottomNavigationBar: viewModel.storeDetail != null &&
                              !viewModel.isOwner &&
                              viewModel.tabController?.index ==
                                  (viewModel.countTabSize - 1)
                          ? BottonNavigatorWidget(
                              viewModel, callbackReview, showSaveGallerySuccess)
                          : SizedBox(),
                    ),
                  ));
            },
            model: viewModel),
        onWillPop: () async {
          SystemNavigator.pop();
          return false;
        });
  }

  void showAlertError(BaseError baseError) {
    if (showing == true) return;

    showing = true;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void showSaveReviewSuccess() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SaveSuccessDialog(
              'shape.png', 'เรียบร้อยแล้ว', 'ขอบคุณสำหรับคะแนนรีวิว');
        });
  }

  void showSaveGallerySuccess() {
    viewModel.getGallery();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SaveSuccessDialog('check_circle.png', 'ขอบคุณค่ะ',
              'เพิ่มภาพของ ${viewModel.storeDetail?.name} เรียบร้อยแล้ว');
        });
  }

  void showSaveCheckInsSuccess() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogResult(viewModel.storeDetail?.images?.logo?.url,
              viewModel.storeDetail?.name);
        });
  }

  void onTapVerify() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogStoreVerify(
            storeId: viewModel.storeDetail?.id.toString(),
          );
        });
  }
}
