import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/component/menu_full_screen.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_edit_dialog.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_slide.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';

class TabMenuFood extends StatefulWidget {
  final HomeViewModel viewModel;
  const TabMenuFood({Key? key, required this.viewModel}) : super(key: key);

  @override
  _TabMenuFoodState createState() => _TabMenuFoodState();
}

class _TabMenuFoodState extends State<TabMenuFood> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.viewModel.galleryMenuDataList.isEmpty) {
      return Container(
        padding: const EdgeInsets.only(top: 34, bottom: 34),
        child: Center(
          child: Text('ไม่พบข้อมูล',
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff6d6d6d),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.5,
              )),
        ),
      );
    }
    return GridView.builder(
        itemCount: widget.viewModel.galleryMenuDataList.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 2.0,
          mainAxisSpacing: 2.0,
          childAspectRatio: 187 / 187,
        ),
        itemBuilder: (BuildContext context, int index) {
          var item = widget.viewModel.galleryMenuDataList[index];
          return GestureDetector(
            onTap: () async {
              Map<String, dynamic> data = {
                "imgList": widget.viewModel.galleryMenuDataList,
                "index": index,
                "total": widget.viewModel.galleryData?.pagination?.total,
                "imagePage": widget.viewModel.menuPage,
                "storeId": widget.viewModel.storeId,
              };
              final result = await WrapNavigation.instance
                  .pushNamed(context, MenuFullScreen(), arguments: data);
            },
            child: Container(
              height: 187.0,
              width: 187.0,
              child: Image.network(
                '${item.url}',
                fit: BoxFit.cover,
              ),
            ),
          );
        });
  }
}
