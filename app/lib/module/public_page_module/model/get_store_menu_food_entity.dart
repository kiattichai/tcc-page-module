import 'package:app/generated/json/get_store_menu_food_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetStoreMenuFoodEntity {
  GetStoreMenuFoodEntity();

  factory GetStoreMenuFoodEntity.fromJson(Map<String, dynamic> json) =>
      $GetStoreMenuFoodEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetStoreMenuFoodEntityToJson(this);

  GetStoreMenuFoodData? data;
  GetStoreMenuFoodBench? bench;
}

@JsonSerializable()
class GetStoreMenuFoodData {
  GetStoreMenuFoodData();

  factory GetStoreMenuFoodData.fromJson(Map<String, dynamic> json) =>
      $GetStoreMenuFoodDataFromJson(json);

  Map<String, dynamic> toJson() => $GetStoreMenuFoodDataToJson(this);

  GetStoreMenuFoodDataPagination? pagination;
  List<GetStoreMenuFoodDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetStoreMenuFoodDataPagination {
  GetStoreMenuFoodDataPagination();

  factory GetStoreMenuFoodDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetStoreMenuFoodDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetStoreMenuFoodDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetStoreMenuFoodDataRecord {
  GetStoreMenuFoodDataRecord();

  factory GetStoreMenuFoodDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetStoreMenuFoodDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetStoreMenuFoodDataRecordToJson(this);

  String? id;
  int? position;
  String? tag;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetStoreMenuFoodBench {
  GetStoreMenuFoodBench();

  factory GetStoreMenuFoodBench.fromJson(Map<String, dynamic> json) =>
      $GetStoreMenuFoodBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetStoreMenuFoodBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
