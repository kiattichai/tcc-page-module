import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/create_review.dart';
import 'package:app/model/get_check_in_user_entity.dart';
import 'package:app/model/get_concert_entity.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/get_promotion_entity.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/publish_page_entity.dart';
import 'package:app/model/response_chekins_entity.dart';
import 'package:app/model/review.dart';
import 'package:app/model/save_review_entity.dart';
import 'package:app/model/save_review_image_entity.dart';
import 'package:app/model/save_review_response_entity.dart';
import 'package:app/model/uploads_banner_entity.dart';
import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:app/module/create_page_module/model/save_page_avatar_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_profile_page_menu.dart';
import 'package:app/module/public_page_module/model/get_store_menu_food_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;
import '../../../globals.dart' as globals;

class HomeViewModel extends BaseViewModel {
  GetOrganizerStoreDetailData? storeDetail;
  GetGalleryData? galleryData;
  GetCheckInUserData? checkInUserData;
  GetReviewData? reviewData;
  GetPromotionData? promotionData;
  CreateReview? createReview;
  GetConcertData? getConcertData;
  ResponseChekinsData? responseCheckIns;
  String storeId; //"5000090"
  bool isOwner;
  bool noInternet = false;
  int _imageIndex = 1;
  TabController? tabController;
  late PageController pageController;
  File? profileImageFile;
  File? bannerImageFile;
  int tabIndex;
  String distance = '';
  int countTabSize = 3;
  late TickerProvider homeTicketProvider;

  int menuLimit = 10;
  int menuPage = 1;
  bool isMenuComplete = false;
  int _imageMenuIndex = 1;

  ScrollController scrollController = ScrollController();
  GetStoreMenuFoodData? galleryMenuData;
  List<GetStoreMenuFoodDataRecord> galleryMenuDataList = [];

  HomeViewModel(
      this.storeId, this.homeTicketProvider, this.isOwner, this.tabIndex) {
    setLoading(true);

    pageController = PageController(initialPage: tabIndex);
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0 &&
          !isMenuComplete &&
          tabIndex == 1 &&
          tabController?.length == 5) {
        getImage();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    if (isOwner) {
      getOrganizerStoreDetail();
    } else {
      getStoreDetail();
    }
  }

  @override
  void dispose() {
    super.dispose();
    tabController?.dispose();
    pageController.dispose();
  }

  void getLaunchScreen() {
    if (isOwner) {
      getConcert();
    } else {
      getConcertInStore();
    }
    getCheckIns();
    getGallery();
    getReview();
    getPromotion();
    getImage();
  }

  Function(BaseError)? showAlertError;
  Function()? showSaveReviewSuccess;
  Function()? showSaveGallerySuccess;
  Function()? showSaveCheckInsSuccess;

  void getOrganizerStoreDetail() {
    catchError(() async {
      setLoading(true);
      storeDetail = await di.pageRepository.getOrganizerStore(storeId);
      handleCountTab();
      getDistance();
      setLoading(false);
      getLaunchScreen();
    });
  }

  void getStoreDetail() {
    catchError(() async {
      setLoading(true);
      var result = await di.pageRepository.getStoreDetailById(storeId);
      storeDetail = GetOrganizerStoreDetailData.fromJson(result!.toJson());
      print(storeDetail?.covidVerify);
      handleCountTab();
      getDistance();
      setLoading(false);
      getLaunchScreen();
    });
  }

  void handleCountTab() {
    if (storeDetail?.section?.text == 'nightclub') {
      countTabSize = 5;
    } else {
      countTabSize = 3;
    }
    tabController = TabController(
        vsync: homeTicketProvider,
        length: countTabSize,
        initialIndex: tabIndex);
    pageController = PageController(initialPage: tabIndex);
    tabController?.addListener(() {
      final _newPage = tabController?.index;
      if (_newPage == null) return;
      tabIndex = _newPage;
      pageController.jumpToPage(_newPage);
    });

    pageController.addListener(() {
      final _newPage = pageController.page?.round();
      if (_newPage != null) {
        tabController?.index = _newPage;
        notifyListeners();
      }
    });
  }

  void imageIndex(int index) {
    _imageIndex = index + 1;
    notifyListeners();
  }

  getImageIndex() {
    return _imageIndex;
  }

  String? genreList() {
    try {
      String attributes = '';
      var genre = storeDetail?.attributes?.firstWhere((element) {
        return element.code == "genre";
      });

      var genreList = genre?.items?.map((e) {
        return e.name;
      }).toList();
      attributes = genreList!.join(", ");
      return attributes;
    } catch (e) {
      return null;
    }
  }

  void getCheckIns() {
    catchError(() async {
      checkInUserData = await di.pageRepository.getStoreCheckInById(storeId, 8);
      notifyListeners();
    });
  }

  void getGallery() {
    catchError(() async {
      galleryData = await di.pageRepository.getStoreGallery(storeId, 3, 1);
      notifyListeners();
    });
  }

  void getGalleryAndReState() {
    catchError(() async {
      galleryData = await di.pageRepository.getStoreGallery(storeId, 3, 1);
      notifyListeners();
      reStatePageAfterBackToScreen();
    });
  }

  void getReview({bool forceTabVisible = false}) {
    catchError(() async {
      reviewData = await di.pageRepository.getStoreReview(storeId, 3);
      notifyListeners();
      if (forceTabVisible) {
        reStatePageAfterBackToScreen();
      }
    });
  }

  void getReviewByIndex(int index) {
    catchError(() async {
      var review = reviewData!.record![index];
      var result = await di.reviewRepository
          .getStoreReviewById(int.parse(storeId), review.id ?? 0);
      if (result != null) {
        reviewData!.record![index] = result;
        notifyListeners();
      }
    });
  }

  void getPromotion() {
    catchError(() async {
      promotionData = await di.pageRepository.getStorePromotion(storeId, 4);
      notifyListeners();
    });
  }

  List<Review>? getReviewList() {
    try {
      return reviewData?.record
          ?.map((e) => Review(
              e.user?.image?.resizeUrl ?? '',
              '${e.user?.firstName} ${e.user?.lastName}',
              e.rating ?? 0,
              DateFormat('dd-MMM-yyyy – HH:mm น.', 'th').format(
                  DateFormat('dd-MM-yyyy HH:mm')
                      .parse(e.createdAt?.value ?? '')),
              e.description ?? ''))
          .toList();
    } catch (e) {
      return null;
    }
  }

  void saveReview() {
    catchError(() async {
      showLoading();
      SaveReviewResponseData? saveReviewResponseData =
          await di.reviewRepository.saveReview(SaveReviewEntity()
            ..description = createReview!.description
            ..rating = createReview!.rating
            ..storeId = storeDetail!.id);
      if (saveReviewResponseData != null) {
        for (File file in (createReview?.images ?? [])) {
          final bytes = await file.readAsBytes();
          final b64 = base64.encode(bytes);
          await di.reviewRepository.saveReviewImage(
              saveReviewResponseData.id ?? 0,
              SaveReviewImageEntity()
                ..data = b64
                ..name = p.basename(file.path));
        }
      }
      await Future.delayed(Duration(milliseconds: 300), () {});
      var result = await di.pageRepository.getStoreDetailById(storeId);
      storeDetail = GetOrganizerStoreDetailData.fromJson(result!.toJson());
      reviewData = await di.pageRepository.getStoreReview(storeId, 3);
      getGallery();
      hideLoading();
      reStatePageAfterBackToScreen();
      showSaveReviewSuccess!();
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    print('onError');
    print(error);
    setLoading(false);
    if (error is BaseError) {
      if (error.code == 0) {
        setNoInternet(true);
      }
      hideLoading();
      showAlertError!(error);
    }
  }

  void setNoInternet(bool noInternet) {
    this.noInternet = noInternet;
    notifyListeners();
  }

  void pickBannerImage() {
    catchError(() async {
      final pickedImage =
          await ImagePicker().getImage(source: ImageSource.gallery);
      bannerImageFile = pickedImage != null ? File(pickedImage.path) : null;
      if (bannerImageFile != null) {
        cropImage(FileType.bannerImageFile);
      }
    });
  }

  void pickProfileImage() {
    catchError(() async {
      final pickedImage =
          await ImagePicker().getImage(source: ImageSource.gallery);
      profileImageFile = pickedImage != null ? File(pickedImage.path) : null;
      if (profileImageFile != null) {
        cropImage(FileType.profileImageFile);
      }
    });
  }

  void cropImage(FileType type) {
    catchError(() async {
      File? croppedFile = await ImageCropper.cropImage(
          sourcePath: FileType.bannerImageFile == type
              ? bannerImageFile!.path
              : profileImageFile!.path,
          aspectRatioPresets: [],
          aspectRatio: type == FileType.profileImageFile
              ? CropAspectRatio(ratioX: 1, ratioY: 1)
              : CropAspectRatio(ratioX: 16, ratioY: 9),
          androidUiSettings: AndroidUiSettings(
              hideBottomControls: true,
              toolbarTitle: 'ครอบตัด',
              toolbarColor: Colors.white,
              toolbarWidgetColor: Colors.black,
              lockAspectRatio: true),
          iosUiSettings: IOSUiSettings(
              title: 'ครอบตัด',
              aspectRatioLockEnabled: true,
              aspectRatioPickerButtonHidden: true));
      if (croppedFile != null) {
        if (FileType.bannerImageFile == type) {
          bannerImageFile = croppedFile;
          uploadBanner();
        }
        if (FileType.profileImageFile == type) {
          profileImageFile = croppedFile;
          updateAvatar();
        }
      }
    });
  }

  void uploadBanner() {
    catchError(() async {
      showLoading();
      if (bannerImageFile != null) {
        final bytes = await bannerImageFile!.readAsBytes();
        final b64 = base64.encode(bytes);
        UploadsBannerEntity uploadsBannerEntity = UploadsBannerEntity()
          ..storeId = storeId
          ..files = [
            UploadsBannerFiles()
              ..position = 0
              ..name = p.basename(bannerImageFile!.path)
              ..data = b64
          ];
        await di.pageRepository.uploadBanner(uploadsBannerEntity);
        getOrganizerStoreDetail();
      }
      hideLoading();
    });
  }

  void updateAvatar() {
    catchError(() async {
      setLoading(true);
      if (profileImageFile != null) {
        final bytes = await profileImageFile!.readAsBytes();
        final b64 = base64.encode(bytes);
        SavePageAvatarEntity savePageAvatar = SavePageAvatarEntity()
          ..fileName = p.basename(profileImageFile!.path)
          ..fileData = b64;
        await di.pageRepository
            .updateAvatarPage(storeDetail!.page!.id!, savePageAvatar);
        getOrganizerStoreDetail();
      }
      setLoading(false);
    });
  }

  publishPage() {
    catchError(() async {
      setLoading(true);
      PublishPageEntity publishPageEntity = PublishPageEntity()
        ..status = true
        ..storeId = storeId;
      final result = await di.pageRepository.savePublish(publishPageEntity);
      if (result != null) {
        getOrganizerStoreDetail();
      }
      setLoading(false);
    });
  }

  navigateToEditProfile(BuildContext context) {
    // TixNavigate.instance.navigateTo(EditProfilePageMenu(), data: {'storeId': storeId, 'status': storeDetail?.status}).then((value) {
    //   getOrganizerStoreDetail();
    // });
    WrapNavigation.instance.pushNamed(context, EditProfilePageMenu(),
        arguments: {
          'storeId': storeId,
          'status': storeDetail?.status
        }).then((value) {
      getOrganizerStoreDetail();
    });
  }

  void checkIns(String? description, BuildContext context) {
    print(description);
    WrapNavigation.instance.pop(context);
    Map params = {"store_id": "${storeId}", "description": "${description}"};
    catchError(() async {
      showLoading();
      responseCheckIns = await di.checkInRepository.checkIns(params);
      if (responseCheckIns != null) {
        Future.delayed(Duration(milliseconds: 100), () async {
          checkInUserData =
              await di.pageRepository.getStoreCheckInById(storeId, 8);
          hideLoading();
          showSaveCheckInsSuccess!();
          reStatePageAfterBackToScreen();
        });
      }
    });
  }

  void getConcert() {
    catchError(() async {
      getConcertData = await di.pageRepository.getConcert(storeId, 1, 3);
      notifyListeners();
    });
  }

  void getConcertInStore() {
    catchError(() async {
      getConcertData = await di.pageRepository.getConcertInStore(storeId, 1, 3);
      notifyListeners();
    });
  }

  bool isLoggedIn() {
    return globals.userAuthen?.data?.accessToken != null;
  }

  void getDistance() async {
    if (storeDetail == null) return;
    final lat = storeDetail?.venue?.lat;
    final long = storeDetail?.venue?.long;

    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();
    if (isLocationServiceEnabled) {
      final lastKnow = await Geolocator.getLastKnownPosition();
      double distanceInMeters = Geolocator.distanceBetween(
          lastKnow?.latitude ?? 0.0,
          lastKnow?.longitude ?? 0.0,
          lat ?? 0.0,
          long ?? 0.0);
      distance = NumberFormat('#,###.0').format((distanceInMeters / 1000));
    }
  }

  void reStatePageAfterBackToScreen() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      pageController.jumpToPage(countTabSize - 1);
      notifyListeners();
    });
  }

  void forceReStatePageAfterBackToScreen() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      await Future.delayed(Duration(seconds: 1));
      pageController.jumpToPage(tabIndex);
      notifyListeners();
    });
  }

  void getImage() async {
    catchError(() async {
      if (menuPage == 1) {
        galleryMenuDataList.clear();
      }
      galleryMenuData = await di.pageRepository
          .getStoreMenuFood(storeId, menuPage, menuLimit);
      if (galleryMenuData == null || galleryMenuData?.record == null) return;

      if (galleryMenuData != null &&
          (galleryMenuData?.record?.length ?? 0) > 0 &&
          (galleryMenuData?.pagination?.currentPage ?? 0) <=
              (galleryMenuData?.pagination?.lastPage ?? 0)) {
        galleryMenuDataList.addAll(galleryMenuData?.record ?? []);
        menuPage++;
        isMenuComplete = false;
      } else {
        isMenuComplete = true;
      }
      notifyListeners();
    });
  }

  void clearMenuPage() {
    menuPage = 1;
    galleryMenuDataList.clear();
    isMenuComplete = false;
  }
}
