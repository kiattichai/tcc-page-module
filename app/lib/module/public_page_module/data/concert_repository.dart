import 'package:app/model/generate_code_entity.dart';
import 'package:app/model/get_calculate_ticket_entity.dart';
import 'package:app/model/get_code_entity.dart';
import 'package:app/model/get_concert_by_id_entity.dart';
import 'package:app/model/get_concert_tickets_entity.dart';
import 'package:app/model/get_ticket_by_id_entity.dart';
import 'package:app/model/get_ticket_scan_entity.dart';
import 'package:app/model/get_ticket_select_entity.dart';
import 'package:app/model/get_ticket_setting_entity.dart';
import 'package:app/model/response_result_entity.dart';
import 'package:app/model/save_concert_ticket_entity.dart';
import 'package:app/model/save_ticket_scan_entity.dart';
import 'package:app/model/save_ticket_setting_entity.dart';
import 'package:app/model/get_discount_entity.dart';
import 'package:app/model/get_ticket_entity.dart';
import 'package:app/model/response_link_id_entity.dart';
import 'package:app/model/response_save_discount_entity.dart';
import 'package:app/model/ticket_payer.dart';
import 'package:app/model/update_concert_status_entity.dart';
import 'package:app/model/update_ticket_status_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_response_entity.dart';
import 'package:app/module/public_page_module/data/concert_api.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:injectable/injectable.dart';

@singleton
class ConcertRepository {
  final ConcertApi api;


  ConcertRepository(this.api);

  Future<GetConcertByIdData?> getConcertById(int concertId) async {
    final result = await api.getConcertById(concertId);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateConcertById(int concertId,
      SaveConcertEntity saveConcertEntity) async {
    final result = await api.updateConcertById(concertId, saveConcertEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> deleteConcertById(int concertId,
      String storeId) async {
    final result = await api.deleteConcertById(concertId, storeId);
    return result.data;
  }

  Future<GetTicketSettingData?> getConcertSetting(int concertId,
      String storeId) async {
    final result = await api.getConcertSetting(concertId, storeId);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateConcertTicketSetting(int concertId,
      SaveTicketSettingEntity saveTicketSettingEntity) async {
    final result = await api.updateConcertTicketSetting(
        concertId, saveTicketSettingEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> saveConcertTicket(int concertId,
      SaveConcertTicketEntity saveConcertTicketEntity) async {
    final result = await api.saveConcertTicket(
        concertId, saveConcertTicketEntity);
    return result.data;
  }

  Future<GetConcertTicketsData?> getConcertTicket(int concertId) async {
    final result = await api.getConcertTicket(concertId);
    return result.data;
  }

  Future<GetTicketData?> getTicket(int concertId) async {
    final result = await api.getTicket(concertId);
    return result.data;
  }

  Future<ResponseSaveDiscountData?> saveDiscount(Map params) async {
    final result = await api.saveDiscount(params);
    return result.data;
  }

  Future<ResponseLinkIdData?> linkId(int codeId, List item) async {
    final result = await api.linkId(codeId, item);
    return result.data;
  }

  Future<GetDiscountData?> getDiscount(String storeId) async {
    final result = await api.getDiscount(storeId);
    return result.data;
  }

  Future<GetTicketByIdData?> getConcertTicketById(int concertId, int ticketId,
      String storeId) async {
    final result = await api.getConcertTicketById(concertId, ticketId, storeId);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateConcertTicket(int concertId,
      int ticketId, SaveConcertTicketEntity saveConcertTicketEntity) async {
    final result = await api.updateConcertTicket(
        concertId, ticketId, saveConcertTicketEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> deleteConcertTicket(int concertId, int ticketId, String storeId) async {
    final result = await api.deleteConcertTicket(concertId, ticketId, storeId);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateConcertTicketStatus(int concertId,int ticketId,UpdateTicketStatusEntity updateTicketStatusEntity) async {
    final result = await api.updateConcertTicketStatus(
        concertId, ticketId, updateTicketStatusEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateConcertStatus(int concertId,UpdateConcertStatusEntity updateConcertStatusEntity) async {
    final result = await api.updateConcertStatus(
        concertId, updateConcertStatusEntity);
    return result.data;

  }

  Future<ResponseResultData?> setStatus(int discountId, bool status, String storeId) async {
    final result = await api.setStatus(discountId, status, storeId);
    return result.data;
  }

  Future<GetCodeData?> getCode(int discountId) async {
    final result = await api.getCode(discountId);
    return result.data;
  }

  Future<ResponseResultData?> setStatusCode(int discountId, int codeId, bool status) async {
    final result = await api.setStatusCode(discountId, codeId, status);
    return result.data;
  }

  Future<ResponseResultData?> deleteCode(int discountId, int codeId) async {
    final result = await api.deleteCode(discountId, codeId);
    return result.data;
  }

  Future<GenerateCodeData?> generateCode(int discountId, List item) async {
    final result = await api.generateCode(discountId, item);
    return result.data;
  }

  Future<ResponseResultData?> updateDiscount(int discountId, Map params) async {
    final result = await api.updateDiscount(discountId, params);
    return result.data;
  }

  Future<ResponseResultData?> deleteDiscount(int discountId, String storeId) async {
    final result = await api.deleteDiscount(discountId, storeId);
    return result.data;
  }

  Future<GetTicketSelectData?> getTicketSelected(int discountId) async {
    final result = await api.getTicketSelected(discountId);
    return result.data;
  }

  Future<GetCalculateTicketData?> getTicketPriceCalculate(
      TicketPayer serviceFee,
      TicketPayer paymentFee,
      double price,
      int amount) async {
    final result = await api.getTicketPriceCalculate(serviceFee,paymentFee,price,amount);
    return result.data;
  }

  Future<GetTicketScanData?> getTicketScan(
      int concertId,
      String storeId,
      int page,
      int limit
      ) async {
    final result = await api.getTicketScan(concertId,storeId,page,limit);
    return result.data;
  }

  Future<SaveConcertResponseData?> saveTicketScan(int concertId,SaveTicketScanEntity saveTicketScanEntity ) async{
    final result = await api.saveTicketScan(concertId,saveTicketScanEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> updateTicketScan(int concertId,int ticketScanId,SaveTicketScanEntity saveTicketScanEntity )async {
    final result = await api.updateTicketScan(concertId,ticketScanId,saveTicketScanEntity);
    return result.data;
  }

  Future<SaveConcertResponseData?> deleteTicketScan(int concertId,int ticketScanId, String storeId) async {
    final result = await api.deleteTicketScan(concertId,ticketScanId,storeId);
    return result.data;
  }
}