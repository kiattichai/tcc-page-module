import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/get_check_in_entity.dart';
import 'package:app/model/response_chekins_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class CheckInApi {
  final String pathStore = '/stores';
  final String pathUsers = '/users';
  final String pathCheckIn = '/checkins';


  final CoreApi api;

  CheckInApi(this.api);

  Future<GetCheckInEntity> getCheckInById(String storeId, int limit, int page) async {
    Response response = await api.get(pathStore + '/$storeId' + pathCheckIn,
        {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'});
    return GetCheckInEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/users/checkins
  Future<ResponseChekinsEntity> checkIns(Map params) async {
    Response response = await api.post(pathUsers+pathCheckIn, params, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return ResponseChekinsEntity.fromJson(response.data);
  }
}