import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/delete_image_review_entity.dart';
import 'package:app/model/delete_image_review_response_entity.dart';
import 'package:app/model/get_review_by_id_entity.dart';
import 'package:app/model/save_review_entity.dart';
import 'package:app/model/save_review_image_entity.dart';
import 'package:app/model/save_review_image_response_entity.dart';
import 'package:app/model/save_review_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class ReviewApi {

  final String pathReview = '/users/reviews';
  final String pathImage = '/image';
  final String pathStore = '/stores';
  final String review = '/reviews';

  final CoreApi api;

  ReviewApi(this.api);

  Future<SaveReviewResponseEntity> saveReview(SaveReviewEntity saveReview) async {
    Response response = await api.post(
        pathReview, saveReview.toJson(), BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return SaveReviewResponseEntity.fromJson(response.data);
  }

  Future<SaveReviewImageResponseEntity> saveReviewImage(int reviewId,SaveReviewImageEntity saveReviewImage) async {
    Response response = await api.post(
          '$pathReview/$reviewId$pathImage', saveReviewImage.toJson(), BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return SaveReviewImageResponseEntity.fromJson(response.data);
  }

  //Review
  Future<GetReviewByIdEntity> getStoreReviewById(int storeId, int reviewId) async {
    Response response = await api.get(pathStore + '/$storeId' + review + '/$reviewId',
        null, BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'});
    return GetReviewByIdEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/users/reviews/564/image
  Future<DeleteImageReviewResponseEntity> deleteImageReview(int reviewId,List<String> imageIds) async {
    DeleteImageReviewEntity deleteImage =  DeleteImageReviewEntity()..files =imageIds.map((e) {
      return DeleteImageReviewFiles()..id = e;
    }).toList();
    Response response = await api.delete(pathReview + '/$reviewId' + pathImage,
        deleteImage, BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return DeleteImageReviewResponseEntity.fromJson(response.data);
  }

  Future<SaveReviewResponseEntity> updateReview(int reviewId,SaveReviewEntity saveReview) async {
    Response response = await api.put(
        pathReview + '/$reviewId', saveReview.toJson(), BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return SaveReviewResponseEntity.fromJson(response.data);
  }
}