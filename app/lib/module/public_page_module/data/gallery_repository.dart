import 'package:app/model/gallery_update_response_entity.dart';
import 'package:app/model/save_gallery_entity.dart';
import 'package:app/model/save_gallery_response_entity.dart';
import 'package:app/module/public_page_module/data/gallery_api.dart';
import 'package:injectable/injectable.dart';

@singleton
class GalleryRepository {
  final GalleryApi api;

  GalleryRepository(this.api);

  Future<SaveGalleryResponseData?> saveGallery(SaveGalleryEntity saveGallery) async {
    final result = await api.saveGallery(saveGallery);
    return result.data;
  }

  Future<GalleryUpdateResponseData?> updateDescription(int record, String description) async {
    final result = await api.updateDescription(record, description);
    return result.data;
  }

  Future<GalleryUpdateResponseData?> deleteImage(int record) async {
    final result = await api.deleteImage(record);
    return result.data;
  }


}