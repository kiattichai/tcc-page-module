
import 'package:app/core/base_repository.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/model/get_district_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/invoice_response_entity.dart';
import 'package:app/module/public_page_module/data/get_address_api.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class GetAddressRepository extends BaseRepository {
  final GetAddressApi api;
  GetAddressRepository(this.api);

  Future<GetAddressData?> getProvince(int countryId) async{
    final result = await api.getProvince(countryId);
    return result.data;
  }

  Future<GetAddressData?> getCities(int provinceId) async {
    final result = await api.getCities(provinceId);
    return result.data;
  }

  Future<GetDistrictData?> getDistrict(int cityId) async {
    final result = await api.getDistrict(cityId);
    return result.data;
  }

  Future<InvoiceResponseData?> updateInvoice(Map params) async {
    final result = await api.updateInvoice(params);
    return result.data;
  }

}