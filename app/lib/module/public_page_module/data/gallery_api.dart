import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/gallery_update_response_entity.dart';
import 'package:app/model/save_gallery_entity.dart';
import 'package:app/model/save_gallery_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class GalleryApi {
  final String pathGallery = '/users/gallery';

  final CoreApi api;

  GalleryApi(this.api);

  Future<SaveGalleryResponseEntity> saveGallery(
      SaveGalleryEntity saveGallery) async {
    Response response = await api.post(pathGallery, saveGallery.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveGalleryResponseEntity.fromJson(response.data);
  }

  Future<GalleryUpdateResponseEntity> updateDescription(
      int record, String description) async {
    Response response = await api.put(
        pathGallery + "/${record}",
        {
          'description': description,
        },
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GalleryUpdateResponseEntity.fromJson(response.data);
  }

  Future<GalleryUpdateResponseEntity> deleteImage(int record) async {
    Response response = await api.delete(
        pathGallery + "/${record}", {}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GalleryUpdateResponseEntity.fromJson(response.data);
  }
}
