import 'dart:convert';

import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/Response_upload_bank_entity.dart';
import 'package:app/model/create_page_entity.dart';
import 'package:app/model/delete_banner_response_entity.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/get_bank_data_entity.dart';
import 'package:app/model/get_concert_entity.dart';
import 'package:app/model/get_document_ticket_entity.dart';
import 'package:app/model/get_check_in_user_entity.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:app/model/get_genre_attribute_entity.dart';
import 'package:app/model/get_my_page_entity.dart';
import 'package:app/model/get_organizer_store_banner_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/get_payment_bank_entity.dart';
import 'package:app/model/get_promotion_entity.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/get_store_detail_entity.dart';
import 'package:app/model/get_venue_entity.dart';
import 'package:app/model/publish_page_entity.dart';
import 'package:app/model/response_image_upload_entity.dart';
import 'package:app/model/save_banner_bulk_entity.dart';
import 'package:app/model/save_banner_bulk_response_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/model/save_edit_profile_response_entity.dart';
import 'package:app/model/save_publish_page_response_entity.dart';
import 'package:app/model/save_venue_entity.dart';
import 'package:app/model/save_venue_response_entity.dart';
import 'package:app/model/uploads_banner_entity.dart';
import 'package:app/module/create_page_module/model/create_page_response_entity.dart';
import 'package:app/module/create_page_module/model/save_avatar_response_entity.dart';
import 'package:app/module/create_page_module/model/save_page_avatar_entity.dart';
import 'package:app/module/public_page_module/model/get_store_menu_food_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class PageApi {
  final String pathStore = '/stores';
  final String checkInUser = '/checkins/users';
  final String gallery = '/gallery';
  final String review = '/reviews';
  final String pathProducts = '/products';
  final String pathMyPage = '/me/page';
  final String pathAvatar = '/avatar';
  final String pathCover = '/cover';
  final String pathOrganizer = '/v2/organizers';
  final String pathProfile = '/profile';
  final String pathUploads = '/uploads';
  final String pathAttributes = '/attributes';
  final String pathVenue = '/venues';
  final String pathAttachments = '/attachments';
  final String pathBulk = '/bulk';
  final String pathArtist = '/2/values';
  final CoreApi api;

  PageApi(this.api);

  //Get Store detail
  //https://alpha-api.theconcert.com/stores/5000090
  Future<GetStoreDetailEntity> getStoreDetailById(String storeId) async {
    Response response =
        await api.get(pathStore + '/$storeId', null, BaseErrorEntity.badRequestToModelError);
    return GetStoreDetailEntity.fromJson(response.data);
  }

  //User checkin
  //https://alpha-api.theconcert.com/stores/5000090/checkins/users?limit=8
  Future<GetCheckInUserEntity> getStoreCheckInById(String storeId, int limit, int page) async {
    Response response = await api.get(pathStore + '/$storeId' + checkInUser,
        {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError);
    return GetCheckInUserEntity.fromJson(response.data);
  }

  //Gallery
  //https://alpha-api.theconcert.com/stores/5000090/gallery?limit=6&page=0
  Future<GetGalleryEntity> getStoreGallery(String storeId, int limit, int page) async {
    Response response = await api.get(pathStore + '/$storeId' + gallery,
        {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError);
    return GetGalleryEntity.fromJson(response.data);
  }

  //Review
  //https://alpha-api.theconcert.com/stores/5000090/reviews?limit=2&page=0
  Future<GetReviewEntity> getStoreReview(String storeId, int limit, int page) async {
    Response response = await api.get(pathStore + '/$storeId' + review,
        {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError);
    return GetReviewEntity.fromJson(response.data);
  }

  //Promotion
  //https://alpha-api.theconcert.com/products?type=voucher&store_id=5000090
  Future<GetPromotionEntity> getStorePromotion(String storeId, int limit, int page) async {
    Response response = await api.get(
        pathProducts,
        {'type': 'voucher', 'store_id': storeId, 'limit': limit, 'page': page},
        BaseErrorEntity.badRequestToModelError);
    return GetPromotionEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/me/page?page=1&limit=5
  Future<GetMyPageEntity> getMyPage(int limit, int page) async {
    Response response = await api.get(
        pathMyPage, {'limit': limit, 'page': page}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetMyPageEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/profile?store_id=5000800
  Future<GetOrganizerStoreDetailEntity> getOrganizerStore(String storeId) async {
    Response response = await api.get(
        pathOrganizer + pathProfile, {'store_id': storeId}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true, headers: {'store': storeId});
    return GetOrganizerStoreDetailEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/me/page
  Future<CreatePageResponseEntity> createPage(CreatePageEntity createPageEntity) async {
    Response response = await api.post(
        pathMyPage, createPageEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return CreatePageResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/me/page/137/avatar
  Future<SaveAvatarResponseEntity> updateAvatarPage(
      int pageId, SavePageAvatarEntity savePageAvatarEntity) async {
    Response response = await api.put(pathMyPage + '/$pageId' + pathAvatar,
        savePageAvatarEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveAvatarResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/me/page/137/cover
  Future<SaveAvatarResponseEntity> updateCoverPage(
      int pageId, SavePageAvatarEntity savePageAvatarEntity) async {
    Response response = await api.put(pathMyPage + '/$pageId' + pathCover,
        savePageAvatarEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveAvatarResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/uploads
  Future<SaveAvatarResponseEntity> uploadBanner(UploadsBannerEntity uploadsBannerEntity) async {
    uploadsBannerEntity
      ..fileableType = 'stores'
      ..status = 'default'
      ..secure = false;
    Response response = await api.post(pathOrganizer + pathUploads, uploadsBannerEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveAvatarResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/profile
  Future<SavePublishPageResponseEntity> savePublish(PublishPageEntity publishPageEntity) async {
    Response response = await api.put(pathOrganizer + pathProfile, publishPageEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SavePublishPageResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/documents?store_id=5000810
  Future<GetDocumentTicketEntity> getDocument(String storeId) async {
    Response response = await api.get('/v2/organizers/documents', {'store_id': "${storeId}"},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetDocumentTicketEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/payment-banks
  Future<GetPaymentBankEntity> getPayment() async {
    Response response = await api.get(
      '/payment-banks',
      null,
      BaseErrorEntity.badRequestToModelError,
    );
    return GetPaymentBankEntity.fromJson(response.data);
  }

  Future<GetBankDataEntity> getBankData(String storeId) async {
    Response response = await api.get(
        '/v2/organizers/banks', {'store_id': storeId}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetBankDataEntity.fromJson(response.data);
  }

  Future<GetGenreAttributeEntity> getGenre() async {
    Response response = await api.get(
        pathAttributes,
        {'code': 'genre', 'show_value': true, 'status': true, 'value_status': true},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetGenreAttributeEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/profile
  Future<SaveEditProfileResponseEntity> saveEditProfile(
      SaveEditProfilePageEntity saveEditProfilePageEntity) async {
    Response response = await api.put(pathOrganizer + pathProfile,
        saveEditProfilePageEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveEditProfileResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/venues/1174?store_id=5000809
  Future<SaveVenueResponseEntity> updateVenue(
      int? venueId, int storeId, SaveVenueEntity saveVenueEntity) async {
    Response response = await api.put(pathVenue + '/$venueId' + '?store_id=$storeId',
        saveVenueEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveVenueResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/venues
  Future<SaveVenueResponseEntity> saveVenue(SaveVenueEntity saveVenueEntity) async {
    Response response = await api.post(
        pathVenue, saveVenueEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveVenueResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/stores/5000817?fields=images(banner)&is_owner=true
  Future<GetOrganizerStoreBannerEntity> getBanner(String storeId) async {
    Response response = await api.get(pathOrganizer + pathStore + '/$storeId',
        {'fields': 'images(banner)', 'is_owner': true}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetOrganizerStoreBannerEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/attachments/3a27c0de0f8ebc5887098725d48cbac20?store_id=5000811&force=true
  Future<DeleteBannerResponseEntity> deleteBanner(String storeId, String imageId) async {
    Response response = await api.delete(pathOrganizer + pathAttachments + '/$imageId',
        {'store_id': '$storeId', 'force': true}, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return DeleteBannerResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/attachments/bulk
  Future<SaveBannerBulkResponseEntity> saveBannerBulk(
      SaveBannerBulkEntity saveBannerBulkEntity) async {
    Response response = await api.post(pathOrganizer + pathAttachments + pathBulk,
        saveBannerBulkEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveBannerBulkResponseEntity.fromJson(response.data);
  }

  Future<ResponseImageUploadEntity> upLoadImage(Map params) async {
    Response response = await api.post(
        pathOrganizer + '/documents', params, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseImageUploadEntity.fromJson(response.data);
  }

  Future<ResponseUploadBankEntity> upLoadBank(Map params) async {
    Response response = await api.post(
        pathOrganizer + '/banks', params, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseUploadBankEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/attributes/2/values?status=true&fields=id%2Cname(th)&page=1
  Future<GetArtistEntity> getArtists(int page, int limit) async {
    Response response = await api.get(
        pathAttributes + pathArtist,
        {'status': true, 'fields': 'id,name,image', 'page': page, 'limit': limit},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetArtistEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/venues?store_id=5000815&status=true&fields=id,name&page=1
  Future<GetVenueEntity> getVenue(String storeId, int page, int limit) async {
    Response response = await api.get(
        pathVenue,
        {'store_id': storeId, 'status': true, 'field': 'id,name', 'page': page, 'limit': limit},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetVenueEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products?page=2&store_id=5000815&type=event&fields=id,name,show_time,status,images,publish_status,remark,has_variant,ticket_count&order=id&sort=desc&limit=5
  Future<GetConcertEntity> getConcert(String storeId, int page, int limit) async {
    //https://alpha-api.theconcert.com/products?fields=id%2Cname%2Cdescription_short%2Cvenue%2Cshow_time%2Cattributes%2Cimages%2Cstore%2Cprice%2Cremain%2Csales_status%2Csales_url%2Csoldout_status&order=show_start&show_status=on-time&sort=desc&status=true&store_id=5000912&type=event
    Response response = await api.get(
        pathOrganizer + pathProducts,
        {
          'store_id': storeId,
          'field': 'id,name,show_time,status,images,publish_status,remark,has_variant,ticket_count',
          'page': page,
          'limit': limit,
          'type': 'event',
          'sort': 'desc',
          'order': 'id'
        },
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetConcertEntity.fromJson(response.data);
  }

  Future<GetConcertEntity> getConcertInStore(String storeId, int page, int limit) async {
    //https://alpha-api.theconcert.com/products?fields=id%2Cname%2Cdescription_short%2Cvenue%2Cshow_time%2Cattributes%2Cimages%2Cstore%2Cprice%2Cremain%2Csales_status%2Csales_url%2Csoldout_status&order=show_start&show_status=on-time&sort=desc&status=true&store_id=5000912&type=event
    Response response = await api.get(
        pathProducts,
        {
          'store_id': storeId,
          'field': 'id,name,show_time,status,images,publish_status,remark,has_variant,ticket_count',
          'page': page,
          'limit': limit,
          'type': 'event',
          'sort': 'desc',
          'order': 'id'
        },
        BaseErrorEntity.badRequestToModelError);
    return GetConcertEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/stores/5000787/menu
  Future<GetStoreMenuFoodEntity> getStoreMenuFood(String storeId, int page, int limit) async {
    Response response = await api.get(pathStore + '/$storeId/menu', {'page': page, 'limit': limit},
        BaseErrorEntity.badRequestToModelError);
    return GetStoreMenuFoodEntity.fromJson(response.data);
  }
}
