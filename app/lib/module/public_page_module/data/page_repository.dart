import 'package:app/core/base_repository.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/Response_upload_bank_entity.dart';
import 'package:app/model/create_page_entity.dart';
import 'package:app/model/delete_banner_response_entity.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/get_bank_data_entity.dart';
import 'package:app/model/get_check_in_user_entity.dart';
import 'package:app/model/get_concert_entity.dart';
import 'package:app/model/get_document_ticket_entity.dart';
import 'package:app/model/get_gallery_entity.dart';
import 'package:app/model/get_genre_attribute_entity.dart';
import 'package:app/model/get_my_page_entity.dart';
import 'package:app/model/get_organizer_store_banner_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/get_payment_bank_entity.dart';
import 'package:app/model/get_promotion_entity.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/get_store_detail_entity.dart';
import 'package:app/model/get_venue_entity.dart';
import 'package:app/model/publish_page_entity.dart';
import 'package:app/model/response_image_upload_entity.dart';
import 'package:app/model/save_banner_bulk_entity.dart';
import 'package:app/model/save_banner_bulk_response_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/model/save_edit_profile_response_entity.dart';
import 'package:app/model/save_publish_page_response_entity.dart';
import 'package:app/model/save_venue_entity.dart';
import 'package:app/model/save_venue_response_entity.dart';
import 'package:app/model/uploads_banner_entity.dart';
import 'package:app/module/create_page_module/model/create_page_response_entity.dart';
import 'package:app/module/create_page_module/model/save_avatar_response_entity.dart';
import 'package:app/module/create_page_module/model/save_page_avatar_entity.dart';
import 'package:app/module/public_page_module/data/page_api.dart';
import 'package:app/module/public_page_module/model/get_store_menu_food_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class PageRepository extends BaseRepository {
  final PageApi api;

  PageRepository(this.api);

  Future<GetStoreDetailData?> getStoreDetailById(String storeId) async {
    final result = await api.getStoreDetailById(storeId);
    return result.data;
  }

  Future<GetOrganizerStoreDetailData?> getOrganizerStore(String storeId) async {
    final result = await api.getOrganizerStore(storeId);
    return result.data;
  }

  Future<GetCheckInUserData?> getStoreCheckInById(String storeId, int limit, {int page = 0}) async {
    final result = await api.getStoreCheckInById(storeId, limit, page);
    return result.data;
  }

  Future<GetGalleryData?> getStoreGallery(String storeId, int limit, int page) async {
    final result = await api.getStoreGallery(storeId, limit, page);
    return result.data;
  }

  Future<GetReviewData?> getStoreReview(String storeId, int limit, {int page = 0}) async {
    final result = await api.getStoreReview(storeId, limit, page);
    return result.data;
  }

  Future<GetPromotionData?> getStorePromotion(String storeId, int limit, {int page = 0}) async {
    final result = await api.getStorePromotion(storeId, limit, page);
    return result.data;
  }

  Future<GetMyPageEntity?> getMyPage(int limit, int page) async {
    final result = await api.getMyPage(limit, page);
    return result;
  }

  Future<CreatePageResponseData?> createPage(CreatePageEntity createPageEntity) async {
    final result = await api.createPage(createPageEntity);
    return result.data;
  }

  Future<SaveAvatarResponseData?> updateAvatarPage(
      int pageId, SavePageAvatarEntity savePageAvatarEntity) async {
    final result = await api.updateAvatarPage(pageId, savePageAvatarEntity);
    return result.data;
  }

  Future<SaveAvatarResponseData?> updateCoverPage(
      int pageId, SavePageAvatarEntity savePageAvatarEntity) async {
    final result = await api.updateCoverPage(pageId, savePageAvatarEntity);
    return result.data;
  }

  Future<SaveAvatarResponseData?> uploadBanner(UploadsBannerEntity uploadsBannerEntity) async {
    final result = await api.uploadBanner(uploadsBannerEntity);
    return result.data;
  }

  Future<SavePublishPageResponseData?> savePublish(PublishPageEntity publishPageEntity) async {
    final result = await api.savePublish(publishPageEntity);
    return result.data;
  }

  Future<GetGenreAttributeDataRecord?> getGenre() async {
    final result = await api.getGenre();
    return result.data?.record?.firstWhere((element) => element.code == 'genre');
  }

  Future<GetDocumentTicketData?> getDocument(String storeId) async {
    final result = await api.getDocument(storeId);
    return result.data;
  }

  Future<GetPaymentBankData?> getPayment() async {
    final result = await api.getPayment();
    return result.data;
  }

  Future<GetBankDataData?> getBankData(String storeId) async {
    final result = await api.getBankData(storeId);
    return result.data;
  }

  Future<GetOrganizerStoreBannerData?> getBanner(String storeId) async {
    final result = await api.getBanner(storeId);
    return result.data;
  }

  Future<DeleteBannerResponseData?> deleteBanner(String storeId, String imageId) async {
    final result = await api.deleteBanner(storeId, imageId);
    return result.data;
  }

  Future<SaveBannerBulkResponseData?> saveBannerBulk(
      SaveBannerBulkEntity saveBannerBulkEntity) async {
    final result = await api.saveBannerBulk(saveBannerBulkEntity);
    return result.data;
  }

  Future<SaveEditProfileResponseData?> saveEditProfile(
      SaveEditProfilePageEntity saveEditProfilePageEntity) async {
    final result = await api.saveEditProfile(saveEditProfilePageEntity);
    return result.data;
  }

  Future<SaveVenueResponseData?> updateVenue(
      int? venueId, int storeId, SaveVenueEntity saveVenueEntity) async {
    final result = await api.updateVenue(venueId, storeId, saveVenueEntity);
    return result.data;
  }

  Future<SaveVenueResponseData?> saveVenue(SaveVenueEntity saveVenueEntity) async {
    final result = await api.saveVenue(saveVenueEntity);
    return result.data;
  }

  Future<ResponseImageUploadData?> upLoadImage(Map params) async {
    final result = await api.upLoadImage(params);
    return result.data;
  }

  Future<ResponseUploadBankData?> upLoadBank(Map params) async {
    final result = await api.upLoadBank(params);
    return result.data;
  }

  Future<GetArtistData?> getArtists(int page, int limit) async {
    final result = await api.getArtists(page, limit);
    return result.data;
  }

  Future<GetVenueData?> getVenue(String storeId, int page, int limit) async {
    final result = await api.getVenue(storeId, page, limit);
    return result.data;
  }

  Future<GetConcertData?> getConcert(String storeId, int page, int limit) async {
    final result = await api.getConcert(storeId, page, limit);
    return result.data;
  }

  Future<GetConcertData?> getConcertInStore(String storeId, int page, int limit) async {
    final result = await api.getConcertInStore(storeId, page, limit);
    return result.data;
  }

  Future<GetStoreMenuFoodData?> getStoreMenuFood(String storeId, int page, int limit) async {
    final result = await api.getStoreMenuFood(storeId, page, limit);
    return result.data;
  }
}
