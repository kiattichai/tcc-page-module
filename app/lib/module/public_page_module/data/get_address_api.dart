import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/model/get_district_entity.dart';
import 'package:app/model/invoice_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class GetAddressApi {
  final CoreApi api;
  GetAddressApi(this.api);
  
  Future<GetAddressEntity> getProvince(int countryId) async {
    Response response = await api.get('/provinces', {"country_id":countryId}, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return GetAddressEntity.fromJson(response.data);
  }
  
  Future<GetAddressEntity> getCities(int provinceId) async {
    Response response = await api.get('/cities', {"province_id": provinceId}, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return GetAddressEntity.fromJson(response.data);
  }

  Future<GetDistrictEntity> getDistrict(int citiesId) async {
    Response response = await api.get('/districts', {"city_id": citiesId}, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return GetDistrictEntity.fromJson(response.data);
  }

  Future<InvoiceResponseEntity> updateInvoice(Map params) async {
    Response response = await api.put('/v2/organizers/profile', params, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return InvoiceResponseEntity.fromJson(response.data);
  }


}