import 'package:app/model/get_check_in_entity.dart';
import 'package:app/model/response_chekins_entity.dart';
import 'package:app/module/public_page_module/data/check_in_api.dart';
import 'package:injectable/injectable.dart';

@singleton
class CheckInRepository {
  final CheckInApi api;

  CheckInRepository(this.api);

  Future<GetCheckInData?> getStoreCheckInById(String storeId, int limit, {int page = 0}) async {
    final result = await api.getCheckInById(storeId, limit, page);
    return result.data;
  }

  Future<ResponseChekinsData?> checkIns(Map params) async {
    final result = await api.checkIns(params);
    return result.data;
  }
}