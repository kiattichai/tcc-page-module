import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/generate_code_entity.dart';
import 'package:app/model/get_calculate_ticket_entity.dart';
import 'package:app/model/get_code_entity.dart';
import 'package:app/model/get_concert_by_id_entity.dart';
import 'package:app/model/get_concert_tickets_entity.dart';
import 'package:app/model/get_ticket_scan_entity.dart';
import 'package:app/model/get_ticket_select_entity.dart';
import 'package:app/model/get_ticket_by_id_entity.dart';
import 'package:app/model/get_ticket_setting_entity.dart';
import 'package:app/model/response_result_entity.dart';
import 'package:app/model/save_concert_ticket_entity.dart';
import 'package:app/model/save_ticket_scan_entity.dart';
import 'package:app/model/save_ticket_setting_entity.dart';
import 'package:app/model/get_discount_entity.dart';
import 'package:app/model/get_ticket_entity.dart';
import 'package:app/model/response_link_id_entity.dart';
import 'package:app/model/response_save_discount_entity.dart';
import 'package:app/model/ticket_payer.dart';
import 'package:app/model/update_concert_status_entity.dart';
import 'package:app/model/update_ticket_status_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@singleton
class ConcertApi {
  final String pathOrganizer = '/v2/organizers';
  final String pathProducts = '/products';
  final String pathPromotion = '/promotions';
  final String pathSettings = '/settings';
  final String pathVariants = '/variants';
  final String pathCode = '/codes';
  final String pathTicketPriceCalculate = '/ticket-price-calculator';
  final String pathCheckInToken = '/checkin-tokens';

  final CoreApi api;

  ConcertApi(this.api);

  Future<GetConcertByIdEntity> getConcertById(int concertId) async {
    Response response = await api.get(
        pathOrganizer + pathProducts + '/$concertId',
        null,
        BaseErrorEntity.badRequestToModelError,
        headers: {'Accept-Language': 'th'},
        hasPermission: true);
    return GetConcertByIdEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> updateConcertById(
      int concertId, SaveConcertEntity saveConcertEntity) async {
    Response response = await api.put(
        pathOrganizer + pathProducts + "/${concertId}",
        saveConcertEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1265?store_id=5000815
  Future<SaveConcertResponseEntity> deleteConcertById(
      int concertId, String storeId) async {
    Response response = await api.delete(
        pathOrganizer + pathProducts + "/${concertId}?store_id=${storeId}",
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1289/settings?store_id=5000815
  Future<GetTicketSettingEntity> getConcertSetting(
      int concertId, String storeId) async {
    Response response = await api.get(
        pathOrganizer + pathProducts + "/${concertId}" + pathSettings,
        {'store_id': storeId},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetTicketSettingEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> updateConcertTicketSetting(
      int concertId, SaveTicketSettingEntity saveTicketSettingEntity) async {
    Response response = await api.put(
        pathOrganizer + pathProducts + "/${concertId}" + pathSettings,
        saveTicketSettingEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> saveConcertTicket(
      int concertId, SaveConcertTicketEntity saveConcertTicketEntity) async {
    Response response = await api.post(
        pathOrganizer + pathProducts + "/${concertId}" + pathVariants,
        saveConcertTicketEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/variants
  Future<GetConcertTicketsEntity> getConcertTicket(int concertId) async {
    Response response = await api.get(
        pathOrganizer + pathProducts + "/${concertId}" + pathVariants,
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetConcertTicketsEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1313/variants
  Future<GetTicketEntity> getTicket(int concertId) async {
    Response response = await api.get(
        pathOrganizer + pathProducts + '/${concertId}' + '/variants',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetTicketEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions
  Future<ResponseSaveDiscountEntity> saveDiscount(Map params) async {
    print(params);
    Response response = await api.post(pathOrganizer + pathPromotion, params,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseSaveDiscountEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/86/products
  Future<ResponseLinkIdEntity> linkId(int codeId, List item) async {
    Response response = await api.post(
        pathOrganizer + pathPromotion + '/${codeId}' + '/products',
        {"products": item.toList()},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseLinkIdEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions?limit=10&order=created_at&page=1&sort=desc&store_id=5000855
  Future<GetDiscountEntity> getDiscount(String storeId) async {
    Response response = await api.get(
        pathOrganizer +
            pathPromotion +
            '?limit=10&order=created_at&page=1&sort=desc&store_id=${storeId}',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetDiscountEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/variants/2049?store_id=5000815
  Future<GetTicketByIdEntity> getConcertTicketById(
      int concertId, int ticketId, String storeId) async {
    Response response = await api.get(
        pathOrganizer +
            pathProducts +
            "/${concertId}" +
            pathVariants +
            '/${ticketId}',
        {'store_id': storeId},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetTicketByIdEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> updateConcertTicket(int concertId,
      int ticketId, SaveConcertTicketEntity saveConcertTicketEntity) async {
    Response response = await api.put(
        pathOrganizer +
            pathProducts +
            "/${concertId}" +
            pathVariants +
            '/${ticketId}',
        saveConcertTicketEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/variants/2049?store_id=5000815
  Future<SaveConcertResponseEntity> deleteConcertTicket(
      int concertId, int ticketId, String storeId) async {
    Response response = await api.delete(
        pathOrganizer +
            pathProducts +
            '/$concertId' +
            pathVariants +
            '/$ticketId' +
            '?store_id=$storeId',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/variants/2051
  Future<SaveConcertResponseEntity> updateConcertTicketStatus(int concertId,
      int ticketId, UpdateTicketStatusEntity updateTicketStatusEntity) async {
    Response response = await api.put(
        pathOrganizer +
            pathProducts +
            "/${concertId}" +
            pathVariants +
            '/${ticketId}',
        updateTicketStatusEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1316
  Future<SaveConcertResponseEntity> updateConcertStatus(int concertId,
      UpdateConcertStatusEntity updateConcertStatusEntity) async {
    Response response = await api.put(
        pathOrganizer + pathProducts + "/${concertId}",
        updateConcertStatusEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/100
  Future<ResponseResultEntity> setStatus(
      int discountId, bool status, String storeId) async {
    Response response = await api.put(
        pathOrganizer + pathPromotion + '/${discountId}',
        {"status": status, "store_id": "${storeId}"},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseResultEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/97/codes?limit=10&order=created_at&page=1&sort=desc
  Future<GetCodeEntity> getCode(int discountId) async {
    Response response = await api.get(
        pathOrganizer +
            pathPromotion +
            '/${discountId}' +
            '/codes?limit=10&order=created_at&page=1&sort=desc',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetCodeEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/97/codes/13695
  Future<ResponseResultEntity> setStatusCode(
      int discountId, int codeId, bool status) async {
    Response response = await api.put(
        pathOrganizer +
            pathPromotion +
            '/${discountId}' +
            pathCode +
            '/${codeId}',
        {"status": "${status}"},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseResultEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/97/codes/13695
  Future<ResponseResultEntity> deleteCode(int discountId, int codeId) async {
    Response response = await api.delete(
        pathOrganizer +
            pathPromotion +
            '/${discountId}' +
            pathCode +
            '/${codeId}',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseResultEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/103/codes
  Future<GenerateCodeEntity> generateCode(int discountId, List item) async {
    Response response = await api.post(
        pathOrganizer + pathPromotion + '/${discountId}' + pathCode,
        {"items": item.toList()},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GenerateCodeEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/103
  Future<ResponseResultEntity> updateDiscount(
      int discountId, Map params) async {
    Response response = await api.put(
        pathOrganizer + pathPromotion + '/${discountId}',
        params,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseResultEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/103?store_id=5000855
  Future<ResponseResultEntity> deleteDiscount(
      int discountId, String storeId) async {
    Response response = await api.delete(
        pathOrganizer +
            pathPromotion +
            '/${discountId}?' +
            'store_id=${storeId}',
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return ResponseResultEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/promotions/109/products
  Future<GetTicketSelectEntity> getTicketSelected(int discountId) async {
    Response response = await api.get(
        pathOrganizer + pathPromotion + '/${discountId}' + pathProducts,
        null,
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return GetTicketSelectEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/ticket-price-calculator?service_fee_payer=buyer&payment_fee_payer=buyer&ticket_price=100.50&ticket_amount=1
  Future<GetCalculateTicketEntity> getTicketPriceCalculate(
      TicketPayer serviceFee,
      TicketPayer paymentFee,
      double price,
      int amount) async {
    Response response = await api.get(
        pathTicketPriceCalculate,
        {
          'service_fee_payer':describeEnum(serviceFee),
          'payment_fee_payer':describeEnum(paymentFee),
          'ticket_price':price,
          'ticket_amount':amount
        },
        BaseErrorEntity.badRequestToModelError);
    return GetCalculateTicketEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/checkin-tokens?page=1&store_id=5000815&fields=id,token,note,last_login_at,created_at,updated_at&order=created_at&sort=desc&limit=30
  Future<GetTicketScanEntity> getTicketScan(
      int concertId,
      String storeId,
      int page,
      int limit
      ) async {
    Response response = await api.get(
        pathOrganizer+pathProducts+'/$concertId'+pathCheckInToken,
        {
          'page':page,
          'store_id':storeId,
          'fields':'id,token,note,last_login_at,created_at,updated_at',
          'order':'created_at',
          'sort': 'desc',
          'limit': limit
        },
        BaseErrorEntity.badRequestToModelError,hasPermission: true);
    return GetTicketScanEntity.fromJson(response.data);
  }

  //https://alpha-api.theconcert.com/v2/organizers/products/1322/checkin-tokens
  Future<SaveConcertResponseEntity> saveTicketScan(int concertId,SaveTicketScanEntity saveTicketScanEntity ) async {
    Response response = await api.post(
        pathOrganizer + pathProducts + '/${concertId}' + pathCheckInToken,
        saveTicketScanEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> updateTicketScan(int concertId,int ticketScanId,SaveTicketScanEntity saveTicketScanEntity )async {
    Response response = await api.put(
        pathOrganizer + pathProducts + '/${concertId}' + pathCheckInToken + '/$ticketScanId',
        saveTicketScanEntity.toJson(),
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> deleteTicketScan(int concertId,int ticketScanId, String storeId) async {
    Response response = await api.delete(
        pathOrganizer + pathProducts + '/${concertId}' + pathCheckInToken + '/$ticketScanId',
        {'store_id':storeId},
        BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }
}
