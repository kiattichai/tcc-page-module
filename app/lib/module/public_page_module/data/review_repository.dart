import 'package:app/core/base_repository.dart';
import 'package:app/model/delete_image_review_response_entity.dart';
import 'package:app/model/get_review_by_id_entity.dart';
import 'package:app/model/get_review_entity.dart';
import 'package:app/model/save_review_entity.dart';
import 'package:app/model/save_review_image_entity.dart';
import 'package:app/model/save_review_image_response_entity.dart';
import 'package:app/model/save_review_response_entity.dart';
import 'package:app/module/public_page_module/data/review_api.dart';
import 'package:injectable/injectable.dart';

@singleton
class ReviewRepository extends BaseRepository {
  final ReviewApi api;

  ReviewRepository(this.api);

  Future<SaveReviewResponseData?> saveReview(SaveReviewEntity saveReview) async {
    final result = await api.saveReview(saveReview);
    return result.data;
  }

  Future<SaveReviewImageResponseData?> saveReviewImage(int reviewId,SaveReviewImageEntity saveReviewImage) async {
    final result = await api.saveReviewImage(reviewId,saveReviewImage);
    return result.data;
  }


  Future<GetReviewDataRecord?> getStoreReviewById(int storeId, int reviewId) async {
    final result = await api.getStoreReviewById(storeId,reviewId);
    if(result.data?.createdAt?.time != null){
      result.data!.createdAt!.time = result.data!.createdAt!.time.toString() + ' น.';
    }
    return GetReviewDataRecord.fromJson(result.data!.toJson());
  }


  Future<DeleteImageReviewResponseData?> deleteImageReview(int reviewId,List<String> imageIds) async {
    final result = await api.deleteImageReview(reviewId,imageIds);
    return result.data;
  }

  Future<SaveReviewResponseData?> updateReview(int reviewId,SaveReviewEntity saveReview) async {
    final result = await api.updateReview(reviewId,saveReview);
    return result.data;
  }
}
