import 'package:app/model/product.dart';
import 'package:app/module/public_page_module/widget/product/product_all.dart';
import 'package:app/module/public_page_module/widget/product/product_widget.dart';
import 'package:app/utils/screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'widget/detail_widget.dart';

class TabProductWidget extends StatelessWidget {
  List<Product> items;
  String storeId;
  bool isOwner;

  TabProductWidget(this.items, this.storeId,this.isOwner);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        isOwner ? Container(
          child: Column(
            children: [
              Container(
                width: Screen.width,
                margin: EdgeInsets.fromLTRB(22.0, 23.5, 22.0, 16.0),
                child: Center(
                  child: Text.rich(
                    TextSpan(children: <TextSpan>[
                      TextSpan(
                        text:
                            '       หากสินค้าของคุณ เปิดให้มีการซื้อผ่านระบบออนไลน์   อาจมีค่าธรรมเนียม และจำเป็นต้องส่งเอกสาร ',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff08080a),
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      TextSpan(
                        text: 'อ่านรายละเอียด',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff08080a),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            print('อ่านรายละเอียด');
                          },
                      ),
                    ]),
                  ),
                ),
              ),
              Container(
                width: 343,
                height: 42,
                margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 15.5),
                decoration: new BoxDecoration(
                    color: Color(0xffda3534),
                    borderRadius: BorderRadius.circular(21)),
                child: Center(
                  child: new Text(
                    "สร้างสินค้า",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ) : SizedBox(),
        DetailWidget(
          title: "สินค้า",
          detail: items.isNotEmpty ? "ดูทั้งหมด" : null,
          widget: ProductWidget(items: items,),
        ),
        Container(
          child: ProductAll(storeId),
        )
      ],
    );
  }
}
