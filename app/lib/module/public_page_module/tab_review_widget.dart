import 'package:app/model/create_review.dart';
import 'package:app/module/public_page_module/add_gallery_screen.dart';
import 'package:app/module/public_page_module/widget/checkIn/check_in_dialog.dart';
import 'package:app/module/public_page_module/widget/main/check_in_widget.dart';
import 'package:app/module/public_page_module/widget/main/picture_widget.dart';
import 'package:app/module/public_page_module/widget/review/create_review_dialog.dart';
import 'package:app/module/public_page_module/widget/review/review_widget.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';

import 'home_page_viewmodel.dart';

class TabReviewWidget extends StatelessWidget {
  HomeViewModel viewModel;
  Function(bool force) reloadReview;
  Function(int index) reloadReviewByIndex;
  Function(CreateReview?) callback;
  Function() callbackSaveGallery;

  TabReviewWidget(this.viewModel, this.reloadReview, this.reloadReviewByIndex, this.callback,
      this.callbackSaveGallery);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //TODO Review List
          viewModel.reviewData?.record?.length != 0
              ? Container(
                  decoration: BoxDecoration(
                      //border: Border(top: BorderSide(width: 3.0, color: Colors.grey.shade300)),
                      ),
                  child: ReviewWidget(viewModel, reloadReview, reloadReviewByIndex),
                )
              : SizedBox(),

          //TODO Check in
          viewModel.storeDetail?.section?.text == 'artist'
              ? SizedBox()
              : viewModel.checkInUserData?.pagination?.total != 0
                  ? CheckInWidget(
                      storeDetail: viewModel.storeDetail,
                      checkInUserData: viewModel.checkInUserData)
                  : Container(
                      margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 30.0),
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              'ยังไม่มีการเช็คอิน',
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          Container(
                            child: Center(
                              child: InkWell(
                                onTap: () async {
                                  print('check in');
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return CheckInDialog(viewModel);
                                      });
                                },
                                child: new Container(
                                  height: 40.0,
                                  margin: EdgeInsets.only(top: 18.0, bottom: 23.0),
                                  decoration: new BoxDecoration(
                                      color: Colors.grey.shade400,
                                      borderRadius: BorderRadius.circular(7)),
                                  child: Center(
                                    child: new Text(
                                      "เช็คอินที่นี่",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

          //TODO Gallery
          viewModel.galleryData?.record?.length != 0
              ? GalleryWidget(
                  galleryData: viewModel.galleryData,
                  storeId: viewModel.storeId,
                  storeName: viewModel.storeDetail?.name ?? '',
                )
              : Container(
                  margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                  child: Column(
                    children: [
                      Container(
                        child: Text(
                          'ยังไม่มีรูปภาพ',
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Colors.grey,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      Container(
                        child: Center(
                          child: InkWell(
                            onTap: () async {
                              print('add gallery');
                              //final result = await TixNavigate.instance.navigateTo(AddGalleryScreen(),data:viewModel.storeId);
                              final result = await WrapNavigation.instance.pushNamed(
                                  context, AddGalleryScreen(),
                                  arguments: viewModel.storeId);
                              if (result != null) {
                                callbackSaveGallery();
                                viewModel.getGallery();
                              }
                            },
                            child: new Container(
                              height: 40.0,
                              margin: EdgeInsets.only(top: 18.0, bottom: 23.0),
                              decoration: new BoxDecoration(
                                  color: Colors.grey.shade400,
                                  borderRadius: BorderRadius.circular(7)),
                              child: Center(
                                child: new Text(
                                  "เพิ่มรูปภาพ",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),

          //TODO Review Empty
          viewModel.reviewData?.record?.length == 0
              ? InkWell(
                  onTap: () async {
                    print('review');
                    var result = await showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CreateReviewDialog();
                        });
                    this.callback(result);
                    viewModel.getReview(forceTabVisible: true);
                    viewModel.getStoreDetail();
                  },
                  child: Container(
                    height: 200.0,
                    margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0, bottom: 16.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7.0),
                      border: Border.all(color: Colors.grey.shade300),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 55.0,
                          width: 55.0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: CircleAvatar(
                              child: Image.network(
                                '${viewModel.storeDetail?.images?.logo?.url}',
                                fit: BoxFit.cover,
                                width: 68,
                                height: 68,
                                errorBuilder: (context, error, stackTrace) => Image(
                                  width: 68,
                                  height: 68,
                                  image: AssetImage("assets/image-empty.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                          child: Text(
                            'คะแนนรีวิว',
                            style: TextStyle(
                                fontFamily: 'sukhumvitSet-Text',
                                fontSize: 16.0,
                                color: Colors.grey.shade400),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 30.0,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
