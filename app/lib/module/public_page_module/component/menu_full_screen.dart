import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/model/get_store_menu_food_entity.dart';
import 'package:app/utils/screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

class MenuFullScreen extends StatefulWidget with TixRoute {
  final Map? data;

  const MenuFullScreen({Key? key, this.data}) : super(key: key);

  @override
  _MenuFullScreenState createState() => _MenuFullScreenState();

  @override
  String buildPath() {
    return '/menu_full_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => MenuFullScreen(data: data));
  }
}

class _MenuFullScreenState extends BaseStateProvider<MenuFullScreen, MenuFullScreenViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = MenuFullScreenViewModel(widget.data);
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<MenuFullScreenViewModel>(
        builder: (context, model, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              actions: [
                Center(
                  child: Container(
                    margin: EdgeInsets.only(right: 16.0),
                    child: new Text(
                      "${(viewModel.getPageIndex() + 1).toString() + "/" + viewModel.total.toString()}",
                      style: TextStyle(
                        fontFamily: 'SFUIText',
                        color: Color(0xffeeeeee),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: Container(
              height: Screen.height,
              decoration: BoxDecoration(color: Colors.black),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        width: Screen.width,
                        child: Stack(
                          children: [
                            Container(
                              child: CarouselSlider.builder(
                                itemCount: viewModel.galleryDataList.length,
                                carouselController: viewModel._controller,
                                options: CarouselOptions(
                                    initialPage: widget.data?["index"] ?? 0,
                                    height: Screen.height,
                                    viewportFraction: 1.0,
                                    enlargeCenterPage: false,
                                    enableInfiniteScroll: false,
                                    onPageChanged: (index, reason) {
                                      viewModel.notifyListeners();
                                      viewModel.pageIndex(index);
                                      int rang = viewModel.galleryDataList.length - index;
                                      if (rang < 5 && !viewModel.isComplete) {
                                        viewModel.getImage();
                                      }
                                    }),
                                itemBuilder: (context, index, realIdx) {
                                  var item = viewModel.galleryDataList[index];
                                  return PhotoView(
                                    initialScale: PhotoViewComputedScale.contained * 1.0,
                                    minScale: PhotoViewComputedScale.contained * 0.8,
                                    maxScale: PhotoViewComputedScale.covered * 2.0,
                                    imageProvider: NetworkImage(item.url ?? ''),
                                  );
                                },
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: IconButton(
                                  onPressed: () {
                                    if (viewModel.indexPage != 0) {
                                      viewModel._controller.previousPage();
                                    }
                                  },
                                  icon: Icon(
                                    Icons.arrow_back_ios_sharp,
                                    color: Color(0xffeeeeee),
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                  onPressed: () {
                                    viewModel._controller.nextPage();
                                  },
                                  icon: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Color(0xffeeeeee),
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        model: viewModel);
  }
}

class MenuFullScreenViewModel extends BaseViewModel {
  final CarouselController _controller = CarouselController();

  Map? data;
  List<GetStoreMenuFoodDataRecord> galleryDataList = [];
  GetStoreMenuFoodData? galleryData;
  String storeId = "";
  int indexPage = 0;
  int? total;
  int imagePage = 1;
  int imageLimit = 10;
  bool isComplete = false;
  bool isEdit = false;

  MenuFullScreenViewModel(this.data) {
    mapData();
  }

  @override
  void postInit() {
    super.postInit();
  }

  void mapData() {
    galleryDataList.addAll(data!["imgList"] ?? []);
    total = data?["total"];
    storeId = data?["storeId"];
    imagePage = data?["imagePage"];
    indexPage = data?["index"];
    notifyListeners();
  }

  void mapStoreId() {
    storeId = data?["storeId"];
  }

  void pageIndex(int index) {
    print(index);
    indexPage = index++;
    notifyListeners();
  }

  getPageIndex() {
    return indexPage;
  }

  void mapTotal() {
    total = data?["total"];
    notifyListeners();
  }

  void mapImage() {
    galleryDataList.addAll(data?["imgList"] ?? []);
    notifyListeners();
  }

  void mapImagePage() {
    imagePage = data?["imagePage"];
    print(imagePage);
  }

  void getImage() async {
    catchError(() async {
      setLoading(true);

      galleryData = await di.pageRepository.getStoreMenuFood(storeId, imageLimit, imagePage);
      if (galleryData == null || galleryData?.record == null) return;
      if (galleryData != null &&
          galleryData!.record!.length > 0 &&
          galleryData!.pagination!.currentPage! <= galleryData!.pagination!.lastPage!) {
        galleryDataList.addAll(galleryData?.record ?? []);
        imagePage = imagePage + 1;
        isComplete = false;
      } else {
        isComplete = true;
      }
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
  }
}
