import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/create_concert_module/add_venue_screen_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AddVenueScreen extends StatefulWidget with TixRoute {
  String? storeId;

  AddVenueScreen({this.storeId});

  @override
  _AddVenueScreen createState() => _AddVenueScreen();

  @override
  String buildPath() {
    return '/add_venue_screen';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => AddVenueScreen(storeId: data['storeId']));
  }
}

class _AddVenueScreen
    extends BaseStateProvider<AddVenueScreen, AddVenueScreenViewModel> {
  @override
  void initState() {
    viewModel = AddVenueScreenViewModel(widget.storeId!);
    // viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddVenueScreenViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Scaffold(
              appBar: AppBarWidget(
                name: 'สร้างสถานที่',
              ),
              body: SafeArea(
                  child: CustomScrollView(slivers: [
                SliverToBoxAdapter(
                    child: Container(
                  padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autofocus: false,
                      controller: viewModel.name,
                      style: textStyle(),
                      validator: viewModel.validateName,
                      decoration: InputDecoration(
                          labelText: 'ชื่อสถานที่',
                          border: OutlineInputBorder(),
                          isDense: true,
                          labelStyle: textStyle(),
                          counterText: ''),
                      maxLength: 50,
                      onChanged: (text) {
                        viewModel.check();
                      }),
                )),
                SliverToBoxAdapter(
                    child: Container(
                        padding:
                            const EdgeInsets.only(top: 10, left: 16, right: 16),
                        child: new Text("ระบุตำแหน่งที่ตั้งของคุณ",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff08080a),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            )))),
                SliverToBoxAdapter(
                    child: Container(
                        padding:
                            const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: TextFormField(
                          validator: viewModel.validateAddress,
                          onChanged: (text) {},
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autofocus: false,
                          maxLength: 200,
                          controller: viewModel.address,
                          style: textStyle(),
                          readOnly: true,
                          onTap: () => viewModel.navigateMap(context),
                          decoration: InputDecoration(
                              labelText: 'ตำแหน่งที่ตั้ง',
                              suffixIcon: Icon(
                                Icons.location_on,
                                size: 25,
                              ),
                              isDense: true,
                              labelStyle: textStyle(),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ))),
                SliverFillRemaining(
                    hasScrollBody: false,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                          margin: const EdgeInsets.only(
                              top: 20, left: 16, right: 16, bottom: 24),
                          width: double.infinity,
                          height: 42,
                          child: ElevatedButton(
                              child: Text(
                                "ถัดไป",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: viewModel.isActive
                                      ? Color(0xffffffff)
                                      : Color(0xffb2b2b2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              style: ButtonStyle(
                                  foregroundColor: MaterialStateProperty.all<
                                      Color>(Colors.white),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          viewModel.isActive
                                              ? Color(0xffda3534)
                                              : Color(0xffe6e7ec)),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(21),
                                  ))),
                              onPressed: () => viewModel.isActive
                                  ? viewModel.save(context)
                                  : null)),
                    ))
              ])));
        });
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: fontSize,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
