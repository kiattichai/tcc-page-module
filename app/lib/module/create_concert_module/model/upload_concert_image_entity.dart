import 'package:app/generated/json/upload_concert_image_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UploadConcertImageEntity {
  UploadConcertImageEntity();

  factory UploadConcertImageEntity.fromJson(Map<String, dynamic> json) =>
      $UploadConcertImageEntityFromJson(json);

  Map<String, dynamic> toJson() => $UploadConcertImageEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  String? status;
  bool? secure;
  List<UploadConcertImageFiles>? files;
}

@JsonSerializable()
class UploadConcertImageFiles {
  UploadConcertImageFiles();

  factory UploadConcertImageFiles.fromJson(Map<String, dynamic> json) =>
      $UploadConcertImageFilesFromJson(json);

  Map<String, dynamic> toJson() => $UploadConcertImageFilesToJson(this);

  String? name;
  String? data;
  int? position;
}
