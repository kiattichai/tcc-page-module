import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_concert_response_entity.g.dart';

@JsonSerializable()
class SaveConcertResponseEntity {
  SaveConcertResponseEntity();

  factory SaveConcertResponseEntity.fromJson(Map<String, dynamic> json) =>
      $SaveConcertResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertResponseEntityToJson(this);

  SaveConcertResponseData? data;
  SaveConcertResponseBench? bench;
}

@JsonSerializable()
class SaveConcertResponseData {
  SaveConcertResponseData();

  factory SaveConcertResponseData.fromJson(Map<String, dynamic> json) =>
      $SaveConcertResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertResponseDataToJson(this);

  String? message;
  int? id;
}

@JsonSerializable()
class SaveConcertResponseBench {
  SaveConcertResponseBench();

  factory SaveConcertResponseBench.fromJson(Map<String, dynamic> json) =>
      $SaveConcertResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
