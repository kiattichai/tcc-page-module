import 'package:app/generated/json/save_concert_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveConcertEntity {
  SaveConcertEntity();

  factory SaveConcertEntity.fromJson(Map<String, dynamic> json) =>
      $SaveConcertEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertEntityToJson(this);

  String? id;
  String? type;
  String? slug;
  @JSONField(name: "venue_id")
  int? venueId;
  @JSONField(name: "store_id")
  String? storeId;
  SaveConcertName? name;
  SaveConcertDescription? description;
  @JSONField(name: "seo_name")
  SaveConcertSeoName? seoName;
  @JSONField(name: "seo_description")
  SaveConcertSeoDescription? seoDescription;
  @JSONField(name: "show_start")
  String? showStart;
  @JSONField(name: "show_end")
  String? showEnd;
  List<SaveConcertAttributes>? attributes;
  List<SaveConcertImages>? images;
  @JSONField(name: "created_by")
  int? createdBy;
  @JSONField(name: "updated_by")
  String? updatedBy;
}

@JsonSerializable()
class SaveConcertName {
  SaveConcertName();

  factory SaveConcertName.fromJson(Map<String, dynamic> json) =>
      $SaveConcertNameFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertNameToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class SaveConcertDescription {
  SaveConcertDescription();

  factory SaveConcertDescription.fromJson(Map<String, dynamic> json) =>
      $SaveConcertDescriptionFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertDescriptionToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class SaveConcertSeoName {
  SaveConcertSeoName();

  factory SaveConcertSeoName.fromJson(Map<String, dynamic> json) =>
      $SaveConcertSeoNameFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertSeoNameToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class SaveConcertSeoDescription {
  SaveConcertSeoDescription();

  factory SaveConcertSeoDescription.fromJson(Map<String, dynamic> json) =>
      $SaveConcertSeoDescriptionFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertSeoDescriptionToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class SaveConcertAttributes {
  SaveConcertAttributes();

  factory SaveConcertAttributes.fromJson(Map<String, dynamic> json) =>
      $SaveConcertAttributesFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertAttributesToJson(this);

  int? id;
  @JSONField(name: "value_id")
  int? valueId;
  @JSONField(name: "value_text")
  String? valueText;
}

@JsonSerializable()
class SaveConcertImages {
  SaveConcertImages();

  factory SaveConcertImages.fromJson(Map<String, dynamic> json) =>
      $SaveConcertImagesFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertImagesToJson(this);

  String? id;
  String? tag;
  int? position;
}
