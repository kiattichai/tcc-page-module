import 'package:app/generated/json/upload_concert_image_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UploadConcertImageResponseEntity {
  UploadConcertImageResponseEntity();

  factory UploadConcertImageResponseEntity.fromJson(
          Map<String, dynamic> json) =>
      $UploadConcertImageResponseEntityFromJson(json);

  Map<String, dynamic> toJson() =>
      $UploadConcertImageResponseEntityToJson(this);

  UploadConcertImageResponseData? data;
  UploadConcertImageResponseBench? bench;
}

@JsonSerializable()
class UploadConcertImageResponseData {
  UploadConcertImageResponseData();

  factory UploadConcertImageResponseData.fromJson(Map<String, dynamic> json) =>
      $UploadConcertImageResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $UploadConcertImageResponseDataToJson(this);

  String? message;
  List<UploadConcertImageResponseDataUploads>? uploads;
}

@JsonSerializable()
class UploadConcertImageResponseDataUploads {
  UploadConcertImageResponseDataUploads();

  factory UploadConcertImageResponseDataUploads.fromJson(
          Map<String, dynamic> json) =>
      $UploadConcertImageResponseDataUploadsFromJson(json);

  Map<String, dynamic> toJson() =>
      $UploadConcertImageResponseDataUploadsToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
  int? position;
}

@JsonSerializable()
class UploadConcertImageResponseBench {
  UploadConcertImageResponseBench();

  factory UploadConcertImageResponseBench.fromJson(Map<String, dynamic> json) =>
      $UploadConcertImageResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $UploadConcertImageResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
