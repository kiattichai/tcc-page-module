import 'package:app/model/get_venue_entity.dart';
import 'package:app/module/create_concert_module/add_venue_screen.dart';
import 'package:app/module/create_concert_module/create_concert_image.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/select_concert_place.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertPlace extends StatefulWidget with TixRoute {
  SaveConcertEntity? saveConcertEntity;

  CreateConcertPlace({this.saveConcertEntity});

  @override
  _CreateConcertName createState() => _CreateConcertName();

  @override
  String buildPath() {
    return '/create_concert_place';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreateConcertPlace(saveConcertEntity: data));
  }
}

class _CreateConcertName extends State<CreateConcertPlace> {
  TextEditingController place = TextEditingController(text: 'สถานที่');
  GetVenueDataRecord? getVenueDataRecord;
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          name: 'สร้างคอนเสิร์ต',
        ),
        body: SafeArea(
            child: CustomScrollView(slivers: [
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Row(
                    children: [
                      new Text("สถานที่แสดงคอนเสิร์ต",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff08080a),
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      Spacer(),
                      TextButton(
                          onPressed: () => navigateToCreateVenue(),
                          child: Row(
                            children: [
                              Icon(
                                Icons.add_location,
                                size: 20,
                                color: Colors.blue,
                              ),
                              Text("เพิ่มสถานที่",
                                  style: TextStyle(
                                    fontFamily: 'SukhumvitSet-Text',
                                    color: Colors.blue,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          )),
                    ],
                  ))),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 10, left: 16, right: 16),
                  child:
                      new Text("เพิ่มตำแหน่งที่คอนเสิร์ตทำการแสดงสำหรับผู้ที่สนใจ คอนเสิร์ตของคุณ",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff6d6d6d),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )))),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    readOnly: true,
                    style: textStyle(),
                    controller: place,
                    onTap: () => navigateToSelectConcertPlace(),
                    validator: validate,
                    decoration: InputDecoration(
                        isDense: true,
                        suffixIcon: Icon(
                          Icons.keyboard_arrow_down,
                          size: 25,
                          color: Colors.black,
                        ),
                        border: OutlineInputBorder(),
                        counterText: ''),
                  ))),
          SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                    width: double.infinity,
                    height: 42,
                    child: ElevatedButton(
                        child: Text(
                          "ถัดไป",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        style: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(21),
                            ))),
                        onPressed: () => isActive ? save(context) : null),
                  )))
        ])));
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }

  save(BuildContext context) {
    widget.saveConcertEntity!.venueId = getVenueDataRecord?.id!;
    //TixNavigate.instance.navigateTo(CreateConcertImage(), data: widget.saveConcertEntity);
    WrapNavigation.instance
        .pushNamed(context, CreateConcertImage(), arguments: widget.saveConcertEntity);
  }

  navigateToSelectConcertPlace() async {
    //GetVenueDataRecord? result = await TixNavigate.instance.navigateTo(SelectConcertPlace(), data: {'storeId':widget.saveConcertEntity!.storeId!});
    GetVenueDataRecord? result = await WrapNavigation.instance.pushNamed(
        context, SelectConcertPlace(),
        arguments: {'storeId': widget.saveConcertEntity!.storeId!});
    if (result != null) {
      setState(() {
        place.text = result.name!;
        getVenueDataRecord = result;
        check();
      });
    }
  }

  check() {
    isActive = validate(place.text) == null;
  }

  String? validate(String? value) {
    if (getVenueDataRecord == null) {
      return 'กรุณาระบุสถานที่';
    }
    return null;
  }

  navigateToCreateVenue() async {
    //final result = await TixNavigate.instance.navigateTo(AddVenueScreen(), data: {'storeId':widget.saveConcertEntity!.storeId!});
    final result = await WrapNavigation.instance.pushNamed(context, AddVenueScreen(),
        arguments: {'storeId': widget.saveConcertEntity!.storeId!});
  }
}
