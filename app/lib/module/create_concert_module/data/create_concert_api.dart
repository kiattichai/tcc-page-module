import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_response_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class CreateConcertApi {
  final CoreApi api;
  final String pathOrganizer = '/v2/organizers';
  final String pathUpload = '/uploads';
  final String pathProducts = '/products';

  CreateConcertApi(this.api);

  Future<UploadConcertImageResponseEntity> saveConcertImage(
      UploadConcertImageEntity uploadConcertImage) async {
    Response response = await api.post(pathOrganizer + pathUpload,
        uploadConcertImage.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return UploadConcertImageResponseEntity.fromJson(response.data);
  }

  Future<SaveConcertResponseEntity> saveConcert(
      SaveConcertEntity saveConcertEntity) async {
    Response response = await api.post(pathOrganizer + pathProducts,
        saveConcertEntity.toJson(), BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return SaveConcertResponseEntity.fromJson(response.data);
  }
}
