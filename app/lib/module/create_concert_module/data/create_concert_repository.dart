import 'package:app/module/create_concert_module/data/create_concert_api.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/save_concert_response_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_response_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class CreateConcertRepository {
  final CreateConcertApi api;

  CreateConcertRepository(this.api);

  Future<UploadConcertImageResponseData?> saveConcertImage(UploadConcertImageEntity uploadConcertImage) async {
    final result = await api.saveConcertImage(uploadConcertImage);
    return result.data;
  }

  Future<SaveConcertResponseData?> saveConcert(SaveConcertEntity saveConcertEntity) async {
    final result = await api.saveConcert(saveConcertEntity);
    return result.data;
  }
}