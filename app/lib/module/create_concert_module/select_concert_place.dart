import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/create_concert_module/select_concert_place_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class SelectConcertPlace extends StatefulWidget with TixRoute {

  String? storeId;
  int? selectedId;


  SelectConcertPlace({this.storeId,this.selectedId});

  @override
  _SelectConcertPlace createState() => _SelectConcertPlace();

  @override
  String buildPath() {
    return '/select_concert_place';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => SelectConcertPlace(storeId:data['storeId'],selectedId:data['selectedId']));
  }
}

class _SelectConcertPlace
    extends BaseStateProvider<SelectConcertPlace, SelectConcertPlaceViewModel> {

  @override
  void initState() {
    viewModel = SelectConcertPlaceViewModel(widget.storeId!,selectedId: widget.selectedId);
    // viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectConcertPlaceViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Scaffold(
              appBar: AppBarWidget(
                name: 'สร้างคอนเสิร์ต',
              ),
              body:
              ListView.builder(
                  itemCount: viewModel.venueList.length,
                  controller: viewModel.scrollController,
                  itemBuilder: (context, index) {
                    return checkVenue(index);
                  }),
              bottomNavigationBar: Container(
                margin: const EdgeInsets.only(
                    top: 5, left: 16, right: 16, bottom: 5),
                width: double.infinity,
                height: 42,
                child: ElevatedButton(
                    child: Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: true ? Color(0xffffffff) : Color(0xffb2b2b2),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            true ? Color(0xffda3534) : Color(0xffe6e7ec)),
                        shape: MaterialStateProperty.all<
                            RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(21),
                        ))),
                    onPressed: () => viewModel.save(context)),
              )
          );
        });

  }

  Widget checkVenue(int index) {
    return Container(
        padding:
            const EdgeInsets.only(top: 10, right: 16, left: 16, bottom: 10),
        child: InkWell(
          child: Row(
            children: [
              Container(width: 45,height: 45,
                child:Icon(Icons.location_on_outlined,size: 28,color: Colors.white,),
                decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xFFB2B2B2),
              ),),
              SizedBox(
                width: 15,
              ),
              Container(
                  width: Screen.width - 109,
                  child: Text(viewModel.venueList[index].name ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ))),
              Spacer(),
              viewModel.venueSelected?.id == viewModel.venueList[index].id
                  ? Image(
                      image: AssetImage('assets/check.png'),
                      width: 17,
                      height: 12,
                      color: Color(0xFF3FA73C),
                    )
                  : SizedBox()
            ],
          ),
          onTap: () => viewModel.selectVenue(index),
        ));
  }
}
