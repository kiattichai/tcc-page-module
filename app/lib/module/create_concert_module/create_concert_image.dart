import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/create_concert_module/create_concert_Image_view_model.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertImage extends StatefulWidget with TixRoute {

  SaveConcertEntity? saveConcertEntity;

  CreateConcertImage({this.saveConcertEntity});

  @override
  _CreateConcertImage createState() => _CreateConcertImage();

  @override
  String buildPath() {
    return '/create_concert_Image';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => CreateConcertImage(saveConcertEntity: data));
  }
}

class _CreateConcertImage
    extends BaseStateProvider<CreateConcertImage, CreateConcertImageViewModel> {

  @override
  void initState() {
    viewModel = CreateConcertImageViewModel(widget.saveConcertEntity!);
    //viewModel = HomeViewModel('5000440', this, widget.isOwner ?? false);

    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateConcertImageViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                  appBar: AppBarWidget(
                    name: 'สร้างคอนเสิร์ต',
                  ),
                  body: SafeArea(child: CustomScrollView(slivers: [
                    SliverToBoxAdapter(
                        child: Container(
                            padding: const EdgeInsets.only(
                                top: 16, left: 16, right: 16),
                            child: new Text("เลือกภาพสำหรับคอนเสิร์ต",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff08080a),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                )))),
                    SliverToBoxAdapter(
                        child: Container(
                            padding: const EdgeInsets.only(
                                top: 10, left: 16, right: 16, bottom: 10),
                            child: new Text(
                                "เพิ่มภาพที่น่าสนใจเกี่ยวกับคอนเสิร์ตของคุณ เพื่อดึงดูดให้คนอยากไปเข้าร่วม ขนาดที่แนะนำ 800 x 1200 px",
                                style: TextStyle(
                                  fontFamily: 'SukhumvitSet-Text',
                                  color: Color(0xff6d6d6d),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )))),
                    SliverToBoxAdapter(
                        child: Container(
                          decoration: BoxDecoration(
                              image: viewModel.concertImage != null?DecorationImage(image: FileImage(viewModel.concertImage!),alignment: Alignment.center):null,
                            color: Color(0xFFE0E0E0),
                          ),
                          height: 350,
                          child: Stack(
                            children: [
                              viewModel.concertImage == null ? Center(
                                child: Text("800 x 1200 px",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )
                                ),
                              ):SizedBox(),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: InkWell(
                                  onTap: () => viewModel.pickProfileImage(),
                                  child: Container(
                                      margin: const EdgeInsets.only(bottom: 16),
                                      width: 119,
                                      height: 37,
                                      child: Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .center,
                                          children: [
                                            Image(image: AssetImage(
                                                'assets/gallery.png'),
                                              width: 18,
                                              height: 18,
                                              color: Colors.black,),
                                            SizedBox(width: 11,),
                                            Text("เลือกภาพ",
                                                style: TextStyle(
                                                  fontFamily: 'SukhumvitSet-Text',
                                                  color: Color(0xff4a4a4a),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                )
                                            )
                                          ]

                                      ),
                                      decoration: new BoxDecoration(
                                          color: Color(0xFFF4F4F4),
                                          borderRadius: BorderRadius.circular(5),

                                      )
                                  ),
                                ),
                              )
                            ],
                          ),
                        )),
                    SliverFillRemaining(
                        hasScrollBody: false,
                        child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  top: 20, left: 16, right: 16, bottom: 24),
                              width: double.infinity,
                              height: 42,
                              child: ElevatedButton(
                                  child: Text(
                                    "สร้างคอนเสิร์ต",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: viewModel.isActive ? Color(0xffffffff) : Color(
                                          0xffb2b2b2),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  style: ButtonStyle(
                                      foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                      backgroundColor: MaterialStateProperty
                                          .all<Color>(
                                          viewModel.isActive ? Color(0xffda3534) : Color(
                                              0xffe6e7ec)),
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                21),
                                          ))),
                                  onPressed: () => viewModel.isActive ? viewModel.save(context) : null),
                            )))
                  ])))
          );
        }
    );
  }

}
