import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/module/create_concert_module/select_artist_view_model.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';


class SelectArtist extends StatefulWidget with TixRoute {

  List<GetArtistDataRecord>? artist;

  SelectArtist({this.artist});

  @override
  _SelectArtist createState() => _SelectArtist();

  @override
  String buildPath() {
    return '/select_artist';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => SelectArtist(artist: data,));
  }
}

class _SelectArtist
    extends BaseStateProvider<SelectArtist, SelectArtistViewModel> {
  @override
  void initState() {
    viewModel = SelectArtistViewModel(widget.artist??[]);
    // viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectArtistViewModel>(
        model: viewModel,
        builder: (context, model, child) {
          return Scaffold(
              appBar: AppBarWidget(
                name: 'สร้างคอนเสิร์ต',
              ),
              body: ListView.builder(
                  itemCount: viewModel.artistList.length,
                  controller: viewModel.scrollController,
                  itemBuilder: (context, index) {
                    return checkArtist(index);
                  }
              ),
              bottomNavigationBar: Container(
                margin: const EdgeInsets.only(
                    top: 5, left: 16, right: 16, bottom: 5),
                width: double.infinity,
                height: 42,
                child: ElevatedButton(
                    child: Text(
                      "บันทึก",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: true ? Color(0xffffffff) : Color(0xffb2b2b2),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            true ? Color(0xffda3534) : Color(0xffe6e7ec)),
                        shape: MaterialStateProperty.all<
                            RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(21),
                        ))),
                    onPressed: () => viewModel.save(context)),
              )
          );
        });
  }

  Widget checkArtist(int index) {
    return Container(
            padding:
            const EdgeInsets.only(top: 10, right: 16, left: 16, bottom: 10),
            child: InkWell(
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 18,
                    backgroundImage: NetworkImage(
                        viewModel.artistList[index].value?.image?.url ?? ''),
                  ),
                  SizedBox(
                    width: 19,
                  ),
                  Container(
                      width: Screen.width -104,
                      child:  Text(viewModel.artistList[index].value?.name ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ))),
                  Spacer(),
                  viewModel.artistList[index].checked
                      ? Image(
                    image: AssetImage('assets/check.png'),
                    width: 17,
                    height: 12,
                    color: Color(0xFF3FA73C),
                  )
                      : SizedBox()
                ],
              ),
              onTap: () => viewModel.selectArtist(index),
            ));
  }
}
