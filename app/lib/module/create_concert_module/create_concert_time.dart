import 'package:app/module/create_concert_module/create_concert_genre.dart';
import 'package:app/module/create_concert_module/create_concert_place.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/date_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/custom_date_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertTime extends StatefulWidget with TixRoute {
  SaveConcertEntity? saveConcertEntity;

  CreateConcertTime({this.saveConcertEntity});

  @override
  _CreateConcertTime createState() => _CreateConcertTime();

  @override
  String buildPath() {
    return '/create_concert_time';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreateConcertTime(saveConcertEntity: data));
  }
}

class _CreateConcertTime extends State<CreateConcertTime> {
  DateTime startDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10);
  DateTime endDateTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 10);

  DateTime maxDateTime =
      DateTime(DateTime.now().year + 5, DateTime.now().month, DateTime.now().day + 1, 10);

  TextEditingController startDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController endTime = TextEditingController();

  bool isActive = false;

  @override
  void initState() {
    formatDate();
    check();
    super.initState();
  }

  void formatDate() {
    startDate.text = DateFormat('dd MMM ', 'th').format(startDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(startDateTime)) + 543).toString();
    startTime.text = DateFormat('HH:mm', 'th').format(startDateTime);
    endDate.text = DateFormat('dd MMM ', 'th').format(endDateTime) +
        (int.parse(DateFormat('yyyy', 'th').format(endDateTime)) + 543).toString();
    endTime.text = DateFormat('HH:mm', 'th').format(endDateTime);
    setState(() {});
  }

  void _showDatePicker(DateTime time, bool isStart) {
    DatePicker.showPicker(context,
        showTitleActions: true,
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ), onConfirm: (date) {
      setState(() {
        if (isStart) {
          startDateTime =
              DateTime(date.year, date.month, date.day, startDateTime.hour, startDateTime.minute);
        } else {
          endDateTime =
              DateTime(date.year, date.month, date.day, endDateTime.hour, endDateTime.minute);
        }
        formatDate();
        check();
      });
    },
        pickerModel: CustomDatePicker(
            minTime: DateTime.now(),
            currentTime: time,
            locale: LocaleType.th,
            maxTime: maxDateTime),
        locale: LocaleType.th);
  }

  void _showTimePicker(DateTime time, bool isStart) {
    DatePicker.showTimePicker(context,
        showTitleActions: true, showSecondsColumn: false, currentTime: time, onConfirm: (date) {
      setState(() {
        if (isStart) {
          startDateTime = DateTime(
              startDateTime.year, startDateTime.month, startDateTime.day, date.hour, date.minute);
        } else {
          endDateTime = DateTime(
              endDateTime.year, endDateTime.month, startDateTime.day, date.hour, date.minute);
        }
        formatDate();
        check();
      });
    },
        theme: DatePickerTheme(
          headerColor: Colors.white70,
          backgroundColor: Colors.white,
          itemStyle: textStyle(),
          doneStyle: textStyle(),
          cancelStyle: textStyle(),
        ),
        locale: LocaleType.th);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          name: 'สร้างคอนเสิร์ต',
        ),
        body: SafeArea(
            child: CustomScrollView(slivers: [
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: new Text("วันและเวลาแสดง",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff08080a),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      )))),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 10, left: 16, right: 16),
                  child: new Text(
                      "ระบุวันที่และเวลาแสดงให้ชัดเจน เพื่อให้ทุกคนทราบวันและเวลาแสดงที่ถูกต้อง",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        color: Color(0xff6d6d6d),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      )))),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 3,
                          child: TextFormField(
                            onChanged: (text) {
                              // viewModel.check();
                            },
                            autovalidateMode: AutovalidateMode.always,
                            autofocus: false,
                            controller: startDate,
                            maxLength: 200,
                            style: textStyle(),
                            readOnly: true,
                            validator: validateStartDate,
                            onTap: () => _showDatePicker(startDateTime, true),
                            decoration: InputDecoration(
                                labelText: 'เริ่มเมื่อ',
                                suffixIcon: Icon(
                                  Icons.calendar_today,
                                  size: 20,
                                ),
                                isDense: true,
                                labelStyle: textStyle(),
                                border: OutlineInputBorder(),
                                counterText: ''),
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 2,
                        child: TextFormField(
                          onChanged: (text) {
                            // viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.always,
                          autofocus: false,
                          controller: startTime,
                          maxLength: 200,
                          style: textStyle(),
                          readOnly: true,
                          validator: validateStartDate,
                          onTap: () => _showTimePicker(startDateTime, true),
                          decoration: InputDecoration(
                              labelText: 'เวลาเริ่มต้น',
                              suffixIcon: Icon(
                                Icons.timer,
                                size: 20,
                              ),
                              isDense: true,
                              labelStyle: textStyle(fontSize: 12),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ),
                      )
                    ],
                  ))),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 3,
                          child: TextFormField(
                            onChanged: (text) {
                              // viewModel.check();
                            },
                            autovalidateMode: AutovalidateMode.always,
                            autofocus: false,
                            controller: endDate,
                            maxLength: 200,
                            style: textStyle(),
                            readOnly: true,
                            validator: validateEndDate,
                            onTap: () => _showDatePicker(endDateTime, false),
                            decoration: InputDecoration(
                                labelText: 'สิ้นสุดเมื่อ',
                                suffixIcon: Icon(
                                  Icons.calendar_today,
                                  size: 20,
                                ),
                                isDense: true,
                                labelStyle: textStyle(),
                                border: OutlineInputBorder(),
                                counterText: ''),
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 2,
                        child: TextFormField(
                          onChanged: (text) {
                            // viewModel.check();
                          },
                          autovalidateMode: AutovalidateMode.always,
                          autofocus: false,
                          controller: endTime,
                          maxLength: 200,
                          style: textStyle(),
                          readOnly: true,
                          validator: validateEndDate,
                          onTap: () => _showTimePicker(endDateTime, false),
                          decoration: InputDecoration(
                              labelText: 'เวลาสิ้นสุด',
                              suffixIcon: Icon(
                                Icons.timer,
                                size: 20,
                              ),
                              isDense: true,
                              labelStyle: textStyle(fontSize: 12),
                              border: OutlineInputBorder(),
                              counterText: ''),
                        ),
                      )
                    ],
                  ))),
          SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                    width: double.infinity,
                    height: 42,
                    child: ElevatedButton(
                        child: Text(
                          "ถัดไป",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        style: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(21),
                            ))),
                        onPressed: () => isActive ? save() : null),
                  )))
        ])));
  }

  check() {
    isActive = startDateTime.isBefore(endDateTime);
    setState(() {});
  }

  String? validateStartDate(String? value) {
    if (!startDateTime.isBefore(endDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  String? validateEndDate(String? value) {
    if (!startDateTime.isBefore(endDateTime)) {
      return 'กรุณาเลือกเวลาให้ถูกต้อง';
    }
    return null;
  }

  save() {
    widget.saveConcertEntity!.showStart = DateFormatUtils.format(startDateTime);
    widget.saveConcertEntity!.showEnd = DateFormatUtils.format(endDateTime);
    //TixNavigate.instance.navigateTo(CreateConcertGenre(),data: widget.saveConcertEntity);
    WrapNavigation.instance
        .pushNamed(context, CreateConcertGenre(), arguments: widget.saveConcertEntity);
  }

  textStyle({double fontSize = 16}) {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: fontSize,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
