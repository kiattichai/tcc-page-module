import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/module/create_concert_module/create_concert_place.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/select_artist.dart';
import 'package:app/module/public_page_module/edit_page_profile/select_genre_screen.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertGenre extends StatefulWidget with TixRoute {
  SaveConcertEntity? saveConcertEntity;

  CreateConcertGenre({this.saveConcertEntity});

  @override
  _CreateConcertGenre createState() => _CreateConcertGenre();

  @override
  String buildPath() {
    return '/create_concert_genre';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreateConcertGenre(saveConcertEntity: data));
  }
}

class _CreateConcertGenre extends State<CreateConcertGenre> {
  List<SaveEditProfilePageAttributes> attributes = [];
  List<GetArtistDataRecord> artists = [];
  bool isActive = false;
  final genre = TextEditingController(text: 'เลือกแนวเพลง');
  final artist = TextEditingController(text: 'ศิลปิน');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'สร้างคอนเสิร์ต',
      ),
      body: SafeArea(
          child: CustomScrollView(slivers: [
        SliverToBoxAdapter(
            child: Container(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: new Text("ศิลปินและแนวเพลง",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff08080a),
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )))),
        SliverToBoxAdapter(
            child: Container(
                padding: const EdgeInsets.only(top: 10, left: 16, right: 16),
                child:
                    new Text("ให้ข้อมูลเพิ่มเติมเกี่ยวกับคอนเสิร์ตนี้ว่าศิลปินคือใคร และเพลงแนวไหน",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        )))),
        SliverToBoxAdapter(
            child: Container(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  autofocus: false,
                  readOnly: true,
                  style: textStyle(),
                  validator: validateGenre,
                  controller: genre,
                  onTap: () => navigateToSelectGenre(context),
                  decoration: InputDecoration(
                      isDense: true,
                      suffixIcon: Icon(
                        Icons.keyboard_arrow_down,
                        size: 25,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(),
                      counterText: ''),
                ))),
        SliverToBoxAdapter(
            child: Container(
          padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
          child: Column(
              children: attributes.map((e) {
            return Row(
              children: [
                Icon(
                  Icons.music_note,
                  size: 20,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  "${e.name}",
                  style: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            );
          }).toList()),
        )),
        SliverToBoxAdapter(
            child: Container(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  autofocus: false,
                  readOnly: true,
                  style: textStyle(),
                  controller: artist,
                  validator: validateArtist,
                  onTap: () => navigateToSelectArtist(context),
                  decoration: InputDecoration(
                      isDense: true,
                      suffixIcon: Icon(
                        Icons.keyboard_arrow_down,
                        size: 25,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(),
                      counterText: ''),
                ))),
        SliverToBoxAdapter(
            child: Container(
          padding: const EdgeInsets.only(top: 5, left: 16, right: 16),
          child: Column(
              children: artists.map((e) {
            return Container(
              padding: const EdgeInsets.only(top: 5),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 15,
                    backgroundImage: NetworkImage(e.image?.url ?? ''),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      "${e.name}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet-Text',
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  )
                ],
              ),
            );
          }).toList()),
        )),
        SliverFillRemaining(
            hasScrollBody: false,
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                  width: double.infinity,
                  height: 42,
                  child: ElevatedButton(
                      child: Text(
                        "ถัดไป",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(21),
                          ))),
                      onPressed: () => isActive ? save() : null),
                )))
      ])),
    );
  }

  navigateToSelectGenre(BuildContext context) async {
    //final result = await TixNavigate.instance.navigateTo(SelectGenreScreen(), data: attributes);
    final result = await WrapNavigation.instance
        .pushNamed(context, SelectGenreScreen(), arguments: attributes);
    if (result is List<SaveEditProfilePageAttributes>) {
      attributes.clear();
      attributes.addAll(result);
      check();
      setState(() {});
    }
  }

  save() {
    widget.saveConcertEntity!.attributes = [
      ...artists
          .map((e) => SaveConcertAttributes()
            ..valueId = e.id
            ..id = 2)
          .toList(),
      ...attributes
          .map((e) => SaveConcertAttributes()
            ..valueId = e.valueId
            ..id = e.id)
          .toList()
    ];

    //TixNavigate.instance.navigateTo(CreateConcertPlace(),data: widget.saveConcertEntity);
    WrapNavigation.instance
        .pushNamed(context, CreateConcertPlace(), arguments: widget.saveConcertEntity);
  }

  check() {
    isActive = attributes.isNotEmpty && artists.isNotEmpty;
  }

  String? validateGenre(String? value) {
    if (attributes.isEmpty) {
      return 'กรุณาเลือกแนวเพลง';
    }
    return null;
  }

  String? validateArtist(String? value) {
    if (artists.isEmpty) {
      return 'กรุณาเลือกศิลปิน';
    }
    return null;
  }

  navigateToSelectArtist(BuildContext context) async {
    //List<GetArtistDataRecord>? result = await TixNavigate.instance.navigateTo(SelectArtist(), data: artists);
    List<GetArtistDataRecord>? result =
        await WrapNavigation.instance.pushNamed(context, SelectArtist(), arguments: artists);
    if (result != null) {
      artists.clear();
      artists.addAll(result);
      check();
      setState(() {});
    }
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
