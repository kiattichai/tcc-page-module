import 'package:app/core/base_view_model.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/model/save_venue_entity.dart';
import 'package:app/module/create_page_module/map_info.dart';
import 'package:app/module/create_page_module/model/get_locally_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AddVenueScreenViewModel extends BaseViewModel {
  final String storeId;
  TextEditingController address = TextEditingController();
  TextEditingController name = TextEditingController();
  LatLng? latLng;
  String? city;
  String? district;
  String? province;
  String? zipcode;
  GetLocallyData? getLocallyData;
  bool isActive = false;

  AddVenueScreenViewModel(this.storeId);

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  void navigateMap(BuildContext context) {
    catchError(() async {
      //var data = await TixNavigate.instance.navigateTo(MapInfo(), data: latLng);
      var data = await WrapNavigation.instance.pushNamed(context, MapInfo(), arguments: latLng);
      if (data is Map) {
        address.text = data['address'] ?? '';
        latLng = data['latLng'];
        province = data['province'] ?? ''; //จังหวัด
        district = data['district'] ?? ''; //ตำบล
        city = data['city'] ?? ''; //อำเภอ
        this.getLocally();
      }
    });
  }

  void getLocally() {
    catchError(() async {
      getLocallyData = await di.locationRepository.getLocally(city!, district!, province!);
      check();
    });
  }

  String? validateName(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อสถานที่';
    }
    if (value.length <= 3) {
      return 'กรุณากรอกชื่อสถานที่มากกว่า 3 ตัวอักษร';
    }
    return null;
  }

  String? validateAddress(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกระบุสถานที่';
    }
  }

  check() {
    print(validateName(name.text));
    isActive = validateName(name.text) == null && getLocallyData != null;
    notifyListeners();
  }

  save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      if (getLocallyData == null) return;
      SaveVenueAddress saveVenueAddress = SaveVenueAddress()
        ..en = ''
        ..th = '';

      SaveVenueName saveVenueName = SaveVenueName()
        ..en = name.text
        ..th = name.text;

      SaveVenueEntity saveVenueEntity = SaveVenueEntity()
        ..address = saveVenueAddress
        ..cityId = getLocallyData?.cityId
        ..countryId = 209
        ..xDefault = false
        ..districtId = getLocallyData?.districtId
        ..lat = latLng?.latitude.toString()
        ..long = latLng?.longitude.toString()
        ..name = saveVenueName
        ..provinceId = getLocallyData?.provinceId
        ..status = true
        ..storeId = storeId
        ..zipCode = getLocallyData?.zipCode;
      final resultVenue = await di.pageRepository.saveVenue(saveVenueEntity);
      WrapNavigation.instance.pop(context, data: resultVenue);
      setLoading(false);
    });
  }
}
