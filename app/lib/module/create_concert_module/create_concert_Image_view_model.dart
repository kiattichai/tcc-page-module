import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_entity.dart';
import 'package:app/module/public_page_module/home_page_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;
import 'package:tix_navigate/tix_navigate.dart';
import '../../../../globals.dart' as globals;

class CreateConcertImageViewModel extends BaseViewModel {
  File? concertImage;
  SaveConcertEntity saveConcertEntity;
  bool isActive = false;

  CreateConcertImageViewModel(this.saveConcertEntity);

  void pickProfileImage() {
    catchError(() async {
      final pickedImage = await ImagePicker().getImage(source: ImageSource.gallery);
      concertImage = pickedImage != null ? File(pickedImage.path) : null;
      check();
      notifyListeners();
    });
  }

  void save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      final bytes = await concertImage!.readAsBytes();
      final b64 = base64.encode(bytes);
      UploadConcertImageEntity uploadConcertImageEntity = UploadConcertImageEntity()
        ..storeId = saveConcertEntity.storeId
        ..status = 'default'
        ..secure = false
        ..files = [
          UploadConcertImageFiles()
            ..position = 0
            ..data = b64
            ..name = p.basename(concertImage!.path)
        ];
      final result = await di.createConcertRepository.saveConcertImage(uploadConcertImageEntity);
      saveConcertEntity.type = "event";
      saveConcertEntity.createdBy = globals.userDetail!.data!.id;
      saveConcertEntity.seoDescription = SaveConcertSeoDescription()
        ..th = ''
        ..en = '';
      saveConcertEntity.seoName = SaveConcertSeoName()
        ..th = ''
        ..en = '';
      saveConcertEntity.images = List.generate(
          result?.uploads?.length ?? 0,
          (index) => SaveConcertImages()
            ..id = result?.uploads![index].id
            ..position = index
            ..tag = 'logo');
      final resultSave = await di.createConcertRepository.saveConcert(saveConcertEntity);

      //TixNavigate.instance.navigateTo(MyHomePage(), data: {'storeId': int.parse(saveConcertEntity.storeId!), 'isOwner': true,'tabIndex':1});
      WrapNavigation.instance.pushNamed(context, MyHomePage(), arguments: {
        'storeId': int.parse(saveConcertEntity.storeId!),
        'isOwner': true,
        'tabIndex': 1
      });

      setLoading(false);
    });
  }

  check() {
    isActive = concertImage != null;
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }
}
