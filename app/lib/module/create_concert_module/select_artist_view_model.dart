import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class SelectArtistViewModel extends BaseViewModel {
  ScrollController scrollController = ScrollController();
  int pageLimit = 10;
  int page = 1;
  bool isComplete = false;

  GetArtistData? artistData;
  List<ArtistSelect> artistList = [];
  List<GetArtistDataRecord> artists = [];

  SelectArtistViewModel(this.artists) {
    scrollController.addListener(() {
      print(page);
      if (scrollController.position.extentAfter == 0 && !isComplete) {
        page++;
        getArtist();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    getArtist();
  }

  @override
  void onError(error) {
    setLoading(false);
    super.onError(error);
  }

  void getArtist() async {
    catchError(() async {
      setLoading(true);
      if (artistData?.pagination?.lastPage != page) {
        isComplete = false;
      } else {
        isComplete = true;
      }
      artistData = await di.pageRepository.getArtists(page, pageLimit);
      if (artistData?.record != null) {
        artistList.addAll(artistData?.record?.map((e) {
              if (artists.where((element) => element.id == e.id).isNotEmpty) {
                return new ArtistSelect(e.id, true, e);
              } else {
                return new ArtistSelect(e.id, false, e);
              }
            }).toList() ??
            []);
      }

      setLoading(false);
    });
  }

  selectArtist(int index) {
    artistList[index].checked = !artistList[index].checked;
    // check();
    notifyListeners();
  }

  save(BuildContext context) {
    List<GetArtistDataRecord> artists =
        artistList.where((element) => element.checked).map((e) => e.value!).toList();
    //TixNavigate.instance.pop(data: artists);
    WrapNavigation.instance.pop(context, data: artists);
  }
}

class ArtistSelect {
  int? id;
  bool checked;
  GetArtistDataRecord? value;

  ArtistSelect(this.id, this.checked, this.value);
}
