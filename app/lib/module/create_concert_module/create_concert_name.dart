import 'package:app/module/create_concert_module/create_concert_time.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreateConcertName extends StatefulWidget with TixRoute {
  String? storeId;
  SaveConcertEntity saveConcertEntity = SaveConcertEntity();

  CreateConcertName({this.storeId});

  @override
  _CreateConcertName createState() => _CreateConcertName();

  @override
  String buildPath() {
    return '/create_concert_name';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreateConcertName(storeId: data['storeId']));
  }
}

class _CreateConcertName extends State<CreateConcertName> {
  TextEditingController concertName = TextEditingController();
  TextEditingController description = TextEditingController();
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'สร้างคอนเสิร์ต',
      ),
      body: SafeArea(
          child: CustomScrollView(slivers: [
        SliverToBoxAdapter(
            child: Container(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: new Text("รายละเอียดคอนเสิร์ต",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff08080a),
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )))),
        SliverToBoxAdapter(
            child: Container(
          padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
          child: TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autofocus: false,
              controller: concertName,
              style: textStyle(),
              validator: validateConcertName,
              decoration: InputDecoration(
                  labelText: 'ชื่อคอนเสิร์ต',
                  border: OutlineInputBorder(),
                  isDense: true,
                  labelStyle: textStyle(),
                  counterText: ''),
              maxLength: 100,
              onChanged: (text) {
                check();
              }),
        )),
        SliverToBoxAdapter(
            child: Container(
          padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
          child: TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autofocus: false,
              maxLines: 4,
              controller: description,
              validator: validateDescription,
              style: textStyle(),
              decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: 'รายละเอียด',
                  border: OutlineInputBorder(),
                  isDense: true,
                  labelStyle: textStyle(),
                  counterText: ''),
              maxLength: 300,
              onChanged: (text) {
                check();
              }),
        )),
        SliverFillRemaining(
            hasScrollBody: false,
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                    width: double.infinity,
                    height: 42,
                    child: ElevatedButton(
                      child: Text(
                        "ถัดไป",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(21),
                          ))),
                      onPressed: () => isActive ? save(context) : null,
                    ))))
      ])),
    );
  }

  String? validateConcertName(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อคอนเสิร์ต';
    }
    if (value.length <= 3) {
      return 'กรุณากรอกชื่อคอนเสิร์ตมากกว่า 3 ตัวอักษร';
    }
    return null;
  }

  String? validateDescription(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกรายละเอียด';
    }
    if (value.length <= 3) {
      return 'กรุณากรอกรายละเอียดมากกว่า 3 ตัวอักษร';
    }
    return null;
  }

  save(BuildContext context) {
    widget.saveConcertEntity.storeId = widget.storeId;
    widget.saveConcertEntity.name = SaveConcertName()
      ..th = concertName.text
      ..en = concertName.text;
    widget.saveConcertEntity.description = SaveConcertDescription()
      ..th = description.text
      ..en = description.text;
    //TixNavigate.instance.navigateTo(CreateConcertTime(),data: widget.saveConcertEntity);
    WrapNavigation.instance
        .pushNamed(context, CreateConcertTime(), arguments: widget.saveConcertEntity);
  }

  check() {
    isActive = validateDescription(concertName.text) == null &&
        validateDescription(description.text) == null;
    setState(() {});
  }

  textStyle() {
    return TextStyle(
      fontFamily: 'SukhumvitSet-Text',
      color: Color(0xff6d6d6d),
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.normal,
    );
  }
}
