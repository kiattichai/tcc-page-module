import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_venue_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:tix_navigate/tix_navigate.dart';

class SelectConcertPlaceViewModel extends BaseViewModel {
  ScrollController scrollController = ScrollController();
  int pageLimit = 10;
  int page = 1;
  bool isComplete = false;
  final String storeId;

  GetVenueData? venueData;
  int? selectedId;
  GetVenueDataRecord? venueSelected;
  List<GetVenueDataRecord> venueList = [];

  SelectConcertPlaceViewModel(this.storeId, {this.selectedId}) {
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0 && !isComplete) {
        page++;
        getVenue();
      }
    });
  }

  @override
  void postInit() {
    super.postInit();
    getVenue();
  }

  @override
  void onError(error) {
    super.onError(error);
  }

  void getVenue() async {
    catchError(() async {
      setLoading(true);
      if (venueData?.pagination?.lastPage != page) {
        isComplete = false;
      } else {
        isComplete = true;
      }
      venueData = await di.pageRepository.getVenue(storeId, page, pageLimit);
      if (venueData?.record != null) {
        venueList.addAll(venueData?.record ?? []);
      }
      if (selectedId != null) {
        print(selectedId);
        venueSelected = venueList.firstWhere((element) => element.id == selectedId);
      }

      setLoading(false);
    });
  }

  selectVenue(int index) {
    venueSelected = venueList[index];
    notifyListeners();
  }

  save(BuildContext context) {
    //TixNavigate.instance.pop(data: venueSelected);
    WrapNavigation.instance.pop(context, data: venueSelected);
  }
}
