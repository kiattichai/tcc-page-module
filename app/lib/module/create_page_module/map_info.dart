import 'dart:async';

import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/create_page_module/model/get_geocode_entity.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/create_page_module/model/page_info_model.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/utils/wrap_navigation.dart';

import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tix_navigate/tix_navigate.dart';

class MapInfo extends StatefulWidget with TixRoute {
  LatLng? latLng;

  MapInfo({this.latLng});

  @override
  _MapInfo createState() => _MapInfo();

  @override
  String buildPath() {
    return '/map_info';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => MapInfo(
              latLng: data,
            ));
  }
}

class _MapInfo extends BaseStateProvider<MapInfo, MapInfoViewModel> {
  @override
  void initState() {
    viewModel = MapInfoViewModel(widget.latLng);
    viewModel.showAlertError = showAlertError;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MapInfoViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'ค้นหา',
                ),
                body: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: viewModel.cameraPosition,
                  onMapCreated: viewModel.complete(context),
                  onTap: viewModel.tap,
                  myLocationEnabled: (viewModel.permission == LocationPermission.always ||
                          viewModel.permission == LocationPermission.whileInUse)
                      ? true
                      : false,
                  markers: Set.from(viewModel.markers),
                ),
                floatingActionButton: FloatingActionButton.extended(
                  onPressed: () => viewModel.markers.length > 0
                      ? viewModel.save(viewModel.markers[0], context)
                      : null,
                  backgroundColor: Color(0xffda3534),
                  label: new Text("เลือกตำแหน่ง",
                      style: TextStyle(
                        fontFamily: 'SukhumvitSet',
                        color: Color(0xffffffff),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      )),
                  icon: Icon(Icons.where_to_vote),
                ),
                floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
              ));
        },
        model: viewModel);
  }

  Future<bool> showAlertError(BaseError baseError) async {
    if (showing == true) return false;
    showing = true;
    bool result = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: baseError.message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
    return result;
  }
}

class MapInfoViewModel extends BaseViewModel {
  GetGeocodeEntity? getGeocode;
  String? address;
  LatLng? latLng;
  Completer<GoogleMapController> _controller = Completer();
  MarkerId markerId = MarkerId("marker");
  List<Marker> markers = [];
  BitmapDescriptor? customIcon;
  CameraPosition cameraPosition = CameraPosition(
    target: LatLng(13.7650413, 100.5371795),
    zoom: 14.4746,
  );
  bool? serviceEnabled;
  LocationPermission? permission;

  MapInfoViewModel(this.latLng);

  @override
  void postInit() {
    super.postInit();
    catchError(() async {
      customIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(size: Size(14, 14)), 'assets/icon_marker.png');
      await checkPermission();
      if (latLng == null) {
        if (permission == LocationPermission.denied ||
            permission == LocationPermission.deniedForever) {
          markers = [
            Marker(
                markerId: markerId,
                position: LatLng(13.7650413, 100.5371795),
                draggable: true,
                icon: customIcon ?? BitmapDescriptor.defaultMarker,
                onDragEnd: (null))
          ];
          notifyListeners();
        } else {
          _getCurrentLocation();
        }
      } else {
        _markedLocation(latLng!);
      }
    });
  }

  void tap(LatLng point) {
    catchError(() async {
      markers = [
        Marker(
          markerId: markerId,
          position: point,
          draggable: true,
          icon: customIcon ?? BitmapDescriptor.defaultMarker,
        )
      ];
      notifyListeners();
    });
  }

  void _markedLocation(LatLng latLng) {
    catchError(() async {
      cameraPosition = CameraPosition(
        target: LatLng(latLng.latitude, latLng.longitude),
        zoom: 15.5,
      );
      markers = [
        Marker(
          markerId: markerId,
          position: LatLng(latLng.latitude, latLng.longitude),
          draggable: true,
          icon: customIcon ?? BitmapDescriptor.defaultMarker,
        )
      ];
      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      notifyListeners();
    });
  }

  checkPermission() async {
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }
  }

  _getCurrentLocation() {
    catchError(() async {
      Position position =
          await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

      cameraPosition = CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: 15.5,
      );

      markers = [
        Marker(
          markerId: markerId,
          position: LatLng(position.latitude, position.longitude),
          draggable: true,
          icon: customIcon ?? BitmapDescriptor.defaultMarker,
        )
      ];

      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      notifyListeners();
    });
  }

  complete(BuildContext context) async {
    GoogleMapController? controller;
    try {
      _controller.complete(controller);
    } catch (e) {
      bool result = await showAlertError!(
          BaseError()..message = 'ไม่สามารถเปิดแผนที่ได้ กรุณาตรวจสอบอินเทอร์เน็ต');
      if (result) {
        //TixNavigate.instance.pop();
        WrapNavigation.instance.pop(context);
      }
    }
  }

  save(Marker marker, BuildContext context) {
    catchError(() async {
      getGeocode =
          await di.locationRepository.geoCode(marker.position.latitude, marker.position.longitude);
      address = getGeocode?.results?.first.formattedAddress;
      final addressComponent = getGeocode?.results?.first.addressComponents;

      final province = addressComponent![addressComponent.length - 3].longName;
      final city = addressComponent[addressComponent.length - 4].longName;
      final district = addressComponent[addressComponent.length - 5].longName;
      // TixNavigate.instance.pop(data: {
      //   'address': address,
      //   'province': province,
      //   'city': city?.replaceAll('อำเภอ', '').replaceAll('เขต', '').trim(),
      //   'district': district?.replaceAll('ตำบล', '').replaceAll('แขวง', '').trim(),
      //   'latLng': LatLng(marker.position.latitude, marker.position.longitude)
      // });

      WrapNavigation.instance.pop(context, data: {
        'address': address,
        'province': province,
        'city': city?.replaceAll('อำเภอ', '').replaceAll('เขต', '').trim(),
        'district': district?.replaceAll('ตำบล', '').replaceAll('แขวง', '').trim(),
        'latLng': LatLng(marker.position.latitude, marker.position.longitude)
      });
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      showAlertError!(error);
    }
  }

  Future<bool> Function(BaseError)? showAlertError;
}
