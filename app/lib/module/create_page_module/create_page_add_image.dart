import 'dart:io';

import 'package:app/app/app_state.dart';
import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/main.dart';
import 'package:app/model/create_page_entity.dart';
import 'package:app/module/create_page_module/create_page_view_model.dart';
import 'package:app/module/public_page_module/home_page_viewmodel.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tix_navigate/tix_navigate.dart';

enum FileType {
  profileImageFile,
  bannerImageFile
}

class CreatePageAddImage extends StatefulWidget with TixRoute {

  CreatePageEntity? createPage;


  CreatePageAddImage({this.createPage});

  @override
  _CreatePageAddImage createState() => _CreatePageAddImage();

  @override
  String buildPath() {
    return '/create_page_add_image';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => CreatePageAddImage(createPage: data,));
  }
}

class _CreatePageAddImage
    extends BaseStateProvider<CreatePageAddImage, CreatePageViewModel> {


  @override
  void initState() {
    viewModel = CreatePageViewModel(widget.createPage);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreatePageViewModel>(
      model: viewModel,
        builder: (context, model, child) {
          return ProgressHUD(
              opacity: 1.0,
              color: Colors.white,
              progressIndicator: CircleLoading(),
              inAsyncCall: viewModel.loading,
              child: Scaffold(
                appBar: AppBarWidget(
                  name: 'เพิ่มรูปภาพในเพจนี้',
                ),
                body: SafeArea(
                    child: CustomScrollView(slivers: [
                      SliverToBoxAdapter(
                          child: Container(
                              padding: const EdgeInsets.only(
                                  top: 23, left: 16, right: 16),
                              child: Center(
                                  child: Text(
                                    "ใช้รูปภาพที่แสดงตัวตนของเพจ เช่น โลโก้ รูปภาพเหล่านี้จะปรากฏในหน้าเพจและผลการค้นหา",
                                    style: TextStyle(
                                      fontFamily: 'SukhumvitSet-Text',
                                      color: Color(0xff6d6d6d),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    ),
                                    textAlign: TextAlign.center,
                                  )))),
                      SliverToBoxAdapter(
                          child: Container(
                            padding: const EdgeInsets.only(top: 49),
                            child: Stack(
                              children: [
                                InkWell(
                                  child: Container(
                                    width: double.infinity,
                                    height: 211,
                                    decoration: new BoxDecoration(
                                        color: Color(0xfff1f2f7)),
                                    child: viewModel.bannerImageFile == null
                                        ? Padding(
                                      padding: EdgeInsets.only(top: 23),
                                      child: Text(
                                        "เพิ่มรูปหน้าปก",
                                        style: TextStyle(
                                          fontFamily: 'SukhumvitSet-Text',
                                          color: Color(0xff08080a),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    )
                                        : Image.file(
                                      viewModel.bannerImageFile!,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  onTap: () {
                                    viewModel.pickImage();
                                  },
                                ),
                                viewModel.profileImageFile == null
                                    ? Container(
                                    margin: EdgeInsets.only(
                                        top: 147.5,
                                        left: ((Screen.width / 2) - 63.5)),
                                    width: 127,
                                    height: 127,
                                    decoration: BoxDecoration(
                                        color: Color(0xfff1f2f7),
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: Colors.white, width: 2)),
                                    child: InkWell(
                                      child: Center(
                                          child: Text("เพิ่มรูปโปรไฟล์",
                                              style: TextStyle(
                                                fontFamily: 'SukhumvitSet-Text',
                                                color: Color(0xff08080a),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                fontStyle: FontStyle.normal,
                                              ))),
                                      onTap: () {
                                        viewModel.pickProfileImage();
                                      },
                                    ))
                                    : Container(
                                  margin: EdgeInsets.only(
                                      top: 147.5,
                                      left: ((Screen.width / 2) - 63.5)),
                                  width: 127,
                                  height: 127,
                                  decoration: BoxDecoration(

                                      color: Color(0xfff1f2f7),
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: FileImage(viewModel.profileImageFile!),
                                          fit: BoxFit.cover),
                                      border:
                                      Border.all(
                                          color: Colors.white, width: 2,)),
                                  child: InkWell(
                                    onTap: () {
                                     viewModel.pickProfileImage();
                                    },
                                  ),
                                )
                              ],
                            ),
                          )),
                      SliverFillRemaining(
                          hasScrollBody: false,
                          child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: const EdgeInsets.only(
                                    top: 20, left: 16, right: 16, bottom: 24),
                                width: double.infinity,
                                height: 42,
                                child: ElevatedButton(
                                    child: Text(
                                      "เรียบร้อย",
                                      style: TextStyle(
                                        fontFamily: 'SukhumvitSet-Text',
                                        color: Color(0xffffffff),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    style: ButtonStyle(
                                        foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                        backgroundColor: MaterialStateProperty
                                            .all<Color>(
                                            Color(0xffda3534)),
                                        shape:
                                        MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius
                                                  .circular(21),
                                            ))),
                                    onPressed: () => viewModel.save(context)),
                              )))
                    ])),
              )

          );
        }
    );
  }


}
