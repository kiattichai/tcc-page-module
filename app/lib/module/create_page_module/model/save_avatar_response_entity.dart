import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_avatar_response_entity.g.dart';


@JsonSerializable()
class SaveAvatarResponseEntity {

	SaveAvatarResponseEntity();

	factory SaveAvatarResponseEntity.fromJson(Map<String, dynamic> json) => $SaveAvatarResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $SaveAvatarResponseEntityToJson(this);

	SaveAvatarResponseData? data;
}

@JsonSerializable()
class SaveAvatarResponseData {

	SaveAvatarResponseData();

	factory SaveAvatarResponseData.fromJson(Map<String, dynamic> json) => $SaveAvatarResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $SaveAvatarResponseDataToJson(this);

	String? message;
	String? image;
}
