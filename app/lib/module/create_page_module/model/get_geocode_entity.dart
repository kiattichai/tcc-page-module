import 'package:app/generated/json/get_geocode_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetGeocodeEntity {
  GetGeocodeEntity();

  factory GetGeocodeEntity.fromJson(Map<String, dynamic> json) =>
      $GetGeocodeEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetGeocodeEntityToJson(this);

  @JSONField(name: "plus_code")
  GetGeocodePlusCode? plusCode;
  List<GetGeocodeResults>? results;
  String? status;
}

@JsonSerializable()
class GetGeocodePlusCode {
  GetGeocodePlusCode();

  factory GetGeocodePlusCode.fromJson(Map<String, dynamic> json) =>
      $GetGeocodePlusCodeFromJson(json);

  Map<String, dynamic> toJson() => $GetGeocodePlusCodeToJson(this);

  @JSONField(name: "compound_code")
  String? compoundCode;
  @JSONField(name: "global_code")
  String? globalCode;
}

@JsonSerializable()
class GetGeocodeResults {
  GetGeocodeResults();

  factory GetGeocodeResults.fromJson(Map<String, dynamic> json) =>
      $GetGeocodeResultsFromJson(json);

  Map<String, dynamic> toJson() => $GetGeocodeResultsToJson(this);

  @JSONField(name: "address_components")
  List<GetGeocodeResultsAddressComponents>? addressComponents;
  @JSONField(name: "formatted_address")
  String? formattedAddress;
  GetGeocodeResultsGeometry? geometry;
  @JSONField(name: "place_id")
  String? placeId;
  @JSONField(name: "plus_code")
  GetGeocodeResultsPlusCode? plusCode;
  List<String>? types;
}

@JsonSerializable()
class GetGeocodeResultsAddressComponents {
  GetGeocodeResultsAddressComponents();

  factory GetGeocodeResultsAddressComponents.fromJson(
          Map<String, dynamic> json) =>
      $GetGeocodeResultsAddressComponentsFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGeocodeResultsAddressComponentsToJson(this);

  @JSONField(name: "long_name")
  String? longName;
  @JSONField(name: "short_name")
  String? shortName;
  List<String>? types;
}

@JsonSerializable()
class GetGeocodeResultsGeometry {
  GetGeocodeResultsGeometry();

  factory GetGeocodeResultsGeometry.fromJson(Map<String, dynamic> json) =>
      $GetGeocodeResultsGeometryFromJson(json);

  Map<String, dynamic> toJson() => $GetGeocodeResultsGeometryToJson(this);

  GetGeocodeResultsGeometryLocation? location;
  @JSONField(name: "location_type")
  String? locationType;
  GetGeocodeResultsGeometryViewport? viewport;
}

@JsonSerializable()
class GetGeocodeResultsGeometryLocation {
  GetGeocodeResultsGeometryLocation();

  factory GetGeocodeResultsGeometryLocation.fromJson(
          Map<String, dynamic> json) =>
      $GetGeocodeResultsGeometryLocationFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGeocodeResultsGeometryLocationToJson(this);

  double? lat;
  double? lng;
}

@JsonSerializable()
class GetGeocodeResultsGeometryViewport {
  GetGeocodeResultsGeometryViewport();

  factory GetGeocodeResultsGeometryViewport.fromJson(
          Map<String, dynamic> json) =>
      $GetGeocodeResultsGeometryViewportFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGeocodeResultsGeometryViewportToJson(this);

  GetGeocodeResultsGeometryViewportNortheast? northeast;
  GetGeocodeResultsGeometryViewportSouthwest? southwest;
}

@JsonSerializable()
class GetGeocodeResultsGeometryViewportNortheast {
  GetGeocodeResultsGeometryViewportNortheast();

  factory GetGeocodeResultsGeometryViewportNortheast.fromJson(
          Map<String, dynamic> json) =>
      $GetGeocodeResultsGeometryViewportNortheastFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGeocodeResultsGeometryViewportNortheastToJson(this);

  double? lat;
  double? lng;
}

@JsonSerializable()
class GetGeocodeResultsGeometryViewportSouthwest {
  GetGeocodeResultsGeometryViewportSouthwest();

  factory GetGeocodeResultsGeometryViewportSouthwest.fromJson(
          Map<String, dynamic> json) =>
      $GetGeocodeResultsGeometryViewportSouthwestFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGeocodeResultsGeometryViewportSouthwestToJson(this);

  double? lat;
  double? lng;
}

@JsonSerializable()
class GetGeocodeResultsPlusCode {
  GetGeocodeResultsPlusCode();

  factory GetGeocodeResultsPlusCode.fromJson(Map<String, dynamic> json) =>
      $GetGeocodeResultsPlusCodeFromJson(json);

  Map<String, dynamic> toJson() => $GetGeocodeResultsPlusCodeToJson(this);

  @JSONField(name: "compound_code")
  String? compoundCode;
  @JSONField(name: "global_code")
  String? globalCode;
}
