import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PageInfo {

  String? phone;
  String? email;
  String? address;
  LatLng? latLng;



  PageInfo({this.phone, this.email, this.address,this.latLng});
}