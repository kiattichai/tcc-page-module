import 'package:app/generated/json/save_page_avatar_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SavePageAvatarEntity {
  SavePageAvatarEntity();

  factory SavePageAvatarEntity.fromJson(Map<String, dynamic> json) =>
      $SavePageAvatarEntityFromJson(json);

  Map<String, dynamic> toJson() => $SavePageAvatarEntityToJson(this);

  @JSONField(name: "file_name")
  String? fileName;
  @JSONField(name: "file_data")
  String? fileData;
}
