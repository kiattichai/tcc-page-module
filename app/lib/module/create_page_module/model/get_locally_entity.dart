import 'package:app/generated/json/get_locally_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetLocallyEntity {
  GetLocallyEntity();

  factory GetLocallyEntity.fromJson(Map<String, dynamic> json) =>
      $GetLocallyEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetLocallyEntityToJson(this);

  GetLocallyData? data;
  GetLocallyBench? bench;
}

@JsonSerializable()
class GetLocallyData {
  GetLocallyData();

  factory GetLocallyData.fromJson(Map<String, dynamic> json) =>
      $GetLocallyDataFromJson(json);

  Map<String, dynamic> toJson() => $GetLocallyDataToJson(this);

  @JSONField(name: "province_id")
  int? provinceId;
  @JSONField(name: "city_id")
  int? cityId;
  @JSONField(name: "district_id")
  int? districtId;
  @JSONField(name: "zip_code")
  String? zipCode;
  bool? cache;
}

@JsonSerializable()
class GetLocallyBench {
  GetLocallyBench();

  factory GetLocallyBench.fromJson(Map<String, dynamic> json) =>
      $GetLocallyBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetLocallyBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
