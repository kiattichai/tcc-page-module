import 'package:app/generated/json/create_page_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class CreatePageResponseEntity {
  CreatePageResponseEntity();

  factory CreatePageResponseEntity.fromJson(Map<String, dynamic> json) =>
      $CreatePageResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $CreatePageResponseEntityToJson(this);

  CreatePageResponseData? data;
}

@JsonSerializable()
class CreatePageResponseData {
  CreatePageResponseData();

  factory CreatePageResponseData.fromJson(Map<String, dynamic> json) =>
      $CreatePageResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $CreatePageResponseDataToJson(this);

  @JSONField(name: "display_name")
  String? displayName;
  String? category;
  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "group_id")
  int? groupId;
  @JSONField(name: "store_id")
  int? storeId;
  int? type;
  int? id;
}
