import 'package:app/generated/json/get_auto_complete_location_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAutoCompleteLocationEntity {
  GetAutoCompleteLocationEntity();

  factory GetAutoCompleteLocationEntity.fromJson(Map<String, dynamic> json) =>
      $GetAutoCompleteLocationEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAutoCompleteLocationEntityToJson(this);

  List<GetAutoCompleteLocationPredictions>? predictions;
  String? status;
}

@JsonSerializable()
class GetAutoCompleteLocationPredictions {
  GetAutoCompleteLocationPredictions();

  factory GetAutoCompleteLocationPredictions.fromJson(
          Map<String, dynamic> json) =>
      $GetAutoCompleteLocationPredictionsFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAutoCompleteLocationPredictionsToJson(this);

  String? description;
  @JSONField(name: "matched_substrings")
  List<GetAutoCompleteLocationPredictionsMatchedSubstrings>? matchedSubstrings;
  @JSONField(name: "place_id")
  String? placeId;
  String? reference;
  @JSONField(name: "structured_formatting")
  GetAutoCompleteLocationPredictionsStructuredFormatting? structuredFormatting;
  List<GetAutoCompleteLocationPredictionsTerms>? terms;
  List<String>? types;
}

@JsonSerializable()
class GetAutoCompleteLocationPredictionsMatchedSubstrings {
  GetAutoCompleteLocationPredictionsMatchedSubstrings();

  factory GetAutoCompleteLocationPredictionsMatchedSubstrings.fromJson(
          Map<String, dynamic> json) =>
      $GetAutoCompleteLocationPredictionsMatchedSubstringsFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAutoCompleteLocationPredictionsMatchedSubstringsToJson(this);

  int? length;
  int? offset;
}

@JsonSerializable()
class GetAutoCompleteLocationPredictionsStructuredFormatting {
  GetAutoCompleteLocationPredictionsStructuredFormatting();

  factory GetAutoCompleteLocationPredictionsStructuredFormatting.fromJson(
          Map<String, dynamic> json) =>
      $GetAutoCompleteLocationPredictionsStructuredFormattingFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAutoCompleteLocationPredictionsStructuredFormattingToJson(this);

  @JSONField(name: "main_text")
  String? mainText;
  @JSONField(name: "main_text_matched_substrings")
  List<GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings>?
      mainTextMatchedSubstrings;
  @JSONField(name: "secondary_text")
  String? secondaryText;
}

@JsonSerializable()
class GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings {
  GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings();

  factory GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings.fromJson(
          Map<String, dynamic> json) =>
      $GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstringsFromJson(
          json);

  Map<String, dynamic> toJson() =>
      $GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstringsToJson(
          this);

  int? length;
  int? offset;
}

@JsonSerializable()
class GetAutoCompleteLocationPredictionsTerms {
  GetAutoCompleteLocationPredictionsTerms();

  factory GetAutoCompleteLocationPredictionsTerms.fromJson(
          Map<String, dynamic> json) =>
      $GetAutoCompleteLocationPredictionsTermsFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetAutoCompleteLocationPredictionsTermsToJson(this);

  int? offset;
  String? value;
}
