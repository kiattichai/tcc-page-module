import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/core/base_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/get_auto_complete_location_entity.dart';

class AutocompleteLocation extends StatefulWidget{

  @override
  _AutocompleteLocation createState()=>_AutocompleteLocation();

}

class _AutocompleteLocation extends BaseStateProvider<AutocompleteLocation, AutocompleteLocationViewModel>{

  static String _displayStringForOption(GetAutoCompleteLocationPredictions option) => option.description ?? '';

  @override
  void initState() {
    viewModel = AutocompleteLocationViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AutocompleteLocationViewModel>(
        builder: (context, model, child) {
          return Autocomplete<GetAutoCompleteLocationPredictions>(
            displayStringForOption: _displayStringForOption,
            fieldViewBuilder: (
                BuildContext context,
                TextEditingController fieldTextEditingController,
                FocusNode fieldFocusNode,
                VoidCallback onFieldSubmitted
                ){
              return TextFormField(
                controller: fieldTextEditingController,
                focusNode: fieldFocusNode,
                onChanged: (String value){
                  viewModel.getAutoComplete(value);
                },
                style: const TextStyle(fontWeight: FontWeight.bold),
              );

            },
            optionsBuilder: (TextEditingValue textEditingValue) {
              if (textEditingValue.text == '') {
                return const Iterable<GetAutoCompleteLocationPredictions>.empty();
              }


              return [];
            },
            onSelected: (GetAutoCompleteLocationPredictions selection) {
              print('You just selected ${_displayStringForOption(selection)}');
            },
          );

        },model: viewModel,);
  }

}


class AutocompleteLocationViewModel extends BaseViewModel {

  GetAutoCompleteLocationEntity? getAutoCompleteLocationEntity;

  void getAutoComplete(String term)  {
    catchError(() async {
      getAutoCompleteLocationEntity = await di.locationRepository.searchLocation(term);
      print(getAutoCompleteLocationEntity?.predictions?.length);
    });
  }

}