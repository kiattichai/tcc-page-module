import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/model/create_page_entity.dart';
import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:app/module/create_page_module/model/save_page_avatar_entity.dart';
import 'package:app/module/public_page_module/home_page_screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;
import 'package:tix_navigate/tix_navigate.dart';

class CreatePageViewModel extends BaseViewModel {
  File? profileImageFile;
  File? bannerImageFile;
  CreatePageEntity? createPage;

  CreatePageViewModel(this.createPage);

  void pickImage() {
    catchError(() async {
      final pickedImage = await ImagePicker().getImage(source: ImageSource.gallery);
      bannerImageFile = pickedImage != null ? File(pickedImage.path) : null;
      if (bannerImageFile != null) {
        cropImage(FileType.bannerImageFile);
      }
    });
  }

  void pickProfileImage() {
    catchError(() async {
      final pickedImage = await ImagePicker().getImage(source: ImageSource.gallery);
      profileImageFile = pickedImage != null ? File(pickedImage.path) : null;
      if (profileImageFile != null) {
        cropImage(FileType.profileImageFile);
      }
    });
  }

  void cropImage(FileType type) {
    catchError(() async {
      File? croppedFile = await ImageCropper.cropImage(
          sourcePath:
              FileType.bannerImageFile == type ? bannerImageFile!.path : profileImageFile!.path,
          aspectRatioPresets: [],
          aspectRatio: type == FileType.profileImageFile
              ? CropAspectRatio(ratioX: 1, ratioY: 1)
              : CropAspectRatio(ratioX: 16, ratioY: 9),
          androidUiSettings: AndroidUiSettings(
              hideBottomControls: true,
              toolbarTitle: 'ครอบตัด',
              toolbarColor: Colors.white,
              toolbarWidgetColor: Colors.black,
              lockAspectRatio: true),
          iosUiSettings: IOSUiSettings(
              title: 'ครอบตัด', aspectRatioLockEnabled: true, aspectRatioPickerButtonHidden: true));
      if (croppedFile != null) {
        if (FileType.bannerImageFile == type) {
          bannerImageFile = croppedFile;
        }
        if (FileType.profileImageFile == type) {
          profileImageFile = croppedFile;
        }

        notifyListeners();
      }
    });
  }

  void save(BuildContext context) {
    catchError(() async {
      setLoading(true);
      var result = await di.pageRepository.createPage(createPage!);
      if (result != null) {
        if (bannerImageFile != null) {
          final bytes = await bannerImageFile!.readAsBytes();
          final b64 = base64.encode(bytes);
          SavePageAvatarEntity savePageAvatar = SavePageAvatarEntity()
            ..fileName = p.basename(bannerImageFile!.path)
            ..fileData = b64;
          await di.pageRepository.updateCoverPage(result.id!, savePageAvatar);
        }
        if (profileImageFile != null) {
          final bytes = await profileImageFile!.readAsBytes();
          final b64 = base64.encode(bytes);
          SavePageAvatarEntity savePageAvatar = SavePageAvatarEntity()
            ..fileName = p.basename(profileImageFile!.path)
            ..fileData = b64;
          await di.pageRepository.updateAvatarPage(result.id!, savePageAvatar);
        }
        //TixNavigate.instance.navigateTo(MyHomePage(),data: {'storeId':result.storeId,'isOwner':true});
        WrapNavigation.instance.pushNamed(context, MyHomePage(),
            arguments: {'storeId': result.storeId, 'isOwner': true});
      }
      setLoading(false);
    });
  }

  @override
  void postInit() {
    super.postInit();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}
