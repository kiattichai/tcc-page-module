import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:app/module/create_page_module/model/page_info_model.dart';
import 'package:app/module/create_page_module/map_info.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreatePageInfo extends StatefulWidget with TixRoute {
  PageInfo? pageInfo;

  CreatePageInfo({this.pageInfo});

  @override
  _CreatePageInfo createState() => _CreatePageInfo();

  @override
  String buildPath() {
    return '/create_page_info';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreatePageInfo(pageInfo: data));
  }
}

class _CreatePageInfo extends State<CreatePageInfo> {
  bool isActive = false;
  final phone = TextEditingController();
  final email = TextEditingController();
  final address = TextEditingController();

  @override
  void initState() {
    phone.text = widget.pageInfo?.phone ?? '';
    email.text = widget.pageInfo?.email ?? '';
    address.text = widget.pageInfo?.address ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'ข้อมูลติดต่อ',
      ),
      body: SafeArea(
        child: CustomScrollView(slivers: [
          SliverToBoxAdapter(
              child: Container(
            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
            child: TextFormField(
              keyboardType: TextInputType.number,
              controller: phone,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autofocus: false,
              maxLength: 10,
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff6d6d6d),
                fontSize: 16,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
              ),
              onChanged: (text) {
                setState(() {
                  check();
                });
              },
              decoration: InputDecoration(
                  labelText: 'เบอร์โทรศัพท์',
                  isDense: true,
                  labelStyle: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff6d6d6d),
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                  border: OutlineInputBorder(),
                  counterText: ''),
              validator: validatePhoneNumber,
            ),
          )),
          SliverToBoxAdapter(
              child: Container(
            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
            child: TextFormField(
              autofocus: false,
              controller: email,
              keyboardType: TextInputType.emailAddress,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              maxLength: 50,
              style: TextStyle(
                fontFamily: 'SukhumvitSet-Text',
                color: Color(0xff6d6d6d),
                fontSize: 16,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
              ),
              onChanged: (text) {
                setState(() {
                  check();
                });
              },
              decoration: InputDecoration(
                  labelText: 'อีเมล',
                  isDense: true,
                  labelStyle: TextStyle(
                    fontFamily: 'SukhumvitSet-Text',
                    color: Color(0xff6d6d6d),
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                  border: OutlineInputBorder(),
                  counterText: ''),
              validator: validateEmail,
            ),
          )),
          SliverToBoxAdapter(
              child: Container(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    controller: address,
                    maxLength: 200,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                    onChanged: (text) {
                      setState(() {
                        check();
                      });
                    },
                    onTap: () => _navigateMap(),
                    decoration: InputDecoration(
                        labelText: 'ตำแหน่งที่ตั้ง',
                        isDense: true,
                        labelStyle: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        ),
                        border: OutlineInputBorder(),
                        counterText: ''),
                    validator: validateAddress,
                  ))),
          SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                    width: double.infinity,
                    height: 42,
                    child: ElevatedButton(
                        child: Text(
                          "ถัดไป",
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        style: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(21),
                            ))),
                        onPressed: () => isActive ? _save() : null),
                  )))
        ]),
      ),
    );
  }

  String? validatePhoneNumber(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกเบอร์โทรศัพท์';
    }
    if (value.length < 10) {
      return 'กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก';
    }
    return null;
  }

  String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกอีเมล';
    }
    if (!EmailValidator.validate(value)) {
      return 'กรุณากรอกอีเมลให้ถูกต้อง';
    }
    return null;
  }

  void check() {
    isActive = ((validatePhoneNumber(phone.text) == null) &&
        (validateEmail(email.text) == null) &&
        (validateAddress(address.text) == null) &&
        (widget.pageInfo?.latLng != null));
  }

  String? validateAddress(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกตำแหน่งที่ตั้ง';
    }
    return null;
  }

  void _save() {
    //TixNavigate.instance.navigateTo(CreatePageAddImage());
    WrapNavigation.instance.pushNamed(context, CreatePageAddImage());
  }

  void _navigateMap() {
    widget.pageInfo?.address = phone.text;
    widget.pageInfo?.email = email.text;
    setState(() async {
      //var data = await TixNavigate.instance.navigateTo(MapInfo(), data: widget.pageInfo?.latLng);
      var data = await WrapNavigation.instance
          .pushNamed(context, MapInfo(), arguments: widget.pageInfo?.latLng);
      if (data is Map) {
        widget.pageInfo?.address = data['address'] ?? '';
        address.text = data['address'] ?? '';
        widget.pageInfo?.latLng = data['latLng'];
      }

      setState(() {
        check();
      });
    });
  }
}
