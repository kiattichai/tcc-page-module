import 'package:app/model/create_page_entity.dart';
import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreatePageType extends StatefulWidget with TixRoute {
  CreatePageEntity? createPage;

  CreatePageType({this.createPage});

  @override
  String buildPath() => 'create_page_type';

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreatePageType(createPage: data));
  }

  @override
  _CreatePageType createState() => _CreatePageType();
}

class _CreatePageType extends State<CreatePageType> {
  var types = [
    {'name': 'ผับ/บาร์', 'value': 'nightclub'},
    {'name': 'ผู้จัด', 'value': 'organizer'},
    {'name': 'ค่ายเพลง', 'value': 'record_label'}
  ];
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        name: 'ข้อมูลติดต่อ',
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
                child: Container(
                    padding: const EdgeInsets.only(top: 25, left: 16, right: 16),
                    child: Center(
                      child: new Text("เลือกประเภทของเพจ เพื่อเข้าถึงกลุ่มของผู้ใช้มากขึ้น",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'SukhumvitSet-Text',
                            color: Color(0xff6d6d6d),
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ))),
            SliverToBoxAdapter(
                child: Container(
              padding: const EdgeInsets.only(top: 27, left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: types.map((e) {
                  if (e['value'] == widget.createPage?.category) {
                    return ElevatedButton(
                        child: Text(e['name'] ?? '',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xffda3534),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                        onPressed: () => select(e['value'] ?? ''),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(14.5),
                                    side: BorderSide(color: Color(0xffda3534))))));
                  } else {
                    return ElevatedButton(
                        child: Text(e['name'] ?? '',
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: Color(0xff08080a),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                        onPressed: () => select(e['value'] ?? ''),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xfff1f2f7)),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14.5),
                            ))));
                  }
                }).toList(),
              ),
            )),
            SliverFillRemaining(
                hasScrollBody: false,
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                      width: double.infinity,
                      height: 42,
                      child: ElevatedButton(
                          child: Text(
                            "ถัดไป",
                            style: TextStyle(
                              fontFamily: 'SukhumvitSet-Text',
                              color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(21),
                              ))),
                          onPressed: () => isActive ? _save() : null),
                    )))
          ],
        ),
      ),
    );
  }

  select(String e) {
    setState(() {
      if (this.widget.createPage?.category == e) {
        this.widget.createPage?.category = null;
      } else {
        this.widget.createPage?.category = e;
      }
      widget.createPage?.category = e;
      this.check();
    });
  }

  check() {
    isActive = this.widget.createPage?.category != null;
  }

  void _save() {
    //TixNavigate.instance.navigateTo(CreatePageAddImage(),data:this.widget.createPage);
    WrapNavigation.instance
        .pushNamed(context, CreatePageAddImage(), arguments: this.widget.createPage);
  }
}
