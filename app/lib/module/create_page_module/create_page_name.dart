import 'package:app/model/create_page_entity.dart';
import 'package:app/module/create_page_module/model/page_info_model.dart';
import 'package:app/module/create_page_module/create_page_type.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CreatePageName extends StatefulWidget with TixRoute {
  @override
  _CreatePageName createState() => _CreatePageName();

  @override
  String buildPath() {
    return '/create_page_name';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CreatePageName());
  }
}

class _CreatePageName extends State<CreatePageName> {
  final pageName = TextEditingController();
  CreatePageEntity createPage = CreatePageEntity();
  int pageNameLength = 0;
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          name: 'สร้างเพจของคุณ',
        ),
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                    padding: const EdgeInsets.only(top: 64),
                    child: Center(
                      child: Image(
                        width: 231,
                        height: 194,
                        image: AssetImage("assets/page.png"),
                      ),
                    )),
              ),
              SliverToBoxAdapter(
                  child: Container(
                padding: const EdgeInsets.only(top: 40, left: 16, right: 16),
                child: TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    autofocus: false,
                    controller: pageName,
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                    decoration: InputDecoration(
                        labelText: 'ชื่อเพจ',
                        border: OutlineInputBorder(),
                        isDense: true,
                        labelStyle: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        ),
                        suffixText: "$pageNameLength / 50",
                        suffixStyle: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xff6d6d6d),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        counterText: ''),
                    maxLength: 50,
                    validator: validatePage,
                    onChanged: (text) {
                      setState(() {
                        pageNameLength = text.length;
                        check();
                      });
                    }),
              )),
              SliverToBoxAdapter(
                  child: Container(
                padding: const EdgeInsets.only(top: 9, left: 16, right: 16),
                child: Text(
                    "ชื่อเพจควรอธิบายถึงชื่อศิลปิน ค่ายเพลง ผู้จัด หรือชื่อร้านผับ/บาร์ของคุณ คุณสามารถเปลี่ยนชื่อเพจได้ในภายหลัง",
                    style: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    )),
              )),
              SliverFillRemaining(
                  hasScrollBody: false,
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        margin: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 24),
                        width: double.infinity,
                        height: 42,
                        child: ElevatedButton(
                            child: Text(
                              "ถัดไป",
                              style: TextStyle(
                                fontFamily: 'SukhumvitSet-Text',
                                color: isActive ? Color(0xffffffff) : Color(0xffb2b2b2),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                backgroundColor: MaterialStateProperty.all<Color>(
                                    isActive ? Color(0xffda3534) : Color(0xffe6e7ec)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(21),
                                ))),
                            onPressed: () => isActive ? _save() : null),
                      )))
            ],
          ),
        ));
  }

  String? validatePage(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณากรอกชื่อเพจ';
    }
    if (value.length <= 2) {
      return 'กรุณากรอกชื่อเพจไม่ต่ำกว่า 2 ตัวอักษร';
    }
    return null;
  }

  void _save() {
    createPage.displayName = pageName.text;
    //TixNavigate.instance.navigateTo(CreatePageType(),data: createPage);
    WrapNavigation.instance.pushNamed(context, CreatePageType(), arguments: createPage);
  }

  void check() {
    isActive = (validatePage(pageName.text) == null);
  }
}
