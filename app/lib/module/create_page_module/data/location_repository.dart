import 'package:app/core/base_repository.dart';
import 'package:app/module/create_page_module/data/location_api.dart';
import 'package:app/module/create_page_module/model/get_auto_complete_location_entity.dart';
import 'package:app/module/create_page_module/model/get_geocode_entity.dart';
import 'package:app/module/create_page_module/model/get_locally_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class LocationRepository extends BaseRepository {
  final LocationApi api;

  LocationRepository(this.api);

  Future<GetGeocodeEntity?> geoCode(double lat, double long) async {
    final result = await api.geoCode(lat,long);
    return result;
  }

  Future<GetAutoCompleteLocationEntity?> searchLocation(String term) async{
    final result = await api.searchLocation(term);
    return result;
  }
  Future<GetLocallyData?> getLocally(String city,String district,String province) async {
    final result = await api.getLocally(city,district,province);
    return result.data;
  }

}