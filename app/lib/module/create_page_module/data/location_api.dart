import 'package:app/api/core_api.dart';
import 'package:app/api/errors/bad_request_error.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/constants.dart';
import 'package:app/module/create_page_module/model/get_auto_complete_location_entity.dart';
import 'package:app/module/create_page_module/model/get_geocode_entity.dart';
import 'package:app/module/create_page_module/model/get_locally_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class LocationApi {

  final CoreApi api;
  final String pathLocally ='/locally';

  LocationApi(this.api);

  static getWithoutBaseUrl(String endpoint) async {
    try {
      Dio dio = Dio();
      dio.options.contentType = Headers.jsonContentType;
      return await dio.get(endpoint);
    } on BadRequestError catch (error) {
      throw BaseErrorEntity.badRequestToModelError(error);
    }
  }

  Future<GetGeocodeEntity> geoCode(double lat, double long) async {
    final response = await getWithoutBaseUrl('https://maps.googleapis'
        '.com/maps/api/geocode/json?latlng=${lat},${long}&key=${GOOGLE_API_KEY}&language=th');
    return GetGeocodeEntity.fromJson(response.data);

  }

  Future<GetAutoCompleteLocationEntity> searchLocation(String term) async {
    final response = await getWithoutBaseUrl('https://maps.googleapis'
        '.com/maps/api/place/autocomplete/json?input=${term}&key=${GOOGLE_API_KEY}&language=th');
    return GetAutoCompleteLocationEntity.fromJson(response.data);

  }

  Future<GetLocallyEntity> getLocally(String city,String district,String province) async {
    final response = await api.get(pathLocally, {
      'city':city,
      'district':district,
      'province':province,
    },  BaseErrorEntity.badRequestToModelError);
    return GetLocallyEntity.fromJson(response.data);

  }
}