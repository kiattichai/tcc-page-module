import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_bank_item.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'agent_bank_viewmodel.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:app/globals.dart' as global;

class AgentBankScreen extends StatefulWidget with TixRoute {
  final Map<String, dynamic>? addressData;
  final String? isUpdate;
  AgentBankScreen({Key? key, this.addressData, this.isUpdate}) : super(key: key);

  @override
  _AgentBankScreenState createState() => _AgentBankScreenState();

  @override
  String buildPath() {
    return '/agent_bank';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => AgentBankScreen(
              addressData: data,
              isUpdate: data['isUpdate'],
            ));
  }
}

class _AgentBankScreenState extends BaseStateProvider<AgentBankScreen, AgentBankViewModel> {
  final FocusNode _nodeTextBankNo = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: false,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeTextBankNo,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 20, top: 8, bottom: 8),
                  child: Text(
                    "ตกลง",
                    textScaleFactor: MediaQuery.of(context).textScaleFactor > 1.2
                        ? 1.1
                        : MediaQuery.of(context).textScaleFactor,
                    style: TextStyle(
                      fontFamily: 'Kanit',
                      color: Color(0xffda3534),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              );
            }
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    viewModel = AgentBankViewModel(widget.addressData ?? {}, widget.isUpdate ?? '');
    viewModel.showAlertError = showAlertError;
    viewModel.showRegisterSuccess = showRegisterSuccess;
    viewModel.showUpdateSuccess = showUpdateSuccess;
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appState = Provider.of(context);
    return BaseWidget<AgentBankViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            inAsyncCall: viewModel.loading,
            color: Colors.white,
            opacity: 1.0,
            progressIndicator: CircleLoading(),
            child: GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: Scaffold(
                  appBar: AgentAppBar(
                    onBack: () {
                      if (WrapNavigation.instance.canPop(context)) {
                        WrapNavigation.instance.pop(context);
                      } else {
                        SystemNavigator.pop();
                        appState.channel.invokeMethod('pop');
                      }
                    },
                    name: viewModel.isUpdate!.isNotEmpty ? 'แก้ไขบัญชีธนาคาร' : 'สมัครเป็นตัวแทน',
                  ),
                  body: KeyboardActions(
                    config: _buildConfig(context),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 14),
                            Text(
                              "บัญชีธนาคาร",
                              style: TextStyle(
                                fontFamily: 'Thonburi-Bold',
                                color: Color(0xff2b2b2b),
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              viewModel.isUpdate!.isEmpty
                                  ? "สามารถระบุในภายหลังได้"
                                  : "ข้อมูลจำเป็นต้องระบุให้ครบ",
                              style: TextStyle(
                                fontFamily: 'Thonburi-Bold',
                                color: Color(0xff2b2b2b),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              child: TextFormField(
                                controller: viewModel.bank,
                                onTap: () async {
                                  Map<String, dynamic> map = {
                                    'title': 'เลือกบัญชีธนาคาร',
                                    'item': viewModel.getPaymentList,
                                    'selected': viewModel.bankSelected
                                  };
                                  //final result = await TixNavigate.instance.navigateTo(DocumentBankItem(), data: map);
                                  final result = await WrapNavigation.instance
                                      .pushNamed(context, DocumentBankItem(), arguments: map);
                                  if (result is Map && result['id'] != -1) {
                                    print(result['id']);
                                    viewModel.setIdBank(result['id']);
                                  }
                                },
                                readOnly: true,
                                onChanged: (value) {
                                  print(value);
                                  if (value.length > 0) {
                                    viewModel.bankForm = true;
                                  } else {
                                    viewModel.bankForm = false;
                                  }
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛.,_-\s]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 6),
                                  hintText: 'ธนาคาร',
                                  hintStyle: hintTextStyle(),
                                  suffixText: viewModel.bank.text.isNotEmpty ? '   เปลี่ยน  ' : '',
                                  suffixStyle: suffixTextStyle(),
                                  suffixIcon: Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    size: 15,
                                    color: Color(0xff545454),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                    maxHeight: 13,
                                  ),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                controller: viewModel.bankName,
                                onChanged: (value) {
                                  if (value.length > 0) {
                                    viewModel.bankForm = true;
                                  } else {
                                    viewModel.bankForm = false;
                                  }
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  //FilteringTextInputFormatter.allow(RegExp('[ก-๛.,_-\s]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'ชื่อบัญชี',
                                  hintStyle: hintTextStyle(),
                                  suffixText: '',
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                focusNode: _nodeTextBankNo,
                                controller: viewModel.bankNumber,
                                onChanged: (value) {
                                  if (value.length > 0) {
                                    viewModel.bankForm = true;
                                  } else {
                                    viewModel.bankForm = false;
                                  }
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[0-9]+')),
                                  LengthLimitingTextInputFormatter(15),
                                ],
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'เลขที่บัญชี',
                                  hintStyle: hintTextStyle(),
                                  suffixText: '',
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                controller: viewModel.bankMajor,
                                onChanged: (value) {
                                  if (value.length > 0) {
                                    viewModel.bankForm = true;
                                  } else {
                                    viewModel.bankForm = false;
                                  }
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛a-zA-Z]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'สาขาธนาคาร',
                                  hintStyle: hintTextStyle(),
                                  suffixText: '',
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            viewModel.getProfileAgentData?.agent?.documentStatus?.id == 2
                                ? SizedBox()
                                : Text("สำเนาหน้าบัญชีธนาคาร",
                                    style: TextStyle(
                                      fontFamily: 'Thonburi-Bold',
                                      color: Color(0xff676767),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                            viewModel.getProfileAgentData?.agent?.documentStatus != null
                                ? Container(
                                    margin: EdgeInsets.only(top: 16.0),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(6),
                                        child: Image.network(
                                          '${viewModel.getProfileAgentData?.agent?.documentBankbook}?v=${viewModel.now}',
                                          headers: {
                                            "Authorization":
                                                "Bearer ${global.userAuthen?.data?.accessToken}"
                                          },
                                          scale: 1,
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            InkWell(
                              onTap: () {
                                FocusScope.of(context).unfocus();
                                viewModel.picImage();
                              },
                              child: Container(
                                width: Screen.width,
                                height: 44,
                                margin: EdgeInsets.only(top: 10),
                                padding: EdgeInsets.only(left: 8, right: 8),
                                decoration: new BoxDecoration(
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(4),
                                    border: Border.all(
                                        color: viewModel.bankImage != null
                                            ? Colors.green
                                            : Colors.grey.shade300,
                                        width: 1)),
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: AssetImage('assets/add.png'),
                                        width: 16,
                                        height: 16,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          viewModel.bankImage != null
                                              ? "${viewModel.fileName}"
                                              : "อัปโหลดหน้าบัญชีธนาคาร",
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontFamily: 'Thonburi-Bold',
                                            color: Color(0xff676767),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  bottomNavigationBar: viewModel.isUpdate!.isNotEmpty
                      ? InkWell(
                          onTap: () {
                            if (viewModel.formSuccess) {
                              _nodeTextBankNo.unfocus();
                              viewModel.updateAgent(context);
                            }
                          },
                          child: Container(
                            width: Screen.width,
                            height: 39,
                            margin: EdgeInsets.only(
                                top: 16,
                                left: 16.0,
                                right: 16.0,
                                bottom: 16.0 + MediaQuery.of(context).padding.bottom),
                            decoration: BoxDecoration(
                                color:
                                    viewModel.formSuccess ? Color(0xffda3534) : Color(0xff989898),
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                              child: Text(
                                "บันทึก",
                                style: TextStyle(
                                  fontFamily: 'Thonburi-Bold',
                                  color: Color(0xffffffff),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            InkWell(
                              onTap: () {
                                _nodeTextBankNo.unfocus();
                                viewModel.registerWithoutBug(context, appState);
                              },
                              child: Center(
                                child: Text(
                                  "ระบุภายหลัง",
                                  style: TextStyle(
                                    fontFamily: 'Thonburi',
                                    color: Color(0xff2b2b2b),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                _nodeTextBankNo.unfocus();
                                viewModel.registerWithoutBug(context, appState);
                              },
                              child: Container(
                                width: Screen.width,
                                height: 39,
                                margin: EdgeInsets.all(16 + MediaQuery.of(context).padding.bottom),
                                decoration: BoxDecoration(
                                    color: Color(0xffda3534),
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                  child: Text(
                                    "ยืนยันการสมัคร",
                                    style: TextStyle(
                                      fontFamily: 'Thonburi-Bold',
                                      color: Color(0xffffffff),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                )),
          );
        },
        model: viewModel);
  }

  void showAlertError(String message) {
    if (showing == true) return;

    showing = true;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "เกิดข้อผิดพลาด",
            description: message,
            dangerButtonText: 'ยกเลิก',
            buttonText: 'ตกลง',
          );
        });
  }

  void showRegisterSuccess() async {
    showing = true;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "สำเร็จ",
            description: 'การดำเนินการสำเร็จ กรุณารอการตรวจสอบข้อมูล',
            buttonText: 'ตกลง',
          );
        });
  }

  void showUpdateSuccess() async {
    showing = true;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "สำเร็จ",
            description: 'แก้ไขข้อมูลเรียบร้อยแล้ว',
            buttonText: 'ตกลง',
          );
        });
  }
}

hintTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff676767),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

suffixTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff989898),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

underlineStyle() {
  return UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.grey.shade300, width: 1),
  );
}
