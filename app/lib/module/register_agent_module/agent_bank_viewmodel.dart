import 'dart:convert';
import 'dart:io';

import 'package:app/app/app_state.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/model/get_payment_bank_entity.dart';
import 'package:app/module/register_agent_module/agent_detail_screen.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';
import 'package:app/module/register_agent_module/model/response_register_entity.dart';
import 'package:app/utils/image_picker_utils.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import 'model/get_agent_entity.dart';

class AgentBankViewModel extends BaseViewModel {
  Map<String, dynamic> addressData;
  TextEditingController bank = TextEditingController();
  TextEditingController bankName = TextEditingController();
  TextEditingController bankNumber = TextEditingController();
  TextEditingController bankMajor = TextEditingController();
  GetPaymentBankData? getPayment;
  List<GetPaymentBankDataRecord> getPaymentList = [];
  PickedFile? pickedFile;
  File? bankImage;
  GetProfileAgentData? getProfileAgentData;
  int? bankSelected;
  bool bankForm = false;
  bool formSuccess = false;
  String? isUpdate;
  String? fileName;
  var now = DateTime.now();

  Function(String)? showAlertError;
  Function()? showRegisterSuccess;
  Function()? showUpdateSuccess;

  AgentBankViewModel(this.addressData, this.isUpdate);

  @override
  void postInit() async {
    super.postInit();
    await getBankList();
    if (isUpdate!.isNotEmpty) {
      getAgent();
    }
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      setLoading(false);
      showAlertError!(error.message);
    }
  }

  void getAgent() async {
    catchError(() async {
      setLoading(true);
      getProfileAgentData = await di.agentRepository.getProfile();
      if (getProfileAgentData != null) {
        bankSelected = getProfileAgentData?.agent?.bank?.id;
        bank.text = getProfileAgentData?.agent?.bank?.name ?? '';
        bankName.text = getProfileAgentData?.agent?.bank?.accountName ?? '';
        bankNumber.text = getProfileAgentData?.agent?.bank?.accountNo ?? '';
        bankMajor.text = getProfileAgentData?.agent?.bank?.branch ?? '';
      }
      setLoading(false);
    });
  }

  Future getBankList() async {
    catchError(() async {
      setLoading(true);
      getPayment = await di.pageRepository.getPayment();
      if (getPayment != null && getPayment?.record != null) {
        getPaymentList.addAll(getPayment?.record ?? []);
      }
      setLoading(false);
    });
  }

  void picImage() async {
    final pathImage = await ImagePickerUtils.pickImage();
    if (pathImage != null) {
      bankImage = File(pathImage);
      String fname = bankImage!.path.substring(bankImage!.path.length - 20, bankImage!.path.length);
      fileName = fname;
    }
    notifyListeners();
    checkValidate();
  }

  void setIdBank(int id) {
    print(id);
    bankSelected = id;
    bank.text = getPaymentList[id - 1].name ?? '';
    checkValidate();
    notifyListeners();
  }

  void checkValidate() {
    if (isUpdate!.isEmpty) {
      if (bank.text.isNotEmpty &&
          bankName.text.isNotEmpty &&
          bankNumber.text.isNotEmpty &&
          bankMajor.text.isNotEmpty &&
          bankImage != null) {
        formSuccess = true;
      } else {
        formSuccess = false;
      }
    } else {
      if (bank.text.isNotEmpty &&
          bankName.text.isNotEmpty &&
          bankNumber.text.isNotEmpty &&
          bankMajor.text.isNotEmpty) {
        formSuccess = true;
      } else {
        formSuccess = false;
      }
    }
  }

  void updateAgent(BuildContext context) {
    Map<String, dynamic> params = {
      "payment_data": {
        "bank_id": bankSelected,
        "account_no": bankNumber.text,
        "account_name": bankName.text,
        "branch": bankMajor.text
      }
    };

    catchError(() async {
      setLoading(true);
      await di.agentRepository.updateAgent(params);
      if (bankImage != null) {
        ImageUploadRequest imageBankUpload =
            await ImagePickerUtils.getImageUploadRequest(bankImage!.path);
        Map<String, dynamic> params1 = {
          "document": {"type": "bankbook", "name": imageBankUpload.fileName},
          "image": imageBankUpload.fileData,
        };
        await di.agentRepository.uploadImage(params1);
      }

      setLoading(false);
      await showUpdateSuccess!();
      WrapNavigation.instance.pop(context, data: 'update');
    });
  }

  void registerWithoutBug(BuildContext context, AppState appState) {
    Map<String, dynamic> params = {
      "name": '${addressData['firstName']}',
      "surname": '${addressData['lastName']}',
      "thai_id": '${addressData['thai_id']}',
      "email": '${addressData['email']}',
      "address": {
        "detail": '${addressData['detail']}',
        "district_id": addressData['district_id'],
        "city_id": addressData['city_id'],
        "province_id": addressData['province_id'],
        "country_id": 209,
        "zip_code": '${addressData['zip_code']}'
      }
    };
    if (bankSelected != null) {
      print('bankSelected != null');
      final Map<String, dynamic> bankId = {"bank_id": bankSelected};
      params.addAll({"payment_data": bankId});

      if (bankNumber.text.isNotEmpty) {
        final Map<String, dynamic> paymentData = params['payment_data'];
        paymentData.addAll({'account_no': '${bankNumber.text}'});
        params['payment_data'] = paymentData;
      }
      if (bankName.text.isNotEmpty) {
        final Map<String, dynamic> paymentData = params['payment_data'];
        paymentData.addAll({'account_name': '${bankName.text}'});
        params['payment_data'] = paymentData;
      }
      if (bankMajor.text.isNotEmpty) {
        final Map<String, dynamic> paymentData = params['payment_data'];
        paymentData.addAll({'branch': '${bankMajor.text}'});
        params['payment_data'] = paymentData;
      }
    }

    catchError(() async {
      setLoading(true);
      final response = await di.agentRepository.registerAgent(params);
      if (response != null) {
        File? cardImage = await addressData['card_image'];
        ImageUploadRequest imageCardUpload =
            await ImagePickerUtils.getImageUploadRequest(cardImage!.path);
        if (bankImage == null) {
          Map<String, dynamic> params = {
            "document": {
              "type": "idcard",
              "name": imageCardUpload.fileName,
            },
            "image": imageCardUpload.fileData,
          };
          await Future.delayed(Duration(milliseconds: 500));
          await di.agentRepository.uploadImage(params);
        } else {
          ImageUploadRequest imageBankUpload =
              await ImagePickerUtils.getImageUploadRequest(bankImage!.path);

          Map<String, dynamic> params1 = {
            "document": {
              "type": "idcard",
              "name": imageCardUpload.fileName,
            },
            "image": imageCardUpload.fileData,
          };

          Map<String, dynamic> params2 = {
            "document": {
              "type": "bankbook",
              "name": imageBankUpload.fileName,
            },
            "image": imageBankUpload.fileData,
          };
          await Future.delayed(Duration(milliseconds: 500));
          await di.agentRepository.uploadImage(params1);
          await Future.delayed(Duration(milliseconds: 500));
          await di.agentRepository.uploadImage(params2);
        }
      }
      setLoading(false);
      await showRegisterSuccess!();
      WrapNavigation.instance.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => AgentDetailScreen(),
        ),
        (route) => false,
      );
    });
  }
}
