

import 'package:app/api/core_api.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/module/register_agent_module/model/get_agent_entity.dart';
import 'package:app/module/register_agent_module/model/get_cities_entity.dart';
import 'package:app/module/register_agent_module/model/get_conditions_entity.dart';
import 'package:app/module/register_agent_module/model/get_districts_entity.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';
import 'package:app/module/register_agent_module/model/get_province_entity.dart';
import 'package:app/module/register_agent_module/model/response_register_entity.dart';
import 'package:app/module/register_agent_module/model/response_update_entity.dart';
import 'package:app/module/register_agent_module/model/response_upload_image_entity.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class AgentApi {

  final CoreApi api;
  final String pathUsers = '/users';
  final String pathProfile = '/profile';
  final String pathAgent = '/agents';

  AgentApi(this.api);

  Future<GetProvinceEntity> getProvince(int countryId) async {
    Response response = await api.get('/provinces', {"country_id":countryId}, BaseErrorEntity.badRequestToModelError,headers: {'Accept-Language':'th'}, hasPermission: true);
    return GetProvinceEntity.fromJson(response.data);
  }
  
  Future<GetCitiesEntity> getCities(int provinceId) async {
    Response response = await api.get('/cities', {'province_id': provinceId}, BaseErrorEntity.badRequestToModelError,headers: {'Accept-Language':'th'}, hasPermission: true);
    return GetCitiesEntity.fromJson(response.data);
  }

  Future<GetDistrictsEntity> getDistricts(int citiesId) async {
    Response response = await api.get('/districts', {'city_id': citiesId}, BaseErrorEntity.badRequestToModelError,headers: {'Accept-Language':'th'}, hasPermission: true);
    return GetDistrictsEntity.fromJson(response.data);
  }


  Future<ResponseRegisterEntity> registerAgent(Map<String,dynamic> params) async {
    Response response = await api.post('/agents/register', params, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return ResponseRegisterEntity.fromJson(response.data);
  }

  Future<ResponseUploadImageEntity> uploadImage(Map<String,dynamic> params) async {
    Response response = await api.put('/agents/attachment', params, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return ResponseUploadImageEntity.fromJson(response.data);
  }
  
  Future<GetConditionsEntity> getConditions() async {
    Response response = await api.get('/contents?group_code=page&slug=tcc-agent-conditions&status=true', null, BaseErrorEntity.badRequestToModelError,headers: {'Accept-Language':'th'}, hasPermission: true);
    return GetConditionsEntity.fromJson(response.data);
  }
  
  Future<GetProfileAgentEntity> getProfile() async {
    Response response = await api.get(pathUsers+pathProfile, null, BaseErrorEntity.badRequestToModelError, headers: {'Accept-Language':'th'}, hasPermission: true);
    return GetProfileAgentEntity.fromJson(response.data);
  }

  Future<GetAgentEntity> getAgent() async {
    Response response = await api.get(pathAgent+pathProfile, null, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return GetAgentEntity.fromJson(response.data);
  }
  
  Future<ResponseUpdateEntity> updateAgent(Map<String,dynamic> params) async {
    Response response = await api.put(pathAgent+pathProfile, params, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return ResponseUpdateEntity.fromJson(response.data);
  }

}