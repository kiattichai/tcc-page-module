
import 'package:app/model/get_promotion_entity.dart';
import 'package:app/module/register_agent_module/data/agent_api.dart';
import 'package:app/module/register_agent_module/model/get_agent_entity.dart';
import 'package:app/module/register_agent_module/model/get_cities_entity.dart';
import 'package:app/module/register_agent_module/model/get_conditions_entity.dart';
import 'package:app/module/register_agent_module/model/get_districts_entity.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';
import 'package:app/module/register_agent_module/model/get_province_entity.dart';
import 'package:app/module/register_agent_module/model/response_register_entity.dart';
import 'package:app/module/register_agent_module/model/response_update_entity.dart';
import 'package:app/module/register_agent_module/model/response_upload_image_entity.dart';
import 'package:injectable/injectable.dart';

@singleton
class AgentRepository {
  final AgentApi api;
  AgentRepository(this.api);



  Future<GetProvinceData?> getProvince(int countryId) async{
    final result = await api.getProvince(countryId);
    return result.data;
  }

  Future<GetCitiesData?> getCities(int provinceId) async {
    final result = await api.getCities(provinceId);
    return result.data;
  }

  Future<GetDistrictsData?> getDistricts(int citiesId) async {
    final result = await api.getDistricts(citiesId);
    return result.data;
  }

  Future<ResponseRegisterData?> registerAgent(Map<String,dynamic> params) async {
    final result = await api.registerAgent(params);
    return result.data;
  }

  Future<ResponseUploadImageData?> uploadImage(Map<String,dynamic> params) async {
    final result = await api.uploadImage(params);
    return result.data;
  }

  Future<GetConditionsData?> getConditions() async{
    final result = await api.getConditions();
    return result.data;
  }

  Future<GetProfileAgentData?> getProfile() async {
    final result = await api.getProfile();
    return result.data;
  }

  Future<GetAgentData?> getAgent() async {
    final result = await api.getAgent();
    return result.data;
  }

  Future<ResponseUpdateData?> updateAgent(Map<String,dynamic> params) async {
    final result = await api.updateAgent(params);
    return result.data;
  }

}