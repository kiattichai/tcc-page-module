import 'package:app/generated/json/get_districts_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetDistrictsEntity {
  GetDistrictsEntity();

  factory GetDistrictsEntity.fromJson(Map<String, dynamic> json) =>
      $GetDistrictsEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetDistrictsEntityToJson(this);

  GetDistrictsData? data;
  GetDistrictsBench? bench;
}

@JsonSerializable()
class GetDistrictsData {
  GetDistrictsData();

  factory GetDistrictsData.fromJson(Map<String, dynamic> json) =>
      $GetDistrictsDataFromJson(json);

  Map<String, dynamic> toJson() => $GetDistrictsDataToJson(this);

  List<GetDistrictsDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetDistrictsDataRecord {
  GetDistrictsDataRecord();

  factory GetDistrictsDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetDistrictsDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetDistrictsDataRecordToJson(this);

  int? id;
  String? name;
  @JSONField(name: "zip_code")
  String? zipCode;
  @JSONField(name: "zip_code_map")
  String? zipCodeMap;
}

@JsonSerializable()
class GetDistrictsBench {
  GetDistrictsBench();

  factory GetDistrictsBench.fromJson(Map<String, dynamic> json) =>
      $GetDistrictsBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetDistrictsBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
