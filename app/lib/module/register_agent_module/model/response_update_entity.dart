import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_update_entity.g.dart';

@JsonSerializable()
class ResponseUpdateEntity {
  ResponseUpdateEntity();

  factory ResponseUpdateEntity.fromJson(Map<String, dynamic> json) =>
      $ResponseUpdateEntityFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUpdateEntityToJson(this);

  ResponseUpdateData? data;
  ResponseUpdateBench? bench;
}

@JsonSerializable()
class ResponseUpdateData {
  ResponseUpdateData();

  factory ResponseUpdateData.fromJson(Map<String, dynamic> json) =>
      $ResponseUpdateDataFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUpdateDataToJson(this);

  String? message;
}

@JsonSerializable()
class ResponseUpdateBench {
  ResponseUpdateBench();

  factory ResponseUpdateBench.fromJson(Map<String, dynamic> json) =>
      $ResponseUpdateBenchFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUpdateBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
