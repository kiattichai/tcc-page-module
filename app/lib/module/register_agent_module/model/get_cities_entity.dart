import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/get_cities_entity.g.dart';

@JsonSerializable()
class GetCitiesEntity {
  GetCitiesEntity();

  factory GetCitiesEntity.fromJson(Map<String, dynamic> json) =>
      $GetCitiesEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetCitiesEntityToJson(this);

  GetCitiesData? data;
  GetCitiesBench? bench;
}

@JsonSerializable()
class GetCitiesData {
  GetCitiesData();

  factory GetCitiesData.fromJson(Map<String, dynamic> json) =>
      $GetCitiesDataFromJson(json);

  Map<String, dynamic> toJson() => $GetCitiesDataToJson(this);

  List<GetCitiesDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetCitiesDataRecord {
  GetCitiesDataRecord();

  factory GetCitiesDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetCitiesDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetCitiesDataRecordToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetCitiesBench {
  GetCitiesBench();

  factory GetCitiesBench.fromJson(Map<String, dynamic> json) =>
      $GetCitiesBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetCitiesBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
