import 'package:app/generated/json/get_profile_agent_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetProfileAgentEntity {
  GetProfileAgentEntity();

  factory GetProfileAgentEntity.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentEntityToJson(this);

  GetProfileAgentData? data;
  GetProfileAgentBench? bench;
}

@JsonSerializable()
class GetProfileAgentData {
  GetProfileAgentData();

  factory GetProfileAgentData.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataToJson(this);

  int? id;
  String? username;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "display_name")
  dynamic? displayName;
  dynamic? avatar;
  String? gender;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  String? email;
  String? idcard;
  String? birthday;
  @JSONField(name: "terms_accepted")
  bool? termsAccepted;
  bool? activated;
  @JSONField(name: "activated_at")
  String? activatedAt;
  bool? blocked;
  @JSONField(name: "profile_score")
  int? profileScore;
  @JSONField(name: "last_login_at")
  String? lastLoginAt;
  @JSONField(name: "last_login_ip")
  String? lastLoginIp;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  String? referral;
  String? lang;
  GetProfileAgentDataWallet? wallet;
  GetProfileAgentDataGroup? group;
  GetProfileAgentDataAgent? agent;
  List<dynamic>? connects;
  bool? cache;
}

@JsonSerializable()
class GetProfileAgentDataWallet {
  GetProfileAgentDataWallet();

  factory GetProfileAgentDataWallet.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataWalletFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataWalletToJson(this);

  int? id;
  int? amount;
}

@JsonSerializable()
class GetProfileAgentDataGroup {
  GetProfileAgentDataGroup();

  factory GetProfileAgentDataGroup.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataGroupFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataGroupToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProfileAgentDataAgent {
  GetProfileAgentDataAgent();

  factory GetProfileAgentDataAgent.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataAgentToJson(this);

  int? id;
  String? code;
  String? status;
  @JSONField(name: "document_status")
  GetProfileAgentDataAgentDocumentStatus? documentStatus;
  String? remark;
  String? address;
  @JSONField(name: "zip_code")
  String? zipCode;
  GetProfileAgentDataAgentBank? bank;
  @JSONField(name: "upload_idcard")
  bool? uploadIdcard;
  @JSONField(name: "document_idcard")
  String? documentIdcard;
  @JSONField(name: "upload_bankbook")
  bool? uploadBankbook;
  @JSONField(name: "document_bankbook")
  String? documentBankbook;
  GetProfileAgentDataAgentCountry? country;
  GetProfileAgentDataAgentProvince? province;
  GetProfileAgentDataAgentCity? city;
  GetProfileAgentDataAgentDistrict? district;
}

@JsonSerializable()
class GetProfileAgentDataAgentDocumentStatus {
  GetProfileAgentDataAgentDocumentStatus();

  factory GetProfileAgentDataAgentDocumentStatus.fromJson(
          Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentDocumentStatusFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetProfileAgentDataAgentDocumentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetProfileAgentDataAgentBank {
  GetProfileAgentDataAgentBank();

  factory GetProfileAgentDataAgentBank.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentBankFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataAgentBankToJson(this);

  int? id;
  String? name;
  @JSONField(name: "account_no")
  String? accountNo;
  @JSONField(name: "account_name")
  String? accountName;
  @JSONField(name: "account_type")
  dynamic? accountType;
  String? branch;
  String? logo;
}

@JsonSerializable()
class GetProfileAgentDataAgentCountry {
  GetProfileAgentDataAgentCountry();

  factory GetProfileAgentDataAgentCountry.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentCountryFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataAgentCountryToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProfileAgentDataAgentProvince {
  GetProfileAgentDataAgentProvince();

  factory GetProfileAgentDataAgentProvince.fromJson(
          Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentProvinceFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetProfileAgentDataAgentProvinceToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProfileAgentDataAgentCity {
  GetProfileAgentDataAgentCity();

  factory GetProfileAgentDataAgentCity.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentCityFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentDataAgentCityToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProfileAgentDataAgentDistrict {
  GetProfileAgentDataAgentDistrict();

  factory GetProfileAgentDataAgentDistrict.fromJson(
          Map<String, dynamic> json) =>
      $GetProfileAgentDataAgentDistrictFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetProfileAgentDataAgentDistrictToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProfileAgentBench {
  GetProfileAgentBench();

  factory GetProfileAgentBench.fromJson(Map<String, dynamic> json) =>
      $GetProfileAgentBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetProfileAgentBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
