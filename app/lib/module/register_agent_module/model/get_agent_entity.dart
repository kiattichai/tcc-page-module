import 'package:app/generated/json/get_agent_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetAgentEntity {
  GetAgentEntity();

  factory GetAgentEntity.fromJson(Map<String, dynamic> json) =>
      $GetAgentEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentEntityToJson(this);

  GetAgentData? data;
  GetAgentBench? bench;
}

@JsonSerializable()
class GetAgentData {
  GetAgentData();

  factory GetAgentData.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataToJson(this);

  @JSONField(name: "agent_id")
  int? agentId;
  @JSONField(name: "user_id")
  int? userId;
  int? status;
  @JSONField(name: "document_status")
  GetAgentDataDocumentStatus? documentStatus;
  dynamic? remark;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  @JSONField(name: "thai_id")
  String? thaiId;
  String? email;
  GetAgentDataAddress? address;
  @JSONField(name: "bank_detail")
  GetAgentDataBankDetail? bankDetail;
}

@JsonSerializable()
class GetAgentDataDocumentStatus {
  GetAgentDataDocumentStatus();

  factory GetAgentDataDocumentStatus.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataDocumentStatusFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataDocumentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetAgentDataAddress {
  GetAgentDataAddress();

  factory GetAgentDataAddress.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataAddressFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataAddressToJson(this);

  String? detail;
  GetAgentDataAddressDistrict? district;
  GetAgentDataAddressCity? city;
  GetAgentDataAddressProvince? province;
  GetAgentDataAddressCountry? country;
  @JSONField(name: "zip_code")
  String? zipCode;
}

@JsonSerializable()
class GetAgentDataAddressDistrict {
  GetAgentDataAddressDistrict();

  factory GetAgentDataAddressDistrict.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataAddressDistrictFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataAddressDistrictToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetAgentDataAddressCity {
  GetAgentDataAddressCity();

  factory GetAgentDataAddressCity.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataAddressCityFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataAddressCityToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetAgentDataAddressProvince {
  GetAgentDataAddressProvince();

  factory GetAgentDataAddressProvince.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataAddressProvinceFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataAddressProvinceToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetAgentDataAddressCountry {
  GetAgentDataAddressCountry();

  factory GetAgentDataAddressCountry.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataAddressCountryFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataAddressCountryToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetAgentDataBankDetail {
  GetAgentDataBankDetail();

  factory GetAgentDataBankDetail.fromJson(Map<String, dynamic> json) =>
      $GetAgentDataBankDetailFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentDataBankDetailToJson(this);

  @JSONField(name: "bank_id")
  dynamic? bankId;
  @JSONField(name: "bank_name")
  String? bankName;
  @JSONField(name: "account_no")
  String? accountNo;
  @JSONField(name: "account_name")
  String? accountName;
  @JSONField(name: "account_type_id")
  int? accountTypeId;
  @JSONField(name: "account_type_name")
  String? accountTypeName;
  String? branch;
}

@JsonSerializable()
class GetAgentBench {
  GetAgentBench();

  factory GetAgentBench.fromJson(Map<String, dynamic> json) =>
      $GetAgentBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetAgentBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
