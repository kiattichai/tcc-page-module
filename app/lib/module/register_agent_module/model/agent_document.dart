import 'dart:io';

class AgentDocument {
  String? name;
  String? surname;
  String? thaiId;
  String? email;
  String? addressDetail;
  int? districtId;
  int? cityId;
  int? provinceId;
  int? countryId;
  String? zipCode;
  int? bankId;
  String? accountNo;
  String? accountName;
  int? accountType;
  String? branch;
  File? cardImage;
  String? cardImageName;
  File? bankImage;
  String? bankImageName;
}