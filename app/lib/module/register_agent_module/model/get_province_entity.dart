import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/get_province_entity.g.dart';

@JsonSerializable()
class GetProvinceEntity {
  GetProvinceEntity();

  factory GetProvinceEntity.fromJson(Map<String, dynamic> json) =>
      $GetProvinceEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetProvinceEntityToJson(this);

  GetProvinceData? data;
  GetProvinceBench? bench;
}

@JsonSerializable()
class GetProvinceData {
  GetProvinceData();

  factory GetProvinceData.fromJson(Map<String, dynamic> json) =>
      $GetProvinceDataFromJson(json);

  Map<String, dynamic> toJson() => $GetProvinceDataToJson(this);

  List<GetProvinceDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetProvinceDataRecord {
  GetProvinceDataRecord();

  factory GetProvinceDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetProvinceDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetProvinceDataRecordToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetProvinceBench {
  GetProvinceBench();

  factory GetProvinceBench.fromJson(Map<String, dynamic> json) =>
      $GetProvinceBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetProvinceBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
