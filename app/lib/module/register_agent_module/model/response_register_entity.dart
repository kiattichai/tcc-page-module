import 'package:app/generated/json/response_register_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class ResponseRegisterEntity {

	ResponseRegisterEntity();

	factory ResponseRegisterEntity.fromJson(Map<String, dynamic> json) => $ResponseRegisterEntityFromJson(json);

	Map<String, dynamic> toJson() => $ResponseRegisterEntityToJson(this);

	ResponseRegisterData? data;
	ResponseRegisterBench? bench;
}

@JsonSerializable()
class ResponseRegisterData {

	ResponseRegisterData();

	factory ResponseRegisterData.fromJson(Map<String, dynamic> json) => $ResponseRegisterDataFromJson(json);

	Map<String, dynamic> toJson() => $ResponseRegisterDataToJson(this);

	String? message;
	@JSONField(name: "agent_id")
	int? agentId;
}

@JsonSerializable()
class ResponseRegisterBench {

	ResponseRegisterBench();

	factory ResponseRegisterBench.fromJson(Map<String, dynamic> json) => $ResponseRegisterBenchFromJson(json);

	Map<String, dynamic> toJson() => $ResponseRegisterBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
