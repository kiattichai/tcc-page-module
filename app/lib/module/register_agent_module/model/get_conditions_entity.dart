import 'package:app/generated/json/get_conditions_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConditionsEntity {

	GetConditionsEntity();

	factory GetConditionsEntity.fromJson(Map<String, dynamic> json) => $GetConditionsEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsEntityToJson(this);

	GetConditionsData? data;
	GetConditionsBench? bench;
}

@JsonSerializable()
class GetConditionsData {

	GetConditionsData();

	factory GetConditionsData.fromJson(Map<String, dynamic> json) => $GetConditionsDataFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataToJson(this);

	GetConditionsDataPagination? pagination;
	List<GetConditionsDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetConditionsDataPagination {

	GetConditionsDataPagination();

	factory GetConditionsDataPagination.fromJson(Map<String, dynamic> json) => $GetConditionsDataPaginationFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class GetConditionsDataRecord {

	GetConditionsDataRecord();

	factory GetConditionsDataRecord.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordToJson(this);

	int? id;
	String? slug;
	GetConditionsDataRecordGroup? group;
	GetConditionsDataRecordCategory? category;
	List<GetConditionsDataRecordFields>? fields;
	List<dynamic>? images;
	int? viewed;
	@JSONField(name: "viewed_text")
	int? viewedText;
	bool? status;
	@JSONField(name: "share_url")
	String? shareUrl;
	@JSONField(name: "webview_url")
	String? webviewUrl;
	@JSONField(name: "total_user")
	int? totalUser;
	@JSONField(name: "published_at")
	String? publishedAt;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
}

@JsonSerializable()
class GetConditionsDataRecordGroup {

	GetConditionsDataRecordGroup();

	factory GetConditionsDataRecordGroup.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordGroupFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordGroupToJson(this);

	int? id;
	String? code;
	String? name;
	String? description;
}

@JsonSerializable()
class GetConditionsDataRecordCategory {

	GetConditionsDataRecordCategory();

	factory GetConditionsDataRecordCategory.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordCategoryFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordCategoryToJson(this);

	GetConditionsDataRecordCategoryCurrent? current;
	List<GetConditionsDataRecordCategoryItems>? items;
}

@JsonSerializable()
class GetConditionsDataRecordCategoryCurrent {

	GetConditionsDataRecordCategoryCurrent();

	factory GetConditionsDataRecordCategoryCurrent.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordCategoryCurrentFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordCategoryCurrentToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
	dynamic? description;
	@JSONField(name: "meta_title")
	dynamic? metaTitle;
	@JSONField(name: "meta_description")
	dynamic? metaDescription;
	@JSONField(name: "meta_keyword")
	dynamic? metaKeyword;
}

@JsonSerializable()
class GetConditionsDataRecordCategoryItems {

	GetConditionsDataRecordCategoryItems();

	factory GetConditionsDataRecordCategoryItems.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordCategoryItemsFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordCategoryItemsToJson(this);

	int? id;
	String? code;
	bool? status;
	int? position;
	String? name;
}

@JsonSerializable()
class GetConditionsDataRecordFields {

	GetConditionsDataRecordFields();

	factory GetConditionsDataRecordFields.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordFieldsFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordFieldsToJson(this);

	String? label;
	String? name;
	GetConditionsDataRecordFieldsType? type;
	@JSONField(name: "has_lang")
	bool? hasLang;
	bool? require;
	dynamic? options;
	String? lang;
}

@JsonSerializable()
class GetConditionsDataRecordFieldsType {

	GetConditionsDataRecordFieldsType();

	factory GetConditionsDataRecordFieldsType.fromJson(Map<String, dynamic> json) => $GetConditionsDataRecordFieldsTypeFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsDataRecordFieldsTypeToJson(this);

	int? id;
	String? name;
	String? value;
}

@JsonSerializable()
class GetConditionsBench {

	GetConditionsBench();

	factory GetConditionsBench.fromJson(Map<String, dynamic> json) => $GetConditionsBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetConditionsBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
