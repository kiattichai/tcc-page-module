import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_upload_image_entity.g.dart';

@JsonSerializable()
class ResponseUploadImageEntity {
  ResponseUploadImageEntity();

  factory ResponseUploadImageEntity.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadImageEntityFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadImageEntityToJson(this);

  ResponseUploadImageData? data;
  ResponseUploadImageBench? bench;
}

@JsonSerializable()
class ResponseUploadImageData {
  ResponseUploadImageData();

  factory ResponseUploadImageData.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadImageDataFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadImageDataToJson(this);

  String? message;
}

@JsonSerializable()
class ResponseUploadImageBench {
  ResponseUploadImageBench();

  factory ResponseUploadImageBench.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadImageBenchFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadImageBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
