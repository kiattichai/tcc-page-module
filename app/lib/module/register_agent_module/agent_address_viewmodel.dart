import 'package:app/core/base_view_model.dart';
import 'package:app/model/get_address_entity.dart';
import 'package:app/model/get_district_entity.dart';
import 'package:app/module/register_agent_module/model/get_cities_entity.dart';
import 'package:app/module/register_agent_module/model/get_districts_entity.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';
import 'package:app/module/register_agent_module/model/get_province_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class AgentAddressViewModel extends BaseViewModel {
  Map<String, dynamic> cardData;
  TextEditingController province = TextEditingController();
  TextEditingController cities = TextEditingController();
  TextEditingController district = TextEditingController();
  TextEditingController zipCode = TextEditingController();
  TextEditingController detailAddress = TextEditingController();
  GetProfileAgentData? getProfileAgentData;
  GetProvinceData? getProvinceData;
  GetCitiesData? getCitiesData;
  GetDistrictsData? getDistrictsData;
  List<GetProvinceDataRecord> provinceList = [];
  List<GetCitiesDataRecord> citiesList = [];
  List<GetDistrictsDataRecord> districtsList = [];
  int? provinceId;
  int? citiesId;
  int? districtId;

  bool formSuccess = false;
  String? isUpdate;
  Function()? showUpdateSuccess;

  AgentAddressViewModel(this.cardData, this.isUpdate);

  @override
  void postInit() {
    super.postInit();
    if (isUpdate!.isNotEmpty) {
      getProfile();
    } else {
      getProvince();
    }
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
  }

  void getProfile() async {
    catchError(() async {
      setLoading(true);
      getProfileAgentData = await di.agentRepository.getProfile();
      if (getProfileAgentData?.agent != null) {
        provinceId = getProfileAgentData?.agent?.province?.id;
        citiesId = getProfileAgentData?.agent?.city?.id;
        districtId = getProfileAgentData?.agent?.district?.id;
        province.text = getProfileAgentData?.agent?.province?.name ?? '';
        cities.text = getProfileAgentData?.agent?.city?.name ?? '';
        district.text = getProfileAgentData?.agent?.district?.name ?? '';
        zipCode.text = getProfileAgentData?.agent?.zipCode ?? '';
        detailAddress.text = getProfileAgentData?.agent?.address ?? '';

        getProvince();
        getCitiesById(provinceId!);
        getDistrictById(citiesId);
      }
      setLoading(false);
    });
  }

  void getProvince() async {
    catchError(() async {
      setLoading(true);
      getProvinceData = await di.agentRepository.getProvince(209);
      if (getProvinceData?.record != null) {
        provinceList.clear();
        provinceList.addAll(getProvinceData?.record ?? []);
      }
      setLoading(false);
      notifyListeners();
    });
    checkValidate();
  }

  void provinceIndex(int index, int province_Id) {
    provinceId = province_Id;
    province.text = provinceList[index].name ?? '';
    cities.clear();
    citiesList.clear();
    district.clear();
    districtsList.clear();
    zipCode.clear();
    notifyListeners();

    if (province_Id != -1) {
      catchError(() async {
        setLoading(true);
        getCitiesData = await di.agentRepository.getCities(province_Id);
        if (getCitiesData != null) {
          citiesList.addAll(getCitiesData?.record ?? []);
        }
        setLoading(false);
      });
    }
    checkValidate();
  }

  void citiesIndex(int index, int cityId) {
    print(index);
    print(cityId);
    citiesId = cityId;
    cities.text = citiesList[index].name ?? '';
    district.clear();
    zipCode.clear();
    districtsList.clear();
    notifyListeners();

    if (cityId != -1) {
      catchError(() async {
        setLoading(true);
        getDistrictsData = await di.agentRepository.getDistricts(cityId);
        if (getDistrictsData != null) {
          districtsList.addAll(getDistrictsData?.record ?? []);
        }
        setLoading(false);
      });
    }
    checkValidate();
  }

  void districtsIndex(int index, int district_Id) {
    districtId = district_Id;
    district.text = districtsList[index].name ?? '';
    zipCode.text = districtsList[index].zipCode!;
    notifyListeners();
    checkValidate();
  }

  checkValidate() {
    if (province.text.isEmpty ||
        cities.text.isEmpty ||
        district.text.isEmpty ||
        zipCode.text.isEmpty ||
        detailAddress.text.isEmpty) {
      formSuccess = false;
    } else {
      formSuccess = true;
    }
    notifyListeners();
  }

  void getCitiesById(int idProvince) {
    citiesList.clear();
    catchError(() async {
      getCitiesData = await di.agentRepository.getCities(idProvince);
      if (getCitiesData != null) {
        citiesList.addAll(getCitiesData?.record ?? []);
      }
    });
  }

  void getDistrictById(int? citiesId) {
    catchError(() async {
      getDistrictsData = await di.agentRepository.getDistricts(citiesId!);
      if (getDistrictsData != null) {
        districtsList.addAll(getDistrictsData?.record ?? []);
      }
    });
  }

  void updateAgent(BuildContext context) async {
    Map<String, dynamic> params = {
      "address": {
        "detail": detailAddress.text,
        "district_id": districtId,
        "city_id": citiesId,
        "province_id": provinceId,
        "country_id": 209,
        "zip_code": zipCode.text
      },
    };
    catchError(() async {
      final resultUpdate = await di.agentRepository.updateAgent(params);
      if (resultUpdate != null) {
        await showUpdateSuccess!();
        WrapNavigation.instance.pop(context, data: 'update');
      }
    });
  }
}
