import 'dart:convert';
import 'dart:io';

import 'package:app/core/base_view_model.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';
import 'package:app/utils/image_picker_utils.dart';
import 'package:app/utils/thai_id_validator.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:app/globals.dart' as global;

class AgentCardViewModel extends BaseViewModel {
  TextEditingController fistName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController idNumber = TextEditingController();
  TextEditingController email = TextEditingController();
  GetProfileAgentData? getProfileAgentData;
  PickedFile? pickedFile;
  File? image;
  bool formSuccess = false;
  String? isUpdate;
  String? imageName;
  var now;

  Function()? showUpdateSuccess;

  AgentCardViewModel(this.isUpdate);
  @override
  void postInit() {
    super.postInit();
    now = DateTime.now();
    getProfile();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }

  void getProfile() async {
    catchError(() async {
      setLoading(true);
      getProfileAgentData = await di.agentRepository.getProfile();
      if (getProfileAgentData != null) {
        fistName.text = getProfileAgentData?.firstName ?? '';
        lastName.text = getProfileAgentData?.lastName ?? '';
        idNumber.text = getProfileAgentData?.idcard ?? '';
        email.text = getProfileAgentData?.email ?? '';
      }
      setLoading(false);
    });
  }

  void picImage() async {
    final pathImage = await ImagePickerUtils.pickImage();
    if (pathImage != null) {
      image = File(pathImage);
      String fname = image!.path.substring(image!.path.length - 20, image!.path.length);
      imageName = fname;
    }
    checkValidate();
    notifyListeners();
  }

  void checkValidate() {
    if (isUpdate!.isEmpty) {
      if (fistName.text.isNotEmpty &&
          lastName.text.isNotEmpty &&
          idNumber.text.isNotEmpty &&
          (image?.path.isNotEmpty ?? false) &&
          idNumber.text.length == 13 &&
          ThaiIdValidator.validate(idNumber.text) &&
          validateEmail(email.text)) {
        formSuccess = true;
      } else {
        formSuccess = false;
      }
    } else {
      if (fistName.text.isNotEmpty &&
          lastName.text.isNotEmpty &&
          idNumber.text.isNotEmpty &&
          idNumber.text.length == 13 &&
          ThaiIdValidator.validate(idNumber.text) &&
          validateEmail(email.text)) {
        formSuccess = true;
      } else {
        formSuccess = false;
      }
    }

    notifyListeners();
  }

  void updateAgent(BuildContext context) async {
    Map<String, dynamic> params = {
      "name": fistName.text,
      "surname": lastName.text,
      "thai_id": idNumber.text,
      "email": email.text,
    };
    catchError(() async {
      setLoading(true);
      final result = await di.agentRepository.updateAgent(params);
      if (result != null) {
        if (image!.path.isNotEmpty) {
          ImageUploadRequest imageCardUpload =
              await ImagePickerUtils.getImageUploadRequest(image!.path);
          params.clear();
          params = {
            "document": {
              "type": "idcard",
              "name": imageCardUpload.fileName,
            },
            "image": imageCardUpload.fileData,
          };
          await di.agentRepository.uploadImage(params);
        }
      }

      setLoading(false);
    });
    await showUpdateSuccess!();
    WrapNavigation.instance.pop(context, data: 'update');
  }

  validateEmail(String? value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp('$pattern');
    if (regExp.hasMatch(value!)) {
      return true;
    } else {
      return false;
    }
  }
}
