import 'package:app/model/get_address_entity.dart';
import 'package:app/module/register_agent_module/model/get_province_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class ProvinceListWidget extends StatefulWidget with TixRoute {
  final List<GetProvinceDataRecord>? provinceList;
  final int? provinceId;
  ProvinceListWidget({Key? key, this.provinceList, this.provinceId}) : super(key: key);

  @override
  _ProvinceListWidgetState createState() => _ProvinceListWidgetState();

  @override
  String buildPath() {
    return '/province_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => ProvinceListWidget(
              provinceId: data['selected'],
              provinceList: data['provinceList'],
            ));
  }
}

class _ProvinceListWidgetState extends State<ProvinceListWidget> {
  int? selectIndex;
  int? provinceId;
  @override
  void initState() {
    super.initState();
    provinceId = widget.provinceId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.red),
          title: new Text(
            "เลือกจังหวัด",
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff4a4a4a),
              fontSize: 18,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.provinceList?.length,
                  itemBuilder: (context, index) {
                    var item = widget.provinceList?[index];
                    return ListTile(
                      title: Text(
                        '${item?.name}',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      trailing: provinceId != -1 && provinceId == item!.id
                          ? Icon(
                              Icons.check,
                              color: Colors.green,
                            )
                          : SizedBox(),
                      onTap: () {
                        setState(() {
                          provinceId = item?.id;
                          selectIndex = index;
                        });
                      },
                    );
                  })
            ],
          ),
        ),
        bottomNavigationBar: InkWell(
          child: Container(
            width: 343,
            height: 42,
            margin: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 10.0 + MediaQuery.of(context).padding.bottom),
            decoration: new BoxDecoration(
                color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
            child: Center(
              child: new Text(
                "บันทึก",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xffffffff),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
          ),
          onTap: () {
            Map data = {"index": selectIndex, "id": provinceId};
            //TixNavigate.instance.pop(data: data);
            WrapNavigation.instance.pop(context, data: data);
          },
        ));
  }
}
