import 'package:app/module/register_agent_module/model/get_cities_entity.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:tix_navigate/tix_navigate.dart';

class CitiesListWidget extends StatefulWidget with TixRoute {
  final List<GetCitiesDataRecord>? citiesList;
  final int? citiesId;

  CitiesListWidget({Key? key, this.citiesList, this.citiesId}) : super(key: key);

  @override
  _CitiesListWidgetState createState() => _CitiesListWidgetState();

  @override
  String buildPath() {
    return '/cities_list';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) =>
            CitiesListWidget(citiesId: data['selected'], citiesList: data['citiesList']));
  }
}

class _CitiesListWidgetState extends State<CitiesListWidget> {
  int? selectIndex;
  int? citiesId;
  @override
  void initState() {
    super.initState();
    citiesId = widget.citiesId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.red),
          title: new Text(
            "เลือกอำเภอ",
            style: TextStyle(
              fontFamily: 'SukhumvitSet-Text',
              color: Color(0xff4a4a4a),
              fontSize: 18,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.citiesList?.length,
                  itemBuilder: (context, index) {
                    var item = widget.citiesList?[index];
                    return ListTile(
                      title: Text(
                        '${item?.name}',
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      trailing: citiesId != -1 && citiesId == item!.id
                          ? Icon(
                              Icons.check,
                              color: Colors.green,
                            )
                          : SizedBox(),
                      onTap: () {
                        setState(() {
                          citiesId = item?.id;
                          selectIndex = index;
                        });
                      },
                    );
                  })
            ],
          ),
        ),
        bottomNavigationBar: InkWell(
          child: Container(
            width: 343,
            height: 42,
            margin: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 10.0 + MediaQuery.of(context).padding.bottom),
            decoration: new BoxDecoration(
                color: Color(0xffda3534), borderRadius: BorderRadius.circular(21)),
            child: Center(
              child: new Text(
                "บันทึก",
                style: TextStyle(
                  fontFamily: 'SukhumvitSet-Text',
                  color: Color(0xffffffff),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
          ),
          onTap: () {
            Map data = {"index": selectIndex, "id": citiesId};
            //TixNavigate.instance.pop(data: data);
            WrapNavigation.instance.pop(context, data: data);
          },
        ));
  }
}
