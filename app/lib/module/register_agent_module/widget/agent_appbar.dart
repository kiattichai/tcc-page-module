import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';

class AgentAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? name;
  final bool showActionButton;
  final Widget actionButton;
  final Function? onBack;

  AgentAppBar(
      {Key? key,
      required this.name,
      this.showActionButton = false,
      this.actionButton = const SizedBox(),
      this.onBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      backgroundColor: Colors.white,
      leading: GestureDetector(
        onTap: () {
          if (onBack != null) {
            onBack!();
          } else {
            WrapNavigation.instance.pop(context);
          }
        },
        child: Image(
          image: AssetImage("assets/path.png"),
          width: 18,
          height: 14,
        ),
      ),
      title: Container(
        margin: EdgeInsets.only(bottom: 5),
        child: new Text(name ?? '',
            style: TextStyle(
              fontFamily: 'Thonburi-Bold',
              color: Color(0xff4a4a4a),
              fontSize: 18,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            )),
      ),
      actions: <Widget>[actionButton],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight - 15);
}
