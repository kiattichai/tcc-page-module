import 'package:app/core/base_view_model.dart';
import 'package:app/module/register_agent_module/model/get_conditions_entity.dart';

class RegisterAgentTermsViewModel extends BaseViewModel{

  GetConditionsData? getConditionsData;
  List<GetConditionsDataRecord> conditionsList = [];

  RegisterAgentTermsViewModel();

  @override
  void postInit() {
    super.postInit();
    conditionsList.clear();
    getConditions();
  }

  void getConditions() {
    catchError(()async{
      setLoading(true);
      getConditionsData = await di.agentRepository.getConditions();
      if(getConditionsData!=null){
        conditionsList.addAll(getConditionsData?.record ?? []);
      }
      setLoading(false);
    });

  }


}