import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/register_agent_module/agent_card_screen.dart';
import 'package:app/module/register_agent_module/agent_adress_screen.dart';
import 'package:app/module/register_agent_module/agent_bank_screen.dart';
import 'package:app/module/register_agent_module/agent_detail_viewmodel.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AgentDetailScreen extends StatefulWidget with TixRoute {
  AgentDetailScreen();

  @override
  _AgentDetailScreenState createState() => _AgentDetailScreenState();

  @override
  String buildPath() {
    return '/agent_menu';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => AgentDetailScreen());
  }
}

class _AgentDetailScreenState extends BaseStateProvider<AgentDetailScreen, AgentDetailViewModel> {
  @override
  void initState() {
    viewModel = AgentDetailViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appState = Provider.of(context);
    return WillPopScope(
      onWillPop: () async {
        SystemNavigator.pop();
        return false;
      },
      child: BaseWidget<AgentDetailViewModel>(
          builder: (context, model, child) {
            return Scaffold(
              appBar: AgentAppBar(
                onBack: () {
                  if (WrapNavigation.instance.canPop(context)) {
                    WrapNavigation.instance.pop(context);
                  } else {
                    SystemNavigator.pop();
                    appState.channel.invokeMethod('pop');
                  }
                },
                name: 'ข้อมูลตัวแทน',
              ),
              body: Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  children: [
                    //TODO: รอการตรวจสอบ
                    viewModel.getProfileAgentData?.agent?.documentStatus?.id == 1
                        ? Container(
                            width: Screen.width,
                            height: 69,
                            margin: EdgeInsets.only(bottom: 16),
                            decoration: new BoxDecoration(
                              color: Color(0xfffef7ea),
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.orange.shade100),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 25.0, right: 14, top: 16),
                                  child: Image(
                                    image: AssetImage('assets/group_6.png'),
                                    width: 21,
                                    height: 16,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 16,
                                    ),
                                    new Text("รอการตรวจสอบ ",
                                        style: TextStyle(
                                          fontFamily: 'Thonburi-Bold',
                                          color: Color(0xffc65f2f),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new Text("อาจใช้เวลาในการตรวจสอบ 1- 2 ชั่วโมง",
                                        style: TextStyle(
                                          fontFamily: 'Thonburi-Bold',
                                          color: Color(0xff757575),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        ))
                                  ],
                                ),
                              ],
                            ))
                        : SizedBox(),

                    //TODO: remark
                    viewModel.getProfileAgentData?.agent?.documentStatus?.id == 3
                        ? Container(
                            width: Screen.width,
                            margin: EdgeInsets.only(bottom: 16),
                            padding: EdgeInsets.only(bottom: 10),
                            decoration: new BoxDecoration(
                              color: Color(0xfffdede9),
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.red.shade100),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 25.0, right: 14, top: 16),
                                  child: Image(
                                    image: AssetImage('assets/error_warning_line.png'),
                                    width: 21,
                                    height: 21,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 16,
                                    ),
                                    new Text("ไม่ผ่านการตรวจสอบ ",
                                        style: TextStyle(
                                          fontFamily: 'Thonburi-Bold',
                                          color: Color(0xffd92011),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    new Text("- ${viewModel.getProfileAgentData?.agent?.remark}",
                                        style: TextStyle(
                                          fontFamily: 'Thonburi-Bold',
                                          color: Color(0xff120e0e),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        )),
                                  ],
                                ),
                              ],
                            ))
                        : SizedBox(),

                    //TODO: Update Card id
                    InkWell(
                      onTap: () async {
                        final result = await WrapNavigation.instance.pushNamed(
                            context, AgentCardScreen(),
                            arguments: {'isUpdate': 'update'});
                        if (result != null) {
                          viewModel.getProfile();
                        }
                      },
                      child: Container(
                          width: Screen.width,
                          height: 69,
                          decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.grey.shade200)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 25.0, right: 14, top: 16),
                                child: Image(
                                  image: AssetImage('assets/icon_user.png'),
                                  width: 21,
                                  height: 21,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 16,
                                  ),
                                  new Text("ข้อมูลส่วนตัว",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff545454),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  new Text("ชื่อและข้อมูลบัตรประชาชน",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff757575),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                ],
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(top: 23.0),
                                child: Icon(Icons.chevron_right_outlined, color: Color(0xff545454)),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                            ],
                          )),
                    ),
                    SizedBox(
                      height: 16,
                    ),

                    //TODO: Update Address
                    InkWell(
                      onTap: () async {
                        final result = await WrapNavigation.instance.pushNamed(
                            context, AgentAddressScreen(),
                            arguments: {'isUpdate': 'update'});
                        if (result != null) {
                          viewModel.getProfile();
                        }
                      },
                      child: Container(
                          width: Screen.width,
                          height: 69,
                          decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.grey.shade200)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 25.0, right: 14, top: 16),
                                child: Image(
                                  image: AssetImage('assets/home_4_line.png'),
                                  width: 21,
                                  height: 21,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 16,
                                  ),
                                  new Text("ที่อยู่ปัจจุบัน",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff545454),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  new Text("สามารถแก้ไขที่อยู่ได้",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff757575),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                ],
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(top: 23.0),
                                child: Icon(Icons.chevron_right_outlined, color: Color(0xff545454)),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                            ],
                          )),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    InkWell(
                      onTap: () async {
                        final result = await WrapNavigation.instance.pushNamed(
                            context, AgentBankScreen(),
                            arguments: {'isUpdate': 'update'});
                        if (result != null) {
                          viewModel.getProfile();
                        }
                      },
                      child: Container(
                          width: Screen.width,
                          height: 69,
                          decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: Colors.grey.shade200)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 25.0, right: 14, top: 16),
                                child: Image(
                                  image: AssetImage('assets/bank_card_line.png'),
                                  width: 21,
                                  height: 21,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 16,
                                  ),
                                  new Text("บัญชีธนาคาร",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff545454),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  new Text("สามารถแก้ไขได้",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xff757575),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                ],
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(top: 23.0),
                                child: Icon(Icons.chevron_right_outlined, color: Color(0xff545454)),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            );
          },
          model: viewModel),
    );
  }
}
