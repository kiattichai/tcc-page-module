import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/agent_adress_screen.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/thai_id_validator.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'agent_card_viewmodel.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:app/globals.dart' as global;

class AgentCardScreen extends StatefulWidget with TixRoute {
  final String? isUpdate;
  AgentCardScreen({Key? key, this.isUpdate}) : super(key: key);

  @override
  _AgentCardScreenState createState() => _AgentCardScreenState();

  @override
  String buildPath() {
    return '/agent_card';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => AgentCardScreen(isUpdate: data['isUpdate']));
  }
}

class _AgentCardScreenState extends BaseStateProvider<AgentCardScreen, AgentCardViewModel> {
  @override
  final FocusNode _nodeTextIdCard = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: false,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeTextIdCard,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 20, top: 8, bottom: 8),
                  child: Text(
                    "ตกลง",
                    textScaleFactor: MediaQuery.of(context).textScaleFactor > 1.2
                        ? 1.1
                        : MediaQuery.of(context).textScaleFactor,
                    style: TextStyle(
                      fontFamily: 'Kanit',
                      color: Color(0xffda3534),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              );
            }
          ],
        ),
      ],
    );
  }

  void initState() {
    super.initState();
    viewModel = AgentCardViewModel(widget.isUpdate ?? '');
    viewModel.showUpdateSuccess = showUpdateSuccess;
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appState = Provider.of(context);
    return BaseWidget<AgentCardViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
              inAsyncCall: viewModel.loading,
              color: Colors.white,
              opacity: 1.0,
              progressIndicator: CircleLoading(),
              child: GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: Scaffold(
                  appBar: AgentAppBar(
                    onBack: () {
                      if (WrapNavigation.instance.canPop(context)) {
                        WrapNavigation.instance.pop(context);
                      } else {
                        SystemNavigator.pop();
                        appState.channel.invokeMethod('pop');
                      }
                    },
                    name: viewModel.isUpdate!.isNotEmpty ? 'แก้ไขข้อมูล' : 'สมัครเป็นตัวแทน',
                  ),
                  body: KeyboardActions(
                    config: _buildConfig(context),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 14),
                            Text("ข้อมูลส่วนตัว",
                                style: TextStyle(
                                  fontFamily: 'Thonburi-Bold',
                                  color: Color(0xff2b2b2b),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            SizedBox(height: 5),
                            Text("ข้อมูลจำเป็นต้องระบุให้ครบ",
                                style: TextStyle(
                                  fontFamily: 'Thonburi-Bold',
                                  color: Color(0xff2b2b2b),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),

                            //TODO:ชื่อ
                            Container(
                              margin: EdgeInsets.only(top: 12),
                              child: TextFormField(
                                controller: viewModel.fistName,
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛a-zA-Z]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'ชื่อ',
                                  hintStyle: hintTextStyle(),
                                  suffixText: '',
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),

                            //TODO:นามสกุล
                            Container(
                              margin: EdgeInsets.only(top: 14),
                              child: TextFormField(
                                controller: viewModel.lastName,
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛a-zA-Z]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'นามสกุล',
                                  hintStyle: hintTextStyle(),
                                  suffixText: '',
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),

                            //TODO:เลขบัตรประชาชน
                            Container(
                              margin: EdgeInsets.only(top: 11),
                              child: Form(
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                child: TextFormField(
                                  focusNode: _nodeTextIdCard,
                                  controller: viewModel.idNumber,
                                  onChanged: (value) {
                                    viewModel.checkValidate();
                                  },
                                  validator: (value) {
                                    if (value!.length != 13) {
                                      return 'กรุณาป้อนข้อมูลให้ถูกต้อง';
                                    } else if (!ThaiIdValidator.validate(value)) {
                                      return 'กรุณาป้อนข้อมูลให้ถูกต้อง';
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp('[0-9.,_-]+')),
                                    LengthLimitingTextInputFormatter(13),
                                  ],
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.zero,
                                    hintText: 'เลขบัตรประจำตัวประชาชน',
                                    hintStyle: hintTextStyle(),
                                    suffixText: '',
                                    suffixStyle: suffixTextStyle(),
                                    enabledBorder: underlineStyle(),
                                    focusedBorder: underlineStyle(),
                                  ),
                                ),
                              ),
                            ),

                            //TODO:Email
                            Container(
                              margin: EdgeInsets.only(top: 11),
                              child: Form(
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                child: TextFormField(
                                  //focusNode: _nodeTextIdCard,
                                  controller: viewModel.email,
                                  onChanged: (value) {
                                    viewModel.checkValidate();
                                  },
                                  validator: (value) {
                                    if (viewModel.validateEmail(value)) {
                                      return null;
                                    } else {
                                      return 'กรุณาป้อนข้อมูลให้ถูกต้อง';
                                    }
                                  },
                                  inputFormatters: [
                                    //FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9.,_-]+')),
                                    LengthLimitingTextInputFormatter(30),
                                  ],
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.zero,
                                    hintText: 'อีเมล์',
                                    hintStyle: hintTextStyle(),
                                    suffixText: '',
                                    suffixStyle: suffixTextStyle(),
                                    enabledBorder: underlineStyle(),
                                    focusedBorder: underlineStyle(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),

                            //TODO:อัพโหลดบัตร
                            viewModel.getProfileAgentData?.group?.id == 2
                                ? SizedBox()
                                : Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: Text("สำเนาบัตรประชาชน",
                                        style: TextStyle(
                                          fontFamily: 'Thonburi-Bold',
                                          color: Color(0xff676767),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ),

                            viewModel.getProfileAgentData?.agent?.documentStatus != null
                                ? Container(
                                    margin: EdgeInsets.only(top: 16.0),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(6),
                                        child: Image.network(
                                          '${viewModel.getProfileAgentData?.agent?.documentIdcard}?v=${viewModel.now}',
                                          headers: {
                                            "Authorization":
                                                "Bearer ${global.userAuthen?.data?.accessToken}"
                                          },
                                          scale: 1,
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),

                            viewModel.getProfileAgentData?.group?.id == 2
                                ? SizedBox()
                                : InkWell(
                                    onTap: () {
                                      FocusScope.of(context).unfocus();
                                      viewModel.picImage();
                                    },
                                    child: Container(
                                      width: Screen.width,
                                      height: 44,
                                      margin: EdgeInsets.only(top: 10),
                                      padding: EdgeInsets.only(left: 8, right: 8),
                                      decoration: new BoxDecoration(
                                          color: Color(0xffffffff),
                                          borderRadius: BorderRadius.circular(4),
                                          border: Border.all(
                                              color: viewModel.image != null
                                                  ? Colors.green
                                                  : Colors.grey.shade300,
                                              width: 1)),
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Image(
                                              image: AssetImage('assets/add.png'),
                                              width: 16,
                                              height: 16,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Text(
                                                viewModel.image != null
                                                    ? "${viewModel.imageName}"
                                                    : "อัปโหลดบัตรประชาชน",
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontFamily: 'Thonburi-Bold',
                                                  color: Color(0xff676767),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                            viewModel.getProfileAgentData?.group?.id == 2
                                ? SizedBox()
                                : Container(
                                    width: Screen.width,
                                    height: 54,
                                    margin: EdgeInsets.only(top: 10),
                                    padding: EdgeInsets.only(left: 15, top: 7),
                                    decoration: new BoxDecoration(
                                        color: Color(0xfffef7ea),
                                        borderRadius: BorderRadius.circular(3)),
                                    child: Text(
                                      "เอกสารสำเนาบัตรประชาชนต้องเซ็นรับรองสำเนาถูกต้องด้วยลายมือเท่านั้น",
                                      style: TextStyle(
                                        fontFamily: 'Thonburi-Bold',
                                        color: Color(0xffc65f2f),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  bottomNavigationBar: InkWell(
                    onTap: () {
                      if (viewModel.formSuccess) {
                        Map<String, dynamic> cardData = {
                          'firstName': viewModel.fistName.text.toString(),
                          'lastName': viewModel.lastName.text.toString(),
                          'thai_id': viewModel.idNumber.text.toString(),
                          'email': viewModel.email.text.toString(),
                          'card_image': viewModel.image
                        };

                        if (viewModel.isUpdate!.isNotEmpty) {
                          viewModel.updateAgent(context);
                        } else {
                          WrapNavigation.instance
                              .pushNamed(context, AgentAddressScreen(), arguments: cardData);
                        }
                      }
                    },
                    child: Container(
                      width: Screen.width,
                      height: 39,
                      margin: EdgeInsets.only(
                          top: 16,
                          left: 16.0,
                          right: 16.0,
                          bottom: 16.0 + MediaQuery.of(context).padding.bottom),
                      decoration: BoxDecoration(
                          color: viewModel.formSuccess ? Color(0xffda3534) : Color(0xff989898),
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: Text(
                          viewModel.isUpdate!.isNotEmpty ? 'บันทึก' : "ถัดไป",
                          style: TextStyle(
                            fontFamily: 'Thonburi-Bold',
                            color: Color(0xffffffff),
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ));
        },
        model: viewModel);
  }

  void showUpdateSuccess() async {
    showing = true;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "สำเร็จ",
            description: 'แก้ไขข้อมูลเรียบร้อยแล้ว',
            buttonText: 'ตกลง',
          );
        });
  }
}

hintTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff676767),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

suffixTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff989898),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

underlineStyle() {
  return UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.grey.shade300, width: 1),
  );
}
