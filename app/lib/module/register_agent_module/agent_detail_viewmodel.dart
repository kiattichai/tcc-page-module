import 'package:app/core/base_view_model.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';


class AgentDetailViewModel extends BaseViewModel {
  GetProfileAgentData? getProfileAgentData;
  AgentDetailViewModel();

  @override
  void postInit() {
    super.postInit();
    getProfile();
  }

  void getProfile() async {
    catchError(()async{
      setLoading(true);
      getProfileAgentData = await di.agentRepository.getProfile();
      setLoading(false);
    });
  }
}