import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_bank_item.dart';
import 'package:app/module/public_page_module/edit_page_profile/province_list_screen.dart';
import 'package:app/module/public_page_module/widget/home/app_bar.dart';
import 'package:app/module/public_page_module/widget/home/custom_dialog.dart';
import 'package:app/module/register_agent_module/agent_bank_screen.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/module/register_agent_module/widget/cities_list_widget.dart';
import 'package:app/module/register_agent_module/widget/districts_list_widget.dart';
import 'package:app/module/register_agent_module/widget/province_list_widget.dart';
import 'package:app/utils/screen.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

import 'agent_address_viewmodel.dart';

class AgentAddressScreen extends StatefulWidget with TixRoute {
  final Map<String, dynamic>? cardData;
  final String? isUpdate;
  AgentAddressScreen({Key? key, this.cardData, this.isUpdate}) : super(key: key);

  @override
  _AgentAddressScreenState createState() => _AgentAddressScreenState();

  @override
  String buildPath() {
    return '/agent_address';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (builder) => AgentAddressScreen(
              cardData: data,
              isUpdate: data['isUpdate'],
            ));
  }
}

class _AgentAddressScreenState
    extends BaseStateProvider<AgentAddressScreen, AgentAddressViewModel> {
  final FocusNode _nodeTextPostCode = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: false,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeTextPostCode,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 20, top: 8, bottom: 8),
                  child: Text(
                    "ตกลง",
                    textScaleFactor: MediaQuery.of(context).textScaleFactor > 1.2
                        ? 1.1
                        : MediaQuery.of(context).textScaleFactor,
                    style: TextStyle(
                      fontFamily: 'Kanit',
                      color: Color(0xffda3534),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              );
            }
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    viewModel = AgentAddressViewModel(widget.cardData ?? {}, widget.isUpdate ?? '');
    viewModel.showUpdateSuccess = showUpdateSuccess;
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appState = Provider.of(context);
    return BaseWidget<AgentAddressViewModel>(
        builder: (context, model, child) {
          return ProgressHUD(
            inAsyncCall: viewModel.loading,
            color: Colors.white,
            opacity: 1.0,
            progressIndicator: CircleLoading(),
            child: GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: Scaffold(
                  appBar: AgentAppBar(
                    onBack: () {
                      if (WrapNavigation.instance.canPop(context)) {
                        WrapNavigation.instance.pop(context);
                      } else {
                        SystemNavigator.pop();
                        appState.channel.invokeMethod('pop');
                      }
                    },
                    name: viewModel.isUpdate!.isNotEmpty ? 'แก้ไขที่อยู่' : 'สมัครเป็นตัวแทน',
                  ),
                  body: KeyboardActions(
                    config: _buildConfig(context),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 14),
                            Text("ที่อยู่ปัจจุบัน",
                                style: TextStyle(
                                  fontFamily: 'Thonburi-Bold',
                                  color: Color(0xff2b2b2b),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            SizedBox(height: 5),
                            Text("ข้อมูลจำเป็นต้องระบุให้ครบ",
                                style: TextStyle(
                                  fontFamily: 'Thonburi-Bold',
                                  color: Color(0xff2b2b2b),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              child: TextFormField(
                                controller: viewModel.province,
                                onTap: () async {
                                  navigateToProvince();
                                },
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                readOnly: true,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛.,_-\s]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 6),
                                  hintText: 'จังหวัด',
                                  hintStyle: hintTextStyle(),
                                  suffixIcon: Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    size: 15,
                                    color: Color(0xff545454),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                    maxHeight: 13,
                                  ),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              child: TextFormField(
                                controller: viewModel.cities,
                                onTap: () async {
                                  if (viewModel.province.text.isEmpty) {
                                    navigateToProvince();
                                  } else {
                                    navigateToCities();
                                  }
                                },
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                readOnly: true,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛.,_-\s]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 6),
                                  hintText: 'เขต/อำเภอ',
                                  hintStyle: hintTextStyle(),
                                  suffixIcon: Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    size: 15,
                                    color: Color(0xff545454),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                    maxHeight: 13,
                                  ),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              child: TextFormField(
                                controller: viewModel.district,
                                onTap: () async {
                                  if (viewModel.province.text.isEmpty) {
                                    navigateToProvince();
                                  } else if (viewModel.cities.text.isEmpty) {
                                    navigateToCities();
                                  } else {
                                    navigateToDistricts();
                                  }
                                },
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                readOnly: true,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[ก-๛.,_-\s]+')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 6),
                                  hintText: 'แขวง/ตำบล',
                                  hintStyle: hintTextStyle(),
                                  suffixIcon: Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    size: 15,
                                    color: Color(0xff545454),
                                  ),
                                  suffixIconConstraints: BoxConstraints(
                                    maxHeight: 13,
                                  ),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              child: TextFormField(
                                focusNode: _nodeTextPostCode,
                                controller: viewModel.zipCode,
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[0-9]+')),
                                  LengthLimitingTextInputFormatter(5),
                                ],
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 6),
                                  hintText: 'รหัสไปรษณีย์',
                                  hintStyle: hintTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "รายละเอียดที่อยู่",
                              style: TextStyle(
                                fontFamily: 'Thonburi-Bold',
                                color: Color(0xff676767),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            Text(
                              "ห้องเลขที่, บ้านเลขที่, ตึก, ชื่อถนน",
                              style: TextStyle(
                                fontFamily: 'Thonburi-Bold',
                                color: Color(0xff989898),
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                controller: viewModel.detailAddress,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp('^[ก-๛0-9a-zA-Z.,-_() ]*')),
                                  LengthLimitingTextInputFormatter(30),
                                ],
                                onChanged: (value) {
                                  viewModel.checkValidate();
                                },
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: 'ใส่รายละเอียดที่อยู่',
                                  hintStyle: hintTextStyle(),
                                  suffixStyle: suffixTextStyle(),
                                  enabledBorder: underlineStyle(),
                                  focusedBorder: underlineStyle(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  bottomNavigationBar: InkWell(
                    onTap: () {
                      if (viewModel.formSuccess) {
                        if (viewModel.isUpdate!.isNotEmpty) {
                          viewModel.updateAgent(context);
                        } else {
                          Map<String, dynamic> addressData = {
                            'firstName': viewModel.cardData['firstName'].toString(),
                            'lastName': viewModel.cardData['lastName'].toString(),
                            'thai_id': viewModel.cardData['thai_id'].toString(),
                            'email': viewModel.cardData['email'].toString(),
                            'card_image': viewModel.cardData['card_image'],
                            'detail': viewModel.detailAddress.text.toString(),
                            'district_id': viewModel.districtId,
                            'city_id': viewModel.citiesId,
                            'province_id': viewModel.provinceId,
                            'country_id': 209,
                            'zip_code': viewModel.zipCode.text
                          };
                          WrapNavigation.instance
                              .pushNamed(context, AgentBankScreen(), arguments: addressData);
                        }
                      }
                    },
                    child: Container(
                      width: Screen.width,
                      height: 39,
                      margin: EdgeInsets.only(
                          top: 16,
                          left: 16.0,
                          right: 16.0,
                          bottom: 16.0 + MediaQuery.of(context).padding.bottom),
                      decoration: BoxDecoration(
                          color: viewModel.formSuccess ? Color(0xffda3534) : Color(0xff989898),
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: Text(
                          viewModel.isUpdate!.isNotEmpty ? 'บันทึก' : "ถัดไป",
                          style: TextStyle(
                            fontFamily: 'Thonburi-Bold',
                            color: Color(0xffffffff),
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
          );
        },
        model: viewModel);
  }

  navigateToProvince() async {
    final result = await WrapNavigation.instance.pushNamed(context, ProvinceListWidget(),
        arguments: {'selected': viewModel.provinceId, 'provinceList': viewModel.provinceList});
    if (result is Map) {
      print(result);
      viewModel.provinceIndex(result["index"], result["id"]);
    }
  }

  navigateToCities() async {
    final result = await WrapNavigation.instance.pushNamed(context, CitiesListWidget(),
        arguments: {'selected': viewModel.citiesId, 'citiesList': viewModel.citiesList});
    if (result is Map) {
      print(result);
      viewModel.citiesIndex(result["index"], result["id"]);
    }
  }

  navigateToDistricts() async {
    final result = await WrapNavigation.instance.pushNamed(context, DistrictsListWidget(),
        arguments: {'selected': viewModel.districtId, 'districtsList': viewModel.districtsList});
    if (result is Map) {
      viewModel.districtsIndex(result["index"], result["id"]);
    }
  }

  void showUpdateSuccess() async {
    showing = true;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomDialog(
            title: "สำเร็จ",
            description: 'แก้ไขข้อมูลเรียบร้อยแล้ว',
            buttonText: 'ตกลง',
          );
        });
  }
}

Widget buildSelectBox(String? hint, String? sub) {
  return Container(
    height: 48,
    decoration: BoxDecoration(
        border: Border(
      bottom: BorderSide(color: Colors.grey.shade300, width: 1),
    )),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        new Text(
          '${hint}',
          style: TextStyle(
            fontFamily: 'Thonburi-Bold',
            color: Color(0xff676767),
            fontSize: 14,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
          ),
        ),
        Spacer(),
        Text(
          "${sub} ",
          style: TextStyle(
            fontFamily: 'Thonburi-Bold',
            color: Color(0xff989898),
            fontSize: 14,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
          ),
        ),
        Icon(Icons.arrow_forward_ios_sharp, size: 15, color: Color(0xff545454)),
      ],
    ),
  );
}

hintTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff676767),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

suffixTextStyle() {
  return TextStyle(
    fontFamily: 'Thonburi-Bold',
    color: Color(0xff989898),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}

underlineStyle() {
  return UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.grey.shade300, width: 1),
  );
}
