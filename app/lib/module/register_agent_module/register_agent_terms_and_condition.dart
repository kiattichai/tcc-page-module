import 'package:app/core/base_state_ful.dart';
import 'package:app/core/base_widget.dart';
import 'package:app/module/register_agent_module/agent_card_screen.dart';
import 'package:app/module/register_agent_module/register_agent_terms_view_model.dart';
import 'package:app/module/register_agent_module/widget/agent_appbar.dart';
import 'package:app/utils/wrap_navigation.dart';
import 'package:app/widgets/circle_progress_indicator.dart';
import 'package:app/widgets/progress_hub_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:tix_navigate/tix_navigate.dart';

class RegisterAgentTermsAndCondition extends StatefulWidget with TixRoute {
  RegisterAgentTermsAndCondition();

  @override
  _RegisterAgentTermsAndConditionState createState() => _RegisterAgentTermsAndConditionState();

  @override
  String buildPath() {
    return '/register_agent_terms_and_condition';
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (builder) => RegisterAgentTermsAndCondition());
  }
}

class _RegisterAgentTermsAndConditionState
    extends BaseStateProvider<RegisterAgentTermsAndCondition, RegisterAgentTermsViewModel> {
  @override
  void initState() {
    viewModel = RegisterAgentTermsViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of(context);
    return WillPopScope(
        onWillPop: () async {
          SystemNavigator.pop();
          return false;
        },
        child: BaseWidget<RegisterAgentTermsViewModel>(
            model: viewModel,
            builder: (context, model, child) {
              return Scaffold(
                appBar: AgentAppBar(
                  onBack: () {
                    if (WrapNavigation.instance.canPop(context)) {
                      WrapNavigation.instance.pop(context);
                    } else {
                      SystemNavigator.pop();
                    }
                    appState.channel.invokeMethod('pop');
                  },
                  name: 'เงื่อนไขและข้อตกลง',
                ),
                body: ProgressHUD(
                  inAsyncCall: viewModel.loading,
                  color: Colors.white,
                  opacity: 1.0,
                  progressIndicator: CircleLoading(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: viewModel.conditionsList.length,
                          itemBuilder: (context, index) {
                            var item = viewModel.conditionsList[index];
                            return Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Html(data: item.fields![0].lang),
                                  Html(data: item.fields![1].lang),
                                ],
                              ),
                            );
                          }),
                    ),
                  ),
                ),
                bottomNavigationBar: InkWell(
                  child: Container(
                    width: double.infinity,
                    height: 42,
                    margin: EdgeInsets.only(
                        left: 16.0,
                        right: 16.0,
                        bottom: 16.0 + MediaQuery.of(context).padding.bottom),
                    decoration: new BoxDecoration(
                        color: Color(0xffda3534), borderRadius: BorderRadius.circular(5)),
                    child: Center(
                      child: new Text(
                        "รับทราบและยอมรับเงื่อนไข",
                        style: TextStyle(
                          fontFamily: 'SukhumvitSet-Text',
                          color: Color(0xffffffff),
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    if (viewModel.loading == false) {
                      WrapNavigation.instance
                          .pushNamed(context, AgentCardScreen(), arguments: {'': ''});
                    }
                  },
                ),
              );
            }));
  }
}
