import 'package:app/generated/json/get_organizer_store_detail_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetOrganizerStoreDetailEntity     {

	GetOrganizerStoreDetailEntity();

	factory GetOrganizerStoreDetailEntity.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailEntityToJson(this);

  GetOrganizerStoreDetailData? data;
  GetOrganizerStoreDetailBench? bench;
}

@JsonSerializable()
class GetOrganizerStoreDetailData     {

	GetOrganizerStoreDetailData();

	factory GetOrganizerStoreDetailData.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataToJson(this);

  int? id;
  String? name;
  String? slug;
  String? description;
  GetOrganizerStoreDetailDataType? type;
  GetOrganizerStoreDetailDataSection? section;
  GetOrganizerStoreDetailDataContact? contact;
  GetOrganizerStoreDetailDataCorporate? corporate;
  GetOrganizerStoreDetailDataBilling? billing;
  @JSONField(name: "accept_terms")
  bool? acceptTerms;
  @JSONField(name: "document_status")
  GetOrganizerStoreDetailDataDocumentStatus? documentStatus;
  @JSONField(name: "payment_status")
  GetOrganizerStoreDetailDataPaymentStatus? paymentStatus;
  @JSONField(name: "publish_status")
  GetOrganizerStoreDetailDataPublishStatus? publishStatus;
  bool? status;
  @JSONField(name: "created_at")
  GetOrganizerStoreDetailDataCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetOrganizerStoreDetailDataUpdatedAt? updatedAt;
  @JSONField(name: "published_at")
  GetOrganizerStoreDetailDataPublishedAt? publishedAt;
  List<GetOrganizerStoreDetailDataSettings>? settings;
  GetOrganizerStoreDetailDataImages? images;
  List<dynamic>? connects;
  GetOrganizerStoreDetailDataStaff? staff;
  GetOrganizerStoreDetailDataVenue? venue;
  List<GetOrganizerStoreDetailDataAttributes>? attributes;
  List<GetOrganizerStoreDetailDataTimes>? times;
  @JSONField(name: "times_display")
  List<dynamic>? timesDisplay;
  @JSONField(name: "time_open")
  GetOrganizerStoreDetailDataTimeOpen? timeOpen;
  dynamic? remark;
  bool? updated;
  @JSONField(name: "accept_fee")
  bool? acceptFee;
  @JSONField(name: "accept_fee_at")
  GetOrganizerStoreDetailDataAcceptFeeAt? acceptFeeAt;
  @JSONField(name: "average_rating")
  int? averageRating;
  @JSONField(name: "review_total")
  int? reviewTotal;
  @JSONField(name: "covid_verify")
  bool? covidVerify;
  GetOrganizerStoreDetailDataParking? parking;
  GetOrganizerStoreDetailDataSeats? seats;
  GetOrganizerStoreDetailDataPage? page;
  @JSONField(name: "is_sha_plus")
  bool? isShaPlus;
  @JSONField(name: "is_covid_free")
  bool? isCovidFree;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataType     {

	GetOrganizerStoreDetailDataType();

	factory GetOrganizerStoreDetailDataType.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataTypeFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataSection     {

	GetOrganizerStoreDetailDataSection();

	factory GetOrganizerStoreDetailDataSection.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataSectionFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataSectionToJson(this);

  int? id;
  String? text;
  @JSONField(name: "text_full")
  String? textFull;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataContact     {

	GetOrganizerStoreDetailDataContact();

	factory GetOrganizerStoreDetailDataContact.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataContactFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataContactToJson(this);

  @JSONField(name: "first_name")
  dynamic? firstName;
  @JSONField(name: "last_name")
  dynamic? lastName;
  @JSONField(name: "citizen_id")
  dynamic? citizenId;
  String? phone;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "phone_option")
  List<dynamic>? phoneOption;
  String? email;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataCorporate     {

	GetOrganizerStoreDetailDataCorporate();

	factory GetOrganizerStoreDetailDataCorporate.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataCorporateFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataCorporateToJson(this);

  dynamic? name;
  dynamic? branch;
  dynamic? phone;
  @JSONField(name: "tax_id")
  dynamic? taxId;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataBilling     {

	GetOrganizerStoreDetailDataBilling();

	factory GetOrganizerStoreDetailDataBilling.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataBillingFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataBillingToJson(this);

  dynamic? address;
  dynamic? district;
  dynamic? city;
  dynamic? province;
  dynamic? country;
  dynamic? zipcode;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataDocumentStatus     {

	GetOrganizerStoreDetailDataDocumentStatus();

	factory GetOrganizerStoreDetailDataDocumentStatus.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataDocumentStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataDocumentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataPaymentStatus     {

	GetOrganizerStoreDetailDataPaymentStatus();

	factory GetOrganizerStoreDetailDataPaymentStatus.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataPaymentStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataPaymentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataPublishStatus     {

	GetOrganizerStoreDetailDataPublishStatus();

	factory GetOrganizerStoreDetailDataPublishStatus.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataPublishStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataPublishStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataCreatedAt     {

	GetOrganizerStoreDetailDataCreatedAt();

	factory GetOrganizerStoreDetailDataCreatedAt.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataCreatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataUpdatedAt     {

	GetOrganizerStoreDetailDataUpdatedAt();

	factory GetOrganizerStoreDetailDataUpdatedAt.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataUpdatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
  @JSONField(name: "value_utc")
  String? valueUtc;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataPublishedAt     {

	GetOrganizerStoreDetailDataPublishedAt();

	factory GetOrganizerStoreDetailDataPublishedAt.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataPublishedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataPublishedAtToJson(this);
}

@JsonSerializable()
class GetOrganizerStoreDetailDataSettings     {

	GetOrganizerStoreDetailDataSettings();

	factory GetOrganizerStoreDetailDataSettings.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataSettingsFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataSettingsToJson(this);

  int? id;
  String? code;
  String? key;
  GetOrganizerStoreDetailDataSettingsValue? value;
  bool? serialize;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataSettingsValue     {

	GetOrganizerStoreDetailDataSettingsValue();

	factory GetOrganizerStoreDetailDataSettingsValue.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataSettingsValueFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataSettingsValueToJson(this);

  String? website;
  String? facebook;
  String? twitter;
  String? line;
  String? instagram;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataImages     {

	GetOrganizerStoreDetailDataImages();

	factory GetOrganizerStoreDetailDataImages.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataImagesToJson(this);

  GetOrganizerStoreDetailDataImagesLogo? logo;
  List<GetOrganizerStoreDetailDataImagesBanner>? banner;
  @JSONField(name: "corporate_logo")
  dynamic? corporateLogo;
  dynamic? signature;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataImagesLogo     {

	GetOrganizerStoreDetailDataImagesLogo();

	factory GetOrganizerStoreDetailDataImagesLogo.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataImagesLogoFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataImagesLogoToJson(this);

  String? id;
  int? position;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataImagesBanner     {

	GetOrganizerStoreDetailDataImagesBanner();

	factory GetOrganizerStoreDetailDataImagesBanner.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataImagesBannerFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataImagesBannerToJson(this);

  String? id;
  int? position;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataStaff     {

	GetOrganizerStoreDetailDataStaff();

	factory GetOrganizerStoreDetailDataStaff.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataStaffFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataStaffToJson(this);
}

@JsonSerializable()
class GetOrganizerStoreDetailDataVenue     {

	GetOrganizerStoreDetailDataVenue();

	factory GetOrganizerStoreDetailDataVenue.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataVenueFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataVenueToJson(this);

  int? id;
  double? lat;
  double? long;
  String? name;
  String? address;
  GetOrganizerStoreDetailDataVenueCountry? country;
  GetOrganizerStoreDetailDataVenueProvince? province;
  GetOrganizerStoreDetailDataVenueCity? city;
  GetOrganizerStoreDetailDataVenueDistrict? district;
  @JSONField(name: "zip_code")
  int? zipCode;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataVenueCountry     {

	GetOrganizerStoreDetailDataVenueCountry();

	factory GetOrganizerStoreDetailDataVenueCountry.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataVenueCountryFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataVenueCountryToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataVenueProvince     {

	GetOrganizerStoreDetailDataVenueProvince();

	factory GetOrganizerStoreDetailDataVenueProvince.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataVenueProvinceFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataVenueProvinceToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataVenueCity     {

	GetOrganizerStoreDetailDataVenueCity();

	factory GetOrganizerStoreDetailDataVenueCity.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataVenueCityFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataVenueCityToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataVenueDistrict     {

	GetOrganizerStoreDetailDataVenueDistrict();

	factory GetOrganizerStoreDetailDataVenueDistrict.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataVenueDistrictFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataVenueDistrictToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataAttributes     {

	GetOrganizerStoreDetailDataAttributes();

	factory GetOrganizerStoreDetailDataAttributes.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataAttributesFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataAttributesToJson(this);

  int? id;
  String? code;
  String? name;
  List<GetOrganizerStoreDetailDataAttributesItems>? items;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataAttributesItems     {

	GetOrganizerStoreDetailDataAttributesItems();

	factory GetOrganizerStoreDetailDataAttributesItems.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataAttributesItemsFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataAttributesItemsToJson(this);

  int? id;
  String? name;
  dynamic? value;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataTimes     {

	GetOrganizerStoreDetailDataTimes();

	factory GetOrganizerStoreDetailDataTimes.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataTimesFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataTimesToJson(this);

  int? day;
  @JSONField(name: "day_text")
  String? dayText;
  dynamic? open;
  dynamic? close;
  bool? status;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataTimeOpen     {

	GetOrganizerStoreDetailDataTimeOpen();

	factory GetOrganizerStoreDetailDataTimeOpen.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataTimeOpenFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataTimeOpenToJson(this);

  dynamic? status;
  dynamic? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataAcceptFeeAt     {

	GetOrganizerStoreDetailDataAcceptFeeAt();

	factory GetOrganizerStoreDetailDataAcceptFeeAt.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataAcceptFeeAtFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataAcceptFeeAtToJson(this);
}

@JsonSerializable()
class GetOrganizerStoreDetailDataParking     {

	GetOrganizerStoreDetailDataParking();

	factory GetOrganizerStoreDetailDataParking.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataParkingFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataParkingToJson(this);

  dynamic? total;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataSeats     {

	GetOrganizerStoreDetailDataSeats();

	factory GetOrganizerStoreDetailDataSeats.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataSeatsFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataSeatsToJson(this);

  dynamic? min;
  dynamic? max;
  String? text;
}

@JsonSerializable()
class GetOrganizerStoreDetailDataPage     {

	GetOrganizerStoreDetailDataPage();

	factory GetOrganizerStoreDetailDataPage.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailDataPageFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailDataPageToJson(this);

  int? id;
  @JSONField(name: "display_name")
  String? displayName;
  dynamic? abouts;
  dynamic? slug;
  @JSONField(name: "basic_id")
  String? basicId;
  @JSONField(name: "premium_id")
  dynamic? premiumId;
  int? type;
  @JSONField(name: "store_id")
  int? storeId;
  @JSONField(name: "user_id")
  int? userId;
  @JSONField(name: "total_followers")
  int? totalFollowers;
  @JSONField(name: "total_posts")
  int? totalPosts;
  @JSONField(name: "total_shares")
  int? totalShares;
  @JSONField(name: "total_views")
  int? totalViews;
  @JSONField(name: "total_reported")
  dynamic? totalReported;
  @JSONField(name: "user_ref")
  dynamic? userRef;
  dynamic? code;
  @JSONField(name: "is_verify")
  int? isVerify;
  @JSONField(name: "is_blocking")
  int? isBlocking;
  @JSONField(name: "staff_id")
  dynamic? staffId;
  dynamic? remark;
  @JSONField(name: "verify_at")
  dynamic? verifyAt;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  @JSONField(name: "deleted_at")
  dynamic? deletedAt;
  @JSONField(name: "deleted_by")
  dynamic? deletedBy;
}

@JsonSerializable()
class GetOrganizerStoreDetailBench     {

	GetOrganizerStoreDetailBench();

	factory GetOrganizerStoreDetailBench.fromJson(Map<String, dynamic> json) => $GetOrganizerStoreDetailBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetOrganizerStoreDetailBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
