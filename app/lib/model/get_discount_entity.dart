import 'package:app/generated/json/get_discount_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetDiscountEntity {
  GetDiscountEntity();

  factory GetDiscountEntity.fromJson(Map<String, dynamic> json) =>
      $GetDiscountEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountEntityToJson(this);

  GetDiscountData? data;
  GetDiscountBench? bench;
}

@JsonSerializable()
class GetDiscountData {
  GetDiscountData();

  factory GetDiscountData.fromJson(Map<String, dynamic> json) =>
      $GetDiscountDataFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountDataToJson(this);

  GetDiscountDataPagination? pagination;
  List<GetDiscountDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetDiscountDataPagination {
  GetDiscountDataPagination();

  factory GetDiscountDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetDiscountDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetDiscountDataRecord {
  GetDiscountDataRecord();

  factory GetDiscountDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetDiscountDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountDataRecordToJson(this);

  int? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? title;
  @JSONField(name: "promotion_type")
  String? promotionType;
  @JSONField(name: "discount_type")
  String? discountType;
  @JSONField(name: "discount_limit")
  int? discountLimit;
  @JSONField(name: "used_type")
  String? usedType;
  @JSONField(name: "used_total")
  int? usedTotal;
  @JSONField(name: "limit_type")
  bool? limitType;
  @JSONField(name: "limit_total")
  int? limitTotal;
  @JSONField(name: "user_limit_type")
  bool? userLimitType;
  @JSONField(name: "user_limit_total")
  int? userLimitTotal;
  int? cashback;
  @JSONField(name: "promotion_time")
  GetDiscountDataRecordPromotionTime? promotionTime;
  List<GetDiscountDataRecordRates>? rates;
  String? condition;
  List<dynamic>? images;
  bool? status;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  dynamic? meta;
}

@JsonSerializable()
class GetDiscountDataRecordPromotionTime {
  GetDiscountDataRecordPromotionTime();

  factory GetDiscountDataRecordPromotionTime.fromJson(
          Map<String, dynamic> json) =>
      $GetDiscountDataRecordPromotionTimeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetDiscountDataRecordPromotionTimeToJson(this);

  @JSONField(name: "start_date")
  String? startDate;
  @JSONField(name: "end_date")
  String? endDate;
  @JSONField(name: "text_full")
  String? textFull;
  @JSONField(name: "text_short")
  String? textShort;
  int? status;
  @JSONField(name: "status_text")
  String? statusText;
}

@JsonSerializable()
class GetDiscountDataRecordRates {
  GetDiscountDataRecordRates();

  factory GetDiscountDataRecordRates.fromJson(Map<String, dynamic> json) =>
      $GetDiscountDataRecordRatesFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountDataRecordRatesToJson(this);

  int? id;
  @JSONField(name: "value_start")
  int? valueStart;
  @JSONField(name: "value_end")
  int? valueEnd;
  int? discount;
}

@JsonSerializable()
class GetDiscountBench {
  GetDiscountBench();

  factory GetDiscountBench.fromJson(Map<String, dynamic> json) =>
      $GetDiscountBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetDiscountBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
