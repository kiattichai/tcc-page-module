import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_edit_profile_response_entity.g.dart';


@JsonSerializable()
class SaveEditProfileResponseEntity {

	SaveEditProfileResponseEntity();

	factory SaveEditProfileResponseEntity.fromJson(Map<String, dynamic> json) => $SaveEditProfileResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $SaveEditProfileResponseEntityToJson(this);

	SaveEditProfileResponseData? data;
	SaveEditProfileResponseBench? bench;
}

@JsonSerializable()
class SaveEditProfileResponseData {

	SaveEditProfileResponseData();

	factory SaveEditProfileResponseData.fromJson(Map<String, dynamic> json) => $SaveEditProfileResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $SaveEditProfileResponseDataToJson(this);

	String? message;
}

@JsonSerializable()
class SaveEditProfileResponseBench {

	SaveEditProfileResponseBench();

	factory SaveEditProfileResponseBench.fromJson(Map<String, dynamic> json) => $SaveEditProfileResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $SaveEditProfileResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
