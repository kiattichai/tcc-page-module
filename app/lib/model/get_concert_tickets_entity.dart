import 'package:app/generated/json/get_concert_tickets_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConcertTicketsEntity {
  GetConcertTicketsEntity();

  factory GetConcertTicketsEntity.fromJson(Map<String, dynamic> json) =>
      $GetConcertTicketsEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertTicketsEntityToJson(this);

  GetConcertTicketsData? data;
  GetConcertTicketsBench? bench;
}

@JsonSerializable()
class GetConcertTicketsData {
  GetConcertTicketsData();

  factory GetConcertTicketsData.fromJson(Map<String, dynamic> json) =>
      $GetConcertTicketsDataFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertTicketsDataToJson(this);

  List<GetConcertTicketsDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetConcertTicketsDataRecord {
  GetConcertTicketsDataRecord();

  factory GetConcertTicketsDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetConcertTicketsDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertTicketsDataRecordToJson(this);

  int? id;
  String? sku;
  String? name;
  String? description;
  dynamic? group;
  dynamic? zone;
  @JSONField(name: "cost_price")
  int? costPrice;
  @JSONField(name: "cost_price_text")
  String? costPriceText;
  int? price;
  @JSONField(name: "price_text")
  String? priceText;
  @JSONField(name: "compare_at_price")
  int? compareAtPrice;
  @JSONField(name: "compare_at_price_text")
  String? compareAtPriceText;
  bool? package;
  @JSONField(name: "per_package")
  int? perPackage;
  int? stock;
  int? quantity;
  @JSONField(name: "order_quantity")
  dynamic? orderQuantity;
  @JSONField(name: "diff_stock")
  int? diffStock;
  int? hold;
  int? position;
  int? points;
  int? weight;
  bool? status;
  @JSONField(name: "published_start")
  String? publishedStart;
  @JSONField(name: "published_end")
  String? publishedEnd;
  @JSONField(name: "gate_open")
  String? gateOpen;
  @JSONField(name: "gate_close")
  String? gateClose;
  @JSONField(name: "allow_order_min")
  int? allowOrderMin;
  @JSONField(name: "allow_order_max")
  int? allowOrderMax;
  dynamic? remark;
  @JSONField(name: "special_option")
  bool? specialOption;
  @JSONField(name: "service_charge")
  bool? serviceCharge;
  @JSONField(name: "service_fee")
  int? serviceFee;
  @JSONField(name: "has_ticket")
  bool? hasTicket;
  GetConcertTicketsDataRecordService? service;
  List<dynamic>? options;
  List<dynamic>? promotions;
  GetConcertTicketsDataRecordImage? image;
  @JSONField(name: "online_meeting")
  bool? onlineMeeting;
  List<dynamic>? meetings;
  dynamic? meta;
  @JSONField(name: "display_status")
  bool? displayStatus;
  @JSONField(name: "display_text")
  String? displayText;
}

@JsonSerializable()
class GetConcertTicketsDataRecordService {
  GetConcertTicketsDataRecordService();

  factory GetConcertTicketsDataRecordService.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertTicketsDataRecordServiceFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertTicketsDataRecordServiceToJson(this);

  bool? charge;
  @JSONField(name: "fee_value")
  int? feeValue;
  int? fee;
  @JSONField(name: "fee_text")
  String? feeText;
}

@JsonSerializable()
class GetConcertTicketsDataRecordImage {
  GetConcertTicketsDataRecordImage();

  factory GetConcertTicketsDataRecordImage.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertTicketsDataRecordImageFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertTicketsDataRecordImageToJson(this);
}

@JsonSerializable()
class GetConcertTicketsBench {
  GetConcertTicketsBench();

  factory GetConcertTicketsBench.fromJson(Map<String, dynamic> json) =>
      $GetConcertTicketsBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertTicketsBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
