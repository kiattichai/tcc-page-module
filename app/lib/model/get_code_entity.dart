import 'package:app/generated/json/get_code_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetCodeEntity {
  GetCodeEntity();

  factory GetCodeEntity.fromJson(Map<String, dynamic> json) =>
      $GetCodeEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetCodeEntityToJson(this);

  GetCodeData? data;
  GetCodeBench? bench;
}

@JsonSerializable()
class GetCodeData {
  GetCodeData();

  factory GetCodeData.fromJson(Map<String, dynamic> json) =>
      $GetCodeDataFromJson(json);

  Map<String, dynamic> toJson() => $GetCodeDataToJson(this);

  GetCodeDataPagination? pagination;
  List<GetCodeDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetCodeDataPagination {
  GetCodeDataPagination();

  factory GetCodeDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetCodeDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetCodeDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetCodeDataRecord {
  GetCodeDataRecord();

  factory GetCodeDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetCodeDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetCodeDataRecordToJson(this);

  int? id;
  String? code;
  bool? status;
  int? used;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  @JSONField(name: "sum_discount")
  int? sumDiscount;
}

@JsonSerializable()
class GetCodeBench {
  GetCodeBench();

  factory GetCodeBench.fromJson(Map<String, dynamic> json) =>
      $GetCodeBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetCodeBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
