import 'package:app/generated/json/get_ticket_scan_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetTicketScanEntity {
  GetTicketScanEntity();

  factory GetTicketScanEntity.fromJson(Map<String, dynamic> json) =>
      $GetTicketScanEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketScanEntityToJson(this);

  GetTicketScanData? data;
  GetTicketScanBench? bench;
}

@JsonSerializable()
class GetTicketScanData {
  GetTicketScanData();

  factory GetTicketScanData.fromJson(Map<String, dynamic> json) =>
      $GetTicketScanDataFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketScanDataToJson(this);

  GetTicketScanDataPagination? pagination;
  List<GetTicketScanDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetTicketScanDataPagination {
  GetTicketScanDataPagination();

  factory GetTicketScanDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetTicketScanDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketScanDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetTicketScanDataRecord {
  GetTicketScanDataRecord();

  factory GetTicketScanDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetTicketScanDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketScanDataRecordToJson(this);

  int? id;
  String? token;
  String? note;
  @JSONField(name: "last_login_at")
  String? lastLoginAt;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
}

@JsonSerializable()
class GetTicketScanBench {
  GetTicketScanBench();

  factory GetTicketScanBench.fromJson(Map<String, dynamic> json) =>
      $GetTicketScanBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketScanBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
