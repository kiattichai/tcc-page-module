import 'package:app/generated/json/get_review_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetReviewEntity {
  GetReviewEntity();

  factory GetReviewEntity.fromJson(Map<String, dynamic> json) =>
      $GetReviewEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewEntityToJson(this);

  GetReviewData? data;
  GetReviewBench? bench;
}

@JsonSerializable()
class GetReviewData {
  GetReviewData();

  factory GetReviewData.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataToJson(this);

  GetReviewDataPagination? pagination;
  List<GetReviewDataRecord>? record;
}

@JsonSerializable()
class GetReviewDataPagination {
  GetReviewDataPagination();

  factory GetReviewDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetReviewDataRecord {
  GetReviewDataRecord();

  factory GetReviewDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordToJson(this);

  int? id;
  String? description;
  GetReviewDataRecordUser? user;
  GetReviewDataRecordStore? store;
  int? rating;
  List<GetReviewDataRecordImages>? images;
  dynamic? remark;
  @JSONField(name: "created_at")
  GetReviewDataRecordCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetReviewDataRecordUpdatedAt? updatedAt;
}

@JsonSerializable()
class GetReviewDataRecordUser {
  GetReviewDataRecordUser();

  factory GetReviewDataRecordUser.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordUserFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordUserToJson(this);

  int? id;
  String? username;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  GetReviewDataRecordUserImage? image;
}

@JsonSerializable()
class GetReviewDataRecordUserImage {
  GetReviewDataRecordUserImage();

  factory GetReviewDataRecordUserImage.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordUserImageFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordUserImageToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}

@JsonSerializable()
class GetReviewDataRecordStore {
  GetReviewDataRecordStore();

  factory GetReviewDataRecordStore.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordStoreFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordStoreToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetReviewDataRecordImages {
  GetReviewDataRecordImages();

  factory GetReviewDataRecordImages.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordImagesFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordImagesToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}

@JsonSerializable()
class GetReviewDataRecordCreatedAt {
  GetReviewDataRecordCreatedAt();

  factory GetReviewDataRecordCreatedAt.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordCreatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetReviewDataRecordUpdatedAt {
  GetReviewDataRecordUpdatedAt();

  factory GetReviewDataRecordUpdatedAt.fromJson(Map<String, dynamic> json) =>
      $GetReviewDataRecordUpdatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewDataRecordUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetReviewBench {
  GetReviewBench();

  factory GetReviewBench.fromJson(Map<String, dynamic> json) =>
      $GetReviewBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
