import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_save_discount_entity.g.dart';

@JsonSerializable()
class ResponseSaveDiscountEntity {
  ResponseSaveDiscountEntity();

  factory ResponseSaveDiscountEntity.fromJson(Map<String, dynamic> json) =>
      $ResponseSaveDiscountEntityFromJson(json);

  Map<String, dynamic> toJson() => $ResponseSaveDiscountEntityToJson(this);

  ResponseSaveDiscountData? data;
  ResponseSaveDiscountBench? bench;
}

@JsonSerializable()
class ResponseSaveDiscountData {
  ResponseSaveDiscountData();

  factory ResponseSaveDiscountData.fromJson(Map<String, dynamic> json) =>
      $ResponseSaveDiscountDataFromJson(json);

  Map<String, dynamic> toJson() => $ResponseSaveDiscountDataToJson(this);

  String? message;
  int? id;
}

@JsonSerializable()
class ResponseSaveDiscountBench {
  ResponseSaveDiscountBench();

  factory ResponseSaveDiscountBench.fromJson(Map<String, dynamic> json) =>
      $ResponseSaveDiscountBenchFromJson(json);

  Map<String, dynamic> toJson() => $ResponseSaveDiscountBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
