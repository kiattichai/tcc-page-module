import 'package:app/generated/json/response_link_id_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class ResponseLinkIdEntity {

	ResponseLinkIdEntity();

	factory ResponseLinkIdEntity.fromJson(Map<String, dynamic> json) => $ResponseLinkIdEntityFromJson(json);

	Map<String, dynamic> toJson() => $ResponseLinkIdEntityToJson(this);

	ResponseLinkIdData? data;
	ResponseLinkIdBench? bench;
}

@JsonSerializable()
class ResponseLinkIdData {

	ResponseLinkIdData();

	factory ResponseLinkIdData.fromJson(Map<String, dynamic> json) => $ResponseLinkIdDataFromJson(json);

	Map<String, dynamic> toJson() => $ResponseLinkIdDataToJson(this);

	String? message;
	@JSONField(name: "promotion_id")
	int? promotionId;
}

@JsonSerializable()
class ResponseLinkIdBench {

	ResponseLinkIdBench();

	factory ResponseLinkIdBench.fromJson(Map<String, dynamic> json) => $ResponseLinkIdBenchFromJson(json);

	Map<String, dynamic> toJson() => $ResponseLinkIdBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
