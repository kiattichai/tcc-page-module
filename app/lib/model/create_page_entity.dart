import 'package:app/generated/json/create_page_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class CreatePageEntity {

	CreatePageEntity();

	factory CreatePageEntity.fromJson(Map<String, dynamic> json) => $CreatePageEntityFromJson(json);

	Map<String, dynamic> toJson() => $CreatePageEntityToJson(this);

	@JSONField(name: "display_name")
	String? displayName;
	String? category;
}
