import 'package:app/generated/json/save_venue_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveVenueEntity {
  SaveVenueEntity();

  factory SaveVenueEntity.fromJson(Map<String, dynamic> json) =>
      $SaveVenueEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueEntityToJson(this);

  int? id;
  @JSONField(name: "country_id")
  int? countryId;
  @JSONField(name: "store_id")
  String? storeId;
  SaveVenueName? name;
  SaveVenueAddress? address;
  @JSONField(name: "province_id")
  int? provinceId;
  @JSONField(name: "city_id")
  int? cityId;
  @JSONField(name: "district_id")
  int? districtId;
  @JSONField(name: "zip_code")
  String? zipCode;
  String? lat;
  String? long;
  bool? status;
  @JSONField(name: "default")
  bool? xDefault;
}

@JsonSerializable()
class SaveVenueName {
  SaveVenueName();

  factory SaveVenueName.fromJson(Map<String, dynamic> json) =>
      $SaveVenueNameFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueNameToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class SaveVenueAddress {
  SaveVenueAddress();

  factory SaveVenueAddress.fromJson(Map<String, dynamic> json) =>
      $SaveVenueAddressFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueAddressToJson(this);

  String? th;
  String? en;
}
