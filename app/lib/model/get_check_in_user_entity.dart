import 'package:app/generated/json/get_check_in_user_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetCheckInUserEntity {
  GetCheckInUserEntity();

  factory GetCheckInUserEntity.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserEntityToJson(this);

  GetCheckInUserData? data;
  GetCheckInUserBench? bench;
}

@JsonSerializable()
class GetCheckInUserData {
  GetCheckInUserData();

  factory GetCheckInUserData.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserDataFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserDataToJson(this);

  GetCheckInUserDataMeta? meta;
  GetCheckInUserDataPagination? pagination;
  List<GetCheckInUserDataRecord>? record;
}

@JsonSerializable()
class GetCheckInUserDataMeta {
  GetCheckInUserDataMeta();

  factory GetCheckInUserDataMeta.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserDataMetaFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserDataMetaToJson(this);

  @JSONField(name: "checkin_total")
  int? checkinTotal;
  @JSONField(name: "user_total")
  int? userTotal;
}

@JsonSerializable()
class GetCheckInUserDataPagination {
  GetCheckInUserDataPagination();

  factory GetCheckInUserDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetCheckInUserDataRecord {
  GetCheckInUserDataRecord();

  factory GetCheckInUserDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserDataRecordToJson(this);

  int? id;
  GetCheckInUserDataRecordUser? user;
  @JSONField(name: "created_at")
  GetCheckInUserDataRecordCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetCheckInUserDataRecordUpdatedAt? updatedAt;
}

@JsonSerializable()
class GetCheckInUserDataRecordUser {
  GetCheckInUserDataRecordUser();

  factory GetCheckInUserDataRecordUser.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserDataRecordUserFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserDataRecordUserToJson(this);

  int? id;
  String? username;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  GetCheckInUserDataRecordUserImage? image;
}

@JsonSerializable()
class GetCheckInUserDataRecordUserImage {
  GetCheckInUserDataRecordUserImage();

  factory GetCheckInUserDataRecordUserImage.fromJson(
          Map<String, dynamic> json) =>
      $GetCheckInUserDataRecordUserImageFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetCheckInUserDataRecordUserImageToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}

@JsonSerializable()
class GetCheckInUserDataRecordCreatedAt {
  GetCheckInUserDataRecordCreatedAt();

  factory GetCheckInUserDataRecordCreatedAt.fromJson(
          Map<String, dynamic> json) =>
      $GetCheckInUserDataRecordCreatedAtFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetCheckInUserDataRecordCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetCheckInUserDataRecordUpdatedAt {
  GetCheckInUserDataRecordUpdatedAt();

  factory GetCheckInUserDataRecordUpdatedAt.fromJson(
          Map<String, dynamic> json) =>
      $GetCheckInUserDataRecordUpdatedAtFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetCheckInUserDataRecordUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetCheckInUserBench {
  GetCheckInUserBench();

  factory GetCheckInUserBench.fromJson(Map<String, dynamic> json) =>
      $GetCheckInUserBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInUserBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
