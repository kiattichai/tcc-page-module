import 'package:app/generated/json/get_promotion_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetPromotionEntity {
  GetPromotionEntity();

  factory GetPromotionEntity.fromJson(Map<String, dynamic> json) =>
      $GetPromotionEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionEntityToJson(this);

  GetPromotionData? data;
  GetPromotionBench? bench;
}

@JsonSerializable()
class GetPromotionData {
  GetPromotionData();

  factory GetPromotionData.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataToJson(this);

  GetPromotionDataPagination? pagination;
  List<GetPromotionDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetPromotionDataPagination {
  GetPromotionDataPagination();

  factory GetPromotionDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetPromotionDataRecord {
  GetPromotionDataRecord();

  factory GetPromotionDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordToJson(this);

  int? id;
  dynamic? slug;
  String? type;
  @JSONField(name: "group_type")
  String? groupType;
  @JSONField(name: "parent_id")
  int? parentId;
  String? name;
  @JSONField(name: "description_short")
  String? descriptionShort;
  String? description;
  GetPromotionDataRecordStore? store;
  GetPromotionDataRecordVenue? venue;
  @JSONField(name: "show_time")
  GetPromotionDataRecordShowTime? showTime;
  GetPromotionDataRecordPrice? price;
  List<dynamic>? variants;
  List<dynamic>? attributes;
  List<GetPromotionDataRecordImages>? images;
  int? remain;
  @JSONField(name: "ticket_count")
  int? ticketCount;
  @JSONField(name: "online_checkin")
  bool? onlineCheckin;
  int? viewed;
  @JSONField(name: "viewed_text")
  int? viewedText;
  @JSONField(name: "sales_url")
  dynamic? salesUrl;
  @JSONField(name: "share_url")
  String? shareUrl;
  @JSONField(name: "webview_url")
  String? webviewUrl;
  @JSONField(name: "include_vat")
  bool? includeVat;
  @JSONField(name: "payment_charge")
  bool? paymentCharge;
  @JSONField(name: "has_variant")
  bool? hasVariant;
  bool? status;
  @JSONField(name: "ticket_shipping")
  bool? ticketShipping;
  @JSONField(name: "publish_status")
  GetPromotionDataRecordPublishStatus? publishStatus;
  @JSONField(name: "publish_at")
  String? publishAt;
  dynamic? remark;
  @JSONField(name: "soldout_status")
  bool? soldoutStatus;
  @JSONField(name: "soldout_status_id")
  int? soldoutStatusId;
  @JSONField(name: "sales_status")
  bool? salesStatus;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  bool? updated;
  bool? verify;
}

@JsonSerializable()
class GetPromotionDataRecordStore {
  GetPromotionDataRecordStore();

  factory GetPromotionDataRecordStore.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordStoreFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordStoreToJson(this);

  int? id;
  String? name;
  String? slug;
  GetPromotionDataRecordStoreType? type;
  GetPromotionDataRecordStoreSection? section;
  bool? status;
  GetPromotionDataRecordStoreImage? image;
  GetPromotionDataRecordStoreVenue? venue;
}

@JsonSerializable()
class GetPromotionDataRecordStoreType {
  GetPromotionDataRecordStoreType();

  factory GetPromotionDataRecordStoreType.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordStoreTypeFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordStoreTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetPromotionDataRecordStoreSection {
  GetPromotionDataRecordStoreSection();

  factory GetPromotionDataRecordStoreSection.fromJson(
          Map<String, dynamic> json) =>
      $GetPromotionDataRecordStoreSectionFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetPromotionDataRecordStoreSectionToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetPromotionDataRecordStoreImage {
  GetPromotionDataRecordStoreImage();

  factory GetPromotionDataRecordStoreImage.fromJson(
          Map<String, dynamic> json) =>
      $GetPromotionDataRecordStoreImageFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetPromotionDataRecordStoreImageToJson(this);
}

@JsonSerializable()
class GetPromotionDataRecordStoreVenue {
  GetPromotionDataRecordStoreVenue();

  factory GetPromotionDataRecordStoreVenue.fromJson(
          Map<String, dynamic> json) =>
      $GetPromotionDataRecordStoreVenueFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetPromotionDataRecordStoreVenueToJson(this);

  int? id;
  double? lat;
  double? long;
  String? name;
  String? address;
}

@JsonSerializable()
class GetPromotionDataRecordVenue {
  GetPromotionDataRecordVenue();

  factory GetPromotionDataRecordVenue.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordVenueFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordVenueToJson(this);

  int? id;
  double? lat;
  double? long;
  String? name;
  String? address;
}

@JsonSerializable()
class GetPromotionDataRecordShowTime {
  GetPromotionDataRecordShowTime();

  factory GetPromotionDataRecordShowTime.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordShowTimeFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordShowTimeToJson(this);

  String? start;
  String? end;
  @JSONField(name: "text_full")
  String? textFull;
  @JSONField(name: "text_short")
  String? textShort;
  @JSONField(name: "text_short_date")
  String? textShortDate;
  int? status;
  @JSONField(name: "status_text")
  String? statusText;
}

@JsonSerializable()
class GetPromotionDataRecordPrice {
  GetPromotionDataRecordPrice();

  factory GetPromotionDataRecordPrice.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordPriceFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordPriceToJson(this);

  @JSONField(name: "currency_code")
  String? currencyCode;
  @JSONField(name: "currency_symbol")
  String? currencySymbol;
  int? min;
  int? max;
  @JSONField(name: "min_text")
  String? minText;
  @JSONField(name: "max_text")
  String? maxText;
  @JSONField(name: "compare_min")
  int? compareMin;
  @JSONField(name: "compare_max")
  int? compareMax;
  @JSONField(name: "compare_min_text")
  String? compareMinText;
  @JSONField(name: "compare_max_text")
  String? compareMaxText;
  bool? status;
}

@JsonSerializable()
class GetPromotionDataRecordImages {
  GetPromotionDataRecordImages();

  factory GetPromotionDataRecordImages.fromJson(Map<String, dynamic> json) =>
      $GetPromotionDataRecordImagesFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionDataRecordImagesToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}

@JsonSerializable()
class GetPromotionDataRecordPublishStatus {
  GetPromotionDataRecordPublishStatus();

  factory GetPromotionDataRecordPublishStatus.fromJson(
          Map<String, dynamic> json) =>
      $GetPromotionDataRecordPublishStatusFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetPromotionDataRecordPublishStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetPromotionBench {
  GetPromotionBench();

  factory GetPromotionBench.fromJson(Map<String, dynamic> json) =>
      $GetPromotionBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetPromotionBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
