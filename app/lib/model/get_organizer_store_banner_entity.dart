import 'package:app/generated/json/get_organizer_store_banner_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetOrganizerStoreBannerEntity {
  GetOrganizerStoreBannerEntity();

  factory GetOrganizerStoreBannerEntity.fromJson(Map<String, dynamic> json) =>
      $GetOrganizerStoreBannerEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetOrganizerStoreBannerEntityToJson(this);

  GetOrganizerStoreBannerData? data;
  GetOrganizerStoreBannerBench? bench;
}

@JsonSerializable()
class GetOrganizerStoreBannerData {
  GetOrganizerStoreBannerData();

  factory GetOrganizerStoreBannerData.fromJson(Map<String, dynamic> json) =>
      $GetOrganizerStoreBannerDataFromJson(json);

  Map<String, dynamic> toJson() => $GetOrganizerStoreBannerDataToJson(this);

  GetOrganizerStoreBannerDataImages? images;
  bool? cache;
}

@JsonSerializable()
class GetOrganizerStoreBannerDataImages {
  GetOrganizerStoreBannerDataImages();

  factory GetOrganizerStoreBannerDataImages.fromJson(
          Map<String, dynamic> json) =>
      $GetOrganizerStoreBannerDataImagesFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetOrganizerStoreBannerDataImagesToJson(this);

  List<GetOrganizerStoreBannerDataImagesBanner>? banner;
}

@JsonSerializable()
class GetOrganizerStoreBannerDataImagesBanner {
  GetOrganizerStoreBannerDataImagesBanner();

  factory GetOrganizerStoreBannerDataImagesBanner.fromJson(
          Map<String, dynamic> json) =>
      $GetOrganizerStoreBannerDataImagesBannerFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetOrganizerStoreBannerDataImagesBannerToJson(this);

  String? id;
  int? position;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetOrganizerStoreBannerBench {
  GetOrganizerStoreBannerBench();

  factory GetOrganizerStoreBannerBench.fromJson(Map<String, dynamic> json) =>
      $GetOrganizerStoreBannerBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetOrganizerStoreBannerBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
