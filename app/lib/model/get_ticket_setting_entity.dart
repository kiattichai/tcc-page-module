import 'package:app/generated/json/get_ticket_setting_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetTicketSettingEntity {
  GetTicketSettingEntity();

  factory GetTicketSettingEntity.fromJson(Map<String, dynamic> json) =>
      $GetTicketSettingEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSettingEntityToJson(this);

  GetTicketSettingData? data;
  GetTicketSettingBench? bench;
}

@JsonSerializable()
class GetTicketSettingData {
  GetTicketSettingData();

  factory GetTicketSettingData.fromJson(Map<String, dynamic> json) =>
      $GetTicketSettingDataFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSettingDataToJson(this);

  List<GetTicketSettingDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetTicketSettingDataRecord {
  GetTicketSettingDataRecord();

  factory GetTicketSettingDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSettingDataRecordToJson(this);

  int? id;
  @JSONField(name: "store_id")
  int? storeId;
  @JSONField(name: "product_id")
  int? productId;
  String? code;
  String? key;
  GetTicketSettingDataRecordValue? value;
  bool? serialize;
}

@JsonSerializable()
class GetTicketSettingDataRecordValue {
  GetTicketSettingDataRecordValue();

  factory GetTicketSettingDataRecordValue.fromJson(Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSettingDataRecordValueToJson(this);

  @JSONField(name: "service_fee")
  GetTicketSettingDataRecordValueServiceFee? serviceFee;
  @JSONField(name: "payment_fee")
  GetTicketSettingDataRecordValuePaymentFee? paymentFee;
  @JSONField(name: "tax_fee")
  GetTicketSettingDataRecordValueTaxFee? taxFee;
  GetTicketSettingDataRecordValueCcw? ccw;
  GetTicketSettingDataRecordValueIbanking? ibanking;
  GetTicketSettingDataRecordValueBanktrans? banktrans;
  GetTicketSettingDataRecordValueBill? bill;
  GetTicketSettingDataRecordValuePromptpay? promptpay;
  GetTicketSettingDataRecordValueInstallment? installment;
  GetTicketSettingDataRecordValueLinepay? linepay;
  GetTicketSettingDataRecordValueFree? free;
  GetTicketSettingDataRecordValuePaid? paid;
  String? ga;
  @JSONField(name: "fb_pixel")
  String? fbPixel;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueServiceFee {
  GetTicketSettingDataRecordValueServiceFee();

  factory GetTicketSettingDataRecordValueServiceFee.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueServiceFeeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueServiceFeeToJson(this);

  @JSONField(name: "organizer_pay")
  int? organizerPay;
  @JSONField(name: "customer_pay")
  int? customerPay;
}

@JsonSerializable()
class GetTicketSettingDataRecordValuePaymentFee {
  GetTicketSettingDataRecordValuePaymentFee();

  factory GetTicketSettingDataRecordValuePaymentFee.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValuePaymentFeeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValuePaymentFeeToJson(this);

  @JSONField(name: "organizer_pay")
  int? organizerPay;
  @JSONField(name: "customer_pay")
  int? customerPay;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueTaxFee {
  GetTicketSettingDataRecordValueTaxFee();

  factory GetTicketSettingDataRecordValueTaxFee.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueTaxFeeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueTaxFeeToJson(this);

  @JSONField(name: "organizer_pay")
  bool? organizerPay;
  @JSONField(name: "customer_pay")
  bool? customerPay;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueCcw {
  GetTicketSettingDataRecordValueCcw();

  factory GetTicketSettingDataRecordValueCcw.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueCcwFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueCcwToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueIbanking {
  GetTicketSettingDataRecordValueIbanking();

  factory GetTicketSettingDataRecordValueIbanking.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueIbankingFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueIbankingToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueBanktrans {
  GetTicketSettingDataRecordValueBanktrans();

  factory GetTicketSettingDataRecordValueBanktrans.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueBanktransFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueBanktransToJson(this);

  bool? status;
  int? expired;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueBill {
  GetTicketSettingDataRecordValueBill();

  factory GetTicketSettingDataRecordValueBill.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueBillFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueBillToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValuePromptpay {
  GetTicketSettingDataRecordValuePromptpay();

  factory GetTicketSettingDataRecordValuePromptpay.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValuePromptpayFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValuePromptpayToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueInstallment {
  GetTicketSettingDataRecordValueInstallment();

  factory GetTicketSettingDataRecordValueInstallment.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueInstallmentFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueInstallmentToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueLinepay {
  GetTicketSettingDataRecordValueLinepay();

  factory GetTicketSettingDataRecordValueLinepay.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueLinepayFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueLinepayToJson(this);

  bool? status;
}

@JsonSerializable()
class GetTicketSettingDataRecordValueFree {
  GetTicketSettingDataRecordValueFree();

  factory GetTicketSettingDataRecordValueFree.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValueFreeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValueFreeToJson(this);

  int? total;
  @JSONField(name: "payment_fee")
  int? paymentFee;
  @JSONField(name: "service_fee")
  int? serviceFee;
}

@JsonSerializable()
class GetTicketSettingDataRecordValuePaid {
  GetTicketSettingDataRecordValuePaid();

  factory GetTicketSettingDataRecordValuePaid.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSettingDataRecordValuePaidFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSettingDataRecordValuePaidToJson(this);

  int? total;
  @JSONField(name: "payment_fee")
  int? paymentFee;
  @JSONField(name: "service_fee")
  int? serviceFee;
}

@JsonSerializable()
class GetTicketSettingBench {
  GetTicketSettingBench();

  factory GetTicketSettingBench.fromJson(Map<String, dynamic> json) =>
      $GetTicketSettingBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSettingBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
