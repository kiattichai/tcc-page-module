import 'package:app/generated/json/get_document_ticket_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetDocumentTicketEntity {

	GetDocumentTicketEntity();

	factory GetDocumentTicketEntity.fromJson(Map<String, dynamic> json) => $GetDocumentTicketEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketEntityToJson(this);

	GetDocumentTicketData? data;
	GetDocumentTicketBench? bench;
}

@JsonSerializable()
class GetDocumentTicketData {

	GetDocumentTicketData();

	factory GetDocumentTicketData.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataToJson(this);

	List<GetDocumentTicketDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetDocumentTicketDataRecord {

	GetDocumentTicketDataRecord();

	factory GetDocumentTicketDataRecord.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataRecordToJson(this);

	int? id;
	@JSONField(name: "store_id")
	int? storeId;
	String? title;
	@JSONField(name: "file_type")
	String? fileType;
	String? type;
	GetDocumentTicketDataRecordStatus? status;
	String? remark;
	GetDocumentTicketDataRecordUser? user;
	GetDocumentTicketDataRecordStaff? staff;
	List<GetDocumentTicketDataRecordImages>? images;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
}

@JsonSerializable()
class GetDocumentTicketDataRecordStatus {

	GetDocumentTicketDataRecordStatus();

	factory GetDocumentTicketDataRecordStatus.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataRecordStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataRecordStatusToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetDocumentTicketDataRecordUser {

	GetDocumentTicketDataRecordUser();

	factory GetDocumentTicketDataRecordUser.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataRecordUserFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataRecordUserToJson(this);

	int? id;
	String? username;
	@JSONField(name: "country_code")
	String? countryCode;
	@JSONField(name: "first_name")
	String? firstName;
	@JSONField(name: "last_name")
	String? lastName;
}

@JsonSerializable()
class GetDocumentTicketDataRecordStaff {

	GetDocumentTicketDataRecordStaff();

	factory GetDocumentTicketDataRecordStaff.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataRecordStaffFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataRecordStaffToJson(this);


}

@JsonSerializable()
class GetDocumentTicketDataRecordImages {

	GetDocumentTicketDataRecordImages();

	factory GetDocumentTicketDataRecordImages.fromJson(Map<String, dynamic> json) => $GetDocumentTicketDataRecordImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketDataRecordImagesToJson(this);

	String? id;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
}

@JsonSerializable()
class GetDocumentTicketBench {

	GetDocumentTicketBench();

	factory GetDocumentTicketBench.fromJson(Map<String, dynamic> json) => $GetDocumentTicketBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetDocumentTicketBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
