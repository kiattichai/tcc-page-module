import 'package:app/generated/json/publish_page_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class PublishPageEntity {
  PublishPageEntity();

  factory PublishPageEntity.fromJson(Map<String, dynamic> json) =>
      $PublishPageEntityFromJson(json);

  Map<String, dynamic> toJson() => $PublishPageEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  bool? status;
}
