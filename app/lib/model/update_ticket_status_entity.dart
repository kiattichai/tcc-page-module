import 'package:app/generated/json/update_ticket_status_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UpdateTicketStatusEntity {
  UpdateTicketStatusEntity();

  factory UpdateTicketStatusEntity.fromJson(Map<String, dynamic> json) =>
      $UpdateTicketStatusEntityFromJson(json);

  Map<String, dynamic> toJson() => $UpdateTicketStatusEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  String? status;
}
