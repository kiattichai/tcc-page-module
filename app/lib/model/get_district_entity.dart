import 'package:app/generated/json/get_district_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetDistrictEntity {

	GetDistrictEntity();

	factory GetDistrictEntity.fromJson(Map<String, dynamic> json) => $GetDistrictEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetDistrictEntityToJson(this);

	GetDistrictData? data;
	GetDistrictBench? bench;
}

@JsonSerializable()
class GetDistrictData {

	GetDistrictData();

	factory GetDistrictData.fromJson(Map<String, dynamic> json) => $GetDistrictDataFromJson(json);

	Map<String, dynamic> toJson() => $GetDistrictDataToJson(this);

	List<GetDistrictDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetDistrictDataRecord {

	GetDistrictDataRecord();

	factory GetDistrictDataRecord.fromJson(Map<String, dynamic> json) => $GetDistrictDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetDistrictDataRecordToJson(this);

	int? id;
	GetDistrictDataRecordName? name;
	@JSONField(name: "zip_code")
	String? zipCode;
	@JSONField(name: "zip_code_map")
	String? zipCodeMap;
}

@JsonSerializable()
class GetDistrictDataRecordName {

	GetDistrictDataRecordName();

	factory GetDistrictDataRecordName.fromJson(Map<String, dynamic> json) => $GetDistrictDataRecordNameFromJson(json);

	Map<String, dynamic> toJson() => $GetDistrictDataRecordNameToJson(this);

	String? th;
	String? en;
}

@JsonSerializable()
class GetDistrictBench {

	GetDistrictBench();

	factory GetDistrictBench.fromJson(Map<String, dynamic> json) => $GetDistrictBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetDistrictBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
