import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/get_address_entity.g.dart';

@JsonSerializable()
class GetAddressEntity {
  GetAddressEntity();

  factory GetAddressEntity.fromJson(Map<String, dynamic> json) =>
      $GetAddressEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetAddressEntityToJson(this);

  GetAddressData? data;
  GetAddressBench? bench;
}

@JsonSerializable()
class GetAddressData {
  GetAddressData();

  factory GetAddressData.fromJson(Map<String, dynamic> json) =>
      $GetAddressDataFromJson(json);

  Map<String, dynamic> toJson() => $GetAddressDataToJson(this);

  List<GetAddressDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetAddressDataRecord {
  GetAddressDataRecord();

  factory GetAddressDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetAddressDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetAddressDataRecordToJson(this);

  int? id;
  GetAddressDataRecordName? name;
}

@JsonSerializable()
class GetAddressDataRecordName {
  GetAddressDataRecordName();

  factory GetAddressDataRecordName.fromJson(Map<String, dynamic> json) =>
      $GetAddressDataRecordNameFromJson(json);

  Map<String, dynamic> toJson() => $GetAddressDataRecordNameToJson(this);

  String? th;
  String? en;
}

@JsonSerializable()
class GetAddressBench {
  GetAddressBench();

  factory GetAddressBench.fromJson(Map<String, dynamic> json) =>
      $GetAddressBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetAddressBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
