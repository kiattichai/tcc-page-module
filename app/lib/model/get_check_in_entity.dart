import 'package:app/generated/json/get_check_in_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetCheckInEntity {
  GetCheckInEntity();

  factory GetCheckInEntity.fromJson(Map<String, dynamic> json) =>
      $GetCheckInEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInEntityToJson(this);

  GetCheckInData? data;
  GetCheckInBench? bench;
}

@JsonSerializable()
class GetCheckInData {
  GetCheckInData();

  factory GetCheckInData.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataToJson(this);

  GetCheckInDataPagination? pagination;
  List<GetCheckInDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetCheckInDataPagination {
  GetCheckInDataPagination();

  factory GetCheckInDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetCheckInDataRecord {
  GetCheckInDataRecord();

  factory GetCheckInDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataRecordToJson(this);

  int? id;
  String? description;
  GetCheckInDataRecordUser? user;
  @JSONField(name: "created_at")
  GetCheckInDataRecordCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetCheckInDataRecordUpdatedAt? updatedAt;
}

@JsonSerializable()
class GetCheckInDataRecordUser {
  GetCheckInDataRecordUser();

  factory GetCheckInDataRecordUser.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataRecordUserFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataRecordUserToJson(this);

  int? id;
  String? username;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  GetCheckInDataRecordUserImage? image;
}

@JsonSerializable()
class GetCheckInDataRecordUserImage {
  GetCheckInDataRecordUserImage();

  factory GetCheckInDataRecordUserImage.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataRecordUserImageFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataRecordUserImageToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}

@JsonSerializable()
class GetCheckInDataRecordCreatedAt {
  GetCheckInDataRecordCreatedAt();

  factory GetCheckInDataRecordCreatedAt.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataRecordCreatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataRecordCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetCheckInDataRecordUpdatedAt {
  GetCheckInDataRecordUpdatedAt();

  factory GetCheckInDataRecordUpdatedAt.fromJson(Map<String, dynamic> json) =>
      $GetCheckInDataRecordUpdatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInDataRecordUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetCheckInBench {
  GetCheckInBench();

  factory GetCheckInBench.fromJson(Map<String, dynamic> json) =>
      $GetCheckInBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetCheckInBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
