import 'dart:io';

import 'package:multi_image_picker/multi_image_picker.dart';

class CreateReview {
  String? description;
  int? rating;
  List<File> images = <File>[];

  CreateReview(this.description, this.rating, this.images);
}