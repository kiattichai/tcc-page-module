import 'package:app/generated/json/save_ticket_setting_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveTicketSettingEntity {
  SaveTicketSettingEntity();

  factory SaveTicketSettingEntity.fromJson(Map<String, dynamic> json) =>
      $SaveTicketSettingEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveTicketSettingEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  List<SaveTicketSettingSettings>? settings;
}

@JsonSerializable()
class SaveTicketSettingSettings {
  SaveTicketSettingSettings();

  factory SaveTicketSettingSettings.fromJson(Map<String, dynamic> json) =>
      $SaveTicketSettingSettingsFromJson(json);

  Map<String, dynamic> toJson() => $SaveTicketSettingSettingsToJson(this);

  String? code;
  String? key;
  String? value;
}
