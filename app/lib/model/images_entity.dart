import 'package:app/generated/json/images_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class ImagesEntity {
  ImagesEntity();

  factory ImagesEntity.fromJson(Map<String, dynamic> json) =>
      $ImagesEntityFromJson(json);

  Map<String, dynamic> toJson() => $ImagesEntityToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}
