import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_venue_response_entity.g.dart';

@JsonSerializable()
class SaveVenueResponseEntity {
  SaveVenueResponseEntity();

  factory SaveVenueResponseEntity.fromJson(Map<String, dynamic> json) =>
      $SaveVenueResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueResponseEntityToJson(this);

  SaveVenueResponseData? data;
  SaveVenueResponseBench? bench;
}

@JsonSerializable()
class SaveVenueResponseData {
  SaveVenueResponseData();

  factory SaveVenueResponseData.fromJson(Map<String, dynamic> json) =>
      $SaveVenueResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueResponseDataToJson(this);

  String? message;
  String? id;
}

@JsonSerializable()
class SaveVenueResponseBench {
  SaveVenueResponseBench();

  factory SaveVenueResponseBench.fromJson(Map<String, dynamic> json) =>
      $SaveVenueResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $SaveVenueResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
