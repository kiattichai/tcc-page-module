import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_publish_page_response_entity.g.dart';


@JsonSerializable()
class SavePublishPageResponseEntity {

	SavePublishPageResponseEntity();

	factory SavePublishPageResponseEntity.fromJson(Map<String, dynamic> json) => $SavePublishPageResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $SavePublishPageResponseEntityToJson(this);

	SavePublishPageResponseData? data;
	SavePublishPageResponseBench? bench;
}

@JsonSerializable()
class SavePublishPageResponseData {

	SavePublishPageResponseData();

	factory SavePublishPageResponseData.fromJson(Map<String, dynamic> json) => $SavePublishPageResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $SavePublishPageResponseDataToJson(this);

	String? message;
}

@JsonSerializable()
class SavePublishPageResponseBench {

	SavePublishPageResponseBench();

	factory SavePublishPageResponseBench.fromJson(Map<String, dynamic> json) => $SavePublishPageResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $SavePublishPageResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
