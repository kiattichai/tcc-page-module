class Promotion {

  String imageUrl;
  String date;
  String detail;
  String subDetail;
  String price;

  Promotion(this.imageUrl, this.date, this.detail,this.subDetail, this.price);
}