import 'package:app/generated/json/get_ticket_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetTicketEntity {
  GetTicketEntity();

  factory GetTicketEntity.fromJson(Map<String, dynamic> json) =>
      $GetTicketEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketEntityToJson(this);

  GetTicketData? data;
  GetTicketBench? bench;
}

@JsonSerializable()
class GetTicketData {
  GetTicketData();

  factory GetTicketData.fromJson(Map<String, dynamic> json) =>
      $GetTicketDataFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketDataToJson(this);

  List<GetTicketDataRecord>? record;
}

@JsonSerializable()
class GetTicketDataRecord {
  GetTicketDataRecord();

  factory GetTicketDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetTicketDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketDataRecordToJson(this);

  int? id;
  String? sku;
  String? name;
  String? description;
  dynamic? group;
  dynamic? zone;
  @JSONField(name: "cost_price")
  int? costPrice;
  @JSONField(name: "cost_price_text")
  String? costPriceText;
  int? price;
  @JSONField(name: "price_text")
  String? priceText;
  @JSONField(name: "compare_at_price")
  int? compareAtPrice;
  @JSONField(name: "compare_at_price_text")
  String? compareAtPriceText;
  bool? package;
  @JSONField(name: "per_package")
  int? perPackage;
  int? stock;
  int? quantity;
  @JSONField(name: "order_quantity")
  dynamic? orderQuantity;
  @JSONField(name: "diff_stock")
  int? diffStock;
  int? hold;
  int? position;
  int? points;
  int? weight;
  bool? status;
  @JSONField(name: "published_start")
  String? publishedStart;
  @JSONField(name: "published_end")
  String? publishedEnd;
  @JSONField(name: "gate_open")
  String? gateOpen;
  @JSONField(name: "gate_close")
  String? gateClose;
  @JSONField(name: "allow_order_min")
  int? allowOrderMin;
  @JSONField(name: "allow_order_max")
  int? allowOrderMax;
  dynamic? remark;
  @JSONField(name: "special_option")
  bool? specialOption;
  @JSONField(name: "service_charge")
  bool? serviceCharge;
  @JSONField(name: "service_fee")
  int? serviceFee;
  @JSONField(name: "has_ticket")
  bool? hasTicket;
  GetTicketDataRecordService? service;
  List<dynamic>? options;
  List<dynamic>? promotions;
  GetTicketDataRecordImage? image;
  @JSONField(name: "online_meeting")
  bool? onlineMeeting;
  List<dynamic>? meetings;
  dynamic? meta;
  @JSONField(name: "display_status")
  bool? displayStatus;
  @JSONField(name: "display_text")
  String? displayText;
}

@JsonSerializable()
class GetTicketDataRecordService {
  GetTicketDataRecordService();

  factory GetTicketDataRecordService.fromJson(Map<String, dynamic> json) =>
      $GetTicketDataRecordServiceFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketDataRecordServiceToJson(this);

  bool? charge;
  @JSONField(name: "fee_value")
  int? feeValue;
  int? fee;
  @JSONField(name: "fee_text")
  String? feeText;
}

@JsonSerializable()
class GetTicketDataRecordImage {
  GetTicketDataRecordImage();

  factory GetTicketDataRecordImage.fromJson(Map<String, dynamic> json) =>
      $GetTicketDataRecordImageFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketDataRecordImageToJson(this);
}

@JsonSerializable()
class GetTicketBench {
  GetTicketBench();

  factory GetTicketBench.fromJson(Map<String, dynamic> json) =>
      $GetTicketBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
