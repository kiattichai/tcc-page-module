import 'package:app/generated/json/uploads_banner_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UploadsBannerEntity {
  UploadsBannerEntity();

  factory UploadsBannerEntity.fromJson(Map<String, dynamic> json) =>
      $UploadsBannerEntityFromJson(json);

  Map<String, dynamic> toJson() => $UploadsBannerEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  String? status;
  @JSONField(name: "fileable_type")
  String? fileableType;
  bool? secure;
  List<UploadsBannerFiles>? files;
}

@JsonSerializable()
class UploadsBannerFiles {
  UploadsBannerFiles();

  factory UploadsBannerFiles.fromJson(Map<String, dynamic> json) =>
      $UploadsBannerFilesFromJson(json);

  Map<String, dynamic> toJson() => $UploadsBannerFilesToJson(this);

  String? name;
  String? data;
  int? position;
}
