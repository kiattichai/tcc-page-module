import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/Response_upload_bank_entity.g.dart';

@JsonSerializable()
class ResponseUploadBankEntity {
  ResponseUploadBankEntity();

  factory ResponseUploadBankEntity.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadBankEntityFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadBankEntityToJson(this);

  ResponseUploadBankData? data;
  ResponseUploadBankBench? bench;
}

@JsonSerializable()
class ResponseUploadBankData {
  ResponseUploadBankData();

  factory ResponseUploadBankData.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadBankDataFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadBankDataToJson(this);

  String? message;
}

@JsonSerializable()
class ResponseUploadBankBench {
  ResponseUploadBankBench();

  factory ResponseUploadBankBench.fromJson(Map<String, dynamic> json) =>
      $ResponseUploadBankBenchFromJson(json);

  Map<String, dynamic> toJson() => $ResponseUploadBankBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
