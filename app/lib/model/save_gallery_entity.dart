import 'package:app/generated/json/save_gallery_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveGalleryEntity {
  SaveGalleryEntity();

  factory SaveGalleryEntity.fromJson(Map<String, dynamic> json) =>
      $SaveGalleryEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveGalleryEntityToJson(this);

  String? description;
  @JSONField(name: "file_data")
  String? fileData;
  @JSONField(name: "file_name")
  String? fileName;
  @JSONField(name: "store_id")
  String? storeId;
}
