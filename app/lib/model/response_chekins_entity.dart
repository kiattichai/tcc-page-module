import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_chekins_entity.g.dart';


@JsonSerializable()
class ResponseChekinsEntity {

	ResponseChekinsEntity();

	factory ResponseChekinsEntity.fromJson(Map<String, dynamic> json) => $ResponseChekinsEntityFromJson(json);

	Map<String, dynamic> toJson() => $ResponseChekinsEntityToJson(this);

	ResponseChekinsData? data;
	ResponseChekinsBench? bench;
}

@JsonSerializable()
class ResponseChekinsData {

	ResponseChekinsData();

	factory ResponseChekinsData.fromJson(Map<String, dynamic> json) => $ResponseChekinsDataFromJson(json);

	Map<String, dynamic> toJson() => $ResponseChekinsDataToJson(this);

	String? message;
	int? id;
}

@JsonSerializable()
class ResponseChekinsBench {

	ResponseChekinsBench();

	factory ResponseChekinsBench.fromJson(Map<String, dynamic> json) => $ResponseChekinsBenchFromJson(json);

	Map<String, dynamic> toJson() => $ResponseChekinsBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
