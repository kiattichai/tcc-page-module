import 'package:app/generated/json/save_review_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveReviewEntity {
  SaveReviewEntity();

  factory SaveReviewEntity.fromJson(Map<String, dynamic> json) =>
      $SaveReviewEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewEntityToJson(this);

  @JSONField(name: "store_id")
  int? storeId;
  String? description;
  int? rating;
}
