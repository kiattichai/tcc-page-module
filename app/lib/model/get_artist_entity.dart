import 'package:app/generated/json/get_artist_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetArtistEntity {
  GetArtistEntity();

  factory GetArtistEntity.fromJson(Map<String, dynamic> json) =>
      $GetArtistEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistEntityToJson(this);

  GetArtistData? data;
  GetArtistBench? bench;
}

@JsonSerializable()
class GetArtistData {
  GetArtistData();

  factory GetArtistData.fromJson(Map<String, dynamic> json) =>
      $GetArtistDataFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistDataToJson(this);

  GetArtistDataPagination? pagination;
  List<GetArtistDataRecord>? record;
}

@JsonSerializable()
class GetArtistDataPagination {
  GetArtistDataPagination();

  factory GetArtistDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetArtistDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetArtistDataRecord {
  GetArtistDataRecord();

  factory GetArtistDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetArtistDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistDataRecordToJson(this);

  int? id;
  String? name;
  GetArtistDataRecordImage? image;
}

@JsonSerializable()
class GetArtistDataRecordImage {
  GetArtistDataRecordImage();

  factory GetArtistDataRecordImage.fromJson(Map<String, dynamic> json) =>
      $GetArtistDataRecordImageFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistDataRecordImageToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetArtistBench {
  GetArtistBench();

  factory GetArtistBench.fromJson(Map<String, dynamic> json) =>
      $GetArtistBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetArtistBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
