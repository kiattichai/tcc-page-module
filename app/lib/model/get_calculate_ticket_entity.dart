import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/get_calculate_ticket_entity.g.dart';


@JsonSerializable()
class GetCalculateTicketEntity {

	GetCalculateTicketEntity();

	factory GetCalculateTicketEntity.fromJson(Map<String, dynamic> json) => $GetCalculateTicketEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketEntityToJson(this);

	GetCalculateTicketData? data;
	GetCalculateTicketBench? bench;
}

@JsonSerializable()
class GetCalculateTicketData {

	GetCalculateTicketData();

	factory GetCalculateTicketData.fromJson(Map<String, dynamic> json) => $GetCalculateTicketDataFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketDataToJson(this);

	GetCalculateTicketDataParameters? parameters;
	GetCalculateTicketDataFees? fees;
	GetCalculateTicketDataAmounts? amounts;
}

@JsonSerializable()
class GetCalculateTicketDataParameters {

	GetCalculateTicketDataParameters();

	factory GetCalculateTicketDataParameters.fromJson(Map<String, dynamic> json) => $GetCalculateTicketDataParametersFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketDataParametersToJson(this);

	String? serviceFeePayer;
	String? paymentFeePayer;
	double? ticketPrice;
	int? ticketAmount;
}

@JsonSerializable()
class GetCalculateTicketDataFees {

	GetCalculateTicketDataFees();

	factory GetCalculateTicketDataFees.fromJson(Map<String, dynamic> json) => $GetCalculateTicketDataFeesFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketDataFeesToJson(this);

	String? serviceFee;
	String? paymentFee;
}

@JsonSerializable()
class GetCalculateTicketDataAmounts {

	GetCalculateTicketDataAmounts();

	factory GetCalculateTicketDataAmounts.fromJson(Map<String, dynamic> json) => $GetCalculateTicketDataAmountsFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketDataAmountsToJson(this);

	String? customerPay;
	String? organizerReceive;
}

@JsonSerializable()
class GetCalculateTicketBench {

	GetCalculateTicketBench();

	factory GetCalculateTicketBench.fromJson(Map<String, dynamic> json) => $GetCalculateTicketBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetCalculateTicketBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
