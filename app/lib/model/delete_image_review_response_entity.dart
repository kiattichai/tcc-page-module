import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/delete_image_review_response_entity.g.dart';

@JsonSerializable()
class DeleteImageReviewResponseEntity {
  DeleteImageReviewResponseEntity();

  factory DeleteImageReviewResponseEntity.fromJson(Map<String, dynamic> json) =>
      $DeleteImageReviewResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $DeleteImageReviewResponseEntityToJson(this);

  DeleteImageReviewResponseData? data;
  DeleteImageReviewResponseBench? bench;
}

@JsonSerializable()
class DeleteImageReviewResponseData {
  DeleteImageReviewResponseData();

  factory DeleteImageReviewResponseData.fromJson(Map<String, dynamic> json) =>
      $DeleteImageReviewResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $DeleteImageReviewResponseDataToJson(this);

  String? message;
}

@JsonSerializable()
class DeleteImageReviewResponseBench {
  DeleteImageReviewResponseBench();

  factory DeleteImageReviewResponseBench.fromJson(Map<String, dynamic> json) =>
      $DeleteImageReviewResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $DeleteImageReviewResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
