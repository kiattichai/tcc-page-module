import 'package:app/model/get_review_entity.dart';

class Review {
  String imageUrl;
  String name;
  int rating;
  String time;
  String comment;
  GetReviewDataRecordUser? user;

  Review(this.imageUrl, this.name, this.rating, this.time, this.comment,{this.user});
}
