import 'package:app/generated/json/update_concert_status_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UpdateConcertStatusEntity {
  UpdateConcertStatusEntity();

  factory UpdateConcertStatusEntity.fromJson(Map<String, dynamic> json) =>
      $UpdateConcertStatusEntityFromJson(json);

  Map<String, dynamic> toJson() => $UpdateConcertStatusEntityToJson(this);

  String? status;
  @JSONField(name: "store_id")
  String? storeId;
}
