import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_review_image_entity.g.dart';

@JsonSerializable()
class SaveReviewImageEntity {
  SaveReviewImageEntity();

  factory SaveReviewImageEntity.fromJson(Map<String, dynamic> json) =>
      $SaveReviewImageEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewImageEntityToJson(this);

  String? name;
  String? data;
}
