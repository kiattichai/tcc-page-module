class Concert {
  int id;
  String imageUrl;
  String date;
  String detail;

  Concert(this.id, this.imageUrl, this.date, this.detail);
}
