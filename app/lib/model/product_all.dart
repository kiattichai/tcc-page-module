class ProductAll {
  String type;
  String url;
  String name;
  int price;
  int onSale;
  int priceOnSale;
  ProductAll(this.type, this.url, this.name, this.price, this.onSale, this.priceOnSale);
}