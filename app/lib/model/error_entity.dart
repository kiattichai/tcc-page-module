import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/error_entity.g.dart';

@JsonSerializable()
class ErrorEntity {
  ErrorEntity();

  factory ErrorEntity.fromJson(Map<String, dynamic> json) =>
      $ErrorEntityFromJson(json);

  Map<String, dynamic> toJson() => $ErrorEntityToJson(this);

  String? type;
  int? code;
  String? message;
  String? field;
  String? line;
}
