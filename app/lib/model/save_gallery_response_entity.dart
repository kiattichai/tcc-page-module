import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_gallery_response_entity.g.dart';

@JsonSerializable()
class SaveGalleryResponseEntity {
  SaveGalleryResponseEntity();

  factory SaveGalleryResponseEntity.fromJson(Map<String, dynamic> json) =>
      $SaveGalleryResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveGalleryResponseEntityToJson(this);

  SaveGalleryResponseData? data;
  SaveGalleryResponseBench? bench;
}

@JsonSerializable()
class SaveGalleryResponseData {
  SaveGalleryResponseData();

  factory SaveGalleryResponseData.fromJson(Map<String, dynamic> json) =>
      $SaveGalleryResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SaveGalleryResponseDataToJson(this);

  String? message;
  int? id;
}

@JsonSerializable()
class SaveGalleryResponseBench {
  SaveGalleryResponseBench();

  factory SaveGalleryResponseBench.fromJson(Map<String, dynamic> json) =>
      $SaveGalleryResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $SaveGalleryResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
