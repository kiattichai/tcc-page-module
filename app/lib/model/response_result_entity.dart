import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_result_entity.g.dart';

@JsonSerializable()
class ResponseResultEntity {
  ResponseResultEntity();

  factory ResponseResultEntity.fromJson(Map<String, dynamic> json) =>
      $ResponseResultEntityFromJson(json);

  Map<String, dynamic> toJson() => $ResponseResultEntityToJson(this);

  ResponseResultData? data;
  ResponseResultBench? bench;
}

@JsonSerializable()
class ResponseResultData {
  ResponseResultData();

  factory ResponseResultData.fromJson(Map<String, dynamic> json) =>
      $ResponseResultDataFromJson(json);

  Map<String, dynamic> toJson() => $ResponseResultDataToJson(this);

  String? message;
}

@JsonSerializable()
class ResponseResultBench {
  ResponseResultBench();

  factory ResponseResultBench.fromJson(Map<String, dynamic> json) =>
      $ResponseResultBenchFromJson(json);

  Map<String, dynamic> toJson() => $ResponseResultBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
