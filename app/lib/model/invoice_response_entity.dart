import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/invoice_response_entity.g.dart';

@JsonSerializable()
class InvoiceResponseEntity {
  InvoiceResponseEntity();

  factory InvoiceResponseEntity.fromJson(Map<String, dynamic> json) =>
      $InvoiceResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $InvoiceResponseEntityToJson(this);

  InvoiceResponseData? data;
  InvoiceResponseBench? bench;
}

@JsonSerializable()
class InvoiceResponseData {
  InvoiceResponseData();

  factory InvoiceResponseData.fromJson(Map<String, dynamic> json) =>
      $InvoiceResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $InvoiceResponseDataToJson(this);

  String? message;
}

@JsonSerializable()
class InvoiceResponseBench {
  InvoiceResponseBench();

  factory InvoiceResponseBench.fromJson(Map<String, dynamic> json) =>
      $InvoiceResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $InvoiceResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
