import 'package:app/generated/json/get_concert_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConcertEntity {
  GetConcertEntity();

  factory GetConcertEntity.fromJson(Map<String, dynamic> json) =>
      $GetConcertEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertEntityToJson(this);

  GetConcertData? data;
  GetConcertBench? bench;
}

@JsonSerializable()
class GetConcertData {
  GetConcertData();

  factory GetConcertData.fromJson(Map<String, dynamic> json) =>
      $GetConcertDataFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertDataToJson(this);

  GetConcertDataPagination? pagination;
  List<GetConcertDataRecord>? record;
}

@JsonSerializable()
class GetConcertDataPagination {
  GetConcertDataPagination();

  factory GetConcertDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetConcertDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetConcertDataRecord {
  GetConcertDataRecord();

  factory GetConcertDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetConcertDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertDataRecordToJson(this);

  int? id;
  String? name;
  @JSONField(name: "show_time")
  GetConcertDataRecordShowTime? showTime;
  bool? status;
  List<GetConcertDataRecordImages>? images;
  @JSONField(name: "publish_status")
  GetConcertDataRecordPublishStatus? publishStatus;
  dynamic? remark;
  @JSONField(name: "has_variant")
  bool? hasVariant;
  @JSONField(name: "ticket_count")
  int? ticketCount;
}

@JsonSerializable()
class GetConcertDataRecordShowTime {
  GetConcertDataRecordShowTime();

  factory GetConcertDataRecordShowTime.fromJson(Map<String, dynamic> json) =>
      $GetConcertDataRecordShowTimeFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertDataRecordShowTimeToJson(this);

  String? start;
  String? end;
  @JSONField(name: "text_full")
  String? textFull;
  @JSONField(name: "text_short")
  String? textShort;
  @JSONField(name: "text_short_date")
  String? textShortDate;
  int? status;
  @JSONField(name: "status_text")
  String? statusText;
}

@JsonSerializable()
class GetConcertDataRecordImages {
  GetConcertDataRecordImages();

  factory GetConcertDataRecordImages.fromJson(Map<String, dynamic> json) =>
      $GetConcertDataRecordImagesFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertDataRecordImagesToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}

@JsonSerializable()
class GetConcertDataRecordPublishStatus {
  GetConcertDataRecordPublishStatus();

  factory GetConcertDataRecordPublishStatus.fromJson(
          Map<String, dynamic> json) =>
      $GetConcertDataRecordPublishStatusFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetConcertDataRecordPublishStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetConcertBench {
  GetConcertBench();

  factory GetConcertBench.fromJson(Map<String, dynamic> json) =>
      $GetConcertBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetConcertBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
