import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_banner_bulk_response_entity.g.dart';


@JsonSerializable()
class SaveBannerBulkResponseEntity {

	SaveBannerBulkResponseEntity();

	factory SaveBannerBulkResponseEntity.fromJson(Map<String, dynamic> json) => $SaveBannerBulkResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $SaveBannerBulkResponseEntityToJson(this);

	SaveBannerBulkResponseData? data;
	SaveBannerBulkResponseBench? bench;
}

@JsonSerializable()
class SaveBannerBulkResponseData {

	SaveBannerBulkResponseData();

	factory SaveBannerBulkResponseData.fromJson(Map<String, dynamic> json) => $SaveBannerBulkResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $SaveBannerBulkResponseDataToJson(this);

	String? message;
}

@JsonSerializable()
class SaveBannerBulkResponseBench {

	SaveBannerBulkResponseBench();

	factory SaveBannerBulkResponseBench.fromJson(Map<String, dynamic> json) => $SaveBannerBulkResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $SaveBannerBulkResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
