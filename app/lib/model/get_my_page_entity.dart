import 'package:app/generated/json/get_my_page_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetMyPageEntity {

	GetMyPageEntity();

	factory GetMyPageEntity.fromJson(Map<String, dynamic> json) => $GetMyPageEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageEntityToJson(this);

	GetMyPagePagination? pagination;
	List<GetMyPageData>? data;
}

@JsonSerializable()
class GetMyPagePagination {

	GetMyPagePagination();

	factory GetMyPagePagination.fromJson(Map<String, dynamic> json) => $GetMyPagePaginationFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPagePaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	@JSONField(name: "limit_page")
	int? limitPage;
	int? total;
}

@JsonSerializable()
class GetMyPageData {

	GetMyPageData();

	factory GetMyPageData.fromJson(Map<String, dynamic> json) => $GetMyPageDataFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageDataToJson(this);

	int? id;
	@JSONField(name: "display_name")
	String? displayName;
	dynamic? abouts;
	dynamic? slug;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "basic_id")
	String? basicId;
	String? type;
	@JSONField(name: "user_ref")
	dynamic? userRef;
	dynamic? code;
	@JSONField(name: "shop_status")
	dynamic? shopStatus;
	@JSONField(name: "shop_status_code")
	dynamic? shopStatusCode;
	@JSONField(name: "shop_status_text")
	dynamic? shopStatusText;
	@JSONField(name: "dealer_url")
	dynamic? dealerUrl;
	@JSONField(name: "total_followers")
	int? totalFollowers;
	@JSONField(name: "total_posts")
	int? totalPosts;
	@JSONField(name: "total_shares")
	int? totalShares;
	@JSONField(name: "total_views")
	int? totalViews;
	@JSONField(name: "time_ago")
	String? timeAgo;
	@JSONField(name: "is_owner")
	bool? isOwner;
	@JSONField(name: "is_follow")
	bool? isFollow;
	@JSONField(name: "is_verify")
	bool? isVerify;
	@JSONField(name: "is_blocking")
	bool? isBlocking;
	@JSONField(name: "user_follow")
	dynamic? userFollow;
	GetMyPageDataAvatar? avatar;
	GetMyPageDataCover? cover;
	List<dynamic>? category;
	GetMyPageDataUser? user;
	dynamic? staff;
	dynamic? remark;
	@JSONField(name: "follow_url")
	String? followUrl;
	@JSONField(name: "updated_at")
	String? updatedAt;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "deleted_at")
	String? deletedAt;
}

@JsonSerializable()
class GetMyPageDataAvatar {

	GetMyPageDataAvatar();

	factory GetMyPageDataAvatar.fromJson(Map<String, dynamic> json) => $GetMyPageDataAvatarFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageDataAvatarToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	String? tag;
	int? position;
}

@JsonSerializable()
class GetMyPageDataCover {

	GetMyPageDataCover();

	factory GetMyPageDataCover.fromJson(Map<String, dynamic> json) => $GetMyPageDataCoverFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageDataCoverToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	String? tag;
	int? position;
}

@JsonSerializable()
class GetMyPageDataUser {

	GetMyPageDataUser();

	factory GetMyPageDataUser.fromJson(Map<String, dynamic> json) => $GetMyPageDataUserFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageDataUserToJson(this);

	int? id;
	@JSONField(name: "display_name")
	dynamic? displayName;
	GetMyPageDataUserAvatar? avatar;
}

@JsonSerializable()
class GetMyPageDataUserAvatar {

	GetMyPageDataUserAvatar();

	factory GetMyPageDataUserAvatar.fromJson(Map<String, dynamic> json) => $GetMyPageDataUserAvatarFromJson(json);

	Map<String, dynamic> toJson() => $GetMyPageDataUserAvatarToJson(this);


}
