import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/get_payment_bank_entity.g.dart';

@JsonSerializable()
class GetPaymentBankEntity {
  GetPaymentBankEntity();

  factory GetPaymentBankEntity.fromJson(Map<String, dynamic> json) =>
      $GetPaymentBankEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetPaymentBankEntityToJson(this);

  GetPaymentBankData? data;
  GetPaymentBankBench? bench;
}

@JsonSerializable()
class GetPaymentBankData {
  GetPaymentBankData();

  factory GetPaymentBankData.fromJson(Map<String, dynamic> json) =>
      $GetPaymentBankDataFromJson(json);

  Map<String, dynamic> toJson() => $GetPaymentBankDataToJson(this);

  List<GetPaymentBankDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetPaymentBankDataRecord {
  GetPaymentBankDataRecord();

  factory GetPaymentBankDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetPaymentBankDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetPaymentBankDataRecordToJson(this);

  int? id;
  String? code;
  String? name;
}

@JsonSerializable()
class GetPaymentBankBench {
  GetPaymentBankBench();

  factory GetPaymentBankBench.fromJson(Map<String, dynamic> json) =>
      $GetPaymentBankBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetPaymentBankBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
