import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/setting_vaccine_limit_response_entity.g.dart';

@JsonSerializable()
class SettingVaccineLimitResponseEntity {
  SettingVaccineLimitResponseEntity();

  factory SettingVaccineLimitResponseEntity.fromJson(
          Map<String, dynamic> json) =>
      $SettingVaccineLimitResponseEntityFromJson(json);

  Map<String, dynamic> toJson() =>
      $SettingVaccineLimitResponseEntityToJson(this);

  SettingVaccineLimitResponseData? data;
  SettingVaccineLimitResponseBench? bench;
}

@JsonSerializable()
class SettingVaccineLimitResponseData {
  SettingVaccineLimitResponseData();

  factory SettingVaccineLimitResponseData.fromJson(Map<String, dynamic> json) =>
      $SettingVaccineLimitResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SettingVaccineLimitResponseDataToJson(this);

  List<SettingVaccineLimitResponseDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class SettingVaccineLimitResponseDataRecord {
  SettingVaccineLimitResponseDataRecord();

  factory SettingVaccineLimitResponseDataRecord.fromJson(
          Map<String, dynamic> json) =>
      $SettingVaccineLimitResponseDataRecordFromJson(json);

  Map<String, dynamic> toJson() =>
      $SettingVaccineLimitResponseDataRecordToJson(this);

  int? id;
  String? code;
  String? key;
  int? value;
  bool? serialized;
}

@JsonSerializable()
class SettingVaccineLimitResponseBench {
  SettingVaccineLimitResponseBench();

  factory SettingVaccineLimitResponseBench.fromJson(
          Map<String, dynamic> json) =>
      $SettingVaccineLimitResponseBenchFromJson(json);

  Map<String, dynamic> toJson() =>
      $SettingVaccineLimitResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
