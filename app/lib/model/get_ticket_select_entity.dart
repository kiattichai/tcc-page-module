import 'package:app/generated/json/get_ticket_select_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetTicketSelectEntity {
  GetTicketSelectEntity();

  factory GetTicketSelectEntity.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectEntityToJson(this);

  GetTicketSelectData? data;
  GetTicketSelectBench? bench;
}

@JsonSerializable()
class GetTicketSelectData {
  GetTicketSelectData();

  factory GetTicketSelectData.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectDataFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectDataToJson(this);

  GetTicketSelectDataPagination? pagination;
  List<GetTicketSelectDataRecord>? record;
}

@JsonSerializable()
class GetTicketSelectDataPagination {
  GetTicketSelectDataPagination();

  factory GetTicketSelectDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetTicketSelectDataRecord {
  GetTicketSelectDataRecord();

  factory GetTicketSelectDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectDataRecordToJson(this);

  int? id;
  @JSONField(name: "promotion_id")
  int? promotionId;
  GetTicketSelectDataRecordProduct? product;
  GetTicketSelectDataRecordVariant? variant;
  GetTicketSelectDataRecordGroup? group;
  dynamic? amount;
}

@JsonSerializable()
class GetTicketSelectDataRecordProduct {
  GetTicketSelectDataRecordProduct();

  factory GetTicketSelectDataRecordProduct.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSelectDataRecordProductFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSelectDataRecordProductToJson(this);

  int? id;
  String? type;
  String? name;
  bool? status;
}

@JsonSerializable()
class GetTicketSelectDataRecordVariant {
  GetTicketSelectDataRecordVariant();

  factory GetTicketSelectDataRecordVariant.fromJson(
          Map<String, dynamic> json) =>
      $GetTicketSelectDataRecordVariantFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetTicketSelectDataRecordVariantToJson(this);

  int? id;
  String? sku;
  String? name;
  int? price;
  @JSONField(name: "compare_at_price")
  int? compareAtPrice;
  bool? status;
}

@JsonSerializable()
class GetTicketSelectDataRecordGroup {
  GetTicketSelectDataRecordGroup();

  factory GetTicketSelectDataRecordGroup.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectDataRecordGroupFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectDataRecordGroupToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetTicketSelectBench {
  GetTicketSelectBench();

  factory GetTicketSelectBench.fromJson(Map<String, dynamic> json) =>
      $GetTicketSelectBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetTicketSelectBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
