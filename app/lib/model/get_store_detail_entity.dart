import 'package:app/generated/json/get_store_detail_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetStoreDetailEntity {

	GetStoreDetailEntity();

	factory GetStoreDetailEntity.fromJson(Map<String, dynamic> json) => $GetStoreDetailEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailEntityToJson(this);

  GetStoreDetailData? data;
  GetStoreDetailBench? bench;
}

@JsonSerializable()
class GetStoreDetailData {

	GetStoreDetailData();

	factory GetStoreDetailData.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataToJson(this);

  int? id;
  String? name;
  String? slug;
  String? description;
  GetStoreDetailDataType? type;
  GetStoreDetailDataSection? section;
  GetStoreDetailDataContact? contact;
  GetStoreDetailDataCorporate? corporate;
  GetStoreDetailDataBilling? billing;
  @JSONField(name: "accept_terms")
  bool? acceptTerms;
  @JSONField(name: "document_status")
  GetStoreDetailDataDocumentStatus? documentStatus;
  @JSONField(name: "payment_status")
  GetStoreDetailDataPaymentStatus? paymentStatus;
  @JSONField(name: "publish_status")
  GetStoreDetailDataPublishStatus? publishStatus;
  bool? status;
  @JSONField(name: "created_at")
  GetStoreDetailDataCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetStoreDetailDataUpdatedAt? updatedAt;
  @JSONField(name: "published_at")
  GetStoreDetailDataPublishedAt? publishedAt;
  List<GetStoreDetailDataSettings>? settings;
  GetStoreDetailDataImages? images;
  List<GetStoreDetailDataConnects>? connects;
  GetStoreDetailDataStaff? staff;
  GetStoreDetailDataVenue? venue;
  List<GetStoreDetailDataAttributes>? attributes;
  List<GetStoreDetailDataTimes>? times;
  @JSONField(name: "times_display")
  List<String>? timesDisplay;
  @JSONField(name: "time_open")
  GetStoreDetailDataTimeOpen? timeOpen;
  dynamic? remark;
  bool? updated;
  @JSONField(name: "accept_fee")
  bool? acceptFee;
  @JSONField(name: "accept_fee_at")
  GetStoreDetailDataAcceptFeeAt? acceptFeeAt;
  @JSONField(name: "average_rating")
  int? averageRating;
  @JSONField(name: "review_total")
  int? reviewTotal;
  @JSONField(name: "covid_verify")
  bool? covidVerify;
  @JSONField(name: "is_sha_plus")
  bool? isShaPlus;
  @JSONField(name: "is_covid_free")
  bool? isCovidFree;
  GetStoreDetailDataParking? parking;
  GetStoreDetailDataSeats? seats;
  bool? cache;
}

@JsonSerializable()
class GetStoreDetailDataType {

	GetStoreDetailDataType();

	factory GetStoreDetailDataType.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataTypeFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataSection {

	GetStoreDetailDataSection();

	factory GetStoreDetailDataSection.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataSectionFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataSectionToJson(this);

  int? id;
  String? text;
  @JSONField(name: "text_full")
  String? textFull;
}

@JsonSerializable()
class GetStoreDetailDataContact {

	GetStoreDetailDataContact();

	factory GetStoreDetailDataContact.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataContactFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataContactToJson(this);

  @JSONField(name: "first_name")
  dynamic? firstName;
  @JSONField(name: "last_name")
  dynamic? lastName;
  @JSONField(name: "citizen_id")
  dynamic? citizenId;
  String? phone;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "phone_option")
  List<dynamic>? phoneOption;
  String? email;
}

@JsonSerializable()
class GetStoreDetailDataCorporate     {

	GetStoreDetailDataCorporate();

	factory GetStoreDetailDataCorporate.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataCorporateFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataCorporateToJson(this);

  dynamic? name;
  dynamic? branch;
  dynamic? phone;
  @JSONField(name: "tax_id")
  dynamic? taxId;
}

@JsonSerializable()
class GetStoreDetailDataBilling {

	GetStoreDetailDataBilling();

	factory GetStoreDetailDataBilling.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataBillingFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataBillingToJson(this);

  dynamic? address;
  dynamic? district;
  dynamic? city;
  dynamic? province;
  dynamic? country;
  dynamic? zipcode;
}

@JsonSerializable()
class GetStoreDetailDataDocumentStatus     {

	GetStoreDetailDataDocumentStatus();

	factory GetStoreDetailDataDocumentStatus.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataDocumentStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataDocumentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataPaymentStatus     {

	GetStoreDetailDataPaymentStatus();

	factory GetStoreDetailDataPaymentStatus.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataPaymentStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataPaymentStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataPublishStatus     {

	GetStoreDetailDataPublishStatus();

	factory GetStoreDetailDataPublishStatus.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataPublishStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataPublishStatusToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataCreatedAt     {

	GetStoreDetailDataCreatedAt();

	factory GetStoreDetailDataCreatedAt.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataCreatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetStoreDetailDataUpdatedAt     {

	GetStoreDetailDataUpdatedAt();

	factory GetStoreDetailDataUpdatedAt.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataUpdatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
  @JSONField(name: "value_utc")
  String? valueUtc;
}

@JsonSerializable()
class GetStoreDetailDataPublishedAt     {

	GetStoreDetailDataPublishedAt();

	factory GetStoreDetailDataPublishedAt.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataPublishedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataPublishedAtToJson(this);
}

@JsonSerializable()
class GetStoreDetailDataSettings {

	GetStoreDetailDataSettings();

	factory GetStoreDetailDataSettings.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataSettingsFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataSettingsToJson(this);

  int? id;
  String? code;
  String? key;
  GetStoreDetailDataSettingsValue? value;
  bool? serialize;
}

@JsonSerializable()
class GetStoreDetailDataSettingsValue     {

	GetStoreDetailDataSettingsValue();

	factory GetStoreDetailDataSettingsValue.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataSettingsValueFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataSettingsValueToJson(this);

  String? website;
  String? facebook;
  String? twitter;
  String? line;
  String? instagram;
}

@JsonSerializable()
class GetStoreDetailDataImages {

	GetStoreDetailDataImages();

	factory GetStoreDetailDataImages.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataImagesToJson(this);

  GetStoreDetailDataImagesLogo? logo;
  List<GetStoreDetailDataImagesBanner>? banner;
  @JSONField(name: "corporate_logo")
  dynamic? corporateLogo;
  dynamic? signature;
}

@JsonSerializable()
class GetStoreDetailDataImagesLogo     {

	GetStoreDetailDataImagesLogo();

	factory GetStoreDetailDataImagesLogo.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataImagesLogoFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataImagesLogoToJson(this);

  String? id;
  int? position;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetStoreDetailDataImagesBanner     {

	GetStoreDetailDataImagesBanner();

	factory GetStoreDetailDataImagesBanner.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataImagesBannerFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataImagesBannerToJson(this);

  String? id;
  int? position;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
}

@JsonSerializable()
class GetStoreDetailDataConnects {

	GetStoreDetailDataConnects();

	factory GetStoreDetailDataConnects.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataConnectsFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataConnectsToJson(this);

  int? id;
  String? username;
  @JSONField(name: "country_code")
  String? countryCode;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
}

@JsonSerializable()
class GetStoreDetailDataStaff {

	GetStoreDetailDataStaff();

	factory GetStoreDetailDataStaff.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataStaffFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataStaffToJson(this);

  int? id;
  String? username;
  dynamic? phone;
}

@JsonSerializable()
class GetStoreDetailDataVenue {

	GetStoreDetailDataVenue();

	factory GetStoreDetailDataVenue.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataVenueFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataVenueToJson(this);

  int? id;
  double? lat;
  double? long;
  String? name;
  String? address;
  GetStoreDetailDataVenueCountry? country;
  GetStoreDetailDataVenueProvince? province;
  GetStoreDetailDataVenueCity? city;
  GetStoreDetailDataVenueDistrict? district;
  @JSONField(name: "zip_code")
  int? zipCode;
}

@JsonSerializable()
class GetStoreDetailDataVenueCountry     {

	GetStoreDetailDataVenueCountry();

	factory GetStoreDetailDataVenueCountry.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataVenueCountryFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataVenueCountryToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetStoreDetailDataVenueProvince     {

	GetStoreDetailDataVenueProvince();

	factory GetStoreDetailDataVenueProvince.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataVenueProvinceFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataVenueProvinceToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetStoreDetailDataVenueCity     {

	GetStoreDetailDataVenueCity();

	factory GetStoreDetailDataVenueCity.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataVenueCityFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataVenueCityToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetStoreDetailDataVenueDistrict     {

	GetStoreDetailDataVenueDistrict();

	factory GetStoreDetailDataVenueDistrict.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataVenueDistrictFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataVenueDistrictToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetStoreDetailDataAttributes     {

	GetStoreDetailDataAttributes();

	factory GetStoreDetailDataAttributes.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataAttributesFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataAttributesToJson(this);

  int? id;
  String? code;
  String? name;
  List<GetStoreDetailDataAttributesItems>? items;
}

@JsonSerializable()
class GetStoreDetailDataAttributesItems     {

	GetStoreDetailDataAttributesItems();

	factory GetStoreDetailDataAttributesItems.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataAttributesItemsFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataAttributesItemsToJson(this);

  int? id;
  String? name;
  dynamic? value;
}

@JsonSerializable()
class GetStoreDetailDataTimes {

	GetStoreDetailDataTimes();

	factory GetStoreDetailDataTimes.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataTimesFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataTimesToJson(this);

  int? day;
  @JSONField(name: "day_text")
  String? dayText;
  String? open;
  String? close;
  bool? status;
}

@JsonSerializable()
class GetStoreDetailDataTimeOpen {

	GetStoreDetailDataTimeOpen();

	factory GetStoreDetailDataTimeOpen.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataTimeOpenFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataTimeOpenToJson(this);

  bool? status;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataAcceptFeeAt     {

	GetStoreDetailDataAcceptFeeAt();

	factory GetStoreDetailDataAcceptFeeAt.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataAcceptFeeAtFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataAcceptFeeAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetStoreDetailDataParking {

	GetStoreDetailDataParking();

	factory GetStoreDetailDataParking.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataParkingFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataParkingToJson(this);

  int? total;
  String? text;
}

@JsonSerializable()
class GetStoreDetailDataSeats {

	GetStoreDetailDataSeats();

	factory GetStoreDetailDataSeats.fromJson(Map<String, dynamic> json) => $GetStoreDetailDataSeatsFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailDataSeatsToJson(this);

  int? min;
  int? max;
  String? text;
}

@JsonSerializable()
class GetStoreDetailBench {

	GetStoreDetailBench();

	factory GetStoreDetailBench.fromJson(Map<String, dynamic> json) => $GetStoreDetailBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetStoreDetailBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
