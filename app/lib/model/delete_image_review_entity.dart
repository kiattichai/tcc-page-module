import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/delete_image_review_entity.g.dart';

@JsonSerializable()
class DeleteImageReviewEntity {
  DeleteImageReviewEntity();

  factory DeleteImageReviewEntity.fromJson(Map<String, dynamic> json) =>
      $DeleteImageReviewEntityFromJson(json);

  Map<String, dynamic> toJson() => $DeleteImageReviewEntityToJson(this);

  List<DeleteImageReviewFiles>? files;
}

@JsonSerializable()
class DeleteImageReviewFiles {
  DeleteImageReviewFiles();

  factory DeleteImageReviewFiles.fromJson(Map<String, dynamic> json) =>
      $DeleteImageReviewFilesFromJson(json);

  Map<String, dynamic> toJson() => $DeleteImageReviewFilesToJson(this);

  String? id;
}
