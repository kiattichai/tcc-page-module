import 'package:app/generated/json/save_edit_profile_page_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveEditProfilePageEntity {
  SaveEditProfilePageEntity();

  factory SaveEditProfilePageEntity.fromJson(Map<String, dynamic> json) =>
      $SaveEditProfilePageEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveEditProfilePageEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  String? name;
  String? slug;
  String? description;
  String? phone;
  @JSONField(name: "country_code")
  String? countryCode;
  String? email;
  int? parking;
  @JSONField(name: "seat_min")
  int? seatMin;
  @JSONField(name: "seat_max")
  dynamic? seatMax;
  String? type;
  List<SaveEditProfilePageAttributes>? attributes;
  @JSONField(name: "phone_option")
  List<dynamic>? phoneOption;
  List<SaveEditProfilePageTimes>? times;
}

@JsonSerializable()
class SaveEditProfilePageAttributes {
  SaveEditProfilePageAttributes();

  factory SaveEditProfilePageAttributes.fromJson(Map<String, dynamic> json) =>
      $SaveEditProfilePageAttributesFromJson(json);

  Map<String, dynamic> toJson() => $SaveEditProfilePageAttributesToJson(this);

  int? id;
  @JSONField(name: "value_id")
  int? valueId;
  String? name;
}

@JsonSerializable()
class SaveEditProfilePageTimes {
  SaveEditProfilePageTimes();

  factory SaveEditProfilePageTimes.fromJson(Map<String, dynamic> json) =>
      $SaveEditProfilePageTimesFromJson(json);

  Map<String, dynamic> toJson() => $SaveEditProfilePageTimesToJson(this);

  int? day;
  String? open;
  String? close;
}
