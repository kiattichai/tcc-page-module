import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/gallery_update_response_entity.g.dart';

@JsonSerializable()
class GalleryUpdateResponseEntity {
  GalleryUpdateResponseEntity();

  factory GalleryUpdateResponseEntity.fromJson(Map<String, dynamic> json) =>
      $GalleryUpdateResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $GalleryUpdateResponseEntityToJson(this);

  GalleryUpdateResponseData? data;
  GalleryUpdateResponseBench? bench;
}

@JsonSerializable()
class GalleryUpdateResponseData {
  GalleryUpdateResponseData();

  factory GalleryUpdateResponseData.fromJson(Map<String, dynamic> json) =>
      $GalleryUpdateResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $GalleryUpdateResponseDataToJson(this);

  String? message;
  int? id;
}

@JsonSerializable()
class GalleryUpdateResponseBench {
  GalleryUpdateResponseBench();

  factory GalleryUpdateResponseBench.fromJson(Map<String, dynamic> json) =>
      $GalleryUpdateResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $GalleryUpdateResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
