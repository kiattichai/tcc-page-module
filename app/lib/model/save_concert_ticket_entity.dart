import 'package:app/generated/json/save_concert_ticket_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveConcertTicketEntity {
  SaveConcertTicketEntity();

  factory SaveConcertTicketEntity.fromJson(Map<String, dynamic> json) =>
      $SaveConcertTicketEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveConcertTicketEntityToJson(this);

  @JSONField(name: "store_id")
  String? storeId;
  @JSONField(name: "product_id")
  String? productId;
  String? sku;
  String? name;
  String? description;
  String? price;
  @JSONField(name: "compare_at_price")
  String? compareAtPrice;
  String? quantity;
  String? hold;
  @JSONField(name: "published_start")
  String? publishedStart;
  @JSONField(name: "published_end")
  String? publishedEnd;
  @JSONField(name: "gate_open")
  String? gateOpen;
  @JSONField(name: "gate_close")
  String? gateClose;
  @JSONField(name: "service_fee")
  String? serviceFee;
  @JSONField(name: "service_charge")
  bool? serviceCharge;
  @JSONField(name: "allow_order_min")
  String? allowOrderMin;
  @JSONField(name: "allow_order_max")
  String? allowOrderMax;
  bool? package;
  @JSONField(name: "per_package")
  int? perPackage;
  @JSONField(name: "has_ticket")
  bool? hasTicket;
  @JSONField(name: "updated_by")
  String? updatedBy;
  @JSONField(name: "created_by")
  int? createdBy;
  String? stock;
  bool? status;
}
