import 'package:app/generated/json/uploads_banner_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class UploadsBannerResponseEntity {
  UploadsBannerResponseEntity();

  factory UploadsBannerResponseEntity.fromJson(Map<String, dynamic> json) =>
      $UploadsBannerResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $UploadsBannerResponseEntityToJson(this);

  UploadsBannerResponseData? data;
  UploadsBannerResponseBench? bench;
}

@JsonSerializable()
class UploadsBannerResponseData {
  UploadsBannerResponseData();

  factory UploadsBannerResponseData.fromJson(Map<String, dynamic> json) =>
      $UploadsBannerResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $UploadsBannerResponseDataToJson(this);

  String? message;
  List<UploadsBannerResponseDataUploads>? uploads;
}

@JsonSerializable()
class UploadsBannerResponseDataUploads {
  UploadsBannerResponseDataUploads();

  factory UploadsBannerResponseDataUploads.fromJson(
          Map<String, dynamic> json) =>
      $UploadsBannerResponseDataUploadsFromJson(json);

  Map<String, dynamic> toJson() =>
      $UploadsBannerResponseDataUploadsToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
  int? position;
}

@JsonSerializable()
class UploadsBannerResponseBench {
  UploadsBannerResponseBench();

  factory UploadsBannerResponseBench.fromJson(Map<String, dynamic> json) =>
      $UploadsBannerResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $UploadsBannerResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
