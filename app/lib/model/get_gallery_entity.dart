import 'package:app/generated/json/get_gallery_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetGalleryEntity {

	GetGalleryEntity();

	factory GetGalleryEntity.fromJson(Map<String, dynamic> json) => $GetGalleryEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryEntityToJson(this);

	GetGalleryData? data;
	GetGalleryBench? bench;
}

@JsonSerializable()
class GetGalleryData {

	GetGalleryData();

	factory GetGalleryData.fromJson(Map<String, dynamic> json) => $GetGalleryDataFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataToJson(this);

	GetGalleryDataPagination? pagination;
	List<GetGalleryDataRecord>? record;
	bool? cache;
}

@JsonSerializable()
class GetGalleryDataPagination {

	GetGalleryDataPagination();

	factory GetGalleryDataPagination.fromJson(Map<String, dynamic> json) => $GetGalleryDataPaginationFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataPaginationToJson(this);

	@JSONField(name: "current_page")
	int? currentPage;
	@JSONField(name: "last_page")
	int? lastPage;
	int? limit;
	int? total;
}

@JsonSerializable()
class GetGalleryDataRecord {

	GetGalleryDataRecord();

	factory GetGalleryDataRecord.fromJson(Map<String, dynamic> json) => $GetGalleryDataRecordFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataRecordToJson(this);

	int? id;
	String? description;
	GetGalleryDataRecordUser? user;
	GetGalleryDataRecordImage? image;
	@JSONField(name: "created_at")
	GetGalleryDataRecordCreatedAt? createdAt;
	@JSONField(name: "updated_at")
	GetGalleryDataRecordUpdatedAt? updatedAt;
}

@JsonSerializable()
class GetGalleryDataRecordUser {

	GetGalleryDataRecordUser();

	factory GetGalleryDataRecordUser.fromJson(Map<String, dynamic> json) => $GetGalleryDataRecordUserFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataRecordUserToJson(this);

	int? id;
	String? username;
	@JSONField(name: "first_name")
	String? firstName;
	@JSONField(name: "last_name")
	String? lastName;
}

@JsonSerializable()
class GetGalleryDataRecordImage {

	GetGalleryDataRecordImage();

	factory GetGalleryDataRecordImage.fromJson(Map<String, dynamic> json) => $GetGalleryDataRecordImageFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataRecordImageToJson(this);

	String? id;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	@JSONField(name: "resize_url")
	String? resizeUrl;
}

@JsonSerializable()
class GetGalleryDataRecordCreatedAt {

	GetGalleryDataRecordCreatedAt();

	factory GetGalleryDataRecordCreatedAt.fromJson(Map<String, dynamic> json) => $GetGalleryDataRecordCreatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataRecordCreatedAtToJson(this);

	String? value;
	String? date;
	String? time;
}

@JsonSerializable()
class GetGalleryDataRecordUpdatedAt {

	GetGalleryDataRecordUpdatedAt();

	factory GetGalleryDataRecordUpdatedAt.fromJson(Map<String, dynamic> json) => $GetGalleryDataRecordUpdatedAtFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryDataRecordUpdatedAtToJson(this);

	String? value;
	String? date;
	String? time;
}

@JsonSerializable()
class GetGalleryBench {

	GetGalleryBench();

	factory GetGalleryBench.fromJson(Map<String, dynamic> json) => $GetGalleryBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetGalleryBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
