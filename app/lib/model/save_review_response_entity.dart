import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/save_review_response_entity.g.dart';

@JsonSerializable()
class SaveReviewResponseEntity {
  SaveReviewResponseEntity();

  factory SaveReviewResponseEntity.fromJson(Map<String, dynamic> json) =>
      $SaveReviewResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewResponseEntityToJson(this);

  SaveReviewResponseData? data;
  SaveReviewResponseBench? bench;
}

@JsonSerializable()
class SaveReviewResponseData {
  SaveReviewResponseData();

  factory SaveReviewResponseData.fromJson(Map<String, dynamic> json) =>
      $SaveReviewResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewResponseDataToJson(this);

  String? message;
  int? id;
}

@JsonSerializable()
class SaveReviewResponseBench {
  SaveReviewResponseBench();

  factory SaveReviewResponseBench.fromJson(Map<String, dynamic> json) =>
      $SaveReviewResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
