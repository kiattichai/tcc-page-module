import 'package:app/generated/json/get_concert_by_id_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetConcertByIdEntity {

	GetConcertByIdEntity();

	factory GetConcertByIdEntity.fromJson(Map<String, dynamic> json) => $GetConcertByIdEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdEntityToJson(this);

	GetConcertByIdData? data;
	GetConcertByIdBench? bench;
}

@JsonSerializable()
class GetConcertByIdData {

	GetConcertByIdData();

	factory GetConcertByIdData.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataToJson(this);

	int? id;
	dynamic? slug;
	String? type;
	@JSONField(name: "group_type")
	String? groupType;
	@JSONField(name: "parent_id")
	int? parentId;
	String? name;
	@JSONField(name: "description_short")
	String? descriptionShort;
	String? description;
	@JSONField(name: "seo_name")
	String? seoName;
	@JSONField(name: "seo_description")
	String? seoDescription;
	GetConcertByIdDataStore? store;
	GetConcertByIdDataVenue? venue;
	GetConcertByIdDataStage? stage;
	@JSONField(name: "show_time")
	GetConcertByIdDataShowTime? showTime;
	GetConcertByIdDataPrice? price;
	List<dynamic>? variants;
	List<GetConcertByIdDataAttributes>? attributes;
	List<GetConcertByIdDataImages>? images;
	int? remain;
	@JSONField(name: "ticket_count")
	int? ticketCount;
	@JSONField(name: "online_checkin")
	bool? onlineCheckin;
	int? viewed;
	@JSONField(name: "viewed_text")
	int? viewedText;
	@JSONField(name: "sales_url")
	dynamic? salesUrl;
	@JSONField(name: "share_url")
	String? shareUrl;
	@JSONField(name: "webview_url")
	String? webviewUrl;
	@JSONField(name: "include_vat")
	bool? includeVat;
	@JSONField(name: "payment_charge")
	bool? paymentCharge;
	@JSONField(name: "has_variant")
	bool? hasVariant;
	bool? status;
	@JSONField(name: "ticket_shipping")
	bool? ticketShipping;
	@JSONField(name: "publish_status")
	GetConcertByIdDataPublishStatus? publishStatus;
	@JSONField(name: "publish_at")
	String? publishAt;
	dynamic? remark;
	@JSONField(name: "soldout_status")
	bool? soldoutStatus;
	@JSONField(name: "soldout_status_id")
	int? soldoutStatusId;
	@JSONField(name: "sales_status")
	bool? salesStatus;
	@JSONField(name: "created_at")
	String? createdAt;
	@JSONField(name: "updated_at")
	String? updatedAt;
	bool? updated;
	bool? verify;
	GetConcertByIdDataSettings? settings;
}

@JsonSerializable()
class GetConcertByIdDataStore {

	GetConcertByIdDataStore();

	factory GetConcertByIdDataStore.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStoreFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStoreToJson(this);

	int? id;
	String? name;
	String? slug;
	GetConcertByIdDataStoreType? type;
	GetConcertByIdDataStoreSection? section;
	bool? status;
	GetConcertByIdDataStoreImage? image;
	GetConcertByIdDataStoreVenue? venue;
}

@JsonSerializable()
class GetConcertByIdDataStoreType {

	GetConcertByIdDataStoreType();

	factory GetConcertByIdDataStoreType.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStoreTypeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStoreTypeToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetConcertByIdDataStoreSection {

	GetConcertByIdDataStoreSection();

	factory GetConcertByIdDataStoreSection.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStoreSectionFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStoreSectionToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetConcertByIdDataStoreImage {

	GetConcertByIdDataStoreImage();

	factory GetConcertByIdDataStoreImage.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStoreImageFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStoreImageToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
}

@JsonSerializable()
class GetConcertByIdDataStoreVenue {

	GetConcertByIdDataStoreVenue();

	factory GetConcertByIdDataStoreVenue.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStoreVenueFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStoreVenueToJson(this);


}

@JsonSerializable()
class GetConcertByIdDataVenue {

	GetConcertByIdDataVenue();

	factory GetConcertByIdDataVenue.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataVenueFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataVenueToJson(this);

	int? id;
	double? lat;
	double? long;
	String? name;
	String? address;
	@JSONField(name: "meta_title")
	String? metaTitle;
	@JSONField(name: "meta_description")
	String? metaDescription;
	@JSONField(name: "meta_keyword")
	String? metaKeyword;
	GetConcertByIdDataVenueCountry? country;
	GetConcertByIdDataVenueProvince? province;
	dynamic? city;
	dynamic? district;
	@JSONField(name: "zip_code")
	dynamic? zipCode;
}

@JsonSerializable()
class GetConcertByIdDataVenueCountry {

	GetConcertByIdDataVenueCountry();

	factory GetConcertByIdDataVenueCountry.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataVenueCountryFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataVenueCountryToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertByIdDataVenueProvince {

	GetConcertByIdDataVenueProvince();

	factory GetConcertByIdDataVenueProvince.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataVenueProvinceFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataVenueProvinceToJson(this);

	int? id;
	String? name;
}

@JsonSerializable()
class GetConcertByIdDataStage {

	GetConcertByIdDataStage();

	factory GetConcertByIdDataStage.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataStageFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataStageToJson(this);


}

@JsonSerializable()
class GetConcertByIdDataShowTime {

	GetConcertByIdDataShowTime();

	factory GetConcertByIdDataShowTime.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataShowTimeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataShowTimeToJson(this);

	String? start;
	String? end;
	@JSONField(name: "text_full")
	String? textFull;
	@JSONField(name: "text_short")
	String? textShort;
	@JSONField(name: "text_short_date")
	String? textShortDate;
	int? status;
	@JSONField(name: "status_text")
	String? statusText;
}

@JsonSerializable()
class GetConcertByIdDataPrice {

	GetConcertByIdDataPrice();

	factory GetConcertByIdDataPrice.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataPriceFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataPriceToJson(this);

	@JSONField(name: "currency_code")
	String? currencyCode;
	@JSONField(name: "currency_symbol")
	String? currencySymbol;
	int? min;
	int? max;
	@JSONField(name: "min_text")
	String? minText;
	@JSONField(name: "max_text")
	String? maxText;
	@JSONField(name: "compare_min")
	int? compareMin;
	@JSONField(name: "compare_max")
	int? compareMax;
	@JSONField(name: "compare_min_text")
	String? compareMinText;
	@JSONField(name: "compare_max_text")
	String? compareMaxText;
	bool? status;
}

@JsonSerializable()
class GetConcertByIdDataAttributes {

	GetConcertByIdDataAttributes();

	factory GetConcertByIdDataAttributes.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataAttributesFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataAttributesToJson(this);

	int? id;
	String? code;
	String? name;
	List<GetConcertByIdDataAttributesItems>? items;
}

@JsonSerializable()
class GetConcertByIdDataAttributesItems {

	GetConcertByIdDataAttributesItems();

	factory GetConcertByIdDataAttributesItems.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataAttributesItemsFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataAttributesItemsToJson(this);

	int? id;
	String? name;
	dynamic? value;
	GetConcertByIdDataAttributesItemsImage? image;
}

@JsonSerializable()
class GetConcertByIdDataAttributesItemsImage {

	GetConcertByIdDataAttributesItemsImage();

	factory GetConcertByIdDataAttributesItemsImage.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataAttributesItemsImageFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataAttributesItemsImageToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
}

@JsonSerializable()
class GetConcertByIdDataImages {

	GetConcertByIdDataImages();

	factory GetConcertByIdDataImages.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataImagesFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataImagesToJson(this);

	String? id;
	@JSONField(name: "store_id")
	int? storeId;
	String? tag;
	@JSONField(name: "album_id")
	int? albumId;
	String? name;
	int? width;
	int? height;
	String? mime;
	int? size;
	String? url;
	int? position;
}

@JsonSerializable()
class GetConcertByIdDataPublishStatus {

	GetConcertByIdDataPublishStatus();

	factory GetConcertByIdDataPublishStatus.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataPublishStatusFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataPublishStatusToJson(this);

	int? id;
	String? text;
}

@JsonSerializable()
class GetConcertByIdDataSettings {

	GetConcertByIdDataSettings();

	factory GetConcertByIdDataSettings.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsToJson(this);

	GetConcertByIdDataSettingsPayment? payment;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPayment {

	GetConcertByIdDataSettingsPayment();

	factory GetConcertByIdDataSettingsPayment.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentToJson(this);

	GetConcertByIdDataSettingsPaymentFee? fee;
	GetConcertByIdDataSettingsPaymentMethod? method;
	GetConcertByIdDataSettingsPaymentSetting? setting;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentFee {

	GetConcertByIdDataSettingsPaymentFee();

	factory GetConcertByIdDataSettingsPaymentFee.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentFeeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentFeeToJson(this);

	@JSONField(name: "service_fee")
	GetConcertByIdDataSettingsPaymentFeeServiceFee? serviceFee;
	@JSONField(name: "payment_fee")
	GetConcertByIdDataSettingsPaymentFeePaymentFee? paymentFee;
	@JSONField(name: "tax_fee")
	GetConcertByIdDataSettingsPaymentFeeTaxFee? taxFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentFeeServiceFee {

	GetConcertByIdDataSettingsPaymentFeeServiceFee();

	factory GetConcertByIdDataSettingsPaymentFeeServiceFee.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentFeeServiceFeeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentFeeServiceFeeToJson(this);

	@JSONField(name: "organizer_pay")
	int? organizerPay;
	@JSONField(name: "customer_pay")
	int? customerPay;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentFeePaymentFee {

	GetConcertByIdDataSettingsPaymentFeePaymentFee();

	factory GetConcertByIdDataSettingsPaymentFeePaymentFee.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentFeePaymentFeeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentFeePaymentFeeToJson(this);

	@JSONField(name: "organizer_pay")
	int? organizerPay;
	@JSONField(name: "customer_pay")
	int? customerPay;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentFeeTaxFee {

	GetConcertByIdDataSettingsPaymentFeeTaxFee();

	factory GetConcertByIdDataSettingsPaymentFeeTaxFee.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentFeeTaxFeeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentFeeTaxFeeToJson(this);

	@JSONField(name: "organizer_pay")
	bool? organizerPay;
	@JSONField(name: "customer_pay")
	bool? customerPay;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethod {

	GetConcertByIdDataSettingsPaymentMethod();

	factory GetConcertByIdDataSettingsPaymentMethod.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodToJson(this);

	GetConcertByIdDataSettingsPaymentMethodCcw? ccw;
	GetConcertByIdDataSettingsPaymentMethodIbanking? ibanking;
	GetConcertByIdDataSettingsPaymentMethodBill? bill;
	GetConcertByIdDataSettingsPaymentMethodBanktrans? banktrans;
	GetConcertByIdDataSettingsPaymentMethodInstallment? installment;
	GetConcertByIdDataSettingsPaymentMethodPromptpay? promptpay;
	GetConcertByIdDataSettingsPaymentMethodLinepay? linepay;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodCcw {

	GetConcertByIdDataSettingsPaymentMethodCcw();

	factory GetConcertByIdDataSettingsPaymentMethodCcw.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodCcwFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodCcwToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodIbanking {

	GetConcertByIdDataSettingsPaymentMethodIbanking();

	factory GetConcertByIdDataSettingsPaymentMethodIbanking.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodIbankingFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodIbankingToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodBill {

	GetConcertByIdDataSettingsPaymentMethodBill();

	factory GetConcertByIdDataSettingsPaymentMethodBill.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodBillFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodBillToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodBanktrans {

	GetConcertByIdDataSettingsPaymentMethodBanktrans();

	factory GetConcertByIdDataSettingsPaymentMethodBanktrans.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodBanktransFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodBanktransToJson(this);

	bool? status;
	int? expired;
	@JSONField(name: "store_bank_id")
	dynamic? storeBankId;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodInstallment {

	GetConcertByIdDataSettingsPaymentMethodInstallment();

	factory GetConcertByIdDataSettingsPaymentMethodInstallment.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodInstallmentFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodInstallmentToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodPromptpay {

	GetConcertByIdDataSettingsPaymentMethodPromptpay();

	factory GetConcertByIdDataSettingsPaymentMethodPromptpay.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodPromptpayFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodPromptpayToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentMethodLinepay {

	GetConcertByIdDataSettingsPaymentMethodLinepay();

	factory GetConcertByIdDataSettingsPaymentMethodLinepay.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentMethodLinepayFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentMethodLinepayToJson(this);

	bool? status;
	@JSONField(name: "payment_fee")
	bool? paymentFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentSetting {

	GetConcertByIdDataSettingsPaymentSetting();

	factory GetConcertByIdDataSettingsPaymentSetting.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentSettingFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentSettingToJson(this);

	GetConcertByIdDataSettingsPaymentSettingFree? free;
	GetConcertByIdDataSettingsPaymentSettingPaid? paid;
	String? ga;
	@JSONField(name: "fb_pixel")
	String? fbPixel;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentSettingFree {

	GetConcertByIdDataSettingsPaymentSettingFree();

	factory GetConcertByIdDataSettingsPaymentSettingFree.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentSettingFreeFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentSettingFreeToJson(this);

	int? total;
	@JSONField(name: "payment_fee")
	int? paymentFee;
	@JSONField(name: "service_fee")
	int? serviceFee;
}

@JsonSerializable()
class GetConcertByIdDataSettingsPaymentSettingPaid {

	GetConcertByIdDataSettingsPaymentSettingPaid();

	factory GetConcertByIdDataSettingsPaymentSettingPaid.fromJson(Map<String, dynamic> json) => $GetConcertByIdDataSettingsPaymentSettingPaidFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdDataSettingsPaymentSettingPaidToJson(this);

	int? total;
	@JSONField(name: "payment_fee")
	int? paymentFee;
	@JSONField(name: "service_fee")
	int? serviceFee;
}

@JsonSerializable()
class GetConcertByIdBench {

	GetConcertByIdBench();

	factory GetConcertByIdBench.fromJson(Map<String, dynamic> json) => $GetConcertByIdBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetConcertByIdBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
