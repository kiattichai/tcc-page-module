import 'package:app/generated/json/get_venue_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetVenueEntity {
  GetVenueEntity();

  factory GetVenueEntity.fromJson(Map<String, dynamic> json) =>
      $GetVenueEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetVenueEntityToJson(this);

  GetVenueData? data;
  GetVenueBench? bench;
}

@JsonSerializable()
class GetVenueData {
  GetVenueData();

  factory GetVenueData.fromJson(Map<String, dynamic> json) =>
      $GetVenueDataFromJson(json);

  Map<String, dynamic> toJson() => $GetVenueDataToJson(this);

  GetVenueDataPagination? pagination;
  List<GetVenueDataRecord>? record;
}

@JsonSerializable()
class GetVenueDataPagination {
  GetVenueDataPagination();

  factory GetVenueDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetVenueDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetVenueDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetVenueDataRecord {
  GetVenueDataRecord();

  factory GetVenueDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetVenueDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetVenueDataRecordToJson(this);

  int? id;
  String? name;
}

@JsonSerializable()
class GetVenueBench {
  GetVenueBench();

  factory GetVenueBench.fromJson(Map<String, dynamic> json) =>
      $GetVenueBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetVenueBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
