import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/delete_banner_response_entity.g.dart';


@JsonSerializable()
class DeleteBannerResponseEntity {

	DeleteBannerResponseEntity();

	factory DeleteBannerResponseEntity.fromJson(Map<String, dynamic> json) => $DeleteBannerResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $DeleteBannerResponseEntityToJson(this);

	DeleteBannerResponseData? data;
	DeleteBannerResponseBench? bench;
}

@JsonSerializable()
class DeleteBannerResponseData {

	DeleteBannerResponseData();

	factory DeleteBannerResponseData.fromJson(Map<String, dynamic> json) => $DeleteBannerResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $DeleteBannerResponseDataToJson(this);

	String? message;
}

@JsonSerializable()
class DeleteBannerResponseBench {

	DeleteBannerResponseBench();

	factory DeleteBannerResponseBench.fromJson(Map<String, dynamic> json) => $DeleteBannerResponseBenchFromJson(json);

	Map<String, dynamic> toJson() => $DeleteBannerResponseBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
