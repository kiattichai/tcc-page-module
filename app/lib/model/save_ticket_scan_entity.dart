import 'package:app/generated/json/save_ticket_scan_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveTicketScanEntity {

	SaveTicketScanEntity();

	factory SaveTicketScanEntity.fromJson(Map<String, dynamic> json) => $SaveTicketScanEntityFromJson(json);

	Map<String, dynamic> toJson() => $SaveTicketScanEntityToJson(this);

	@JSONField(name: "store_id")
	String? storeId;
	String? note;
}
