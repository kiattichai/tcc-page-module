import 'package:app/generated/json/get_review_by_id_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetReviewByIdEntity {
  GetReviewByIdEntity();

  factory GetReviewByIdEntity.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdEntityToJson(this);

  GetReviewByIdData? data;
  GetReviewByIdBench? bench;
}

@JsonSerializable()
class GetReviewByIdData {
  GetReviewByIdData();

  factory GetReviewByIdData.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataToJson(this);

  int? id;
  String? description;
  GetReviewByIdDataUser? user;
  GetReviewByIdDataStore? store;
  int? rating;
  List<GetReviewByIdDataImages>? images;
  dynamic? remark;
  @JSONField(name: "created_at")
  GetReviewByIdDataCreatedAt? createdAt;
  @JSONField(name: "updated_at")
  GetReviewByIdDataUpdatedAt? updatedAt;
  bool? cache;
}

@JsonSerializable()
class GetReviewByIdDataUser {
  GetReviewByIdDataUser();

  factory GetReviewByIdDataUser.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataUserFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataUserToJson(this);

  int? id;
  String? username;
  @JSONField(name: "first_name")
  String? firstName;
  @JSONField(name: "last_name")
  String? lastName;
  GetReviewByIdDataUserImage? image;
}

@JsonSerializable()
class GetReviewByIdDataUserImage {
  GetReviewByIdDataUserImage();

  factory GetReviewByIdDataUserImage.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataUserImageFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataUserImageToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}

@JsonSerializable()
class GetReviewByIdDataStore {
  GetReviewByIdDataStore();

  factory GetReviewByIdDataStore.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataStoreFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataStoreToJson(this);
}

@JsonSerializable()
class GetReviewByIdDataImages {
  GetReviewByIdDataImages();

  factory GetReviewByIdDataImages.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataImagesFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataImagesToJson(this);

  String? id;
  @JSONField(name: "store_id")
  int? storeId;
  String? tag;
  @JSONField(name: "album_id")
  int? albumId;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  int? position;
}

@JsonSerializable()
class GetReviewByIdDataCreatedAt {
  GetReviewByIdDataCreatedAt();

  factory GetReviewByIdDataCreatedAt.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataCreatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataCreatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetReviewByIdDataUpdatedAt {
  GetReviewByIdDataUpdatedAt();

  factory GetReviewByIdDataUpdatedAt.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdDataUpdatedAtFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdDataUpdatedAtToJson(this);

  String? value;
  String? date;
  String? time;
}

@JsonSerializable()
class GetReviewByIdBench {
  GetReviewByIdBench();

  factory GetReviewByIdBench.fromJson(Map<String, dynamic> json) =>
      $GetReviewByIdBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetReviewByIdBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
