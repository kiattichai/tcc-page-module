import 'package:app/generated/json/save_review_image_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveReviewImageResponseEntity {
  SaveReviewImageResponseEntity();

  factory SaveReviewImageResponseEntity.fromJson(Map<String, dynamic> json) =>
      $SaveReviewImageResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewImageResponseEntityToJson(this);

  SaveReviewImageResponseData? data;
  SaveReviewImageResponseBench? bench;
}

@JsonSerializable()
class SaveReviewImageResponseData {
  SaveReviewImageResponseData();

  factory SaveReviewImageResponseData.fromJson(Map<String, dynamic> json) =>
      $SaveReviewImageResponseDataFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewImageResponseDataToJson(this);

  String? message;
  SaveReviewImageResponseDataUpload? upload;
}

@JsonSerializable()
class SaveReviewImageResponseDataUpload {
  SaveReviewImageResponseDataUpload();

  factory SaveReviewImageResponseDataUpload.fromJson(
          Map<String, dynamic> json) =>
      $SaveReviewImageResponseDataUploadFromJson(json);

  Map<String, dynamic> toJson() =>
      $SaveReviewImageResponseDataUploadToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
  int? position;
}

@JsonSerializable()
class SaveReviewImageResponseBench {
  SaveReviewImageResponseBench();

  factory SaveReviewImageResponseBench.fromJson(Map<String, dynamic> json) =>
      $SaveReviewImageResponseBenchFromJson(json);

  Map<String, dynamic> toJson() => $SaveReviewImageResponseBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
