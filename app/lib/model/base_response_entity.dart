import 'package:app/generated/json/base_response_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class BaseResponseEntity {

	BaseResponseEntity();

	factory BaseResponseEntity.fromJson(Map<String, dynamic> json) => $BaseResponseEntityFromJson(json);

	Map<String, dynamic> toJson() => $BaseResponseEntityToJson(this);

  BaseResponseData? data;
}

@JsonSerializable()
class BaseResponseData {

	BaseResponseData();

	factory BaseResponseData.fromJson(Map<String, dynamic> json) => $BaseResponseDataFromJson(json);

	Map<String, dynamic> toJson() => $BaseResponseDataToJson(this);

  @JSONField(name: "order_id")
  int? orderId;
  @JSONField(name: "order_no")
  String? orderNo;
  String? message;
  int? id;
  String? comment;
  List<DataUpload>? uploads;
  bool? status;
}

@JsonSerializable()
class DataUpload {

	DataUpload();

	factory DataUpload.fromJson(Map<String, dynamic> json) => $DataUploadFromJson(json);

	Map<String, dynamic> toJson() => $DataUploadToJson(this);

  String? id;
  String? name;
  int? width;
  int? height;
  String? mime;
  int? size;
  String? url;
  @JSONField(name: "resize_url")
  String? resizeUrl;
}
