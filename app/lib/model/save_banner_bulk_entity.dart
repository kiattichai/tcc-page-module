import 'package:app/generated/json/save_banner_bulk_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class SaveBannerBulkEntity {
  SaveBannerBulkEntity();

  factory SaveBannerBulkEntity.fromJson(Map<String, dynamic> json) =>
      $SaveBannerBulkEntityFromJson(json);

  Map<String, dynamic> toJson() => $SaveBannerBulkEntityToJson(this);

  @JSONField(name: "fileable_id")
  String? fileableId;
  @JSONField(name: "fileable_type")
  String? fileableType;
  List<SaveBannerBulkItems>? items;
}

@JsonSerializable()
class SaveBannerBulkItems {
  SaveBannerBulkItems();

  factory SaveBannerBulkItems.fromJson(Map<String, dynamic> json) =>
      $SaveBannerBulkItemsFromJson(json);

  Map<String, dynamic> toJson() => $SaveBannerBulkItemsToJson(this);

  @JSONField(name: "attachment_id")
  String? attachmentId;
  int? position;
}
