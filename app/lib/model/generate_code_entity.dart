import 'package:app/generated/json/generate_code_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GenerateCodeEntity {
  GenerateCodeEntity();

  factory GenerateCodeEntity.fromJson(Map<String, dynamic> json) =>
      $GenerateCodeEntityFromJson(json);

  Map<String, dynamic> toJson() => $GenerateCodeEntityToJson(this);

  GenerateCodeData? data;
  GenerateCodeBench? bench;
}

@JsonSerializable()
class GenerateCodeData {
  GenerateCodeData();

  factory GenerateCodeData.fromJson(Map<String, dynamic> json) =>
      $GenerateCodeDataFromJson(json);

  Map<String, dynamic> toJson() => $GenerateCodeDataToJson(this);

  String? message;
  @JSONField(name: "promotion_code")
  List<GenerateCodeDataPromotionCode>? promotionCode;
}

@JsonSerializable()
class GenerateCodeDataPromotionCode {
  GenerateCodeDataPromotionCode();

  factory GenerateCodeDataPromotionCode.fromJson(Map<String, dynamic> json) =>
      $GenerateCodeDataPromotionCodeFromJson(json);

  Map<String, dynamic> toJson() => $GenerateCodeDataPromotionCodeToJson(this);

  @JSONField(name: "promotion_id")
  int? promotionId;
  String? code;
  int? used;
  int? status;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  int? id;
}

@JsonSerializable()
class GenerateCodeBench {
  GenerateCodeBench();

  factory GenerateCodeBench.fromJson(Map<String, dynamic> json) =>
      $GenerateCodeBenchFromJson(json);

  Map<String, dynamic> toJson() => $GenerateCodeBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
