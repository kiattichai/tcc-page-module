import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/response_image_upload_entity.g.dart';


@JsonSerializable()
class ResponseImageUploadEntity {

	ResponseImageUploadEntity();

	factory ResponseImageUploadEntity.fromJson(Map<String, dynamic> json) => $ResponseImageUploadEntityFromJson(json);

	Map<String, dynamic> toJson() => $ResponseImageUploadEntityToJson(this);

	ResponseImageUploadData? data;
	ResponseImageUploadBench? bench;
}

@JsonSerializable()
class ResponseImageUploadData {

	ResponseImageUploadData();

	factory ResponseImageUploadData.fromJson(Map<String, dynamic> json) => $ResponseImageUploadDataFromJson(json);

	Map<String, dynamic> toJson() => $ResponseImageUploadDataToJson(this);

	String? message;
}

@JsonSerializable()
class ResponseImageUploadBench {

	ResponseImageUploadBench();

	factory ResponseImageUploadBench.fromJson(Map<String, dynamic> json) => $ResponseImageUploadBenchFromJson(json);

	Map<String, dynamic> toJson() => $ResponseImageUploadBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
