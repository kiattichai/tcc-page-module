import 'package:app/generated/json/get_ticket_by_id_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetTicketByIdEntity {

	GetTicketByIdEntity();

	factory GetTicketByIdEntity.fromJson(Map<String, dynamic> json) => $GetTicketByIdEntityFromJson(json);

	Map<String, dynamic> toJson() => $GetTicketByIdEntityToJson(this);

	GetTicketByIdData? data;
	GetTicketByIdBench? bench;
}

@JsonSerializable()
class GetTicketByIdData {

	GetTicketByIdData();

	factory GetTicketByIdData.fromJson(Map<String, dynamic> json) => $GetTicketByIdDataFromJson(json);

	Map<String, dynamic> toJson() => $GetTicketByIdDataToJson(this);

	int? id;
	String? sku;
	String? name;
	String? description;
	dynamic? group;
	dynamic? zone;
	@JSONField(name: "cost_price")
	int? costPrice;
	@JSONField(name: "cost_price_text")
	String? costPriceText;
	int? price;
	@JSONField(name: "price_text")
	String? priceText;
	@JSONField(name: "compare_at_price")
	int? compareAtPrice;
	@JSONField(name: "compare_at_price_text")
	String? compareAtPriceText;
	bool? package;
	@JSONField(name: "per_package")
	int? perPackage;
	int? stock;
	int? quantity;
	@JSONField(name: "order_quantity")
	dynamic? orderQuantity;
	@JSONField(name: "diff_stock")
	int? diffStock;
	int? hold;
	int? position;
	int? points;
	int? weight;
	bool? status;
	@JSONField(name: "published_start")
	String? publishedStart;
	@JSONField(name: "published_end")
	String? publishedEnd;
	@JSONField(name: "gate_open")
	String? gateOpen;
	@JSONField(name: "gate_close")
	String? gateClose;
	@JSONField(name: "allow_order_min")
	int? allowOrderMin;
	@JSONField(name: "allow_order_max")
	int? allowOrderMax;
	dynamic? remark;
	@JSONField(name: "special_option")
	bool? specialOption;
	@JSONField(name: "service_charge")
	bool? serviceCharge;
	@JSONField(name: "service_fee")
	int? serviceFee;
	@JSONField(name: "has_ticket")
	bool? hasTicket;
	GetTicketByIdDataService? service;
	List<dynamic>? options;
	List<dynamic>? promotions;
	GetTicketByIdDataImage? image;
	@JSONField(name: "online_meeting")
	bool? onlineMeeting;
	List<dynamic>? meetings;
	dynamic? meta;
	@JSONField(name: "display_status")
	bool? displayStatus;
	@JSONField(name: "display_text")
	String? displayText;
	bool? cache;
}

@JsonSerializable()
class GetTicketByIdDataService {

	GetTicketByIdDataService();

	factory GetTicketByIdDataService.fromJson(Map<String, dynamic> json) => $GetTicketByIdDataServiceFromJson(json);

	Map<String, dynamic> toJson() => $GetTicketByIdDataServiceToJson(this);

	bool? charge;
	@JSONField(name: "fee_value")
	int? feeValue;
	int? fee;
	@JSONField(name: "fee_text")
	String? feeText;
}

@JsonSerializable()
class GetTicketByIdDataImage {

	GetTicketByIdDataImage();

	factory GetTicketByIdDataImage.fromJson(Map<String, dynamic> json) => $GetTicketByIdDataImageFromJson(json);

	Map<String, dynamic> toJson() => $GetTicketByIdDataImageToJson(this);


}

@JsonSerializable()
class GetTicketByIdBench {

	GetTicketByIdBench();

	factory GetTicketByIdBench.fromJson(Map<String, dynamic> json) => $GetTicketByIdBenchFromJson(json);

	Map<String, dynamic> toJson() => $GetTicketByIdBenchToJson(this);

	int? second;
	double? millisecond;
	String? format;
}
