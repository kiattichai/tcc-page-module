import 'package:app/generated/json/get_genre_attribute_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetGenreAttributeEntity {
  GetGenreAttributeEntity();

  factory GetGenreAttributeEntity.fromJson(Map<String, dynamic> json) =>
      $GetGenreAttributeEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetGenreAttributeEntityToJson(this);

  GetGenreAttributeData? data;
  GetGenreAttributeBench? bench;
}

@JsonSerializable()
class GetGenreAttributeData {
  GetGenreAttributeData();

  factory GetGenreAttributeData.fromJson(Map<String, dynamic> json) =>
      $GetGenreAttributeDataFromJson(json);

  Map<String, dynamic> toJson() => $GetGenreAttributeDataToJson(this);

  GetGenreAttributeDataPagination? pagination;
  List<GetGenreAttributeDataRecord>? record;
  bool? cache;
}

@JsonSerializable()
class GetGenreAttributeDataPagination {
  GetGenreAttributeDataPagination();

  factory GetGenreAttributeDataPagination.fromJson(Map<String, dynamic> json) =>
      $GetGenreAttributeDataPaginationFromJson(json);

  Map<String, dynamic> toJson() => $GetGenreAttributeDataPaginationToJson(this);

  @JSONField(name: "current_page")
  int? currentPage;
  @JSONField(name: "last_page")
  int? lastPage;
  int? limit;
  int? total;
}

@JsonSerializable()
class GetGenreAttributeDataRecord {
  GetGenreAttributeDataRecord();

  factory GetGenreAttributeDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetGenreAttributeDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetGenreAttributeDataRecordToJson(this);

  int? id;
  String? code;
  int? position;
  bool? status;
  bool? require;
  int? type;
  bool? filter;
  dynamic? metafield;
  String? name;
  List<GetGenreAttributeDataRecordValues>? values;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
}

@JsonSerializable()
class GetGenreAttributeDataRecordValues {
  GetGenreAttributeDataRecordValues();

  factory GetGenreAttributeDataRecordValues.fromJson(
          Map<String, dynamic> json) =>
      $GetGenreAttributeDataRecordValuesFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetGenreAttributeDataRecordValuesToJson(this);

  int? id;
  int? position;
  bool? status;
  String? name;
  dynamic? image;
}

@JsonSerializable()
class GetGenreAttributeBench {
  GetGenreAttributeBench();

  factory GetGenreAttributeBench.fromJson(Map<String, dynamic> json) =>
      $GetGenreAttributeBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetGenreAttributeBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
