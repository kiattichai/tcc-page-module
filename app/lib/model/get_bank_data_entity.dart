import 'package:app/generated/json/get_bank_data_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class GetBankDataEntity {
  GetBankDataEntity();

  factory GetBankDataEntity.fromJson(Map<String, dynamic> json) =>
      $GetBankDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataEntityToJson(this);

  GetBankDataData? data;
  GetBankDataBench? bench;
}

@JsonSerializable()
class GetBankDataData {
  GetBankDataData();

  factory GetBankDataData.fromJson(Map<String, dynamic> json) =>
      $GetBankDataDataFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataDataToJson(this);

  List<GetBankDataDataRecord>? record;
}

@JsonSerializable()
class GetBankDataDataRecord {
  GetBankDataDataRecord();

  factory GetBankDataDataRecord.fromJson(Map<String, dynamic> json) =>
      $GetBankDataDataRecordFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataDataRecordToJson(this);

  int? id;
  @JSONField(name: "store_id")
  int? storeId;
  GetBankDataDataRecordType? type;
  GetBankDataDataRecordAccount? account;
  @JSONField(name: "payment_bank")
  GetBankDataDataRecordPaymentBank? paymentBank;
  dynamic? image;
  dynamic? staff;
  dynamic? remark;
  String? status;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
}

@JsonSerializable()
class GetBankDataDataRecordType {
  GetBankDataDataRecordType();

  factory GetBankDataDataRecordType.fromJson(Map<String, dynamic> json) =>
      $GetBankDataDataRecordTypeFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataDataRecordTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetBankDataDataRecordAccount {
  GetBankDataDataRecordAccount();

  factory GetBankDataDataRecordAccount.fromJson(Map<String, dynamic> json) =>
      $GetBankDataDataRecordAccountFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataDataRecordAccountToJson(this);

  GetBankDataDataRecordAccountType? type;
  String? name;
  String? number;
  String? branch;
}

@JsonSerializable()
class GetBankDataDataRecordAccountType {
  GetBankDataDataRecordAccountType();

  factory GetBankDataDataRecordAccountType.fromJson(
          Map<String, dynamic> json) =>
      $GetBankDataDataRecordAccountTypeFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetBankDataDataRecordAccountTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class GetBankDataDataRecordPaymentBank {
  GetBankDataDataRecordPaymentBank();

  factory GetBankDataDataRecordPaymentBank.fromJson(
          Map<String, dynamic> json) =>
      $GetBankDataDataRecordPaymentBankFromJson(json);

  Map<String, dynamic> toJson() =>
      $GetBankDataDataRecordPaymentBankToJson(this);

  int? id;
  String? code;
  String? name;
}

@JsonSerializable()
class GetBankDataBench {
  GetBankDataBench();

  factory GetBankDataBench.fromJson(Map<String, dynamic> json) =>
      $GetBankDataBenchFromJson(json);

  Map<String, dynamic> toJson() => $GetBankDataBenchToJson(this);

  int? second;
  double? millisecond;
  String? format;
}
