import 'dart:io';

import 'package:app/env_config.dart';
import 'package:app/main.dart';
import 'package:app/module/share_module/component_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'app_state.dart';

class MyApp extends StatelessWidget with WidgetsBindingObserver {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance?.addObserver(this);
    if (Platform.isIOS) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ));
    } else {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.dark,
      ));
    }
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

    return ChangeNotifierProvider<AppState>(
      child: Consumer<AppState>(builder: (context, model, child) {
        model.checkTokenExpire();
        if (EnvConfig.isDevelopment == false) {
          return MaterialApp(
            builder: (context, child) {
              final MediaQueryData data = MediaQuery.of(context);
              final scale = data.textScaleFactor;
              if (child == null) return Container(color: Colors.white);
              return MediaQuery(
                data: data.copyWith(
                    textScaleFactor: Platform.isIOS
                        ? 1.0
                        : scale > 1.3
                            ? 1.3
                            : scale),
                child: child,
              );
            },
            debugShowCheckedModeBanner: false,
            title: 'TCC PAGE MODULE',
            theme: ThemeData(
                scaffoldBackgroundColor: Color(0xffffffff),
                primaryColor: null,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent),
                inputDecorationTheme: InputDecorationTheme(
                    labelStyle: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFE0E0E0), width: 1.0),
                    ))),
            onGenerateRoute: (settings) => model.generateRoute(settings),
          );
        } else {
          return MaterialApp(
            navigatorKey: navigatorKey,
            builder: (context, child) {
              final MediaQueryData data = MediaQuery.of(context);
              final scale = data.textScaleFactor;
              if (child == null) return Container(color: Colors.white);
              return MediaQuery(
                data: data.copyWith(
                    textScaleFactor: Platform.isIOS
                        ? 1.0
                        : scale > 1.3
                            ? 1.3
                            : scale),
                child: child,
              );
            },
            debugShowCheckedModeBanner: false,
            title: 'TCC PAGE MODULE',
            theme: ThemeData(
                scaffoldBackgroundColor: Color(0xffffffff),
                primaryColor: null,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent),
                inputDecorationTheme: InputDecorationTheme(
                    labelStyle: TextStyle(
                      fontFamily: 'SukhumvitSet-Text',
                      color: Color(0xff6d6d6d),
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFE0E0E0), width: 1.0),
                    ))),
            initialRoute: '/',
            onGenerateRoute: (settings) => model.generateRoute(settings),
            home: ComponentPageScreen(),
          );
        }
      }),
      create: (_) => AppState(),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('AppLifecycleState: $state');
    if (state == AppLifecycleState.resumed) {
      if (Platform.isIOS) {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: Colors.black,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.light,
        ));
      } else {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.dark,
        ));
      }
    }
  }
}
