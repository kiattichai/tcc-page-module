import 'dart:async';
import 'dart:convert';
import 'package:app/app/app_localization.dart';
import 'package:app/core/base_view_model.dart';
import 'package:app/env_config.dart';
import 'package:app/globals.dart';
import 'package:app/model/create_page_entity.dart';
import 'package:app/model/get_artist_entity.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';
import 'package:app/model/language_entity.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';
import 'package:app/model_from_native/app_constant.dart';
import 'package:app/model_from_native/near_by_store_entity.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_confirm.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_history.dart';
import 'package:app/module/agent_withdraw_module/agent_withdraw_screen.dart';
import 'package:app/module/concert_agent_module/concert_agent_detail_screen.dart';
import 'package:app/module/concert_agent_module/concert_agent_screen.dart';
import 'package:app/module/create_concert_module/add_venue_screen.dart';
import 'package:app/module/create_concert_module/create_concert_genre.dart';
import 'package:app/module/create_concert_module/create_concert_image.dart';
import 'package:app/module/create_concert_module/create_concert_name.dart';
import 'package:app/module/create_concert_module/create_concert_place.dart';
import 'package:app/module/create_concert_module/create_concert_time.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';
import 'package:app/module/create_concert_module/select_artist.dart';
import 'package:app/module/create_discount_code_module/create_discount_screen.dart';
import 'package:app/module/create_discount_code_module/discount_screen.dart';
import 'package:app/module/create_discount_code_module/manage_code_scren.dart';
import 'package:app/module/create_page_module/create_page_add_image.dart';
import 'package:app/module/create_page_module/create_page_info.dart';
import 'package:app/module/create_page_module/create_page_name.dart';
import 'package:app/module/create_page_module/create_page_type.dart';
import 'package:app/module/create_page_module/map_info.dart';
import 'package:app/module/create_page_module/model/page_info_model.dart';
import 'package:app/module/log_in_module/login_screen.dart';
import 'package:app/module/log_in_module/model/get_user_authen_entity.dart';
import 'package:app/module/nightlife_passport_module/nightlife_passport_screen.dart';
import 'package:app/module/nightlife_passport_module/register_nightlife_screen.dart';
import 'package:app/module/public_page_module/add_gallery_screen.dart';
import 'package:app/module/public_page_module/add_product_screen.dart';
import 'package:app/module/public_page_module/component/menu_full_screen.dart';
import 'package:app/module/public_page_module/create_page_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/cities_list_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/district_list_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_bank_item.dart';
import 'package:app/module/public_page_module/edit_page_profile/document/document_ticket_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_gallery_image_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_profile_page.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_profile_page_menu.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_publish_page.dart';
import 'package:app/module/public_page_module/edit_page_profile/edit_time_open.dart';
import 'package:app/module/public_page_module/edit_page_profile/invoice_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/province_list_screen.dart';
import 'package:app/module/public_page_module/edit_page_profile/select_genre_screen.dart';
import 'package:app/module/public_page_module/home_page_screen.dart';
import 'package:app/module/public_page_module/widget/checkIn/check_in_screen.dart';
import 'package:app/module/public_page_module/widget/concert/concert_list_screen.dart';
import 'package:app/module/public_page_module/widget/concert/concert_ticket_list.dart';
import 'package:app/module/public_page_module/widget/concert/create_concert_ticket.dart';
import 'package:app/module/public_page_module/widget/concert/edit_concert.dart';
import 'package:app/module/public_page_module/widget/concert/manage_concert_menu.dart';
import 'package:app/module/public_page_module/widget/concert/payment_channel.dart';
import 'package:app/module/public_page_module/widget/concert/set_fee_concert.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_management.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_scan.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_term_and_condition.dart';
import 'package:app/module/public_page_module/widget/concert/ticket_tracking.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_screen.dart';
import 'package:app/module/public_page_module/widget/gallery/gallery_slide.dart';
import 'package:app/module/public_page_module/widget/review/edit_review_screen.dart';
import 'package:app/module/public_page_module/widget/review/review_screen.dart';
import 'package:app/module/register_agent_module/agent_card_screen.dart';
import 'package:app/module/register_agent_module/agent_adress_screen.dart';
import 'package:app/module/register_agent_module/agent_bank_screen.dart';
import 'package:app/module/register_agent_module/agent_detail_screen.dart';
import 'package:app/module/register_agent_module/register_agent_terms_and_condition.dart';
import 'package:app/module/register_agent_module/widget/cities_list_widget.dart';
import 'package:app/module/register_agent_module/widget/districts_list_widget.dart';
import 'package:app/module/register_agent_module/widget/province_list_widget.dart';
import 'package:app/module/share_module/component_screen.dart';
import 'package:app/utils/jwt_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_boost/flutter_native_boost.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tix_navigate/tix_navigate.dart';

class AppState extends BaseViewModel {
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  Locale? _appLocale;
  Key key = new UniqueKey();
  bool forceLogin = false;
  bool maintenanceMode = false;
  bool launchScreenBuildSuccess = false;
  bool settingsUsePasscode = false;
  late MethodChannel channel;
  SharedPreferences? sharedPreferences;
  String initRoute = '/';
  NearByStoreEntity? nearByStoreEntity;

  AppState() {
    initSharePreference();
    channel = MethodChannel('tcc_flutter');
    channel.setMethodCallHandler((MethodCall call) async {
      print(call.method);
      switch (call.method) {
        case 'ACTION_OPEN_PUBLIC_PAGE':
          openPublicPage(call.arguments);
          break;
        case 'ACTION_OPEN_MAIN_PAGE':
          openMain(call.arguments);
          break;
        case 'ACTION_OPEN_REGISTER_AGENT':
          openRegisterAgent(call.arguments);
          break;
        case 'ACTION_OPEN_EDIT_AGENT':
          openEditAgent(call.arguments);
          break;
        case 'INIT_TOKEN':
          saveToken(call.arguments);
          break;
        case 'LOGOUT':
          clearToken();
          break;
      }
    });
  }

  void initSharePreference() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  void saveToken(arguments) {
    print("saveToken $arguments");
    try {
      Map<String, dynamic> model = jsonDecode(arguments);
      final data = GetUserAuthenData.fromJson(model);
      userAuthen = GetUserAuthenEntity()..data = data;
      print("saveTokenSuccess ${userAuthen?.toJson()}");
      notifyListeners();
      checkTokenExpire();
    } catch (e, s) {
      userAuthen = null;
      notifyListeners();
    }
  }

  void checkTokenExpire() {
    try {
      if (userAuthen != null) {
        final isExpired =
            JwtUtils().isTokenExpired(userAuthen?.data?.accessToken ?? '');
        if (isExpired) {
          print("token is Expired and clear token");
          clearToken();
        }
      }
    } catch (e) {
      clearToken();
    }
  }

  void clearToken() {
    userAuthen = null;
    notifyListeners();
  }

  void openPublicPage(arguments) async {
    print("openPublicPage\n$arguments");
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        // initRoute = 'ACTION_OPEN_PUBLIC_PAGE';
        Map<String, dynamic> model = jsonDecode(arguments);
        nearByStoreEntity = NearByStoreEntity.fromJson(model);
        TixNavigate.instance.navigateTo(MyHomePage(),
            data: {'storeId': nearByStoreEntity?.id, 'isOwner': false});
        // notifyListeners();
      });
    } catch (e) {}
  }

  void openMain(arguments) async {
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        initRoute = '/';
        notifyListeners();
      });
    } catch (e) {}
  }

  void openRegisterAgent(arguments) async {
    print("openPublicPage\n$arguments");
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        TixNavigate.instance.navigateTo(RegisterAgentTermsAndCondition());
        // notifyListeners();
      });
    } catch (e) {}
  }

  void openEditAgent(arguments) async {
    print("openPublicPage\n$arguments");
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        TixNavigate.instance.navigateTo(AgentDetailScreen());
        // notifyListeners();
      });
    } catch (e) {}
  }

  void openConcertAgent(arguments) async {
    print("openPublicPage\n$arguments");
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        TixNavigate.instance.navigateTo(ConcertAgentScreen());
        // notifyListeners();
      });
    } catch (e) {}
  }

  void openWithdrawAgent(arguments) async {
    print("openWithdrawAgent\n$arguments");
    try {
      initRoute = '/loading';
      notifyListeners();
      Future.delayed(Duration(milliseconds: 200), () {
        TixNavigate.instance.navigateTo(AgentWithdrawScreen());
        // notifyListeners();
      });
    } catch (e) {}
  }

  void reStartApp() {
    key = new UniqueKey();
    navigatorKey = GlobalKey<NavigatorState>();
    notifyListeners();
  }

  Locale get appLocal => _appLocale ?? Locale("th");

  fetchLocale() async {
    return Null;
  }

  Future<LanguageEntity> mockLocal() async {
    final app = AppLocalizations(Locale("th"));
    await app.load();
    return app.languageEntity;
  }

  @override
  void dispose() {
    super.dispose();
  }

  void launchScreenBuild() {
    launchScreenBuildSuccess = true;
  }

  Map<String, RouteFactory> routes = {
    ACTION_OPEN_PUBLIC_PAGE: (settings) => CupertinoPageRoute(
        builder: (context) {
          if (settings.arguments is int) {
            String data = settings.arguments.toString();
            return MyHomePage(isOwner: false, storeId: data);
          } else {
            return SizedBox();
          }
        },
        settings: settings),
    ACTION_OPEN_REGISTER_AGENT: (settings) => CupertinoPageRoute(
        builder: (context) {
          return RegisterAgentTermsAndCondition();
        },
        settings: settings),
    ACTION_OPEN_CONCERT_AGENT: (settings) => CupertinoPageRoute(
        builder: (context) {
          return ConcertAgentScreen();
        },
        settings: settings),
    ACTION_OPEN_WITHDRAW_AGENT: (settings) => CupertinoPageRoute(
        builder: (context) {
          return AgentWithdrawScreen();
        },
        settings: settings),
    ACTION_OPEN_EDIT_AGENT: (settings) => CupertinoPageRoute(
        builder: (context) {
          return AgentDetailScreen();
        },
        settings: settings),
    ACTION_OPEN_REGISTER_NIGHTLIFE: (settings) => CupertinoPageRoute(
        builder: (context) {
          if (settings.arguments is String) {
            bool forceFocus = settings.arguments.toString() == 'force_focus';
            return RegisterNightlifeScreen(forceFocus: forceFocus);
          } else {
            return RegisterNightlifeScreen();
          }
        },
        settings: settings),
    ACTION_OPEN_NIGHTLIFE_PASSPORT: (settings) => CupertinoPageRoute(
        builder: (context) {
          return NightlifePassportScreen();
        },
        settings: settings),
    ACTION_OPEN_CONCERT_AGENT_DETAIL: (settings) => CupertinoPageRoute(
        builder: (context) {
          if (settings.arguments is String) {
            String data = settings.arguments as String;
            return ConcertAgentDetailScreen(concertId: int.parse(data));
          } else {
            return SizedBox();
          }
        },
        settings: settings),
    NightlifePassportScreen().buildPath(): (settings) =>
        CupertinoPageRoute(builder: (context) {
          return NightlifePassportScreen();
        }),
    RegisterNightlifeScreen().buildPath(): (settings) =>
        CupertinoPageRoute(builder: (context) {
          return RegisterNightlifeScreen();
        }),
    ConcertListScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ConcertListScreen(
                storeId: data['storeId'],
                storeName: data['storeName'],
                isOwner: data['isOwner']);
          } else {
            return SizedBox();
          }
        }),
    AddGalleryScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is String) {
            return AddGalleryScreen(storeId: settings.arguments.toString());
          } else {
            return SizedBox();
          }
        }),
    CreatePageScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return CreatePageScreen();
        }),
    AddVenueScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return AddVenueScreen(storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertGenre().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is SaveConcertEntity) {
            SaveConcertEntity data = settings.arguments as SaveConcertEntity;
            return CreateConcertGenre(saveConcertEntity: data);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertImage().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is SaveConcertEntity) {
            SaveConcertEntity data = settings.arguments as SaveConcertEntity;
            return CreateConcertImage(saveConcertEntity: data);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertName().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CreateConcertName(storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertPlace().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is SaveConcertEntity) {
            SaveConcertEntity data = settings.arguments as SaveConcertEntity;
            return CreateConcertPlace(saveConcertEntity: data);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertTime().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            SaveConcertEntity data = settings.arguments as SaveConcertEntity;
            return CreateConcertTime(saveConcertEntity: data);
          } else {
            return SizedBox();
          }
        }),
    SelectArtist().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is List<GetArtistDataRecord>) {
            List<GetArtistDataRecord> data =
                settings.arguments as List<GetArtistDataRecord>;
            return SelectArtist(artist: data);
          } else {
            return SizedBox();
          }
        }),
    PaymentChannel().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return PaymentChannel(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    CreateDiscountScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CreateDiscountScreen(
              concertId: data['concertId'],
              storeId: data['storeId'],
              discountDetail: data['discountRecord'],
            );
          } else {
            return SizedBox();
          }
        }),
    DiscountScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return DiscountScreen(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    MapInfo().buildPath(): (settings) => MaterialPageRoute(builder: (context) {
          if (settings.arguments is LatLng) {
            LatLng data = settings.arguments as LatLng;
            return MapInfo(latLng: data);
          } else {
            return SizedBox();
          }
        }),
    ManageCodeScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ManageCodeScreen(
              storeId: data['storeId'],
              discountId: data['discountId'],
            );
          } else {
            return SizedBox();
          }
        }),
    CreatePageAddImage().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is CreatePageEntity) {
            CreatePageEntity data = settings.arguments as CreatePageEntity;
            return CreatePageAddImage(createPage: data);
          } else {
            return SizedBox();
          }
        }),
    CreatePageType().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is CreatePageEntity) {
            CreatePageEntity data = settings.arguments as CreatePageEntity;
            return CreatePageType(createPage: data);
          } else {
            return SizedBox();
          }
        }),
    CreatePageInfo().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is PageInfo) {
            PageInfo data = settings.arguments as PageInfo;
            return CreatePageInfo(pageInfo: data);
          } else {
            return SizedBox();
          }
        }),
    CreatePageName().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return CreatePageName();
        }),
    LoginScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return LoginScreen();
        }),
    AddProductScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return AddProductScreen();
        }),
    CitiesListScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CitiesListScreen(
              citiesId: data['selected'],
              citiesList: data['citiesList'],
            );
          } else {
            return SizedBox();
          }
        }),
    DistrictListScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return DistrictListScreen(
              districtId: data['selected'],
              districtList: data['districtList'],
            );
          } else {
            return SizedBox();
          }
        }),
    DocumentBankItem().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return DocumentBankItem(
              map: data,
            );
          } else {
            return SizedBox();
          }
        }),
    DocumentTicketScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return DocumentTicketScreen(
              storeId: data['storeId'],
            );
          } else {
            return SizedBox();
          }
        }),
    EditGalleryImageScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditGalleryImageScreen(
              storeId: data['storeId'],
            );
          } else {
            return SizedBox();
          }
        }),
    EditProfilePage().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditProfilePage(
              storeId: data['storeId'],
            );
          } else {
            return SizedBox();
          }
        }),
    EditProfilePageMenu().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditProfilePageMenu(
              storeId: data['storeId'],
              status: data['status'],
            );
          } else {
            return SizedBox();
          }
        }),
    EditPublishPage().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditPublishPage(
              storeId: data['storeId'],
              status: data['status'],
            );
          } else {
            return SizedBox();
          }
        }),
    EditTimeOpen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is List<SaveEditProfilePageTimes>) {
            List<SaveEditProfilePageTimes> data =
                settings.arguments as List<SaveEditProfilePageTimes>;
            return EditTimeOpen(
              data: data,
            );
          } else {
            return SizedBox();
          }
        }),
    InvoiceScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return InvoiceScreen(
              storeId: data['storeId'],
            );
          } else {
            return SizedBox();
          }
        }),
    ProvinceListScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ProvinceListScreen(
              provinceId: data['selected'],
              provinceList: data['provinceList'],
            );
          } else {
            return SizedBox();
          }
        }),
    SelectGenreScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is List<SaveEditProfilePageAttributes>) {
            List<SaveEditProfilePageAttributes> data =
                settings.arguments as List<SaveEditProfilePageAttributes>;
            return SelectGenreScreen(
              attributes: data,
            );
          } else {
            return SizedBox();
          }
        }),
    MyHomePage().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return MyHomePage(
              storeId: data['storeId'],
              isOwner: data['isOwner'],
              tabIndex: data['tabIndex'],
            );
          } else {
            return SizedBox();
          }
        }),
    CheckInScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CheckInScreen(
                storeId: data['storeId'], storeName: data['storeName']);
          } else {
            return SizedBox();
          }
        }),
    ConcertTicketList().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ConcertTicketList(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    CreateConcertTicket().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CreateConcertTicket(
                concertId: data['concertId'],
                storeId: data['storeId'],
                ticketId: data['ticketId']);
          } else {
            return SizedBox();
          }
        }),
    EditConcert().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditConcert(concertId: data['concertId']);
          } else {
            return SizedBox();
          }
        }),
    ManageConcertMenu().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ManageConcertMenu(
              storeId: data['storeId'],
              concertId: data['concertId'],
            );
          } else {
            return SizedBox();
          }
        }),
    SetFeeConcert().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return SetFeeConcert(
                concertId: data['concertId'],
                storeId: data['storeId'],
                isPop: data['isPop']);
          } else {
            return SizedBox();
          }
        }),
    TicketManagement().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return TicketManagement(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    TicketScan().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return TicketScan(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    TicketTermAndCondition().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return TicketTermAndCondition();
        }),
    TicketTracking().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return TicketTracking(
                concertId: data['concertId'], storeId: data['storeId']);
          } else {
            return SizedBox();
          }
        }),
    GalleryScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return GalleryScreen(
              storeId: data['storeId'],
              storeName: data['storeName'],
            );
          } else {
            return SizedBox();
          }
        }),
    GalleryScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return GalleryScreen(
              storeId: data['storeId'],
              storeName: data['storeName'],
            );
          } else {
            return SizedBox();
          }
        }),
    GallerySlide().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return GallerySlide(data: data);
          } else {
            return SizedBox();
          }
        }),
    EditReviewScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return EditReviewScreen(
                storeName: data['storeName'],
                storeId: data['storeId'],
                reviewId: data['reviewId']);
          } else {
            return SizedBox();
          }
        }),
    ReviewScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is GetOrganizerStoreDetailData) {
            GetOrganizerStoreDetailData data =
                settings.arguments as GetOrganizerStoreDetailData;
            return ReviewScreen(
              storeDetailData: data,
            );
          } else {
            return SizedBox();
          }
        }),
    ComponentPageScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return ComponentPageScreen();
        }),
    'home': (settings) => MaterialPageRoute(builder: (context) {
          return ComponentPageScreen();
        }),
    RegisterAgentTermsAndCondition().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return RegisterAgentTermsAndCondition();
        }),
    ConcertAgentDetailScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is int) {
            var data = settings.arguments as int;
            return ConcertAgentDetailScreen(
              concertId: data,
            );
          } else {
            return SizedBox();
          }
        }),
    AgentCardScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return AgentCardScreen(isUpdate: data['isUpdate']);
          } else {
            return SizedBox();
          }
        }),
    AgentAddressScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return AgentAddressScreen(
              cardData: data,
              isUpdate: data['isUpdate'],
            );
          } else {
            return SizedBox();
          }
        }),
    AgentBankScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return AgentBankScreen(
              addressData: data,
              isUpdate: data['isUpdate'],
            );
          } else {
            return SizedBox();
          }
        }),
    ProvinceListWidget().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return ProvinceListWidget(
                provinceId: data['selected'],
                provinceList: data['provinceList']);
          } else {
            return SizedBox();
          }
        }),
    CitiesListWidget().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return CitiesListWidget(
                citiesId: data['selected'], citiesList: data['citiesList']);
          } else {
            return SizedBox();
          }
        }),
    DistrictsListWidget().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            Map<String, dynamic> data =
                settings.arguments as Map<String, dynamic>;
            return DistrictsListWidget(
                districtsId: data['selected'],
                districtsList: data['districtsList']);
          } else {
            return SizedBox();
          }
        }),
    AgentDetailScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return AgentDetailScreen();
        }),
    AgentWithdrawConfirm().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is int) {
            var data = settings.arguments as int;
            return AgentWithdrawConfirm(amount: data);
          } else {
            return SizedBox();
          }
        }),
    AgentWithdrawHistory().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          return AgentWithdrawHistory();
        }),
    MenuFullScreen().buildPath(): (settings) =>
        MaterialPageRoute(builder: (context) {
          if (settings.arguments is Map) {
            var data = settings.arguments as Map;
            return MenuFullScreen(data: data);
          } else {
            return SizedBox();
          }
        })
  };

  Route<dynamic> generateRoute(RouteSettings settings) {
    if (EnvConfig.isDevelopment == false) {
      final route = container.wrapper((settings) {
        // //TODO V1
        // final f = routes[settings.name!];
        // if (f == null) return null;
        // return f(settings);
        final f = routes[settings.name];
        if (f != null) {
          return f(settings);
        } else {
          return TixNavigate.instance.generator(settings);
        }
      }, switchPageAnimation: (currentRoute) {
        if (currentRoute['route'] == '') {
          return ((context, child) => AnimatedSwitcher(
                duration: Duration(seconds: 1),
                child: child,
                transitionBuilder: (child, animation) => RotationTransition(
                  turns: animation,
                  child: child,
                ),
              ));
        }
        return null;
      });

      return route;
    } else {
      return TixNavigate.instance.generator(settings);
    }
  }
}
