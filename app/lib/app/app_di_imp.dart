import 'package:app/di/injector.dart';
import 'package:app/module/agent_withdraw_module/data/agent_withdraw_repository.dart';
import 'package:app/module/concert_agent_module/data/concert_agent_repository.dart';
import 'package:app/module/create_concert_module/data/create_concert_repository.dart';
import 'package:app/module/create_page_module/data/location_repository.dart';
import 'package:app/module/log_in_module/data/log_in_repository.dart';
import 'package:app/module/nightlife_passport_module/data/nightlife_repository.dart';
import 'package:app/module/public_page_module/data/check_in_repository.dart';
import 'package:app/module/public_page_module/data/concert_repository.dart';
import 'package:app/module/public_page_module/data/gallery_repository.dart';
import 'package:app/module/public_page_module/data/get_adress_repository.dart';
import 'package:app/module/public_page_module/data/page_repository.dart';
import 'package:app/module/public_page_module/data/review_repository.dart';
import 'package:app/module/register_agent_module/data/agent_repository.dart';
import 'package:app/module/share_module/data/refresh_token_repository.dart';
import 'package:injectable/injectable.dart';

final AppDiImp appDi = getIt<AppDiImp>();

@injectable
class AppDiImp {
  AppDiImp(
      this.pageRepository,
      this.locationRepository,
      this.logInRepository,
      this.reviewRepository,
      this.galleryRepository,
      this.checkInRepository,
      this.getAddressRepository,
      this.createConcertRepository,
      this.concertRepository,
      this.refreshTokenRepository,
      this.concertAgentRepository,
      this.agentRepository,
      this.agentWithdrawRepository,
      this.nightlifeRepository,
      ) {
    print('create App di imp');
  }

  final PageRepository pageRepository;
  final LocationRepository locationRepository;
  final LogInRepository logInRepository;
  final ReviewRepository reviewRepository;
  final GalleryRepository galleryRepository;
  final CheckInRepository checkInRepository;
  final GetAddressRepository getAddressRepository;
  final CreateConcertRepository createConcertRepository;
  final ConcertRepository concertRepository;
  final RefreshTokenRepository refreshTokenRepository;
  final ConcertAgentRepository concertAgentRepository;
  final AgentRepository  agentRepository;
  final AgentWithdrawRepository  agentWithdrawRepository;
  final NightlifeRepository  nightlifeRepository;
}
