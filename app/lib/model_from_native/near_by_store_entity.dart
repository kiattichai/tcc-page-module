import 'package:app/generated/json/near_by_store_entity.g.dart';

import 'package:app/generated/json/base/json_field.dart';

@JsonSerializable()
class NearByStoreEntity {
  NearByStoreEntity();

  factory NearByStoreEntity.fromJson(Map<String, dynamic> json) =>
      $NearByStoreEntityFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreEntityToJson(this);

  int? id;
  NearByStoreImages? images;
  String? name;
  NearByStoreSection? section;
  NearByStoreType? type;
  NearByStoreVenue? venue;
}

@JsonSerializable()
class NearByStoreImages {
  NearByStoreImages();

  factory NearByStoreImages.fromJson(Map<String, dynamic> json) =>
      $NearByStoreImagesFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreImagesToJson(this);

  NearByStoreImagesLogo? logo;
}

@JsonSerializable()
class NearByStoreImagesLogo {
  NearByStoreImagesLogo();

  factory NearByStoreImagesLogo.fromJson(Map<String, dynamic> json) =>
      $NearByStoreImagesLogoFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreImagesLogoToJson(this);

  @JSONField(name: "album_id")
  int? albumId;
  int? height;
  String? id;
  String? mime;
  String? name;
  int? position;
  int? size;
  String? tag;
  String? url;
  int? width;
}

@JsonSerializable()
class NearByStoreSection {
  NearByStoreSection();

  factory NearByStoreSection.fromJson(Map<String, dynamic> json) =>
      $NearByStoreSectionFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreSectionToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class NearByStoreType {
  NearByStoreType();

  factory NearByStoreType.fromJson(Map<String, dynamic> json) =>
      $NearByStoreTypeFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreTypeToJson(this);

  int? id;
  String? text;
}

@JsonSerializable()
class NearByStoreVenue {
  NearByStoreVenue();

  factory NearByStoreVenue.fromJson(Map<String, dynamic> json) =>
      $NearByStoreVenueFromJson(json);

  Map<String, dynamic> toJson() => $NearByStoreVenueToJson(this);

  String? address;
  double? distance;
  int? id;
  double? lat;
  double? long;
  String? name;
}
