// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../api/cms_public_api.dart' as _i21;
import '../api/core_api.dart' as _i22;
import '../api/interceptors/refresh_token_interceptor.dart' as _i20;
import '../api/public_api.dart' as _i19;
import '../app/app_di.dart' as _i37;
import '../app/app_di_imp.dart' as _i4;
import '../module/agent_withdraw_module/data/agent_withdraw_api.dart' as _i33;
import '../module/agent_withdraw_module/data/agent_withdraw_repository.dart'
    as _i17;
import '../module/concert_agent_module/data/concert_agent_api.dart' as _i35;
import '../module/concert_agent_module/data/concert_agent_repository.dart'
    as _i15;
import '../module/create_concert_module/data/create_concert_api.dart' as _i23;
import '../module/create_concert_module/data/create_concert_repository.dart'
    as _i12;
import '../module/create_page_module/data/location_api.dart' as _i26;
import '../module/create_page_module/data/location_repository.dart' as _i6;
import '../module/log_in_module/data/log_in_api.dart' as _i27;
import '../module/log_in_module/data/log_in_repository.dart' as _i7;
import '../module/nightlife_passport_module/data/nightlife_api.dart' as _i28;
import '../module/nightlife_passport_module/data/nightlife_repository.dart'
    as _i18;
import '../module/public_page_module/data/check_in_api.dart' as _i34;
import '../module/public_page_module/data/check_in_repository.dart' as _i10;
import '../module/public_page_module/data/concert_api.dart' as _i36;
import '../module/public_page_module/data/concert_repository.dart' as _i13;
import '../module/public_page_module/data/gallery_api.dart' as _i24;
import '../module/public_page_module/data/gallery_repository.dart' as _i9;
import '../module/public_page_module/data/get_address_api.dart' as _i25;
import '../module/public_page_module/data/get_adress_repository.dart' as _i11;
import '../module/public_page_module/data/page_api.dart' as _i29;
import '../module/public_page_module/data/page_repository.dart' as _i5;
import '../module/public_page_module/data/review_api.dart' as _i31;
import '../module/public_page_module/data/review_repository.dart' as _i8;
import '../module/register_agent_module/data/agent_api.dart' as _i32;
import '../module/register_agent_module/data/agent_repository.dart' as _i16;
import '../module/share_module/data/refresh_token_api.dart' as _i30;
import '../module/share_module/data/refresh_token_repository.dart'
    as _i14; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final dioDi = _$DioDi();
  gh.factory<_i3.Dio>(() => dioDi.dio);
  gh.factory<_i4.AppDiImp>(() => _i4.AppDiImp(
      get<_i5.PageRepository>(),
      get<_i6.LocationRepository>(),
      get<_i7.LogInRepository>(),
      get<_i8.ReviewRepository>(),
      get<_i9.GalleryRepository>(),
      get<_i10.CheckInRepository>(),
      get<_i11.GetAddressRepository>(),
      get<_i12.CreateConcertRepository>(),
      get<_i13.ConcertRepository>(),
      get<_i14.RefreshTokenRepository>(),
      get<_i15.ConcertAgentRepository>(),
      get<_i16.AgentRepository>(),
      get<_i17.AgentWithdrawRepository>(),
      get<_i18.NightlifeRepository>()));
  gh.singleton<_i19.PublicApi>(_i19.PublicApi(get<_i3.Dio>()));
  gh.singleton<_i20.RefreshTokenInterceptor>(
      _i20.RefreshTokenInterceptor(get<_i3.Dio>()));
  gh.singleton<_i21.CmsPublicApi>(_i21.CmsPublicApi(get<_i3.Dio>()));
  gh.singleton<_i22.CoreApi>(_i22.CoreApi(get<_i3.Dio>()));
  gh.singleton<_i23.CreateConcertApi>(
      _i23.CreateConcertApi(get<_i22.CoreApi>()));
  gh.singleton<_i12.CreateConcertRepository>(
      _i12.CreateConcertRepository(get<_i23.CreateConcertApi>()));
  gh.singleton<_i24.GalleryApi>(_i24.GalleryApi(get<_i22.CoreApi>()));
  gh.singleton<_i9.GalleryRepository>(
      _i9.GalleryRepository(get<_i24.GalleryApi>()));
  gh.singleton<_i25.GetAddressApi>(_i25.GetAddressApi(get<_i22.CoreApi>()));
  gh.singleton<_i11.GetAddressRepository>(
      _i11.GetAddressRepository(get<_i25.GetAddressApi>()));
  gh.singleton<_i26.LocationApi>(_i26.LocationApi(get<_i22.CoreApi>()));
  gh.singleton<_i6.LocationRepository>(
      _i6.LocationRepository(get<_i26.LocationApi>()));
  gh.singleton<_i27.LogInApi>(_i27.LogInApi(get<_i22.CoreApi>()));
  gh.singleton<_i7.LogInRepository>(_i7.LogInRepository(get<_i27.LogInApi>()));
  gh.singleton<_i28.NightlifeApi>(_i28.NightlifeApi(get<_i22.CoreApi>()));
  gh.singleton<_i18.NightlifeRepository>(
      _i18.NightlifeRepository(get<_i28.NightlifeApi>()));
  gh.singleton<_i29.PageApi>(_i29.PageApi(get<_i22.CoreApi>()));
  gh.singleton<_i5.PageRepository>(_i5.PageRepository(get<_i29.PageApi>()));
  gh.singleton<_i30.RefreshTokenApi>(_i30.RefreshTokenApi(get<_i22.CoreApi>()));
  gh.singleton<_i14.RefreshTokenRepository>(
      _i14.RefreshTokenRepository(get<_i30.RefreshTokenApi>()));
  gh.singleton<_i31.ReviewApi>(_i31.ReviewApi(get<_i22.CoreApi>()));
  gh.singleton<_i8.ReviewRepository>(
      _i8.ReviewRepository(get<_i31.ReviewApi>()));
  gh.singleton<_i32.AgentApi>(_i32.AgentApi(get<_i22.CoreApi>()));
  gh.singleton<_i16.AgentRepository>(
      _i16.AgentRepository(get<_i32.AgentApi>()));
  gh.singleton<_i33.AgentWithdrawApi>(
      _i33.AgentWithdrawApi(get<_i22.CoreApi>()));
  gh.singleton<_i17.AgentWithdrawRepository>(
      _i17.AgentWithdrawRepository(get<_i33.AgentWithdrawApi>()));
  gh.singleton<_i34.CheckInApi>(_i34.CheckInApi(get<_i22.CoreApi>()));
  gh.singleton<_i10.CheckInRepository>(
      _i10.CheckInRepository(get<_i34.CheckInApi>()));
  gh.singleton<_i35.ConcertAgentApi>(_i35.ConcertAgentApi(get<_i22.CoreApi>()));
  gh.singleton<_i15.ConcertAgentRepository>(
      _i15.ConcertAgentRepository(get<_i35.ConcertAgentApi>()));
  gh.singleton<_i36.ConcertApi>(_i36.ConcertApi(get<_i22.CoreApi>()));
  gh.singleton<_i13.ConcertRepository>(
      _i13.ConcertRepository(get<_i36.ConcertApi>()));
  return get;
}

class _$DioDi extends _i37.DioDi {}
