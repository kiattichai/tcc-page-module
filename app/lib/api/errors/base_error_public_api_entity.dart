import 'package:app/generated/json/base/json_field.dart';
import 'package:app/generated/json/base_error_public_api_entity.g.dart';

@JsonSerializable()
class BaseErrorPublicApiEntity {
  BaseErrorPublicApiEntity();

  factory BaseErrorPublicApiEntity.fromJson(Map<String, dynamic> json) =>
      $BaseErrorPublicApiEntityFromJson(json);

  Map<String, dynamic> toJson() => $BaseErrorPublicApiEntityToJson(this);

  dynamic? errors;
  String? message;
}
