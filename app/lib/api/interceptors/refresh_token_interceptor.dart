import 'package:app/model_from_native/app_constant.dart';
import 'package:app/module/share_module/data/refresh_token_repository.dart';
import 'package:app/module/share_module/model/refresh_token_response_entity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import '../../../globals.dart' as globals;

@singleton
class RefreshTokenInterceptor extends Interceptor {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  final Dio _dio;
  final String pathRefreshToken = "/users/refresh-token";
  RefreshTokenInterceptor(this._dio);

  @override
  Future onError(DioError error, ErrorInterceptorHandler handler) async {
    if (error.response?.statusCode == 403) {
      var refreshToken = globals.userAuthen?.data?.refreshToken ?? '';
      if (refreshToken.isNotEmpty) {
        var result = await putRefreshToken(refreshToken);
        if (result) {
          error.requestOptions.headers["Authorization"] =
              "${globals.userAuthen?.data?.tokenType ?? ''} " +
                  '${globals.userAuthen?.data?.refreshToken ?? ''}';
          final opts = new Options(
              method: error.requestOptions.method, headers: error.requestOptions.headers);
          final cloneReq = await _dio.request(error.requestOptions.path,
              options: opts,
              data: error.requestOptions.data,
              queryParameters: error.requestOptions.queryParameters);
          return handler.resolve(cloneReq);
        } else {
          await navigatorKey.currentState?.pushNamed(ACTION_OPEN_LOGIN_PAGE);
        }
      }
    }
    return handler.next(error);
  }

  Future<bool> putRefreshToken(String token) async {
    final option = Options(headers: {'X-Api-Refresh-Token': token});
    option.extra = {"hasPermission": true};
    Response response = await _dio.put(pathRefreshToken, options: option);
    final responseEntity = RefreshTokenResponseEntity.fromJson(response.data);
    if (responseEntity != null && responseEntity.data != null) {
      final data = responseEntity.data;
      globals.userAuthen!.data!.refreshToken = data!.accessToken!;
      globals.userAuthen!.data!.accessToken = data.accessToken!;
      globals.userAuthen!.data!.tokenType = data.tokenType!;
      return true;
    } else {
      return false;
    }
  }
}
