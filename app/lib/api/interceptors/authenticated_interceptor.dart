import 'package:dio/dio.dart';
import '../../../globals.dart' as globals;

class AuthenticatedInterceptor extends Interceptor {
  // final SharePrefInterface share;
  // AuthenticatedInterceptor(this.share);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    if (options.extra.containsKey('hasPermission')) {
      //method get
      if (options.extra['hasPermission']) {
        if (globals.userAuthen?.data?.accessToken != null) {
          options.headers['Authorization'] =
              '${globals.userAuthen?.data?.tokenType ?? ''} ${globals.userAuthen?.data?.accessToken ?? ''}';
          options.headers['X-Api-Refresh-Token'] = globals.userAuthen?.data?.refreshToken ?? '';
        }
      } else {
        options.headers['Authorization'] = '';
      }
    } else {
      options.headers['Authorization'] = '';
    }
    options.headers['Accept-Language'] = 'th';
    return handler.next(options);
  }
}
