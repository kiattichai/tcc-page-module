import 'package:app/app/tcc_page_app.dart';
import 'package:app/env_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tix_navigate/tix_navigate.dart';

import 'di/injector.dart';

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() {
  EnvConfig.isDevelopment = false;
  EnvConfig.isProductionURL = false;
  SharedPreferences.setMockInitialValues({});
  initializeDateFormatting();
  configureDependencies();
  TixNavigate.instance.configRoute([], key: navigatorKey);
  runApp(MyApp());
}
