class EnvConfig {
  static bool isDevelopment = true;
  static bool isProductionURL = true;
  static String baseUrlAlpha = 'https://alpha-api.theconcert.com';
  static String baseUrlProd = 'https://api.theconcert.com';
  static String resUrl = isProductionURL
      ? "https://res.theconcert.com"
      : "https://alpha-res.theconcert.com";
  static String getBaseUrl() {
    return isProductionURL ? baseUrlProd : baseUrlAlpha;
  }
}
