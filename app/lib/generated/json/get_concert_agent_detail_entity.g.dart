import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_detail_entity.dart';

GetConcertAgentDetailEntity $GetConcertAgentDetailEntityFromJson(
    Map<String, dynamic> json) {
  final GetConcertAgentDetailEntity getConcertAgentDetailEntity =
      GetConcertAgentDetailEntity();
  final GetConcertAgentDetailData? data =
      jsonConvert.convert<GetConcertAgentDetailData>(json['data']);
  if (data != null) {
    getConcertAgentDetailEntity.data = data;
  }
  final GetConcertAgentDetailBench? bench =
      jsonConvert.convert<GetConcertAgentDetailBench>(json['bench']);
  if (bench != null) {
    getConcertAgentDetailEntity.bench = bench;
  }
  return getConcertAgentDetailEntity;
}

Map<String, dynamic> $GetConcertAgentDetailEntityToJson(
    GetConcertAgentDetailEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetConcertAgentDetailData $GetConcertAgentDetailDataFromJson(
    Map<String, dynamic> json) {
  final GetConcertAgentDetailData getConcertAgentDetailData =
      GetConcertAgentDetailData();
  final GetConcertAgentDetailDataRecord? record =
      jsonConvert.convert<GetConcertAgentDetailDataRecord>(json['record']);
  if (record != null) {
    getConcertAgentDetailData.record = record;
  }
  return getConcertAgentDetailData;
}

Map<String, dynamic> $GetConcertAgentDetailDataToJson(
    GetConcertAgentDetailData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.toJson();
  return data;
}

GetConcertAgentDetailDataRecord $GetConcertAgentDetailDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecord getConcertAgentDetailDataRecord =
      GetConcertAgentDetailDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecord.name = name;
  }
  final GetConcertAgentDetailDataRecordStore? store =
      jsonConvert.convert<GetConcertAgentDetailDataRecordStore>(json['store']);
  if (store != null) {
    getConcertAgentDetailDataRecord.store = store;
  }
  final GetConcertAgentDetailDataRecordVenue? venue =
      jsonConvert.convert<GetConcertAgentDetailDataRecordVenue>(json['venue']);
  if (venue != null) {
    getConcertAgentDetailDataRecord.venue = venue;
  }
  final GetConcertAgentDetailDataRecordShowTime? showTime = jsonConvert
      .convert<GetConcertAgentDetailDataRecordShowTime>(json['show_time']);
  if (showTime != null) {
    getConcertAgentDetailDataRecord.showTime = showTime;
  }
  final List<GetConcertAgentDetailDataRecordCommissionableTickets>?
      commissionableTickets = jsonConvert.convertListNotNull<
              GetConcertAgentDetailDataRecordCommissionableTickets>(
          json['commissionable_tickets']);
  if (commissionableTickets != null) {
    getConcertAgentDetailDataRecord.commissionableTickets =
        commissionableTickets;
  }
  final int? promotionId = jsonConvert.convert<int>(json['promotion_id']);
  if (promotionId != null) {
    getConcertAgentDetailDataRecord.promotionId = promotionId;
  }
  final List<GetConcertAgentDetailDataRecordConcertImages>? concertImages =
      jsonConvert.convertListNotNull<
          GetConcertAgentDetailDataRecordConcertImages>(json['concert_images']);
  if (concertImages != null) {
    getConcertAgentDetailDataRecord.concertImages = concertImages;
  }
  final bool? isCommissioner =
      jsonConvert.convert<bool>(json['is_commissioner']);
  if (isCommissioner != null) {
    getConcertAgentDetailDataRecord.isCommissioner = isCommissioner;
  }
  final String? afLink = jsonConvert.convert<String>(json['af_link']);
  if (afLink != null) {
    getConcertAgentDetailDataRecord.afLink = afLink;
  }
  return getConcertAgentDetailDataRecord;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordToJson(
    GetConcertAgentDetailDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['store'] = entity.store?.toJson();
  data['venue'] = entity.venue?.toJson();
  data['show_time'] = entity.showTime?.toJson();
  data['commissionable_tickets'] =
      entity.commissionableTickets?.map((v) => v.toJson()).toList();
  data['promotion_id'] = entity.promotionId;
  data['concert_images'] =
      entity.concertImages?.map((v) => v.toJson()).toList();
  data['is_commissioner'] = entity.isCommissioner;
  data['af_link'] = entity.afLink;
  return data;
}

GetConcertAgentDetailDataRecordStore
    $GetConcertAgentDetailDataRecordStoreFromJson(Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordStore
      getConcertAgentDetailDataRecordStore =
      GetConcertAgentDetailDataRecordStore();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordStore.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordStore.name = name;
  }
  final GetConcertAgentDetailDataRecordStoreSection? section = jsonConvert
      .convert<GetConcertAgentDetailDataRecordStoreSection>(json['section']);
  if (section != null) {
    getConcertAgentDetailDataRecordStore.section = section;
  }
  return getConcertAgentDetailDataRecordStore;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordStoreToJson(
    GetConcertAgentDetailDataRecordStore entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['section'] = entity.section?.toJson();
  return data;
}

GetConcertAgentDetailDataRecordStoreSection
    $GetConcertAgentDetailDataRecordStoreSectionFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordStoreSection
      getConcertAgentDetailDataRecordStoreSection =
      GetConcertAgentDetailDataRecordStoreSection();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordStoreSection.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getConcertAgentDetailDataRecordStoreSection.text = text;
  }
  return getConcertAgentDetailDataRecordStoreSection;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordStoreSectionToJson(
    GetConcertAgentDetailDataRecordStoreSection entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetConcertAgentDetailDataRecordVenue
    $GetConcertAgentDetailDataRecordVenueFromJson(Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordVenue
      getConcertAgentDetailDataRecordVenue =
      GetConcertAgentDetailDataRecordVenue();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getConcertAgentDetailDataRecordVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    getConcertAgentDetailDataRecordVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordVenue.name = name;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getConcertAgentDetailDataRecordVenue.address = address;
  }
  final GetConcertAgentDetailDataRecordVenueCountry? country = jsonConvert
      .convert<GetConcertAgentDetailDataRecordVenueCountry>(json['country']);
  if (country != null) {
    getConcertAgentDetailDataRecordVenue.country = country;
  }
  final GetConcertAgentDetailDataRecordVenueProvince? province = jsonConvert
      .convert<GetConcertAgentDetailDataRecordVenueProvince>(json['province']);
  if (province != null) {
    getConcertAgentDetailDataRecordVenue.province = province;
  }
  final GetConcertAgentDetailDataRecordVenueCity? city = jsonConvert
      .convert<GetConcertAgentDetailDataRecordVenueCity>(json['city']);
  if (city != null) {
    getConcertAgentDetailDataRecordVenue.city = city;
  }
  final GetConcertAgentDetailDataRecordVenueDistrict? district = jsonConvert
      .convert<GetConcertAgentDetailDataRecordVenueDistrict>(json['district']);
  if (district != null) {
    getConcertAgentDetailDataRecordVenue.district = district;
  }
  final int? zipCode = jsonConvert.convert<int>(json['zip_code']);
  if (zipCode != null) {
    getConcertAgentDetailDataRecordVenue.zipCode = zipCode;
  }
  return getConcertAgentDetailDataRecordVenue;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordVenueToJson(
    GetConcertAgentDetailDataRecordVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  data['address'] = entity.address;
  data['country'] = entity.country?.toJson();
  data['province'] = entity.province?.toJson();
  data['city'] = entity.city?.toJson();
  data['district'] = entity.district?.toJson();
  data['zip_code'] = entity.zipCode;
  return data;
}

GetConcertAgentDetailDataRecordVenueCountry
    $GetConcertAgentDetailDataRecordVenueCountryFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordVenueCountry
      getConcertAgentDetailDataRecordVenueCountry =
      GetConcertAgentDetailDataRecordVenueCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordVenueCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordVenueCountry.name = name;
  }
  return getConcertAgentDetailDataRecordVenueCountry;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordVenueCountryToJson(
    GetConcertAgentDetailDataRecordVenueCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetConcertAgentDetailDataRecordVenueProvince
    $GetConcertAgentDetailDataRecordVenueProvinceFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordVenueProvince
      getConcertAgentDetailDataRecordVenueProvince =
      GetConcertAgentDetailDataRecordVenueProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordVenueProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordVenueProvince.name = name;
  }
  return getConcertAgentDetailDataRecordVenueProvince;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordVenueProvinceToJson(
    GetConcertAgentDetailDataRecordVenueProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetConcertAgentDetailDataRecordVenueCity
    $GetConcertAgentDetailDataRecordVenueCityFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordVenueCity
      getConcertAgentDetailDataRecordVenueCity =
      GetConcertAgentDetailDataRecordVenueCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordVenueCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordVenueCity.name = name;
  }
  return getConcertAgentDetailDataRecordVenueCity;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordVenueCityToJson(
    GetConcertAgentDetailDataRecordVenueCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetConcertAgentDetailDataRecordVenueDistrict
    $GetConcertAgentDetailDataRecordVenueDistrictFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordVenueDistrict
      getConcertAgentDetailDataRecordVenueDistrict =
      GetConcertAgentDetailDataRecordVenueDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordVenueDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordVenueDistrict.name = name;
  }
  return getConcertAgentDetailDataRecordVenueDistrict;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordVenueDistrictToJson(
    GetConcertAgentDetailDataRecordVenueDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetConcertAgentDetailDataRecordShowTime
    $GetConcertAgentDetailDataRecordShowTimeFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordShowTime
      getConcertAgentDetailDataRecordShowTime =
      GetConcertAgentDetailDataRecordShowTime();
  final String? start = jsonConvert.convert<String>(json['start']);
  if (start != null) {
    getConcertAgentDetailDataRecordShowTime.start = start;
  }
  final String? end = jsonConvert.convert<String>(json['end']);
  if (end != null) {
    getConcertAgentDetailDataRecordShowTime.end = end;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getConcertAgentDetailDataRecordShowTime.textFull = textFull;
  }
  final String? textShort = jsonConvert.convert<String>(json['text_short']);
  if (textShort != null) {
    getConcertAgentDetailDataRecordShowTime.textShort = textShort;
  }
  final String? textShortDate =
      jsonConvert.convert<String>(json['text_short_date']);
  if (textShortDate != null) {
    getConcertAgentDetailDataRecordShowTime.textShortDate = textShortDate;
  }
  final int? status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    getConcertAgentDetailDataRecordShowTime.status = status;
  }
  final String? statusText = jsonConvert.convert<String>(json['status_text']);
  if (statusText != null) {
    getConcertAgentDetailDataRecordShowTime.statusText = statusText;
  }
  return getConcertAgentDetailDataRecordShowTime;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordShowTimeToJson(
    GetConcertAgentDetailDataRecordShowTime entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['start'] = entity.start;
  data['end'] = entity.end;
  data['text_full'] = entity.textFull;
  data['text_short'] = entity.textShort;
  data['text_short_date'] = entity.textShortDate;
  data['status'] = entity.status;
  data['status_text'] = entity.statusText;
  return data;
}

GetConcertAgentDetailDataRecordCommissionableTickets
    $GetConcertAgentDetailDataRecordCommissionableTicketsFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordCommissionableTickets
      getConcertAgentDetailDataRecordCommissionableTickets =
      GetConcertAgentDetailDataRecordCommissionableTickets();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.name = name;
  }
  final int? price = jsonConvert.convert<int>(json['price']);
  if (price != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.price = price;
  }
  final int? commissionerReward =
      jsonConvert.convert<int>(json['commissioner_reward']);
  if (commissionerReward != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.commissionerReward =
        commissionerReward;
  }
  final String? startDate = jsonConvert.convert<String>(json['start_date']);
  if (startDate != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.startDate = startDate;
  }
  final String? endDate = jsonConvert.convert<String>(json['end_date']);
  if (endDate != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.endDate = endDate;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.textFull = textFull;
  }
  final String? textShort = jsonConvert.convert<String>(json['text_short']);
  if (textShort != null) {
    getConcertAgentDetailDataRecordCommissionableTickets.textShort = textShort;
  }
  return getConcertAgentDetailDataRecordCommissionableTickets;
}

Map<String, dynamic>
    $GetConcertAgentDetailDataRecordCommissionableTicketsToJson(
        GetConcertAgentDetailDataRecordCommissionableTickets entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['price'] = entity.price;
  data['commissioner_reward'] = entity.commissionerReward;
  data['start_date'] = entity.startDate;
  data['end_date'] = entity.endDate;
  data['text_full'] = entity.textFull;
  data['text_short'] = entity.textShort;
  return data;
}

GetConcertAgentDetailDataRecordConcertImages
    $GetConcertAgentDetailDataRecordConcertImagesFromJson(
        Map<String, dynamic> json) {
  final GetConcertAgentDetailDataRecordConcertImages
      getConcertAgentDetailDataRecordConcertImages =
      GetConcertAgentDetailDataRecordConcertImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getConcertAgentDetailDataRecordConcertImages.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getConcertAgentDetailDataRecordConcertImages.storeId = storeId;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getConcertAgentDetailDataRecordConcertImages.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getConcertAgentDetailDataRecordConcertImages.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertAgentDetailDataRecordConcertImages.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getConcertAgentDetailDataRecordConcertImages.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getConcertAgentDetailDataRecordConcertImages.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getConcertAgentDetailDataRecordConcertImages.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getConcertAgentDetailDataRecordConcertImages.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getConcertAgentDetailDataRecordConcertImages.url = url;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getConcertAgentDetailDataRecordConcertImages.position = position;
  }
  return getConcertAgentDetailDataRecordConcertImages;
}

Map<String, dynamic> $GetConcertAgentDetailDataRecordConcertImagesToJson(
    GetConcertAgentDetailDataRecordConcertImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['position'] = entity.position;
  return data;
}

GetConcertAgentDetailBench $GetConcertAgentDetailBenchFromJson(
    Map<String, dynamic> json) {
  final GetConcertAgentDetailBench getConcertAgentDetailBench =
      GetConcertAgentDetailBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getConcertAgentDetailBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getConcertAgentDetailBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getConcertAgentDetailBench.format = format;
  }
  return getConcertAgentDetailBench;
}

Map<String, dynamic> $GetConcertAgentDetailBenchToJson(
    GetConcertAgentDetailBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
