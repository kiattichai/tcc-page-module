import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/update_ticket_status_entity.dart';

UpdateTicketStatusEntity $UpdateTicketStatusEntityFromJson(
    Map<String, dynamic> json) {
  final UpdateTicketStatusEntity updateTicketStatusEntity =
      UpdateTicketStatusEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    updateTicketStatusEntity.storeId = storeId;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    updateTicketStatusEntity.status = status;
  }
  return updateTicketStatusEntity;
}

Map<String, dynamic> $UpdateTicketStatusEntityToJson(
    UpdateTicketStatusEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['status'] = entity.status;
  return data;
}
