import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/save_page_avatar_entity.dart';

SavePageAvatarEntity $SavePageAvatarEntityFromJson(Map<String, dynamic> json) {
  final SavePageAvatarEntity savePageAvatarEntity = SavePageAvatarEntity();
  final String? fileName = jsonConvert.convert<String>(json['file_name']);
  if (fileName != null) {
    savePageAvatarEntity.fileName = fileName;
  }
  final String? fileData = jsonConvert.convert<String>(json['file_data']);
  if (fileData != null) {
    savePageAvatarEntity.fileData = fileData;
  }
  return savePageAvatarEntity;
}

Map<String, dynamic> $SavePageAvatarEntityToJson(SavePageAvatarEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['file_name'] = entity.fileName;
  data['file_data'] = entity.fileData;
  return data;
}
