import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/uploads_banner_response_entity.dart';

UploadsBannerResponseEntity $UploadsBannerResponseEntityFromJson(
    Map<String, dynamic> json) {
  final UploadsBannerResponseEntity uploadsBannerResponseEntity =
      UploadsBannerResponseEntity();
  final UploadsBannerResponseData? data =
      jsonConvert.convert<UploadsBannerResponseData>(json['data']);
  if (data != null) {
    uploadsBannerResponseEntity.data = data;
  }
  final UploadsBannerResponseBench? bench =
      jsonConvert.convert<UploadsBannerResponseBench>(json['bench']);
  if (bench != null) {
    uploadsBannerResponseEntity.bench = bench;
  }
  return uploadsBannerResponseEntity;
}

Map<String, dynamic> $UploadsBannerResponseEntityToJson(
    UploadsBannerResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

UploadsBannerResponseData $UploadsBannerResponseDataFromJson(
    Map<String, dynamic> json) {
  final UploadsBannerResponseData uploadsBannerResponseData =
      UploadsBannerResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    uploadsBannerResponseData.message = message;
  }
  final List<UploadsBannerResponseDataUploads>? uploads = jsonConvert
      .convertListNotNull<UploadsBannerResponseDataUploads>(json['uploads']);
  if (uploads != null) {
    uploadsBannerResponseData.uploads = uploads;
  }
  return uploadsBannerResponseData;
}

Map<String, dynamic> $UploadsBannerResponseDataToJson(
    UploadsBannerResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['uploads'] = entity.uploads?.map((v) => v.toJson()).toList();
  return data;
}

UploadsBannerResponseDataUploads $UploadsBannerResponseDataUploadsFromJson(
    Map<String, dynamic> json) {
  final UploadsBannerResponseDataUploads uploadsBannerResponseDataUploads =
      UploadsBannerResponseDataUploads();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    uploadsBannerResponseDataUploads.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    uploadsBannerResponseDataUploads.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    uploadsBannerResponseDataUploads.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    uploadsBannerResponseDataUploads.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    uploadsBannerResponseDataUploads.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    uploadsBannerResponseDataUploads.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    uploadsBannerResponseDataUploads.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    uploadsBannerResponseDataUploads.resizeUrl = resizeUrl;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    uploadsBannerResponseDataUploads.position = position;
  }
  return uploadsBannerResponseDataUploads;
}

Map<String, dynamic> $UploadsBannerResponseDataUploadsToJson(
    UploadsBannerResponseDataUploads entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  data['position'] = entity.position;
  return data;
}

UploadsBannerResponseBench $UploadsBannerResponseBenchFromJson(
    Map<String, dynamic> json) {
  final UploadsBannerResponseBench uploadsBannerResponseBench =
      UploadsBannerResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    uploadsBannerResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    uploadsBannerResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    uploadsBannerResponseBench.format = format;
  }
  return uploadsBannerResponseBench;
}

Map<String, dynamic> $UploadsBannerResponseBenchToJson(
    UploadsBannerResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
