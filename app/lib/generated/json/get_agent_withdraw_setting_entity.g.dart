import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_setting_entity.dart';

GetAgentWithdrawSettingEntity $GetAgentWithdrawSettingEntityFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawSettingEntity getAgentWithdrawSettingEntity =
      GetAgentWithdrawSettingEntity();
  final GetAgentWithdrawSettingData? data =
      jsonConvert.convert<GetAgentWithdrawSettingData>(json['data']);
  if (data != null) {
    getAgentWithdrawSettingEntity.data = data;
  }
  final GetAgentWithdrawSettingBench? bench =
      jsonConvert.convert<GetAgentWithdrawSettingBench>(json['bench']);
  if (bench != null) {
    getAgentWithdrawSettingEntity.bench = bench;
  }
  return getAgentWithdrawSettingEntity;
}

Map<String, dynamic> $GetAgentWithdrawSettingEntityToJson(
    GetAgentWithdrawSettingEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAgentWithdrawSettingData $GetAgentWithdrawSettingDataFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawSettingData getAgentWithdrawSettingData =
      GetAgentWithdrawSettingData();
  final List<GetAgentWithdrawSettingDataRecord>? record = jsonConvert
      .convertListNotNull<GetAgentWithdrawSettingDataRecord>(json['record']);
  if (record != null) {
    getAgentWithdrawSettingData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getAgentWithdrawSettingData.cache = cache;
  }
  return getAgentWithdrawSettingData;
}

Map<String, dynamic> $GetAgentWithdrawSettingDataToJson(
    GetAgentWithdrawSettingData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetAgentWithdrawSettingDataRecord $GetAgentWithdrawSettingDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawSettingDataRecord getAgentWithdrawSettingDataRecord =
      GetAgentWithdrawSettingDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawSettingDataRecord.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getAgentWithdrawSettingDataRecord.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    getAgentWithdrawSettingDataRecord.key = key;
  }
  final GetAgentWithdrawSettingDataRecordValue? value = jsonConvert
      .convert<GetAgentWithdrawSettingDataRecordValue>(json['value']);
  if (value != null) {
    getAgentWithdrawSettingDataRecord.value = value;
  }
  final bool? serialized = jsonConvert.convert<bool>(json['serialized']);
  if (serialized != null) {
    getAgentWithdrawSettingDataRecord.serialized = serialized;
  }
  return getAgentWithdrawSettingDataRecord;
}

Map<String, dynamic> $GetAgentWithdrawSettingDataRecordToJson(
    GetAgentWithdrawSettingDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value?.toJson();
  data['serialized'] = entity.serialized;
  return data;
}

GetAgentWithdrawSettingDataRecordValue
    $GetAgentWithdrawSettingDataRecordValueFromJson(Map<String, dynamic> json) {
  final GetAgentWithdrawSettingDataRecordValue
      getAgentWithdrawSettingDataRecordValue =
      GetAgentWithdrawSettingDataRecordValue();
  final GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal?
      minmaxWithdrawal = jsonConvert
          .convert<GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal>(
              json['minmax_withdrawal']);
  if (minmaxWithdrawal != null) {
    getAgentWithdrawSettingDataRecordValue.minmaxWithdrawal = minmaxWithdrawal;
  }
  final int? receiveWithinHr =
      jsonConvert.convert<int>(json['receive_within_hr']);
  if (receiveWithinHr != null) {
    getAgentWithdrawSettingDataRecordValue.receiveWithinHr = receiveWithinHr;
  }
  final int? coinsToThbRate =
      jsonConvert.convert<int>(json['coins_to_thb_rate']);
  if (coinsToThbRate != null) {
    getAgentWithdrawSettingDataRecordValue.coinsToThbRate = coinsToThbRate;
  }
  final int? withdrawalFee = jsonConvert.convert<int>(json['withdrawal_fee']);
  if (withdrawalFee != null) {
    getAgentWithdrawSettingDataRecordValue.withdrawalFee = withdrawalFee;
  }
  return getAgentWithdrawSettingDataRecordValue;
}

Map<String, dynamic> $GetAgentWithdrawSettingDataRecordValueToJson(
    GetAgentWithdrawSettingDataRecordValue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['minmax_withdrawal'] = entity.minmaxWithdrawal?.toJson();
  data['receive_within_hr'] = entity.receiveWithinHr;
  data['coins_to_thb_rate'] = entity.coinsToThbRate;
  data['withdrawal_fee'] = entity.withdrawalFee;
  return data;
}

GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal
    $GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawalFromJson(
        Map<String, dynamic> json) {
  final GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal
      getAgentWithdrawSettingDataRecordValueMinmaxWithdrawal =
      GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal();
  final int? minimumValue = jsonConvert.convert<int>(json['minimum_value']);
  if (minimumValue != null) {
    getAgentWithdrawSettingDataRecordValueMinmaxWithdrawal.minimumValue =
        minimumValue;
  }
  final int? maximumValue = jsonConvert.convert<int>(json['maximum_value']);
  if (maximumValue != null) {
    getAgentWithdrawSettingDataRecordValueMinmaxWithdrawal.maximumValue =
        maximumValue;
  }
  return getAgentWithdrawSettingDataRecordValueMinmaxWithdrawal;
}

Map<String, dynamic>
    $GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawalToJson(
        GetAgentWithdrawSettingDataRecordValueMinmaxWithdrawal entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['minimum_value'] = entity.minimumValue;
  data['maximum_value'] = entity.maximumValue;
  return data;
}

GetAgentWithdrawSettingBench $GetAgentWithdrawSettingBenchFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawSettingBench getAgentWithdrawSettingBench =
      GetAgentWithdrawSettingBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAgentWithdrawSettingBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAgentWithdrawSettingBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAgentWithdrawSettingBench.format = format;
  }
  return getAgentWithdrawSettingBench;
}

Map<String, dynamic> $GetAgentWithdrawSettingBenchToJson(
    GetAgentWithdrawSettingBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
