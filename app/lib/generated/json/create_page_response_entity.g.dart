import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/create_page_response_entity.dart';

CreatePageResponseEntity $CreatePageResponseEntityFromJson(Map<String, dynamic> json) {
	final CreatePageResponseEntity createPageResponseEntity = CreatePageResponseEntity();
	final CreatePageResponseData? data = jsonConvert.convert<CreatePageResponseData>(json['data']);
	if (data != null) {
		createPageResponseEntity.data = data;
	}
	return createPageResponseEntity;
}

Map<String, dynamic> $CreatePageResponseEntityToJson(CreatePageResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	return data;
}

CreatePageResponseData $CreatePageResponseDataFromJson(Map<String, dynamic> json) {
	final CreatePageResponseData createPageResponseData = CreatePageResponseData();
	final String? displayName = jsonConvert.convert<String>(json['display_name']);
	if (displayName != null) {
		createPageResponseData.displayName = displayName;
	}
	final String? category = jsonConvert.convert<String>(json['category']);
	if (category != null) {
		createPageResponseData.category = category;
	}
	final int? userId = jsonConvert.convert<int>(json['user_id']);
	if (userId != null) {
		createPageResponseData.userId = userId;
	}
	final int? groupId = jsonConvert.convert<int>(json['group_id']);
	if (groupId != null) {
		createPageResponseData.groupId = groupId;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		createPageResponseData.storeId = storeId;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		createPageResponseData.type = type;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		createPageResponseData.id = id;
	}
	return createPageResponseData;
}

Map<String, dynamic> $CreatePageResponseDataToJson(CreatePageResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['display_name'] = entity.displayName;
	data['category'] = entity.category;
	data['user_id'] = entity.userId;
	data['group_id'] = entity.groupId;
	data['store_id'] = entity.storeId;
	data['type'] = entity.type;
	data['id'] = entity.id;
	return data;
}