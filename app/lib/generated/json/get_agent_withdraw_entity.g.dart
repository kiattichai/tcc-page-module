import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/agent_withdraw_module/model/get_agent_withdraw_entity.dart';

GetAgentWithdrawEntity $GetAgentWithdrawEntityFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawEntity getAgentWithdrawEntity =
      GetAgentWithdrawEntity();
  final GetAgentWithdrawData? data =
      jsonConvert.convert<GetAgentWithdrawData>(json['data']);
  if (data != null) {
    getAgentWithdrawEntity.data = data;
  }
  final GetAgentWithdrawBench? bench =
      jsonConvert.convert<GetAgentWithdrawBench>(json['bench']);
  if (bench != null) {
    getAgentWithdrawEntity.bench = bench;
  }
  return getAgentWithdrawEntity;
}

Map<String, dynamic> $GetAgentWithdrawEntityToJson(
    GetAgentWithdrawEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAgentWithdrawData $GetAgentWithdrawDataFromJson(Map<String, dynamic> json) {
  final GetAgentWithdrawData getAgentWithdrawData = GetAgentWithdrawData();
  final GetAgentWithdrawDataPagination? pagination =
      jsonConvert.convert<GetAgentWithdrawDataPagination>(json['pagination']);
  if (pagination != null) {
    getAgentWithdrawData.pagination = pagination;
  }
  final List<GetAgentWithdrawDataRecord>? record = jsonConvert
      .convertListNotNull<GetAgentWithdrawDataRecord>(json['record']);
  if (record != null) {
    getAgentWithdrawData.record = record;
  }
  return getAgentWithdrawData;
}

Map<String, dynamic> $GetAgentWithdrawDataToJson(GetAgentWithdrawData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetAgentWithdrawDataPagination $GetAgentWithdrawDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawDataPagination getAgentWithdrawDataPagination =
      GetAgentWithdrawDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getAgentWithdrawDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getAgentWithdrawDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getAgentWithdrawDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getAgentWithdrawDataPagination.total = total;
  }
  return getAgentWithdrawDataPagination;
}

Map<String, dynamic> $GetAgentWithdrawDataPaginationToJson(
    GetAgentWithdrawDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetAgentWithdrawDataRecord $GetAgentWithdrawDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecord getAgentWithdrawDataRecord =
      GetAgentWithdrawDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawDataRecord.id = id;
  }
  final String? transactionNo =
      jsonConvert.convert<String>(json['transaction_no']);
  if (transactionNo != null) {
    getAgentWithdrawDataRecord.transactionNo = transactionNo;
  }
  final GetAgentWithdrawDataRecordType? type =
      jsonConvert.convert<GetAgentWithdrawDataRecordType>(json['type']);
  if (type != null) {
    getAgentWithdrawDataRecord.type = type;
  }
  final String? sof = jsonConvert.convert<String>(json['sof']);
  if (sof != null) {
    getAgentWithdrawDataRecord.sof = sof;
  }
  final int? amount = jsonConvert.convert<int>(json['amount']);
  if (amount != null) {
    getAgentWithdrawDataRecord.amount = amount;
  }
  final int? amountWithServiceFee =
      jsonConvert.convert<int>(json['amount_with_service_fee']);
  if (amountWithServiceFee != null) {
    getAgentWithdrawDataRecord.amountWithServiceFee = amountWithServiceFee;
  }
  final GetAgentWithdrawDataRecordTransferStatus? transferStatus =
      jsonConvert.convert<GetAgentWithdrawDataRecordTransferStatus>(
          json['transfer_status']);
  if (transferStatus != null) {
    getAgentWithdrawDataRecord.transferStatus = transferStatus;
  }
  final GetAgentWithdrawDataRecordTransferAt? transferAt = jsonConvert
      .convert<GetAgentWithdrawDataRecordTransferAt>(json['transfer_at']);
  if (transferAt != null) {
    getAgentWithdrawDataRecord.transferAt = transferAt;
  }
  final GetAgentWithdrawDataRecordStatus? status =
      jsonConvert.convert<GetAgentWithdrawDataRecordStatus>(json['status']);
  if (status != null) {
    getAgentWithdrawDataRecord.status = status;
  }
  final GetAgentWithdrawDataRecordUser? user =
      jsonConvert.convert<GetAgentWithdrawDataRecordUser>(json['user']);
  if (user != null) {
    getAgentWithdrawDataRecord.user = user;
  }
  final dynamic? staff = jsonConvert.convert<dynamic>(json['staff']);
  if (staff != null) {
    getAgentWithdrawDataRecord.staff = staff;
  }
  final dynamic? storeBank = jsonConvert.convert<dynamic>(json['store_bank']);
  if (storeBank != null) {
    getAgentWithdrawDataRecord.storeBank = storeBank;
  }
  final dynamic? image = jsonConvert.convert<dynamic>(json['image']);
  if (image != null) {
    getAgentWithdrawDataRecord.image = image;
  }
  final GetAgentWithdrawDataRecordCreatedAt? createdAt = jsonConvert
      .convert<GetAgentWithdrawDataRecordCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getAgentWithdrawDataRecord.createdAt = createdAt;
  }
  final GetAgentWithdrawDataRecordUpdatedAt? updatedAt = jsonConvert
      .convert<GetAgentWithdrawDataRecordUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getAgentWithdrawDataRecord.updatedAt = updatedAt;
  }
  return getAgentWithdrawDataRecord;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordToJson(
    GetAgentWithdrawDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['transaction_no'] = entity.transactionNo;
  data['type'] = entity.type?.toJson();
  data['sof'] = entity.sof;
  data['amount'] = entity.amount;
  data['amount_with_service_fee'] = entity.amountWithServiceFee;
  data['transfer_status'] = entity.transferStatus?.toJson();
  data['transfer_at'] = entity.transferAt?.toJson();
  data['status'] = entity.status?.toJson();
  data['user'] = entity.user?.toJson();
  data['staff'] = entity.staff;
  data['store_bank'] = entity.storeBank;
  data['image'] = entity.image;
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

GetAgentWithdrawDataRecordType $GetAgentWithdrawDataRecordTypeFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordType getAgentWithdrawDataRecordType =
      GetAgentWithdrawDataRecordType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawDataRecordType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getAgentWithdrawDataRecordType.text = text;
  }
  return getAgentWithdrawDataRecordType;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordTypeToJson(
    GetAgentWithdrawDataRecordType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetAgentWithdrawDataRecordTransferStatus
    $GetAgentWithdrawDataRecordTransferStatusFromJson(
        Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordTransferStatus
      getAgentWithdrawDataRecordTransferStatus =
      GetAgentWithdrawDataRecordTransferStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawDataRecordTransferStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getAgentWithdrawDataRecordTransferStatus.text = text;
  }
  return getAgentWithdrawDataRecordTransferStatus;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordTransferStatusToJson(
    GetAgentWithdrawDataRecordTransferStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetAgentWithdrawDataRecordTransferAt
    $GetAgentWithdrawDataRecordTransferAtFromJson(Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordTransferAt
      getAgentWithdrawDataRecordTransferAt =
      GetAgentWithdrawDataRecordTransferAt();
  return getAgentWithdrawDataRecordTransferAt;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordTransferAtToJson(
    GetAgentWithdrawDataRecordTransferAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetAgentWithdrawDataRecordStatus $GetAgentWithdrawDataRecordStatusFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordStatus getAgentWithdrawDataRecordStatus =
      GetAgentWithdrawDataRecordStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawDataRecordStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getAgentWithdrawDataRecordStatus.text = text;
  }
  return getAgentWithdrawDataRecordStatus;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordStatusToJson(
    GetAgentWithdrawDataRecordStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetAgentWithdrawDataRecordUser $GetAgentWithdrawDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordUser getAgentWithdrawDataRecordUser =
      GetAgentWithdrawDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentWithdrawDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getAgentWithdrawDataRecordUser.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getAgentWithdrawDataRecordUser.countryCode = countryCode;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getAgentWithdrawDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getAgentWithdrawDataRecordUser.lastName = lastName;
  }
  return getAgentWithdrawDataRecordUser;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordUserToJson(
    GetAgentWithdrawDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  return data;
}

GetAgentWithdrawDataRecordCreatedAt
    $GetAgentWithdrawDataRecordCreatedAtFromJson(Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordCreatedAt
      getAgentWithdrawDataRecordCreatedAt =
      GetAgentWithdrawDataRecordCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getAgentWithdrawDataRecordCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getAgentWithdrawDataRecordCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getAgentWithdrawDataRecordCreatedAt.time = time;
  }
  return getAgentWithdrawDataRecordCreatedAt;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordCreatedAtToJson(
    GetAgentWithdrawDataRecordCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetAgentWithdrawDataRecordUpdatedAt
    $GetAgentWithdrawDataRecordUpdatedAtFromJson(Map<String, dynamic> json) {
  final GetAgentWithdrawDataRecordUpdatedAt
      getAgentWithdrawDataRecordUpdatedAt =
      GetAgentWithdrawDataRecordUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getAgentWithdrawDataRecordUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getAgentWithdrawDataRecordUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getAgentWithdrawDataRecordUpdatedAt.time = time;
  }
  return getAgentWithdrawDataRecordUpdatedAt;
}

Map<String, dynamic> $GetAgentWithdrawDataRecordUpdatedAtToJson(
    GetAgentWithdrawDataRecordUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetAgentWithdrawBench $GetAgentWithdrawBenchFromJson(
    Map<String, dynamic> json) {
  final GetAgentWithdrawBench getAgentWithdrawBench = GetAgentWithdrawBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAgentWithdrawBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAgentWithdrawBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAgentWithdrawBench.format = format;
  }
  return getAgentWithdrawBench;
}

Map<String, dynamic> $GetAgentWithdrawBenchToJson(
    GetAgentWithdrawBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
