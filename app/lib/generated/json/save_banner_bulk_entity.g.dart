import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_banner_bulk_entity.dart';

SaveBannerBulkEntity $SaveBannerBulkEntityFromJson(Map<String, dynamic> json) {
  final SaveBannerBulkEntity saveBannerBulkEntity = SaveBannerBulkEntity();
  final String? fileableId = jsonConvert.convert<String>(json['fileable_id']);
  if (fileableId != null) {
    saveBannerBulkEntity.fileableId = fileableId;
  }
  final String? fileableType =
      jsonConvert.convert<String>(json['fileable_type']);
  if (fileableType != null) {
    saveBannerBulkEntity.fileableType = fileableType;
  }
  final List<SaveBannerBulkItems>? items =
      jsonConvert.convertListNotNull<SaveBannerBulkItems>(json['items']);
  if (items != null) {
    saveBannerBulkEntity.items = items;
  }
  return saveBannerBulkEntity;
}

Map<String, dynamic> $SaveBannerBulkEntityToJson(SaveBannerBulkEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['fileable_id'] = entity.fileableId;
  data['fileable_type'] = entity.fileableType;
  data['items'] = entity.items?.map((v) => v.toJson()).toList();
  return data;
}

SaveBannerBulkItems $SaveBannerBulkItemsFromJson(Map<String, dynamic> json) {
  final SaveBannerBulkItems saveBannerBulkItems = SaveBannerBulkItems();
  final String? attachmentId =
      jsonConvert.convert<String>(json['attachment_id']);
  if (attachmentId != null) {
    saveBannerBulkItems.attachmentId = attachmentId;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    saveBannerBulkItems.position = position;
  }
  return saveBannerBulkItems;
}

Map<String, dynamic> $SaveBannerBulkItemsToJson(SaveBannerBulkItems entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['attachment_id'] = entity.attachmentId;
  data['position'] = entity.position;
  return data;
}
