import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_calculate_ticket_entity.dart';

GetCalculateTicketEntity $GetCalculateTicketEntityFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketEntity getCalculateTicketEntity =
      GetCalculateTicketEntity();
  final GetCalculateTicketData? data =
      jsonConvert.convert<GetCalculateTicketData>(json['data']);
  if (data != null) {
    getCalculateTicketEntity.data = data;
  }
  final GetCalculateTicketBench? bench =
      jsonConvert.convert<GetCalculateTicketBench>(json['bench']);
  if (bench != null) {
    getCalculateTicketEntity.bench = bench;
  }
  return getCalculateTicketEntity;
}

Map<String, dynamic> $GetCalculateTicketEntityToJson(
    GetCalculateTicketEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetCalculateTicketData $GetCalculateTicketDataFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketData getCalculateTicketData =
      GetCalculateTicketData();
  final GetCalculateTicketDataParameters? parameters =
      jsonConvert.convert<GetCalculateTicketDataParameters>(json['parameters']);
  if (parameters != null) {
    getCalculateTicketData.parameters = parameters;
  }
  final GetCalculateTicketDataFees? fees =
      jsonConvert.convert<GetCalculateTicketDataFees>(json['fees']);
  if (fees != null) {
    getCalculateTicketData.fees = fees;
  }
  final GetCalculateTicketDataAmounts? amounts =
      jsonConvert.convert<GetCalculateTicketDataAmounts>(json['amounts']);
  if (amounts != null) {
    getCalculateTicketData.amounts = amounts;
  }
  return getCalculateTicketData;
}

Map<String, dynamic> $GetCalculateTicketDataToJson(
    GetCalculateTicketData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['parameters'] = entity.parameters?.toJson();
  data['fees'] = entity.fees?.toJson();
  data['amounts'] = entity.amounts?.toJson();
  return data;
}

GetCalculateTicketDataParameters $GetCalculateTicketDataParametersFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketDataParameters getCalculateTicketDataParameters =
      GetCalculateTicketDataParameters();
  final String? serviceFeePayer =
      jsonConvert.convert<String>(json['serviceFeePayer']);
  if (serviceFeePayer != null) {
    getCalculateTicketDataParameters.serviceFeePayer = serviceFeePayer;
  }
  final String? paymentFeePayer =
      jsonConvert.convert<String>(json['paymentFeePayer']);
  if (paymentFeePayer != null) {
    getCalculateTicketDataParameters.paymentFeePayer = paymentFeePayer;
  }
  final double? ticketPrice = jsonConvert.convert<double>(json['ticketPrice']);
  if (ticketPrice != null) {
    getCalculateTicketDataParameters.ticketPrice = ticketPrice;
  }
  final int? ticketAmount = jsonConvert.convert<int>(json['ticketAmount']);
  if (ticketAmount != null) {
    getCalculateTicketDataParameters.ticketAmount = ticketAmount;
  }
  return getCalculateTicketDataParameters;
}

Map<String, dynamic> $GetCalculateTicketDataParametersToJson(
    GetCalculateTicketDataParameters entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['serviceFeePayer'] = entity.serviceFeePayer;
  data['paymentFeePayer'] = entity.paymentFeePayer;
  data['ticketPrice'] = entity.ticketPrice;
  data['ticketAmount'] = entity.ticketAmount;
  return data;
}

GetCalculateTicketDataFees $GetCalculateTicketDataFeesFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketDataFees getCalculateTicketDataFees =
      GetCalculateTicketDataFees();
  final String? serviceFee = jsonConvert.convert<String>(json['serviceFee']);
  if (serviceFee != null) {
    getCalculateTicketDataFees.serviceFee = serviceFee;
  }
  final String? paymentFee = jsonConvert.convert<String>(json['paymentFee']);
  if (paymentFee != null) {
    getCalculateTicketDataFees.paymentFee = paymentFee;
  }
  return getCalculateTicketDataFees;
}

Map<String, dynamic> $GetCalculateTicketDataFeesToJson(
    GetCalculateTicketDataFees entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['serviceFee'] = entity.serviceFee;
  data['paymentFee'] = entity.paymentFee;
  return data;
}

GetCalculateTicketDataAmounts $GetCalculateTicketDataAmountsFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketDataAmounts getCalculateTicketDataAmounts =
      GetCalculateTicketDataAmounts();
  final String? customerPay = jsonConvert.convert<String>(json['customerPay']);
  if (customerPay != null) {
    getCalculateTicketDataAmounts.customerPay = customerPay;
  }
  final String? organizerReceive =
      jsonConvert.convert<String>(json['organizerReceive']);
  if (organizerReceive != null) {
    getCalculateTicketDataAmounts.organizerReceive = organizerReceive;
  }
  return getCalculateTicketDataAmounts;
}

Map<String, dynamic> $GetCalculateTicketDataAmountsToJson(
    GetCalculateTicketDataAmounts entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['customerPay'] = entity.customerPay;
  data['organizerReceive'] = entity.organizerReceive;
  return data;
}

GetCalculateTicketBench $GetCalculateTicketBenchFromJson(
    Map<String, dynamic> json) {
  final GetCalculateTicketBench getCalculateTicketBench =
      GetCalculateTicketBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getCalculateTicketBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getCalculateTicketBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getCalculateTicketBench.format = format;
  }
  return getCalculateTicketBench;
}

Map<String, dynamic> $GetCalculateTicketBenchToJson(
    GetCalculateTicketBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
