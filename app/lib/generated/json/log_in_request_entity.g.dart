import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/log_in_module/model/log_in_request_entity.dart';

LogInRequestEntity $LogInRequestEntityFromJson(Map<String, dynamic> json) {
	final LogInRequestEntity logInRequestEntity = LogInRequestEntity();
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		logInRequestEntity.username = username;
	}
	final String? password = jsonConvert.convert<String>(json['password']);
	if (password != null) {
		logInRequestEntity.password = password;
	}
	final String? deviceId = jsonConvert.convert<String>(json['device_id']);
	if (deviceId != null) {
		logInRequestEntity.deviceId = deviceId;
	}
	final String? notificationToken = jsonConvert.convert<String>(json['notification_token']);
	if (notificationToken != null) {
		logInRequestEntity.notificationToken = notificationToken;
	}
	final String? deviceType = jsonConvert.convert<String>(json['device_type']);
	if (deviceType != null) {
		logInRequestEntity.deviceType = deviceType;
	}
	final String? ipAddress = jsonConvert.convert<String>(json['ip_address']);
	if (ipAddress != null) {
		logInRequestEntity.ipAddress = ipAddress;
	}
	final String? countryCode = jsonConvert.convert<String>(json['country_code']);
	if (countryCode != null) {
		logInRequestEntity.countryCode = countryCode;
	}
	return logInRequestEntity;
}

Map<String, dynamic> $LogInRequestEntityToJson(LogInRequestEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['username'] = entity.username;
	data['password'] = entity.password;
	data['device_id'] = entity.deviceId;
	data['notification_token'] = entity.notificationToken;
	data['device_type'] = entity.deviceType;
	data['ip_address'] = entity.ipAddress;
	data['country_code'] = entity.countryCode;
	return data;
}