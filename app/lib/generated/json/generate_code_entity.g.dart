import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/generate_code_entity.dart';

GenerateCodeEntity $GenerateCodeEntityFromJson(Map<String, dynamic> json) {
  final GenerateCodeEntity generateCodeEntity = GenerateCodeEntity();
  final GenerateCodeData? data =
      jsonConvert.convert<GenerateCodeData>(json['data']);
  if (data != null) {
    generateCodeEntity.data = data;
  }
  final GenerateCodeBench? bench =
      jsonConvert.convert<GenerateCodeBench>(json['bench']);
  if (bench != null) {
    generateCodeEntity.bench = bench;
  }
  return generateCodeEntity;
}

Map<String, dynamic> $GenerateCodeEntityToJson(GenerateCodeEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GenerateCodeData $GenerateCodeDataFromJson(Map<String, dynamic> json) {
  final GenerateCodeData generateCodeData = GenerateCodeData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    generateCodeData.message = message;
  }
  final List<GenerateCodeDataPromotionCode>? promotionCode =
      jsonConvert.convertListNotNull<GenerateCodeDataPromotionCode>(
          json['promotion_code']);
  if (promotionCode != null) {
    generateCodeData.promotionCode = promotionCode;
  }
  return generateCodeData;
}

Map<String, dynamic> $GenerateCodeDataToJson(GenerateCodeData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['promotion_code'] =
      entity.promotionCode?.map((v) => v.toJson()).toList();
  return data;
}

GenerateCodeDataPromotionCode $GenerateCodeDataPromotionCodeFromJson(
    Map<String, dynamic> json) {
  final GenerateCodeDataPromotionCode generateCodeDataPromotionCode =
      GenerateCodeDataPromotionCode();
  final int? promotionId = jsonConvert.convert<int>(json['promotion_id']);
  if (promotionId != null) {
    generateCodeDataPromotionCode.promotionId = promotionId;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    generateCodeDataPromotionCode.code = code;
  }
  final int? used = jsonConvert.convert<int>(json['used']);
  if (used != null) {
    generateCodeDataPromotionCode.used = used;
  }
  final int? status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    generateCodeDataPromotionCode.status = status;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    generateCodeDataPromotionCode.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    generateCodeDataPromotionCode.updatedAt = updatedAt;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    generateCodeDataPromotionCode.id = id;
  }
  return generateCodeDataPromotionCode;
}

Map<String, dynamic> $GenerateCodeDataPromotionCodeToJson(
    GenerateCodeDataPromotionCode entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['promotion_id'] = entity.promotionId;
  data['code'] = entity.code;
  data['used'] = entity.used;
  data['status'] = entity.status;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['id'] = entity.id;
  return data;
}

GenerateCodeBench $GenerateCodeBenchFromJson(Map<String, dynamic> json) {
  final GenerateCodeBench generateCodeBench = GenerateCodeBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    generateCodeBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    generateCodeBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    generateCodeBench.format = format;
  }
  return generateCodeBench;
}

Map<String, dynamic> $GenerateCodeBenchToJson(GenerateCodeBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
