import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_venue_response_entity.dart';

SaveVenueResponseEntity $SaveVenueResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SaveVenueResponseEntity saveVenueResponseEntity =
      SaveVenueResponseEntity();
  final SaveVenueResponseData? data =
      jsonConvert.convert<SaveVenueResponseData>(json['data']);
  if (data != null) {
    saveVenueResponseEntity.data = data;
  }
  final SaveVenueResponseBench? bench =
      jsonConvert.convert<SaveVenueResponseBench>(json['bench']);
  if (bench != null) {
    saveVenueResponseEntity.bench = bench;
  }
  return saveVenueResponseEntity;
}

Map<String, dynamic> $SaveVenueResponseEntityToJson(
    SaveVenueResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SaveVenueResponseData $SaveVenueResponseDataFromJson(
    Map<String, dynamic> json) {
  final SaveVenueResponseData saveVenueResponseData = SaveVenueResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    saveVenueResponseData.message = message;
  }
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    saveVenueResponseData.id = id;
  }
  return saveVenueResponseData;
}

Map<String, dynamic> $SaveVenueResponseDataToJson(
    SaveVenueResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  return data;
}

SaveVenueResponseBench $SaveVenueResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SaveVenueResponseBench saveVenueResponseBench =
      SaveVenueResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    saveVenueResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    saveVenueResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    saveVenueResponseBench.format = format;
  }
  return saveVenueResponseBench;
}

Map<String, dynamic> $SaveVenueResponseBenchToJson(
    SaveVenueResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
