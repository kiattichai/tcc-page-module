import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_qr_response_entity.dart';

TicketQrResponseEntity $TicketQrResponseEntityFromJson(Map<String, dynamic> json) {
	final TicketQrResponseEntity ticketQrResponseEntity = TicketQrResponseEntity();
	final TicketQrResponseData? data = jsonConvert.convert<TicketQrResponseData>(json['data']);
	if (data != null) {
		ticketQrResponseEntity.data = data;
	}
	final TicketQrResponseBench? bench = jsonConvert.convert<TicketQrResponseBench>(json['bench']);
	if (bench != null) {
		ticketQrResponseEntity.bench = bench;
	}
	return ticketQrResponseEntity;
}

Map<String, dynamic> $TicketQrResponseEntityToJson(TicketQrResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

TicketQrResponseData $TicketQrResponseDataFromJson(Map<String, dynamic> json) {
	final TicketQrResponseData ticketQrResponseData = TicketQrResponseData();
	final String? ticketNo = jsonConvert.convert<String>(json['ticket_no']);
	if (ticketNo != null) {
		ticketQrResponseData.ticketNo = ticketNo;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		ticketQrResponseData.mime = mime;
	}
	final String? imageData = jsonConvert.convert<String>(json['image_data']);
	if (imageData != null) {
		ticketQrResponseData.imageData = imageData;
	}
	final int? expired = jsonConvert.convert<int>(json['expired']);
	if (expired != null) {
		ticketQrResponseData.expired = expired;
	}
	return ticketQrResponseData;
}

Map<String, dynamic> $TicketQrResponseDataToJson(TicketQrResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['ticket_no'] = entity.ticketNo;
	data['mime'] = entity.mime;
	data['image_data'] = entity.imageData;
	data['expired'] = entity.expired;
	return data;
}

TicketQrResponseBench $TicketQrResponseBenchFromJson(Map<String, dynamic> json) {
	final TicketQrResponseBench ticketQrResponseBench = TicketQrResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		ticketQrResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		ticketQrResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		ticketQrResponseBench.format = format;
	}
	return ticketQrResponseBench;
}

Map<String, dynamic> $TicketQrResponseBenchToJson(TicketQrResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}