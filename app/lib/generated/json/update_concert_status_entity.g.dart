import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/update_concert_status_entity.dart';

UpdateConcertStatusEntity $UpdateConcertStatusEntityFromJson(
    Map<String, dynamic> json) {
  final UpdateConcertStatusEntity updateConcertStatusEntity =
      UpdateConcertStatusEntity();
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    updateConcertStatusEntity.status = status;
  }
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    updateConcertStatusEntity.storeId = storeId;
  }
  return updateConcertStatusEntity;
}

Map<String, dynamic> $UpdateConcertStatusEntityToJson(
    UpdateConcertStatusEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  data['store_id'] = entity.storeId;
  return data;
}
