import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_artist_entity.dart';

GetArtistEntity $GetArtistEntityFromJson(Map<String, dynamic> json) {
  final GetArtistEntity getArtistEntity = GetArtistEntity();
  final GetArtistData? data = jsonConvert.convert<GetArtistData>(json['data']);
  if (data != null) {
    getArtistEntity.data = data;
  }
  final GetArtistBench? bench =
      jsonConvert.convert<GetArtistBench>(json['bench']);
  if (bench != null) {
    getArtistEntity.bench = bench;
  }
  return getArtistEntity;
}

Map<String, dynamic> $GetArtistEntityToJson(GetArtistEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetArtistData $GetArtistDataFromJson(Map<String, dynamic> json) {
  final GetArtistData getArtistData = GetArtistData();
  final GetArtistDataPagination? pagination =
      jsonConvert.convert<GetArtistDataPagination>(json['pagination']);
  if (pagination != null) {
    getArtistData.pagination = pagination;
  }
  final List<GetArtistDataRecord>? record =
      jsonConvert.convertListNotNull<GetArtistDataRecord>(json['record']);
  if (record != null) {
    getArtistData.record = record;
  }
  return getArtistData;
}

Map<String, dynamic> $GetArtistDataToJson(GetArtistData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetArtistDataPagination $GetArtistDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetArtistDataPagination getArtistDataPagination =
      GetArtistDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getArtistDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getArtistDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getArtistDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getArtistDataPagination.total = total;
  }
  return getArtistDataPagination;
}

Map<String, dynamic> $GetArtistDataPaginationToJson(
    GetArtistDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetArtistDataRecord $GetArtistDataRecordFromJson(Map<String, dynamic> json) {
  final GetArtistDataRecord getArtistDataRecord = GetArtistDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getArtistDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getArtistDataRecord.name = name;
  }
  final GetArtistDataRecordImage? image =
      jsonConvert.convert<GetArtistDataRecordImage>(json['image']);
  if (image != null) {
    getArtistDataRecord.image = image;
  }
  return getArtistDataRecord;
}

Map<String, dynamic> $GetArtistDataRecordToJson(GetArtistDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['image'] = entity.image?.toJson();
  return data;
}

GetArtistDataRecordImage $GetArtistDataRecordImageFromJson(
    Map<String, dynamic> json) {
  final GetArtistDataRecordImage getArtistDataRecordImage =
      GetArtistDataRecordImage();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getArtistDataRecordImage.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getArtistDataRecordImage.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getArtistDataRecordImage.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getArtistDataRecordImage.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getArtistDataRecordImage.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getArtistDataRecordImage.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getArtistDataRecordImage.url = url;
  }
  return getArtistDataRecordImage;
}

Map<String, dynamic> $GetArtistDataRecordImageToJson(
    GetArtistDataRecordImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetArtistBench $GetArtistBenchFromJson(Map<String, dynamic> json) {
  final GetArtistBench getArtistBench = GetArtistBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getArtistBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getArtistBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getArtistBench.format = format;
  }
  return getArtistBench;
}

Map<String, dynamic> $GetArtistBenchToJson(GetArtistBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
