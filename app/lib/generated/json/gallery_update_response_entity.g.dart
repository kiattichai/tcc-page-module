import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/gallery_update_response_entity.dart';

GalleryUpdateResponseEntity $GalleryUpdateResponseEntityFromJson(Map<String, dynamic> json) {
	final GalleryUpdateResponseEntity galleryUpdateResponseEntity = GalleryUpdateResponseEntity();
	final GalleryUpdateResponseData? data = jsonConvert.convert<GalleryUpdateResponseData>(json['data']);
	if (data != null) {
		galleryUpdateResponseEntity.data = data;
	}
	final GalleryUpdateResponseBench? bench = jsonConvert.convert<GalleryUpdateResponseBench>(json['bench']);
	if (bench != null) {
		galleryUpdateResponseEntity.bench = bench;
	}
	return galleryUpdateResponseEntity;
}

Map<String, dynamic> $GalleryUpdateResponseEntityToJson(GalleryUpdateResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GalleryUpdateResponseData $GalleryUpdateResponseDataFromJson(Map<String, dynamic> json) {
	final GalleryUpdateResponseData galleryUpdateResponseData = GalleryUpdateResponseData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		galleryUpdateResponseData.message = message;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		galleryUpdateResponseData.id = id;
	}
	return galleryUpdateResponseData;
}

Map<String, dynamic> $GalleryUpdateResponseDataToJson(GalleryUpdateResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	data['id'] = entity.id;
	return data;
}

GalleryUpdateResponseBench $GalleryUpdateResponseBenchFromJson(Map<String, dynamic> json) {
	final GalleryUpdateResponseBench galleryUpdateResponseBench = GalleryUpdateResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		galleryUpdateResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		galleryUpdateResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		galleryUpdateResponseBench.format = format;
	}
	return galleryUpdateResponseBench;
}

Map<String, dynamic> $GalleryUpdateResponseBenchToJson(GalleryUpdateResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}