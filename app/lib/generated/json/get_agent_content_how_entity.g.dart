import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/concert_agent_module/model/get_agent_content_how_entity.dart';

GetAgentContentHowEntity $GetAgentContentHowEntityFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowEntity getAgentContentHowEntity =
      GetAgentContentHowEntity();
  final GetAgentContentHowData? data =
      jsonConvert.convert<GetAgentContentHowData>(json['data']);
  if (data != null) {
    getAgentContentHowEntity.data = data;
  }
  final GetAgentContentHowBench? bench =
      jsonConvert.convert<GetAgentContentHowBench>(json['bench']);
  if (bench != null) {
    getAgentContentHowEntity.bench = bench;
  }
  return getAgentContentHowEntity;
}

Map<String, dynamic> $GetAgentContentHowEntityToJson(
    GetAgentContentHowEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAgentContentHowData $GetAgentContentHowDataFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowData getAgentContentHowData =
      GetAgentContentHowData();
  final GetAgentContentHowDataPagination? pagination =
      jsonConvert.convert<GetAgentContentHowDataPagination>(json['pagination']);
  if (pagination != null) {
    getAgentContentHowData.pagination = pagination;
  }
  final List<GetAgentContentHowDataRecord>? record = jsonConvert
      .convertListNotNull<GetAgentContentHowDataRecord>(json['record']);
  if (record != null) {
    getAgentContentHowData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getAgentContentHowData.cache = cache;
  }
  return getAgentContentHowData;
}

Map<String, dynamic> $GetAgentContentHowDataToJson(
    GetAgentContentHowData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetAgentContentHowDataPagination $GetAgentContentHowDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowDataPagination getAgentContentHowDataPagination =
      GetAgentContentHowDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getAgentContentHowDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getAgentContentHowDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getAgentContentHowDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getAgentContentHowDataPagination.total = total;
  }
  return getAgentContentHowDataPagination;
}

Map<String, dynamic> $GetAgentContentHowDataPaginationToJson(
    GetAgentContentHowDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetAgentContentHowDataRecord $GetAgentContentHowDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowDataRecord getAgentContentHowDataRecord =
      GetAgentContentHowDataRecord();
  final List<GetAgentContentHowDataRecordFields>? fields = jsonConvert
      .convertListNotNull<GetAgentContentHowDataRecordFields>(json['fields']);
  if (fields != null) {
    getAgentContentHowDataRecord.fields = fields;
  }
  return getAgentContentHowDataRecord;
}

Map<String, dynamic> $GetAgentContentHowDataRecordToJson(
    GetAgentContentHowDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['fields'] = entity.fields?.map((v) => v.toJson()).toList();
  return data;
}

GetAgentContentHowDataRecordFields $GetAgentContentHowDataRecordFieldsFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowDataRecordFields getAgentContentHowDataRecordFields =
      GetAgentContentHowDataRecordFields();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentContentHowDataRecordFields.name = name;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getAgentContentHowDataRecordFields.lang = lang;
  }
  return getAgentContentHowDataRecordFields;
}

Map<String, dynamic> $GetAgentContentHowDataRecordFieldsToJson(
    GetAgentContentHowDataRecordFields entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['lang'] = entity.lang;
  return data;
}

GetAgentContentHowBench $GetAgentContentHowBenchFromJson(
    Map<String, dynamic> json) {
  final GetAgentContentHowBench getAgentContentHowBench =
      GetAgentContentHowBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAgentContentHowBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAgentContentHowBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAgentContentHowBench.format = format;
  }
  return getAgentContentHowBench;
}

Map<String, dynamic> $GetAgentContentHowBenchToJson(
    GetAgentContentHowBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
