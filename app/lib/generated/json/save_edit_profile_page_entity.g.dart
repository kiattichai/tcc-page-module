import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_edit_profile_page_entity.dart';

SaveEditProfilePageEntity $SaveEditProfilePageEntityFromJson(
    Map<String, dynamic> json) {
  final SaveEditProfilePageEntity saveEditProfilePageEntity =
      SaveEditProfilePageEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    saveEditProfilePageEntity.storeId = storeId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    saveEditProfilePageEntity.name = name;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    saveEditProfilePageEntity.slug = slug;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    saveEditProfilePageEntity.description = description;
  }
  final String? phone = jsonConvert.convert<String>(json['phone']);
  if (phone != null) {
    saveEditProfilePageEntity.phone = phone;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    saveEditProfilePageEntity.countryCode = countryCode;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    saveEditProfilePageEntity.email = email;
  }
  final int? parking = jsonConvert.convert<int>(json['parking']);
  if (parking != null) {
    saveEditProfilePageEntity.parking = parking;
  }
  final int? seatMin = jsonConvert.convert<int>(json['seat_min']);
  if (seatMin != null) {
    saveEditProfilePageEntity.seatMin = seatMin;
  }
  final dynamic? seatMax = jsonConvert.convert<dynamic>(json['seat_max']);
  if (seatMax != null) {
    saveEditProfilePageEntity.seatMax = seatMax;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    saveEditProfilePageEntity.type = type;
  }
  final List<SaveEditProfilePageAttributes>? attributes = jsonConvert
      .convertListNotNull<SaveEditProfilePageAttributes>(json['attributes']);
  if (attributes != null) {
    saveEditProfilePageEntity.attributes = attributes;
  }
  final List<dynamic>? phoneOption =
      jsonConvert.convertListNotNull<dynamic>(json['phone_option']);
  if (phoneOption != null) {
    saveEditProfilePageEntity.phoneOption = phoneOption;
  }
  final List<SaveEditProfilePageTimes>? times =
      jsonConvert.convertListNotNull<SaveEditProfilePageTimes>(json['times']);
  if (times != null) {
    saveEditProfilePageEntity.times = times;
  }
  return saveEditProfilePageEntity;
}

Map<String, dynamic> $SaveEditProfilePageEntityToJson(
    SaveEditProfilePageEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['name'] = entity.name;
  data['slug'] = entity.slug;
  data['description'] = entity.description;
  data['phone'] = entity.phone;
  data['country_code'] = entity.countryCode;
  data['email'] = entity.email;
  data['parking'] = entity.parking;
  data['seat_min'] = entity.seatMin;
  data['seat_max'] = entity.seatMax;
  data['type'] = entity.type;
  data['attributes'] = entity.attributes?.map((v) => v.toJson()).toList();
  data['phone_option'] = entity.phoneOption;
  data['times'] = entity.times?.map((v) => v.toJson()).toList();
  return data;
}

SaveEditProfilePageAttributes $SaveEditProfilePageAttributesFromJson(
    Map<String, dynamic> json) {
  final SaveEditProfilePageAttributes saveEditProfilePageAttributes =
      SaveEditProfilePageAttributes();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    saveEditProfilePageAttributes.id = id;
  }
  final int? valueId = jsonConvert.convert<int>(json['value_id']);
  if (valueId != null) {
    saveEditProfilePageAttributes.valueId = valueId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    saveEditProfilePageAttributes.name = name;
  }
  return saveEditProfilePageAttributes;
}

Map<String, dynamic> $SaveEditProfilePageAttributesToJson(
    SaveEditProfilePageAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['value_id'] = entity.valueId;
  data['name'] = entity.name;
  return data;
}

SaveEditProfilePageTimes $SaveEditProfilePageTimesFromJson(
    Map<String, dynamic> json) {
  final SaveEditProfilePageTimes saveEditProfilePageTimes =
      SaveEditProfilePageTimes();
  final int? day = jsonConvert.convert<int>(json['day']);
  if (day != null) {
    saveEditProfilePageTimes.day = day;
  }
  final String? open = jsonConvert.convert<String>(json['open']);
  if (open != null) {
    saveEditProfilePageTimes.open = open;
  }
  final String? close = jsonConvert.convert<String>(json['close']);
  if (close != null) {
    saveEditProfilePageTimes.close = close;
  }
  return saveEditProfilePageTimes;
}

Map<String, dynamic> $SaveEditProfilePageTimesToJson(
    SaveEditProfilePageTimes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['day'] = entity.day;
  data['open'] = entity.open;
  data['close'] = entity.close;
  return data;
}
