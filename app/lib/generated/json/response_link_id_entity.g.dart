import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/response_link_id_entity.dart';

ResponseLinkIdEntity $ResponseLinkIdEntityFromJson(Map<String, dynamic> json) {
	final ResponseLinkIdEntity responseLinkIdEntity = ResponseLinkIdEntity();
	final ResponseLinkIdData? data = jsonConvert.convert<ResponseLinkIdData>(json['data']);
	if (data != null) {
		responseLinkIdEntity.data = data;
	}
	final ResponseLinkIdBench? bench = jsonConvert.convert<ResponseLinkIdBench>(json['bench']);
	if (bench != null) {
		responseLinkIdEntity.bench = bench;
	}
	return responseLinkIdEntity;
}

Map<String, dynamic> $ResponseLinkIdEntityToJson(ResponseLinkIdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

ResponseLinkIdData $ResponseLinkIdDataFromJson(Map<String, dynamic> json) {
	final ResponseLinkIdData responseLinkIdData = ResponseLinkIdData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		responseLinkIdData.message = message;
	}
	final int? promotionId = jsonConvert.convert<int>(json['promotion_id']);
	if (promotionId != null) {
		responseLinkIdData.promotionId = promotionId;
	}
	return responseLinkIdData;
}

Map<String, dynamic> $ResponseLinkIdDataToJson(ResponseLinkIdData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	data['promotion_id'] = entity.promotionId;
	return data;
}

ResponseLinkIdBench $ResponseLinkIdBenchFromJson(Map<String, dynamic> json) {
	final ResponseLinkIdBench responseLinkIdBench = ResponseLinkIdBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		responseLinkIdBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		responseLinkIdBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		responseLinkIdBench.format = format;
	}
	return responseLinkIdBench;
}

Map<String, dynamic> $ResponseLinkIdBenchToJson(ResponseLinkIdBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}