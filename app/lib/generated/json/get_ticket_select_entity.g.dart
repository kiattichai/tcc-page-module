import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_ticket_select_entity.dart';

GetTicketSelectEntity $GetTicketSelectEntityFromJson(Map<String, dynamic> json) {
	final GetTicketSelectEntity getTicketSelectEntity = GetTicketSelectEntity();
	final GetTicketSelectData? data = jsonConvert.convert<GetTicketSelectData>(json['data']);
	if (data != null) {
		getTicketSelectEntity.data = data;
	}
	final GetTicketSelectBench? bench = jsonConvert.convert<GetTicketSelectBench>(json['bench']);
	if (bench != null) {
		getTicketSelectEntity.bench = bench;
	}
	return getTicketSelectEntity;
}

Map<String, dynamic> $GetTicketSelectEntityToJson(GetTicketSelectEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetTicketSelectData $GetTicketSelectDataFromJson(Map<String, dynamic> json) {
	final GetTicketSelectData getTicketSelectData = GetTicketSelectData();
	final GetTicketSelectDataPagination? pagination = jsonConvert.convert<GetTicketSelectDataPagination>(json['pagination']);
	if (pagination != null) {
		getTicketSelectData.pagination = pagination;
	}
	final List<GetTicketSelectDataRecord>? record = jsonConvert.convertListNotNull<GetTicketSelectDataRecord>(json['record']);
	if (record != null) {
		getTicketSelectData.record = record;
	}
	return getTicketSelectData;
}

Map<String, dynamic> $GetTicketSelectDataToJson(GetTicketSelectData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['pagination'] = entity.pagination?.toJson();
	data['record'] =  entity.record?.map((v) => v.toJson()).toList();
	return data;
}

GetTicketSelectDataPagination $GetTicketSelectDataPaginationFromJson(Map<String, dynamic> json) {
	final GetTicketSelectDataPagination getTicketSelectDataPagination = GetTicketSelectDataPagination();
	final int? currentPage = jsonConvert.convert<int>(json['current_page']);
	if (currentPage != null) {
		getTicketSelectDataPagination.currentPage = currentPage;
	}
	final int? lastPage = jsonConvert.convert<int>(json['last_page']);
	if (lastPage != null) {
		getTicketSelectDataPagination.lastPage = lastPage;
	}
	final int? limit = jsonConvert.convert<int>(json['limit']);
	if (limit != null) {
		getTicketSelectDataPagination.limit = limit;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getTicketSelectDataPagination.total = total;
	}
	return getTicketSelectDataPagination;
}

Map<String, dynamic> $GetTicketSelectDataPaginationToJson(GetTicketSelectDataPagination entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

GetTicketSelectDataRecord $GetTicketSelectDataRecordFromJson(Map<String, dynamic> json) {
	final GetTicketSelectDataRecord getTicketSelectDataRecord = GetTicketSelectDataRecord();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getTicketSelectDataRecord.id = id;
	}
	final int? promotionId = jsonConvert.convert<int>(json['promotion_id']);
	if (promotionId != null) {
		getTicketSelectDataRecord.promotionId = promotionId;
	}
	final GetTicketSelectDataRecordProduct? product = jsonConvert.convert<GetTicketSelectDataRecordProduct>(json['product']);
	if (product != null) {
		getTicketSelectDataRecord.product = product;
	}
	final GetTicketSelectDataRecordVariant? variant = jsonConvert.convert<GetTicketSelectDataRecordVariant>(json['variant']);
	if (variant != null) {
		getTicketSelectDataRecord.variant = variant;
	}
	final GetTicketSelectDataRecordGroup? group = jsonConvert.convert<GetTicketSelectDataRecordGroup>(json['group']);
	if (group != null) {
		getTicketSelectDataRecord.group = group;
	}
	final dynamic? amount = jsonConvert.convert<dynamic>(json['amount']);
	if (amount != null) {
		getTicketSelectDataRecord.amount = amount;
	}
	return getTicketSelectDataRecord;
}

Map<String, dynamic> $GetTicketSelectDataRecordToJson(GetTicketSelectDataRecord entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['promotion_id'] = entity.promotionId;
	data['product'] = entity.product?.toJson();
	data['variant'] = entity.variant?.toJson();
	data['group'] = entity.group?.toJson();
	data['amount'] = entity.amount;
	return data;
}

GetTicketSelectDataRecordProduct $GetTicketSelectDataRecordProductFromJson(Map<String, dynamic> json) {
	final GetTicketSelectDataRecordProduct getTicketSelectDataRecordProduct = GetTicketSelectDataRecordProduct();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getTicketSelectDataRecordProduct.id = id;
	}
	final String? type = jsonConvert.convert<String>(json['type']);
	if (type != null) {
		getTicketSelectDataRecordProduct.type = type;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getTicketSelectDataRecordProduct.name = name;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getTicketSelectDataRecordProduct.status = status;
	}
	return getTicketSelectDataRecordProduct;
}

Map<String, dynamic> $GetTicketSelectDataRecordProductToJson(GetTicketSelectDataRecordProduct entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['type'] = entity.type;
	data['name'] = entity.name;
	data['status'] = entity.status;
	return data;
}

GetTicketSelectDataRecordVariant $GetTicketSelectDataRecordVariantFromJson(Map<String, dynamic> json) {
	final GetTicketSelectDataRecordVariant getTicketSelectDataRecordVariant = GetTicketSelectDataRecordVariant();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getTicketSelectDataRecordVariant.id = id;
	}
	final String? sku = jsonConvert.convert<String>(json['sku']);
	if (sku != null) {
		getTicketSelectDataRecordVariant.sku = sku;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getTicketSelectDataRecordVariant.name = name;
	}
	final int? price = jsonConvert.convert<int>(json['price']);
	if (price != null) {
		getTicketSelectDataRecordVariant.price = price;
	}
	final int? compareAtPrice = jsonConvert.convert<int>(json['compare_at_price']);
	if (compareAtPrice != null) {
		getTicketSelectDataRecordVariant.compareAtPrice = compareAtPrice;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getTicketSelectDataRecordVariant.status = status;
	}
	return getTicketSelectDataRecordVariant;
}

Map<String, dynamic> $GetTicketSelectDataRecordVariantToJson(GetTicketSelectDataRecordVariant entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['sku'] = entity.sku;
	data['name'] = entity.name;
	data['price'] = entity.price;
	data['compare_at_price'] = entity.compareAtPrice;
	data['status'] = entity.status;
	return data;
}

GetTicketSelectDataRecordGroup $GetTicketSelectDataRecordGroupFromJson(Map<String, dynamic> json) {
	final GetTicketSelectDataRecordGroup getTicketSelectDataRecordGroup = GetTicketSelectDataRecordGroup();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getTicketSelectDataRecordGroup.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getTicketSelectDataRecordGroup.name = name;
	}
	return getTicketSelectDataRecordGroup;
}

Map<String, dynamic> $GetTicketSelectDataRecordGroupToJson(GetTicketSelectDataRecordGroup entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetTicketSelectBench $GetTicketSelectBenchFromJson(Map<String, dynamic> json) {
	final GetTicketSelectBench getTicketSelectBench = GetTicketSelectBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getTicketSelectBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getTicketSelectBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getTicketSelectBench.format = format;
	}
	return getTicketSelectBench;
}

Map<String, dynamic> $GetTicketSelectBenchToJson(GetTicketSelectBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}