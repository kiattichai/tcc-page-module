import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/share_module/model/refresh_token_response_entity.dart';

RefreshTokenResponseEntity $RefreshTokenResponseEntityFromJson(Map<String, dynamic> json) {
	final RefreshTokenResponseEntity refreshTokenResponseEntity = RefreshTokenResponseEntity();
	final RefreshTokenResponseData? data = jsonConvert.convert<RefreshTokenResponseData>(json['data']);
	if (data != null) {
		refreshTokenResponseEntity.data = data;
	}
	return refreshTokenResponseEntity;
}

Map<String, dynamic> $RefreshTokenResponseEntityToJson(RefreshTokenResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	return data;
}

RefreshTokenResponseData $RefreshTokenResponseDataFromJson(Map<String, dynamic> json) {
	final RefreshTokenResponseData refreshTokenResponseData = RefreshTokenResponseData();
	final String? tokenType = jsonConvert.convert<String>(json['token_type']);
	if (tokenType != null) {
		refreshTokenResponseData.tokenType = tokenType;
	}
	final String? accessToken = jsonConvert.convert<String>(json['access_token']);
	if (accessToken != null) {
		refreshTokenResponseData.accessToken = accessToken;
	}
	final String? refreshToken = jsonConvert.convert<String>(json['refresh_token']);
	if (refreshToken != null) {
		refreshTokenResponseData.refreshToken = refreshToken;
	}
	return refreshTokenResponseData;
}

Map<String, dynamic> $RefreshTokenResponseDataToJson(RefreshTokenResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['token_type'] = entity.tokenType;
	data['access_token'] = entity.accessToken;
	data['refresh_token'] = entity.refreshToken;
	return data;
}