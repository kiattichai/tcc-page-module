import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/error_entity.dart';

ErrorEntity $ErrorEntityFromJson(Map<String, dynamic> json) {
  final ErrorEntity errorEntity = ErrorEntity();
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    errorEntity.type = type;
  }
  final int? code = jsonConvert.convert<int>(json['code']);
  if (code != null) {
    errorEntity.code = code;
  }
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    errorEntity.message = message;
  }
  final String? field = jsonConvert.convert<String>(json['field']);
  if (field != null) {
    errorEntity.field = field;
  }
  final String? line = jsonConvert.convert<String>(json['line']);
  if (line != null) {
    errorEntity.line = line;
  }
  return errorEntity;
}

Map<String, dynamic> $ErrorEntityToJson(ErrorEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['type'] = entity.type;
  data['code'] = entity.code;
  data['message'] = entity.message;
  data['field'] = entity.field;
  data['line'] = entity.line;
  return data;
}
