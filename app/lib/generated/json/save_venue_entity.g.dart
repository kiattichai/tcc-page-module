import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_venue_entity.dart';

SaveVenueEntity $SaveVenueEntityFromJson(Map<String, dynamic> json) {
	final SaveVenueEntity saveVenueEntity = SaveVenueEntity();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		saveVenueEntity.id = id;
	}
	final int? countryId = jsonConvert.convert<int>(json['country_id']);
	if (countryId != null) {
		saveVenueEntity.countryId = countryId;
	}
	final String? storeId = jsonConvert.convert<String>(json['store_id']);
	if (storeId != null) {
		saveVenueEntity.storeId = storeId;
	}
	final SaveVenueName? name = jsonConvert.convert<SaveVenueName>(json['name']);
	if (name != null) {
		saveVenueEntity.name = name;
	}
	final SaveVenueAddress? address = jsonConvert.convert<SaveVenueAddress>(json['address']);
	if (address != null) {
		saveVenueEntity.address = address;
	}
	final int? provinceId = jsonConvert.convert<int>(json['province_id']);
	if (provinceId != null) {
		saveVenueEntity.provinceId = provinceId;
	}
	final int? cityId = jsonConvert.convert<int>(json['city_id']);
	if (cityId != null) {
		saveVenueEntity.cityId = cityId;
	}
	final int? districtId = jsonConvert.convert<int>(json['district_id']);
	if (districtId != null) {
		saveVenueEntity.districtId = districtId;
	}
	final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
	if (zipCode != null) {
		saveVenueEntity.zipCode = zipCode;
	}
	final String? lat = jsonConvert.convert<String>(json['lat']);
	if (lat != null) {
		saveVenueEntity.lat = lat;
	}
	final String? long = jsonConvert.convert<String>(json['long']);
	if (long != null) {
		saveVenueEntity.long = long;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		saveVenueEntity.status = status;
	}
	final bool? xDefault = jsonConvert.convert<bool>(json['default']);
	if (xDefault != null) {
		saveVenueEntity.xDefault = xDefault;
	}
	return saveVenueEntity;
}

Map<String, dynamic> $SaveVenueEntityToJson(SaveVenueEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['country_id'] = entity.countryId;
	data['store_id'] = entity.storeId;
	data['name'] = entity.name?.toJson();
	data['address'] = entity.address?.toJson();
	data['province_id'] = entity.provinceId;
	data['city_id'] = entity.cityId;
	data['district_id'] = entity.districtId;
	data['zip_code'] = entity.zipCode;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['status'] = entity.status;
	data['default'] = entity.xDefault;
	return data;
}

SaveVenueName $SaveVenueNameFromJson(Map<String, dynamic> json) {
	final SaveVenueName saveVenueName = SaveVenueName();
	final String? th = jsonConvert.convert<String>(json['th']);
	if (th != null) {
		saveVenueName.th = th;
	}
	final String? en = jsonConvert.convert<String>(json['en']);
	if (en != null) {
		saveVenueName.en = en;
	}
	return saveVenueName;
}

Map<String, dynamic> $SaveVenueNameToJson(SaveVenueName entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['th'] = entity.th;
	data['en'] = entity.en;
	return data;
}

SaveVenueAddress $SaveVenueAddressFromJson(Map<String, dynamic> json) {
	final SaveVenueAddress saveVenueAddress = SaveVenueAddress();
	final String? th = jsonConvert.convert<String>(json['th']);
	if (th != null) {
		saveVenueAddress.th = th;
	}
	final String? en = jsonConvert.convert<String>(json['en']);
	if (en != null) {
		saveVenueAddress.en = en;
	}
	return saveVenueAddress;
}

Map<String, dynamic> $SaveVenueAddressToJson(SaveVenueAddress entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['th'] = entity.th;
	data['en'] = entity.en;
	return data;
}