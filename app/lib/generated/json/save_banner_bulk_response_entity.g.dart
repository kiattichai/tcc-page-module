import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_banner_bulk_response_entity.dart';

SaveBannerBulkResponseEntity $SaveBannerBulkResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SaveBannerBulkResponseEntity saveBannerBulkResponseEntity =
      SaveBannerBulkResponseEntity();
  final SaveBannerBulkResponseData? data =
      jsonConvert.convert<SaveBannerBulkResponseData>(json['data']);
  if (data != null) {
    saveBannerBulkResponseEntity.data = data;
  }
  final SaveBannerBulkResponseBench? bench =
      jsonConvert.convert<SaveBannerBulkResponseBench>(json['bench']);
  if (bench != null) {
    saveBannerBulkResponseEntity.bench = bench;
  }
  return saveBannerBulkResponseEntity;
}

Map<String, dynamic> $SaveBannerBulkResponseEntityToJson(
    SaveBannerBulkResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SaveBannerBulkResponseData $SaveBannerBulkResponseDataFromJson(
    Map<String, dynamic> json) {
  final SaveBannerBulkResponseData saveBannerBulkResponseData =
      SaveBannerBulkResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    saveBannerBulkResponseData.message = message;
  }
  return saveBannerBulkResponseData;
}

Map<String, dynamic> $SaveBannerBulkResponseDataToJson(
    SaveBannerBulkResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

SaveBannerBulkResponseBench $SaveBannerBulkResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SaveBannerBulkResponseBench saveBannerBulkResponseBench =
      SaveBannerBulkResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    saveBannerBulkResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    saveBannerBulkResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    saveBannerBulkResponseBench.format = format;
  }
  return saveBannerBulkResponseBench;
}

Map<String, dynamic> $SaveBannerBulkResponseBenchToJson(
    SaveBannerBulkResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
