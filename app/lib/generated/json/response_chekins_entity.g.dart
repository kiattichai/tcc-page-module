import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/response_chekins_entity.dart';

ResponseChekinsEntity $ResponseChekinsEntityFromJson(
    Map<String, dynamic> json) {
  final ResponseChekinsEntity responseChekinsEntity = ResponseChekinsEntity();
  final ResponseChekinsData? data =
      jsonConvert.convert<ResponseChekinsData>(json['data']);
  if (data != null) {
    responseChekinsEntity.data = data;
  }
  final ResponseChekinsBench? bench =
      jsonConvert.convert<ResponseChekinsBench>(json['bench']);
  if (bench != null) {
    responseChekinsEntity.bench = bench;
  }
  return responseChekinsEntity;
}

Map<String, dynamic> $ResponseChekinsEntityToJson(
    ResponseChekinsEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseChekinsData $ResponseChekinsDataFromJson(Map<String, dynamic> json) {
  final ResponseChekinsData responseChekinsData = ResponseChekinsData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseChekinsData.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    responseChekinsData.id = id;
  }
  return responseChekinsData;
}

Map<String, dynamic> $ResponseChekinsDataToJson(ResponseChekinsData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  return data;
}

ResponseChekinsBench $ResponseChekinsBenchFromJson(Map<String, dynamic> json) {
  final ResponseChekinsBench responseChekinsBench = ResponseChekinsBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseChekinsBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseChekinsBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseChekinsBench.format = format;
  }
  return responseChekinsBench;
}

Map<String, dynamic> $ResponseChekinsBenchToJson(ResponseChekinsBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
