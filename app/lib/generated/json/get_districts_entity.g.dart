import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_districts_entity.dart';

GetDistrictsEntity $GetDistrictsEntityFromJson(Map<String, dynamic> json) {
  final GetDistrictsEntity getDistrictsEntity = GetDistrictsEntity();
  final GetDistrictsData? data =
      jsonConvert.convert<GetDistrictsData>(json['data']);
  if (data != null) {
    getDistrictsEntity.data = data;
  }
  final GetDistrictsBench? bench =
      jsonConvert.convert<GetDistrictsBench>(json['bench']);
  if (bench != null) {
    getDistrictsEntity.bench = bench;
  }
  return getDistrictsEntity;
}

Map<String, dynamic> $GetDistrictsEntityToJson(GetDistrictsEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetDistrictsData $GetDistrictsDataFromJson(Map<String, dynamic> json) {
  final GetDistrictsData getDistrictsData = GetDistrictsData();
  final List<GetDistrictsDataRecord>? record =
      jsonConvert.convertListNotNull<GetDistrictsDataRecord>(json['record']);
  if (record != null) {
    getDistrictsData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getDistrictsData.cache = cache;
  }
  return getDistrictsData;
}

Map<String, dynamic> $GetDistrictsDataToJson(GetDistrictsData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetDistrictsDataRecord $GetDistrictsDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetDistrictsDataRecord getDistrictsDataRecord =
      GetDistrictsDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDistrictsDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getDistrictsDataRecord.name = name;
  }
  final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
  if (zipCode != null) {
    getDistrictsDataRecord.zipCode = zipCode;
  }
  final String? zipCodeMap = jsonConvert.convert<String>(json['zip_code_map']);
  if (zipCodeMap != null) {
    getDistrictsDataRecord.zipCodeMap = zipCodeMap;
  }
  return getDistrictsDataRecord;
}

Map<String, dynamic> $GetDistrictsDataRecordToJson(
    GetDistrictsDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['zip_code'] = entity.zipCode;
  data['zip_code_map'] = entity.zipCodeMap;
  return data;
}

GetDistrictsBench $GetDistrictsBenchFromJson(Map<String, dynamic> json) {
  final GetDistrictsBench getDistrictsBench = GetDistrictsBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getDistrictsBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getDistrictsBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getDistrictsBench.format = format;
  }
  return getDistrictsBench;
}

Map<String, dynamic> $GetDistrictsBenchToJson(GetDistrictsBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
