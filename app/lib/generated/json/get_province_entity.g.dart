import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_province_entity.dart';

GetProvinceEntity $GetProvinceEntityFromJson(Map<String, dynamic> json) {
  final GetProvinceEntity getProvinceEntity = GetProvinceEntity();
  final GetProvinceData? data =
      jsonConvert.convert<GetProvinceData>(json['data']);
  if (data != null) {
    getProvinceEntity.data = data;
  }
  final GetProvinceBench? bench =
      jsonConvert.convert<GetProvinceBench>(json['bench']);
  if (bench != null) {
    getProvinceEntity.bench = bench;
  }
  return getProvinceEntity;
}

Map<String, dynamic> $GetProvinceEntityToJson(GetProvinceEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetProvinceData $GetProvinceDataFromJson(Map<String, dynamic> json) {
  final GetProvinceData getProvinceData = GetProvinceData();
  final List<GetProvinceDataRecord>? record =
      jsonConvert.convertListNotNull<GetProvinceDataRecord>(json['record']);
  if (record != null) {
    getProvinceData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getProvinceData.cache = cache;
  }
  return getProvinceData;
}

Map<String, dynamic> $GetProvinceDataToJson(GetProvinceData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetProvinceDataRecord $GetProvinceDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetProvinceDataRecord getProvinceDataRecord = GetProvinceDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProvinceDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProvinceDataRecord.name = name;
  }
  return getProvinceDataRecord;
}

Map<String, dynamic> $GetProvinceDataRecordToJson(
    GetProvinceDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProvinceBench $GetProvinceBenchFromJson(Map<String, dynamic> json) {
  final GetProvinceBench getProvinceBench = GetProvinceBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getProvinceBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getProvinceBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getProvinceBench.format = format;
  }
  return getProvinceBench;
}

Map<String, dynamic> $GetProvinceBenchToJson(GetProvinceBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
