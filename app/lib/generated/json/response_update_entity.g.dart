import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/response_update_entity.dart';

ResponseUpdateEntity $ResponseUpdateEntityFromJson(Map<String, dynamic> json) {
  final ResponseUpdateEntity responseUpdateEntity = ResponseUpdateEntity();
  final ResponseUpdateData? data =
      jsonConvert.convert<ResponseUpdateData>(json['data']);
  if (data != null) {
    responseUpdateEntity.data = data;
  }
  final ResponseUpdateBench? bench =
      jsonConvert.convert<ResponseUpdateBench>(json['bench']);
  if (bench != null) {
    responseUpdateEntity.bench = bench;
  }
  return responseUpdateEntity;
}

Map<String, dynamic> $ResponseUpdateEntityToJson(ResponseUpdateEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseUpdateData $ResponseUpdateDataFromJson(Map<String, dynamic> json) {
  final ResponseUpdateData responseUpdateData = ResponseUpdateData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseUpdateData.message = message;
  }
  return responseUpdateData;
}

Map<String, dynamic> $ResponseUpdateDataToJson(ResponseUpdateData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

ResponseUpdateBench $ResponseUpdateBenchFromJson(Map<String, dynamic> json) {
  final ResponseUpdateBench responseUpdateBench = ResponseUpdateBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseUpdateBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseUpdateBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseUpdateBench.format = format;
  }
  return responseUpdateBench;
}

Map<String, dynamic> $ResponseUpdateBenchToJson(ResponseUpdateBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
