import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/register_nightlife_entity.dart';

RegisterNightlifeEntity $RegisterNightlifeEntityFromJson(
    Map<String, dynamic> json) {
  final RegisterNightlifeEntity registerNightlifeEntity =
      RegisterNightlifeEntity();
  final List<int>? vaccinesReceived =
      jsonConvert.convertListNotNull<int>(json['vaccines_received']);
  if (vaccinesReceived != null) {
    registerNightlifeEntity.vaccinesReceived = vaccinesReceived;
  }
  final RegisterNightlifeEvidence? evidence =
      jsonConvert.convert<RegisterNightlifeEvidence>(json['evidence']);
  if (evidence != null) {
    registerNightlifeEntity.evidence = evidence;
  }
  return registerNightlifeEntity;
}

Map<String, dynamic> $RegisterNightlifeEntityToJson(
    RegisterNightlifeEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['vaccines_received'] = entity.vaccinesReceived;
  data['evidence'] = entity.evidence?.toJson();
  return data;
}

RegisterNightlifeEvidence $RegisterNightlifeEvidenceFromJson(
    Map<String, dynamic> json) {
  final RegisterNightlifeEvidence registerNightlifeEvidence =
      RegisterNightlifeEvidence();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    registerNightlifeEvidence.name = name;
  }
  final String? image = jsonConvert.convert<String>(json['image']);
  if (image != null) {
    registerNightlifeEvidence.image = image;
  }
  return registerNightlifeEvidence;
}

Map<String, dynamic> $RegisterNightlifeEvidenceToJson(
    RegisterNightlifeEvidence entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['image'] = entity.image;
  return data;
}
