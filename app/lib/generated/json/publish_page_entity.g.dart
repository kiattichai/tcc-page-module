import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/publish_page_entity.dart';

PublishPageEntity $PublishPageEntityFromJson(Map<String, dynamic> json) {
  final PublishPageEntity publishPageEntity = PublishPageEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    publishPageEntity.storeId = storeId;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    publishPageEntity.status = status;
  }
  return publishPageEntity;
}

Map<String, dynamic> $PublishPageEntityToJson(PublishPageEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['status'] = entity.status;
  return data;
}
