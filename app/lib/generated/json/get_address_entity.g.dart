import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_address_entity.dart';

GetAddressEntity $GetAddressEntityFromJson(Map<String, dynamic> json) {
  final GetAddressEntity getAddressEntity = GetAddressEntity();
  final GetAddressData? data =
      jsonConvert.convert<GetAddressData>(json['data']);
  if (data != null) {
    getAddressEntity.data = data;
  }
  final GetAddressBench? bench =
      jsonConvert.convert<GetAddressBench>(json['bench']);
  if (bench != null) {
    getAddressEntity.bench = bench;
  }
  return getAddressEntity;
}

Map<String, dynamic> $GetAddressEntityToJson(GetAddressEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAddressData $GetAddressDataFromJson(Map<String, dynamic> json) {
  final GetAddressData getAddressData = GetAddressData();
  final List<GetAddressDataRecord>? record =
      jsonConvert.convertListNotNull<GetAddressDataRecord>(json['record']);
  if (record != null) {
    getAddressData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getAddressData.cache = cache;
  }
  return getAddressData;
}

Map<String, dynamic> $GetAddressDataToJson(GetAddressData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetAddressDataRecord $GetAddressDataRecordFromJson(Map<String, dynamic> json) {
  final GetAddressDataRecord getAddressDataRecord = GetAddressDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAddressDataRecord.id = id;
  }
  final GetAddressDataRecordName? name =
      jsonConvert.convert<GetAddressDataRecordName>(json['name']);
  if (name != null) {
    getAddressDataRecord.name = name;
  }
  return getAddressDataRecord;
}

Map<String, dynamic> $GetAddressDataRecordToJson(GetAddressDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name?.toJson();
  return data;
}

GetAddressDataRecordName $GetAddressDataRecordNameFromJson(
    Map<String, dynamic> json) {
  final GetAddressDataRecordName getAddressDataRecordName =
      GetAddressDataRecordName();
  final String? th = jsonConvert.convert<String>(json['th']);
  if (th != null) {
    getAddressDataRecordName.th = th;
  }
  final String? en = jsonConvert.convert<String>(json['en']);
  if (en != null) {
    getAddressDataRecordName.en = en;
  }
  return getAddressDataRecordName;
}

Map<String, dynamic> $GetAddressDataRecordNameToJson(
    GetAddressDataRecordName entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['th'] = entity.th;
  data['en'] = entity.en;
  return data;
}

GetAddressBench $GetAddressBenchFromJson(Map<String, dynamic> json) {
  final GetAddressBench getAddressBench = GetAddressBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAddressBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAddressBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAddressBench.format = format;
  }
  return getAddressBench;
}

Map<String, dynamic> $GetAddressBenchToJson(GetAddressBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
