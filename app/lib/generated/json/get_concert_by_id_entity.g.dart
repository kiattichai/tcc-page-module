import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_concert_by_id_entity.dart';

GetConcertByIdEntity $GetConcertByIdEntityFromJson(Map<String, dynamic> json) {
	final GetConcertByIdEntity getConcertByIdEntity = GetConcertByIdEntity();
	final GetConcertByIdData? data = jsonConvert.convert<GetConcertByIdData>(json['data']);
	if (data != null) {
		getConcertByIdEntity.data = data;
	}
	final GetConcertByIdBench? bench = jsonConvert.convert<GetConcertByIdBench>(json['bench']);
	if (bench != null) {
		getConcertByIdEntity.bench = bench;
	}
	return getConcertByIdEntity;
}

Map<String, dynamic> $GetConcertByIdEntityToJson(GetConcertByIdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetConcertByIdData $GetConcertByIdDataFromJson(Map<String, dynamic> json) {
	final GetConcertByIdData getConcertByIdData = GetConcertByIdData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdData.id = id;
	}
	final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
	if (slug != null) {
		getConcertByIdData.slug = slug;
	}
	final String? type = jsonConvert.convert<String>(json['type']);
	if (type != null) {
		getConcertByIdData.type = type;
	}
	final String? groupType = jsonConvert.convert<String>(json['group_type']);
	if (groupType != null) {
		getConcertByIdData.groupType = groupType;
	}
	final int? parentId = jsonConvert.convert<int>(json['parent_id']);
	if (parentId != null) {
		getConcertByIdData.parentId = parentId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdData.name = name;
	}
	final String? descriptionShort = jsonConvert.convert<String>(json['description_short']);
	if (descriptionShort != null) {
		getConcertByIdData.descriptionShort = descriptionShort;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		getConcertByIdData.description = description;
	}
	final String? seoName = jsonConvert.convert<String>(json['seo_name']);
	if (seoName != null) {
		getConcertByIdData.seoName = seoName;
	}
	final String? seoDescription = jsonConvert.convert<String>(json['seo_description']);
	if (seoDescription != null) {
		getConcertByIdData.seoDescription = seoDescription;
	}
	final GetConcertByIdDataStore? store = jsonConvert.convert<GetConcertByIdDataStore>(json['store']);
	if (store != null) {
		getConcertByIdData.store = store;
	}
	final GetConcertByIdDataVenue? venue = jsonConvert.convert<GetConcertByIdDataVenue>(json['venue']);
	if (venue != null) {
		getConcertByIdData.venue = venue;
	}
	final GetConcertByIdDataStage? stage = jsonConvert.convert<GetConcertByIdDataStage>(json['stage']);
	if (stage != null) {
		getConcertByIdData.stage = stage;
	}
	final GetConcertByIdDataShowTime? showTime = jsonConvert.convert<GetConcertByIdDataShowTime>(json['show_time']);
	if (showTime != null) {
		getConcertByIdData.showTime = showTime;
	}
	final GetConcertByIdDataPrice? price = jsonConvert.convert<GetConcertByIdDataPrice>(json['price']);
	if (price != null) {
		getConcertByIdData.price = price;
	}
	final List<dynamic>? variants = jsonConvert.convertListNotNull<dynamic>(json['variants']);
	if (variants != null) {
		getConcertByIdData.variants = variants;
	}
	final List<GetConcertByIdDataAttributes>? attributes = jsonConvert.convertListNotNull<GetConcertByIdDataAttributes>(json['attributes']);
	if (attributes != null) {
		getConcertByIdData.attributes = attributes;
	}
	final List<GetConcertByIdDataImages>? images = jsonConvert.convertListNotNull<GetConcertByIdDataImages>(json['images']);
	if (images != null) {
		getConcertByIdData.images = images;
	}
	final int? remain = jsonConvert.convert<int>(json['remain']);
	if (remain != null) {
		getConcertByIdData.remain = remain;
	}
	final int? ticketCount = jsonConvert.convert<int>(json['ticket_count']);
	if (ticketCount != null) {
		getConcertByIdData.ticketCount = ticketCount;
	}
	final bool? onlineCheckin = jsonConvert.convert<bool>(json['online_checkin']);
	if (onlineCheckin != null) {
		getConcertByIdData.onlineCheckin = onlineCheckin;
	}
	final int? viewed = jsonConvert.convert<int>(json['viewed']);
	if (viewed != null) {
		getConcertByIdData.viewed = viewed;
	}
	final int? viewedText = jsonConvert.convert<int>(json['viewed_text']);
	if (viewedText != null) {
		getConcertByIdData.viewedText = viewedText;
	}
	final dynamic? salesUrl = jsonConvert.convert<dynamic>(json['sales_url']);
	if (salesUrl != null) {
		getConcertByIdData.salesUrl = salesUrl;
	}
	final String? shareUrl = jsonConvert.convert<String>(json['share_url']);
	if (shareUrl != null) {
		getConcertByIdData.shareUrl = shareUrl;
	}
	final String? webviewUrl = jsonConvert.convert<String>(json['webview_url']);
	if (webviewUrl != null) {
		getConcertByIdData.webviewUrl = webviewUrl;
	}
	final bool? includeVat = jsonConvert.convert<bool>(json['include_vat']);
	if (includeVat != null) {
		getConcertByIdData.includeVat = includeVat;
	}
	final bool? paymentCharge = jsonConvert.convert<bool>(json['payment_charge']);
	if (paymentCharge != null) {
		getConcertByIdData.paymentCharge = paymentCharge;
	}
	final bool? hasVariant = jsonConvert.convert<bool>(json['has_variant']);
	if (hasVariant != null) {
		getConcertByIdData.hasVariant = hasVariant;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdData.status = status;
	}
	final bool? ticketShipping = jsonConvert.convert<bool>(json['ticket_shipping']);
	if (ticketShipping != null) {
		getConcertByIdData.ticketShipping = ticketShipping;
	}
	final GetConcertByIdDataPublishStatus? publishStatus = jsonConvert.convert<GetConcertByIdDataPublishStatus>(json['publish_status']);
	if (publishStatus != null) {
		getConcertByIdData.publishStatus = publishStatus;
	}
	final String? publishAt = jsonConvert.convert<String>(json['publish_at']);
	if (publishAt != null) {
		getConcertByIdData.publishAt = publishAt;
	}
	final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
	if (remark != null) {
		getConcertByIdData.remark = remark;
	}
	final bool? soldoutStatus = jsonConvert.convert<bool>(json['soldout_status']);
	if (soldoutStatus != null) {
		getConcertByIdData.soldoutStatus = soldoutStatus;
	}
	final int? soldoutStatusId = jsonConvert.convert<int>(json['soldout_status_id']);
	if (soldoutStatusId != null) {
		getConcertByIdData.soldoutStatusId = soldoutStatusId;
	}
	final bool? salesStatus = jsonConvert.convert<bool>(json['sales_status']);
	if (salesStatus != null) {
		getConcertByIdData.salesStatus = salesStatus;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		getConcertByIdData.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		getConcertByIdData.updatedAt = updatedAt;
	}
	final bool? updated = jsonConvert.convert<bool>(json['updated']);
	if (updated != null) {
		getConcertByIdData.updated = updated;
	}
	final bool? verify = jsonConvert.convert<bool>(json['verify']);
	if (verify != null) {
		getConcertByIdData.verify = verify;
	}
	final GetConcertByIdDataSettings? settings = jsonConvert.convert<GetConcertByIdDataSettings>(json['settings']);
	if (settings != null) {
		getConcertByIdData.settings = settings;
	}
	return getConcertByIdData;
}

Map<String, dynamic> $GetConcertByIdDataToJson(GetConcertByIdData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['slug'] = entity.slug;
	data['type'] = entity.type;
	data['group_type'] = entity.groupType;
	data['parent_id'] = entity.parentId;
	data['name'] = entity.name;
	data['description_short'] = entity.descriptionShort;
	data['description'] = entity.description;
	data['seo_name'] = entity.seoName;
	data['seo_description'] = entity.seoDescription;
	data['store'] = entity.store?.toJson();
	data['venue'] = entity.venue?.toJson();
	data['stage'] = entity.stage?.toJson();
	data['show_time'] = entity.showTime?.toJson();
	data['price'] = entity.price?.toJson();
	data['variants'] =  entity.variants;
	data['attributes'] =  entity.attributes?.map((v) => v.toJson()).toList();
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['remain'] = entity.remain;
	data['ticket_count'] = entity.ticketCount;
	data['online_checkin'] = entity.onlineCheckin;
	data['viewed'] = entity.viewed;
	data['viewed_text'] = entity.viewedText;
	data['sales_url'] = entity.salesUrl;
	data['share_url'] = entity.shareUrl;
	data['webview_url'] = entity.webviewUrl;
	data['include_vat'] = entity.includeVat;
	data['payment_charge'] = entity.paymentCharge;
	data['has_variant'] = entity.hasVariant;
	data['status'] = entity.status;
	data['ticket_shipping'] = entity.ticketShipping;
	data['publish_status'] = entity.publishStatus?.toJson();
	data['publish_at'] = entity.publishAt;
	data['remark'] = entity.remark;
	data['soldout_status'] = entity.soldoutStatus;
	data['soldout_status_id'] = entity.soldoutStatusId;
	data['sales_status'] = entity.salesStatus;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['updated'] = entity.updated;
	data['verify'] = entity.verify;
	data['settings'] = entity.settings?.toJson();
	return data;
}

GetConcertByIdDataStore $GetConcertByIdDataStoreFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStore getConcertByIdDataStore = GetConcertByIdDataStore();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataStore.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataStore.name = name;
	}
	final String? slug = jsonConvert.convert<String>(json['slug']);
	if (slug != null) {
		getConcertByIdDataStore.slug = slug;
	}
	final GetConcertByIdDataStoreType? type = jsonConvert.convert<GetConcertByIdDataStoreType>(json['type']);
	if (type != null) {
		getConcertByIdDataStore.type = type;
	}
	final GetConcertByIdDataStoreSection? section = jsonConvert.convert<GetConcertByIdDataStoreSection>(json['section']);
	if (section != null) {
		getConcertByIdDataStore.section = section;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataStore.status = status;
	}
	final GetConcertByIdDataStoreImage? image = jsonConvert.convert<GetConcertByIdDataStoreImage>(json['image']);
	if (image != null) {
		getConcertByIdDataStore.image = image;
	}
	final GetConcertByIdDataStoreVenue? venue = jsonConvert.convert<GetConcertByIdDataStoreVenue>(json['venue']);
	if (venue != null) {
		getConcertByIdDataStore.venue = venue;
	}
	return getConcertByIdDataStore;
}

Map<String, dynamic> $GetConcertByIdDataStoreToJson(GetConcertByIdDataStore entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['slug'] = entity.slug;
	data['type'] = entity.type?.toJson();
	data['section'] = entity.section?.toJson();
	data['status'] = entity.status;
	data['image'] = entity.image?.toJson();
	data['venue'] = entity.venue?.toJson();
	return data;
}

GetConcertByIdDataStoreType $GetConcertByIdDataStoreTypeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStoreType getConcertByIdDataStoreType = GetConcertByIdDataStoreType();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataStoreType.id = id;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		getConcertByIdDataStoreType.text = text;
	}
	return getConcertByIdDataStoreType;
}

Map<String, dynamic> $GetConcertByIdDataStoreTypeToJson(GetConcertByIdDataStoreType entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

GetConcertByIdDataStoreSection $GetConcertByIdDataStoreSectionFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStoreSection getConcertByIdDataStoreSection = GetConcertByIdDataStoreSection();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataStoreSection.id = id;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		getConcertByIdDataStoreSection.text = text;
	}
	return getConcertByIdDataStoreSection;
}

Map<String, dynamic> $GetConcertByIdDataStoreSectionToJson(GetConcertByIdDataStoreSection entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

GetConcertByIdDataStoreImage $GetConcertByIdDataStoreImageFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStoreImage getConcertByIdDataStoreImage = GetConcertByIdDataStoreImage();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getConcertByIdDataStoreImage.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertByIdDataStoreImage.storeId = storeId;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getConcertByIdDataStoreImage.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataStoreImage.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getConcertByIdDataStoreImage.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getConcertByIdDataStoreImage.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getConcertByIdDataStoreImage.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getConcertByIdDataStoreImage.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getConcertByIdDataStoreImage.url = url;
	}
	return getConcertByIdDataStoreImage;
}

Map<String, dynamic> $GetConcertByIdDataStoreImageToJson(GetConcertByIdDataStoreImage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

GetConcertByIdDataStoreVenue $GetConcertByIdDataStoreVenueFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStoreVenue getConcertByIdDataStoreVenue = GetConcertByIdDataStoreVenue();
	return getConcertByIdDataStoreVenue;
}

Map<String, dynamic> $GetConcertByIdDataStoreVenueToJson(GetConcertByIdDataStoreVenue entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	return data;
}

GetConcertByIdDataVenue $GetConcertByIdDataVenueFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataVenue getConcertByIdDataVenue = GetConcertByIdDataVenue();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataVenue.id = id;
	}
	final double? lat = jsonConvert.convert<double>(json['lat']);
	if (lat != null) {
		getConcertByIdDataVenue.lat = lat;
	}
	final double? long = jsonConvert.convert<double>(json['long']);
	if (long != null) {
		getConcertByIdDataVenue.long = long;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataVenue.name = name;
	}
	final String? address = jsonConvert.convert<String>(json['address']);
	if (address != null) {
		getConcertByIdDataVenue.address = address;
	}
	final String? metaTitle = jsonConvert.convert<String>(json['meta_title']);
	if (metaTitle != null) {
		getConcertByIdDataVenue.metaTitle = metaTitle;
	}
	final String? metaDescription = jsonConvert.convert<String>(json['meta_description']);
	if (metaDescription != null) {
		getConcertByIdDataVenue.metaDescription = metaDescription;
	}
	final String? metaKeyword = jsonConvert.convert<String>(json['meta_keyword']);
	if (metaKeyword != null) {
		getConcertByIdDataVenue.metaKeyword = metaKeyword;
	}
	final GetConcertByIdDataVenueCountry? country = jsonConvert.convert<GetConcertByIdDataVenueCountry>(json['country']);
	if (country != null) {
		getConcertByIdDataVenue.country = country;
	}
	final GetConcertByIdDataVenueProvince? province = jsonConvert.convert<GetConcertByIdDataVenueProvince>(json['province']);
	if (province != null) {
		getConcertByIdDataVenue.province = province;
	}
	final dynamic? city = jsonConvert.convert<dynamic>(json['city']);
	if (city != null) {
		getConcertByIdDataVenue.city = city;
	}
	final dynamic? district = jsonConvert.convert<dynamic>(json['district']);
	if (district != null) {
		getConcertByIdDataVenue.district = district;
	}
	final dynamic? zipCode = jsonConvert.convert<dynamic>(json['zip_code']);
	if (zipCode != null) {
		getConcertByIdDataVenue.zipCode = zipCode;
	}
	return getConcertByIdDataVenue;
}

Map<String, dynamic> $GetConcertByIdDataVenueToJson(GetConcertByIdDataVenue entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['name'] = entity.name;
	data['address'] = entity.address;
	data['meta_title'] = entity.metaTitle;
	data['meta_description'] = entity.metaDescription;
	data['meta_keyword'] = entity.metaKeyword;
	data['country'] = entity.country?.toJson();
	data['province'] = entity.province?.toJson();
	data['city'] = entity.city;
	data['district'] = entity.district;
	data['zip_code'] = entity.zipCode;
	return data;
}

GetConcertByIdDataVenueCountry $GetConcertByIdDataVenueCountryFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataVenueCountry getConcertByIdDataVenueCountry = GetConcertByIdDataVenueCountry();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataVenueCountry.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataVenueCountry.name = name;
	}
	return getConcertByIdDataVenueCountry;
}

Map<String, dynamic> $GetConcertByIdDataVenueCountryToJson(GetConcertByIdDataVenueCountry entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertByIdDataVenueProvince $GetConcertByIdDataVenueProvinceFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataVenueProvince getConcertByIdDataVenueProvince = GetConcertByIdDataVenueProvince();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataVenueProvince.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataVenueProvince.name = name;
	}
	return getConcertByIdDataVenueProvince;
}

Map<String, dynamic> $GetConcertByIdDataVenueProvinceToJson(GetConcertByIdDataVenueProvince entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertByIdDataStage $GetConcertByIdDataStageFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataStage getConcertByIdDataStage = GetConcertByIdDataStage();
	return getConcertByIdDataStage;
}

Map<String, dynamic> $GetConcertByIdDataStageToJson(GetConcertByIdDataStage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	return data;
}

GetConcertByIdDataShowTime $GetConcertByIdDataShowTimeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataShowTime getConcertByIdDataShowTime = GetConcertByIdDataShowTime();
	final String? start = jsonConvert.convert<String>(json['start']);
	if (start != null) {
		getConcertByIdDataShowTime.start = start;
	}
	final String? end = jsonConvert.convert<String>(json['end']);
	if (end != null) {
		getConcertByIdDataShowTime.end = end;
	}
	final String? textFull = jsonConvert.convert<String>(json['text_full']);
	if (textFull != null) {
		getConcertByIdDataShowTime.textFull = textFull;
	}
	final String? textShort = jsonConvert.convert<String>(json['text_short']);
	if (textShort != null) {
		getConcertByIdDataShowTime.textShort = textShort;
	}
	final String? textShortDate = jsonConvert.convert<String>(json['text_short_date']);
	if (textShortDate != null) {
		getConcertByIdDataShowTime.textShortDate = textShortDate;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		getConcertByIdDataShowTime.status = status;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		getConcertByIdDataShowTime.statusText = statusText;
	}
	return getConcertByIdDataShowTime;
}

Map<String, dynamic> $GetConcertByIdDataShowTimeToJson(GetConcertByIdDataShowTime entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['start'] = entity.start;
	data['end'] = entity.end;
	data['text_full'] = entity.textFull;
	data['text_short'] = entity.textShort;
	data['text_short_date'] = entity.textShortDate;
	data['status'] = entity.status;
	data['status_text'] = entity.statusText;
	return data;
}

GetConcertByIdDataPrice $GetConcertByIdDataPriceFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataPrice getConcertByIdDataPrice = GetConcertByIdDataPrice();
	final String? currencyCode = jsonConvert.convert<String>(json['currency_code']);
	if (currencyCode != null) {
		getConcertByIdDataPrice.currencyCode = currencyCode;
	}
	final String? currencySymbol = jsonConvert.convert<String>(json['currency_symbol']);
	if (currencySymbol != null) {
		getConcertByIdDataPrice.currencySymbol = currencySymbol;
	}
	final int? min = jsonConvert.convert<int>(json['min']);
	if (min != null) {
		getConcertByIdDataPrice.min = min;
	}
	final int? max = jsonConvert.convert<int>(json['max']);
	if (max != null) {
		getConcertByIdDataPrice.max = max;
	}
	final String? minText = jsonConvert.convert<String>(json['min_text']);
	if (minText != null) {
		getConcertByIdDataPrice.minText = minText;
	}
	final String? maxText = jsonConvert.convert<String>(json['max_text']);
	if (maxText != null) {
		getConcertByIdDataPrice.maxText = maxText;
	}
	final int? compareMin = jsonConvert.convert<int>(json['compare_min']);
	if (compareMin != null) {
		getConcertByIdDataPrice.compareMin = compareMin;
	}
	final int? compareMax = jsonConvert.convert<int>(json['compare_max']);
	if (compareMax != null) {
		getConcertByIdDataPrice.compareMax = compareMax;
	}
	final String? compareMinText = jsonConvert.convert<String>(json['compare_min_text']);
	if (compareMinText != null) {
		getConcertByIdDataPrice.compareMinText = compareMinText;
	}
	final String? compareMaxText = jsonConvert.convert<String>(json['compare_max_text']);
	if (compareMaxText != null) {
		getConcertByIdDataPrice.compareMaxText = compareMaxText;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataPrice.status = status;
	}
	return getConcertByIdDataPrice;
}

Map<String, dynamic> $GetConcertByIdDataPriceToJson(GetConcertByIdDataPrice entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['currency_code'] = entity.currencyCode;
	data['currency_symbol'] = entity.currencySymbol;
	data['min'] = entity.min;
	data['max'] = entity.max;
	data['min_text'] = entity.minText;
	data['max_text'] = entity.maxText;
	data['compare_min'] = entity.compareMin;
	data['compare_max'] = entity.compareMax;
	data['compare_min_text'] = entity.compareMinText;
	data['compare_max_text'] = entity.compareMaxText;
	data['status'] = entity.status;
	return data;
}

GetConcertByIdDataAttributes $GetConcertByIdDataAttributesFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataAttributes getConcertByIdDataAttributes = GetConcertByIdDataAttributes();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataAttributes.id = id;
	}
	final String? code = jsonConvert.convert<String>(json['code']);
	if (code != null) {
		getConcertByIdDataAttributes.code = code;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataAttributes.name = name;
	}
	final List<GetConcertByIdDataAttributesItems>? items = jsonConvert.convertListNotNull<GetConcertByIdDataAttributesItems>(json['items']);
	if (items != null) {
		getConcertByIdDataAttributes.items = items;
	}
	return getConcertByIdDataAttributes;
}

Map<String, dynamic> $GetConcertByIdDataAttributesToJson(GetConcertByIdDataAttributes entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	data['items'] =  entity.items?.map((v) => v.toJson()).toList();
	return data;
}

GetConcertByIdDataAttributesItems $GetConcertByIdDataAttributesItemsFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataAttributesItems getConcertByIdDataAttributesItems = GetConcertByIdDataAttributesItems();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataAttributesItems.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataAttributesItems.name = name;
	}
	final dynamic? value = jsonConvert.convert<dynamic>(json['value']);
	if (value != null) {
		getConcertByIdDataAttributesItems.value = value;
	}
	final GetConcertByIdDataAttributesItemsImage? image = jsonConvert.convert<GetConcertByIdDataAttributesItemsImage>(json['image']);
	if (image != null) {
		getConcertByIdDataAttributesItems.image = image;
	}
	return getConcertByIdDataAttributesItems;
}

Map<String, dynamic> $GetConcertByIdDataAttributesItemsToJson(GetConcertByIdDataAttributesItems entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['value'] = entity.value;
	data['image'] = entity.image?.toJson();
	return data;
}

GetConcertByIdDataAttributesItemsImage $GetConcertByIdDataAttributesItemsImageFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataAttributesItemsImage getConcertByIdDataAttributesItemsImage = GetConcertByIdDataAttributesItemsImage();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getConcertByIdDataAttributesItemsImage.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertByIdDataAttributesItemsImage.storeId = storeId;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getConcertByIdDataAttributesItemsImage.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataAttributesItemsImage.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getConcertByIdDataAttributesItemsImage.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getConcertByIdDataAttributesItemsImage.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getConcertByIdDataAttributesItemsImage.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getConcertByIdDataAttributesItemsImage.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getConcertByIdDataAttributesItemsImage.url = url;
	}
	return getConcertByIdDataAttributesItemsImage;
}

Map<String, dynamic> $GetConcertByIdDataAttributesItemsImageToJson(GetConcertByIdDataAttributesItemsImage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

GetConcertByIdDataImages $GetConcertByIdDataImagesFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataImages getConcertByIdDataImages = GetConcertByIdDataImages();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getConcertByIdDataImages.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertByIdDataImages.storeId = storeId;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getConcertByIdDataImages.tag = tag;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getConcertByIdDataImages.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertByIdDataImages.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getConcertByIdDataImages.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getConcertByIdDataImages.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getConcertByIdDataImages.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getConcertByIdDataImages.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getConcertByIdDataImages.url = url;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getConcertByIdDataImages.position = position;
	}
	return getConcertByIdDataImages;
}

Map<String, dynamic> $GetConcertByIdDataImagesToJson(GetConcertByIdDataImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

GetConcertByIdDataPublishStatus $GetConcertByIdDataPublishStatusFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataPublishStatus getConcertByIdDataPublishStatus = GetConcertByIdDataPublishStatus();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertByIdDataPublishStatus.id = id;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		getConcertByIdDataPublishStatus.text = text;
	}
	return getConcertByIdDataPublishStatus;
}

Map<String, dynamic> $GetConcertByIdDataPublishStatusToJson(GetConcertByIdDataPublishStatus entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

GetConcertByIdDataSettings $GetConcertByIdDataSettingsFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettings getConcertByIdDataSettings = GetConcertByIdDataSettings();
	final GetConcertByIdDataSettingsPayment? payment = jsonConvert.convert<GetConcertByIdDataSettingsPayment>(json['payment']);
	if (payment != null) {
		getConcertByIdDataSettings.payment = payment;
	}
	return getConcertByIdDataSettings;
}

Map<String, dynamic> $GetConcertByIdDataSettingsToJson(GetConcertByIdDataSettings entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['payment'] = entity.payment?.toJson();
	return data;
}

GetConcertByIdDataSettingsPayment $GetConcertByIdDataSettingsPaymentFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPayment getConcertByIdDataSettingsPayment = GetConcertByIdDataSettingsPayment();
	final GetConcertByIdDataSettingsPaymentFee? fee = jsonConvert.convert<GetConcertByIdDataSettingsPaymentFee>(json['fee']);
	if (fee != null) {
		getConcertByIdDataSettingsPayment.fee = fee;
	}
	final GetConcertByIdDataSettingsPaymentMethod? method = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethod>(json['method']);
	if (method != null) {
		getConcertByIdDataSettingsPayment.method = method;
	}
	final GetConcertByIdDataSettingsPaymentSetting? setting = jsonConvert.convert<GetConcertByIdDataSettingsPaymentSetting>(json['setting']);
	if (setting != null) {
		getConcertByIdDataSettingsPayment.setting = setting;
	}
	return getConcertByIdDataSettingsPayment;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentToJson(GetConcertByIdDataSettingsPayment entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['fee'] = entity.fee?.toJson();
	data['method'] = entity.method?.toJson();
	data['setting'] = entity.setting?.toJson();
	return data;
}

GetConcertByIdDataSettingsPaymentFee $GetConcertByIdDataSettingsPaymentFeeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentFee getConcertByIdDataSettingsPaymentFee = GetConcertByIdDataSettingsPaymentFee();
	final GetConcertByIdDataSettingsPaymentFeeServiceFee? serviceFee = jsonConvert.convert<GetConcertByIdDataSettingsPaymentFeeServiceFee>(json['service_fee']);
	if (serviceFee != null) {
		getConcertByIdDataSettingsPaymentFee.serviceFee = serviceFee;
	}
	final GetConcertByIdDataSettingsPaymentFeePaymentFee? paymentFee = jsonConvert.convert<GetConcertByIdDataSettingsPaymentFeePaymentFee>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentFee.paymentFee = paymentFee;
	}
	final GetConcertByIdDataSettingsPaymentFeeTaxFee? taxFee = jsonConvert.convert<GetConcertByIdDataSettingsPaymentFeeTaxFee>(json['tax_fee']);
	if (taxFee != null) {
		getConcertByIdDataSettingsPaymentFee.taxFee = taxFee;
	}
	return getConcertByIdDataSettingsPaymentFee;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentFeeToJson(GetConcertByIdDataSettingsPaymentFee entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['service_fee'] = entity.serviceFee?.toJson();
	data['payment_fee'] = entity.paymentFee?.toJson();
	data['tax_fee'] = entity.taxFee?.toJson();
	return data;
}

GetConcertByIdDataSettingsPaymentFeeServiceFee $GetConcertByIdDataSettingsPaymentFeeServiceFeeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentFeeServiceFee getConcertByIdDataSettingsPaymentFeeServiceFee = GetConcertByIdDataSettingsPaymentFeeServiceFee();
	final int? organizerPay = jsonConvert.convert<int>(json['organizer_pay']);
	if (organizerPay != null) {
		getConcertByIdDataSettingsPaymentFeeServiceFee.organizerPay = organizerPay;
	}
	final int? customerPay = jsonConvert.convert<int>(json['customer_pay']);
	if (customerPay != null) {
		getConcertByIdDataSettingsPaymentFeeServiceFee.customerPay = customerPay;
	}
	return getConcertByIdDataSettingsPaymentFeeServiceFee;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentFeeServiceFeeToJson(GetConcertByIdDataSettingsPaymentFeeServiceFee entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['organizer_pay'] = entity.organizerPay;
	data['customer_pay'] = entity.customerPay;
	return data;
}

GetConcertByIdDataSettingsPaymentFeePaymentFee $GetConcertByIdDataSettingsPaymentFeePaymentFeeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentFeePaymentFee getConcertByIdDataSettingsPaymentFeePaymentFee = GetConcertByIdDataSettingsPaymentFeePaymentFee();
	final int? organizerPay = jsonConvert.convert<int>(json['organizer_pay']);
	if (organizerPay != null) {
		getConcertByIdDataSettingsPaymentFeePaymentFee.organizerPay = organizerPay;
	}
	final int? customerPay = jsonConvert.convert<int>(json['customer_pay']);
	if (customerPay != null) {
		getConcertByIdDataSettingsPaymentFeePaymentFee.customerPay = customerPay;
	}
	return getConcertByIdDataSettingsPaymentFeePaymentFee;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentFeePaymentFeeToJson(GetConcertByIdDataSettingsPaymentFeePaymentFee entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['organizer_pay'] = entity.organizerPay;
	data['customer_pay'] = entity.customerPay;
	return data;
}

GetConcertByIdDataSettingsPaymentFeeTaxFee $GetConcertByIdDataSettingsPaymentFeeTaxFeeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentFeeTaxFee getConcertByIdDataSettingsPaymentFeeTaxFee = GetConcertByIdDataSettingsPaymentFeeTaxFee();
	final bool? organizerPay = jsonConvert.convert<bool>(json['organizer_pay']);
	if (organizerPay != null) {
		getConcertByIdDataSettingsPaymentFeeTaxFee.organizerPay = organizerPay;
	}
	final bool? customerPay = jsonConvert.convert<bool>(json['customer_pay']);
	if (customerPay != null) {
		getConcertByIdDataSettingsPaymentFeeTaxFee.customerPay = customerPay;
	}
	return getConcertByIdDataSettingsPaymentFeeTaxFee;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentFeeTaxFeeToJson(GetConcertByIdDataSettingsPaymentFeeTaxFee entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['organizer_pay'] = entity.organizerPay;
	data['customer_pay'] = entity.customerPay;
	return data;
}

GetConcertByIdDataSettingsPaymentMethod $GetConcertByIdDataSettingsPaymentMethodFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethod getConcertByIdDataSettingsPaymentMethod = GetConcertByIdDataSettingsPaymentMethod();
	final GetConcertByIdDataSettingsPaymentMethodCcw? ccw = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodCcw>(json['ccw']);
	if (ccw != null) {
		getConcertByIdDataSettingsPaymentMethod.ccw = ccw;
	}
	final GetConcertByIdDataSettingsPaymentMethodIbanking? ibanking = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodIbanking>(json['ibanking']);
	if (ibanking != null) {
		getConcertByIdDataSettingsPaymentMethod.ibanking = ibanking;
	}
	final GetConcertByIdDataSettingsPaymentMethodBill? bill = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodBill>(json['bill']);
	if (bill != null) {
		getConcertByIdDataSettingsPaymentMethod.bill = bill;
	}
	final GetConcertByIdDataSettingsPaymentMethodBanktrans? banktrans = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodBanktrans>(json['banktrans']);
	if (banktrans != null) {
		getConcertByIdDataSettingsPaymentMethod.banktrans = banktrans;
	}
	final GetConcertByIdDataSettingsPaymentMethodInstallment? installment = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodInstallment>(json['installment']);
	if (installment != null) {
		getConcertByIdDataSettingsPaymentMethod.installment = installment;
	}
	final GetConcertByIdDataSettingsPaymentMethodPromptpay? promptpay = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodPromptpay>(json['promptpay']);
	if (promptpay != null) {
		getConcertByIdDataSettingsPaymentMethod.promptpay = promptpay;
	}
	final GetConcertByIdDataSettingsPaymentMethodLinepay? linepay = jsonConvert.convert<GetConcertByIdDataSettingsPaymentMethodLinepay>(json['linepay']);
	if (linepay != null) {
		getConcertByIdDataSettingsPaymentMethod.linepay = linepay;
	}
	return getConcertByIdDataSettingsPaymentMethod;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodToJson(GetConcertByIdDataSettingsPaymentMethod entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['ccw'] = entity.ccw?.toJson();
	data['ibanking'] = entity.ibanking?.toJson();
	data['bill'] = entity.bill?.toJson();
	data['banktrans'] = entity.banktrans?.toJson();
	data['installment'] = entity.installment?.toJson();
	data['promptpay'] = entity.promptpay?.toJson();
	data['linepay'] = entity.linepay?.toJson();
	return data;
}

GetConcertByIdDataSettingsPaymentMethodCcw $GetConcertByIdDataSettingsPaymentMethodCcwFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodCcw getConcertByIdDataSettingsPaymentMethodCcw = GetConcertByIdDataSettingsPaymentMethodCcw();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodCcw.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodCcw.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodCcw;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodCcwToJson(GetConcertByIdDataSettingsPaymentMethodCcw entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodIbanking $GetConcertByIdDataSettingsPaymentMethodIbankingFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodIbanking getConcertByIdDataSettingsPaymentMethodIbanking = GetConcertByIdDataSettingsPaymentMethodIbanking();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodIbanking.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodIbanking.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodIbanking;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodIbankingToJson(GetConcertByIdDataSettingsPaymentMethodIbanking entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodBill $GetConcertByIdDataSettingsPaymentMethodBillFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodBill getConcertByIdDataSettingsPaymentMethodBill = GetConcertByIdDataSettingsPaymentMethodBill();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodBill.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodBill.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodBill;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodBillToJson(GetConcertByIdDataSettingsPaymentMethodBill entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodBanktrans $GetConcertByIdDataSettingsPaymentMethodBanktransFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodBanktrans getConcertByIdDataSettingsPaymentMethodBanktrans = GetConcertByIdDataSettingsPaymentMethodBanktrans();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodBanktrans.status = status;
	}
	final int? expired = jsonConvert.convert<int>(json['expired']);
	if (expired != null) {
		getConcertByIdDataSettingsPaymentMethodBanktrans.expired = expired;
	}
	final dynamic? storeBankId = jsonConvert.convert<dynamic>(json['store_bank_id']);
	if (storeBankId != null) {
		getConcertByIdDataSettingsPaymentMethodBanktrans.storeBankId = storeBankId;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodBanktrans.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodBanktrans;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodBanktransToJson(GetConcertByIdDataSettingsPaymentMethodBanktrans entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['expired'] = entity.expired;
	data['store_bank_id'] = entity.storeBankId;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodInstallment $GetConcertByIdDataSettingsPaymentMethodInstallmentFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodInstallment getConcertByIdDataSettingsPaymentMethodInstallment = GetConcertByIdDataSettingsPaymentMethodInstallment();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodInstallment.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodInstallment.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodInstallment;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodInstallmentToJson(GetConcertByIdDataSettingsPaymentMethodInstallment entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodPromptpay $GetConcertByIdDataSettingsPaymentMethodPromptpayFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodPromptpay getConcertByIdDataSettingsPaymentMethodPromptpay = GetConcertByIdDataSettingsPaymentMethodPromptpay();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodPromptpay.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodPromptpay.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodPromptpay;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodPromptpayToJson(GetConcertByIdDataSettingsPaymentMethodPromptpay entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentMethodLinepay $GetConcertByIdDataSettingsPaymentMethodLinepayFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentMethodLinepay getConcertByIdDataSettingsPaymentMethodLinepay = GetConcertByIdDataSettingsPaymentMethodLinepay();
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertByIdDataSettingsPaymentMethodLinepay.status = status;
	}
	final bool? paymentFee = jsonConvert.convert<bool>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentMethodLinepay.paymentFee = paymentFee;
	}
	return getConcertByIdDataSettingsPaymentMethodLinepay;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentMethodLinepayToJson(GetConcertByIdDataSettingsPaymentMethodLinepay entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

GetConcertByIdDataSettingsPaymentSetting $GetConcertByIdDataSettingsPaymentSettingFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentSetting getConcertByIdDataSettingsPaymentSetting = GetConcertByIdDataSettingsPaymentSetting();
	final GetConcertByIdDataSettingsPaymentSettingFree? free = jsonConvert.convert<GetConcertByIdDataSettingsPaymentSettingFree>(json['free']);
	if (free != null) {
		getConcertByIdDataSettingsPaymentSetting.free = free;
	}
	final GetConcertByIdDataSettingsPaymentSettingPaid? paid = jsonConvert.convert<GetConcertByIdDataSettingsPaymentSettingPaid>(json['paid']);
	if (paid != null) {
		getConcertByIdDataSettingsPaymentSetting.paid = paid;
	}
	final String? ga = jsonConvert.convert<String>(json['ga']);
	if (ga != null) {
		getConcertByIdDataSettingsPaymentSetting.ga = ga;
	}
	final String? fbPixel = jsonConvert.convert<String>(json['fb_pixel']);
	if (fbPixel != null) {
		getConcertByIdDataSettingsPaymentSetting.fbPixel = fbPixel;
	}
	return getConcertByIdDataSettingsPaymentSetting;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentSettingToJson(GetConcertByIdDataSettingsPaymentSetting entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['free'] = entity.free?.toJson();
	data['paid'] = entity.paid?.toJson();
	data['ga'] = entity.ga;
	data['fb_pixel'] = entity.fbPixel;
	return data;
}

GetConcertByIdDataSettingsPaymentSettingFree $GetConcertByIdDataSettingsPaymentSettingFreeFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentSettingFree getConcertByIdDataSettingsPaymentSettingFree = GetConcertByIdDataSettingsPaymentSettingFree();
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getConcertByIdDataSettingsPaymentSettingFree.total = total;
	}
	final int? paymentFee = jsonConvert.convert<int>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentSettingFree.paymentFee = paymentFee;
	}
	final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
	if (serviceFee != null) {
		getConcertByIdDataSettingsPaymentSettingFree.serviceFee = serviceFee;
	}
	return getConcertByIdDataSettingsPaymentSettingFree;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentSettingFreeToJson(GetConcertByIdDataSettingsPaymentSettingFree entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['total'] = entity.total;
	data['payment_fee'] = entity.paymentFee;
	data['service_fee'] = entity.serviceFee;
	return data;
}

GetConcertByIdDataSettingsPaymentSettingPaid $GetConcertByIdDataSettingsPaymentSettingPaidFromJson(Map<String, dynamic> json) {
	final GetConcertByIdDataSettingsPaymentSettingPaid getConcertByIdDataSettingsPaymentSettingPaid = GetConcertByIdDataSettingsPaymentSettingPaid();
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getConcertByIdDataSettingsPaymentSettingPaid.total = total;
	}
	final int? paymentFee = jsonConvert.convert<int>(json['payment_fee']);
	if (paymentFee != null) {
		getConcertByIdDataSettingsPaymentSettingPaid.paymentFee = paymentFee;
	}
	final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
	if (serviceFee != null) {
		getConcertByIdDataSettingsPaymentSettingPaid.serviceFee = serviceFee;
	}
	return getConcertByIdDataSettingsPaymentSettingPaid;
}

Map<String, dynamic> $GetConcertByIdDataSettingsPaymentSettingPaidToJson(GetConcertByIdDataSettingsPaymentSettingPaid entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['total'] = entity.total;
	data['payment_fee'] = entity.paymentFee;
	data['service_fee'] = entity.serviceFee;
	return data;
}

GetConcertByIdBench $GetConcertByIdBenchFromJson(Map<String, dynamic> json) {
	final GetConcertByIdBench getConcertByIdBench = GetConcertByIdBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getConcertByIdBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getConcertByIdBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getConcertByIdBench.format = format;
	}
	return getConcertByIdBench;
}

Map<String, dynamic> $GetConcertByIdBenchToJson(GetConcertByIdBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}