import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_bank_data_entity.dart';

GetBankDataEntity $GetBankDataEntityFromJson(Map<String, dynamic> json) {
  final GetBankDataEntity getBankDataEntity = GetBankDataEntity();
  final GetBankDataData? data =
      jsonConvert.convert<GetBankDataData>(json['data']);
  if (data != null) {
    getBankDataEntity.data = data;
  }
  final GetBankDataBench? bench =
      jsonConvert.convert<GetBankDataBench>(json['bench']);
  if (bench != null) {
    getBankDataEntity.bench = bench;
  }
  return getBankDataEntity;
}

Map<String, dynamic> $GetBankDataEntityToJson(GetBankDataEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetBankDataData $GetBankDataDataFromJson(Map<String, dynamic> json) {
  final GetBankDataData getBankDataData = GetBankDataData();
  final List<GetBankDataDataRecord>? record =
      jsonConvert.convertListNotNull<GetBankDataDataRecord>(json['record']);
  if (record != null) {
    getBankDataData.record = record;
  }
  return getBankDataData;
}

Map<String, dynamic> $GetBankDataDataToJson(GetBankDataData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetBankDataDataRecord $GetBankDataDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetBankDataDataRecord getBankDataDataRecord = GetBankDataDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getBankDataDataRecord.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getBankDataDataRecord.storeId = storeId;
  }
  final GetBankDataDataRecordType? type =
      jsonConvert.convert<GetBankDataDataRecordType>(json['type']);
  if (type != null) {
    getBankDataDataRecord.type = type;
  }
  final GetBankDataDataRecordAccount? account =
      jsonConvert.convert<GetBankDataDataRecordAccount>(json['account']);
  if (account != null) {
    getBankDataDataRecord.account = account;
  }
  final GetBankDataDataRecordPaymentBank? paymentBank = jsonConvert
      .convert<GetBankDataDataRecordPaymentBank>(json['payment_bank']);
  if (paymentBank != null) {
    getBankDataDataRecord.paymentBank = paymentBank;
  }
  final dynamic? image = jsonConvert.convert<dynamic>(json['image']);
  if (image != null) {
    getBankDataDataRecord.image = image;
  }
  final dynamic? staff = jsonConvert.convert<dynamic>(json['staff']);
  if (staff != null) {
    getBankDataDataRecord.staff = staff;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getBankDataDataRecord.remark = remark;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    getBankDataDataRecord.status = status;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getBankDataDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getBankDataDataRecord.updatedAt = updatedAt;
  }
  return getBankDataDataRecord;
}

Map<String, dynamic> $GetBankDataDataRecordToJson(
    GetBankDataDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['type'] = entity.type?.toJson();
  data['account'] = entity.account?.toJson();
  data['payment_bank'] = entity.paymentBank?.toJson();
  data['image'] = entity.image;
  data['staff'] = entity.staff;
  data['remark'] = entity.remark;
  data['status'] = entity.status;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetBankDataDataRecordType $GetBankDataDataRecordTypeFromJson(
    Map<String, dynamic> json) {
  final GetBankDataDataRecordType getBankDataDataRecordType =
      GetBankDataDataRecordType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getBankDataDataRecordType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getBankDataDataRecordType.text = text;
  }
  return getBankDataDataRecordType;
}

Map<String, dynamic> $GetBankDataDataRecordTypeToJson(
    GetBankDataDataRecordType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetBankDataDataRecordAccount $GetBankDataDataRecordAccountFromJson(
    Map<String, dynamic> json) {
  final GetBankDataDataRecordAccount getBankDataDataRecordAccount =
      GetBankDataDataRecordAccount();
  final GetBankDataDataRecordAccountType? type =
      jsonConvert.convert<GetBankDataDataRecordAccountType>(json['type']);
  if (type != null) {
    getBankDataDataRecordAccount.type = type;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getBankDataDataRecordAccount.name = name;
  }
  final String? number = jsonConvert.convert<String>(json['number']);
  if (number != null) {
    getBankDataDataRecordAccount.number = number;
  }
  final String? branch = jsonConvert.convert<String>(json['branch']);
  if (branch != null) {
    getBankDataDataRecordAccount.branch = branch;
  }
  return getBankDataDataRecordAccount;
}

Map<String, dynamic> $GetBankDataDataRecordAccountToJson(
    GetBankDataDataRecordAccount entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['type'] = entity.type?.toJson();
  data['name'] = entity.name;
  data['number'] = entity.number;
  data['branch'] = entity.branch;
  return data;
}

GetBankDataDataRecordAccountType $GetBankDataDataRecordAccountTypeFromJson(
    Map<String, dynamic> json) {
  final GetBankDataDataRecordAccountType getBankDataDataRecordAccountType =
      GetBankDataDataRecordAccountType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getBankDataDataRecordAccountType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getBankDataDataRecordAccountType.text = text;
  }
  return getBankDataDataRecordAccountType;
}

Map<String, dynamic> $GetBankDataDataRecordAccountTypeToJson(
    GetBankDataDataRecordAccountType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetBankDataDataRecordPaymentBank $GetBankDataDataRecordPaymentBankFromJson(
    Map<String, dynamic> json) {
  final GetBankDataDataRecordPaymentBank getBankDataDataRecordPaymentBank =
      GetBankDataDataRecordPaymentBank();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getBankDataDataRecordPaymentBank.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getBankDataDataRecordPaymentBank.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getBankDataDataRecordPaymentBank.name = name;
  }
  return getBankDataDataRecordPaymentBank;
}

Map<String, dynamic> $GetBankDataDataRecordPaymentBankToJson(
    GetBankDataDataRecordPaymentBank entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  return data;
}

GetBankDataBench $GetBankDataBenchFromJson(Map<String, dynamic> json) {
  final GetBankDataBench getBankDataBench = GetBankDataBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getBankDataBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getBankDataBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getBankDataBench.format = format;
  }
  return getBankDataBench;
}

Map<String, dynamic> $GetBankDataBenchToJson(GetBankDataBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
