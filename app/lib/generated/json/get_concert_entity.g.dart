import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_concert_entity.dart';

GetConcertEntity $GetConcertEntityFromJson(Map<String, dynamic> json) {
	final GetConcertEntity getConcertEntity = GetConcertEntity();
	final GetConcertData? data = jsonConvert.convert<GetConcertData>(json['data']);
	if (data != null) {
		getConcertEntity.data = data;
	}
	final GetConcertBench? bench = jsonConvert.convert<GetConcertBench>(json['bench']);
	if (bench != null) {
		getConcertEntity.bench = bench;
	}
	return getConcertEntity;
}

Map<String, dynamic> $GetConcertEntityToJson(GetConcertEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetConcertData $GetConcertDataFromJson(Map<String, dynamic> json) {
	final GetConcertData getConcertData = GetConcertData();
	final GetConcertDataPagination? pagination = jsonConvert.convert<GetConcertDataPagination>(json['pagination']);
	if (pagination != null) {
		getConcertData.pagination = pagination;
	}
	final List<GetConcertDataRecord>? record = jsonConvert.convertListNotNull<GetConcertDataRecord>(json['record']);
	if (record != null) {
		getConcertData.record = record;
	}
	return getConcertData;
}

Map<String, dynamic> $GetConcertDataToJson(GetConcertData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['pagination'] = entity.pagination?.toJson();
	data['record'] =  entity.record?.map((v) => v.toJson()).toList();
	return data;
}

GetConcertDataPagination $GetConcertDataPaginationFromJson(Map<String, dynamic> json) {
	final GetConcertDataPagination getConcertDataPagination = GetConcertDataPagination();
	final int? currentPage = jsonConvert.convert<int>(json['current_page']);
	if (currentPage != null) {
		getConcertDataPagination.currentPage = currentPage;
	}
	final int? lastPage = jsonConvert.convert<int>(json['last_page']);
	if (lastPage != null) {
		getConcertDataPagination.lastPage = lastPage;
	}
	final int? limit = jsonConvert.convert<int>(json['limit']);
	if (limit != null) {
		getConcertDataPagination.limit = limit;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getConcertDataPagination.total = total;
	}
	return getConcertDataPagination;
}

Map<String, dynamic> $GetConcertDataPaginationToJson(GetConcertDataPagination entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

GetConcertDataRecord $GetConcertDataRecordFromJson(Map<String, dynamic> json) {
	final GetConcertDataRecord getConcertDataRecord = GetConcertDataRecord();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertDataRecord.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertDataRecord.name = name;
	}
	final GetConcertDataRecordShowTime? showTime = jsonConvert.convert<GetConcertDataRecordShowTime>(json['show_time']);
	if (showTime != null) {
		getConcertDataRecord.showTime = showTime;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getConcertDataRecord.status = status;
	}
	final List<GetConcertDataRecordImages>? images = jsonConvert.convertListNotNull<GetConcertDataRecordImages>(json['images']);
	if (images != null) {
		getConcertDataRecord.images = images;
	}
	final GetConcertDataRecordPublishStatus? publishStatus = jsonConvert.convert<GetConcertDataRecordPublishStatus>(json['publish_status']);
	if (publishStatus != null) {
		getConcertDataRecord.publishStatus = publishStatus;
	}
	final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
	if (remark != null) {
		getConcertDataRecord.remark = remark;
	}
	final bool? hasVariant = jsonConvert.convert<bool>(json['has_variant']);
	if (hasVariant != null) {
		getConcertDataRecord.hasVariant = hasVariant;
	}
	final int? ticketCount = jsonConvert.convert<int>(json['ticket_count']);
	if (ticketCount != null) {
		getConcertDataRecord.ticketCount = ticketCount;
	}
	return getConcertDataRecord;
}

Map<String, dynamic> $GetConcertDataRecordToJson(GetConcertDataRecord entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['show_time'] = entity.showTime?.toJson();
	data['status'] = entity.status;
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['publish_status'] = entity.publishStatus?.toJson();
	data['remark'] = entity.remark;
	data['has_variant'] = entity.hasVariant;
	data['ticket_count'] = entity.ticketCount;
	return data;
}

GetConcertDataRecordShowTime $GetConcertDataRecordShowTimeFromJson(Map<String, dynamic> json) {
	final GetConcertDataRecordShowTime getConcertDataRecordShowTime = GetConcertDataRecordShowTime();
	final String? start = jsonConvert.convert<String>(json['start']);
	if (start != null) {
		getConcertDataRecordShowTime.start = start;
	}
	final String? end = jsonConvert.convert<String>(json['end']);
	if (end != null) {
		getConcertDataRecordShowTime.end = end;
	}
	final String? textFull = jsonConvert.convert<String>(json['text_full']);
	if (textFull != null) {
		getConcertDataRecordShowTime.textFull = textFull;
	}
	final String? textShort = jsonConvert.convert<String>(json['text_short']);
	if (textShort != null) {
		getConcertDataRecordShowTime.textShort = textShort;
	}
	final String? textShortDate = jsonConvert.convert<String>(json['text_short_date']);
	if (textShortDate != null) {
		getConcertDataRecordShowTime.textShortDate = textShortDate;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		getConcertDataRecordShowTime.status = status;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		getConcertDataRecordShowTime.statusText = statusText;
	}
	return getConcertDataRecordShowTime;
}

Map<String, dynamic> $GetConcertDataRecordShowTimeToJson(GetConcertDataRecordShowTime entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['start'] = entity.start;
	data['end'] = entity.end;
	data['text_full'] = entity.textFull;
	data['text_short'] = entity.textShort;
	data['text_short_date'] = entity.textShortDate;
	data['status'] = entity.status;
	data['status_text'] = entity.statusText;
	return data;
}

GetConcertDataRecordImages $GetConcertDataRecordImagesFromJson(Map<String, dynamic> json) {
	final GetConcertDataRecordImages getConcertDataRecordImages = GetConcertDataRecordImages();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getConcertDataRecordImages.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertDataRecordImages.storeId = storeId;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getConcertDataRecordImages.tag = tag;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getConcertDataRecordImages.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertDataRecordImages.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getConcertDataRecordImages.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getConcertDataRecordImages.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getConcertDataRecordImages.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getConcertDataRecordImages.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getConcertDataRecordImages.url = url;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getConcertDataRecordImages.position = position;
	}
	return getConcertDataRecordImages;
}

Map<String, dynamic> $GetConcertDataRecordImagesToJson(GetConcertDataRecordImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

GetConcertDataRecordPublishStatus $GetConcertDataRecordPublishStatusFromJson(Map<String, dynamic> json) {
	final GetConcertDataRecordPublishStatus getConcertDataRecordPublishStatus = GetConcertDataRecordPublishStatus();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertDataRecordPublishStatus.id = id;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		getConcertDataRecordPublishStatus.text = text;
	}
	return getConcertDataRecordPublishStatus;
}

Map<String, dynamic> $GetConcertDataRecordPublishStatusToJson(GetConcertDataRecordPublishStatus entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

GetConcertBench $GetConcertBenchFromJson(Map<String, dynamic> json) {
	final GetConcertBench getConcertBench = GetConcertBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getConcertBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getConcertBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getConcertBench.format = format;
	}
	return getConcertBench;
}

Map<String, dynamic> $GetConcertBenchToJson(GetConcertBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}