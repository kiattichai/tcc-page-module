import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_concert_module/model/save_concert_entity.dart';

SaveConcertEntity $SaveConcertEntityFromJson(Map<String, dynamic> json) {
  final SaveConcertEntity saveConcertEntity = SaveConcertEntity();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    saveConcertEntity.id = id;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    saveConcertEntity.type = type;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    saveConcertEntity.slug = slug;
  }
  final int? venueId = jsonConvert.convert<int>(json['venue_id']);
  if (venueId != null) {
    saveConcertEntity.venueId = venueId;
  }
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    saveConcertEntity.storeId = storeId;
  }
  final SaveConcertName? name =
      jsonConvert.convert<SaveConcertName>(json['name']);
  if (name != null) {
    saveConcertEntity.name = name;
  }
  final SaveConcertDescription? description =
      jsonConvert.convert<SaveConcertDescription>(json['description']);
  if (description != null) {
    saveConcertEntity.description = description;
  }
  final SaveConcertSeoName? seoName =
      jsonConvert.convert<SaveConcertSeoName>(json['seo_name']);
  if (seoName != null) {
    saveConcertEntity.seoName = seoName;
  }
  final SaveConcertSeoDescription? seoDescription =
      jsonConvert.convert<SaveConcertSeoDescription>(json['seo_description']);
  if (seoDescription != null) {
    saveConcertEntity.seoDescription = seoDescription;
  }
  final String? showStart = jsonConvert.convert<String>(json['show_start']);
  if (showStart != null) {
    saveConcertEntity.showStart = showStart;
  }
  final String? showEnd = jsonConvert.convert<String>(json['show_end']);
  if (showEnd != null) {
    saveConcertEntity.showEnd = showEnd;
  }
  final List<SaveConcertAttributes>? attributes =
      jsonConvert.convertListNotNull<SaveConcertAttributes>(json['attributes']);
  if (attributes != null) {
    saveConcertEntity.attributes = attributes;
  }
  final List<SaveConcertImages>? images =
      jsonConvert.convertListNotNull<SaveConcertImages>(json['images']);
  if (images != null) {
    saveConcertEntity.images = images;
  }
  final int? createdBy = jsonConvert.convert<int>(json['created_by']);
  if (createdBy != null) {
    saveConcertEntity.createdBy = createdBy;
  }
  final String? updatedBy = jsonConvert.convert<String>(json['updated_by']);
  if (updatedBy != null) {
    saveConcertEntity.updatedBy = updatedBy;
  }
  return saveConcertEntity;
}

Map<String, dynamic> $SaveConcertEntityToJson(SaveConcertEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['type'] = entity.type;
  data['slug'] = entity.slug;
  data['venue_id'] = entity.venueId;
  data['store_id'] = entity.storeId;
  data['name'] = entity.name?.toJson();
  data['description'] = entity.description?.toJson();
  data['seo_name'] = entity.seoName?.toJson();
  data['seo_description'] = entity.seoDescription?.toJson();
  data['show_start'] = entity.showStart;
  data['show_end'] = entity.showEnd;
  data['attributes'] = entity.attributes?.map((v) => v.toJson()).toList();
  data['images'] = entity.images?.map((v) => v.toJson()).toList();
  data['created_by'] = entity.createdBy;
  data['updated_by'] = entity.updatedBy;
  return data;
}

SaveConcertName $SaveConcertNameFromJson(Map<String, dynamic> json) {
  final SaveConcertName saveConcertName = SaveConcertName();
  final String? th = jsonConvert.convert<String>(json['th']);
  if (th != null) {
    saveConcertName.th = th;
  }
  final String? en = jsonConvert.convert<String>(json['en']);
  if (en != null) {
    saveConcertName.en = en;
  }
  return saveConcertName;
}

Map<String, dynamic> $SaveConcertNameToJson(SaveConcertName entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['th'] = entity.th;
  data['en'] = entity.en;
  return data;
}

SaveConcertDescription $SaveConcertDescriptionFromJson(
    Map<String, dynamic> json) {
  final SaveConcertDescription saveConcertDescription =
      SaveConcertDescription();
  final String? th = jsonConvert.convert<String>(json['th']);
  if (th != null) {
    saveConcertDescription.th = th;
  }
  final String? en = jsonConvert.convert<String>(json['en']);
  if (en != null) {
    saveConcertDescription.en = en;
  }
  return saveConcertDescription;
}

Map<String, dynamic> $SaveConcertDescriptionToJson(
    SaveConcertDescription entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['th'] = entity.th;
  data['en'] = entity.en;
  return data;
}

SaveConcertSeoName $SaveConcertSeoNameFromJson(Map<String, dynamic> json) {
  final SaveConcertSeoName saveConcertSeoName = SaveConcertSeoName();
  final String? th = jsonConvert.convert<String>(json['th']);
  if (th != null) {
    saveConcertSeoName.th = th;
  }
  final String? en = jsonConvert.convert<String>(json['en']);
  if (en != null) {
    saveConcertSeoName.en = en;
  }
  return saveConcertSeoName;
}

Map<String, dynamic> $SaveConcertSeoNameToJson(SaveConcertSeoName entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['th'] = entity.th;
  data['en'] = entity.en;
  return data;
}

SaveConcertSeoDescription $SaveConcertSeoDescriptionFromJson(
    Map<String, dynamic> json) {
  final SaveConcertSeoDescription saveConcertSeoDescription =
      SaveConcertSeoDescription();
  final String? th = jsonConvert.convert<String>(json['th']);
  if (th != null) {
    saveConcertSeoDescription.th = th;
  }
  final String? en = jsonConvert.convert<String>(json['en']);
  if (en != null) {
    saveConcertSeoDescription.en = en;
  }
  return saveConcertSeoDescription;
}

Map<String, dynamic> $SaveConcertSeoDescriptionToJson(
    SaveConcertSeoDescription entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['th'] = entity.th;
  data['en'] = entity.en;
  return data;
}

SaveConcertAttributes $SaveConcertAttributesFromJson(
    Map<String, dynamic> json) {
  final SaveConcertAttributes saveConcertAttributes = SaveConcertAttributes();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    saveConcertAttributes.id = id;
  }
  final int? valueId = jsonConvert.convert<int>(json['value_id']);
  if (valueId != null) {
    saveConcertAttributes.valueId = valueId;
  }
  final String? valueText = jsonConvert.convert<String>(json['value_text']);
  if (valueText != null) {
    saveConcertAttributes.valueText = valueText;
  }
  return saveConcertAttributes;
}

Map<String, dynamic> $SaveConcertAttributesToJson(
    SaveConcertAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['value_id'] = entity.valueId;
  data['value_text'] = entity.valueText;
  return data;
}

SaveConcertImages $SaveConcertImagesFromJson(Map<String, dynamic> json) {
  final SaveConcertImages saveConcertImages = SaveConcertImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    saveConcertImages.id = id;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    saveConcertImages.tag = tag;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    saveConcertImages.position = position;
  }
  return saveConcertImages;
}

Map<String, dynamic> $SaveConcertImagesToJson(SaveConcertImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['tag'] = entity.tag;
  data['position'] = entity.position;
  return data;
}
