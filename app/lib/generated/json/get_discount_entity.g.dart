import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_discount_entity.dart';

GetDiscountEntity $GetDiscountEntityFromJson(Map<String, dynamic> json) {
  final GetDiscountEntity getDiscountEntity = GetDiscountEntity();
  final GetDiscountData? data =
      jsonConvert.convert<GetDiscountData>(json['data']);
  if (data != null) {
    getDiscountEntity.data = data;
  }
  final GetDiscountBench? bench =
      jsonConvert.convert<GetDiscountBench>(json['bench']);
  if (bench != null) {
    getDiscountEntity.bench = bench;
  }
  return getDiscountEntity;
}

Map<String, dynamic> $GetDiscountEntityToJson(GetDiscountEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetDiscountData $GetDiscountDataFromJson(Map<String, dynamic> json) {
  final GetDiscountData getDiscountData = GetDiscountData();
  final GetDiscountDataPagination? pagination =
      jsonConvert.convert<GetDiscountDataPagination>(json['pagination']);
  if (pagination != null) {
    getDiscountData.pagination = pagination;
  }
  final List<GetDiscountDataRecord>? record =
      jsonConvert.convertListNotNull<GetDiscountDataRecord>(json['record']);
  if (record != null) {
    getDiscountData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getDiscountData.cache = cache;
  }
  return getDiscountData;
}

Map<String, dynamic> $GetDiscountDataToJson(GetDiscountData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetDiscountDataPagination $GetDiscountDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetDiscountDataPagination getDiscountDataPagination =
      GetDiscountDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getDiscountDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getDiscountDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getDiscountDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getDiscountDataPagination.total = total;
  }
  return getDiscountDataPagination;
}

Map<String, dynamic> $GetDiscountDataPaginationToJson(
    GetDiscountDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetDiscountDataRecord $GetDiscountDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetDiscountDataRecord getDiscountDataRecord = GetDiscountDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDiscountDataRecord.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getDiscountDataRecord.storeId = storeId;
  }
  final String? title = jsonConvert.convert<String>(json['title']);
  if (title != null) {
    getDiscountDataRecord.title = title;
  }
  final String? promotionType =
      jsonConvert.convert<String>(json['promotion_type']);
  if (promotionType != null) {
    getDiscountDataRecord.promotionType = promotionType;
  }
  final String? discountType =
      jsonConvert.convert<String>(json['discount_type']);
  if (discountType != null) {
    getDiscountDataRecord.discountType = discountType;
  }
  final int? discountLimit = jsonConvert.convert<int>(json['discount_limit']);
  if (discountLimit != null) {
    getDiscountDataRecord.discountLimit = discountLimit;
  }
  final String? usedType = jsonConvert.convert<String>(json['used_type']);
  if (usedType != null) {
    getDiscountDataRecord.usedType = usedType;
  }
  final int? usedTotal = jsonConvert.convert<int>(json['used_total']);
  if (usedTotal != null) {
    getDiscountDataRecord.usedTotal = usedTotal;
  }
  final bool? limitType = jsonConvert.convert<bool>(json['limit_type']);
  if (limitType != null) {
    getDiscountDataRecord.limitType = limitType;
  }
  final int? limitTotal = jsonConvert.convert<int>(json['limit_total']);
  if (limitTotal != null) {
    getDiscountDataRecord.limitTotal = limitTotal;
  }
  final bool? userLimitType =
      jsonConvert.convert<bool>(json['user_limit_type']);
  if (userLimitType != null) {
    getDiscountDataRecord.userLimitType = userLimitType;
  }
  final int? userLimitTotal =
      jsonConvert.convert<int>(json['user_limit_total']);
  if (userLimitTotal != null) {
    getDiscountDataRecord.userLimitTotal = userLimitTotal;
  }
  final int? cashback = jsonConvert.convert<int>(json['cashback']);
  if (cashback != null) {
    getDiscountDataRecord.cashback = cashback;
  }
  final GetDiscountDataRecordPromotionTime? promotionTime = jsonConvert
      .convert<GetDiscountDataRecordPromotionTime>(json['promotion_time']);
  if (promotionTime != null) {
    getDiscountDataRecord.promotionTime = promotionTime;
  }
  final List<GetDiscountDataRecordRates>? rates =
      jsonConvert.convertListNotNull<GetDiscountDataRecordRates>(json['rates']);
  if (rates != null) {
    getDiscountDataRecord.rates = rates;
  }
  final String? condition = jsonConvert.convert<String>(json['condition']);
  if (condition != null) {
    getDiscountDataRecord.condition = condition;
  }
  final List<dynamic>? images =
      jsonConvert.convertListNotNull<dynamic>(json['images']);
  if (images != null) {
    getDiscountDataRecord.images = images;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getDiscountDataRecord.status = status;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getDiscountDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getDiscountDataRecord.updatedAt = updatedAt;
  }
  final dynamic? meta = jsonConvert.convert<dynamic>(json['meta']);
  if (meta != null) {
    getDiscountDataRecord.meta = meta;
  }
  return getDiscountDataRecord;
}

Map<String, dynamic> $GetDiscountDataRecordToJson(
    GetDiscountDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['title'] = entity.title;
  data['promotion_type'] = entity.promotionType;
  data['discount_type'] = entity.discountType;
  data['discount_limit'] = entity.discountLimit;
  data['used_type'] = entity.usedType;
  data['used_total'] = entity.usedTotal;
  data['limit_type'] = entity.limitType;
  data['limit_total'] = entity.limitTotal;
  data['user_limit_type'] = entity.userLimitType;
  data['user_limit_total'] = entity.userLimitTotal;
  data['cashback'] = entity.cashback;
  data['promotion_time'] = entity.promotionTime?.toJson();
  data['rates'] = entity.rates?.map((v) => v.toJson()).toList();
  data['condition'] = entity.condition;
  data['images'] = entity.images;
  data['status'] = entity.status;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['meta'] = entity.meta;
  return data;
}

GetDiscountDataRecordPromotionTime $GetDiscountDataRecordPromotionTimeFromJson(
    Map<String, dynamic> json) {
  final GetDiscountDataRecordPromotionTime getDiscountDataRecordPromotionTime =
      GetDiscountDataRecordPromotionTime();
  final String? startDate = jsonConvert.convert<String>(json['start_date']);
  if (startDate != null) {
    getDiscountDataRecordPromotionTime.startDate = startDate;
  }
  final String? endDate = jsonConvert.convert<String>(json['end_date']);
  if (endDate != null) {
    getDiscountDataRecordPromotionTime.endDate = endDate;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getDiscountDataRecordPromotionTime.textFull = textFull;
  }
  final String? textShort = jsonConvert.convert<String>(json['text_short']);
  if (textShort != null) {
    getDiscountDataRecordPromotionTime.textShort = textShort;
  }
  final int? status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    getDiscountDataRecordPromotionTime.status = status;
  }
  final String? statusText = jsonConvert.convert<String>(json['status_text']);
  if (statusText != null) {
    getDiscountDataRecordPromotionTime.statusText = statusText;
  }
  return getDiscountDataRecordPromotionTime;
}

Map<String, dynamic> $GetDiscountDataRecordPromotionTimeToJson(
    GetDiscountDataRecordPromotionTime entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['start_date'] = entity.startDate;
  data['end_date'] = entity.endDate;
  data['text_full'] = entity.textFull;
  data['text_short'] = entity.textShort;
  data['status'] = entity.status;
  data['status_text'] = entity.statusText;
  return data;
}

GetDiscountDataRecordRates $GetDiscountDataRecordRatesFromJson(
    Map<String, dynamic> json) {
  final GetDiscountDataRecordRates getDiscountDataRecordRates =
      GetDiscountDataRecordRates();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDiscountDataRecordRates.id = id;
  }
  final int? valueStart = jsonConvert.convert<int>(json['value_start']);
  if (valueStart != null) {
    getDiscountDataRecordRates.valueStart = valueStart;
  }
  final int? valueEnd = jsonConvert.convert<int>(json['value_end']);
  if (valueEnd != null) {
    getDiscountDataRecordRates.valueEnd = valueEnd;
  }
  final int? discount = jsonConvert.convert<int>(json['discount']);
  if (discount != null) {
    getDiscountDataRecordRates.discount = discount;
  }
  return getDiscountDataRecordRates;
}

Map<String, dynamic> $GetDiscountDataRecordRatesToJson(
    GetDiscountDataRecordRates entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['value_start'] = entity.valueStart;
  data['value_end'] = entity.valueEnd;
  data['discount'] = entity.discount;
  return data;
}

GetDiscountBench $GetDiscountBenchFromJson(Map<String, dynamic> json) {
  final GetDiscountBench getDiscountBench = GetDiscountBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getDiscountBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getDiscountBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getDiscountBench.format = format;
  }
  return getDiscountBench;
}

Map<String, dynamic> $GetDiscountBenchToJson(GetDiscountBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
