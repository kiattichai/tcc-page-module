import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/uploads_banner_entity.dart';

UploadsBannerEntity $UploadsBannerEntityFromJson(Map<String, dynamic> json) {
  final UploadsBannerEntity uploadsBannerEntity = UploadsBannerEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    uploadsBannerEntity.storeId = storeId;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    uploadsBannerEntity.status = status;
  }
  final String? fileableType =
      jsonConvert.convert<String>(json['fileable_type']);
  if (fileableType != null) {
    uploadsBannerEntity.fileableType = fileableType;
  }
  final bool? secure = jsonConvert.convert<bool>(json['secure']);
  if (secure != null) {
    uploadsBannerEntity.secure = secure;
  }
  final List<UploadsBannerFiles>? files =
      jsonConvert.convertListNotNull<UploadsBannerFiles>(json['files']);
  if (files != null) {
    uploadsBannerEntity.files = files;
  }
  return uploadsBannerEntity;
}

Map<String, dynamic> $UploadsBannerEntityToJson(UploadsBannerEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['status'] = entity.status;
  data['fileable_type'] = entity.fileableType;
  data['secure'] = entity.secure;
  data['files'] = entity.files?.map((v) => v.toJson()).toList();
  return data;
}

UploadsBannerFiles $UploadsBannerFilesFromJson(Map<String, dynamic> json) {
  final UploadsBannerFiles uploadsBannerFiles = UploadsBannerFiles();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    uploadsBannerFiles.name = name;
  }
  final String? data = jsonConvert.convert<String>(json['data']);
  if (data != null) {
    uploadsBannerFiles.data = data;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    uploadsBannerFiles.position = position;
  }
  return uploadsBannerFiles;
}

Map<String, dynamic> $UploadsBannerFilesToJson(UploadsBannerFiles entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['data'] = entity.data;
  data['position'] = entity.position;
  return data;
}
