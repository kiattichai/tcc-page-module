import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_agent_entity.dart';

GetAgentEntity $GetAgentEntityFromJson(Map<String, dynamic> json) {
  final GetAgentEntity getAgentEntity = GetAgentEntity();
  final GetAgentData? data = jsonConvert.convert<GetAgentData>(json['data']);
  if (data != null) {
    getAgentEntity.data = data;
  }
  final GetAgentBench? bench =
      jsonConvert.convert<GetAgentBench>(json['bench']);
  if (bench != null) {
    getAgentEntity.bench = bench;
  }
  return getAgentEntity;
}

Map<String, dynamic> $GetAgentEntityToJson(GetAgentEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAgentData $GetAgentDataFromJson(Map<String, dynamic> json) {
  final GetAgentData getAgentData = GetAgentData();
  final int? agentId = jsonConvert.convert<int>(json['agent_id']);
  if (agentId != null) {
    getAgentData.agentId = agentId;
  }
  final int? userId = jsonConvert.convert<int>(json['user_id']);
  if (userId != null) {
    getAgentData.userId = userId;
  }
  final int? status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    getAgentData.status = status;
  }
  final GetAgentDataDocumentStatus? documentStatus =
      jsonConvert.convert<GetAgentDataDocumentStatus>(json['document_status']);
  if (documentStatus != null) {
    getAgentData.documentStatus = documentStatus;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getAgentData.remark = remark;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getAgentData.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getAgentData.lastName = lastName;
  }
  final String? thaiId = jsonConvert.convert<String>(json['thai_id']);
  if (thaiId != null) {
    getAgentData.thaiId = thaiId;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getAgentData.email = email;
  }
  final GetAgentDataAddress? address =
      jsonConvert.convert<GetAgentDataAddress>(json['address']);
  if (address != null) {
    getAgentData.address = address;
  }
  final GetAgentDataBankDetail? bankDetail =
      jsonConvert.convert<GetAgentDataBankDetail>(json['bank_detail']);
  if (bankDetail != null) {
    getAgentData.bankDetail = bankDetail;
  }
  return getAgentData;
}

Map<String, dynamic> $GetAgentDataToJson(GetAgentData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['agent_id'] = entity.agentId;
  data['user_id'] = entity.userId;
  data['status'] = entity.status;
  data['document_status'] = entity.documentStatus?.toJson();
  data['remark'] = entity.remark;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['thai_id'] = entity.thaiId;
  data['email'] = entity.email;
  data['address'] = entity.address?.toJson();
  data['bank_detail'] = entity.bankDetail?.toJson();
  return data;
}

GetAgentDataDocumentStatus $GetAgentDataDocumentStatusFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataDocumentStatus getAgentDataDocumentStatus =
      GetAgentDataDocumentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentDataDocumentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getAgentDataDocumentStatus.text = text;
  }
  return getAgentDataDocumentStatus;
}

Map<String, dynamic> $GetAgentDataDocumentStatusToJson(
    GetAgentDataDocumentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetAgentDataAddress $GetAgentDataAddressFromJson(Map<String, dynamic> json) {
  final GetAgentDataAddress getAgentDataAddress = GetAgentDataAddress();
  final String? detail = jsonConvert.convert<String>(json['detail']);
  if (detail != null) {
    getAgentDataAddress.detail = detail;
  }
  final GetAgentDataAddressDistrict? district =
      jsonConvert.convert<GetAgentDataAddressDistrict>(json['district']);
  if (district != null) {
    getAgentDataAddress.district = district;
  }
  final GetAgentDataAddressCity? city =
      jsonConvert.convert<GetAgentDataAddressCity>(json['city']);
  if (city != null) {
    getAgentDataAddress.city = city;
  }
  final GetAgentDataAddressProvince? province =
      jsonConvert.convert<GetAgentDataAddressProvince>(json['province']);
  if (province != null) {
    getAgentDataAddress.province = province;
  }
  final GetAgentDataAddressCountry? country =
      jsonConvert.convert<GetAgentDataAddressCountry>(json['country']);
  if (country != null) {
    getAgentDataAddress.country = country;
  }
  final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
  if (zipCode != null) {
    getAgentDataAddress.zipCode = zipCode;
  }
  return getAgentDataAddress;
}

Map<String, dynamic> $GetAgentDataAddressToJson(GetAgentDataAddress entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['detail'] = entity.detail;
  data['district'] = entity.district?.toJson();
  data['city'] = entity.city?.toJson();
  data['province'] = entity.province?.toJson();
  data['country'] = entity.country?.toJson();
  data['zip_code'] = entity.zipCode;
  return data;
}

GetAgentDataAddressDistrict $GetAgentDataAddressDistrictFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataAddressDistrict getAgentDataAddressDistrict =
      GetAgentDataAddressDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentDataAddressDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentDataAddressDistrict.name = name;
  }
  return getAgentDataAddressDistrict;
}

Map<String, dynamic> $GetAgentDataAddressDistrictToJson(
    GetAgentDataAddressDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetAgentDataAddressCity $GetAgentDataAddressCityFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataAddressCity getAgentDataAddressCity =
      GetAgentDataAddressCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentDataAddressCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentDataAddressCity.name = name;
  }
  return getAgentDataAddressCity;
}

Map<String, dynamic> $GetAgentDataAddressCityToJson(
    GetAgentDataAddressCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetAgentDataAddressProvince $GetAgentDataAddressProvinceFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataAddressProvince getAgentDataAddressProvince =
      GetAgentDataAddressProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentDataAddressProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentDataAddressProvince.name = name;
  }
  return getAgentDataAddressProvince;
}

Map<String, dynamic> $GetAgentDataAddressProvinceToJson(
    GetAgentDataAddressProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetAgentDataAddressCountry $GetAgentDataAddressCountryFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataAddressCountry getAgentDataAddressCountry =
      GetAgentDataAddressCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getAgentDataAddressCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentDataAddressCountry.name = name;
  }
  return getAgentDataAddressCountry;
}

Map<String, dynamic> $GetAgentDataAddressCountryToJson(
    GetAgentDataAddressCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetAgentDataBankDetail $GetAgentDataBankDetailFromJson(
    Map<String, dynamic> json) {
  final GetAgentDataBankDetail getAgentDataBankDetail =
      GetAgentDataBankDetail();
  final dynamic? bankId = jsonConvert.convert<dynamic>(json['bank_id']);
  if (bankId != null) {
    getAgentDataBankDetail.bankId = bankId;
  }
  final String? bankName = jsonConvert.convert<String>(json['bank_name']);
  if (bankName != null) {
    getAgentDataBankDetail.bankName = bankName;
  }
  final String? accountNo = jsonConvert.convert<String>(json['account_no']);
  if (accountNo != null) {
    getAgentDataBankDetail.accountNo = accountNo;
  }
  final String? accountName = jsonConvert.convert<String>(json['account_name']);
  if (accountName != null) {
    getAgentDataBankDetail.accountName = accountName;
  }
  final int? accountTypeId = jsonConvert.convert<int>(json['account_type_id']);
  if (accountTypeId != null) {
    getAgentDataBankDetail.accountTypeId = accountTypeId;
  }
  final String? accountTypeName =
      jsonConvert.convert<String>(json['account_type_name']);
  if (accountTypeName != null) {
    getAgentDataBankDetail.accountTypeName = accountTypeName;
  }
  final String? branch = jsonConvert.convert<String>(json['branch']);
  if (branch != null) {
    getAgentDataBankDetail.branch = branch;
  }
  return getAgentDataBankDetail;
}

Map<String, dynamic> $GetAgentDataBankDetailToJson(
    GetAgentDataBankDetail entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['bank_id'] = entity.bankId;
  data['bank_name'] = entity.bankName;
  data['account_no'] = entity.accountNo;
  data['account_name'] = entity.accountName;
  data['account_type_id'] = entity.accountTypeId;
  data['account_type_name'] = entity.accountTypeName;
  data['branch'] = entity.branch;
  return data;
}

GetAgentBench $GetAgentBenchFromJson(Map<String, dynamic> json) {
  final GetAgentBench getAgentBench = GetAgentBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAgentBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAgentBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAgentBench.format = format;
  }
  return getAgentBench;
}

Map<String, dynamic> $GetAgentBenchToJson(GetAgentBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
