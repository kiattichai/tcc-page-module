import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/save_avatar_response_entity.dart';

SaveAvatarResponseEntity $SaveAvatarResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SaveAvatarResponseEntity saveAvatarResponseEntity =
      SaveAvatarResponseEntity();
  final SaveAvatarResponseData? data =
      jsonConvert.convert<SaveAvatarResponseData>(json['data']);
  if (data != null) {
    saveAvatarResponseEntity.data = data;
  }
  return saveAvatarResponseEntity;
}

Map<String, dynamic> $SaveAvatarResponseEntityToJson(
    SaveAvatarResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  return data;
}

SaveAvatarResponseData $SaveAvatarResponseDataFromJson(
    Map<String, dynamic> json) {
  final SaveAvatarResponseData saveAvatarResponseData =
      SaveAvatarResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    saveAvatarResponseData.message = message;
  }
  final String? image = jsonConvert.convert<String>(json['image']);
  if (image != null) {
    saveAvatarResponseData.image = image;
  }
  return saveAvatarResponseData;
}

Map<String, dynamic> $SaveAvatarResponseDataToJson(
    SaveAvatarResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['image'] = entity.image;
  return data;
}
