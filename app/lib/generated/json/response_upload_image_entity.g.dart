import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/response_upload_image_entity.dart';

ResponseUploadImageEntity $ResponseUploadImageEntityFromJson(
    Map<String, dynamic> json) {
  final ResponseUploadImageEntity responseUploadImageEntity =
      ResponseUploadImageEntity();
  final ResponseUploadImageData? data =
      jsonConvert.convert<ResponseUploadImageData>(json['data']);
  if (data != null) {
    responseUploadImageEntity.data = data;
  }
  final ResponseUploadImageBench? bench =
      jsonConvert.convert<ResponseUploadImageBench>(json['bench']);
  if (bench != null) {
    responseUploadImageEntity.bench = bench;
  }
  return responseUploadImageEntity;
}

Map<String, dynamic> $ResponseUploadImageEntityToJson(
    ResponseUploadImageEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseUploadImageData $ResponseUploadImageDataFromJson(
    Map<String, dynamic> json) {
  final ResponseUploadImageData responseUploadImageData =
      ResponseUploadImageData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseUploadImageData.message = message;
  }
  return responseUploadImageData;
}

Map<String, dynamic> $ResponseUploadImageDataToJson(
    ResponseUploadImageData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

ResponseUploadImageBench $ResponseUploadImageBenchFromJson(
    Map<String, dynamic> json) {
  final ResponseUploadImageBench responseUploadImageBench =
      ResponseUploadImageBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseUploadImageBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseUploadImageBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseUploadImageBench.format = format;
  }
  return responseUploadImageBench;
}

Map<String, dynamic> $ResponseUploadImageBenchToJson(
    ResponseUploadImageBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
