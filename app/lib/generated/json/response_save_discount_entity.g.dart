import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/response_save_discount_entity.dart';

ResponseSaveDiscountEntity $ResponseSaveDiscountEntityFromJson(
    Map<String, dynamic> json) {
  final ResponseSaveDiscountEntity responseSaveDiscountEntity =
      ResponseSaveDiscountEntity();
  final ResponseSaveDiscountData? data =
      jsonConvert.convert<ResponseSaveDiscountData>(json['data']);
  if (data != null) {
    responseSaveDiscountEntity.data = data;
  }
  final ResponseSaveDiscountBench? bench =
      jsonConvert.convert<ResponseSaveDiscountBench>(json['bench']);
  if (bench != null) {
    responseSaveDiscountEntity.bench = bench;
  }
  return responseSaveDiscountEntity;
}

Map<String, dynamic> $ResponseSaveDiscountEntityToJson(
    ResponseSaveDiscountEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseSaveDiscountData $ResponseSaveDiscountDataFromJson(
    Map<String, dynamic> json) {
  final ResponseSaveDiscountData responseSaveDiscountData =
      ResponseSaveDiscountData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseSaveDiscountData.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    responseSaveDiscountData.id = id;
  }
  return responseSaveDiscountData;
}

Map<String, dynamic> $ResponseSaveDiscountDataToJson(
    ResponseSaveDiscountData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  return data;
}

ResponseSaveDiscountBench $ResponseSaveDiscountBenchFromJson(
    Map<String, dynamic> json) {
  final ResponseSaveDiscountBench responseSaveDiscountBench =
      ResponseSaveDiscountBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseSaveDiscountBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseSaveDiscountBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseSaveDiscountBench.format = format;
  }
  return responseSaveDiscountBench;
}

Map<String, dynamic> $ResponseSaveDiscountBenchToJson(
    ResponseSaveDiscountBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
