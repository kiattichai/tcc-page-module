import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_ticket_entity.dart';

GetTicketEntity $GetTicketEntityFromJson(Map<String, dynamic> json) {
  final GetTicketEntity getTicketEntity = GetTicketEntity();
  final GetTicketData? data = jsonConvert.convert<GetTicketData>(json['data']);
  if (data != null) {
    getTicketEntity.data = data;
  }
  final GetTicketBench? bench =
      jsonConvert.convert<GetTicketBench>(json['bench']);
  if (bench != null) {
    getTicketEntity.bench = bench;
  }
  return getTicketEntity;
}

Map<String, dynamic> $GetTicketEntityToJson(GetTicketEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetTicketData $GetTicketDataFromJson(Map<String, dynamic> json) {
  final GetTicketData getTicketData = GetTicketData();
  final List<GetTicketDataRecord>? record =
      jsonConvert.convertListNotNull<GetTicketDataRecord>(json['record']);
  if (record != null) {
    getTicketData.record = record;
  }
  return getTicketData;
}

Map<String, dynamic> $GetTicketDataToJson(GetTicketData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetTicketDataRecord $GetTicketDataRecordFromJson(Map<String, dynamic> json) {
  final GetTicketDataRecord getTicketDataRecord = GetTicketDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getTicketDataRecord.id = id;
  }
  final String? sku = jsonConvert.convert<String>(json['sku']);
  if (sku != null) {
    getTicketDataRecord.sku = sku;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getTicketDataRecord.name = name;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getTicketDataRecord.description = description;
  }
  final dynamic? group = jsonConvert.convert<dynamic>(json['group']);
  if (group != null) {
    getTicketDataRecord.group = group;
  }
  final dynamic? zone = jsonConvert.convert<dynamic>(json['zone']);
  if (zone != null) {
    getTicketDataRecord.zone = zone;
  }
  final int? costPrice = jsonConvert.convert<int>(json['cost_price']);
  if (costPrice != null) {
    getTicketDataRecord.costPrice = costPrice;
  }
  final String? costPriceText =
      jsonConvert.convert<String>(json['cost_price_text']);
  if (costPriceText != null) {
    getTicketDataRecord.costPriceText = costPriceText;
  }
  final int? price = jsonConvert.convert<int>(json['price']);
  if (price != null) {
    getTicketDataRecord.price = price;
  }
  final String? priceText = jsonConvert.convert<String>(json['price_text']);
  if (priceText != null) {
    getTicketDataRecord.priceText = priceText;
  }
  final int? compareAtPrice =
      jsonConvert.convert<int>(json['compare_at_price']);
  if (compareAtPrice != null) {
    getTicketDataRecord.compareAtPrice = compareAtPrice;
  }
  final String? compareAtPriceText =
      jsonConvert.convert<String>(json['compare_at_price_text']);
  if (compareAtPriceText != null) {
    getTicketDataRecord.compareAtPriceText = compareAtPriceText;
  }
  final bool? package = jsonConvert.convert<bool>(json['package']);
  if (package != null) {
    getTicketDataRecord.package = package;
  }
  final int? perPackage = jsonConvert.convert<int>(json['per_package']);
  if (perPackage != null) {
    getTicketDataRecord.perPackage = perPackage;
  }
  final int? stock = jsonConvert.convert<int>(json['stock']);
  if (stock != null) {
    getTicketDataRecord.stock = stock;
  }
  final int? quantity = jsonConvert.convert<int>(json['quantity']);
  if (quantity != null) {
    getTicketDataRecord.quantity = quantity;
  }
  final dynamic? orderQuantity =
      jsonConvert.convert<dynamic>(json['order_quantity']);
  if (orderQuantity != null) {
    getTicketDataRecord.orderQuantity = orderQuantity;
  }
  final int? diffStock = jsonConvert.convert<int>(json['diff_stock']);
  if (diffStock != null) {
    getTicketDataRecord.diffStock = diffStock;
  }
  final int? hold = jsonConvert.convert<int>(json['hold']);
  if (hold != null) {
    getTicketDataRecord.hold = hold;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getTicketDataRecord.position = position;
  }
  final int? points = jsonConvert.convert<int>(json['points']);
  if (points != null) {
    getTicketDataRecord.points = points;
  }
  final int? weight = jsonConvert.convert<int>(json['weight']);
  if (weight != null) {
    getTicketDataRecord.weight = weight;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketDataRecord.status = status;
  }
  final String? publishedStart =
      jsonConvert.convert<String>(json['published_start']);
  if (publishedStart != null) {
    getTicketDataRecord.publishedStart = publishedStart;
  }
  final String? publishedEnd =
      jsonConvert.convert<String>(json['published_end']);
  if (publishedEnd != null) {
    getTicketDataRecord.publishedEnd = publishedEnd;
  }
  final String? gateOpen = jsonConvert.convert<String>(json['gate_open']);
  if (gateOpen != null) {
    getTicketDataRecord.gateOpen = gateOpen;
  }
  final String? gateClose = jsonConvert.convert<String>(json['gate_close']);
  if (gateClose != null) {
    getTicketDataRecord.gateClose = gateClose;
  }
  final int? allowOrderMin = jsonConvert.convert<int>(json['allow_order_min']);
  if (allowOrderMin != null) {
    getTicketDataRecord.allowOrderMin = allowOrderMin;
  }
  final int? allowOrderMax = jsonConvert.convert<int>(json['allow_order_max']);
  if (allowOrderMax != null) {
    getTicketDataRecord.allowOrderMax = allowOrderMax;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getTicketDataRecord.remark = remark;
  }
  final bool? specialOption = jsonConvert.convert<bool>(json['special_option']);
  if (specialOption != null) {
    getTicketDataRecord.specialOption = specialOption;
  }
  final bool? serviceCharge = jsonConvert.convert<bool>(json['service_charge']);
  if (serviceCharge != null) {
    getTicketDataRecord.serviceCharge = serviceCharge;
  }
  final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
  if (serviceFee != null) {
    getTicketDataRecord.serviceFee = serviceFee;
  }
  final bool? hasTicket = jsonConvert.convert<bool>(json['has_ticket']);
  if (hasTicket != null) {
    getTicketDataRecord.hasTicket = hasTicket;
  }
  final GetTicketDataRecordService? service =
      jsonConvert.convert<GetTicketDataRecordService>(json['service']);
  if (service != null) {
    getTicketDataRecord.service = service;
  }
  final List<dynamic>? options =
      jsonConvert.convertListNotNull<dynamic>(json['options']);
  if (options != null) {
    getTicketDataRecord.options = options;
  }
  final List<dynamic>? promotions =
      jsonConvert.convertListNotNull<dynamic>(json['promotions']);
  if (promotions != null) {
    getTicketDataRecord.promotions = promotions;
  }
  final GetTicketDataRecordImage? image =
      jsonConvert.convert<GetTicketDataRecordImage>(json['image']);
  if (image != null) {
    getTicketDataRecord.image = image;
  }
  final bool? onlineMeeting = jsonConvert.convert<bool>(json['online_meeting']);
  if (onlineMeeting != null) {
    getTicketDataRecord.onlineMeeting = onlineMeeting;
  }
  final List<dynamic>? meetings =
      jsonConvert.convertListNotNull<dynamic>(json['meetings']);
  if (meetings != null) {
    getTicketDataRecord.meetings = meetings;
  }
  final dynamic? meta = jsonConvert.convert<dynamic>(json['meta']);
  if (meta != null) {
    getTicketDataRecord.meta = meta;
  }
  final bool? displayStatus = jsonConvert.convert<bool>(json['display_status']);
  if (displayStatus != null) {
    getTicketDataRecord.displayStatus = displayStatus;
  }
  final String? displayText = jsonConvert.convert<String>(json['display_text']);
  if (displayText != null) {
    getTicketDataRecord.displayText = displayText;
  }
  return getTicketDataRecord;
}

Map<String, dynamic> $GetTicketDataRecordToJson(GetTicketDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['sku'] = entity.sku;
  data['name'] = entity.name;
  data['description'] = entity.description;
  data['group'] = entity.group;
  data['zone'] = entity.zone;
  data['cost_price'] = entity.costPrice;
  data['cost_price_text'] = entity.costPriceText;
  data['price'] = entity.price;
  data['price_text'] = entity.priceText;
  data['compare_at_price'] = entity.compareAtPrice;
  data['compare_at_price_text'] = entity.compareAtPriceText;
  data['package'] = entity.package;
  data['per_package'] = entity.perPackage;
  data['stock'] = entity.stock;
  data['quantity'] = entity.quantity;
  data['order_quantity'] = entity.orderQuantity;
  data['diff_stock'] = entity.diffStock;
  data['hold'] = entity.hold;
  data['position'] = entity.position;
  data['points'] = entity.points;
  data['weight'] = entity.weight;
  data['status'] = entity.status;
  data['published_start'] = entity.publishedStart;
  data['published_end'] = entity.publishedEnd;
  data['gate_open'] = entity.gateOpen;
  data['gate_close'] = entity.gateClose;
  data['allow_order_min'] = entity.allowOrderMin;
  data['allow_order_max'] = entity.allowOrderMax;
  data['remark'] = entity.remark;
  data['special_option'] = entity.specialOption;
  data['service_charge'] = entity.serviceCharge;
  data['service_fee'] = entity.serviceFee;
  data['has_ticket'] = entity.hasTicket;
  data['service'] = entity.service?.toJson();
  data['options'] = entity.options;
  data['promotions'] = entity.promotions;
  data['image'] = entity.image?.toJson();
  data['online_meeting'] = entity.onlineMeeting;
  data['meetings'] = entity.meetings;
  data['meta'] = entity.meta;
  data['display_status'] = entity.displayStatus;
  data['display_text'] = entity.displayText;
  return data;
}

GetTicketDataRecordService $GetTicketDataRecordServiceFromJson(
    Map<String, dynamic> json) {
  final GetTicketDataRecordService getTicketDataRecordService =
      GetTicketDataRecordService();
  final bool? charge = jsonConvert.convert<bool>(json['charge']);
  if (charge != null) {
    getTicketDataRecordService.charge = charge;
  }
  final int? feeValue = jsonConvert.convert<int>(json['fee_value']);
  if (feeValue != null) {
    getTicketDataRecordService.feeValue = feeValue;
  }
  final int? fee = jsonConvert.convert<int>(json['fee']);
  if (fee != null) {
    getTicketDataRecordService.fee = fee;
  }
  final String? feeText = jsonConvert.convert<String>(json['fee_text']);
  if (feeText != null) {
    getTicketDataRecordService.feeText = feeText;
  }
  return getTicketDataRecordService;
}

Map<String, dynamic> $GetTicketDataRecordServiceToJson(
    GetTicketDataRecordService entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['charge'] = entity.charge;
  data['fee_value'] = entity.feeValue;
  data['fee'] = entity.fee;
  data['fee_text'] = entity.feeText;
  return data;
}

GetTicketDataRecordImage $GetTicketDataRecordImageFromJson(
    Map<String, dynamic> json) {
  final GetTicketDataRecordImage getTicketDataRecordImage =
      GetTicketDataRecordImage();
  return getTicketDataRecordImage;
}

Map<String, dynamic> $GetTicketDataRecordImageToJson(
    GetTicketDataRecordImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetTicketBench $GetTicketBenchFromJson(Map<String, dynamic> json) {
  final GetTicketBench getTicketBench = GetTicketBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getTicketBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getTicketBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getTicketBench.format = format;
  }
  return getTicketBench;
}

Map<String, dynamic> $GetTicketBenchToJson(GetTicketBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
