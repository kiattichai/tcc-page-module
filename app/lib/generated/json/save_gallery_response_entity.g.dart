import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_gallery_response_entity.dart';

SaveGalleryResponseEntity $SaveGalleryResponseEntityFromJson(Map<String, dynamic> json) {
	final SaveGalleryResponseEntity saveGalleryResponseEntity = SaveGalleryResponseEntity();
	final SaveGalleryResponseData? data = jsonConvert.convert<SaveGalleryResponseData>(json['data']);
	if (data != null) {
		saveGalleryResponseEntity.data = data;
	}
	final SaveGalleryResponseBench? bench = jsonConvert.convert<SaveGalleryResponseBench>(json['bench']);
	if (bench != null) {
		saveGalleryResponseEntity.bench = bench;
	}
	return saveGalleryResponseEntity;
}

Map<String, dynamic> $SaveGalleryResponseEntityToJson(SaveGalleryResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

SaveGalleryResponseData $SaveGalleryResponseDataFromJson(Map<String, dynamic> json) {
	final SaveGalleryResponseData saveGalleryResponseData = SaveGalleryResponseData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		saveGalleryResponseData.message = message;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		saveGalleryResponseData.id = id;
	}
	return saveGalleryResponseData;
}

Map<String, dynamic> $SaveGalleryResponseDataToJson(SaveGalleryResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	data['id'] = entity.id;
	return data;
}

SaveGalleryResponseBench $SaveGalleryResponseBenchFromJson(Map<String, dynamic> json) {
	final SaveGalleryResponseBench saveGalleryResponseBench = SaveGalleryResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		saveGalleryResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		saveGalleryResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		saveGalleryResponseBench.format = format;
	}
	return saveGalleryResponseBench;
}

Map<String, dynamic> $SaveGalleryResponseBenchToJson(SaveGalleryResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}