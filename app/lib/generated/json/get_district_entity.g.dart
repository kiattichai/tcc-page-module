import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_district_entity.dart';

GetDistrictEntity $GetDistrictEntityFromJson(Map<String, dynamic> json) {
	final GetDistrictEntity getDistrictEntity = GetDistrictEntity();
	final GetDistrictData? data = jsonConvert.convert<GetDistrictData>(json['data']);
	if (data != null) {
		getDistrictEntity.data = data;
	}
	final GetDistrictBench? bench = jsonConvert.convert<GetDistrictBench>(json['bench']);
	if (bench != null) {
		getDistrictEntity.bench = bench;
	}
	return getDistrictEntity;
}

Map<String, dynamic> $GetDistrictEntityToJson(GetDistrictEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetDistrictData $GetDistrictDataFromJson(Map<String, dynamic> json) {
	final GetDistrictData getDistrictData = GetDistrictData();
	final List<GetDistrictDataRecord>? record = jsonConvert.convertListNotNull<GetDistrictDataRecord>(json['record']);
	if (record != null) {
		getDistrictData.record = record;
	}
	final bool? cache = jsonConvert.convert<bool>(json['cache']);
	if (cache != null) {
		getDistrictData.cache = cache;
	}
	return getDistrictData;
}

Map<String, dynamic> $GetDistrictDataToJson(GetDistrictData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['record'] =  entity.record?.map((v) => v.toJson()).toList();
	data['cache'] = entity.cache;
	return data;
}

GetDistrictDataRecord $GetDistrictDataRecordFromJson(Map<String, dynamic> json) {
	final GetDistrictDataRecord getDistrictDataRecord = GetDistrictDataRecord();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getDistrictDataRecord.id = id;
	}
	final GetDistrictDataRecordName? name = jsonConvert.convert<GetDistrictDataRecordName>(json['name']);
	if (name != null) {
		getDistrictDataRecord.name = name;
	}
	final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
	if (zipCode != null) {
		getDistrictDataRecord.zipCode = zipCode;
	}
	final String? zipCodeMap = jsonConvert.convert<String>(json['zip_code_map']);
	if (zipCodeMap != null) {
		getDistrictDataRecord.zipCodeMap = zipCodeMap;
	}
	return getDistrictDataRecord;
}

Map<String, dynamic> $GetDistrictDataRecordToJson(GetDistrictDataRecord entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name?.toJson();
	data['zip_code'] = entity.zipCode;
	data['zip_code_map'] = entity.zipCodeMap;
	return data;
}

GetDistrictDataRecordName $GetDistrictDataRecordNameFromJson(Map<String, dynamic> json) {
	final GetDistrictDataRecordName getDistrictDataRecordName = GetDistrictDataRecordName();
	final String? th = jsonConvert.convert<String>(json['th']);
	if (th != null) {
		getDistrictDataRecordName.th = th;
	}
	final String? en = jsonConvert.convert<String>(json['en']);
	if (en != null) {
		getDistrictDataRecordName.en = en;
	}
	return getDistrictDataRecordName;
}

Map<String, dynamic> $GetDistrictDataRecordNameToJson(GetDistrictDataRecordName entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['th'] = entity.th;
	data['en'] = entity.en;
	return data;
}

GetDistrictBench $GetDistrictBenchFromJson(Map<String, dynamic> json) {
	final GetDistrictBench getDistrictBench = GetDistrictBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getDistrictBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getDistrictBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getDistrictBench.format = format;
	}
	return getDistrictBench;
}

Map<String, dynamic> $GetDistrictBenchToJson(GetDistrictBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}