import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_entity.dart';

UploadConcertImageEntity $UploadConcertImageEntityFromJson(Map<String, dynamic> json) {
	final UploadConcertImageEntity uploadConcertImageEntity = UploadConcertImageEntity();
	final String? storeId = jsonConvert.convert<String>(json['store_id']);
	if (storeId != null) {
		uploadConcertImageEntity.storeId = storeId;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		uploadConcertImageEntity.status = status;
	}
	final bool? secure = jsonConvert.convert<bool>(json['secure']);
	if (secure != null) {
		uploadConcertImageEntity.secure = secure;
	}
	final List<UploadConcertImageFiles>? files = jsonConvert.convertListNotNull<UploadConcertImageFiles>(json['files']);
	if (files != null) {
		uploadConcertImageEntity.files = files;
	}
	return uploadConcertImageEntity;
}

Map<String, dynamic> $UploadConcertImageEntityToJson(UploadConcertImageEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['store_id'] = entity.storeId;
	data['status'] = entity.status;
	data['secure'] = entity.secure;
	data['files'] =  entity.files?.map((v) => v.toJson()).toList();
	return data;
}

UploadConcertImageFiles $UploadConcertImageFilesFromJson(Map<String, dynamic> json) {
	final UploadConcertImageFiles uploadConcertImageFiles = UploadConcertImageFiles();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		uploadConcertImageFiles.name = name;
	}
	final String? data = jsonConvert.convert<String>(json['data']);
	if (data != null) {
		uploadConcertImageFiles.data = data;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		uploadConcertImageFiles.position = position;
	}
	return uploadConcertImageFiles;
}

Map<String, dynamic> $UploadConcertImageFilesToJson(UploadConcertImageFiles entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	data['data'] = entity.data;
	data['position'] = entity.position;
	return data;
}