import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_venue_entity.dart';

GetVenueEntity $GetVenueEntityFromJson(Map<String, dynamic> json) {
  final GetVenueEntity getVenueEntity = GetVenueEntity();
  final GetVenueData? data = jsonConvert.convert<GetVenueData>(json['data']);
  if (data != null) {
    getVenueEntity.data = data;
  }
  final GetVenueBench? bench =
      jsonConvert.convert<GetVenueBench>(json['bench']);
  if (bench != null) {
    getVenueEntity.bench = bench;
  }
  return getVenueEntity;
}

Map<String, dynamic> $GetVenueEntityToJson(GetVenueEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetVenueData $GetVenueDataFromJson(Map<String, dynamic> json) {
  final GetVenueData getVenueData = GetVenueData();
  final GetVenueDataPagination? pagination =
      jsonConvert.convert<GetVenueDataPagination>(json['pagination']);
  if (pagination != null) {
    getVenueData.pagination = pagination;
  }
  final List<GetVenueDataRecord>? record =
      jsonConvert.convertListNotNull<GetVenueDataRecord>(json['record']);
  if (record != null) {
    getVenueData.record = record;
  }
  return getVenueData;
}

Map<String, dynamic> $GetVenueDataToJson(GetVenueData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetVenueDataPagination $GetVenueDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetVenueDataPagination getVenueDataPagination =
      GetVenueDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getVenueDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getVenueDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getVenueDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getVenueDataPagination.total = total;
  }
  return getVenueDataPagination;
}

Map<String, dynamic> $GetVenueDataPaginationToJson(
    GetVenueDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetVenueDataRecord $GetVenueDataRecordFromJson(Map<String, dynamic> json) {
  final GetVenueDataRecord getVenueDataRecord = GetVenueDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVenueDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVenueDataRecord.name = name;
  }
  return getVenueDataRecord;
}

Map<String, dynamic> $GetVenueDataRecordToJson(GetVenueDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetVenueBench $GetVenueBenchFromJson(Map<String, dynamic> json) {
  final GetVenueBench getVenueBench = GetVenueBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getVenueBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getVenueBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getVenueBench.format = format;
  }
  return getVenueBench;
}

Map<String, dynamic> $GetVenueBenchToJson(GetVenueBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
