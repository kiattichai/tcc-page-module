import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/response_result_entity.dart';

ResponseResultEntity $ResponseResultEntityFromJson(Map<String, dynamic> json) {
  final ResponseResultEntity responseResultEntity = ResponseResultEntity();
  final ResponseResultData? data =
      jsonConvert.convert<ResponseResultData>(json['data']);
  if (data != null) {
    responseResultEntity.data = data;
  }
  final ResponseResultBench? bench =
      jsonConvert.convert<ResponseResultBench>(json['bench']);
  if (bench != null) {
    responseResultEntity.bench = bench;
  }
  return responseResultEntity;
}

Map<String, dynamic> $ResponseResultEntityToJson(ResponseResultEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseResultData $ResponseResultDataFromJson(Map<String, dynamic> json) {
  final ResponseResultData responseResultData = ResponseResultData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseResultData.message = message;
  }
  return responseResultData;
}

Map<String, dynamic> $ResponseResultDataToJson(ResponseResultData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

ResponseResultBench $ResponseResultBenchFromJson(Map<String, dynamic> json) {
  final ResponseResultBench responseResultBench = ResponseResultBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseResultBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseResultBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseResultBench.format = format;
  }
  return responseResultBench;
}

Map<String, dynamic> $ResponseResultBenchToJson(ResponseResultBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
