import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_concert_module/model/save_concert_response_entity.dart';

SaveConcertResponseEntity $SaveConcertResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SaveConcertResponseEntity saveConcertResponseEntity =
      SaveConcertResponseEntity();
  final SaveConcertResponseData? data =
      jsonConvert.convert<SaveConcertResponseData>(json['data']);
  if (data != null) {
    saveConcertResponseEntity.data = data;
  }
  final SaveConcertResponseBench? bench =
      jsonConvert.convert<SaveConcertResponseBench>(json['bench']);
  if (bench != null) {
    saveConcertResponseEntity.bench = bench;
  }
  return saveConcertResponseEntity;
}

Map<String, dynamic> $SaveConcertResponseEntityToJson(
    SaveConcertResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SaveConcertResponseData $SaveConcertResponseDataFromJson(
    Map<String, dynamic> json) {
  final SaveConcertResponseData saveConcertResponseData =
      SaveConcertResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    saveConcertResponseData.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    saveConcertResponseData.id = id;
  }
  return saveConcertResponseData;
}

Map<String, dynamic> $SaveConcertResponseDataToJson(
    SaveConcertResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  return data;
}

SaveConcertResponseBench $SaveConcertResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SaveConcertResponseBench saveConcertResponseBench =
      SaveConcertResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    saveConcertResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    saveConcertResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    saveConcertResponseBench.format = format;
  }
  return saveConcertResponseBench;
}

Map<String, dynamic> $SaveConcertResponseBenchToJson(
    SaveConcertResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
