import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_review_entity.dart';

GetReviewEntity $GetReviewEntityFromJson(Map<String, dynamic> json) {
  final GetReviewEntity getReviewEntity = GetReviewEntity();
  final GetReviewData? data = jsonConvert.convert<GetReviewData>(json['data']);
  if (data != null) {
    getReviewEntity.data = data;
  }
  final GetReviewBench? bench =
      jsonConvert.convert<GetReviewBench>(json['bench']);
  if (bench != null) {
    getReviewEntity.bench = bench;
  }
  return getReviewEntity;
}

Map<String, dynamic> $GetReviewEntityToJson(GetReviewEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetReviewData $GetReviewDataFromJson(Map<String, dynamic> json) {
  final GetReviewData getReviewData = GetReviewData();
  final GetReviewDataPagination? pagination =
      jsonConvert.convert<GetReviewDataPagination>(json['pagination']);
  if (pagination != null) {
    getReviewData.pagination = pagination;
  }
  final List<GetReviewDataRecord>? record =
      jsonConvert.convertListNotNull<GetReviewDataRecord>(json['record']);
  if (record != null) {
    getReviewData.record = record;
  }
  return getReviewData;
}

Map<String, dynamic> $GetReviewDataToJson(GetReviewData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetReviewDataPagination $GetReviewDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataPagination getReviewDataPagination =
      GetReviewDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getReviewDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getReviewDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getReviewDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getReviewDataPagination.total = total;
  }
  return getReviewDataPagination;
}

Map<String, dynamic> $GetReviewDataPaginationToJson(
    GetReviewDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetReviewDataRecord $GetReviewDataRecordFromJson(Map<String, dynamic> json) {
  final GetReviewDataRecord getReviewDataRecord = GetReviewDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getReviewDataRecord.id = id;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getReviewDataRecord.description = description;
  }
  final GetReviewDataRecordUser? user =
      jsonConvert.convert<GetReviewDataRecordUser>(json['user']);
  if (user != null) {
    getReviewDataRecord.user = user;
  }
  final GetReviewDataRecordStore? store =
      jsonConvert.convert<GetReviewDataRecordStore>(json['store']);
  if (store != null) {
    getReviewDataRecord.store = store;
  }
  final int? rating = jsonConvert.convert<int>(json['rating']);
  if (rating != null) {
    getReviewDataRecord.rating = rating;
  }
  final List<GetReviewDataRecordImages>? images =
      jsonConvert.convertListNotNull<GetReviewDataRecordImages>(json['images']);
  if (images != null) {
    getReviewDataRecord.images = images;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getReviewDataRecord.remark = remark;
  }
  final GetReviewDataRecordCreatedAt? createdAt =
      jsonConvert.convert<GetReviewDataRecordCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getReviewDataRecord.createdAt = createdAt;
  }
  final GetReviewDataRecordUpdatedAt? updatedAt =
      jsonConvert.convert<GetReviewDataRecordUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getReviewDataRecord.updatedAt = updatedAt;
  }
  return getReviewDataRecord;
}

Map<String, dynamic> $GetReviewDataRecordToJson(GetReviewDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['description'] = entity.description;
  data['user'] = entity.user?.toJson();
  data['store'] = entity.store?.toJson();
  data['rating'] = entity.rating;
  data['images'] = entity.images?.map((v) => v.toJson()).toList();
  data['remark'] = entity.remark;
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

GetReviewDataRecordUser $GetReviewDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordUser getReviewDataRecordUser =
      GetReviewDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getReviewDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getReviewDataRecordUser.username = username;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getReviewDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getReviewDataRecordUser.lastName = lastName;
  }
  final GetReviewDataRecordUserImage? image =
      jsonConvert.convert<GetReviewDataRecordUserImage>(json['image']);
  if (image != null) {
    getReviewDataRecordUser.image = image;
  }
  return getReviewDataRecordUser;
}

Map<String, dynamic> $GetReviewDataRecordUserToJson(
    GetReviewDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['image'] = entity.image?.toJson();
  return data;
}

GetReviewDataRecordUserImage $GetReviewDataRecordUserImageFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordUserImage getReviewDataRecordUserImage =
      GetReviewDataRecordUserImage();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getReviewDataRecordUserImage.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getReviewDataRecordUserImage.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getReviewDataRecordUserImage.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getReviewDataRecordUserImage.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getReviewDataRecordUserImage.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getReviewDataRecordUserImage.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getReviewDataRecordUserImage.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getReviewDataRecordUserImage.resizeUrl = resizeUrl;
  }
  return getReviewDataRecordUserImage;
}

Map<String, dynamic> $GetReviewDataRecordUserImageToJson(
    GetReviewDataRecordUserImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetReviewDataRecordStore $GetReviewDataRecordStoreFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordStore getReviewDataRecordStore =
      GetReviewDataRecordStore();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getReviewDataRecordStore.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getReviewDataRecordStore.name = name;
  }
  return getReviewDataRecordStore;
}

Map<String, dynamic> $GetReviewDataRecordStoreToJson(
    GetReviewDataRecordStore entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetReviewDataRecordImages $GetReviewDataRecordImagesFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordImages getReviewDataRecordImages =
      GetReviewDataRecordImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getReviewDataRecordImages.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getReviewDataRecordImages.storeId = storeId;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getReviewDataRecordImages.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getReviewDataRecordImages.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getReviewDataRecordImages.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getReviewDataRecordImages.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getReviewDataRecordImages.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getReviewDataRecordImages.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getReviewDataRecordImages.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getReviewDataRecordImages.url = url;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getReviewDataRecordImages.position = position;
  }
  return getReviewDataRecordImages;
}

Map<String, dynamic> $GetReviewDataRecordImagesToJson(
    GetReviewDataRecordImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['position'] = entity.position;
  return data;
}

GetReviewDataRecordCreatedAt $GetReviewDataRecordCreatedAtFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordCreatedAt getReviewDataRecordCreatedAt =
      GetReviewDataRecordCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getReviewDataRecordCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getReviewDataRecordCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getReviewDataRecordCreatedAt.time = time;
  }
  return getReviewDataRecordCreatedAt;
}

Map<String, dynamic> $GetReviewDataRecordCreatedAtToJson(
    GetReviewDataRecordCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetReviewDataRecordUpdatedAt $GetReviewDataRecordUpdatedAtFromJson(
    Map<String, dynamic> json) {
  final GetReviewDataRecordUpdatedAt getReviewDataRecordUpdatedAt =
      GetReviewDataRecordUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getReviewDataRecordUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getReviewDataRecordUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getReviewDataRecordUpdatedAt.time = time;
  }
  return getReviewDataRecordUpdatedAt;
}

Map<String, dynamic> $GetReviewDataRecordUpdatedAtToJson(
    GetReviewDataRecordUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetReviewBench $GetReviewBenchFromJson(Map<String, dynamic> json) {
  final GetReviewBench getReviewBench = GetReviewBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getReviewBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getReviewBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getReviewBench.format = format;
  }
  return getReviewBench;
}

Map<String, dynamic> $GetReviewBenchToJson(GetReviewBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
