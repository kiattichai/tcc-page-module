import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/get_auto_complete_location_entity.dart';

GetAutoCompleteLocationEntity $GetAutoCompleteLocationEntityFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationEntity getAutoCompleteLocationEntity = GetAutoCompleteLocationEntity();
	final List<GetAutoCompleteLocationPredictions>? predictions = jsonConvert.convertListNotNull<GetAutoCompleteLocationPredictions>(json['predictions']);
	if (predictions != null) {
		getAutoCompleteLocationEntity.predictions = predictions;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		getAutoCompleteLocationEntity.status = status;
	}
	return getAutoCompleteLocationEntity;
}

Map<String, dynamic> $GetAutoCompleteLocationEntityToJson(GetAutoCompleteLocationEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['predictions'] =  entity.predictions?.map((v) => v.toJson()).toList();
	data['status'] = entity.status;
	return data;
}

GetAutoCompleteLocationPredictions $GetAutoCompleteLocationPredictionsFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationPredictions getAutoCompleteLocationPredictions = GetAutoCompleteLocationPredictions();
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		getAutoCompleteLocationPredictions.description = description;
	}
	final List<GetAutoCompleteLocationPredictionsMatchedSubstrings>? matchedSubstrings = jsonConvert.convertListNotNull<GetAutoCompleteLocationPredictionsMatchedSubstrings>(json['matched_substrings']);
	if (matchedSubstrings != null) {
		getAutoCompleteLocationPredictions.matchedSubstrings = matchedSubstrings;
	}
	final String? placeId = jsonConvert.convert<String>(json['place_id']);
	if (placeId != null) {
		getAutoCompleteLocationPredictions.placeId = placeId;
	}
	final String? reference = jsonConvert.convert<String>(json['reference']);
	if (reference != null) {
		getAutoCompleteLocationPredictions.reference = reference;
	}
	final GetAutoCompleteLocationPredictionsStructuredFormatting? structuredFormatting = jsonConvert.convert<GetAutoCompleteLocationPredictionsStructuredFormatting>(json['structured_formatting']);
	if (structuredFormatting != null) {
		getAutoCompleteLocationPredictions.structuredFormatting = structuredFormatting;
	}
	final List<GetAutoCompleteLocationPredictionsTerms>? terms = jsonConvert.convertListNotNull<GetAutoCompleteLocationPredictionsTerms>(json['terms']);
	if (terms != null) {
		getAutoCompleteLocationPredictions.terms = terms;
	}
	final List<String>? types = jsonConvert.convertListNotNull<String>(json['types']);
	if (types != null) {
		getAutoCompleteLocationPredictions.types = types;
	}
	return getAutoCompleteLocationPredictions;
}

Map<String, dynamic> $GetAutoCompleteLocationPredictionsToJson(GetAutoCompleteLocationPredictions entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['description'] = entity.description;
	data['matched_substrings'] =  entity.matchedSubstrings?.map((v) => v.toJson()).toList();
	data['place_id'] = entity.placeId;
	data['reference'] = entity.reference;
	data['structured_formatting'] = entity.structuredFormatting?.toJson();
	data['terms'] =  entity.terms?.map((v) => v.toJson()).toList();
	data['types'] =  entity.types;
	return data;
}

GetAutoCompleteLocationPredictionsMatchedSubstrings $GetAutoCompleteLocationPredictionsMatchedSubstringsFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationPredictionsMatchedSubstrings getAutoCompleteLocationPredictionsMatchedSubstrings = GetAutoCompleteLocationPredictionsMatchedSubstrings();
	final int? length = jsonConvert.convert<int>(json['length']);
	if (length != null) {
		getAutoCompleteLocationPredictionsMatchedSubstrings.length = length;
	}
	final int? offset = jsonConvert.convert<int>(json['offset']);
	if (offset != null) {
		getAutoCompleteLocationPredictionsMatchedSubstrings.offset = offset;
	}
	return getAutoCompleteLocationPredictionsMatchedSubstrings;
}

Map<String, dynamic> $GetAutoCompleteLocationPredictionsMatchedSubstringsToJson(GetAutoCompleteLocationPredictionsMatchedSubstrings entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['length'] = entity.length;
	data['offset'] = entity.offset;
	return data;
}

GetAutoCompleteLocationPredictionsStructuredFormatting $GetAutoCompleteLocationPredictionsStructuredFormattingFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationPredictionsStructuredFormatting getAutoCompleteLocationPredictionsStructuredFormatting = GetAutoCompleteLocationPredictionsStructuredFormatting();
	final String? mainText = jsonConvert.convert<String>(json['main_text']);
	if (mainText != null) {
		getAutoCompleteLocationPredictionsStructuredFormatting.mainText = mainText;
	}
	final List<GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings>? mainTextMatchedSubstrings = jsonConvert.convertListNotNull<GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings>(json['main_text_matched_substrings']);
	if (mainTextMatchedSubstrings != null) {
		getAutoCompleteLocationPredictionsStructuredFormatting.mainTextMatchedSubstrings = mainTextMatchedSubstrings;
	}
	final String? secondaryText = jsonConvert.convert<String>(json['secondary_text']);
	if (secondaryText != null) {
		getAutoCompleteLocationPredictionsStructuredFormatting.secondaryText = secondaryText;
	}
	return getAutoCompleteLocationPredictionsStructuredFormatting;
}

Map<String, dynamic> $GetAutoCompleteLocationPredictionsStructuredFormattingToJson(GetAutoCompleteLocationPredictionsStructuredFormatting entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['main_text'] = entity.mainText;
	data['main_text_matched_substrings'] =  entity.mainTextMatchedSubstrings?.map((v) => v.toJson()).toList();
	data['secondary_text'] = entity.secondaryText;
	return data;
}

GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings $GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstringsFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings getAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings = GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings();
	final int? length = jsonConvert.convert<int>(json['length']);
	if (length != null) {
		getAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings.length = length;
	}
	final int? offset = jsonConvert.convert<int>(json['offset']);
	if (offset != null) {
		getAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings.offset = offset;
	}
	return getAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings;
}

Map<String, dynamic> $GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstringsToJson(GetAutoCompleteLocationPredictionsStructuredFormattingMainTextMatchedSubstrings entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['length'] = entity.length;
	data['offset'] = entity.offset;
	return data;
}

GetAutoCompleteLocationPredictionsTerms $GetAutoCompleteLocationPredictionsTermsFromJson(Map<String, dynamic> json) {
	final GetAutoCompleteLocationPredictionsTerms getAutoCompleteLocationPredictionsTerms = GetAutoCompleteLocationPredictionsTerms();
	final int? offset = jsonConvert.convert<int>(json['offset']);
	if (offset != null) {
		getAutoCompleteLocationPredictionsTerms.offset = offset;
	}
	final String? value = jsonConvert.convert<String>(json['value']);
	if (value != null) {
		getAutoCompleteLocationPredictionsTerms.value = value;
	}
	return getAutoCompleteLocationPredictionsTerms;
}

Map<String, dynamic> $GetAutoCompleteLocationPredictionsTermsToJson(GetAutoCompleteLocationPredictionsTerms entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['offset'] = entity.offset;
	data['value'] = entity.value;
	return data;
}