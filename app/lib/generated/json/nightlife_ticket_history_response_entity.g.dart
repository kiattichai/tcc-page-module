import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/nightlife_ticket_history_response_entity.dart';

NightlifeTicketHistoryResponseEntity
    $NightlifeTicketHistoryResponseEntityFromJson(Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseEntity
      nightlifeTicketHistoryResponseEntity =
      NightlifeTicketHistoryResponseEntity();
  final NightlifeTicketHistoryResponseData? data =
      jsonConvert.convert<NightlifeTicketHistoryResponseData>(json['data']);
  if (data != null) {
    nightlifeTicketHistoryResponseEntity.data = data;
  }
  final NightlifeTicketHistoryResponseBench? bench =
      jsonConvert.convert<NightlifeTicketHistoryResponseBench>(json['bench']);
  if (bench != null) {
    nightlifeTicketHistoryResponseEntity.bench = bench;
  }
  return nightlifeTicketHistoryResponseEntity;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseEntityToJson(
    NightlifeTicketHistoryResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

NightlifeTicketHistoryResponseData $NightlifeTicketHistoryResponseDataFromJson(
    Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseData nightlifeTicketHistoryResponseData =
      NightlifeTicketHistoryResponseData();
  final NightlifeTicketHistoryResponseDataRecord? record = jsonConvert
      .convert<NightlifeTicketHistoryResponseDataRecord>(json['record']);
  if (record != null) {
    nightlifeTicketHistoryResponseData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    nightlifeTicketHistoryResponseData.cache = cache;
  }
  return nightlifeTicketHistoryResponseData;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseDataToJson(
    NightlifeTicketHistoryResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.toJson();
  data['cache'] = entity.cache;
  return data;
}

NightlifeTicketHistoryResponseDataRecord
    $NightlifeTicketHistoryResponseDataRecordFromJson(
        Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseDataRecord
      nightlifeTicketHistoryResponseDataRecord =
      NightlifeTicketHistoryResponseDataRecord();
  final NightlifeTicketHistoryResponseDataRecordPagination? pagination =
      jsonConvert.convert<NightlifeTicketHistoryResponseDataRecordPagination>(
          json['pagination']);
  if (pagination != null) {
    nightlifeTicketHistoryResponseDataRecord.pagination = pagination;
  }
  final List<NightlifeTicketHistoryResponseDataRecordData>? data = jsonConvert
      .convertListNotNull<NightlifeTicketHistoryResponseDataRecordData>(
          json['data']);
  if (data != null) {
    nightlifeTicketHistoryResponseDataRecord.data = data;
  }
  return nightlifeTicketHistoryResponseDataRecord;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseDataRecordToJson(
    NightlifeTicketHistoryResponseDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['data'] = entity.data?.map((v) => v.toJson()).toList();
  return data;
}

NightlifeTicketHistoryResponseDataRecordPagination
    $NightlifeTicketHistoryResponseDataRecordPaginationFromJson(
        Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseDataRecordPagination
      nightlifeTicketHistoryResponseDataRecordPagination =
      NightlifeTicketHistoryResponseDataRecordPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    nightlifeTicketHistoryResponseDataRecordPagination.currentPage =
        currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    nightlifeTicketHistoryResponseDataRecordPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    nightlifeTicketHistoryResponseDataRecordPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    nightlifeTicketHistoryResponseDataRecordPagination.total = total;
  }
  return nightlifeTicketHistoryResponseDataRecordPagination;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseDataRecordPaginationToJson(
    NightlifeTicketHistoryResponseDataRecordPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

NightlifeTicketHistoryResponseDataRecordData
    $NightlifeTicketHistoryResponseDataRecordDataFromJson(
        Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseDataRecordData
      nightlifeTicketHistoryResponseDataRecordData =
      NightlifeTicketHistoryResponseDataRecordData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    nightlifeTicketHistoryResponseDataRecordData.id = id;
  }
  final int? userId = jsonConvert.convert<int>(json['user_id']);
  if (userId != null) {
    nightlifeTicketHistoryResponseDataRecordData.userId = userId;
  }
  final int? ticketId = jsonConvert.convert<int>(json['ticket_id']);
  if (ticketId != null) {
    nightlifeTicketHistoryResponseDataRecordData.ticketId = ticketId;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    nightlifeTicketHistoryResponseDataRecordData.storeId = storeId;
  }
  final String? storeName = jsonConvert.convert<String>(json['store_name']);
  if (storeName != null) {
    nightlifeTicketHistoryResponseDataRecordData.storeName = storeName;
  }
  final int? productId = jsonConvert.convert<int>(json['product_id']);
  if (productId != null) {
    nightlifeTicketHistoryResponseDataRecordData.productId = productId;
  }
  final int? productVariantId =
      jsonConvert.convert<int>(json['product_variant_id']);
  if (productVariantId != null) {
    nightlifeTicketHistoryResponseDataRecordData.productVariantId =
        productVariantId;
  }
  final NightlifeTicketHistoryResponseDataRecordDataCreatedAt? createdAt =
      jsonConvert
          .convert<NightlifeTicketHistoryResponseDataRecordDataCreatedAt>(
              json['created_at']);
  if (createdAt != null) {
    nightlifeTicketHistoryResponseDataRecordData.createdAt = createdAt;
  }
  final NightlifeTicketHistoryResponseDataRecordDataUpdatedAt? updatedAt =
      jsonConvert
          .convert<NightlifeTicketHistoryResponseDataRecordDataUpdatedAt>(
              json['updated_at']);
  if (updatedAt != null) {
    nightlifeTicketHistoryResponseDataRecordData.updatedAt = updatedAt;
  }
  return nightlifeTicketHistoryResponseDataRecordData;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseDataRecordDataToJson(
    NightlifeTicketHistoryResponseDataRecordData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['user_id'] = entity.userId;
  data['ticket_id'] = entity.ticketId;
  data['store_id'] = entity.storeId;
  data['store_name'] = entity.storeName;
  data['product_id'] = entity.productId;
  data['product_variant_id'] = entity.productVariantId;
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

NightlifeTicketHistoryResponseDataRecordDataCreatedAt
    $NightlifeTicketHistoryResponseDataRecordDataCreatedAtFromJson(
        Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseDataRecordDataCreatedAt
      nightlifeTicketHistoryResponseDataRecordDataCreatedAt =
      NightlifeTicketHistoryResponseDataRecordDataCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    nightlifeTicketHistoryResponseDataRecordDataCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    nightlifeTicketHistoryResponseDataRecordDataCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    nightlifeTicketHistoryResponseDataRecordDataCreatedAt.time = time;
  }
  return nightlifeTicketHistoryResponseDataRecordDataCreatedAt;
}

Map<String, dynamic>
    $NightlifeTicketHistoryResponseDataRecordDataCreatedAtToJson(
        NightlifeTicketHistoryResponseDataRecordDataCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

NightlifeTicketHistoryResponseDataRecordDataUpdatedAt
    $NightlifeTicketHistoryResponseDataRecordDataUpdatedAtFromJson(
        Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseDataRecordDataUpdatedAt
      nightlifeTicketHistoryResponseDataRecordDataUpdatedAt =
      NightlifeTicketHistoryResponseDataRecordDataUpdatedAt();
  return nightlifeTicketHistoryResponseDataRecordDataUpdatedAt;
}

Map<String, dynamic>
    $NightlifeTicketHistoryResponseDataRecordDataUpdatedAtToJson(
        NightlifeTicketHistoryResponseDataRecordDataUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

NightlifeTicketHistoryResponseBench
    $NightlifeTicketHistoryResponseBenchFromJson(Map<String, dynamic> json) {
  final NightlifeTicketHistoryResponseBench
      nightlifeTicketHistoryResponseBench =
      NightlifeTicketHistoryResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    nightlifeTicketHistoryResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    nightlifeTicketHistoryResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    nightlifeTicketHistoryResponseBench.format = format;
  }
  return nightlifeTicketHistoryResponseBench;
}

Map<String, dynamic> $NightlifeTicketHistoryResponseBenchToJson(
    NightlifeTicketHistoryResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
