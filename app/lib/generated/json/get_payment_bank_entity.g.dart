import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_payment_bank_entity.dart';

GetPaymentBankEntity $GetPaymentBankEntityFromJson(Map<String, dynamic> json) {
  final GetPaymentBankEntity getPaymentBankEntity = GetPaymentBankEntity();
  final GetPaymentBankData? data =
      jsonConvert.convert<GetPaymentBankData>(json['data']);
  if (data != null) {
    getPaymentBankEntity.data = data;
  }
  final GetPaymentBankBench? bench =
      jsonConvert.convert<GetPaymentBankBench>(json['bench']);
  if (bench != null) {
    getPaymentBankEntity.bench = bench;
  }
  return getPaymentBankEntity;
}

Map<String, dynamic> $GetPaymentBankEntityToJson(GetPaymentBankEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetPaymentBankData $GetPaymentBankDataFromJson(Map<String, dynamic> json) {
  final GetPaymentBankData getPaymentBankData = GetPaymentBankData();
  final List<GetPaymentBankDataRecord>? record =
      jsonConvert.convertListNotNull<GetPaymentBankDataRecord>(json['record']);
  if (record != null) {
    getPaymentBankData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getPaymentBankData.cache = cache;
  }
  return getPaymentBankData;
}

Map<String, dynamic> $GetPaymentBankDataToJson(GetPaymentBankData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetPaymentBankDataRecord $GetPaymentBankDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetPaymentBankDataRecord getPaymentBankDataRecord =
      GetPaymentBankDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPaymentBankDataRecord.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getPaymentBankDataRecord.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPaymentBankDataRecord.name = name;
  }
  return getPaymentBankDataRecord;
}

Map<String, dynamic> $GetPaymentBankDataRecordToJson(
    GetPaymentBankDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  return data;
}

GetPaymentBankBench $GetPaymentBankBenchFromJson(Map<String, dynamic> json) {
  final GetPaymentBankBench getPaymentBankBench = GetPaymentBankBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getPaymentBankBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getPaymentBankBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getPaymentBankBench.format = format;
  }
  return getPaymentBankBench;
}

Map<String, dynamic> $GetPaymentBankBenchToJson(GetPaymentBankBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
