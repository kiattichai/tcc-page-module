import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/errors/base_error_entity.dart';
import 'package:app/api/errors/base_error_public_api_entity.dart';

import 'package:dio/dio.dart';

BaseErrorEntity $BaseErrorEntityFromJson(Map<String, dynamic> json) {
  final BaseErrorEntity baseErrorEntity = BaseErrorEntity();
  final BaseError? error = jsonConvert.convert<BaseError>(json['error']);
  if (error != null) {
    baseErrorEntity.error = error;
  }
  return baseErrorEntity;
}

Map<String, dynamic> $BaseErrorEntityToJson(BaseErrorEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['error'] = entity.error?.toJson();
  return data;
}

BaseError $BaseErrorFromJson(Map<String, dynamic> json) {
  final BaseError baseError = BaseError();
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    baseError.type = type;
  }
  final int? code = jsonConvert.convert<int>(json['code']);
  if (code != null) {
    baseError.code = code;
  }
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    baseError.message = message;
  }
  final String? field = jsonConvert.convert<String>(json['field']);
  if (field != null) {
    baseError.field = field;
  }
  final String? line = jsonConvert.convert<String>(json['line']);
  if (line != null) {
    baseError.line = line;
  }
  return baseError;
}

Map<String, dynamic> $BaseErrorToJson(BaseError entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['type'] = entity.type;
  data['code'] = entity.code;
  data['message'] = entity.message;
  data['field'] = entity.field;
  data['line'] = entity.line;
  return data;
}
