import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model_from_native/near_by_store_entity.dart';

NearByStoreEntity $NearByStoreEntityFromJson(Map<String, dynamic> json) {
  final NearByStoreEntity nearByStoreEntity = NearByStoreEntity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    nearByStoreEntity.id = id;
  }
  final NearByStoreImages? images =
      jsonConvert.convert<NearByStoreImages>(json['images']);
  if (images != null) {
    nearByStoreEntity.images = images;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    nearByStoreEntity.name = name;
  }
  final NearByStoreSection? section =
      jsonConvert.convert<NearByStoreSection>(json['section']);
  if (section != null) {
    nearByStoreEntity.section = section;
  }
  final NearByStoreType? type =
      jsonConvert.convert<NearByStoreType>(json['type']);
  if (type != null) {
    nearByStoreEntity.type = type;
  }
  final NearByStoreVenue? venue =
      jsonConvert.convert<NearByStoreVenue>(json['venue']);
  if (venue != null) {
    nearByStoreEntity.venue = venue;
  }
  return nearByStoreEntity;
}

Map<String, dynamic> $NearByStoreEntityToJson(NearByStoreEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['images'] = entity.images?.toJson();
  data['name'] = entity.name;
  data['section'] = entity.section?.toJson();
  data['type'] = entity.type?.toJson();
  data['venue'] = entity.venue?.toJson();
  return data;
}

NearByStoreImages $NearByStoreImagesFromJson(Map<String, dynamic> json) {
  final NearByStoreImages nearByStoreImages = NearByStoreImages();
  final NearByStoreImagesLogo? logo =
      jsonConvert.convert<NearByStoreImagesLogo>(json['logo']);
  if (logo != null) {
    nearByStoreImages.logo = logo;
  }
  return nearByStoreImages;
}

Map<String, dynamic> $NearByStoreImagesToJson(NearByStoreImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['logo'] = entity.logo?.toJson();
  return data;
}

NearByStoreImagesLogo $NearByStoreImagesLogoFromJson(
    Map<String, dynamic> json) {
  final NearByStoreImagesLogo nearByStoreImagesLogo = NearByStoreImagesLogo();
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    nearByStoreImagesLogo.albumId = albumId;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    nearByStoreImagesLogo.height = height;
  }
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    nearByStoreImagesLogo.id = id;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    nearByStoreImagesLogo.mime = mime;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    nearByStoreImagesLogo.name = name;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    nearByStoreImagesLogo.position = position;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    nearByStoreImagesLogo.size = size;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    nearByStoreImagesLogo.tag = tag;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    nearByStoreImagesLogo.url = url;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    nearByStoreImagesLogo.width = width;
  }
  return nearByStoreImagesLogo;
}

Map<String, dynamic> $NearByStoreImagesLogoToJson(
    NearByStoreImagesLogo entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['album_id'] = entity.albumId;
  data['height'] = entity.height;
  data['id'] = entity.id;
  data['mime'] = entity.mime;
  data['name'] = entity.name;
  data['position'] = entity.position;
  data['size'] = entity.size;
  data['tag'] = entity.tag;
  data['url'] = entity.url;
  data['width'] = entity.width;
  return data;
}

NearByStoreSection $NearByStoreSectionFromJson(Map<String, dynamic> json) {
  final NearByStoreSection nearByStoreSection = NearByStoreSection();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    nearByStoreSection.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    nearByStoreSection.text = text;
  }
  return nearByStoreSection;
}

Map<String, dynamic> $NearByStoreSectionToJson(NearByStoreSection entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

NearByStoreType $NearByStoreTypeFromJson(Map<String, dynamic> json) {
  final NearByStoreType nearByStoreType = NearByStoreType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    nearByStoreType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    nearByStoreType.text = text;
  }
  return nearByStoreType;
}

Map<String, dynamic> $NearByStoreTypeToJson(NearByStoreType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

NearByStoreVenue $NearByStoreVenueFromJson(Map<String, dynamic> json) {
  final NearByStoreVenue nearByStoreVenue = NearByStoreVenue();
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    nearByStoreVenue.address = address;
  }
  final double? distance = jsonConvert.convert<double>(json['distance']);
  if (distance != null) {
    nearByStoreVenue.distance = distance;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    nearByStoreVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    nearByStoreVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    nearByStoreVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    nearByStoreVenue.name = name;
  }
  return nearByStoreVenue;
}

Map<String, dynamic> $NearByStoreVenueToJson(NearByStoreVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['address'] = entity.address;
  data['distance'] = entity.distance;
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  return data;
}
