import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_review_image_entity.dart';

SaveReviewImageEntity $SaveReviewImageEntityFromJson(
    Map<String, dynamic> json) {
  final SaveReviewImageEntity saveReviewImageEntity = SaveReviewImageEntity();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    saveReviewImageEntity.name = name;
  }
  final String? data = jsonConvert.convert<String>(json['data']);
  if (data != null) {
    saveReviewImageEntity.data = data;
  }
  return saveReviewImageEntity;
}

Map<String, dynamic> $SaveReviewImageEntityToJson(
    SaveReviewImageEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['data'] = entity.data;
  return data;
}
