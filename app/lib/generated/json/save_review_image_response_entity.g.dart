import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_review_image_response_entity.dart';

SaveReviewImageResponseEntity $SaveReviewImageResponseEntityFromJson(Map<String, dynamic> json) {
	final SaveReviewImageResponseEntity saveReviewImageResponseEntity = SaveReviewImageResponseEntity();
	final SaveReviewImageResponseData? data = jsonConvert.convert<SaveReviewImageResponseData>(json['data']);
	if (data != null) {
		saveReviewImageResponseEntity.data = data;
	}
	final SaveReviewImageResponseBench? bench = jsonConvert.convert<SaveReviewImageResponseBench>(json['bench']);
	if (bench != null) {
		saveReviewImageResponseEntity.bench = bench;
	}
	return saveReviewImageResponseEntity;
}

Map<String, dynamic> $SaveReviewImageResponseEntityToJson(SaveReviewImageResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

SaveReviewImageResponseData $SaveReviewImageResponseDataFromJson(Map<String, dynamic> json) {
	final SaveReviewImageResponseData saveReviewImageResponseData = SaveReviewImageResponseData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		saveReviewImageResponseData.message = message;
	}
	final SaveReviewImageResponseDataUpload? upload = jsonConvert.convert<SaveReviewImageResponseDataUpload>(json['upload']);
	if (upload != null) {
		saveReviewImageResponseData.upload = upload;
	}
	return saveReviewImageResponseData;
}

Map<String, dynamic> $SaveReviewImageResponseDataToJson(SaveReviewImageResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	data['upload'] = entity.upload?.toJson();
	return data;
}

SaveReviewImageResponseDataUpload $SaveReviewImageResponseDataUploadFromJson(Map<String, dynamic> json) {
	final SaveReviewImageResponseDataUpload saveReviewImageResponseDataUpload = SaveReviewImageResponseDataUpload();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		saveReviewImageResponseDataUpload.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		saveReviewImageResponseDataUpload.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		saveReviewImageResponseDataUpload.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		saveReviewImageResponseDataUpload.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		saveReviewImageResponseDataUpload.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		saveReviewImageResponseDataUpload.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		saveReviewImageResponseDataUpload.url = url;
	}
	final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
	if (resizeUrl != null) {
		saveReviewImageResponseDataUpload.resizeUrl = resizeUrl;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		saveReviewImageResponseDataUpload.position = position;
	}
	return saveReviewImageResponseDataUpload;
}

Map<String, dynamic> $SaveReviewImageResponseDataUploadToJson(SaveReviewImageResponseDataUpload entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	data['position'] = entity.position;
	return data;
}

SaveReviewImageResponseBench $SaveReviewImageResponseBenchFromJson(Map<String, dynamic> json) {
	final SaveReviewImageResponseBench saveReviewImageResponseBench = SaveReviewImageResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		saveReviewImageResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		saveReviewImageResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		saveReviewImageResponseBench.format = format;
	}
	return saveReviewImageResponseBench;
}

Map<String, dynamic> $SaveReviewImageResponseBenchToJson(SaveReviewImageResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}