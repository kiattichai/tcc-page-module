import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/invoice_response_entity.dart';

InvoiceResponseEntity $InvoiceResponseEntityFromJson(
    Map<String, dynamic> json) {
  final InvoiceResponseEntity invoiceResponseEntity = InvoiceResponseEntity();
  final InvoiceResponseData? data =
      jsonConvert.convert<InvoiceResponseData>(json['data']);
  if (data != null) {
    invoiceResponseEntity.data = data;
  }
  final InvoiceResponseBench? bench =
      jsonConvert.convert<InvoiceResponseBench>(json['bench']);
  if (bench != null) {
    invoiceResponseEntity.bench = bench;
  }
  return invoiceResponseEntity;
}

Map<String, dynamic> $InvoiceResponseEntityToJson(
    InvoiceResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

InvoiceResponseData $InvoiceResponseDataFromJson(Map<String, dynamic> json) {
  final InvoiceResponseData invoiceResponseData = InvoiceResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    invoiceResponseData.message = message;
  }
  return invoiceResponseData;
}

Map<String, dynamic> $InvoiceResponseDataToJson(InvoiceResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

InvoiceResponseBench $InvoiceResponseBenchFromJson(Map<String, dynamic> json) {
  final InvoiceResponseBench invoiceResponseBench = InvoiceResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    invoiceResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    invoiceResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    invoiceResponseBench.format = format;
  }
  return invoiceResponseBench;
}

Map<String, dynamic> $InvoiceResponseBenchToJson(InvoiceResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
