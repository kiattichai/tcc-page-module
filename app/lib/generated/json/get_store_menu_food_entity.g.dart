import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/public_page_module/model/get_store_menu_food_entity.dart';

GetStoreMenuFoodEntity $GetStoreMenuFoodEntityFromJson(
    Map<String, dynamic> json) {
  final GetStoreMenuFoodEntity getStoreMenuFoodEntity =
      GetStoreMenuFoodEntity();
  final GetStoreMenuFoodData? data =
      jsonConvert.convert<GetStoreMenuFoodData>(json['data']);
  if (data != null) {
    getStoreMenuFoodEntity.data = data;
  }
  final GetStoreMenuFoodBench? bench =
      jsonConvert.convert<GetStoreMenuFoodBench>(json['bench']);
  if (bench != null) {
    getStoreMenuFoodEntity.bench = bench;
  }
  return getStoreMenuFoodEntity;
}

Map<String, dynamic> $GetStoreMenuFoodEntityToJson(
    GetStoreMenuFoodEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetStoreMenuFoodData $GetStoreMenuFoodDataFromJson(Map<String, dynamic> json) {
  final GetStoreMenuFoodData getStoreMenuFoodData = GetStoreMenuFoodData();
  final GetStoreMenuFoodDataPagination? pagination =
      jsonConvert.convert<GetStoreMenuFoodDataPagination>(json['pagination']);
  if (pagination != null) {
    getStoreMenuFoodData.pagination = pagination;
  }
  final List<GetStoreMenuFoodDataRecord>? record = jsonConvert
      .convertListNotNull<GetStoreMenuFoodDataRecord>(json['record']);
  if (record != null) {
    getStoreMenuFoodData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getStoreMenuFoodData.cache = cache;
  }
  return getStoreMenuFoodData;
}

Map<String, dynamic> $GetStoreMenuFoodDataToJson(GetStoreMenuFoodData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetStoreMenuFoodDataPagination $GetStoreMenuFoodDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetStoreMenuFoodDataPagination getStoreMenuFoodDataPagination =
      GetStoreMenuFoodDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getStoreMenuFoodDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getStoreMenuFoodDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getStoreMenuFoodDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getStoreMenuFoodDataPagination.total = total;
  }
  return getStoreMenuFoodDataPagination;
}

Map<String, dynamic> $GetStoreMenuFoodDataPaginationToJson(
    GetStoreMenuFoodDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetStoreMenuFoodDataRecord $GetStoreMenuFoodDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetStoreMenuFoodDataRecord getStoreMenuFoodDataRecord =
      GetStoreMenuFoodDataRecord();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getStoreMenuFoodDataRecord.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getStoreMenuFoodDataRecord.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getStoreMenuFoodDataRecord.tag = tag;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreMenuFoodDataRecord.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getStoreMenuFoodDataRecord.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getStoreMenuFoodDataRecord.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getStoreMenuFoodDataRecord.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getStoreMenuFoodDataRecord.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getStoreMenuFoodDataRecord.url = url;
  }
  return getStoreMenuFoodDataRecord;
}

Map<String, dynamic> $GetStoreMenuFoodDataRecordToJson(
    GetStoreMenuFoodDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetStoreMenuFoodBench $GetStoreMenuFoodBenchFromJson(
    Map<String, dynamic> json) {
  final GetStoreMenuFoodBench getStoreMenuFoodBench = GetStoreMenuFoodBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getStoreMenuFoodBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getStoreMenuFoodBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getStoreMenuFoodBench.format = format;
  }
  return getStoreMenuFoodBench;
}

Map<String, dynamic> $GetStoreMenuFoodBenchToJson(
    GetStoreMenuFoodBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
