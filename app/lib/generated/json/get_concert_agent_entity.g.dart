import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/concert_agent_module/model/get_concert_agent_entity.dart';

GetConcertAgentEntity $GetConcertAgentEntityFromJson(Map<String, dynamic> json) {
	final GetConcertAgentEntity getConcertAgentEntity = GetConcertAgentEntity();
	final GetConcertAgentData? data = jsonConvert.convert<GetConcertAgentData>(json['data']);
	if (data != null) {
		getConcertAgentEntity.data = data;
	}
	final GetConcertAgentBench? bench = jsonConvert.convert<GetConcertAgentBench>(json['bench']);
	if (bench != null) {
		getConcertAgentEntity.bench = bench;
	}
	return getConcertAgentEntity;
}

Map<String, dynamic> $GetConcertAgentEntityToJson(GetConcertAgentEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetConcertAgentData $GetConcertAgentDataFromJson(Map<String, dynamic> json) {
	final GetConcertAgentData getConcertAgentData = GetConcertAgentData();
	final GetConcertAgentDataPagination? pagination = jsonConvert.convert<GetConcertAgentDataPagination>(json['pagination']);
	if (pagination != null) {
		getConcertAgentData.pagination = pagination;
	}
	final List<GetConcertAgentDataRecord>? record = jsonConvert.convertListNotNull<GetConcertAgentDataRecord>(json['record']);
	if (record != null) {
		getConcertAgentData.record = record;
	}
	return getConcertAgentData;
}

Map<String, dynamic> $GetConcertAgentDataToJson(GetConcertAgentData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['pagination'] = entity.pagination?.toJson();
	data['record'] =  entity.record?.map((v) => v.toJson()).toList();
	return data;
}

GetConcertAgentDataPagination $GetConcertAgentDataPaginationFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataPagination getConcertAgentDataPagination = GetConcertAgentDataPagination();
	final int? currentPage = jsonConvert.convert<int>(json['current_page']);
	if (currentPage != null) {
		getConcertAgentDataPagination.currentPage = currentPage;
	}
	final int? lastPage = jsonConvert.convert<int>(json['last_page']);
	if (lastPage != null) {
		getConcertAgentDataPagination.lastPage = lastPage;
	}
	final int? limit = jsonConvert.convert<int>(json['limit']);
	if (limit != null) {
		getConcertAgentDataPagination.limit = limit;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getConcertAgentDataPagination.total = total;
	}
	return getConcertAgentDataPagination;
}

Map<String, dynamic> $GetConcertAgentDataPaginationToJson(GetConcertAgentDataPagination entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

GetConcertAgentDataRecord $GetConcertAgentDataRecordFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecord getConcertAgentDataRecord = GetConcertAgentDataRecord();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecord.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertAgentDataRecord.storeId = storeId;
	}
	final bool? isCommissioner = jsonConvert.convert<bool>(json['is_commissioner']);
	if (isCommissioner != null) {
		getConcertAgentDataRecord.isCommissioner = isCommissioner;
	}
	final GetConcertAgentDataRecordProduct? product = jsonConvert.convert<GetConcertAgentDataRecordProduct>(json['product']);
	if (product != null) {
		getConcertAgentDataRecord.product = product;
	}
	final String? afLink = jsonConvert.convert<String>(json['af_link']);
	if (afLink != null) {
		getConcertAgentDataRecord.afLink = afLink;
	}
	final GetConcertAgentDataRecordStore? store = jsonConvert.convert<GetConcertAgentDataRecordStore>(json['store']);
	if (store != null) {
		getConcertAgentDataRecord.store = store;
	}
	return getConcertAgentDataRecord;
}

Map<String, dynamic> $GetConcertAgentDataRecordToJson(GetConcertAgentDataRecord entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['is_commissioner'] = entity.isCommissioner;
	data['product'] = entity.product?.toJson();
	data['af_link'] = entity.afLink;
	data['store'] = entity.store?.toJson();
	return data;
}

GetConcertAgentDataRecordProduct $GetConcertAgentDataRecordProductFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProduct getConcertAgentDataRecordProduct = GetConcertAgentDataRecordProduct();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProduct.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProduct.name = name;
	}
	final GetConcertAgentDataRecordProductVenue? venue = jsonConvert.convert<GetConcertAgentDataRecordProductVenue>(json['venue']);
	if (venue != null) {
		getConcertAgentDataRecordProduct.venue = venue;
	}
	final List<GetConcertAgentDataRecordProductImages>? images = jsonConvert.convertListNotNull<GetConcertAgentDataRecordProductImages>(json['images']);
	if (images != null) {
		getConcertAgentDataRecordProduct.images = images;
	}
	final GetConcertAgentDataRecordProductShowTime? showTime = jsonConvert.convert<GetConcertAgentDataRecordProductShowTime>(json['show_time']);
	if (showTime != null) {
		getConcertAgentDataRecordProduct.showTime = showTime;
	}
	return getConcertAgentDataRecordProduct;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductToJson(GetConcertAgentDataRecordProduct entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['venue'] = entity.venue?.toJson();
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['show_time'] = entity.showTime?.toJson();
	return data;
}

GetConcertAgentDataRecordProductVenue $GetConcertAgentDataRecordProductVenueFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductVenue getConcertAgentDataRecordProductVenue = GetConcertAgentDataRecordProductVenue();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductVenue.id = id;
	}
	final double? lat = jsonConvert.convert<double>(json['lat']);
	if (lat != null) {
		getConcertAgentDataRecordProductVenue.lat = lat;
	}
	final double? long = jsonConvert.convert<double>(json['long']);
	if (long != null) {
		getConcertAgentDataRecordProductVenue.long = long;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductVenue.name = name;
	}
	final dynamic? address = jsonConvert.convert<dynamic>(json['address']);
	if (address != null) {
		getConcertAgentDataRecordProductVenue.address = address;
	}
	final GetConcertAgentDataRecordProductVenueCountry? country = jsonConvert.convert<GetConcertAgentDataRecordProductVenueCountry>(json['country']);
	if (country != null) {
		getConcertAgentDataRecordProductVenue.country = country;
	}
	final GetConcertAgentDataRecordProductVenueProvince? province = jsonConvert.convert<GetConcertAgentDataRecordProductVenueProvince>(json['province']);
	if (province != null) {
		getConcertAgentDataRecordProductVenue.province = province;
	}
	final GetConcertAgentDataRecordProductVenueCity? city = jsonConvert.convert<GetConcertAgentDataRecordProductVenueCity>(json['city']);
	if (city != null) {
		getConcertAgentDataRecordProductVenue.city = city;
	}
	final GetConcertAgentDataRecordProductVenueDistrict? district = jsonConvert.convert<GetConcertAgentDataRecordProductVenueDistrict>(json['district']);
	if (district != null) {
		getConcertAgentDataRecordProductVenue.district = district;
	}
	final int? zipCode = jsonConvert.convert<int>(json['zip_code']);
	if (zipCode != null) {
		getConcertAgentDataRecordProductVenue.zipCode = zipCode;
	}
	return getConcertAgentDataRecordProductVenue;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductVenueToJson(GetConcertAgentDataRecordProductVenue entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['name'] = entity.name;
	data['address'] = entity.address;
	data['country'] = entity.country?.toJson();
	data['province'] = entity.province?.toJson();
	data['city'] = entity.city?.toJson();
	data['district'] = entity.district?.toJson();
	data['zip_code'] = entity.zipCode;
	return data;
}

GetConcertAgentDataRecordProductVenueCountry $GetConcertAgentDataRecordProductVenueCountryFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductVenueCountry getConcertAgentDataRecordProductVenueCountry = GetConcertAgentDataRecordProductVenueCountry();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductVenueCountry.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductVenueCountry.name = name;
	}
	return getConcertAgentDataRecordProductVenueCountry;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductVenueCountryToJson(GetConcertAgentDataRecordProductVenueCountry entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertAgentDataRecordProductVenueProvince $GetConcertAgentDataRecordProductVenueProvinceFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductVenueProvince getConcertAgentDataRecordProductVenueProvince = GetConcertAgentDataRecordProductVenueProvince();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductVenueProvince.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductVenueProvince.name = name;
	}
	return getConcertAgentDataRecordProductVenueProvince;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductVenueProvinceToJson(GetConcertAgentDataRecordProductVenueProvince entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertAgentDataRecordProductVenueCity $GetConcertAgentDataRecordProductVenueCityFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductVenueCity getConcertAgentDataRecordProductVenueCity = GetConcertAgentDataRecordProductVenueCity();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductVenueCity.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductVenueCity.name = name;
	}
	return getConcertAgentDataRecordProductVenueCity;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductVenueCityToJson(GetConcertAgentDataRecordProductVenueCity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertAgentDataRecordProductVenueDistrict $GetConcertAgentDataRecordProductVenueDistrictFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductVenueDistrict getConcertAgentDataRecordProductVenueDistrict = GetConcertAgentDataRecordProductVenueDistrict();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductVenueDistrict.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductVenueDistrict.name = name;
	}
	return getConcertAgentDataRecordProductVenueDistrict;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductVenueDistrictToJson(GetConcertAgentDataRecordProductVenueDistrict entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

GetConcertAgentDataRecordProductImages $GetConcertAgentDataRecordProductImagesFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductImages getConcertAgentDataRecordProductImages = GetConcertAgentDataRecordProductImages();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordProductImages.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getConcertAgentDataRecordProductImages.storeId = storeId;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getConcertAgentDataRecordProductImages.tag = tag;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getConcertAgentDataRecordProductImages.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordProductImages.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getConcertAgentDataRecordProductImages.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getConcertAgentDataRecordProductImages.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getConcertAgentDataRecordProductImages.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getConcertAgentDataRecordProductImages.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getConcertAgentDataRecordProductImages.url = url;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getConcertAgentDataRecordProductImages.position = position;
	}
	return getConcertAgentDataRecordProductImages;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductImagesToJson(GetConcertAgentDataRecordProductImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

GetConcertAgentDataRecordProductShowTime $GetConcertAgentDataRecordProductShowTimeFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordProductShowTime getConcertAgentDataRecordProductShowTime = GetConcertAgentDataRecordProductShowTime();
	final String? start = jsonConvert.convert<String>(json['start']);
	if (start != null) {
		getConcertAgentDataRecordProductShowTime.start = start;
	}
	final String? end = jsonConvert.convert<String>(json['end']);
	if (end != null) {
		getConcertAgentDataRecordProductShowTime.end = end;
	}
	final String? textFull = jsonConvert.convert<String>(json['text_full']);
	if (textFull != null) {
		getConcertAgentDataRecordProductShowTime.textFull = textFull;
	}
	final String? textShort = jsonConvert.convert<String>(json['text_short']);
	if (textShort != null) {
		getConcertAgentDataRecordProductShowTime.textShort = textShort;
	}
	final String? textShortDate = jsonConvert.convert<String>(json['text_short_date']);
	if (textShortDate != null) {
		getConcertAgentDataRecordProductShowTime.textShortDate = textShortDate;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		getConcertAgentDataRecordProductShowTime.status = status;
	}
	final String? statusText = jsonConvert.convert<String>(json['status_text']);
	if (statusText != null) {
		getConcertAgentDataRecordProductShowTime.statusText = statusText;
	}
	return getConcertAgentDataRecordProductShowTime;
}

Map<String, dynamic> $GetConcertAgentDataRecordProductShowTimeToJson(GetConcertAgentDataRecordProductShowTime entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['start'] = entity.start;
	data['end'] = entity.end;
	data['text_full'] = entity.textFull;
	data['text_short'] = entity.textShort;
	data['text_short_date'] = entity.textShortDate;
	data['status'] = entity.status;
	data['status_text'] = entity.statusText;
	return data;
}

GetConcertAgentDataRecordStore $GetConcertAgentDataRecordStoreFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordStore getConcertAgentDataRecordStore = GetConcertAgentDataRecordStore();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getConcertAgentDataRecordStore.name = name;
	}
	final String? slug = jsonConvert.convert<String>(json['slug']);
	if (slug != null) {
		getConcertAgentDataRecordStore.slug = slug;
	}
	final GetConcertAgentDataRecordStoreSection? section = jsonConvert.convert<GetConcertAgentDataRecordStoreSection>(json['section']);
	if (section != null) {
		getConcertAgentDataRecordStore.section = section;
	}
	return getConcertAgentDataRecordStore;
}

Map<String, dynamic> $GetConcertAgentDataRecordStoreToJson(GetConcertAgentDataRecordStore entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	data['slug'] = entity.slug;
	data['section'] = entity.section?.toJson();
	return data;
}

GetConcertAgentDataRecordStoreSection $GetConcertAgentDataRecordStoreSectionFromJson(Map<String, dynamic> json) {
	final GetConcertAgentDataRecordStoreSection getConcertAgentDataRecordStoreSection = GetConcertAgentDataRecordStoreSection();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getConcertAgentDataRecordStoreSection.id = id;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		getConcertAgentDataRecordStoreSection.text = text;
	}
	return getConcertAgentDataRecordStoreSection;
}

Map<String, dynamic> $GetConcertAgentDataRecordStoreSectionToJson(GetConcertAgentDataRecordStoreSection entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

GetConcertAgentBench $GetConcertAgentBenchFromJson(Map<String, dynamic> json) {
	final GetConcertAgentBench getConcertAgentBench = GetConcertAgentBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getConcertAgentBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getConcertAgentBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getConcertAgentBench.format = format;
	}
	return getConcertAgentBench;
}

Map<String, dynamic> $GetConcertAgentBenchToJson(GetConcertAgentBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}