import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/delete_banner_response_entity.dart';

DeleteBannerResponseEntity $DeleteBannerResponseEntityFromJson(
    Map<String, dynamic> json) {
  final DeleteBannerResponseEntity deleteBannerResponseEntity =
      DeleteBannerResponseEntity();
  final DeleteBannerResponseData? data =
      jsonConvert.convert<DeleteBannerResponseData>(json['data']);
  if (data != null) {
    deleteBannerResponseEntity.data = data;
  }
  final DeleteBannerResponseBench? bench =
      jsonConvert.convert<DeleteBannerResponseBench>(json['bench']);
  if (bench != null) {
    deleteBannerResponseEntity.bench = bench;
  }
  return deleteBannerResponseEntity;
}

Map<String, dynamic> $DeleteBannerResponseEntityToJson(
    DeleteBannerResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

DeleteBannerResponseData $DeleteBannerResponseDataFromJson(
    Map<String, dynamic> json) {
  final DeleteBannerResponseData deleteBannerResponseData =
      DeleteBannerResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    deleteBannerResponseData.message = message;
  }
  return deleteBannerResponseData;
}

Map<String, dynamic> $DeleteBannerResponseDataToJson(
    DeleteBannerResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

DeleteBannerResponseBench $DeleteBannerResponseBenchFromJson(
    Map<String, dynamic> json) {
  final DeleteBannerResponseBench deleteBannerResponseBench =
      DeleteBannerResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    deleteBannerResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    deleteBannerResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    deleteBannerResponseBench.format = format;
  }
  return deleteBannerResponseBench;
}

Map<String, dynamic> $DeleteBannerResponseBenchToJson(
    DeleteBannerResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
