import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/passport_data_response_entity.dart';

PassportDataResponseEntity $PassportDataResponseEntityFromJson(
    Map<String, dynamic> json) {
  final PassportDataResponseEntity passportDataResponseEntity =
      PassportDataResponseEntity();
  final PassportDataResponseData? data =
      jsonConvert.convert<PassportDataResponseData>(json['data']);
  if (data != null) {
    passportDataResponseEntity.data = data;
  }
  final PassportDataResponseBench? bench =
      jsonConvert.convert<PassportDataResponseBench>(json['bench']);
  if (bench != null) {
    passportDataResponseEntity.bench = bench;
  }
  return passportDataResponseEntity;
}

Map<String, dynamic> $PassportDataResponseEntityToJson(
    PassportDataResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

PassportDataResponseData $PassportDataResponseDataFromJson(
    Map<String, dynamic> json) {
  final PassportDataResponseData passportDataResponseData =
      PassportDataResponseData();
  final PassportDataResponseDataRecord? record =
      jsonConvert.convert<PassportDataResponseDataRecord>(json['record']);
  if (record != null) {
    passportDataResponseData.record = record;
  }
  return passportDataResponseData;
}

Map<String, dynamic> $PassportDataResponseDataToJson(
    PassportDataResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.toJson();
  return data;
}

PassportDataResponseDataRecord $PassportDataResponseDataRecordFromJson(
    Map<String, dynamic> json) {
  final PassportDataResponseDataRecord passportDataResponseDataRecord =
      PassportDataResponseDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    passportDataResponseDataRecord.id = id;
  }
  final int? productId = jsonConvert.convert<int>(json['product_id']);
  if (productId != null) {
    passportDataResponseDataRecord.productId = productId;
  }
  final int? productVariantId =
      jsonConvert.convert<int>(json['product_variant_id']);
  if (productVariantId != null) {
    passportDataResponseDataRecord.productVariantId = productVariantId;
  }
  final String? ticketNo = jsonConvert.convert<String>(json['ticket_no']);
  if (ticketNo != null) {
    passportDataResponseDataRecord.ticketNo = ticketNo;
  }
  final String? orderNo = jsonConvert.convert<String>(json['order_no']);
  if (orderNo != null) {
    passportDataResponseDataRecord.orderNo = orderNo;
  }
  return passportDataResponseDataRecord;
}

Map<String, dynamic> $PassportDataResponseDataRecordToJson(
    PassportDataResponseDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['product_id'] = entity.productId;
  data['product_variant_id'] = entity.productVariantId;
  data['ticket_no'] = entity.ticketNo;
  data['order_no'] = entity.orderNo;
  return data;
}

PassportDataResponseBench $PassportDataResponseBenchFromJson(
    Map<String, dynamic> json) {
  final PassportDataResponseBench passportDataResponseBench =
      PassportDataResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    passportDataResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    passportDataResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    passportDataResponseBench.format = format;
  }
  return passportDataResponseBench;
}

Map<String, dynamic> $PassportDataResponseBenchToJson(
    PassportDataResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
