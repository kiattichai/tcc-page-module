import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_conditions_entity.dart';

GetConditionsEntity $GetConditionsEntityFromJson(Map<String, dynamic> json) {
  final GetConditionsEntity getConditionsEntity = GetConditionsEntity();
  final GetConditionsData? data =
      jsonConvert.convert<GetConditionsData>(json['data']);
  if (data != null) {
    getConditionsEntity.data = data;
  }
  final GetConditionsBench? bench =
      jsonConvert.convert<GetConditionsBench>(json['bench']);
  if (bench != null) {
    getConditionsEntity.bench = bench;
  }
  return getConditionsEntity;
}

Map<String, dynamic> $GetConditionsEntityToJson(GetConditionsEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetConditionsData $GetConditionsDataFromJson(Map<String, dynamic> json) {
  final GetConditionsData getConditionsData = GetConditionsData();
  final GetConditionsDataPagination? pagination =
      jsonConvert.convert<GetConditionsDataPagination>(json['pagination']);
  if (pagination != null) {
    getConditionsData.pagination = pagination;
  }
  final List<GetConditionsDataRecord>? record =
      jsonConvert.convertListNotNull<GetConditionsDataRecord>(json['record']);
  if (record != null) {
    getConditionsData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getConditionsData.cache = cache;
  }
  return getConditionsData;
}

Map<String, dynamic> $GetConditionsDataToJson(GetConditionsData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetConditionsDataPagination $GetConditionsDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataPagination getConditionsDataPagination =
      GetConditionsDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getConditionsDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getConditionsDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getConditionsDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getConditionsDataPagination.total = total;
  }
  return getConditionsDataPagination;
}

Map<String, dynamic> $GetConditionsDataPaginationToJson(
    GetConditionsDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetConditionsDataRecord $GetConditionsDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataRecord getConditionsDataRecord =
      GetConditionsDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConditionsDataRecord.id = id;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    getConditionsDataRecord.slug = slug;
  }
  final GetConditionsDataRecordGroup? group =
      jsonConvert.convert<GetConditionsDataRecordGroup>(json['group']);
  if (group != null) {
    getConditionsDataRecord.group = group;
  }
  final GetConditionsDataRecordCategory? category =
      jsonConvert.convert<GetConditionsDataRecordCategory>(json['category']);
  if (category != null) {
    getConditionsDataRecord.category = category;
  }
  final List<GetConditionsDataRecordFields>? fields = jsonConvert
      .convertListNotNull<GetConditionsDataRecordFields>(json['fields']);
  if (fields != null) {
    getConditionsDataRecord.fields = fields;
  }
  final List<dynamic>? images =
      jsonConvert.convertListNotNull<dynamic>(json['images']);
  if (images != null) {
    getConditionsDataRecord.images = images;
  }
  final int? viewed = jsonConvert.convert<int>(json['viewed']);
  if (viewed != null) {
    getConditionsDataRecord.viewed = viewed;
  }
  final int? viewedText = jsonConvert.convert<int>(json['viewed_text']);
  if (viewedText != null) {
    getConditionsDataRecord.viewedText = viewedText;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getConditionsDataRecord.status = status;
  }
  final String? shareUrl = jsonConvert.convert<String>(json['share_url']);
  if (shareUrl != null) {
    getConditionsDataRecord.shareUrl = shareUrl;
  }
  final String? webviewUrl = jsonConvert.convert<String>(json['webview_url']);
  if (webviewUrl != null) {
    getConditionsDataRecord.webviewUrl = webviewUrl;
  }
  final int? totalUser = jsonConvert.convert<int>(json['total_user']);
  if (totalUser != null) {
    getConditionsDataRecord.totalUser = totalUser;
  }
  final String? publishedAt = jsonConvert.convert<String>(json['published_at']);
  if (publishedAt != null) {
    getConditionsDataRecord.publishedAt = publishedAt;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getConditionsDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getConditionsDataRecord.updatedAt = updatedAt;
  }
  return getConditionsDataRecord;
}

Map<String, dynamic> $GetConditionsDataRecordToJson(
    GetConditionsDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['slug'] = entity.slug;
  data['group'] = entity.group?.toJson();
  data['category'] = entity.category?.toJson();
  data['fields'] = entity.fields?.map((v) => v.toJson()).toList();
  data['images'] = entity.images;
  data['viewed'] = entity.viewed;
  data['viewed_text'] = entity.viewedText;
  data['status'] = entity.status;
  data['share_url'] = entity.shareUrl;
  data['webview_url'] = entity.webviewUrl;
  data['total_user'] = entity.totalUser;
  data['published_at'] = entity.publishedAt;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetConditionsDataRecordGroup $GetConditionsDataRecordGroupFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataRecordGroup getConditionsDataRecordGroup =
      GetConditionsDataRecordGroup();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConditionsDataRecordGroup.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getConditionsDataRecordGroup.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConditionsDataRecordGroup.name = name;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getConditionsDataRecordGroup.description = description;
  }
  return getConditionsDataRecordGroup;
}

Map<String, dynamic> $GetConditionsDataRecordGroupToJson(
    GetConditionsDataRecordGroup entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  data['description'] = entity.description;
  return data;
}

GetConditionsDataRecordCategory $GetConditionsDataRecordCategoryFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataRecordCategory getConditionsDataRecordCategory =
      GetConditionsDataRecordCategory();
  final GetConditionsDataRecordCategoryCurrent? current = jsonConvert
      .convert<GetConditionsDataRecordCategoryCurrent>(json['current']);
  if (current != null) {
    getConditionsDataRecordCategory.current = current;
  }
  final List<GetConditionsDataRecordCategoryItems>? items = jsonConvert
      .convertListNotNull<GetConditionsDataRecordCategoryItems>(json['items']);
  if (items != null) {
    getConditionsDataRecordCategory.items = items;
  }
  return getConditionsDataRecordCategory;
}

Map<String, dynamic> $GetConditionsDataRecordCategoryToJson(
    GetConditionsDataRecordCategory entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current'] = entity.current?.toJson();
  data['items'] = entity.items?.map((v) => v.toJson()).toList();
  return data;
}

GetConditionsDataRecordCategoryCurrent
    $GetConditionsDataRecordCategoryCurrentFromJson(Map<String, dynamic> json) {
  final GetConditionsDataRecordCategoryCurrent
      getConditionsDataRecordCategoryCurrent =
      GetConditionsDataRecordCategoryCurrent();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConditionsDataRecordCategoryCurrent.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getConditionsDataRecordCategoryCurrent.code = code;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getConditionsDataRecordCategoryCurrent.status = status;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getConditionsDataRecordCategoryCurrent.position = position;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConditionsDataRecordCategoryCurrent.name = name;
  }
  final dynamic? description =
      jsonConvert.convert<dynamic>(json['description']);
  if (description != null) {
    getConditionsDataRecordCategoryCurrent.description = description;
  }
  final dynamic? metaTitle = jsonConvert.convert<dynamic>(json['meta_title']);
  if (metaTitle != null) {
    getConditionsDataRecordCategoryCurrent.metaTitle = metaTitle;
  }
  final dynamic? metaDescription =
      jsonConvert.convert<dynamic>(json['meta_description']);
  if (metaDescription != null) {
    getConditionsDataRecordCategoryCurrent.metaDescription = metaDescription;
  }
  final dynamic? metaKeyword =
      jsonConvert.convert<dynamic>(json['meta_keyword']);
  if (metaKeyword != null) {
    getConditionsDataRecordCategoryCurrent.metaKeyword = metaKeyword;
  }
  return getConditionsDataRecordCategoryCurrent;
}

Map<String, dynamic> $GetConditionsDataRecordCategoryCurrentToJson(
    GetConditionsDataRecordCategoryCurrent entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['position'] = entity.position;
  data['name'] = entity.name;
  data['description'] = entity.description;
  data['meta_title'] = entity.metaTitle;
  data['meta_description'] = entity.metaDescription;
  data['meta_keyword'] = entity.metaKeyword;
  return data;
}

GetConditionsDataRecordCategoryItems
    $GetConditionsDataRecordCategoryItemsFromJson(Map<String, dynamic> json) {
  final GetConditionsDataRecordCategoryItems
      getConditionsDataRecordCategoryItems =
      GetConditionsDataRecordCategoryItems();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConditionsDataRecordCategoryItems.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getConditionsDataRecordCategoryItems.code = code;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getConditionsDataRecordCategoryItems.status = status;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getConditionsDataRecordCategoryItems.position = position;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConditionsDataRecordCategoryItems.name = name;
  }
  return getConditionsDataRecordCategoryItems;
}

Map<String, dynamic> $GetConditionsDataRecordCategoryItemsToJson(
    GetConditionsDataRecordCategoryItems entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['position'] = entity.position;
  data['name'] = entity.name;
  return data;
}

GetConditionsDataRecordFields $GetConditionsDataRecordFieldsFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataRecordFields getConditionsDataRecordFields =
      GetConditionsDataRecordFields();
  final String? label = jsonConvert.convert<String>(json['label']);
  if (label != null) {
    getConditionsDataRecordFields.label = label;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConditionsDataRecordFields.name = name;
  }
  final GetConditionsDataRecordFieldsType? type =
      jsonConvert.convert<GetConditionsDataRecordFieldsType>(json['type']);
  if (type != null) {
    getConditionsDataRecordFields.type = type;
  }
  final bool? hasLang = jsonConvert.convert<bool>(json['has_lang']);
  if (hasLang != null) {
    getConditionsDataRecordFields.hasLang = hasLang;
  }
  final bool? require = jsonConvert.convert<bool>(json['require']);
  if (require != null) {
    getConditionsDataRecordFields.require = require;
  }
  final dynamic? options = jsonConvert.convert<dynamic>(json['options']);
  if (options != null) {
    getConditionsDataRecordFields.options = options;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getConditionsDataRecordFields.lang = lang;
  }
  return getConditionsDataRecordFields;
}

Map<String, dynamic> $GetConditionsDataRecordFieldsToJson(
    GetConditionsDataRecordFields entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['label'] = entity.label;
  data['name'] = entity.name;
  data['type'] = entity.type?.toJson();
  data['has_lang'] = entity.hasLang;
  data['require'] = entity.require;
  data['options'] = entity.options;
  data['lang'] = entity.lang;
  return data;
}

GetConditionsDataRecordFieldsType $GetConditionsDataRecordFieldsTypeFromJson(
    Map<String, dynamic> json) {
  final GetConditionsDataRecordFieldsType getConditionsDataRecordFieldsType =
      GetConditionsDataRecordFieldsType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConditionsDataRecordFieldsType.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConditionsDataRecordFieldsType.name = name;
  }
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getConditionsDataRecordFieldsType.value = value;
  }
  return getConditionsDataRecordFieldsType;
}

Map<String, dynamic> $GetConditionsDataRecordFieldsTypeToJson(
    GetConditionsDataRecordFieldsType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['value'] = entity.value;
  return data;
}

GetConditionsBench $GetConditionsBenchFromJson(Map<String, dynamic> json) {
  final GetConditionsBench getConditionsBench = GetConditionsBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getConditionsBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getConditionsBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getConditionsBench.format = format;
  }
  return getConditionsBench;
}

Map<String, dynamic> $GetConditionsBenchToJson(GetConditionsBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
