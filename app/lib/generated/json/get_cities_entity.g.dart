import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_cities_entity.dart';

GetCitiesEntity $GetCitiesEntityFromJson(Map<String, dynamic> json) {
  final GetCitiesEntity getCitiesEntity = GetCitiesEntity();
  final GetCitiesData? data = jsonConvert.convert<GetCitiesData>(json['data']);
  if (data != null) {
    getCitiesEntity.data = data;
  }
  final GetCitiesBench? bench =
      jsonConvert.convert<GetCitiesBench>(json['bench']);
  if (bench != null) {
    getCitiesEntity.bench = bench;
  }
  return getCitiesEntity;
}

Map<String, dynamic> $GetCitiesEntityToJson(GetCitiesEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetCitiesData $GetCitiesDataFromJson(Map<String, dynamic> json) {
  final GetCitiesData getCitiesData = GetCitiesData();
  final List<GetCitiesDataRecord>? record =
      jsonConvert.convertListNotNull<GetCitiesDataRecord>(json['record']);
  if (record != null) {
    getCitiesData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getCitiesData.cache = cache;
  }
  return getCitiesData;
}

Map<String, dynamic> $GetCitiesDataToJson(GetCitiesData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetCitiesDataRecord $GetCitiesDataRecordFromJson(Map<String, dynamic> json) {
  final GetCitiesDataRecord getCitiesDataRecord = GetCitiesDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCitiesDataRecord.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getCitiesDataRecord.name = name;
  }
  return getCitiesDataRecord;
}

Map<String, dynamic> $GetCitiesDataRecordToJson(GetCitiesDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetCitiesBench $GetCitiesBenchFromJson(Map<String, dynamic> json) {
  final GetCitiesBench getCitiesBench = GetCitiesBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getCitiesBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getCitiesBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getCitiesBench.format = format;
  }
  return getCitiesBench;
}

Map<String, dynamic> $GetCitiesBenchToJson(GetCitiesBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
