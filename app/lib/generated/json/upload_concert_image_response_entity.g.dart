import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_concert_module/model/upload_concert_image_response_entity.dart';

UploadConcertImageResponseEntity $UploadConcertImageResponseEntityFromJson(
    Map<String, dynamic> json) {
  final UploadConcertImageResponseEntity uploadConcertImageResponseEntity =
      UploadConcertImageResponseEntity();
  final UploadConcertImageResponseData? data =
      jsonConvert.convert<UploadConcertImageResponseData>(json['data']);
  if (data != null) {
    uploadConcertImageResponseEntity.data = data;
  }
  final UploadConcertImageResponseBench? bench =
      jsonConvert.convert<UploadConcertImageResponseBench>(json['bench']);
  if (bench != null) {
    uploadConcertImageResponseEntity.bench = bench;
  }
  return uploadConcertImageResponseEntity;
}

Map<String, dynamic> $UploadConcertImageResponseEntityToJson(
    UploadConcertImageResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

UploadConcertImageResponseData $UploadConcertImageResponseDataFromJson(
    Map<String, dynamic> json) {
  final UploadConcertImageResponseData uploadConcertImageResponseData =
      UploadConcertImageResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    uploadConcertImageResponseData.message = message;
  }
  final List<UploadConcertImageResponseDataUploads>? uploads =
      jsonConvert.convertListNotNull<UploadConcertImageResponseDataUploads>(
          json['uploads']);
  if (uploads != null) {
    uploadConcertImageResponseData.uploads = uploads;
  }
  return uploadConcertImageResponseData;
}

Map<String, dynamic> $UploadConcertImageResponseDataToJson(
    UploadConcertImageResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['uploads'] = entity.uploads?.map((v) => v.toJson()).toList();
  return data;
}

UploadConcertImageResponseDataUploads
    $UploadConcertImageResponseDataUploadsFromJson(Map<String, dynamic> json) {
  final UploadConcertImageResponseDataUploads
      uploadConcertImageResponseDataUploads =
      UploadConcertImageResponseDataUploads();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    uploadConcertImageResponseDataUploads.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    uploadConcertImageResponseDataUploads.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    uploadConcertImageResponseDataUploads.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    uploadConcertImageResponseDataUploads.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    uploadConcertImageResponseDataUploads.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    uploadConcertImageResponseDataUploads.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    uploadConcertImageResponseDataUploads.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    uploadConcertImageResponseDataUploads.resizeUrl = resizeUrl;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    uploadConcertImageResponseDataUploads.position = position;
  }
  return uploadConcertImageResponseDataUploads;
}

Map<String, dynamic> $UploadConcertImageResponseDataUploadsToJson(
    UploadConcertImageResponseDataUploads entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  data['position'] = entity.position;
  return data;
}

UploadConcertImageResponseBench $UploadConcertImageResponseBenchFromJson(
    Map<String, dynamic> json) {
  final UploadConcertImageResponseBench uploadConcertImageResponseBench =
      UploadConcertImageResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    uploadConcertImageResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    uploadConcertImageResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    uploadConcertImageResponseBench.format = format;
  }
  return uploadConcertImageResponseBench;
}

Map<String, dynamic> $UploadConcertImageResponseBenchToJson(
    UploadConcertImageResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
