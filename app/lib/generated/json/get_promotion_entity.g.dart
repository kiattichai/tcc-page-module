import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_promotion_entity.dart';

GetPromotionEntity $GetPromotionEntityFromJson(Map<String, dynamic> json) {
  final GetPromotionEntity getPromotionEntity = GetPromotionEntity();
  final GetPromotionData? data =
      jsonConvert.convert<GetPromotionData>(json['data']);
  if (data != null) {
    getPromotionEntity.data = data;
  }
  final GetPromotionBench? bench =
      jsonConvert.convert<GetPromotionBench>(json['bench']);
  if (bench != null) {
    getPromotionEntity.bench = bench;
  }
  return getPromotionEntity;
}

Map<String, dynamic> $GetPromotionEntityToJson(GetPromotionEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetPromotionData $GetPromotionDataFromJson(Map<String, dynamic> json) {
  final GetPromotionData getPromotionData = GetPromotionData();
  final GetPromotionDataPagination? pagination =
      jsonConvert.convert<GetPromotionDataPagination>(json['pagination']);
  if (pagination != null) {
    getPromotionData.pagination = pagination;
  }
  final List<GetPromotionDataRecord>? record =
      jsonConvert.convertListNotNull<GetPromotionDataRecord>(json['record']);
  if (record != null) {
    getPromotionData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getPromotionData.cache = cache;
  }
  return getPromotionData;
}

Map<String, dynamic> $GetPromotionDataToJson(GetPromotionData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetPromotionDataPagination $GetPromotionDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataPagination getPromotionDataPagination =
      GetPromotionDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getPromotionDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getPromotionDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getPromotionDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getPromotionDataPagination.total = total;
  }
  return getPromotionDataPagination;
}

Map<String, dynamic> $GetPromotionDataPaginationToJson(
    GetPromotionDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetPromotionDataRecord $GetPromotionDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecord getPromotionDataRecord =
      GetPromotionDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecord.id = id;
  }
  final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
  if (slug != null) {
    getPromotionDataRecord.slug = slug;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    getPromotionDataRecord.type = type;
  }
  final String? groupType = jsonConvert.convert<String>(json['group_type']);
  if (groupType != null) {
    getPromotionDataRecord.groupType = groupType;
  }
  final int? parentId = jsonConvert.convert<int>(json['parent_id']);
  if (parentId != null) {
    getPromotionDataRecord.parentId = parentId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPromotionDataRecord.name = name;
  }
  final String? descriptionShort =
      jsonConvert.convert<String>(json['description_short']);
  if (descriptionShort != null) {
    getPromotionDataRecord.descriptionShort = descriptionShort;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getPromotionDataRecord.description = description;
  }
  final GetPromotionDataRecordStore? store =
      jsonConvert.convert<GetPromotionDataRecordStore>(json['store']);
  if (store != null) {
    getPromotionDataRecord.store = store;
  }
  final GetPromotionDataRecordVenue? venue =
      jsonConvert.convert<GetPromotionDataRecordVenue>(json['venue']);
  if (venue != null) {
    getPromotionDataRecord.venue = venue;
  }
  final GetPromotionDataRecordShowTime? showTime =
      jsonConvert.convert<GetPromotionDataRecordShowTime>(json['show_time']);
  if (showTime != null) {
    getPromotionDataRecord.showTime = showTime;
  }
  final GetPromotionDataRecordPrice? price =
      jsonConvert.convert<GetPromotionDataRecordPrice>(json['price']);
  if (price != null) {
    getPromotionDataRecord.price = price;
  }
  final List<dynamic>? variants =
      jsonConvert.convertListNotNull<dynamic>(json['variants']);
  if (variants != null) {
    getPromotionDataRecord.variants = variants;
  }
  final List<dynamic>? attributes =
      jsonConvert.convertListNotNull<dynamic>(json['attributes']);
  if (attributes != null) {
    getPromotionDataRecord.attributes = attributes;
  }
  final List<GetPromotionDataRecordImages>? images = jsonConvert
      .convertListNotNull<GetPromotionDataRecordImages>(json['images']);
  if (images != null) {
    getPromotionDataRecord.images = images;
  }
  final int? remain = jsonConvert.convert<int>(json['remain']);
  if (remain != null) {
    getPromotionDataRecord.remain = remain;
  }
  final int? ticketCount = jsonConvert.convert<int>(json['ticket_count']);
  if (ticketCount != null) {
    getPromotionDataRecord.ticketCount = ticketCount;
  }
  final bool? onlineCheckin = jsonConvert.convert<bool>(json['online_checkin']);
  if (onlineCheckin != null) {
    getPromotionDataRecord.onlineCheckin = onlineCheckin;
  }
  final int? viewed = jsonConvert.convert<int>(json['viewed']);
  if (viewed != null) {
    getPromotionDataRecord.viewed = viewed;
  }
  final int? viewedText = jsonConvert.convert<int>(json['viewed_text']);
  if (viewedText != null) {
    getPromotionDataRecord.viewedText = viewedText;
  }
  final dynamic? salesUrl = jsonConvert.convert<dynamic>(json['sales_url']);
  if (salesUrl != null) {
    getPromotionDataRecord.salesUrl = salesUrl;
  }
  final String? shareUrl = jsonConvert.convert<String>(json['share_url']);
  if (shareUrl != null) {
    getPromotionDataRecord.shareUrl = shareUrl;
  }
  final String? webviewUrl = jsonConvert.convert<String>(json['webview_url']);
  if (webviewUrl != null) {
    getPromotionDataRecord.webviewUrl = webviewUrl;
  }
  final bool? includeVat = jsonConvert.convert<bool>(json['include_vat']);
  if (includeVat != null) {
    getPromotionDataRecord.includeVat = includeVat;
  }
  final bool? paymentCharge = jsonConvert.convert<bool>(json['payment_charge']);
  if (paymentCharge != null) {
    getPromotionDataRecord.paymentCharge = paymentCharge;
  }
  final bool? hasVariant = jsonConvert.convert<bool>(json['has_variant']);
  if (hasVariant != null) {
    getPromotionDataRecord.hasVariant = hasVariant;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getPromotionDataRecord.status = status;
  }
  final bool? ticketShipping =
      jsonConvert.convert<bool>(json['ticket_shipping']);
  if (ticketShipping != null) {
    getPromotionDataRecord.ticketShipping = ticketShipping;
  }
  final GetPromotionDataRecordPublishStatus? publishStatus = jsonConvert
      .convert<GetPromotionDataRecordPublishStatus>(json['publish_status']);
  if (publishStatus != null) {
    getPromotionDataRecord.publishStatus = publishStatus;
  }
  final String? publishAt = jsonConvert.convert<String>(json['publish_at']);
  if (publishAt != null) {
    getPromotionDataRecord.publishAt = publishAt;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getPromotionDataRecord.remark = remark;
  }
  final bool? soldoutStatus = jsonConvert.convert<bool>(json['soldout_status']);
  if (soldoutStatus != null) {
    getPromotionDataRecord.soldoutStatus = soldoutStatus;
  }
  final int? soldoutStatusId =
      jsonConvert.convert<int>(json['soldout_status_id']);
  if (soldoutStatusId != null) {
    getPromotionDataRecord.soldoutStatusId = soldoutStatusId;
  }
  final bool? salesStatus = jsonConvert.convert<bool>(json['sales_status']);
  if (salesStatus != null) {
    getPromotionDataRecord.salesStatus = salesStatus;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getPromotionDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getPromotionDataRecord.updatedAt = updatedAt;
  }
  final bool? updated = jsonConvert.convert<bool>(json['updated']);
  if (updated != null) {
    getPromotionDataRecord.updated = updated;
  }
  final bool? verify = jsonConvert.convert<bool>(json['verify']);
  if (verify != null) {
    getPromotionDataRecord.verify = verify;
  }
  return getPromotionDataRecord;
}

Map<String, dynamic> $GetPromotionDataRecordToJson(
    GetPromotionDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['slug'] = entity.slug;
  data['type'] = entity.type;
  data['group_type'] = entity.groupType;
  data['parent_id'] = entity.parentId;
  data['name'] = entity.name;
  data['description_short'] = entity.descriptionShort;
  data['description'] = entity.description;
  data['store'] = entity.store?.toJson();
  data['venue'] = entity.venue?.toJson();
  data['show_time'] = entity.showTime?.toJson();
  data['price'] = entity.price?.toJson();
  data['variants'] = entity.variants;
  data['attributes'] = entity.attributes;
  data['images'] = entity.images?.map((v) => v.toJson()).toList();
  data['remain'] = entity.remain;
  data['ticket_count'] = entity.ticketCount;
  data['online_checkin'] = entity.onlineCheckin;
  data['viewed'] = entity.viewed;
  data['viewed_text'] = entity.viewedText;
  data['sales_url'] = entity.salesUrl;
  data['share_url'] = entity.shareUrl;
  data['webview_url'] = entity.webviewUrl;
  data['include_vat'] = entity.includeVat;
  data['payment_charge'] = entity.paymentCharge;
  data['has_variant'] = entity.hasVariant;
  data['status'] = entity.status;
  data['ticket_shipping'] = entity.ticketShipping;
  data['publish_status'] = entity.publishStatus?.toJson();
  data['publish_at'] = entity.publishAt;
  data['remark'] = entity.remark;
  data['soldout_status'] = entity.soldoutStatus;
  data['soldout_status_id'] = entity.soldoutStatusId;
  data['sales_status'] = entity.salesStatus;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['updated'] = entity.updated;
  data['verify'] = entity.verify;
  return data;
}

GetPromotionDataRecordStore $GetPromotionDataRecordStoreFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordStore getPromotionDataRecordStore =
      GetPromotionDataRecordStore();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordStore.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPromotionDataRecordStore.name = name;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    getPromotionDataRecordStore.slug = slug;
  }
  final GetPromotionDataRecordStoreType? type =
      jsonConvert.convert<GetPromotionDataRecordStoreType>(json['type']);
  if (type != null) {
    getPromotionDataRecordStore.type = type;
  }
  final GetPromotionDataRecordStoreSection? section =
      jsonConvert.convert<GetPromotionDataRecordStoreSection>(json['section']);
  if (section != null) {
    getPromotionDataRecordStore.section = section;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getPromotionDataRecordStore.status = status;
  }
  final GetPromotionDataRecordStoreImage? image =
      jsonConvert.convert<GetPromotionDataRecordStoreImage>(json['image']);
  if (image != null) {
    getPromotionDataRecordStore.image = image;
  }
  final GetPromotionDataRecordStoreVenue? venue =
      jsonConvert.convert<GetPromotionDataRecordStoreVenue>(json['venue']);
  if (venue != null) {
    getPromotionDataRecordStore.venue = venue;
  }
  return getPromotionDataRecordStore;
}

Map<String, dynamic> $GetPromotionDataRecordStoreToJson(
    GetPromotionDataRecordStore entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['slug'] = entity.slug;
  data['type'] = entity.type?.toJson();
  data['section'] = entity.section?.toJson();
  data['status'] = entity.status;
  data['image'] = entity.image?.toJson();
  data['venue'] = entity.venue?.toJson();
  return data;
}

GetPromotionDataRecordStoreType $GetPromotionDataRecordStoreTypeFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordStoreType getPromotionDataRecordStoreType =
      GetPromotionDataRecordStoreType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordStoreType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getPromotionDataRecordStoreType.text = text;
  }
  return getPromotionDataRecordStoreType;
}

Map<String, dynamic> $GetPromotionDataRecordStoreTypeToJson(
    GetPromotionDataRecordStoreType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetPromotionDataRecordStoreSection $GetPromotionDataRecordStoreSectionFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordStoreSection getPromotionDataRecordStoreSection =
      GetPromotionDataRecordStoreSection();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordStoreSection.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getPromotionDataRecordStoreSection.text = text;
  }
  return getPromotionDataRecordStoreSection;
}

Map<String, dynamic> $GetPromotionDataRecordStoreSectionToJson(
    GetPromotionDataRecordStoreSection entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetPromotionDataRecordStoreImage $GetPromotionDataRecordStoreImageFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordStoreImage getPromotionDataRecordStoreImage =
      GetPromotionDataRecordStoreImage();
  return getPromotionDataRecordStoreImage;
}

Map<String, dynamic> $GetPromotionDataRecordStoreImageToJson(
    GetPromotionDataRecordStoreImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetPromotionDataRecordStoreVenue $GetPromotionDataRecordStoreVenueFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordStoreVenue getPromotionDataRecordStoreVenue =
      GetPromotionDataRecordStoreVenue();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordStoreVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getPromotionDataRecordStoreVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    getPromotionDataRecordStoreVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPromotionDataRecordStoreVenue.name = name;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getPromotionDataRecordStoreVenue.address = address;
  }
  return getPromotionDataRecordStoreVenue;
}

Map<String, dynamic> $GetPromotionDataRecordStoreVenueToJson(
    GetPromotionDataRecordStoreVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  data['address'] = entity.address;
  return data;
}

GetPromotionDataRecordVenue $GetPromotionDataRecordVenueFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordVenue getPromotionDataRecordVenue =
      GetPromotionDataRecordVenue();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getPromotionDataRecordVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    getPromotionDataRecordVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPromotionDataRecordVenue.name = name;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getPromotionDataRecordVenue.address = address;
  }
  return getPromotionDataRecordVenue;
}

Map<String, dynamic> $GetPromotionDataRecordVenueToJson(
    GetPromotionDataRecordVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  data['address'] = entity.address;
  return data;
}

GetPromotionDataRecordShowTime $GetPromotionDataRecordShowTimeFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordShowTime getPromotionDataRecordShowTime =
      GetPromotionDataRecordShowTime();
  final String? start = jsonConvert.convert<String>(json['start']);
  if (start != null) {
    getPromotionDataRecordShowTime.start = start;
  }
  final String? end = jsonConvert.convert<String>(json['end']);
  if (end != null) {
    getPromotionDataRecordShowTime.end = end;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getPromotionDataRecordShowTime.textFull = textFull;
  }
  final String? textShort = jsonConvert.convert<String>(json['text_short']);
  if (textShort != null) {
    getPromotionDataRecordShowTime.textShort = textShort;
  }
  final String? textShortDate =
      jsonConvert.convert<String>(json['text_short_date']);
  if (textShortDate != null) {
    getPromotionDataRecordShowTime.textShortDate = textShortDate;
  }
  final int? status = jsonConvert.convert<int>(json['status']);
  if (status != null) {
    getPromotionDataRecordShowTime.status = status;
  }
  final String? statusText = jsonConvert.convert<String>(json['status_text']);
  if (statusText != null) {
    getPromotionDataRecordShowTime.statusText = statusText;
  }
  return getPromotionDataRecordShowTime;
}

Map<String, dynamic> $GetPromotionDataRecordShowTimeToJson(
    GetPromotionDataRecordShowTime entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['start'] = entity.start;
  data['end'] = entity.end;
  data['text_full'] = entity.textFull;
  data['text_short'] = entity.textShort;
  data['text_short_date'] = entity.textShortDate;
  data['status'] = entity.status;
  data['status_text'] = entity.statusText;
  return data;
}

GetPromotionDataRecordPrice $GetPromotionDataRecordPriceFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordPrice getPromotionDataRecordPrice =
      GetPromotionDataRecordPrice();
  final String? currencyCode =
      jsonConvert.convert<String>(json['currency_code']);
  if (currencyCode != null) {
    getPromotionDataRecordPrice.currencyCode = currencyCode;
  }
  final String? currencySymbol =
      jsonConvert.convert<String>(json['currency_symbol']);
  if (currencySymbol != null) {
    getPromotionDataRecordPrice.currencySymbol = currencySymbol;
  }
  final int? min = jsonConvert.convert<int>(json['min']);
  if (min != null) {
    getPromotionDataRecordPrice.min = min;
  }
  final int? max = jsonConvert.convert<int>(json['max']);
  if (max != null) {
    getPromotionDataRecordPrice.max = max;
  }
  final String? minText = jsonConvert.convert<String>(json['min_text']);
  if (minText != null) {
    getPromotionDataRecordPrice.minText = minText;
  }
  final String? maxText = jsonConvert.convert<String>(json['max_text']);
  if (maxText != null) {
    getPromotionDataRecordPrice.maxText = maxText;
  }
  final int? compareMin = jsonConvert.convert<int>(json['compare_min']);
  if (compareMin != null) {
    getPromotionDataRecordPrice.compareMin = compareMin;
  }
  final int? compareMax = jsonConvert.convert<int>(json['compare_max']);
  if (compareMax != null) {
    getPromotionDataRecordPrice.compareMax = compareMax;
  }
  final String? compareMinText =
      jsonConvert.convert<String>(json['compare_min_text']);
  if (compareMinText != null) {
    getPromotionDataRecordPrice.compareMinText = compareMinText;
  }
  final String? compareMaxText =
      jsonConvert.convert<String>(json['compare_max_text']);
  if (compareMaxText != null) {
    getPromotionDataRecordPrice.compareMaxText = compareMaxText;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getPromotionDataRecordPrice.status = status;
  }
  return getPromotionDataRecordPrice;
}

Map<String, dynamic> $GetPromotionDataRecordPriceToJson(
    GetPromotionDataRecordPrice entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['currency_code'] = entity.currencyCode;
  data['currency_symbol'] = entity.currencySymbol;
  data['min'] = entity.min;
  data['max'] = entity.max;
  data['min_text'] = entity.minText;
  data['max_text'] = entity.maxText;
  data['compare_min'] = entity.compareMin;
  data['compare_max'] = entity.compareMax;
  data['compare_min_text'] = entity.compareMinText;
  data['compare_max_text'] = entity.compareMaxText;
  data['status'] = entity.status;
  return data;
}

GetPromotionDataRecordImages $GetPromotionDataRecordImagesFromJson(
    Map<String, dynamic> json) {
  final GetPromotionDataRecordImages getPromotionDataRecordImages =
      GetPromotionDataRecordImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getPromotionDataRecordImages.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getPromotionDataRecordImages.storeId = storeId;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getPromotionDataRecordImages.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getPromotionDataRecordImages.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getPromotionDataRecordImages.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getPromotionDataRecordImages.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getPromotionDataRecordImages.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getPromotionDataRecordImages.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getPromotionDataRecordImages.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getPromotionDataRecordImages.url = url;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getPromotionDataRecordImages.position = position;
  }
  return getPromotionDataRecordImages;
}

Map<String, dynamic> $GetPromotionDataRecordImagesToJson(
    GetPromotionDataRecordImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['position'] = entity.position;
  return data;
}

GetPromotionDataRecordPublishStatus
    $GetPromotionDataRecordPublishStatusFromJson(Map<String, dynamic> json) {
  final GetPromotionDataRecordPublishStatus
      getPromotionDataRecordPublishStatus =
      GetPromotionDataRecordPublishStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getPromotionDataRecordPublishStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getPromotionDataRecordPublishStatus.text = text;
  }
  return getPromotionDataRecordPublishStatus;
}

Map<String, dynamic> $GetPromotionDataRecordPublishStatusToJson(
    GetPromotionDataRecordPublishStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetPromotionBench $GetPromotionBenchFromJson(Map<String, dynamic> json) {
  final GetPromotionBench getPromotionBench = GetPromotionBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getPromotionBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getPromotionBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getPromotionBench.format = format;
  }
  return getPromotionBench;
}

Map<String, dynamic> $GetPromotionBenchToJson(GetPromotionBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
