import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_concert_ticket_entity.dart';

SaveConcertTicketEntity $SaveConcertTicketEntityFromJson(Map<String, dynamic> json) {
	final SaveConcertTicketEntity saveConcertTicketEntity = SaveConcertTicketEntity();
	final String? storeId = jsonConvert.convert<String>(json['store_id']);
	if (storeId != null) {
		saveConcertTicketEntity.storeId = storeId;
	}
	final String? productId = jsonConvert.convert<String>(json['product_id']);
	if (productId != null) {
		saveConcertTicketEntity.productId = productId;
	}
	final String? sku = jsonConvert.convert<String>(json['sku']);
	if (sku != null) {
		saveConcertTicketEntity.sku = sku;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		saveConcertTicketEntity.name = name;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		saveConcertTicketEntity.description = description;
	}
	final String? price = jsonConvert.convert<String>(json['price']);
	if (price != null) {
		saveConcertTicketEntity.price = price;
	}
	final String? compareAtPrice = jsonConvert.convert<String>(json['compare_at_price']);
	if (compareAtPrice != null) {
		saveConcertTicketEntity.compareAtPrice = compareAtPrice;
	}
	final String? quantity = jsonConvert.convert<String>(json['quantity']);
	if (quantity != null) {
		saveConcertTicketEntity.quantity = quantity;
	}
	final String? hold = jsonConvert.convert<String>(json['hold']);
	if (hold != null) {
		saveConcertTicketEntity.hold = hold;
	}
	final String? publishedStart = jsonConvert.convert<String>(json['published_start']);
	if (publishedStart != null) {
		saveConcertTicketEntity.publishedStart = publishedStart;
	}
	final String? publishedEnd = jsonConvert.convert<String>(json['published_end']);
	if (publishedEnd != null) {
		saveConcertTicketEntity.publishedEnd = publishedEnd;
	}
	final String? gateOpen = jsonConvert.convert<String>(json['gate_open']);
	if (gateOpen != null) {
		saveConcertTicketEntity.gateOpen = gateOpen;
	}
	final String? gateClose = jsonConvert.convert<String>(json['gate_close']);
	if (gateClose != null) {
		saveConcertTicketEntity.gateClose = gateClose;
	}
	final String? serviceFee = jsonConvert.convert<String>(json['service_fee']);
	if (serviceFee != null) {
		saveConcertTicketEntity.serviceFee = serviceFee;
	}
	final bool? serviceCharge = jsonConvert.convert<bool>(json['service_charge']);
	if (serviceCharge != null) {
		saveConcertTicketEntity.serviceCharge = serviceCharge;
	}
	final String? allowOrderMin = jsonConvert.convert<String>(json['allow_order_min']);
	if (allowOrderMin != null) {
		saveConcertTicketEntity.allowOrderMin = allowOrderMin;
	}
	final String? allowOrderMax = jsonConvert.convert<String>(json['allow_order_max']);
	if (allowOrderMax != null) {
		saveConcertTicketEntity.allowOrderMax = allowOrderMax;
	}
	final bool? package = jsonConvert.convert<bool>(json['package']);
	if (package != null) {
		saveConcertTicketEntity.package = package;
	}
	final int? perPackage = jsonConvert.convert<int>(json['per_package']);
	if (perPackage != null) {
		saveConcertTicketEntity.perPackage = perPackage;
	}
	final bool? hasTicket = jsonConvert.convert<bool>(json['has_ticket']);
	if (hasTicket != null) {
		saveConcertTicketEntity.hasTicket = hasTicket;
	}
	final String? updatedBy = jsonConvert.convert<String>(json['updated_by']);
	if (updatedBy != null) {
		saveConcertTicketEntity.updatedBy = updatedBy;
	}
	final int? createdBy = jsonConvert.convert<int>(json['created_by']);
	if (createdBy != null) {
		saveConcertTicketEntity.createdBy = createdBy;
	}
	final String? stock = jsonConvert.convert<String>(json['stock']);
	if (stock != null) {
		saveConcertTicketEntity.stock = stock;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		saveConcertTicketEntity.status = status;
	}
	return saveConcertTicketEntity;
}

Map<String, dynamic> $SaveConcertTicketEntityToJson(SaveConcertTicketEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['store_id'] = entity.storeId;
	data['product_id'] = entity.productId;
	data['sku'] = entity.sku;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['price'] = entity.price;
	data['compare_at_price'] = entity.compareAtPrice;
	data['quantity'] = entity.quantity;
	data['hold'] = entity.hold;
	data['published_start'] = entity.publishedStart;
	data['published_end'] = entity.publishedEnd;
	data['gate_open'] = entity.gateOpen;
	data['gate_close'] = entity.gateClose;
	data['service_fee'] = entity.serviceFee;
	data['service_charge'] = entity.serviceCharge;
	data['allow_order_min'] = entity.allowOrderMin;
	data['allow_order_max'] = entity.allowOrderMax;
	data['package'] = entity.package;
	data['per_package'] = entity.perPackage;
	data['has_ticket'] = entity.hasTicket;
	data['updated_by'] = entity.updatedBy;
	data['created_by'] = entity.createdBy;
	data['stock'] = entity.stock;
	data['status'] = entity.status;
	return data;
}