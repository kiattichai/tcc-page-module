import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_organizer_store_banner_entity.dart';

GetOrganizerStoreBannerEntity $GetOrganizerStoreBannerEntityFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreBannerEntity getOrganizerStoreBannerEntity =
      GetOrganizerStoreBannerEntity();
  final GetOrganizerStoreBannerData? data =
      jsonConvert.convert<GetOrganizerStoreBannerData>(json['data']);
  if (data != null) {
    getOrganizerStoreBannerEntity.data = data;
  }
  final GetOrganizerStoreBannerBench? bench =
      jsonConvert.convert<GetOrganizerStoreBannerBench>(json['bench']);
  if (bench != null) {
    getOrganizerStoreBannerEntity.bench = bench;
  }
  return getOrganizerStoreBannerEntity;
}

Map<String, dynamic> $GetOrganizerStoreBannerEntityToJson(
    GetOrganizerStoreBannerEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetOrganizerStoreBannerData $GetOrganizerStoreBannerDataFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreBannerData getOrganizerStoreBannerData =
      GetOrganizerStoreBannerData();
  final GetOrganizerStoreBannerDataImages? images =
      jsonConvert.convert<GetOrganizerStoreBannerDataImages>(json['images']);
  if (images != null) {
    getOrganizerStoreBannerData.images = images;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getOrganizerStoreBannerData.cache = cache;
  }
  return getOrganizerStoreBannerData;
}

Map<String, dynamic> $GetOrganizerStoreBannerDataToJson(
    GetOrganizerStoreBannerData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['images'] = entity.images?.toJson();
  data['cache'] = entity.cache;
  return data;
}

GetOrganizerStoreBannerDataImages $GetOrganizerStoreBannerDataImagesFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreBannerDataImages getOrganizerStoreBannerDataImages =
      GetOrganizerStoreBannerDataImages();
  final List<GetOrganizerStoreBannerDataImagesBanner>? banner =
      jsonConvert.convertListNotNull<GetOrganizerStoreBannerDataImagesBanner>(
          json['banner']);
  if (banner != null) {
    getOrganizerStoreBannerDataImages.banner = banner;
  }
  return getOrganizerStoreBannerDataImages;
}

Map<String, dynamic> $GetOrganizerStoreBannerDataImagesToJson(
    GetOrganizerStoreBannerDataImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['banner'] = entity.banner?.map((v) => v.toJson()).toList();
  return data;
}

GetOrganizerStoreBannerDataImagesBanner
    $GetOrganizerStoreBannerDataImagesBannerFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreBannerDataImagesBanner
      getOrganizerStoreBannerDataImagesBanner =
      GetOrganizerStoreBannerDataImagesBanner();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getOrganizerStoreBannerDataImagesBanner.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getOrganizerStoreBannerDataImagesBanner.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getOrganizerStoreBannerDataImagesBanner.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getOrganizerStoreBannerDataImagesBanner.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreBannerDataImagesBanner.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getOrganizerStoreBannerDataImagesBanner.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getOrganizerStoreBannerDataImagesBanner.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getOrganizerStoreBannerDataImagesBanner.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getOrganizerStoreBannerDataImagesBanner.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getOrganizerStoreBannerDataImagesBanner.url = url;
  }
  return getOrganizerStoreBannerDataImagesBanner;
}

Map<String, dynamic> $GetOrganizerStoreBannerDataImagesBannerToJson(
    GetOrganizerStoreBannerDataImagesBanner entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetOrganizerStoreBannerBench $GetOrganizerStoreBannerBenchFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreBannerBench getOrganizerStoreBannerBench =
      GetOrganizerStoreBannerBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getOrganizerStoreBannerBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getOrganizerStoreBannerBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getOrganizerStoreBannerBench.format = format;
  }
  return getOrganizerStoreBannerBench;
}

Map<String, dynamic> $GetOrganizerStoreBannerBenchToJson(
    GetOrganizerStoreBannerBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
