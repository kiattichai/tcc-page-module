import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/delete_image_review_response_entity.dart';

DeleteImageReviewResponseEntity $DeleteImageReviewResponseEntityFromJson(
    Map<String, dynamic> json) {
  final DeleteImageReviewResponseEntity deleteImageReviewResponseEntity =
      DeleteImageReviewResponseEntity();
  final DeleteImageReviewResponseData? data =
      jsonConvert.convert<DeleteImageReviewResponseData>(json['data']);
  if (data != null) {
    deleteImageReviewResponseEntity.data = data;
  }
  final DeleteImageReviewResponseBench? bench =
      jsonConvert.convert<DeleteImageReviewResponseBench>(json['bench']);
  if (bench != null) {
    deleteImageReviewResponseEntity.bench = bench;
  }
  return deleteImageReviewResponseEntity;
}

Map<String, dynamic> $DeleteImageReviewResponseEntityToJson(
    DeleteImageReviewResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

DeleteImageReviewResponseData $DeleteImageReviewResponseDataFromJson(
    Map<String, dynamic> json) {
  final DeleteImageReviewResponseData deleteImageReviewResponseData =
      DeleteImageReviewResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    deleteImageReviewResponseData.message = message;
  }
  return deleteImageReviewResponseData;
}

Map<String, dynamic> $DeleteImageReviewResponseDataToJson(
    DeleteImageReviewResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

DeleteImageReviewResponseBench $DeleteImageReviewResponseBenchFromJson(
    Map<String, dynamic> json) {
  final DeleteImageReviewResponseBench deleteImageReviewResponseBench =
      DeleteImageReviewResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    deleteImageReviewResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    deleteImageReviewResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    deleteImageReviewResponseBench.format = format;
  }
  return deleteImageReviewResponseBench;
}

Map<String, dynamic> $DeleteImageReviewResponseBenchToJson(
    DeleteImageReviewResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
