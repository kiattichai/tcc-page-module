import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/response_register_entity.dart';

ResponseRegisterEntity $ResponseRegisterEntityFromJson(Map<String, dynamic> json) {
	final ResponseRegisterEntity responseRegisterEntity = ResponseRegisterEntity();
	final ResponseRegisterData? data = jsonConvert.convert<ResponseRegisterData>(json['data']);
	if (data != null) {
		responseRegisterEntity.data = data;
	}
	final ResponseRegisterBench? bench = jsonConvert.convert<ResponseRegisterBench>(json['bench']);
	if (bench != null) {
		responseRegisterEntity.bench = bench;
	}
	return responseRegisterEntity;
}

Map<String, dynamic> $ResponseRegisterEntityToJson(ResponseRegisterEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

ResponseRegisterData $ResponseRegisterDataFromJson(Map<String, dynamic> json) {
	final ResponseRegisterData responseRegisterData = ResponseRegisterData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		responseRegisterData.message = message;
	}
	final int? agentId = jsonConvert.convert<int>(json['agent_id']);
	if (agentId != null) {
		responseRegisterData.agentId = agentId;
	}
	return responseRegisterData;
}

Map<String, dynamic> $ResponseRegisterDataToJson(ResponseRegisterData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	data['agent_id'] = entity.agentId;
	return data;
}

ResponseRegisterBench $ResponseRegisterBenchFromJson(Map<String, dynamic> json) {
	final ResponseRegisterBench responseRegisterBench = ResponseRegisterBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		responseRegisterBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		responseRegisterBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		responseRegisterBench.format = format;
	}
	return responseRegisterBench;
}

Map<String, dynamic> $ResponseRegisterBenchToJson(ResponseRegisterBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}