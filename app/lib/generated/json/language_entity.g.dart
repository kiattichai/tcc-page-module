import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/language_entity.dart';

LanguageEntity $LanguageEntityFromJson(Map<String, dynamic> json) {
  final LanguageEntity languageEntity = LanguageEntity();
  final String? mainBottomMenuHome =
      jsonConvert.convert<String>(json['main_bottom_menu_home']);
  if (mainBottomMenuHome != null) {
    languageEntity.mainBottomMenuHome = mainBottomMenuHome;
  }
  final String? mainBottomMenuLotto =
      jsonConvert.convert<String>(json['main_bottom_menu_lotto']);
  if (mainBottomMenuLotto != null) {
    languageEntity.mainBottomMenuLotto = mainBottomMenuLotto;
  }
  final String? mainBottomMenuNotifications =
      jsonConvert.convert<String>(json['main_bottom_menu_notifications']);
  if (mainBottomMenuNotifications != null) {
    languageEntity.mainBottomMenuNotifications = mainBottomMenuNotifications;
  }
  final String? mainBottomMenuProfile =
      jsonConvert.convert<String>(json['main_bottom_menu_profile']);
  if (mainBottomMenuProfile != null) {
    languageEntity.mainBottomMenuProfile = mainBottomMenuProfile;
  }
  final String? commonConfirm =
      jsonConvert.convert<String>(json['common_confirm']);
  if (commonConfirm != null) {
    languageEntity.commonConfirm = commonConfirm;
  }
  final String? commonReject =
      jsonConvert.convert<String>(json['common_reject']);
  if (commonReject != null) {
    languageEntity.commonReject = commonReject;
  }
  final String? commonNext = jsonConvert.convert<String>(json['common_next']);
  if (commonNext != null) {
    languageEntity.commonNext = commonNext;
  }
  final String? commonCancel =
      jsonConvert.convert<String>(json['common_cancel']);
  if (commonCancel != null) {
    languageEntity.commonCancel = commonCancel;
  }
  final String? commonSearch =
      jsonConvert.convert<String>(json['common_search']);
  if (commonSearch != null) {
    languageEntity.commonSearch = commonSearch;
  }
  final String? commonEmpty = jsonConvert.convert<String>(json['common_empty']);
  if (commonEmpty != null) {
    languageEntity.commonEmpty = commonEmpty;
  }
  final String? commonYes = jsonConvert.convert<String>(json['common_yes']);
  if (commonYes != null) {
    languageEntity.commonYes = commonYes;
  }
  final String? commonNo = jsonConvert.convert<String>(json['common_no']);
  if (commonNo != null) {
    languageEntity.commonNo = commonNo;
  }
  final String? commonAlbum = jsonConvert.convert<String>(json['common_album']);
  if (commonAlbum != null) {
    languageEntity.commonAlbum = commonAlbum;
  }
  final String? commonRemark =
      jsonConvert.convert<String>(json['common_remark']);
  if (commonRemark != null) {
    languageEntity.commonRemark = commonRemark;
  }
  final String? commonChoose =
      jsonConvert.convert<String>(json['common_choose']);
  if (commonChoose != null) {
    languageEntity.commonChoose = commonChoose;
  }
  final String? commonClose = jsonConvert.convert<String>(json['common_close']);
  if (commonClose != null) {
    languageEntity.commonClose = commonClose;
  }
  final String? commonChange =
      jsonConvert.convert<String>(json['common_change']);
  if (commonChange != null) {
    languageEntity.commonChange = commonChange;
  }
  final String? commonEdit = jsonConvert.convert<String>(json['common_edit']);
  if (commonEdit != null) {
    languageEntity.commonEdit = commonEdit;
  }
  final String? unitMinute = jsonConvert.convert<String>(json['unit_minute']);
  if (unitMinute != null) {
    languageEntity.unitMinute = unitMinute;
  }
  final String? unitDistanceKm =
      jsonConvert.convert<String>(json['unit_distance_km']);
  if (unitDistanceKm != null) {
    languageEntity.unitDistanceKm = unitDistanceKm;
  }
  final String? commonDeliverTo =
      jsonConvert.convert<String>(json['common_deliver_to']);
  if (commonDeliverTo != null) {
    languageEntity.commonDeliverTo = commonDeliverTo;
  }
  final String? cameraTitle = jsonConvert.convert<String>(json['camera_title']);
  if (cameraTitle != null) {
    languageEntity.cameraTitle = cameraTitle;
  }
  final String? photoLibraryTitle =
      jsonConvert.convert<String>(json['photo_library_title']);
  if (photoLibraryTitle != null) {
    languageEntity.photoLibraryTitle = photoLibraryTitle;
  }
  final String? loginPhoneNoHint =
      jsonConvert.convert<String>(json['login_phone_no_hint']);
  if (loginPhoneNoHint != null) {
    languageEntity.loginPhoneNoHint = loginPhoneNoHint;
  }
  final String? loginPasswordHint =
      jsonConvert.convert<String>(json['login_password_hint']);
  if (loginPasswordHint != null) {
    languageEntity.loginPasswordHint = loginPasswordHint;
  }
  final String? loginBtnSubmit =
      jsonConvert.convert<String>(json['login_btn_submit']);
  if (loginBtnSubmit != null) {
    languageEntity.loginBtnSubmit = loginBtnSubmit;
  }
  final String? loginBtnRegister =
      jsonConvert.convert<String>(json['login_btn_register']);
  if (loginBtnRegister != null) {
    languageEntity.loginBtnRegister = loginBtnRegister;
  }
  final String? loginBtnForgotPassword =
      jsonConvert.convert<String>(json['login_btn_forgot_password']);
  if (loginBtnForgotPassword != null) {
    languageEntity.loginBtnForgotPassword = loginBtnForgotPassword;
  }
  final String? loginBtnFbTitle =
      jsonConvert.convert<String>(json['login_btn_fb_title']);
  if (loginBtnFbTitle != null) {
    languageEntity.loginBtnFbTitle = loginBtnFbTitle;
  }
  final String? loginTitleOr =
      jsonConvert.convert<String>(json['login_title_or']);
  if (loginTitleOr != null) {
    languageEntity.loginTitleOr = loginTitleOr;
  }
  final String? loginNotMemberTitle =
      jsonConvert.convert<String>(json['login_not_member_title']);
  if (loginNotMemberTitle != null) {
    languageEntity.loginNotMemberTitle = loginNotMemberTitle;
  }
  final String? loginNotMemberSignupTitle =
      jsonConvert.convert<String>(json['login_not_member_signup_title']);
  if (loginNotMemberSignupTitle != null) {
    languageEntity.loginNotMemberSignupTitle = loginNotMemberSignupTitle;
  }
  final String? loginBeforeMobileRegisterTitle =
      jsonConvert.convert<String>(json['login_before_mobile_register_title']);
  if (loginBeforeMobileRegisterTitle != null) {
    languageEntity.loginBeforeMobileRegisterTitle =
        loginBeforeMobileRegisterTitle;
  }
  final String? registerHeader =
      jsonConvert.convert<String>(json['register_header']);
  if (registerHeader != null) {
    languageEntity.registerHeader = registerHeader;
  }
  final String? registerInputPhoneTitle =
      jsonConvert.convert<String>(json['register_input_phone_title']);
  if (registerInputPhoneTitle != null) {
    languageEntity.registerInputPhoneTitle = registerInputPhoneTitle;
  }
  final String? registerInputPhoneHint =
      jsonConvert.convert<String>(json['register_input_phone_hint']);
  if (registerInputPhoneHint != null) {
    languageEntity.registerInputPhoneHint = registerInputPhoneHint;
  }
  final String? registerOtpMessage =
      jsonConvert.convert<String>(json['register_otp_message']);
  if (registerOtpMessage != null) {
    languageEntity.registerOtpMessage = registerOtpMessage;
  }
  final String? registerAccept =
      jsonConvert.convert<String>(json['register_accept']);
  if (registerAccept != null) {
    languageEntity.registerAccept = registerAccept;
  }
  final String? registerTermsAndConditions =
      jsonConvert.convert<String>(json['register_terms_and_conditions']);
  if (registerTermsAndConditions != null) {
    languageEntity.registerTermsAndConditions = registerTermsAndConditions;
  }
  final String? registerBtnConfirm =
      jsonConvert.convert<String>(json['register_btn_confirm']);
  if (registerBtnConfirm != null) {
    languageEntity.registerBtnConfirm = registerBtnConfirm;
  }
  final String? registerBtnFbTitle =
      jsonConvert.convert<String>(json['register_btn_fb_title']);
  if (registerBtnFbTitle != null) {
    languageEntity.registerBtnFbTitle = registerBtnFbTitle;
  }
  final String? registerBtnAppleTitle =
      jsonConvert.convert<String>(json['register_btn_apple_title']);
  if (registerBtnAppleTitle != null) {
    languageEntity.registerBtnAppleTitle = registerBtnAppleTitle;
  }
  final String? registerBtnMobilePhoneTitle =
      jsonConvert.convert<String>(json['register_btn_mobile_phone_title']);
  if (registerBtnMobilePhoneTitle != null) {
    languageEntity.registerBtnMobilePhoneTitle = registerBtnMobilePhoneTitle;
  }
  final String? channelVerificationTitle =
      jsonConvert.convert<String>(json['channel_verification_title']);
  if (channelVerificationTitle != null) {
    languageEntity.channelVerificationTitle = channelVerificationTitle;
  }
  final String? channelVerificationSms =
      jsonConvert.convert<String>(json['channel_verification_sms']);
  if (channelVerificationSms != null) {
    languageEntity.channelVerificationSms = channelVerificationSms;
  }
  final String? channelVerificationVoiceCall =
      jsonConvert.convert<String>(json['channel_verification_voice_call']);
  if (channelVerificationVoiceCall != null) {
    languageEntity.channelVerificationVoiceCall = channelVerificationVoiceCall;
  }
  final String? confirmOtpTitle =
      jsonConvert.convert<String>(json['confirm_otp_title']);
  if (confirmOtpTitle != null) {
    languageEntity.confirmOtpTitle = confirmOtpTitle;
  }
  final String? confirmOtpMessage =
      jsonConvert.convert<String>(json['confirm_otp_message']);
  if (confirmOtpMessage != null) {
    languageEntity.confirmOtpMessage = confirmOtpMessage;
  }
  final String? confirmOtpSendAgain =
      jsonConvert.convert<String>(json['confirm_otp_send_again']);
  if (confirmOtpSendAgain != null) {
    languageEntity.confirmOtpSendAgain = confirmOtpSendAgain;
  }
  final String? confirmOtpWaitingSendAgain =
      jsonConvert.convert<String>(json['confirm_otp_waiting_send_again']);
  if (confirmOtpWaitingSendAgain != null) {
    languageEntity.confirmOtpWaitingSendAgain = confirmOtpWaitingSendAgain;
  }
  final String? second = jsonConvert.convert<String>(json['second']);
  if (second != null) {
    languageEntity.second = second;
  }
  final String? confirmOtpReference =
      jsonConvert.convert<String>(json['confirm_otp_reference']);
  if (confirmOtpReference != null) {
    languageEntity.confirmOtpReference = confirmOtpReference;
  }
  final String? userInfoHeader =
      jsonConvert.convert<String>(json['user_info_header']);
  if (userInfoHeader != null) {
    languageEntity.userInfoHeader = userInfoHeader;
  }
  final String? userInfoFirstname =
      jsonConvert.convert<String>(json['user_info_firstname']);
  if (userInfoFirstname != null) {
    languageEntity.userInfoFirstname = userInfoFirstname;
  }
  final String? userInfoFirstnameHint =
      jsonConvert.convert<String>(json['user_info_firstname_hint']);
  if (userInfoFirstnameHint != null) {
    languageEntity.userInfoFirstnameHint = userInfoFirstnameHint;
  }
  final String? userInfoLastname =
      jsonConvert.convert<String>(json['user_info_lastname']);
  if (userInfoLastname != null) {
    languageEntity.userInfoLastname = userInfoLastname;
  }
  final String? userInfoLastnameHint =
      jsonConvert.convert<String>(json['user_info_lastname_hint']);
  if (userInfoLastnameHint != null) {
    languageEntity.userInfoLastnameHint = userInfoLastnameHint;
  }
  final String? userInfoGender =
      jsonConvert.convert<String>(json['user_info_gender']);
  if (userInfoGender != null) {
    languageEntity.userInfoGender = userInfoGender;
  }
  final String? userInfoBirthday =
      jsonConvert.convert<String>(json['user_info_birthday']);
  if (userInfoBirthday != null) {
    languageEntity.userInfoBirthday = userInfoBirthday;
  }
  final String? userInfoGenderHint =
      jsonConvert.convert<String>(json['user_info_gender_hint']);
  if (userInfoGenderHint != null) {
    languageEntity.userInfoGenderHint = userInfoGenderHint;
  }
  final String? userInfoBirthdayHint =
      jsonConvert.convert<String>(json['user_info_birthday_hint']);
  if (userInfoBirthdayHint != null) {
    languageEntity.userInfoBirthdayHint = userInfoBirthdayHint;
  }
  final String? userInfoAlertTitle =
      jsonConvert.convert<String>(json['user_info_alert_title']);
  if (userInfoAlertTitle != null) {
    languageEntity.userInfoAlertTitle = userInfoAlertTitle;
  }
  final String? createPasswordHeader =
      jsonConvert.convert<String>(json['create_password_header']);
  if (createPasswordHeader != null) {
    languageEntity.createPasswordHeader = createPasswordHeader;
  }
  final String? createPasswordTitle =
      jsonConvert.convert<String>(json['create_password_title']);
  if (createPasswordTitle != null) {
    languageEntity.createPasswordTitle = createPasswordTitle;
  }
  final String? createPasswordHelp =
      jsonConvert.convert<String>(json['create_password_help']);
  if (createPasswordHelp != null) {
    languageEntity.createPasswordHelp = createPasswordHelp;
  }
  final String? createPasswordAgainTitle =
      jsonConvert.convert<String>(json['create_password_again_title']);
  if (createPasswordAgainTitle != null) {
    languageEntity.createPasswordAgainTitle = createPasswordAgainTitle;
  }
  final String? createUserSuccessTitle =
      jsonConvert.convert<String>(json['create_user_success_title']);
  if (createUserSuccessTitle != null) {
    languageEntity.createUserSuccessTitle = createUserSuccessTitle;
  }
  final String? createUserSuccessStart =
      jsonConvert.convert<String>(json['create_user_success_start']);
  if (createUserSuccessStart != null) {
    languageEntity.createUserSuccessStart = createUserSuccessStart;
  }
  final String? forgotPasswordTitle =
      jsonConvert.convert<String>(json['forgot_password_title']);
  if (forgotPasswordTitle != null) {
    languageEntity.forgotPasswordTitle = forgotPasswordTitle;
  }
  final String? forgotPasswordPhoneNo =
      jsonConvert.convert<String>(json['forgot_password_phone_no']);
  if (forgotPasswordPhoneNo != null) {
    languageEntity.forgotPasswordPhoneNo = forgotPasswordPhoneNo;
  }
  final String? forgotPasswordPhoneNoHint =
      jsonConvert.convert<String>(json['forgot_password_phone_no_hint']);
  if (forgotPasswordPhoneNoHint != null) {
    languageEntity.forgotPasswordPhoneNoHint = forgotPasswordPhoneNoHint;
  }
  final String? forgotPasswordMessage =
      jsonConvert.convert<String>(json['forgot_password_message']);
  if (forgotPasswordMessage != null) {
    languageEntity.forgotPasswordMessage = forgotPasswordMessage;
  }
  final String? forgotPasswordCreateNewPassword =
      jsonConvert.convert<String>(json['forgot_password_create_new_password']);
  if (forgotPasswordCreateNewPassword != null) {
    languageEntity.forgotPasswordCreateNewPassword =
        forgotPasswordCreateNewPassword;
  }
  final String? profile = jsonConvert.convert<String>(json['profile']);
  if (profile != null) {
    languageEntity.profile = profile;
  }
  final String? profileTitle =
      jsonConvert.convert<String>(json['profile_title']);
  if (profileTitle != null) {
    languageEntity.profileTitle = profileTitle;
  }
  final String? profileEdit = jsonConvert.convert<String>(json['profile_edit']);
  if (profileEdit != null) {
    languageEntity.profileEdit = profileEdit;
  }
  final String? profileInfo = jsonConvert.convert<String>(json['profile_info']);
  if (profileInfo != null) {
    languageEntity.profileInfo = profileInfo;
  }
  final String? profileSetting =
      jsonConvert.convert<String>(json['profile_setting']);
  if (profileSetting != null) {
    languageEntity.profileSetting = profileSetting;
  }
  final String? profileHelpCenter =
      jsonConvert.convert<String>(json['profile_help_center']);
  if (profileHelpCenter != null) {
    languageEntity.profileHelpCenter = profileHelpCenter;
  }
  final String? profileContactUs =
      jsonConvert.convert<String>(json['profile_contact_us']);
  if (profileContactUs != null) {
    languageEntity.profileContactUs = profileContactUs;
  }
  final String? profileTermsOfService =
      jsonConvert.convert<String>(json['profile_terms_of_service']);
  if (profileTermsOfService != null) {
    languageEntity.profileTermsOfService = profileTermsOfService;
  }
  final String? profilePrivacyPolicy =
      jsonConvert.convert<String>(json['profile_privacy_policy']);
  if (profilePrivacyPolicy != null) {
    languageEntity.profilePrivacyPolicy = profilePrivacyPolicy;
  }
  final String? settingTitle =
      jsonConvert.convert<String>(json['setting_title']);
  if (settingTitle != null) {
    languageEntity.settingTitle = settingTitle;
  }
  final String? settingEditProfile =
      jsonConvert.convert<String>(json['setting_edit_profile']);
  if (settingEditProfile != null) {
    languageEntity.settingEditProfile = settingEditProfile;
  }
  final String? settingNotification =
      jsonConvert.convert<String>(json['setting_notification']);
  if (settingNotification != null) {
    languageEntity.settingNotification = settingNotification;
  }
  final String? settingChangeLanguage =
      jsonConvert.convert<String>(json['setting_change_language']);
  if (settingChangeLanguage != null) {
    languageEntity.settingChangeLanguage = settingChangeLanguage;
  }
  final String? settingChangePassword =
      jsonConvert.convert<String>(json['setting_change_password']);
  if (settingChangePassword != null) {
    languageEntity.settingChangePassword = settingChangePassword;
  }
  final String? settingLogout =
      jsonConvert.convert<String>(json['setting_logout']);
  if (settingLogout != null) {
    languageEntity.settingLogout = settingLogout;
  }
  final String? settingCurrentVersion =
      jsonConvert.convert<String>(json['setting_current_version']);
  if (settingCurrentVersion != null) {
    languageEntity.settingCurrentVersion = settingCurrentVersion;
  }
  final String? editProfileTitle =
      jsonConvert.convert<String>(json['edit_profile_title']);
  if (editProfileTitle != null) {
    languageEntity.editProfileTitle = editProfileTitle;
  }
  final String? editProfilePhoneNo =
      jsonConvert.convert<String>(json['edit_profile_phone_no']);
  if (editProfilePhoneNo != null) {
    languageEntity.editProfilePhoneNo = editProfilePhoneNo;
  }
  final String? editProfileName =
      jsonConvert.convert<String>(json['edit_profile_name']);
  if (editProfileName != null) {
    languageEntity.editProfileName = editProfileName;
  }
  final String? editProfileNameHint =
      jsonConvert.convert<String>(json['edit_profile_name_hint']);
  if (editProfileNameHint != null) {
    languageEntity.editProfileNameHint = editProfileNameHint;
  }
  final String? editProfileLastName =
      jsonConvert.convert<String>(json['edit_profile_last_name']);
  if (editProfileLastName != null) {
    languageEntity.editProfileLastName = editProfileLastName;
  }
  final String? editProfileLastNameHint =
      jsonConvert.convert<String>(json['edit_profile_last_name_hint']);
  if (editProfileLastNameHint != null) {
    languageEntity.editProfileLastNameHint = editProfileLastNameHint;
  }
  final String? editProfileBirthday =
      jsonConvert.convert<String>(json['edit_profile_birthday']);
  if (editProfileBirthday != null) {
    languageEntity.editProfileBirthday = editProfileBirthday;
  }
  final String? editProfileChooseBirthDate =
      jsonConvert.convert<String>(json['edit_profile_choose_birth_date']);
  if (editProfileChooseBirthDate != null) {
    languageEntity.editProfileChooseBirthDate = editProfileChooseBirthDate;
  }
  final String? editProfileGender =
      jsonConvert.convert<String>(json['edit_profile_gender']);
  if (editProfileGender != null) {
    languageEntity.editProfileGender = editProfileGender;
  }
  final String? editProfileSelectGender =
      jsonConvert.convert<String>(json['edit_profile_select_gender']);
  if (editProfileSelectGender != null) {
    languageEntity.editProfileSelectGender = editProfileSelectGender;
  }
  final String? editProfileSave =
      jsonConvert.convert<String>(json['edit_profile_save']);
  if (editProfileSave != null) {
    languageEntity.editProfileSave = editProfileSave;
  }
  final String? editProfilePhotoCameraErrorMessage = jsonConvert
      .convert<String>(json['edit_profile_photo_camera_error_message']);
  if (editProfilePhotoCameraErrorMessage != null) {
    languageEntity.editProfilePhotoCameraErrorMessage =
        editProfilePhotoCameraErrorMessage;
  }
  final String? editProfilePhotoLibraryErrorMessage = jsonConvert
      .convert<String>(json['edit_profile_photo_library_error_message']);
  if (editProfilePhotoLibraryErrorMessage != null) {
    languageEntity.editProfilePhotoLibraryErrorMessage =
        editProfilePhotoLibraryErrorMessage;
  }
  final String? genderDialogTitle =
      jsonConvert.convert<String>(json['gender_dialog_title']);
  if (genderDialogTitle != null) {
    languageEntity.genderDialogTitle = genderDialogTitle;
  }
  final String? genderDialogMale =
      jsonConvert.convert<String>(json['gender_dialog_male']);
  if (genderDialogMale != null) {
    languageEntity.genderDialogMale = genderDialogMale;
  }
  final String? genderDialogFemale =
      jsonConvert.convert<String>(json['gender_dialog_female']);
  if (genderDialogFemale != null) {
    languageEntity.genderDialogFemale = genderDialogFemale;
  }
  final String? genderDialogEveryone =
      jsonConvert.convert<String>(json['gender_dialog_everyone']);
  if (genderDialogEveryone != null) {
    languageEntity.genderDialogEveryone = genderDialogEveryone;
  }
  final String? genderDialogNotSpecified =
      jsonConvert.convert<String>(json['gender_dialog_not_specified']);
  if (genderDialogNotSpecified != null) {
    languageEntity.genderDialogNotSpecified = genderDialogNotSpecified;
  }
  final String? male = jsonConvert.convert<String>(json['male']);
  if (male != null) {
    languageEntity.male = male;
  }
  final String? female = jsonConvert.convert<String>(json['female']);
  if (female != null) {
    languageEntity.female = female;
  }
  final String? unknown = jsonConvert.convert<String>(json['unknown']);
  if (unknown != null) {
    languageEntity.unknown = unknown;
  }
  final String? changePasswordTitle =
      jsonConvert.convert<String>(json['change_password_title']);
  if (changePasswordTitle != null) {
    languageEntity.changePasswordTitle = changePasswordTitle;
  }
  final String? changePasswordOldPassword =
      jsonConvert.convert<String>(json['change_password_old_password']);
  if (changePasswordOldPassword != null) {
    languageEntity.changePasswordOldPassword = changePasswordOldPassword;
  }
  final String? changePasswordOldPasswordHint =
      jsonConvert.convert<String>(json['change_password_old_password_hint']);
  if (changePasswordOldPasswordHint != null) {
    languageEntity.changePasswordOldPasswordHint =
        changePasswordOldPasswordHint;
  }
  final String? changePasswordNewPassword =
      jsonConvert.convert<String>(json['change_password_new_password']);
  if (changePasswordNewPassword != null) {
    languageEntity.changePasswordNewPassword = changePasswordNewPassword;
  }
  final String? changePasswordNewPasswordHint =
      jsonConvert.convert<String>(json['change_password_new_password_hint']);
  if (changePasswordNewPasswordHint != null) {
    languageEntity.changePasswordNewPasswordHint =
        changePasswordNewPasswordHint;
  }
  final String? changePasswordNewPasswordHelp =
      jsonConvert.convert<String>(json['change_password_new_password_help']);
  if (changePasswordNewPasswordHelp != null) {
    languageEntity.changePasswordNewPasswordHelp =
        changePasswordNewPasswordHelp;
  }
  final String? changePasswordNewPasswordAgain =
      jsonConvert.convert<String>(json['change_password_new_password_again']);
  if (changePasswordNewPasswordAgain != null) {
    languageEntity.changePasswordNewPasswordAgain =
        changePasswordNewPasswordAgain;
  }
  final String? errorDialog = jsonConvert.convert<String>(json['error_dialog']);
  if (errorDialog != null) {
    languageEntity.errorDialog = errorDialog;
  }
  final String? actionSheetCamera =
      jsonConvert.convert<String>(json['action_sheet_camera']);
  if (actionSheetCamera != null) {
    languageEntity.actionSheetCamera = actionSheetCamera;
  }
  final String? actionSheetPhotoLibrary =
      jsonConvert.convert<String>(json['action_sheet_photo_library']);
  if (actionSheetPhotoLibrary != null) {
    languageEntity.actionSheetPhotoLibrary = actionSheetPhotoLibrary;
  }
  final String? actionSheetProfileImage =
      jsonConvert.convert<String>(json['action_sheet_profile_image']);
  if (actionSheetProfileImage != null) {
    languageEntity.actionSheetProfileImage = actionSheetProfileImage;
  }
  final String? actionCancel =
      jsonConvert.convert<String>(json['action_cancel']);
  if (actionCancel != null) {
    languageEntity.actionCancel = actionCancel;
  }
  final String? countrySelectTitle =
      jsonConvert.convert<String>(json['country_select_title']);
  if (countrySelectTitle != null) {
    languageEntity.countrySelectTitle = countrySelectTitle;
  }
  final String? notificationHeader =
      jsonConvert.convert<String>(json['notification_header']);
  if (notificationHeader != null) {
    languageEntity.notificationHeader = notificationHeader;
  }
  final String? notificationPromotionTitle =
      jsonConvert.convert<String>(json['notification_promotion_title']);
  if (notificationPromotionTitle != null) {
    languageEntity.notificationPromotionTitle = notificationPromotionTitle;
  }
  final String? notificationActivityTitle =
      jsonConvert.convert<String>(json['notification_activity_title']);
  if (notificationActivityTitle != null) {
    languageEntity.notificationActivityTitle = notificationActivityTitle;
  }
  final String? notificationSectionTitle =
      jsonConvert.convert<String>(json['notification_section_title']);
  if (notificationSectionTitle != null) {
    languageEntity.notificationSectionTitle = notificationSectionTitle;
  }
  final String? notificationActivityDesc =
      jsonConvert.convert<String>(json['notification_activity_desc']);
  if (notificationActivityDesc != null) {
    languageEntity.notificationActivityDesc = notificationActivityDesc;
  }
  final String? notificationPromotionDesc =
      jsonConvert.convert<String>(json['notification_promotion_desc']);
  if (notificationPromotionDesc != null) {
    languageEntity.notificationPromotionDesc = notificationPromotionDesc;
  }
  final String? errorDialogTitle =
      jsonConvert.convert<String>(json['error_dialog_title']);
  if (errorDialogTitle != null) {
    languageEntity.errorDialogTitle = errorDialogTitle;
  }
  final String? errorInternet =
      jsonConvert.convert<String>(json['error_internet']);
  if (errorInternet != null) {
    languageEntity.errorInternet = errorInternet;
  }
  final String? errorServerDown =
      jsonConvert.convert<String>(json['error_server_down']);
  if (errorServerDown != null) {
    languageEntity.errorServerDown = errorServerDown;
  }
  final String? errorTimeout =
      jsonConvert.convert<String>(json['error_timeout']);
  if (errorTimeout != null) {
    languageEntity.errorTimeout = errorTimeout;
  }
  final String? errorDataNotFound =
      jsonConvert.convert<String>(json['error_data_not_found']);
  if (errorDataNotFound != null) {
    languageEntity.errorDataNotFound = errorDataNotFound;
  }
  final String? errorNotEnoughCoins =
      jsonConvert.convert<String>(json['error_not_enough_coins']);
  if (errorNotEnoughCoins != null) {
    languageEntity.errorNotEnoughCoins = errorNotEnoughCoins;
  }
  final String? currentLanguage =
      jsonConvert.convert<String>(json['current_language']);
  if (currentLanguage != null) {
    languageEntity.currentLanguage = currentLanguage;
  }
  final String? profileGuestDesc =
      jsonConvert.convert<String>(json['profile_guest_desc']);
  if (profileGuestDesc != null) {
    languageEntity.profileGuestDesc = profileGuestDesc;
  }
  final String? confirmation =
      jsonConvert.convert<String>(json['confirmation']);
  if (confirmation != null) {
    languageEntity.confirmation = confirmation;
  }
  final String? profileUpdatePasswordSuccess =
      jsonConvert.convert<String>(json['profile_update_password_success']);
  if (profileUpdatePasswordSuccess != null) {
    languageEntity.profileUpdatePasswordSuccess = profileUpdatePasswordSuccess;
  }
  final String? profileCreatePasswordSuccess =
      jsonConvert.convert<String>(json['profile_create_password_success']);
  if (profileCreatePasswordSuccess != null) {
    languageEntity.profileCreatePasswordSuccess = profileCreatePasswordSuccess;
  }
  final String? validatePasswordNotMatched =
      jsonConvert.convert<String>(json['validate_password_not_matched']);
  if (validatePasswordNotMatched != null) {
    languageEntity.validatePasswordNotMatched = validatePasswordNotMatched;
  }
  final String? validateTextFieldFillUpText =
      jsonConvert.convert<String>(json['validate_text_field_fill_up_text']);
  if (validateTextFieldFillUpText != null) {
    languageEntity.validateTextFieldFillUpText = validateTextFieldFillUpText;
  }
  final String? validateTextFieldPasswordText =
      jsonConvert.convert<String>(json['validate_text_field_password_text']);
  if (validateTextFieldPasswordText != null) {
    languageEntity.validateTextFieldPasswordText =
        validateTextFieldPasswordText;
  }
  final String? validateTextFieldPasswordCheckCharactersText =
      jsonConvert.convert<String>(
          json['validate_text_field_password_check_characters_text']);
  if (validateTextFieldPasswordCheckCharactersText != null) {
    languageEntity.validateTextFieldPasswordCheckCharactersText =
        validateTextFieldPasswordCheckCharactersText;
  }
  final String? validateTextFieldPhoneNumberLengthText = jsonConvert
      .convert<String>(json['validate_text_field_phone_number_length_text']);
  if (validateTextFieldPhoneNumberLengthText != null) {
    languageEntity.validateTextFieldPhoneNumberLengthText =
        validateTextFieldPhoneNumberLengthText;
  }
  final String? validateTextFieldPhoneNumberLength20Text = jsonConvert
      .convert<String>(json['validate_text_field_phone_number_length_20_text']);
  if (validateTextFieldPhoneNumberLength20Text != null) {
    languageEntity.validateTextFieldPhoneNumberLength20Text =
        validateTextFieldPhoneNumberLength20Text;
  }
  final String? validateTextFieldPhoneNumberInvalidText = jsonConvert
      .convert<String>(json['validate_text_field_phone_number_invalid_text']);
  if (validateTextFieldPhoneNumberInvalidText != null) {
    languageEntity.validateTextFieldPhoneNumberInvalidText =
        validateTextFieldPhoneNumberInvalidText;
  }
  final String? validateTextFieldFirstNameLength20Text = jsonConvert
      .convert<String>(json['validate_text_field_first_name_length_20_text']);
  if (validateTextFieldFirstNameLength20Text != null) {
    languageEntity.validateTextFieldFirstNameLength20Text =
        validateTextFieldFirstNameLength20Text;
  }
  final String? validateTextFieldFirstNameCheckCharactersText =
      jsonConvert.convert<String>(
          json['validate_text_field_first_name_check_characters_text']);
  if (validateTextFieldFirstNameCheckCharactersText != null) {
    languageEntity.validateTextFieldFirstNameCheckCharactersText =
        validateTextFieldFirstNameCheckCharactersText;
  }
  final String? validateTextFieldLastNameLength20Text = jsonConvert
      .convert<String>(json['validate_text_field_last_name_length_20_text']);
  if (validateTextFieldLastNameLength20Text != null) {
    languageEntity.validateTextFieldLastNameLength20Text =
        validateTextFieldLastNameLength20Text;
  }
  final String? validateTextFieldLastNameCheckCharactersText =
      jsonConvert.convert<String>(
          json['validate_text_field_last_name_check_characters_text']);
  if (validateTextFieldLastNameCheckCharactersText != null) {
    languageEntity.validateTextFieldLastNameCheckCharactersText =
        validateTextFieldLastNameCheckCharactersText;
  }
  final String? selectAddressTitle =
      jsonConvert.convert<String>(json['select_address_title']);
  if (selectAddressTitle != null) {
    languageEntity.selectAddressTitle = selectAddressTitle;
  }
  final String? selectAddressFavorites =
      jsonConvert.convert<String>(json['select_address_favorites']);
  if (selectAddressFavorites != null) {
    languageEntity.selectAddressFavorites = selectAddressFavorites;
  }
  final String? selectAddressHome =
      jsonConvert.convert<String>(json['select_address_home']);
  if (selectAddressHome != null) {
    languageEntity.selectAddressHome = selectAddressHome;
  }
  final String? selectAddressWork =
      jsonConvert.convert<String>(json['select_address_work']);
  if (selectAddressWork != null) {
    languageEntity.selectAddressWork = selectAddressWork;
  }
  final String? selectAddressOther =
      jsonConvert.convert<String>(json['select_address_other']);
  if (selectAddressOther != null) {
    languageEntity.selectAddressOther = selectAddressOther;
  }
  final String? selectAddressAddAddress =
      jsonConvert.convert<String>(json['select_address_add_address']);
  if (selectAddressAddAddress != null) {
    languageEntity.selectAddressAddAddress = selectAddressAddAddress;
  }
  final String? hintInputHome =
      jsonConvert.convert<String>(json['hint_input_home']);
  if (hintInputHome != null) {
    languageEntity.hintInputHome = hintInputHome;
  }
  final String? createAddressSavePlace =
      jsonConvert.convert<String>(json['create_address_save_place']);
  if (createAddressSavePlace != null) {
    languageEntity.createAddressSavePlace = createAddressSavePlace;
  }
  final String? modalOverDistanceTitle =
      jsonConvert.convert<String>(json['modal_over_distance_title']);
  if (modalOverDistanceTitle != null) {
    languageEntity.modalOverDistanceTitle = modalOverDistanceTitle;
  }
  final String? modalOverDistanceDesc =
      jsonConvert.convert<String>(json['modal_over_distance_desc']);
  if (modalOverDistanceDesc != null) {
    languageEntity.modalOverDistanceDesc = modalOverDistanceDesc;
  }
  final String? modalOverDistanceChangeAddress =
      jsonConvert.convert<String>(json['modal_over_distance_change_address']);
  if (modalOverDistanceChangeAddress != null) {
    languageEntity.modalOverDistanceChangeAddress =
        modalOverDistanceChangeAddress;
  }
  final String? userLocationTile =
      jsonConvert.convert<String>(json['user_location_tile']);
  if (userLocationTile != null) {
    languageEntity.userLocationTile = userLocationTile;
  }
  final String? userLocationDesc =
      jsonConvert.convert<String>(json['user_location_desc']);
  if (userLocationDesc != null) {
    languageEntity.userLocationDesc = userLocationDesc;
  }
  final String? userLocationAllowLocation =
      jsonConvert.convert<String>(json['user_location_allow_location']);
  if (userLocationAllowLocation != null) {
    languageEntity.userLocationAllowLocation = userLocationAllowLocation;
  }
  final String? userLocationEnterMyLocation =
      jsonConvert.convert<String>(json['user_location_enter_my_location']);
  if (userLocationEnterMyLocation != null) {
    languageEntity.userLocationEnterMyLocation = userLocationEnterMyLocation;
  }
  final String? userLocationUseLocation =
      jsonConvert.convert<String>(json['user_location_use_location']);
  if (userLocationUseLocation != null) {
    languageEntity.userLocationUseLocation = userLocationUseLocation;
  }
  final String? userLocationThisYourLocation =
      jsonConvert.convert<String>(json['user_location_this_your_location']);
  if (userLocationThisYourLocation != null) {
    languageEntity.userLocationThisYourLocation = userLocationThisYourLocation;
  }
  final String? userLocationSkipStep =
      jsonConvert.convert<String>(json['user_location_skip_step']);
  if (userLocationSkipStep != null) {
    languageEntity.userLocationSkipStep = userLocationSkipStep;
  }
  final String? userLocationLastEntered =
      jsonConvert.convert<String>(json['user_location_last_entered']);
  if (userLocationLastEntered != null) {
    languageEntity.userLocationLastEntered = userLocationLastEntered;
  }
  final String? userLocationYourLocation =
      jsonConvert.convert<String>(json['user_location_your_location']);
  if (userLocationYourLocation != null) {
    languageEntity.userLocationYourLocation = userLocationYourLocation;
  }
  final String? userLocationFavoritePlaces =
      jsonConvert.convert<String>(json['user_location_favorite_places']);
  if (userLocationFavoritePlaces != null) {
    languageEntity.userLocationFavoritePlaces = userLocationFavoritePlaces;
  }
  final String? allCategories =
      jsonConvert.convert<String>(json['all_categories']);
  if (allCategories != null) {
    languageEntity.allCategories = allCategories;
  }
  final String? selectByCategory =
      jsonConvert.convert<String>(json['select_by_category']);
  if (selectByCategory != null) {
    languageEntity.selectByCategory = selectByCategory;
  }
  final String? seeAll = jsonConvert.convert<String>(json['see_all']);
  if (seeAll != null) {
    languageEntity.seeAll = seeAll;
  }
  final String? menu = jsonConvert.convert<String>(json['menu']);
  if (menu != null) {
    languageEntity.menu = menu;
  }
  final String? popularMenu = jsonConvert.convert<String>(json['popular_menu']);
  if (popularMenu != null) {
    languageEntity.popularMenu = popularMenu;
  }
  final String? recommendedMenu =
      jsonConvert.convert<String>(json['recommended_menu']);
  if (recommendedMenu != null) {
    languageEntity.recommendedMenu = recommendedMenu;
  }
  final String? deliveryAddress =
      jsonConvert.convert<String>(json['delivery_address']);
  if (deliveryAddress != null) {
    languageEntity.deliveryAddress = deliveryAddress;
  }
  final String? addressSavePlace =
      jsonConvert.convert<String>(json['address_save_place']);
  if (addressSavePlace != null) {
    languageEntity.addressSavePlace = addressSavePlace;
  }
  final String? addressSavePlaceDesc =
      jsonConvert.convert<String>(json['address_save_place_desc']);
  if (addressSavePlaceDesc != null) {
    languageEntity.addressSavePlaceDesc = addressSavePlaceDesc;
  }
  final String? useCurrentLocation =
      jsonConvert.convert<String>(json['use_current_location']);
  if (useCurrentLocation != null) {
    languageEntity.useCurrentLocation = useCurrentLocation;
  }
  final String? paymentMethod =
      jsonConvert.convert<String>(json['payment_method']);
  if (paymentMethod != null) {
    languageEntity.paymentMethod = paymentMethod;
  }
  final String? ccw = jsonConvert.convert<String>(json['ccw']);
  if (ccw != null) {
    languageEntity.ccw = ccw;
  }
  final String? ibanking = jsonConvert.convert<String>(json['ibanking']);
  if (ibanking != null) {
    languageEntity.ibanking = ibanking;
  }
  final String? free = jsonConvert.convert<String>(json['free']);
  if (free != null) {
    languageEntity.free = free;
  }
  final String? banktrans = jsonConvert.convert<String>(json['banktrans']);
  if (banktrans != null) {
    languageEntity.banktrans = banktrans;
  }
  final String? cod = jsonConvert.convert<String>(json['cod']);
  if (cod != null) {
    languageEntity.cod = cod;
  }
  final String? promptpay = jsonConvert.convert<String>(json['promptpay']);
  if (promptpay != null) {
    languageEntity.promptpay = promptpay;
  }
  final String? internetBankingBbl =
      jsonConvert.convert<String>(json['internet_banking_bbl']);
  if (internetBankingBbl != null) {
    languageEntity.internetBankingBbl = internetBankingBbl;
  }
  final String? internetBankingScb =
      jsonConvert.convert<String>(json['internet_banking_scb']);
  if (internetBankingScb != null) {
    languageEntity.internetBankingScb = internetBankingScb;
  }
  final String? internetBankingKtb =
      jsonConvert.convert<String>(json['internet_banking_ktb']);
  if (internetBankingKtb != null) {
    languageEntity.internetBankingKtb = internetBankingKtb;
  }
  final String? internetBankingBay =
      jsonConvert.convert<String>(json['internet_banking_bay']);
  if (internetBankingBay != null) {
    languageEntity.internetBankingBay = internetBankingBay;
  }
  final String? internetBankingTbank =
      jsonConvert.convert<String>(json['internet_banking_tbank']);
  if (internetBankingTbank != null) {
    languageEntity.internetBankingTbank = internetBankingTbank;
  }
  final String? ibankingDesc1 =
      jsonConvert.convert<String>(json['ibanking_desc1']);
  if (ibankingDesc1 != null) {
    languageEntity.ibankingDesc1 = ibankingDesc1;
  }
  final String? ibankingDesc2 =
      jsonConvert.convert<String>(json['ibanking_desc2']);
  if (ibankingDesc2 != null) {
    languageEntity.ibankingDesc2 = ibankingDesc2;
  }
  final String? ibankingDesc3 =
      jsonConvert.convert<String>(json['ibanking_desc3']);
  if (ibankingDesc3 != null) {
    languageEntity.ibankingDesc3 = ibankingDesc3;
  }
  final String? subTotal = jsonConvert.convert<String>(json['sub_total']);
  if (subTotal != null) {
    languageEntity.subTotal = subTotal;
  }
  final String? shipping = jsonConvert.convert<String>(json['shipping']);
  if (shipping != null) {
    languageEntity.shipping = shipping;
  }
  final String? tax = jsonConvert.convert<String>(json['tax']);
  if (tax != null) {
    languageEntity.tax = tax;
  }
  final String? paymentFee = jsonConvert.convert<String>(json['payment_fee']);
  if (paymentFee != null) {
    languageEntity.paymentFee = paymentFee;
  }
  final String? discount = jsonConvert.convert<String>(json['discount']);
  if (discount != null) {
    languageEntity.discount = discount;
  }
  final String? total = jsonConvert.convert<String>(json['total']);
  if (total != null) {
    languageEntity.total = total;
  }
  final String? freeShipping =
      jsonConvert.convert<String>(json['free-shipping']);
  if (freeShipping != null) {
    languageEntity.freeShipping = freeShipping;
  }
  final String? checkoutDeliverTo =
      jsonConvert.convert<String>(json['checkout_deliver_to']);
  if (checkoutDeliverTo != null) {
    languageEntity.checkoutDeliverTo = checkoutDeliverTo;
  }
  final String? checkoutTitleOrder =
      jsonConvert.convert<String>(json['checkout_title_order']);
  if (checkoutTitleOrder != null) {
    languageEntity.checkoutTitleOrder = checkoutTitleOrder;
  }
  final String? checkoutPaymentMethod =
      jsonConvert.convert<String>(json['checkout_payment_method']);
  if (checkoutPaymentMethod != null) {
    languageEntity.checkoutPaymentMethod = checkoutPaymentMethod;
  }
  final String? checkoutGrandTotal =
      jsonConvert.convert<String>(json['checkout_grand_total']);
  if (checkoutGrandTotal != null) {
    languageEntity.checkoutGrandTotal = checkoutGrandTotal;
  }
  final String? checkoutConfirmation =
      jsonConvert.convert<String>(json['checkout_confirmation']);
  if (checkoutConfirmation != null) {
    languageEntity.checkoutConfirmation = checkoutConfirmation;
  }
  final String? checkoutRemark =
      jsonConvert.convert<String>(json['checkout_remark']);
  if (checkoutRemark != null) {
    languageEntity.checkoutRemark = checkoutRemark;
  }
  final String? verifyAccount =
      jsonConvert.convert<String>(json['verify_account']);
  if (verifyAccount != null) {
    languageEntity.verifyAccount = verifyAccount;
  }
  final String? unverifyAccount =
      jsonConvert.convert<String>(json['unverify_account']);
  if (unverifyAccount != null) {
    languageEntity.unverifyAccount = unverifyAccount;
  }
  final String? ordered = jsonConvert.convert<String>(json['ordered']);
  if (ordered != null) {
    languageEntity.ordered = ordered;
  }
  final String? reserved = jsonConvert.convert<String>(json['reserved']);
  if (reserved != null) {
    languageEntity.reserved = reserved;
  }
  final String? cancelled = jsonConvert.convert<String>(json['cancelled']);
  if (cancelled != null) {
    languageEntity.cancelled = cancelled;
  }
  final String? versionTitle =
      jsonConvert.convert<String>(json['version_title']);
  if (versionTitle != null) {
    languageEntity.versionTitle = versionTitle;
  }
  final String? versionDesc = jsonConvert.convert<String>(json['version_desc']);
  if (versionDesc != null) {
    languageEntity.versionDesc = versionDesc;
  }
  final String? versionUpdateLater =
      jsonConvert.convert<String>(json['version_update_later']);
  if (versionUpdateLater != null) {
    languageEntity.versionUpdateLater = versionUpdateLater;
  }
  final String? versionUpdateNow =
      jsonConvert.convert<String>(json['version_update_now']);
  if (versionUpdateNow != null) {
    languageEntity.versionUpdateNow = versionUpdateNow;
  }
  final String? versionForceUpdate =
      jsonConvert.convert<String>(json['version_force_update']);
  if (versionForceUpdate != null) {
    languageEntity.versionForceUpdate = versionForceUpdate;
  }
  return languageEntity;
}

Map<String, dynamic> $LanguageEntityToJson(LanguageEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['main_bottom_menu_home'] = entity.mainBottomMenuHome;
  data['main_bottom_menu_lotto'] = entity.mainBottomMenuLotto;
  data['main_bottom_menu_notifications'] = entity.mainBottomMenuNotifications;
  data['main_bottom_menu_profile'] = entity.mainBottomMenuProfile;
  data['common_confirm'] = entity.commonConfirm;
  data['common_reject'] = entity.commonReject;
  data['common_next'] = entity.commonNext;
  data['common_cancel'] = entity.commonCancel;
  data['common_search'] = entity.commonSearch;
  data['common_empty'] = entity.commonEmpty;
  data['common_yes'] = entity.commonYes;
  data['common_no'] = entity.commonNo;
  data['common_album'] = entity.commonAlbum;
  data['common_remark'] = entity.commonRemark;
  data['common_choose'] = entity.commonChoose;
  data['common_close'] = entity.commonClose;
  data['common_change'] = entity.commonChange;
  data['common_edit'] = entity.commonEdit;
  data['unit_minute'] = entity.unitMinute;
  data['unit_distance_km'] = entity.unitDistanceKm;
  data['common_deliver_to'] = entity.commonDeliverTo;
  data['camera_title'] = entity.cameraTitle;
  data['photo_library_title'] = entity.photoLibraryTitle;
  data['login_phone_no_hint'] = entity.loginPhoneNoHint;
  data['login_password_hint'] = entity.loginPasswordHint;
  data['login_btn_submit'] = entity.loginBtnSubmit;
  data['login_btn_register'] = entity.loginBtnRegister;
  data['login_btn_forgot_password'] = entity.loginBtnForgotPassword;
  data['login_btn_fb_title'] = entity.loginBtnFbTitle;
  data['login_title_or'] = entity.loginTitleOr;
  data['login_not_member_title'] = entity.loginNotMemberTitle;
  data['login_not_member_signup_title'] = entity.loginNotMemberSignupTitle;
  data['login_before_mobile_register_title'] =
      entity.loginBeforeMobileRegisterTitle;
  data['register_header'] = entity.registerHeader;
  data['register_input_phone_title'] = entity.registerInputPhoneTitle;
  data['register_input_phone_hint'] = entity.registerInputPhoneHint;
  data['register_otp_message'] = entity.registerOtpMessage;
  data['register_accept'] = entity.registerAccept;
  data['register_terms_and_conditions'] = entity.registerTermsAndConditions;
  data['register_btn_confirm'] = entity.registerBtnConfirm;
  data['register_btn_fb_title'] = entity.registerBtnFbTitle;
  data['register_btn_apple_title'] = entity.registerBtnAppleTitle;
  data['register_btn_mobile_phone_title'] = entity.registerBtnMobilePhoneTitle;
  data['channel_verification_title'] = entity.channelVerificationTitle;
  data['channel_verification_sms'] = entity.channelVerificationSms;
  data['channel_verification_voice_call'] = entity.channelVerificationVoiceCall;
  data['confirm_otp_title'] = entity.confirmOtpTitle;
  data['confirm_otp_message'] = entity.confirmOtpMessage;
  data['confirm_otp_send_again'] = entity.confirmOtpSendAgain;
  data['confirm_otp_waiting_send_again'] = entity.confirmOtpWaitingSendAgain;
  data['second'] = entity.second;
  data['confirm_otp_reference'] = entity.confirmOtpReference;
  data['user_info_header'] = entity.userInfoHeader;
  data['user_info_firstname'] = entity.userInfoFirstname;
  data['user_info_firstname_hint'] = entity.userInfoFirstnameHint;
  data['user_info_lastname'] = entity.userInfoLastname;
  data['user_info_lastname_hint'] = entity.userInfoLastnameHint;
  data['user_info_gender'] = entity.userInfoGender;
  data['user_info_birthday'] = entity.userInfoBirthday;
  data['user_info_gender_hint'] = entity.userInfoGenderHint;
  data['user_info_birthday_hint'] = entity.userInfoBirthdayHint;
  data['user_info_alert_title'] = entity.userInfoAlertTitle;
  data['create_password_header'] = entity.createPasswordHeader;
  data['create_password_title'] = entity.createPasswordTitle;
  data['create_password_help'] = entity.createPasswordHelp;
  data['create_password_again_title'] = entity.createPasswordAgainTitle;
  data['create_user_success_title'] = entity.createUserSuccessTitle;
  data['create_user_success_start'] = entity.createUserSuccessStart;
  data['forgot_password_title'] = entity.forgotPasswordTitle;
  data['forgot_password_phone_no'] = entity.forgotPasswordPhoneNo;
  data['forgot_password_phone_no_hint'] = entity.forgotPasswordPhoneNoHint;
  data['forgot_password_message'] = entity.forgotPasswordMessage;
  data['forgot_password_create_new_password'] =
      entity.forgotPasswordCreateNewPassword;
  data['profile'] = entity.profile;
  data['profile_title'] = entity.profileTitle;
  data['profile_edit'] = entity.profileEdit;
  data['profile_info'] = entity.profileInfo;
  data['profile_setting'] = entity.profileSetting;
  data['profile_help_center'] = entity.profileHelpCenter;
  data['profile_contact_us'] = entity.profileContactUs;
  data['profile_terms_of_service'] = entity.profileTermsOfService;
  data['profile_privacy_policy'] = entity.profilePrivacyPolicy;
  data['setting_title'] = entity.settingTitle;
  data['setting_edit_profile'] = entity.settingEditProfile;
  data['setting_notification'] = entity.settingNotification;
  data['setting_change_language'] = entity.settingChangeLanguage;
  data['setting_change_password'] = entity.settingChangePassword;
  data['setting_logout'] = entity.settingLogout;
  data['setting_current_version'] = entity.settingCurrentVersion;
  data['edit_profile_title'] = entity.editProfileTitle;
  data['edit_profile_phone_no'] = entity.editProfilePhoneNo;
  data['edit_profile_name'] = entity.editProfileName;
  data['edit_profile_name_hint'] = entity.editProfileNameHint;
  data['edit_profile_last_name'] = entity.editProfileLastName;
  data['edit_profile_last_name_hint'] = entity.editProfileLastNameHint;
  data['edit_profile_birthday'] = entity.editProfileBirthday;
  data['edit_profile_choose_birth_date'] = entity.editProfileChooseBirthDate;
  data['edit_profile_gender'] = entity.editProfileGender;
  data['edit_profile_select_gender'] = entity.editProfileSelectGender;
  data['edit_profile_save'] = entity.editProfileSave;
  data['edit_profile_photo_camera_error_message'] =
      entity.editProfilePhotoCameraErrorMessage;
  data['edit_profile_photo_library_error_message'] =
      entity.editProfilePhotoLibraryErrorMessage;
  data['gender_dialog_title'] = entity.genderDialogTitle;
  data['gender_dialog_male'] = entity.genderDialogMale;
  data['gender_dialog_female'] = entity.genderDialogFemale;
  data['gender_dialog_everyone'] = entity.genderDialogEveryone;
  data['gender_dialog_not_specified'] = entity.genderDialogNotSpecified;
  data['male'] = entity.male;
  data['female'] = entity.female;
  data['unknown'] = entity.unknown;
  data['change_password_title'] = entity.changePasswordTitle;
  data['change_password_old_password'] = entity.changePasswordOldPassword;
  data['change_password_old_password_hint'] =
      entity.changePasswordOldPasswordHint;
  data['change_password_new_password'] = entity.changePasswordNewPassword;
  data['change_password_new_password_hint'] =
      entity.changePasswordNewPasswordHint;
  data['change_password_new_password_help'] =
      entity.changePasswordNewPasswordHelp;
  data['change_password_new_password_again'] =
      entity.changePasswordNewPasswordAgain;
  data['error_dialog'] = entity.errorDialog;
  data['action_sheet_camera'] = entity.actionSheetCamera;
  data['action_sheet_photo_library'] = entity.actionSheetPhotoLibrary;
  data['action_sheet_profile_image'] = entity.actionSheetProfileImage;
  data['action_cancel'] = entity.actionCancel;
  data['country_select_title'] = entity.countrySelectTitle;
  data['notification_header'] = entity.notificationHeader;
  data['notification_promotion_title'] = entity.notificationPromotionTitle;
  data['notification_activity_title'] = entity.notificationActivityTitle;
  data['notification_section_title'] = entity.notificationSectionTitle;
  data['notification_activity_desc'] = entity.notificationActivityDesc;
  data['notification_promotion_desc'] = entity.notificationPromotionDesc;
  data['error_dialog_title'] = entity.errorDialogTitle;
  data['error_internet'] = entity.errorInternet;
  data['error_server_down'] = entity.errorServerDown;
  data['error_timeout'] = entity.errorTimeout;
  data['error_data_not_found'] = entity.errorDataNotFound;
  data['error_not_enough_coins'] = entity.errorNotEnoughCoins;
  data['current_language'] = entity.currentLanguage;
  data['profile_guest_desc'] = entity.profileGuestDesc;
  data['confirmation'] = entity.confirmation;
  data['profile_update_password_success'] = entity.profileUpdatePasswordSuccess;
  data['profile_create_password_success'] = entity.profileCreatePasswordSuccess;
  data['validate_password_not_matched'] = entity.validatePasswordNotMatched;
  data['validate_text_field_fill_up_text'] = entity.validateTextFieldFillUpText;
  data['validate_text_field_password_text'] =
      entity.validateTextFieldPasswordText;
  data['validate_text_field_password_check_characters_text'] =
      entity.validateTextFieldPasswordCheckCharactersText;
  data['validate_text_field_phone_number_length_text'] =
      entity.validateTextFieldPhoneNumberLengthText;
  data['validate_text_field_phone_number_length_20_text'] =
      entity.validateTextFieldPhoneNumberLength20Text;
  data['validate_text_field_phone_number_invalid_text'] =
      entity.validateTextFieldPhoneNumberInvalidText;
  data['validate_text_field_first_name_length_20_text'] =
      entity.validateTextFieldFirstNameLength20Text;
  data['validate_text_field_first_name_check_characters_text'] =
      entity.validateTextFieldFirstNameCheckCharactersText;
  data['validate_text_field_last_name_length_20_text'] =
      entity.validateTextFieldLastNameLength20Text;
  data['validate_text_field_last_name_check_characters_text'] =
      entity.validateTextFieldLastNameCheckCharactersText;
  data['select_address_title'] = entity.selectAddressTitle;
  data['select_address_favorites'] = entity.selectAddressFavorites;
  data['select_address_home'] = entity.selectAddressHome;
  data['select_address_work'] = entity.selectAddressWork;
  data['select_address_other'] = entity.selectAddressOther;
  data['select_address_add_address'] = entity.selectAddressAddAddress;
  data['hint_input_home'] = entity.hintInputHome;
  data['create_address_save_place'] = entity.createAddressSavePlace;
  data['modal_over_distance_title'] = entity.modalOverDistanceTitle;
  data['modal_over_distance_desc'] = entity.modalOverDistanceDesc;
  data['modal_over_distance_change_address'] =
      entity.modalOverDistanceChangeAddress;
  data['user_location_tile'] = entity.userLocationTile;
  data['user_location_desc'] = entity.userLocationDesc;
  data['user_location_allow_location'] = entity.userLocationAllowLocation;
  data['user_location_enter_my_location'] = entity.userLocationEnterMyLocation;
  data['user_location_use_location'] = entity.userLocationUseLocation;
  data['user_location_this_your_location'] =
      entity.userLocationThisYourLocation;
  data['user_location_skip_step'] = entity.userLocationSkipStep;
  data['user_location_last_entered'] = entity.userLocationLastEntered;
  data['user_location_your_location'] = entity.userLocationYourLocation;
  data['user_location_favorite_places'] = entity.userLocationFavoritePlaces;
  data['all_categories'] = entity.allCategories;
  data['select_by_category'] = entity.selectByCategory;
  data['see_all'] = entity.seeAll;
  data['menu'] = entity.menu;
  data['popular_menu'] = entity.popularMenu;
  data['recommended_menu'] = entity.recommendedMenu;
  data['delivery_address'] = entity.deliveryAddress;
  data['address_save_place'] = entity.addressSavePlace;
  data['address_save_place_desc'] = entity.addressSavePlaceDesc;
  data['use_current_location'] = entity.useCurrentLocation;
  data['payment_method'] = entity.paymentMethod;
  data['ccw'] = entity.ccw;
  data['ibanking'] = entity.ibanking;
  data['free'] = entity.free;
  data['banktrans'] = entity.banktrans;
  data['cod'] = entity.cod;
  data['promptpay'] = entity.promptpay;
  data['internet_banking_bbl'] = entity.internetBankingBbl;
  data['internet_banking_scb'] = entity.internetBankingScb;
  data['internet_banking_ktb'] = entity.internetBankingKtb;
  data['internet_banking_bay'] = entity.internetBankingBay;
  data['internet_banking_tbank'] = entity.internetBankingTbank;
  data['ibanking_desc1'] = entity.ibankingDesc1;
  data['ibanking_desc2'] = entity.ibankingDesc2;
  data['ibanking_desc3'] = entity.ibankingDesc3;
  data['sub_total'] = entity.subTotal;
  data['shipping'] = entity.shipping;
  data['tax'] = entity.tax;
  data['payment_fee'] = entity.paymentFee;
  data['discount'] = entity.discount;
  data['total'] = entity.total;
  data['free-shipping'] = entity.freeShipping;
  data['checkout_deliver_to'] = entity.checkoutDeliverTo;
  data['checkout_title_order'] = entity.checkoutTitleOrder;
  data['checkout_payment_method'] = entity.checkoutPaymentMethod;
  data['checkout_grand_total'] = entity.checkoutGrandTotal;
  data['checkout_confirmation'] = entity.checkoutConfirmation;
  data['checkout_remark'] = entity.checkoutRemark;
  data['verify_account'] = entity.verifyAccount;
  data['unverify_account'] = entity.unverifyAccount;
  data['ordered'] = entity.ordered;
  data['reserved'] = entity.reserved;
  data['cancelled'] = entity.cancelled;
  data['version_title'] = entity.versionTitle;
  data['version_desc'] = entity.versionDesc;
  data['version_update_later'] = entity.versionUpdateLater;
  data['version_update_now'] = entity.versionUpdateNow;
  data['version_force_update'] = entity.versionForceUpdate;
  return data;
}
