import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/Response_upload_bank_entity.dart';

ResponseUploadBankEntity $ResponseUploadBankEntityFromJson(Map<String, dynamic> json) {
	final ResponseUploadBankEntity responseUploadBankEntity = ResponseUploadBankEntity();
	final ResponseUploadBankData? data = jsonConvert.convert<ResponseUploadBankData>(json['data']);
	if (data != null) {
		responseUploadBankEntity.data = data;
	}
	final ResponseUploadBankBench? bench = jsonConvert.convert<ResponseUploadBankBench>(json['bench']);
	if (bench != null) {
		responseUploadBankEntity.bench = bench;
	}
	return responseUploadBankEntity;
}

Map<String, dynamic> $ResponseUploadBankEntityToJson(ResponseUploadBankEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

ResponseUploadBankData $ResponseUploadBankDataFromJson(Map<String, dynamic> json) {
	final ResponseUploadBankData responseUploadBankData = ResponseUploadBankData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		responseUploadBankData.message = message;
	}
	return responseUploadBankData;
}

Map<String, dynamic> $ResponseUploadBankDataToJson(ResponseUploadBankData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	return data;
}

ResponseUploadBankBench $ResponseUploadBankBenchFromJson(Map<String, dynamic> json) {
	final ResponseUploadBankBench responseUploadBankBench = ResponseUploadBankBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		responseUploadBankBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		responseUploadBankBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		responseUploadBankBench.format = format;
	}
	return responseUploadBankBench;
}

Map<String, dynamic> $ResponseUploadBankBenchToJson(ResponseUploadBankBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}