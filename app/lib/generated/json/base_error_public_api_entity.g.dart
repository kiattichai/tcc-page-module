import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/api/errors/base_error_public_api_entity.dart';

BaseErrorPublicApiEntity $BaseErrorPublicApiEntityFromJson(Map<String, dynamic> json) {
	final BaseErrorPublicApiEntity baseErrorPublicApiEntity = BaseErrorPublicApiEntity();
	final dynamic? errors = jsonConvert.convert<dynamic>(json['errors']);
	if (errors != null) {
		baseErrorPublicApiEntity.errors = errors;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		baseErrorPublicApiEntity.message = message;
	}
	return baseErrorPublicApiEntity;
}

Map<String, dynamic> $BaseErrorPublicApiEntityToJson(BaseErrorPublicApiEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['errors'] = entity.errors;
	data['message'] = entity.message;
	return data;
}