import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/register_agent_module/model/get_profile_agent_entity.dart';

GetProfileAgentEntity $GetProfileAgentEntityFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentEntity getProfileAgentEntity = GetProfileAgentEntity();
  final GetProfileAgentData? data =
      jsonConvert.convert<GetProfileAgentData>(json['data']);
  if (data != null) {
    getProfileAgentEntity.data = data;
  }
  final GetProfileAgentBench? bench =
      jsonConvert.convert<GetProfileAgentBench>(json['bench']);
  if (bench != null) {
    getProfileAgentEntity.bench = bench;
  }
  return getProfileAgentEntity;
}

Map<String, dynamic> $GetProfileAgentEntityToJson(
    GetProfileAgentEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetProfileAgentData $GetProfileAgentDataFromJson(Map<String, dynamic> json) {
  final GetProfileAgentData getProfileAgentData = GetProfileAgentData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentData.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getProfileAgentData.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getProfileAgentData.countryCode = countryCode;
  }
  final dynamic? displayName =
      jsonConvert.convert<dynamic>(json['display_name']);
  if (displayName != null) {
    getProfileAgentData.displayName = displayName;
  }
  final dynamic? avatar = jsonConvert.convert<dynamic>(json['avatar']);
  if (avatar != null) {
    getProfileAgentData.avatar = avatar;
  }
  final String? gender = jsonConvert.convert<String>(json['gender']);
  if (gender != null) {
    getProfileAgentData.gender = gender;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getProfileAgentData.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getProfileAgentData.lastName = lastName;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getProfileAgentData.email = email;
  }
  final String? idcard = jsonConvert.convert<String>(json['idcard']);
  if (idcard != null) {
    getProfileAgentData.idcard = idcard;
  }
  final String? birthday = jsonConvert.convert<String>(json['birthday']);
  if (birthday != null) {
    getProfileAgentData.birthday = birthday;
  }
  final bool? termsAccepted = jsonConvert.convert<bool>(json['terms_accepted']);
  if (termsAccepted != null) {
    getProfileAgentData.termsAccepted = termsAccepted;
  }
  final bool? activated = jsonConvert.convert<bool>(json['activated']);
  if (activated != null) {
    getProfileAgentData.activated = activated;
  }
  final String? activatedAt = jsonConvert.convert<String>(json['activated_at']);
  if (activatedAt != null) {
    getProfileAgentData.activatedAt = activatedAt;
  }
  final bool? blocked = jsonConvert.convert<bool>(json['blocked']);
  if (blocked != null) {
    getProfileAgentData.blocked = blocked;
  }
  final int? profileScore = jsonConvert.convert<int>(json['profile_score']);
  if (profileScore != null) {
    getProfileAgentData.profileScore = profileScore;
  }
  final String? lastLoginAt =
      jsonConvert.convert<String>(json['last_login_at']);
  if (lastLoginAt != null) {
    getProfileAgentData.lastLoginAt = lastLoginAt;
  }
  final String? lastLoginIp =
      jsonConvert.convert<String>(json['last_login_ip']);
  if (lastLoginIp != null) {
    getProfileAgentData.lastLoginIp = lastLoginIp;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getProfileAgentData.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getProfileAgentData.updatedAt = updatedAt;
  }
  final String? referral = jsonConvert.convert<String>(json['referral']);
  if (referral != null) {
    getProfileAgentData.referral = referral;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getProfileAgentData.lang = lang;
  }
  final GetProfileAgentDataWallet? wallet =
      jsonConvert.convert<GetProfileAgentDataWallet>(json['wallet']);
  if (wallet != null) {
    getProfileAgentData.wallet = wallet;
  }
  final GetProfileAgentDataGroup? group =
      jsonConvert.convert<GetProfileAgentDataGroup>(json['group']);
  if (group != null) {
    getProfileAgentData.group = group;
  }
  final GetProfileAgentDataAgent? agent =
      jsonConvert.convert<GetProfileAgentDataAgent>(json['agent']);
  if (agent != null) {
    getProfileAgentData.agent = agent;
  }
  final List<dynamic>? connects =
      jsonConvert.convertListNotNull<dynamic>(json['connects']);
  if (connects != null) {
    getProfileAgentData.connects = connects;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getProfileAgentData.cache = cache;
  }
  return getProfileAgentData;
}

Map<String, dynamic> $GetProfileAgentDataToJson(GetProfileAgentData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['display_name'] = entity.displayName;
  data['avatar'] = entity.avatar;
  data['gender'] = entity.gender;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['email'] = entity.email;
  data['idcard'] = entity.idcard;
  data['birthday'] = entity.birthday;
  data['terms_accepted'] = entity.termsAccepted;
  data['activated'] = entity.activated;
  data['activated_at'] = entity.activatedAt;
  data['blocked'] = entity.blocked;
  data['profile_score'] = entity.profileScore;
  data['last_login_at'] = entity.lastLoginAt;
  data['last_login_ip'] = entity.lastLoginIp;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['referral'] = entity.referral;
  data['lang'] = entity.lang;
  data['wallet'] = entity.wallet?.toJson();
  data['group'] = entity.group?.toJson();
  data['agent'] = entity.agent?.toJson();
  data['connects'] = entity.connects;
  data['cache'] = entity.cache;
  return data;
}

GetProfileAgentDataWallet $GetProfileAgentDataWalletFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataWallet getProfileAgentDataWallet =
      GetProfileAgentDataWallet();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataWallet.id = id;
  }
  final int? amount = jsonConvert.convert<int>(json['amount']);
  if (amount != null) {
    getProfileAgentDataWallet.amount = amount;
  }
  return getProfileAgentDataWallet;
}

Map<String, dynamic> $GetProfileAgentDataWalletToJson(
    GetProfileAgentDataWallet entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['amount'] = entity.amount;
  return data;
}

GetProfileAgentDataGroup $GetProfileAgentDataGroupFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataGroup getProfileAgentDataGroup =
      GetProfileAgentDataGroup();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataGroup.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataGroup.name = name;
  }
  return getProfileAgentDataGroup;
}

Map<String, dynamic> $GetProfileAgentDataGroupToJson(
    GetProfileAgentDataGroup entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProfileAgentDataAgent $GetProfileAgentDataAgentFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgent getProfileAgentDataAgent =
      GetProfileAgentDataAgent();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgent.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getProfileAgentDataAgent.code = code;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    getProfileAgentDataAgent.status = status;
  }
  final GetProfileAgentDataAgentDocumentStatus? documentStatus = jsonConvert
      .convert<GetProfileAgentDataAgentDocumentStatus>(json['document_status']);
  if (documentStatus != null) {
    getProfileAgentDataAgent.documentStatus = documentStatus;
  }
  final String? remark = jsonConvert.convert<String>(json['remark']);
  if (remark != null) {
    getProfileAgentDataAgent.remark = remark;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getProfileAgentDataAgent.address = address;
  }
  final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
  if (zipCode != null) {
    getProfileAgentDataAgent.zipCode = zipCode;
  }
  final GetProfileAgentDataAgentBank? bank =
      jsonConvert.convert<GetProfileAgentDataAgentBank>(json['bank']);
  if (bank != null) {
    getProfileAgentDataAgent.bank = bank;
  }
  final bool? uploadIdcard = jsonConvert.convert<bool>(json['upload_idcard']);
  if (uploadIdcard != null) {
    getProfileAgentDataAgent.uploadIdcard = uploadIdcard;
  }
  final String? documentIdcard =
      jsonConvert.convert<String>(json['document_idcard']);
  if (documentIdcard != null) {
    getProfileAgentDataAgent.documentIdcard = documentIdcard;
  }
  final bool? uploadBankbook =
      jsonConvert.convert<bool>(json['upload_bankbook']);
  if (uploadBankbook != null) {
    getProfileAgentDataAgent.uploadBankbook = uploadBankbook;
  }
  final String? documentBankbook =
      jsonConvert.convert<String>(json['document_bankbook']);
  if (documentBankbook != null) {
    getProfileAgentDataAgent.documentBankbook = documentBankbook;
  }
  final GetProfileAgentDataAgentCountry? country =
      jsonConvert.convert<GetProfileAgentDataAgentCountry>(json['country']);
  if (country != null) {
    getProfileAgentDataAgent.country = country;
  }
  final GetProfileAgentDataAgentProvince? province =
      jsonConvert.convert<GetProfileAgentDataAgentProvince>(json['province']);
  if (province != null) {
    getProfileAgentDataAgent.province = province;
  }
  final GetProfileAgentDataAgentCity? city =
      jsonConvert.convert<GetProfileAgentDataAgentCity>(json['city']);
  if (city != null) {
    getProfileAgentDataAgent.city = city;
  }
  final GetProfileAgentDataAgentDistrict? district =
      jsonConvert.convert<GetProfileAgentDataAgentDistrict>(json['district']);
  if (district != null) {
    getProfileAgentDataAgent.district = district;
  }
  return getProfileAgentDataAgent;
}

Map<String, dynamic> $GetProfileAgentDataAgentToJson(
    GetProfileAgentDataAgent entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['document_status'] = entity.documentStatus?.toJson();
  data['remark'] = entity.remark;
  data['address'] = entity.address;
  data['zip_code'] = entity.zipCode;
  data['bank'] = entity.bank?.toJson();
  data['upload_idcard'] = entity.uploadIdcard;
  data['document_idcard'] = entity.documentIdcard;
  data['upload_bankbook'] = entity.uploadBankbook;
  data['document_bankbook'] = entity.documentBankbook;
  data['country'] = entity.country?.toJson();
  data['province'] = entity.province?.toJson();
  data['city'] = entity.city?.toJson();
  data['district'] = entity.district?.toJson();
  return data;
}

GetProfileAgentDataAgentDocumentStatus
    $GetProfileAgentDataAgentDocumentStatusFromJson(Map<String, dynamic> json) {
  final GetProfileAgentDataAgentDocumentStatus
      getProfileAgentDataAgentDocumentStatus =
      GetProfileAgentDataAgentDocumentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentDocumentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getProfileAgentDataAgentDocumentStatus.text = text;
  }
  return getProfileAgentDataAgentDocumentStatus;
}

Map<String, dynamic> $GetProfileAgentDataAgentDocumentStatusToJson(
    GetProfileAgentDataAgentDocumentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetProfileAgentDataAgentBank $GetProfileAgentDataAgentBankFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgentBank getProfileAgentDataAgentBank =
      GetProfileAgentDataAgentBank();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentBank.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataAgentBank.name = name;
  }
  final String? accountNo = jsonConvert.convert<String>(json['account_no']);
  if (accountNo != null) {
    getProfileAgentDataAgentBank.accountNo = accountNo;
  }
  final String? accountName = jsonConvert.convert<String>(json['account_name']);
  if (accountName != null) {
    getProfileAgentDataAgentBank.accountName = accountName;
  }
  final dynamic? accountType =
      jsonConvert.convert<dynamic>(json['account_type']);
  if (accountType != null) {
    getProfileAgentDataAgentBank.accountType = accountType;
  }
  final String? branch = jsonConvert.convert<String>(json['branch']);
  if (branch != null) {
    getProfileAgentDataAgentBank.branch = branch;
  }
  final String? logo = jsonConvert.convert<String>(json['logo']);
  if (logo != null) {
    getProfileAgentDataAgentBank.logo = logo;
  }
  return getProfileAgentDataAgentBank;
}

Map<String, dynamic> $GetProfileAgentDataAgentBankToJson(
    GetProfileAgentDataAgentBank entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['account_no'] = entity.accountNo;
  data['account_name'] = entity.accountName;
  data['account_type'] = entity.accountType;
  data['branch'] = entity.branch;
  data['logo'] = entity.logo;
  return data;
}

GetProfileAgentDataAgentCountry $GetProfileAgentDataAgentCountryFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgentCountry getProfileAgentDataAgentCountry =
      GetProfileAgentDataAgentCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataAgentCountry.name = name;
  }
  return getProfileAgentDataAgentCountry;
}

Map<String, dynamic> $GetProfileAgentDataAgentCountryToJson(
    GetProfileAgentDataAgentCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProfileAgentDataAgentProvince $GetProfileAgentDataAgentProvinceFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgentProvince getProfileAgentDataAgentProvince =
      GetProfileAgentDataAgentProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataAgentProvince.name = name;
  }
  return getProfileAgentDataAgentProvince;
}

Map<String, dynamic> $GetProfileAgentDataAgentProvinceToJson(
    GetProfileAgentDataAgentProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProfileAgentDataAgentCity $GetProfileAgentDataAgentCityFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgentCity getProfileAgentDataAgentCity =
      GetProfileAgentDataAgentCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataAgentCity.name = name;
  }
  return getProfileAgentDataAgentCity;
}

Map<String, dynamic> $GetProfileAgentDataAgentCityToJson(
    GetProfileAgentDataAgentCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProfileAgentDataAgentDistrict $GetProfileAgentDataAgentDistrictFromJson(
    Map<String, dynamic> json) {
  final GetProfileAgentDataAgentDistrict getProfileAgentDataAgentDistrict =
      GetProfileAgentDataAgentDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getProfileAgentDataAgentDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getProfileAgentDataAgentDistrict.name = name;
  }
  return getProfileAgentDataAgentDistrict;
}

Map<String, dynamic> $GetProfileAgentDataAgentDistrictToJson(
    GetProfileAgentDataAgentDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetProfileAgentBench $GetProfileAgentBenchFromJson(Map<String, dynamic> json) {
  final GetProfileAgentBench getProfileAgentBench = GetProfileAgentBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getProfileAgentBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getProfileAgentBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getProfileAgentBench.format = format;
  }
  return getProfileAgentBench;
}

Map<String, dynamic> $GetProfileAgentBenchToJson(GetProfileAgentBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
