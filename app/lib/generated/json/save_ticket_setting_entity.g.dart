import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_ticket_setting_entity.dart';

SaveTicketSettingEntity $SaveTicketSettingEntityFromJson(
    Map<String, dynamic> json) {
  final SaveTicketSettingEntity saveTicketSettingEntity =
      SaveTicketSettingEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    saveTicketSettingEntity.storeId = storeId;
  }
  final List<SaveTicketSettingSettings>? settings = jsonConvert
      .convertListNotNull<SaveTicketSettingSettings>(json['settings']);
  if (settings != null) {
    saveTicketSettingEntity.settings = settings;
  }
  return saveTicketSettingEntity;
}

Map<String, dynamic> $SaveTicketSettingEntityToJson(
    SaveTicketSettingEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['settings'] = entity.settings?.map((v) => v.toJson()).toList();
  return data;
}

SaveTicketSettingSettings $SaveTicketSettingSettingsFromJson(
    Map<String, dynamic> json) {
  final SaveTicketSettingSettings saveTicketSettingSettings =
      SaveTicketSettingSettings();
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    saveTicketSettingSettings.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    saveTicketSettingSettings.key = key;
  }
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    saveTicketSettingSettings.value = value;
  }
  return saveTicketSettingSettings;
}

Map<String, dynamic> $SaveTicketSettingSettingsToJson(
    SaveTicketSettingSettings entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value;
  return data;
}
