import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/ticket_contents_response_entity.dart';

TicketContentsResponseEntity $TicketContentsResponseEntityFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseEntity ticketContentsResponseEntity = TicketContentsResponseEntity();
	final TicketContentsResponseData? data = jsonConvert.convert<TicketContentsResponseData>(json['data']);
	if (data != null) {
		ticketContentsResponseEntity.data = data;
	}
	final TicketContentsResponseBench? bench = jsonConvert.convert<TicketContentsResponseBench>(json['bench']);
	if (bench != null) {
		ticketContentsResponseEntity.bench = bench;
	}
	return ticketContentsResponseEntity;
}

Map<String, dynamic> $TicketContentsResponseEntityToJson(TicketContentsResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

TicketContentsResponseData $TicketContentsResponseDataFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseData ticketContentsResponseData = TicketContentsResponseData();
	final TicketContentsResponseDataPagination? pagination = jsonConvert.convert<TicketContentsResponseDataPagination>(json['pagination']);
	if (pagination != null) {
		ticketContentsResponseData.pagination = pagination;
	}
	final List<TicketContentsResponseDataRecord>? record = jsonConvert.convertListNotNull<TicketContentsResponseDataRecord>(json['record']);
	if (record != null) {
		ticketContentsResponseData.record = record;
	}
	return ticketContentsResponseData;
}

Map<String, dynamic> $TicketContentsResponseDataToJson(TicketContentsResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['pagination'] = entity.pagination?.toJson();
	data['record'] =  entity.record?.map((v) => v.toJson()).toList();
	return data;
}

TicketContentsResponseDataPagination $TicketContentsResponseDataPaginationFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataPagination ticketContentsResponseDataPagination = TicketContentsResponseDataPagination();
	final int? currentPage = jsonConvert.convert<int>(json['current_page']);
	if (currentPage != null) {
		ticketContentsResponseDataPagination.currentPage = currentPage;
	}
	final int? lastPage = jsonConvert.convert<int>(json['last_page']);
	if (lastPage != null) {
		ticketContentsResponseDataPagination.lastPage = lastPage;
	}
	final int? limit = jsonConvert.convert<int>(json['limit']);
	if (limit != null) {
		ticketContentsResponseDataPagination.limit = limit;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		ticketContentsResponseDataPagination.total = total;
	}
	return ticketContentsResponseDataPagination;
}

Map<String, dynamic> $TicketContentsResponseDataPaginationToJson(TicketContentsResponseDataPagination entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

TicketContentsResponseDataRecord $TicketContentsResponseDataRecordFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecord ticketContentsResponseDataRecord = TicketContentsResponseDataRecord();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecord.id = id;
	}
	final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
	if (slug != null) {
		ticketContentsResponseDataRecord.slug = slug;
	}
	final TicketContentsResponseDataRecordGroup? group = jsonConvert.convert<TicketContentsResponseDataRecordGroup>(json['group']);
	if (group != null) {
		ticketContentsResponseDataRecord.group = group;
	}
	final TicketContentsResponseDataRecordCategory? category = jsonConvert.convert<TicketContentsResponseDataRecordCategory>(json['category']);
	if (category != null) {
		ticketContentsResponseDataRecord.category = category;
	}
	final List<TicketContentsResponseDataRecordFields>? fields = jsonConvert.convertListNotNull<TicketContentsResponseDataRecordFields>(json['fields']);
	if (fields != null) {
		ticketContentsResponseDataRecord.fields = fields;
	}
	final List<TicketContentsResponseDataRecordImages>? images = jsonConvert.convertListNotNull<TicketContentsResponseDataRecordImages>(json['images']);
	if (images != null) {
		ticketContentsResponseDataRecord.images = images;
	}
	final int? viewed = jsonConvert.convert<int>(json['viewed']);
	if (viewed != null) {
		ticketContentsResponseDataRecord.viewed = viewed;
	}
	final int? viewedText = jsonConvert.convert<int>(json['viewed_text']);
	if (viewedText != null) {
		ticketContentsResponseDataRecord.viewedText = viewedText;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		ticketContentsResponseDataRecord.status = status;
	}
	final String? shareUrl = jsonConvert.convert<String>(json['share_url']);
	if (shareUrl != null) {
		ticketContentsResponseDataRecord.shareUrl = shareUrl;
	}
	final String? webviewUrl = jsonConvert.convert<String>(json['webview_url']);
	if (webviewUrl != null) {
		ticketContentsResponseDataRecord.webviewUrl = webviewUrl;
	}
	final int? totalUser = jsonConvert.convert<int>(json['total_user']);
	if (totalUser != null) {
		ticketContentsResponseDataRecord.totalUser = totalUser;
	}
	final String? publishedAt = jsonConvert.convert<String>(json['published_at']);
	if (publishedAt != null) {
		ticketContentsResponseDataRecord.publishedAt = publishedAt;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		ticketContentsResponseDataRecord.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		ticketContentsResponseDataRecord.updatedAt = updatedAt;
	}
	return ticketContentsResponseDataRecord;
}

Map<String, dynamic> $TicketContentsResponseDataRecordToJson(TicketContentsResponseDataRecord entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['slug'] = entity.slug;
	data['group'] = entity.group?.toJson();
	data['category'] = entity.category?.toJson();
	data['fields'] =  entity.fields?.map((v) => v.toJson()).toList();
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['viewed'] = entity.viewed;
	data['viewed_text'] = entity.viewedText;
	data['status'] = entity.status;
	data['share_url'] = entity.shareUrl;
	data['webview_url'] = entity.webviewUrl;
	data['total_user'] = entity.totalUser;
	data['published_at'] = entity.publishedAt;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

TicketContentsResponseDataRecordGroup $TicketContentsResponseDataRecordGroupFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordGroup ticketContentsResponseDataRecordGroup = TicketContentsResponseDataRecordGroup();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecordGroup.id = id;
	}
	final String? code = jsonConvert.convert<String>(json['code']);
	if (code != null) {
		ticketContentsResponseDataRecordGroup.code = code;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordGroup.name = name;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		ticketContentsResponseDataRecordGroup.description = description;
	}
	return ticketContentsResponseDataRecordGroup;
}

Map<String, dynamic> $TicketContentsResponseDataRecordGroupToJson(TicketContentsResponseDataRecordGroup entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	data['description'] = entity.description;
	return data;
}

TicketContentsResponseDataRecordCategory $TicketContentsResponseDataRecordCategoryFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordCategory ticketContentsResponseDataRecordCategory = TicketContentsResponseDataRecordCategory();
	final TicketContentsResponseDataRecordCategoryCurrent? current = jsonConvert.convert<TicketContentsResponseDataRecordCategoryCurrent>(json['current']);
	if (current != null) {
		ticketContentsResponseDataRecordCategory.current = current;
	}
	final List<TicketContentsResponseDataRecordCategoryItems>? items = jsonConvert.convertListNotNull<TicketContentsResponseDataRecordCategoryItems>(json['items']);
	if (items != null) {
		ticketContentsResponseDataRecordCategory.items = items;
	}
	return ticketContentsResponseDataRecordCategory;
}

Map<String, dynamic> $TicketContentsResponseDataRecordCategoryToJson(TicketContentsResponseDataRecordCategory entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current'] = entity.current?.toJson();
	data['items'] =  entity.items?.map((v) => v.toJson()).toList();
	return data;
}

TicketContentsResponseDataRecordCategoryCurrent $TicketContentsResponseDataRecordCategoryCurrentFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordCategoryCurrent ticketContentsResponseDataRecordCategoryCurrent = TicketContentsResponseDataRecordCategoryCurrent();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecordCategoryCurrent.id = id;
	}
	final String? code = jsonConvert.convert<String>(json['code']);
	if (code != null) {
		ticketContentsResponseDataRecordCategoryCurrent.code = code;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		ticketContentsResponseDataRecordCategoryCurrent.status = status;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		ticketContentsResponseDataRecordCategoryCurrent.position = position;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordCategoryCurrent.name = name;
	}
	final dynamic? description = jsonConvert.convert<dynamic>(json['description']);
	if (description != null) {
		ticketContentsResponseDataRecordCategoryCurrent.description = description;
	}
	final dynamic? metaTitle = jsonConvert.convert<dynamic>(json['meta_title']);
	if (metaTitle != null) {
		ticketContentsResponseDataRecordCategoryCurrent.metaTitle = metaTitle;
	}
	final dynamic? metaDescription = jsonConvert.convert<dynamic>(json['meta_description']);
	if (metaDescription != null) {
		ticketContentsResponseDataRecordCategoryCurrent.metaDescription = metaDescription;
	}
	final dynamic? metaKeyword = jsonConvert.convert<dynamic>(json['meta_keyword']);
	if (metaKeyword != null) {
		ticketContentsResponseDataRecordCategoryCurrent.metaKeyword = metaKeyword;
	}
	return ticketContentsResponseDataRecordCategoryCurrent;
}

Map<String, dynamic> $TicketContentsResponseDataRecordCategoryCurrentToJson(TicketContentsResponseDataRecordCategoryCurrent entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['status'] = entity.status;
	data['position'] = entity.position;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['meta_title'] = entity.metaTitle;
	data['meta_description'] = entity.metaDescription;
	data['meta_keyword'] = entity.metaKeyword;
	return data;
}

TicketContentsResponseDataRecordCategoryItems $TicketContentsResponseDataRecordCategoryItemsFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordCategoryItems ticketContentsResponseDataRecordCategoryItems = TicketContentsResponseDataRecordCategoryItems();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecordCategoryItems.id = id;
	}
	final String? code = jsonConvert.convert<String>(json['code']);
	if (code != null) {
		ticketContentsResponseDataRecordCategoryItems.code = code;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		ticketContentsResponseDataRecordCategoryItems.status = status;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		ticketContentsResponseDataRecordCategoryItems.position = position;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordCategoryItems.name = name;
	}
	return ticketContentsResponseDataRecordCategoryItems;
}

Map<String, dynamic> $TicketContentsResponseDataRecordCategoryItemsToJson(TicketContentsResponseDataRecordCategoryItems entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['status'] = entity.status;
	data['position'] = entity.position;
	data['name'] = entity.name;
	return data;
}

TicketContentsResponseDataRecordFields $TicketContentsResponseDataRecordFieldsFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordFields ticketContentsResponseDataRecordFields = TicketContentsResponseDataRecordFields();
	final String? label = jsonConvert.convert<String>(json['label']);
	if (label != null) {
		ticketContentsResponseDataRecordFields.label = label;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordFields.name = name;
	}
	final TicketContentsResponseDataRecordFieldsType? type = jsonConvert.convert<TicketContentsResponseDataRecordFieldsType>(json['type']);
	if (type != null) {
		ticketContentsResponseDataRecordFields.type = type;
	}
	final bool? hasLang = jsonConvert.convert<bool>(json['has_lang']);
	if (hasLang != null) {
		ticketContentsResponseDataRecordFields.hasLang = hasLang;
	}
	final bool? require = jsonConvert.convert<bool>(json['require']);
	if (require != null) {
		ticketContentsResponseDataRecordFields.require = require;
	}
	final dynamic? options = jsonConvert.convert<dynamic>(json['options']);
	if (options != null) {
		ticketContentsResponseDataRecordFields.options = options;
	}
	final String? lang = jsonConvert.convert<String>(json['lang']);
	if (lang != null) {
		ticketContentsResponseDataRecordFields.lang = lang;
	}
	return ticketContentsResponseDataRecordFields;
}

Map<String, dynamic> $TicketContentsResponseDataRecordFieldsToJson(TicketContentsResponseDataRecordFields entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['label'] = entity.label;
	data['name'] = entity.name;
	data['type'] = entity.type?.toJson();
	data['has_lang'] = entity.hasLang;
	data['require'] = entity.require;
	data['options'] = entity.options;
	data['lang'] = entity.lang;
	return data;
}

TicketContentsResponseDataRecordFieldsType $TicketContentsResponseDataRecordFieldsTypeFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordFieldsType ticketContentsResponseDataRecordFieldsType = TicketContentsResponseDataRecordFieldsType();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecordFieldsType.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordFieldsType.name = name;
	}
	final String? value = jsonConvert.convert<String>(json['value']);
	if (value != null) {
		ticketContentsResponseDataRecordFieldsType.value = value;
	}
	return ticketContentsResponseDataRecordFieldsType;
}

Map<String, dynamic> $TicketContentsResponseDataRecordFieldsTypeToJson(TicketContentsResponseDataRecordFieldsType entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['value'] = entity.value;
	return data;
}

TicketContentsResponseDataRecordImages $TicketContentsResponseDataRecordImagesFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseDataRecordImages ticketContentsResponseDataRecordImages = TicketContentsResponseDataRecordImages();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		ticketContentsResponseDataRecordImages.id = id;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		ticketContentsResponseDataRecordImages.tag = tag;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		ticketContentsResponseDataRecordImages.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		ticketContentsResponseDataRecordImages.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		ticketContentsResponseDataRecordImages.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		ticketContentsResponseDataRecordImages.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		ticketContentsResponseDataRecordImages.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		ticketContentsResponseDataRecordImages.url = url;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		ticketContentsResponseDataRecordImages.position = position;
	}
	return ticketContentsResponseDataRecordImages;
}

Map<String, dynamic> $TicketContentsResponseDataRecordImagesToJson(TicketContentsResponseDataRecordImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

TicketContentsResponseBench $TicketContentsResponseBenchFromJson(Map<String, dynamic> json) {
	final TicketContentsResponseBench ticketContentsResponseBench = TicketContentsResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		ticketContentsResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		ticketContentsResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		ticketContentsResponseBench.format = format;
	}
	return ticketContentsResponseBench;
}

Map<String, dynamic> $TicketContentsResponseBenchToJson(TicketContentsResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}