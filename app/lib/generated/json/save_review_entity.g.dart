import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_review_entity.dart';

SaveReviewEntity $SaveReviewEntityFromJson(Map<String, dynamic> json) {
  final SaveReviewEntity saveReviewEntity = SaveReviewEntity();
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    saveReviewEntity.storeId = storeId;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    saveReviewEntity.description = description;
  }
  final int? rating = jsonConvert.convert<int>(json['rating']);
  if (rating != null) {
    saveReviewEntity.rating = rating;
  }
  return saveReviewEntity;
}

Map<String, dynamic> $SaveReviewEntityToJson(SaveReviewEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['description'] = entity.description;
  data['rating'] = entity.rating;
  return data;
}
