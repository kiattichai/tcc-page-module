import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_ticket_setting_entity.dart';

GetTicketSettingEntity $GetTicketSettingEntityFromJson(
    Map<String, dynamic> json) {
  final GetTicketSettingEntity getTicketSettingEntity =
      GetTicketSettingEntity();
  final GetTicketSettingData? data =
      jsonConvert.convert<GetTicketSettingData>(json['data']);
  if (data != null) {
    getTicketSettingEntity.data = data;
  }
  final GetTicketSettingBench? bench =
      jsonConvert.convert<GetTicketSettingBench>(json['bench']);
  if (bench != null) {
    getTicketSettingEntity.bench = bench;
  }
  return getTicketSettingEntity;
}

Map<String, dynamic> $GetTicketSettingEntityToJson(
    GetTicketSettingEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetTicketSettingData $GetTicketSettingDataFromJson(Map<String, dynamic> json) {
  final GetTicketSettingData getTicketSettingData = GetTicketSettingData();
  final List<GetTicketSettingDataRecord>? record = jsonConvert
      .convertListNotNull<GetTicketSettingDataRecord>(json['record']);
  if (record != null) {
    getTicketSettingData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getTicketSettingData.cache = cache;
  }
  return getTicketSettingData;
}

Map<String, dynamic> $GetTicketSettingDataToJson(GetTicketSettingData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetTicketSettingDataRecord $GetTicketSettingDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetTicketSettingDataRecord getTicketSettingDataRecord =
      GetTicketSettingDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getTicketSettingDataRecord.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getTicketSettingDataRecord.storeId = storeId;
  }
  final int? productId = jsonConvert.convert<int>(json['product_id']);
  if (productId != null) {
    getTicketSettingDataRecord.productId = productId;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getTicketSettingDataRecord.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    getTicketSettingDataRecord.key = key;
  }
  final GetTicketSettingDataRecordValue? value =
      jsonConvert.convert<GetTicketSettingDataRecordValue>(json['value']);
  if (value != null) {
    getTicketSettingDataRecord.value = value;
  }
  final bool? serialize = jsonConvert.convert<bool>(json['serialize']);
  if (serialize != null) {
    getTicketSettingDataRecord.serialize = serialize;
  }
  return getTicketSettingDataRecord;
}

Map<String, dynamic> $GetTicketSettingDataRecordToJson(
    GetTicketSettingDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['product_id'] = entity.productId;
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value?.toJson();
  data['serialize'] = entity.serialize;
  return data;
}

GetTicketSettingDataRecordValue $GetTicketSettingDataRecordValueFromJson(
    Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValue getTicketSettingDataRecordValue =
      GetTicketSettingDataRecordValue();
  final GetTicketSettingDataRecordValueServiceFee? serviceFee = jsonConvert
      .convert<GetTicketSettingDataRecordValueServiceFee>(json['service_fee']);
  if (serviceFee != null) {
    getTicketSettingDataRecordValue.serviceFee = serviceFee;
  }
  final GetTicketSettingDataRecordValuePaymentFee? paymentFee = jsonConvert
      .convert<GetTicketSettingDataRecordValuePaymentFee>(json['payment_fee']);
  if (paymentFee != null) {
    getTicketSettingDataRecordValue.paymentFee = paymentFee;
  }
  final GetTicketSettingDataRecordValueTaxFee? taxFee = jsonConvert
      .convert<GetTicketSettingDataRecordValueTaxFee>(json['tax_fee']);
  if (taxFee != null) {
    getTicketSettingDataRecordValue.taxFee = taxFee;
  }
  final GetTicketSettingDataRecordValueCcw? ccw =
      jsonConvert.convert<GetTicketSettingDataRecordValueCcw>(json['ccw']);
  if (ccw != null) {
    getTicketSettingDataRecordValue.ccw = ccw;
  }
  final GetTicketSettingDataRecordValueIbanking? ibanking = jsonConvert
      .convert<GetTicketSettingDataRecordValueIbanking>(json['ibanking']);
  if (ibanking != null) {
    getTicketSettingDataRecordValue.ibanking = ibanking;
  }
  final GetTicketSettingDataRecordValueBanktrans? banktrans = jsonConvert
      .convert<GetTicketSettingDataRecordValueBanktrans>(json['banktrans']);
  if (banktrans != null) {
    getTicketSettingDataRecordValue.banktrans = banktrans;
  }
  final GetTicketSettingDataRecordValueBill? bill =
      jsonConvert.convert<GetTicketSettingDataRecordValueBill>(json['bill']);
  if (bill != null) {
    getTicketSettingDataRecordValue.bill = bill;
  }
  final GetTicketSettingDataRecordValuePromptpay? promptpay = jsonConvert
      .convert<GetTicketSettingDataRecordValuePromptpay>(json['promptpay']);
  if (promptpay != null) {
    getTicketSettingDataRecordValue.promptpay = promptpay;
  }
  final GetTicketSettingDataRecordValueInstallment? installment = jsonConvert
      .convert<GetTicketSettingDataRecordValueInstallment>(json['installment']);
  if (installment != null) {
    getTicketSettingDataRecordValue.installment = installment;
  }
  final GetTicketSettingDataRecordValueLinepay? linepay = jsonConvert
      .convert<GetTicketSettingDataRecordValueLinepay>(json['linepay']);
  if (linepay != null) {
    getTicketSettingDataRecordValue.linepay = linepay;
  }
  final GetTicketSettingDataRecordValueFree? free =
      jsonConvert.convert<GetTicketSettingDataRecordValueFree>(json['free']);
  if (free != null) {
    getTicketSettingDataRecordValue.free = free;
  }
  final GetTicketSettingDataRecordValuePaid? paid =
      jsonConvert.convert<GetTicketSettingDataRecordValuePaid>(json['paid']);
  if (paid != null) {
    getTicketSettingDataRecordValue.paid = paid;
  }
  final String? ga = jsonConvert.convert<String>(json['ga']);
  if (ga != null) {
    getTicketSettingDataRecordValue.ga = ga;
  }
  final String? fbPixel = jsonConvert.convert<String>(json['fb_pixel']);
  if (fbPixel != null) {
    getTicketSettingDataRecordValue.fbPixel = fbPixel;
  }
  return getTicketSettingDataRecordValue;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueToJson(
    GetTicketSettingDataRecordValue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['service_fee'] = entity.serviceFee?.toJson();
  data['payment_fee'] = entity.paymentFee?.toJson();
  data['tax_fee'] = entity.taxFee?.toJson();
  data['ccw'] = entity.ccw?.toJson();
  data['ibanking'] = entity.ibanking?.toJson();
  data['banktrans'] = entity.banktrans?.toJson();
  data['bill'] = entity.bill?.toJson();
  data['promptpay'] = entity.promptpay?.toJson();
  data['installment'] = entity.installment?.toJson();
  data['linepay'] = entity.linepay?.toJson();
  data['free'] = entity.free?.toJson();
  data['paid'] = entity.paid?.toJson();
  data['ga'] = entity.ga;
  data['fb_pixel'] = entity.fbPixel;
  return data;
}

GetTicketSettingDataRecordValueServiceFee
    $GetTicketSettingDataRecordValueServiceFeeFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueServiceFee
      getTicketSettingDataRecordValueServiceFee =
      GetTicketSettingDataRecordValueServiceFee();
  final int? organizerPay = jsonConvert.convert<int>(json['organizer_pay']);
  if (organizerPay != null) {
    getTicketSettingDataRecordValueServiceFee.organizerPay = organizerPay;
  }
  final int? customerPay = jsonConvert.convert<int>(json['customer_pay']);
  if (customerPay != null) {
    getTicketSettingDataRecordValueServiceFee.customerPay = customerPay;
  }
  return getTicketSettingDataRecordValueServiceFee;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueServiceFeeToJson(
    GetTicketSettingDataRecordValueServiceFee entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['organizer_pay'] = entity.organizerPay;
  data['customer_pay'] = entity.customerPay;
  return data;
}

GetTicketSettingDataRecordValuePaymentFee
    $GetTicketSettingDataRecordValuePaymentFeeFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValuePaymentFee
      getTicketSettingDataRecordValuePaymentFee =
      GetTicketSettingDataRecordValuePaymentFee();
  final int? organizerPay = jsonConvert.convert<int>(json['organizer_pay']);
  if (organizerPay != null) {
    getTicketSettingDataRecordValuePaymentFee.organizerPay = organizerPay;
  }
  final int? customerPay = jsonConvert.convert<int>(json['customer_pay']);
  if (customerPay != null) {
    getTicketSettingDataRecordValuePaymentFee.customerPay = customerPay;
  }
  return getTicketSettingDataRecordValuePaymentFee;
}

Map<String, dynamic> $GetTicketSettingDataRecordValuePaymentFeeToJson(
    GetTicketSettingDataRecordValuePaymentFee entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['organizer_pay'] = entity.organizerPay;
  data['customer_pay'] = entity.customerPay;
  return data;
}

GetTicketSettingDataRecordValueTaxFee
    $GetTicketSettingDataRecordValueTaxFeeFromJson(Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueTaxFee
      getTicketSettingDataRecordValueTaxFee =
      GetTicketSettingDataRecordValueTaxFee();
  final bool? organizerPay = jsonConvert.convert<bool>(json['organizer_pay']);
  if (organizerPay != null) {
    getTicketSettingDataRecordValueTaxFee.organizerPay = organizerPay;
  }
  final bool? customerPay = jsonConvert.convert<bool>(json['customer_pay']);
  if (customerPay != null) {
    getTicketSettingDataRecordValueTaxFee.customerPay = customerPay;
  }
  return getTicketSettingDataRecordValueTaxFee;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueTaxFeeToJson(
    GetTicketSettingDataRecordValueTaxFee entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['organizer_pay'] = entity.organizerPay;
  data['customer_pay'] = entity.customerPay;
  return data;
}

GetTicketSettingDataRecordValueCcw $GetTicketSettingDataRecordValueCcwFromJson(
    Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueCcw getTicketSettingDataRecordValueCcw =
      GetTicketSettingDataRecordValueCcw();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueCcw.status = status;
  }
  return getTicketSettingDataRecordValueCcw;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueCcwToJson(
    GetTicketSettingDataRecordValueCcw entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValueIbanking
    $GetTicketSettingDataRecordValueIbankingFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueIbanking
      getTicketSettingDataRecordValueIbanking =
      GetTicketSettingDataRecordValueIbanking();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueIbanking.status = status;
  }
  return getTicketSettingDataRecordValueIbanking;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueIbankingToJson(
    GetTicketSettingDataRecordValueIbanking entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValueBanktrans
    $GetTicketSettingDataRecordValueBanktransFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueBanktrans
      getTicketSettingDataRecordValueBanktrans =
      GetTicketSettingDataRecordValueBanktrans();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueBanktrans.status = status;
  }
  final int? expired = jsonConvert.convert<int>(json['expired']);
  if (expired != null) {
    getTicketSettingDataRecordValueBanktrans.expired = expired;
  }
  return getTicketSettingDataRecordValueBanktrans;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueBanktransToJson(
    GetTicketSettingDataRecordValueBanktrans entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  data['expired'] = entity.expired;
  return data;
}

GetTicketSettingDataRecordValueBill
    $GetTicketSettingDataRecordValueBillFromJson(Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueBill
      getTicketSettingDataRecordValueBill =
      GetTicketSettingDataRecordValueBill();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueBill.status = status;
  }
  return getTicketSettingDataRecordValueBill;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueBillToJson(
    GetTicketSettingDataRecordValueBill entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValuePromptpay
    $GetTicketSettingDataRecordValuePromptpayFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValuePromptpay
      getTicketSettingDataRecordValuePromptpay =
      GetTicketSettingDataRecordValuePromptpay();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValuePromptpay.status = status;
  }
  return getTicketSettingDataRecordValuePromptpay;
}

Map<String, dynamic> $GetTicketSettingDataRecordValuePromptpayToJson(
    GetTicketSettingDataRecordValuePromptpay entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValueInstallment
    $GetTicketSettingDataRecordValueInstallmentFromJson(
        Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueInstallment
      getTicketSettingDataRecordValueInstallment =
      GetTicketSettingDataRecordValueInstallment();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueInstallment.status = status;
  }
  return getTicketSettingDataRecordValueInstallment;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueInstallmentToJson(
    GetTicketSettingDataRecordValueInstallment entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValueLinepay
    $GetTicketSettingDataRecordValueLinepayFromJson(Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueLinepay
      getTicketSettingDataRecordValueLinepay =
      GetTicketSettingDataRecordValueLinepay();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getTicketSettingDataRecordValueLinepay.status = status;
  }
  return getTicketSettingDataRecordValueLinepay;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueLinepayToJson(
    GetTicketSettingDataRecordValueLinepay entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  return data;
}

GetTicketSettingDataRecordValueFree
    $GetTicketSettingDataRecordValueFreeFromJson(Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValueFree
      getTicketSettingDataRecordValueFree =
      GetTicketSettingDataRecordValueFree();
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getTicketSettingDataRecordValueFree.total = total;
  }
  final int? paymentFee = jsonConvert.convert<int>(json['payment_fee']);
  if (paymentFee != null) {
    getTicketSettingDataRecordValueFree.paymentFee = paymentFee;
  }
  final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
  if (serviceFee != null) {
    getTicketSettingDataRecordValueFree.serviceFee = serviceFee;
  }
  return getTicketSettingDataRecordValueFree;
}

Map<String, dynamic> $GetTicketSettingDataRecordValueFreeToJson(
    GetTicketSettingDataRecordValueFree entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['total'] = entity.total;
  data['payment_fee'] = entity.paymentFee;
  data['service_fee'] = entity.serviceFee;
  return data;
}

GetTicketSettingDataRecordValuePaid
    $GetTicketSettingDataRecordValuePaidFromJson(Map<String, dynamic> json) {
  final GetTicketSettingDataRecordValuePaid
      getTicketSettingDataRecordValuePaid =
      GetTicketSettingDataRecordValuePaid();
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getTicketSettingDataRecordValuePaid.total = total;
  }
  final int? paymentFee = jsonConvert.convert<int>(json['payment_fee']);
  if (paymentFee != null) {
    getTicketSettingDataRecordValuePaid.paymentFee = paymentFee;
  }
  final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
  if (serviceFee != null) {
    getTicketSettingDataRecordValuePaid.serviceFee = serviceFee;
  }
  return getTicketSettingDataRecordValuePaid;
}

Map<String, dynamic> $GetTicketSettingDataRecordValuePaidToJson(
    GetTicketSettingDataRecordValuePaid entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['total'] = entity.total;
  data['payment_fee'] = entity.paymentFee;
  data['service_fee'] = entity.serviceFee;
  return data;
}

GetTicketSettingBench $GetTicketSettingBenchFromJson(
    Map<String, dynamic> json) {
  final GetTicketSettingBench getTicketSettingBench = GetTicketSettingBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getTicketSettingBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getTicketSettingBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getTicketSettingBench.format = format;
  }
  return getTicketSettingBench;
}

Map<String, dynamic> $GetTicketSettingBenchToJson(
    GetTicketSettingBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
