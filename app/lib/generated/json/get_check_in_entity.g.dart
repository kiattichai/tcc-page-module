import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_check_in_entity.dart';

GetCheckInEntity $GetCheckInEntityFromJson(Map<String, dynamic> json) {
  final GetCheckInEntity getCheckInEntity = GetCheckInEntity();
  final GetCheckInData? data =
      jsonConvert.convert<GetCheckInData>(json['data']);
  if (data != null) {
    getCheckInEntity.data = data;
  }
  final GetCheckInBench? bench =
      jsonConvert.convert<GetCheckInBench>(json['bench']);
  if (bench != null) {
    getCheckInEntity.bench = bench;
  }
  return getCheckInEntity;
}

Map<String, dynamic> $GetCheckInEntityToJson(GetCheckInEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetCheckInData $GetCheckInDataFromJson(Map<String, dynamic> json) {
  final GetCheckInData getCheckInData = GetCheckInData();
  final GetCheckInDataPagination? pagination =
      jsonConvert.convert<GetCheckInDataPagination>(json['pagination']);
  if (pagination != null) {
    getCheckInData.pagination = pagination;
  }
  final List<GetCheckInDataRecord>? record =
      jsonConvert.convertListNotNull<GetCheckInDataRecord>(json['record']);
  if (record != null) {
    getCheckInData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getCheckInData.cache = cache;
  }
  return getCheckInData;
}

Map<String, dynamic> $GetCheckInDataToJson(GetCheckInData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetCheckInDataPagination $GetCheckInDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetCheckInDataPagination getCheckInDataPagination =
      GetCheckInDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getCheckInDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getCheckInDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getCheckInDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getCheckInDataPagination.total = total;
  }
  return getCheckInDataPagination;
}

Map<String, dynamic> $GetCheckInDataPaginationToJson(
    GetCheckInDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetCheckInDataRecord $GetCheckInDataRecordFromJson(Map<String, dynamic> json) {
  final GetCheckInDataRecord getCheckInDataRecord = GetCheckInDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCheckInDataRecord.id = id;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getCheckInDataRecord.description = description;
  }
  final GetCheckInDataRecordUser? user =
      jsonConvert.convert<GetCheckInDataRecordUser>(json['user']);
  if (user != null) {
    getCheckInDataRecord.user = user;
  }
  final GetCheckInDataRecordCreatedAt? createdAt =
      jsonConvert.convert<GetCheckInDataRecordCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getCheckInDataRecord.createdAt = createdAt;
  }
  final GetCheckInDataRecordUpdatedAt? updatedAt =
      jsonConvert.convert<GetCheckInDataRecordUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getCheckInDataRecord.updatedAt = updatedAt;
  }
  return getCheckInDataRecord;
}

Map<String, dynamic> $GetCheckInDataRecordToJson(GetCheckInDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['description'] = entity.description;
  data['user'] = entity.user?.toJson();
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

GetCheckInDataRecordUser $GetCheckInDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetCheckInDataRecordUser getCheckInDataRecordUser =
      GetCheckInDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCheckInDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getCheckInDataRecordUser.username = username;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getCheckInDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getCheckInDataRecordUser.lastName = lastName;
  }
  final GetCheckInDataRecordUserImage? image =
      jsonConvert.convert<GetCheckInDataRecordUserImage>(json['image']);
  if (image != null) {
    getCheckInDataRecordUser.image = image;
  }
  return getCheckInDataRecordUser;
}

Map<String, dynamic> $GetCheckInDataRecordUserToJson(
    GetCheckInDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['image'] = entity.image?.toJson();
  return data;
}

GetCheckInDataRecordUserImage $GetCheckInDataRecordUserImageFromJson(
    Map<String, dynamic> json) {
  final GetCheckInDataRecordUserImage getCheckInDataRecordUserImage =
      GetCheckInDataRecordUserImage();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getCheckInDataRecordUserImage.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getCheckInDataRecordUserImage.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getCheckInDataRecordUserImage.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getCheckInDataRecordUserImage.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getCheckInDataRecordUserImage.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getCheckInDataRecordUserImage.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getCheckInDataRecordUserImage.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getCheckInDataRecordUserImage.resizeUrl = resizeUrl;
  }
  return getCheckInDataRecordUserImage;
}

Map<String, dynamic> $GetCheckInDataRecordUserImageToJson(
    GetCheckInDataRecordUserImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetCheckInDataRecordCreatedAt $GetCheckInDataRecordCreatedAtFromJson(
    Map<String, dynamic> json) {
  final GetCheckInDataRecordCreatedAt getCheckInDataRecordCreatedAt =
      GetCheckInDataRecordCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getCheckInDataRecordCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getCheckInDataRecordCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getCheckInDataRecordCreatedAt.time = time;
  }
  return getCheckInDataRecordCreatedAt;
}

Map<String, dynamic> $GetCheckInDataRecordCreatedAtToJson(
    GetCheckInDataRecordCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetCheckInDataRecordUpdatedAt $GetCheckInDataRecordUpdatedAtFromJson(
    Map<String, dynamic> json) {
  final GetCheckInDataRecordUpdatedAt getCheckInDataRecordUpdatedAt =
      GetCheckInDataRecordUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getCheckInDataRecordUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getCheckInDataRecordUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getCheckInDataRecordUpdatedAt.time = time;
  }
  return getCheckInDataRecordUpdatedAt;
}

Map<String, dynamic> $GetCheckInDataRecordUpdatedAtToJson(
    GetCheckInDataRecordUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetCheckInBench $GetCheckInBenchFromJson(Map<String, dynamic> json) {
  final GetCheckInBench getCheckInBench = GetCheckInBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getCheckInBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getCheckInBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getCheckInBench.format = format;
  }
  return getCheckInBench;
}

Map<String, dynamic> $GetCheckInBenchToJson(GetCheckInBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
