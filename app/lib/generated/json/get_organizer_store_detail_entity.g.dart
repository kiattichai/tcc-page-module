import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_organizer_store_detail_entity.dart';

GetOrganizerStoreDetailEntity $GetOrganizerStoreDetailEntityFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailEntity getOrganizerStoreDetailEntity =
      GetOrganizerStoreDetailEntity();
  final GetOrganizerStoreDetailData? data =
      jsonConvert.convert<GetOrganizerStoreDetailData>(json['data']);
  if (data != null) {
    getOrganizerStoreDetailEntity.data = data;
  }
  final GetOrganizerStoreDetailBench? bench =
      jsonConvert.convert<GetOrganizerStoreDetailBench>(json['bench']);
  if (bench != null) {
    getOrganizerStoreDetailEntity.bench = bench;
  }
  return getOrganizerStoreDetailEntity;
}

Map<String, dynamic> $GetOrganizerStoreDetailEntityToJson(
    GetOrganizerStoreDetailEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetOrganizerStoreDetailData $GetOrganizerStoreDetailDataFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailData getOrganizerStoreDetailData =
      GetOrganizerStoreDetailData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailData.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailData.name = name;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    getOrganizerStoreDetailData.slug = slug;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getOrganizerStoreDetailData.description = description;
  }
  final GetOrganizerStoreDetailDataType? type =
      jsonConvert.convert<GetOrganizerStoreDetailDataType>(json['type']);
  if (type != null) {
    getOrganizerStoreDetailData.type = type;
  }
  final GetOrganizerStoreDetailDataSection? section =
      jsonConvert.convert<GetOrganizerStoreDetailDataSection>(json['section']);
  if (section != null) {
    getOrganizerStoreDetailData.section = section;
  }
  final GetOrganizerStoreDetailDataContact? contact =
      jsonConvert.convert<GetOrganizerStoreDetailDataContact>(json['contact']);
  if (contact != null) {
    getOrganizerStoreDetailData.contact = contact;
  }
  final GetOrganizerStoreDetailDataCorporate? corporate = jsonConvert
      .convert<GetOrganizerStoreDetailDataCorporate>(json['corporate']);
  if (corporate != null) {
    getOrganizerStoreDetailData.corporate = corporate;
  }
  final GetOrganizerStoreDetailDataBilling? billing =
      jsonConvert.convert<GetOrganizerStoreDetailDataBilling>(json['billing']);
  if (billing != null) {
    getOrganizerStoreDetailData.billing = billing;
  }
  final bool? acceptTerms = jsonConvert.convert<bool>(json['accept_terms']);
  if (acceptTerms != null) {
    getOrganizerStoreDetailData.acceptTerms = acceptTerms;
  }
  final GetOrganizerStoreDetailDataDocumentStatus? documentStatus =
      jsonConvert.convert<GetOrganizerStoreDetailDataDocumentStatus>(
          json['document_status']);
  if (documentStatus != null) {
    getOrganizerStoreDetailData.documentStatus = documentStatus;
  }
  final GetOrganizerStoreDetailDataPaymentStatus? paymentStatus =
      jsonConvert.convert<GetOrganizerStoreDetailDataPaymentStatus>(
          json['payment_status']);
  if (paymentStatus != null) {
    getOrganizerStoreDetailData.paymentStatus = paymentStatus;
  }
  final GetOrganizerStoreDetailDataPublishStatus? publishStatus =
      jsonConvert.convert<GetOrganizerStoreDetailDataPublishStatus>(
          json['publish_status']);
  if (publishStatus != null) {
    getOrganizerStoreDetailData.publishStatus = publishStatus;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getOrganizerStoreDetailData.status = status;
  }
  final GetOrganizerStoreDetailDataCreatedAt? createdAt = jsonConvert
      .convert<GetOrganizerStoreDetailDataCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getOrganizerStoreDetailData.createdAt = createdAt;
  }
  final GetOrganizerStoreDetailDataUpdatedAt? updatedAt = jsonConvert
      .convert<GetOrganizerStoreDetailDataUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getOrganizerStoreDetailData.updatedAt = updatedAt;
  }
  final GetOrganizerStoreDetailDataPublishedAt? publishedAt = jsonConvert
      .convert<GetOrganizerStoreDetailDataPublishedAt>(json['published_at']);
  if (publishedAt != null) {
    getOrganizerStoreDetailData.publishedAt = publishedAt;
  }
  final List<GetOrganizerStoreDetailDataSettings>? settings =
      jsonConvert.convertListNotNull<GetOrganizerStoreDetailDataSettings>(
          json['settings']);
  if (settings != null) {
    getOrganizerStoreDetailData.settings = settings;
  }
  final GetOrganizerStoreDetailDataImages? images =
      jsonConvert.convert<GetOrganizerStoreDetailDataImages>(json['images']);
  if (images != null) {
    getOrganizerStoreDetailData.images = images;
  }
  final List<dynamic>? connects =
      jsonConvert.convertListNotNull<dynamic>(json['connects']);
  if (connects != null) {
    getOrganizerStoreDetailData.connects = connects;
  }
  final GetOrganizerStoreDetailDataStaff? staff =
      jsonConvert.convert<GetOrganizerStoreDetailDataStaff>(json['staff']);
  if (staff != null) {
    getOrganizerStoreDetailData.staff = staff;
  }
  final GetOrganizerStoreDetailDataVenue? venue =
      jsonConvert.convert<GetOrganizerStoreDetailDataVenue>(json['venue']);
  if (venue != null) {
    getOrganizerStoreDetailData.venue = venue;
  }
  final List<GetOrganizerStoreDetailDataAttributes>? attributes =
      jsonConvert.convertListNotNull<GetOrganizerStoreDetailDataAttributes>(
          json['attributes']);
  if (attributes != null) {
    getOrganizerStoreDetailData.attributes = attributes;
  }
  final List<GetOrganizerStoreDetailDataTimes>? times = jsonConvert
      .convertListNotNull<GetOrganizerStoreDetailDataTimes>(json['times']);
  if (times != null) {
    getOrganizerStoreDetailData.times = times;
  }
  final List<dynamic>? timesDisplay =
      jsonConvert.convertListNotNull<dynamic>(json['times_display']);
  if (timesDisplay != null) {
    getOrganizerStoreDetailData.timesDisplay = timesDisplay;
  }
  final GetOrganizerStoreDetailDataTimeOpen? timeOpen = jsonConvert
      .convert<GetOrganizerStoreDetailDataTimeOpen>(json['time_open']);
  if (timeOpen != null) {
    getOrganizerStoreDetailData.timeOpen = timeOpen;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getOrganizerStoreDetailData.remark = remark;
  }
  final bool? updated = jsonConvert.convert<bool>(json['updated']);
  if (updated != null) {
    getOrganizerStoreDetailData.updated = updated;
  }
  final bool? acceptFee = jsonConvert.convert<bool>(json['accept_fee']);
  if (acceptFee != null) {
    getOrganizerStoreDetailData.acceptFee = acceptFee;
  }
  final GetOrganizerStoreDetailDataAcceptFeeAt? acceptFeeAt = jsonConvert
      .convert<GetOrganizerStoreDetailDataAcceptFeeAt>(json['accept_fee_at']);
  if (acceptFeeAt != null) {
    getOrganizerStoreDetailData.acceptFeeAt = acceptFeeAt;
  }
  final int? averageRating = jsonConvert.convert<int>(json['average_rating']);
  if (averageRating != null) {
    getOrganizerStoreDetailData.averageRating = averageRating;
  }
  final int? reviewTotal = jsonConvert.convert<int>(json['review_total']);
  if (reviewTotal != null) {
    getOrganizerStoreDetailData.reviewTotal = reviewTotal;
  }
  final bool? covidVerify = jsonConvert.convert<bool>(json['covid_verify']);
  if (covidVerify != null) {
    getOrganizerStoreDetailData.covidVerify = covidVerify;
  }
  final GetOrganizerStoreDetailDataParking? parking =
      jsonConvert.convert<GetOrganizerStoreDetailDataParking>(json['parking']);
  if (parking != null) {
    getOrganizerStoreDetailData.parking = parking;
  }
  final GetOrganizerStoreDetailDataSeats? seats =
      jsonConvert.convert<GetOrganizerStoreDetailDataSeats>(json['seats']);
  if (seats != null) {
    getOrganizerStoreDetailData.seats = seats;
  }
  final GetOrganizerStoreDetailDataPage? page =
      jsonConvert.convert<GetOrganizerStoreDetailDataPage>(json['page']);
  if (page != null) {
    getOrganizerStoreDetailData.page = page;
  }
  final bool? isShaPlus = jsonConvert.convert<bool>(json['is_sha_plus']);
  if (isShaPlus != null) {
    getOrganizerStoreDetailData.isShaPlus = isShaPlus;
  }
  final bool? isCovidFree = jsonConvert.convert<bool>(json['is_covid_free']);
  if (isCovidFree != null) {
    getOrganizerStoreDetailData.isCovidFree = isCovidFree;
  }
  return getOrganizerStoreDetailData;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataToJson(
    GetOrganizerStoreDetailData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['slug'] = entity.slug;
  data['description'] = entity.description;
  data['type'] = entity.type?.toJson();
  data['section'] = entity.section?.toJson();
  data['contact'] = entity.contact?.toJson();
  data['corporate'] = entity.corporate?.toJson();
  data['billing'] = entity.billing?.toJson();
  data['accept_terms'] = entity.acceptTerms;
  data['document_status'] = entity.documentStatus?.toJson();
  data['payment_status'] = entity.paymentStatus?.toJson();
  data['publish_status'] = entity.publishStatus?.toJson();
  data['status'] = entity.status;
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  data['published_at'] = entity.publishedAt?.toJson();
  data['settings'] = entity.settings?.map((v) => v.toJson()).toList();
  data['images'] = entity.images?.toJson();
  data['connects'] = entity.connects;
  data['staff'] = entity.staff?.toJson();
  data['venue'] = entity.venue?.toJson();
  data['attributes'] = entity.attributes?.map((v) => v.toJson()).toList();
  data['times'] = entity.times?.map((v) => v.toJson()).toList();
  data['times_display'] = entity.timesDisplay;
  data['time_open'] = entity.timeOpen?.toJson();
  data['remark'] = entity.remark;
  data['updated'] = entity.updated;
  data['accept_fee'] = entity.acceptFee;
  data['accept_fee_at'] = entity.acceptFeeAt?.toJson();
  data['average_rating'] = entity.averageRating;
  data['review_total'] = entity.reviewTotal;
  data['covid_verify'] = entity.covidVerify;
  data['parking'] = entity.parking?.toJson();
  data['seats'] = entity.seats?.toJson();
  data['page'] = entity.page?.toJson();
  data['is_sha_plus'] = entity.isShaPlus;
  data['is_covid_free'] = entity.isCovidFree;
  return data;
}

GetOrganizerStoreDetailDataType $GetOrganizerStoreDetailDataTypeFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataType getOrganizerStoreDetailDataType =
      GetOrganizerStoreDetailDataType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataType.text = text;
  }
  return getOrganizerStoreDetailDataType;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataTypeToJson(
    GetOrganizerStoreDetailDataType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataSection $GetOrganizerStoreDetailDataSectionFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataSection getOrganizerStoreDetailDataSection =
      GetOrganizerStoreDetailDataSection();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataSection.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataSection.text = text;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getOrganizerStoreDetailDataSection.textFull = textFull;
  }
  return getOrganizerStoreDetailDataSection;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataSectionToJson(
    GetOrganizerStoreDetailDataSection entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  data['text_full'] = entity.textFull;
  return data;
}

GetOrganizerStoreDetailDataContact $GetOrganizerStoreDetailDataContactFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataContact getOrganizerStoreDetailDataContact =
      GetOrganizerStoreDetailDataContact();
  final dynamic? firstName = jsonConvert.convert<dynamic>(json['first_name']);
  if (firstName != null) {
    getOrganizerStoreDetailDataContact.firstName = firstName;
  }
  final dynamic? lastName = jsonConvert.convert<dynamic>(json['last_name']);
  if (lastName != null) {
    getOrganizerStoreDetailDataContact.lastName = lastName;
  }
  final dynamic? citizenId = jsonConvert.convert<dynamic>(json['citizen_id']);
  if (citizenId != null) {
    getOrganizerStoreDetailDataContact.citizenId = citizenId;
  }
  final String? phone = jsonConvert.convert<String>(json['phone']);
  if (phone != null) {
    getOrganizerStoreDetailDataContact.phone = phone;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getOrganizerStoreDetailDataContact.countryCode = countryCode;
  }
  final List<dynamic>? phoneOption =
      jsonConvert.convertListNotNull<dynamic>(json['phone_option']);
  if (phoneOption != null) {
    getOrganizerStoreDetailDataContact.phoneOption = phoneOption;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getOrganizerStoreDetailDataContact.email = email;
  }
  return getOrganizerStoreDetailDataContact;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataContactToJson(
    GetOrganizerStoreDetailDataContact entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['citizen_id'] = entity.citizenId;
  data['phone'] = entity.phone;
  data['country_code'] = entity.countryCode;
  data['phone_option'] = entity.phoneOption;
  data['email'] = entity.email;
  return data;
}

GetOrganizerStoreDetailDataCorporate
    $GetOrganizerStoreDetailDataCorporateFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataCorporate
      getOrganizerStoreDetailDataCorporate =
      GetOrganizerStoreDetailDataCorporate();
  final dynamic? name = jsonConvert.convert<dynamic>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataCorporate.name = name;
  }
  final dynamic? branch = jsonConvert.convert<dynamic>(json['branch']);
  if (branch != null) {
    getOrganizerStoreDetailDataCorporate.branch = branch;
  }
  final dynamic? phone = jsonConvert.convert<dynamic>(json['phone']);
  if (phone != null) {
    getOrganizerStoreDetailDataCorporate.phone = phone;
  }
  final dynamic? taxId = jsonConvert.convert<dynamic>(json['tax_id']);
  if (taxId != null) {
    getOrganizerStoreDetailDataCorporate.taxId = taxId;
  }
  return getOrganizerStoreDetailDataCorporate;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataCorporateToJson(
    GetOrganizerStoreDetailDataCorporate entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['branch'] = entity.branch;
  data['phone'] = entity.phone;
  data['tax_id'] = entity.taxId;
  return data;
}

GetOrganizerStoreDetailDataBilling $GetOrganizerStoreDetailDataBillingFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataBilling getOrganizerStoreDetailDataBilling =
      GetOrganizerStoreDetailDataBilling();
  final dynamic? address = jsonConvert.convert<dynamic>(json['address']);
  if (address != null) {
    getOrganizerStoreDetailDataBilling.address = address;
  }
  final dynamic? district = jsonConvert.convert<dynamic>(json['district']);
  if (district != null) {
    getOrganizerStoreDetailDataBilling.district = district;
  }
  final dynamic? city = jsonConvert.convert<dynamic>(json['city']);
  if (city != null) {
    getOrganizerStoreDetailDataBilling.city = city;
  }
  final dynamic? province = jsonConvert.convert<dynamic>(json['province']);
  if (province != null) {
    getOrganizerStoreDetailDataBilling.province = province;
  }
  final dynamic? country = jsonConvert.convert<dynamic>(json['country']);
  if (country != null) {
    getOrganizerStoreDetailDataBilling.country = country;
  }
  final dynamic? zipcode = jsonConvert.convert<dynamic>(json['zipcode']);
  if (zipcode != null) {
    getOrganizerStoreDetailDataBilling.zipcode = zipcode;
  }
  return getOrganizerStoreDetailDataBilling;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataBillingToJson(
    GetOrganizerStoreDetailDataBilling entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['address'] = entity.address;
  data['district'] = entity.district;
  data['city'] = entity.city;
  data['province'] = entity.province;
  data['country'] = entity.country;
  data['zipcode'] = entity.zipcode;
  return data;
}

GetOrganizerStoreDetailDataDocumentStatus
    $GetOrganizerStoreDetailDataDocumentStatusFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataDocumentStatus
      getOrganizerStoreDetailDataDocumentStatus =
      GetOrganizerStoreDetailDataDocumentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataDocumentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataDocumentStatus.text = text;
  }
  return getOrganizerStoreDetailDataDocumentStatus;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataDocumentStatusToJson(
    GetOrganizerStoreDetailDataDocumentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataPaymentStatus
    $GetOrganizerStoreDetailDataPaymentStatusFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataPaymentStatus
      getOrganizerStoreDetailDataPaymentStatus =
      GetOrganizerStoreDetailDataPaymentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataPaymentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataPaymentStatus.text = text;
  }
  return getOrganizerStoreDetailDataPaymentStatus;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataPaymentStatusToJson(
    GetOrganizerStoreDetailDataPaymentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataPublishStatus
    $GetOrganizerStoreDetailDataPublishStatusFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataPublishStatus
      getOrganizerStoreDetailDataPublishStatus =
      GetOrganizerStoreDetailDataPublishStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataPublishStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataPublishStatus.text = text;
  }
  return getOrganizerStoreDetailDataPublishStatus;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataPublishStatusToJson(
    GetOrganizerStoreDetailDataPublishStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataCreatedAt
    $GetOrganizerStoreDetailDataCreatedAtFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataCreatedAt
      getOrganizerStoreDetailDataCreatedAt =
      GetOrganizerStoreDetailDataCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getOrganizerStoreDetailDataCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getOrganizerStoreDetailDataCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getOrganizerStoreDetailDataCreatedAt.time = time;
  }
  return getOrganizerStoreDetailDataCreatedAt;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataCreatedAtToJson(
    GetOrganizerStoreDetailDataCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetOrganizerStoreDetailDataUpdatedAt
    $GetOrganizerStoreDetailDataUpdatedAtFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataUpdatedAt
      getOrganizerStoreDetailDataUpdatedAt =
      GetOrganizerStoreDetailDataUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getOrganizerStoreDetailDataUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getOrganizerStoreDetailDataUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getOrganizerStoreDetailDataUpdatedAt.time = time;
  }
  final String? valueUtc = jsonConvert.convert<String>(json['value_utc']);
  if (valueUtc != null) {
    getOrganizerStoreDetailDataUpdatedAt.valueUtc = valueUtc;
  }
  return getOrganizerStoreDetailDataUpdatedAt;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataUpdatedAtToJson(
    GetOrganizerStoreDetailDataUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  data['value_utc'] = entity.valueUtc;
  return data;
}

GetOrganizerStoreDetailDataPublishedAt
    $GetOrganizerStoreDetailDataPublishedAtFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataPublishedAt
      getOrganizerStoreDetailDataPublishedAt =
      GetOrganizerStoreDetailDataPublishedAt();
  return getOrganizerStoreDetailDataPublishedAt;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataPublishedAtToJson(
    GetOrganizerStoreDetailDataPublishedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetOrganizerStoreDetailDataSettings
    $GetOrganizerStoreDetailDataSettingsFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataSettings
      getOrganizerStoreDetailDataSettings =
      GetOrganizerStoreDetailDataSettings();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataSettings.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getOrganizerStoreDetailDataSettings.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    getOrganizerStoreDetailDataSettings.key = key;
  }
  final GetOrganizerStoreDetailDataSettingsValue? value = jsonConvert
      .convert<GetOrganizerStoreDetailDataSettingsValue>(json['value']);
  if (value != null) {
    getOrganizerStoreDetailDataSettings.value = value;
  }
  final bool? serialize = jsonConvert.convert<bool>(json['serialize']);
  if (serialize != null) {
    getOrganizerStoreDetailDataSettings.serialize = serialize;
  }
  return getOrganizerStoreDetailDataSettings;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataSettingsToJson(
    GetOrganizerStoreDetailDataSettings entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value?.toJson();
  data['serialize'] = entity.serialize;
  return data;
}

GetOrganizerStoreDetailDataSettingsValue
    $GetOrganizerStoreDetailDataSettingsValueFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataSettingsValue
      getOrganizerStoreDetailDataSettingsValue =
      GetOrganizerStoreDetailDataSettingsValue();
  final String? website = jsonConvert.convert<String>(json['website']);
  if (website != null) {
    getOrganizerStoreDetailDataSettingsValue.website = website;
  }
  final String? facebook = jsonConvert.convert<String>(json['facebook']);
  if (facebook != null) {
    getOrganizerStoreDetailDataSettingsValue.facebook = facebook;
  }
  final String? twitter = jsonConvert.convert<String>(json['twitter']);
  if (twitter != null) {
    getOrganizerStoreDetailDataSettingsValue.twitter = twitter;
  }
  final String? line = jsonConvert.convert<String>(json['line']);
  if (line != null) {
    getOrganizerStoreDetailDataSettingsValue.line = line;
  }
  final String? instagram = jsonConvert.convert<String>(json['instagram']);
  if (instagram != null) {
    getOrganizerStoreDetailDataSettingsValue.instagram = instagram;
  }
  return getOrganizerStoreDetailDataSettingsValue;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataSettingsValueToJson(
    GetOrganizerStoreDetailDataSettingsValue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['website'] = entity.website;
  data['facebook'] = entity.facebook;
  data['twitter'] = entity.twitter;
  data['line'] = entity.line;
  data['instagram'] = entity.instagram;
  return data;
}

GetOrganizerStoreDetailDataImages $GetOrganizerStoreDetailDataImagesFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataImages getOrganizerStoreDetailDataImages =
      GetOrganizerStoreDetailDataImages();
  final GetOrganizerStoreDetailDataImagesLogo? logo =
      jsonConvert.convert<GetOrganizerStoreDetailDataImagesLogo>(json['logo']);
  if (logo != null) {
    getOrganizerStoreDetailDataImages.logo = logo;
  }
  final List<GetOrganizerStoreDetailDataImagesBanner>? banner =
      jsonConvert.convertListNotNull<GetOrganizerStoreDetailDataImagesBanner>(
          json['banner']);
  if (banner != null) {
    getOrganizerStoreDetailDataImages.banner = banner;
  }
  final dynamic? corporateLogo =
      jsonConvert.convert<dynamic>(json['corporate_logo']);
  if (corporateLogo != null) {
    getOrganizerStoreDetailDataImages.corporateLogo = corporateLogo;
  }
  final dynamic? signature = jsonConvert.convert<dynamic>(json['signature']);
  if (signature != null) {
    getOrganizerStoreDetailDataImages.signature = signature;
  }
  return getOrganizerStoreDetailDataImages;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataImagesToJson(
    GetOrganizerStoreDetailDataImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['logo'] = entity.logo?.toJson();
  data['banner'] = entity.banner?.map((v) => v.toJson()).toList();
  data['corporate_logo'] = entity.corporateLogo;
  data['signature'] = entity.signature;
  return data;
}

GetOrganizerStoreDetailDataImagesLogo
    $GetOrganizerStoreDetailDataImagesLogoFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataImagesLogo
      getOrganizerStoreDetailDataImagesLogo =
      GetOrganizerStoreDetailDataImagesLogo();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataImagesLogo.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getOrganizerStoreDetailDataImagesLogo.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getOrganizerStoreDetailDataImagesLogo.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getOrganizerStoreDetailDataImagesLogo.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataImagesLogo.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getOrganizerStoreDetailDataImagesLogo.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getOrganizerStoreDetailDataImagesLogo.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getOrganizerStoreDetailDataImagesLogo.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getOrganizerStoreDetailDataImagesLogo.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getOrganizerStoreDetailDataImagesLogo.url = url;
  }
  return getOrganizerStoreDetailDataImagesLogo;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataImagesLogoToJson(
    GetOrganizerStoreDetailDataImagesLogo entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetOrganizerStoreDetailDataImagesBanner
    $GetOrganizerStoreDetailDataImagesBannerFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataImagesBanner
      getOrganizerStoreDetailDataImagesBanner =
      GetOrganizerStoreDetailDataImagesBanner();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataImagesBanner.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getOrganizerStoreDetailDataImagesBanner.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getOrganizerStoreDetailDataImagesBanner.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getOrganizerStoreDetailDataImagesBanner.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataImagesBanner.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getOrganizerStoreDetailDataImagesBanner.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getOrganizerStoreDetailDataImagesBanner.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getOrganizerStoreDetailDataImagesBanner.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getOrganizerStoreDetailDataImagesBanner.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getOrganizerStoreDetailDataImagesBanner.url = url;
  }
  return getOrganizerStoreDetailDataImagesBanner;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataImagesBannerToJson(
    GetOrganizerStoreDetailDataImagesBanner entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetOrganizerStoreDetailDataStaff $GetOrganizerStoreDetailDataStaffFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataStaff getOrganizerStoreDetailDataStaff =
      GetOrganizerStoreDetailDataStaff();
  return getOrganizerStoreDetailDataStaff;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataStaffToJson(
    GetOrganizerStoreDetailDataStaff entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetOrganizerStoreDetailDataVenue $GetOrganizerStoreDetailDataVenueFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataVenue getOrganizerStoreDetailDataVenue =
      GetOrganizerStoreDetailDataVenue();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getOrganizerStoreDetailDataVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    getOrganizerStoreDetailDataVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataVenue.name = name;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getOrganizerStoreDetailDataVenue.address = address;
  }
  final GetOrganizerStoreDetailDataVenueCountry? country = jsonConvert
      .convert<GetOrganizerStoreDetailDataVenueCountry>(json['country']);
  if (country != null) {
    getOrganizerStoreDetailDataVenue.country = country;
  }
  final GetOrganizerStoreDetailDataVenueProvince? province = jsonConvert
      .convert<GetOrganizerStoreDetailDataVenueProvince>(json['province']);
  if (province != null) {
    getOrganizerStoreDetailDataVenue.province = province;
  }
  final GetOrganizerStoreDetailDataVenueCity? city =
      jsonConvert.convert<GetOrganizerStoreDetailDataVenueCity>(json['city']);
  if (city != null) {
    getOrganizerStoreDetailDataVenue.city = city;
  }
  final GetOrganizerStoreDetailDataVenueDistrict? district = jsonConvert
      .convert<GetOrganizerStoreDetailDataVenueDistrict>(json['district']);
  if (district != null) {
    getOrganizerStoreDetailDataVenue.district = district;
  }
  final int? zipCode = jsonConvert.convert<int>(json['zip_code']);
  if (zipCode != null) {
    getOrganizerStoreDetailDataVenue.zipCode = zipCode;
  }
  return getOrganizerStoreDetailDataVenue;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataVenueToJson(
    GetOrganizerStoreDetailDataVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  data['address'] = entity.address;
  data['country'] = entity.country?.toJson();
  data['province'] = entity.province?.toJson();
  data['city'] = entity.city?.toJson();
  data['district'] = entity.district?.toJson();
  data['zip_code'] = entity.zipCode;
  return data;
}

GetOrganizerStoreDetailDataVenueCountry
    $GetOrganizerStoreDetailDataVenueCountryFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataVenueCountry
      getOrganizerStoreDetailDataVenueCountry =
      GetOrganizerStoreDetailDataVenueCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataVenueCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataVenueCountry.name = name;
  }
  return getOrganizerStoreDetailDataVenueCountry;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataVenueCountryToJson(
    GetOrganizerStoreDetailDataVenueCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetOrganizerStoreDetailDataVenueProvince
    $GetOrganizerStoreDetailDataVenueProvinceFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataVenueProvince
      getOrganizerStoreDetailDataVenueProvince =
      GetOrganizerStoreDetailDataVenueProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataVenueProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataVenueProvince.name = name;
  }
  return getOrganizerStoreDetailDataVenueProvince;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataVenueProvinceToJson(
    GetOrganizerStoreDetailDataVenueProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetOrganizerStoreDetailDataVenueCity
    $GetOrganizerStoreDetailDataVenueCityFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataVenueCity
      getOrganizerStoreDetailDataVenueCity =
      GetOrganizerStoreDetailDataVenueCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataVenueCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataVenueCity.name = name;
  }
  return getOrganizerStoreDetailDataVenueCity;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataVenueCityToJson(
    GetOrganizerStoreDetailDataVenueCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetOrganizerStoreDetailDataVenueDistrict
    $GetOrganizerStoreDetailDataVenueDistrictFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataVenueDistrict
      getOrganizerStoreDetailDataVenueDistrict =
      GetOrganizerStoreDetailDataVenueDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataVenueDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataVenueDistrict.name = name;
  }
  return getOrganizerStoreDetailDataVenueDistrict;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataVenueDistrictToJson(
    GetOrganizerStoreDetailDataVenueDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetOrganizerStoreDetailDataAttributes
    $GetOrganizerStoreDetailDataAttributesFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataAttributes
      getOrganizerStoreDetailDataAttributes =
      GetOrganizerStoreDetailDataAttributes();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataAttributes.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getOrganizerStoreDetailDataAttributes.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataAttributes.name = name;
  }
  final List<GetOrganizerStoreDetailDataAttributesItems>? items = jsonConvert
      .convertListNotNull<GetOrganizerStoreDetailDataAttributesItems>(
          json['items']);
  if (items != null) {
    getOrganizerStoreDetailDataAttributes.items = items;
  }
  return getOrganizerStoreDetailDataAttributes;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataAttributesToJson(
    GetOrganizerStoreDetailDataAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  data['items'] = entity.items?.map((v) => v.toJson()).toList();
  return data;
}

GetOrganizerStoreDetailDataAttributesItems
    $GetOrganizerStoreDetailDataAttributesItemsFromJson(
        Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataAttributesItems
      getOrganizerStoreDetailDataAttributesItems =
      GetOrganizerStoreDetailDataAttributesItems();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataAttributesItems.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getOrganizerStoreDetailDataAttributesItems.name = name;
  }
  final dynamic? value = jsonConvert.convert<dynamic>(json['value']);
  if (value != null) {
    getOrganizerStoreDetailDataAttributesItems.value = value;
  }
  return getOrganizerStoreDetailDataAttributesItems;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataAttributesItemsToJson(
    GetOrganizerStoreDetailDataAttributesItems entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['value'] = entity.value;
  return data;
}

GetOrganizerStoreDetailDataTimes $GetOrganizerStoreDetailDataTimesFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataTimes getOrganizerStoreDetailDataTimes =
      GetOrganizerStoreDetailDataTimes();
  final int? day = jsonConvert.convert<int>(json['day']);
  if (day != null) {
    getOrganizerStoreDetailDataTimes.day = day;
  }
  final String? dayText = jsonConvert.convert<String>(json['day_text']);
  if (dayText != null) {
    getOrganizerStoreDetailDataTimes.dayText = dayText;
  }
  final dynamic? open = jsonConvert.convert<dynamic>(json['open']);
  if (open != null) {
    getOrganizerStoreDetailDataTimes.open = open;
  }
  final dynamic? close = jsonConvert.convert<dynamic>(json['close']);
  if (close != null) {
    getOrganizerStoreDetailDataTimes.close = close;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getOrganizerStoreDetailDataTimes.status = status;
  }
  return getOrganizerStoreDetailDataTimes;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataTimesToJson(
    GetOrganizerStoreDetailDataTimes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['day'] = entity.day;
  data['day_text'] = entity.dayText;
  data['open'] = entity.open;
  data['close'] = entity.close;
  data['status'] = entity.status;
  return data;
}

GetOrganizerStoreDetailDataTimeOpen
    $GetOrganizerStoreDetailDataTimeOpenFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataTimeOpen
      getOrganizerStoreDetailDataTimeOpen =
      GetOrganizerStoreDetailDataTimeOpen();
  final dynamic? status = jsonConvert.convert<dynamic>(json['status']);
  if (status != null) {
    getOrganizerStoreDetailDataTimeOpen.status = status;
  }
  final dynamic? text = jsonConvert.convert<dynamic>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataTimeOpen.text = text;
  }
  return getOrganizerStoreDetailDataTimeOpen;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataTimeOpenToJson(
    GetOrganizerStoreDetailDataTimeOpen entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataAcceptFeeAt
    $GetOrganizerStoreDetailDataAcceptFeeAtFromJson(Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataAcceptFeeAt
      getOrganizerStoreDetailDataAcceptFeeAt =
      GetOrganizerStoreDetailDataAcceptFeeAt();
  return getOrganizerStoreDetailDataAcceptFeeAt;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataAcceptFeeAtToJson(
    GetOrganizerStoreDetailDataAcceptFeeAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetOrganizerStoreDetailDataParking $GetOrganizerStoreDetailDataParkingFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataParking getOrganizerStoreDetailDataParking =
      GetOrganizerStoreDetailDataParking();
  final dynamic? total = jsonConvert.convert<dynamic>(json['total']);
  if (total != null) {
    getOrganizerStoreDetailDataParking.total = total;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataParking.text = text;
  }
  return getOrganizerStoreDetailDataParking;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataParkingToJson(
    GetOrganizerStoreDetailDataParking entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['total'] = entity.total;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataSeats $GetOrganizerStoreDetailDataSeatsFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataSeats getOrganizerStoreDetailDataSeats =
      GetOrganizerStoreDetailDataSeats();
  final dynamic? min = jsonConvert.convert<dynamic>(json['min']);
  if (min != null) {
    getOrganizerStoreDetailDataSeats.min = min;
  }
  final dynamic? max = jsonConvert.convert<dynamic>(json['max']);
  if (max != null) {
    getOrganizerStoreDetailDataSeats.max = max;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getOrganizerStoreDetailDataSeats.text = text;
  }
  return getOrganizerStoreDetailDataSeats;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataSeatsToJson(
    GetOrganizerStoreDetailDataSeats entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['min'] = entity.min;
  data['max'] = entity.max;
  data['text'] = entity.text;
  return data;
}

GetOrganizerStoreDetailDataPage $GetOrganizerStoreDetailDataPageFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailDataPage getOrganizerStoreDetailDataPage =
      GetOrganizerStoreDetailDataPage();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getOrganizerStoreDetailDataPage.id = id;
  }
  final String? displayName = jsonConvert.convert<String>(json['display_name']);
  if (displayName != null) {
    getOrganizerStoreDetailDataPage.displayName = displayName;
  }
  final dynamic? abouts = jsonConvert.convert<dynamic>(json['abouts']);
  if (abouts != null) {
    getOrganizerStoreDetailDataPage.abouts = abouts;
  }
  final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
  if (slug != null) {
    getOrganizerStoreDetailDataPage.slug = slug;
  }
  final String? basicId = jsonConvert.convert<String>(json['basic_id']);
  if (basicId != null) {
    getOrganizerStoreDetailDataPage.basicId = basicId;
  }
  final dynamic? premiumId = jsonConvert.convert<dynamic>(json['premium_id']);
  if (premiumId != null) {
    getOrganizerStoreDetailDataPage.premiumId = premiumId;
  }
  final int? type = jsonConvert.convert<int>(json['type']);
  if (type != null) {
    getOrganizerStoreDetailDataPage.type = type;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getOrganizerStoreDetailDataPage.storeId = storeId;
  }
  final int? userId = jsonConvert.convert<int>(json['user_id']);
  if (userId != null) {
    getOrganizerStoreDetailDataPage.userId = userId;
  }
  final int? totalFollowers = jsonConvert.convert<int>(json['total_followers']);
  if (totalFollowers != null) {
    getOrganizerStoreDetailDataPage.totalFollowers = totalFollowers;
  }
  final int? totalPosts = jsonConvert.convert<int>(json['total_posts']);
  if (totalPosts != null) {
    getOrganizerStoreDetailDataPage.totalPosts = totalPosts;
  }
  final int? totalShares = jsonConvert.convert<int>(json['total_shares']);
  if (totalShares != null) {
    getOrganizerStoreDetailDataPage.totalShares = totalShares;
  }
  final int? totalViews = jsonConvert.convert<int>(json['total_views']);
  if (totalViews != null) {
    getOrganizerStoreDetailDataPage.totalViews = totalViews;
  }
  final dynamic? totalReported =
      jsonConvert.convert<dynamic>(json['total_reported']);
  if (totalReported != null) {
    getOrganizerStoreDetailDataPage.totalReported = totalReported;
  }
  final dynamic? userRef = jsonConvert.convert<dynamic>(json['user_ref']);
  if (userRef != null) {
    getOrganizerStoreDetailDataPage.userRef = userRef;
  }
  final dynamic? code = jsonConvert.convert<dynamic>(json['code']);
  if (code != null) {
    getOrganizerStoreDetailDataPage.code = code;
  }
  final int? isVerify = jsonConvert.convert<int>(json['is_verify']);
  if (isVerify != null) {
    getOrganizerStoreDetailDataPage.isVerify = isVerify;
  }
  final int? isBlocking = jsonConvert.convert<int>(json['is_blocking']);
  if (isBlocking != null) {
    getOrganizerStoreDetailDataPage.isBlocking = isBlocking;
  }
  final dynamic? staffId = jsonConvert.convert<dynamic>(json['staff_id']);
  if (staffId != null) {
    getOrganizerStoreDetailDataPage.staffId = staffId;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getOrganizerStoreDetailDataPage.remark = remark;
  }
  final dynamic? verifyAt = jsonConvert.convert<dynamic>(json['verify_at']);
  if (verifyAt != null) {
    getOrganizerStoreDetailDataPage.verifyAt = verifyAt;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getOrganizerStoreDetailDataPage.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getOrganizerStoreDetailDataPage.updatedAt = updatedAt;
  }
  final dynamic? deletedAt = jsonConvert.convert<dynamic>(json['deleted_at']);
  if (deletedAt != null) {
    getOrganizerStoreDetailDataPage.deletedAt = deletedAt;
  }
  final dynamic? deletedBy = jsonConvert.convert<dynamic>(json['deleted_by']);
  if (deletedBy != null) {
    getOrganizerStoreDetailDataPage.deletedBy = deletedBy;
  }
  return getOrganizerStoreDetailDataPage;
}

Map<String, dynamic> $GetOrganizerStoreDetailDataPageToJson(
    GetOrganizerStoreDetailDataPage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['display_name'] = entity.displayName;
  data['abouts'] = entity.abouts;
  data['slug'] = entity.slug;
  data['basic_id'] = entity.basicId;
  data['premium_id'] = entity.premiumId;
  data['type'] = entity.type;
  data['store_id'] = entity.storeId;
  data['user_id'] = entity.userId;
  data['total_followers'] = entity.totalFollowers;
  data['total_posts'] = entity.totalPosts;
  data['total_shares'] = entity.totalShares;
  data['total_views'] = entity.totalViews;
  data['total_reported'] = entity.totalReported;
  data['user_ref'] = entity.userRef;
  data['code'] = entity.code;
  data['is_verify'] = entity.isVerify;
  data['is_blocking'] = entity.isBlocking;
  data['staff_id'] = entity.staffId;
  data['remark'] = entity.remark;
  data['verify_at'] = entity.verifyAt;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['deleted_at'] = entity.deletedAt;
  data['deleted_by'] = entity.deletedBy;
  return data;
}

GetOrganizerStoreDetailBench $GetOrganizerStoreDetailBenchFromJson(
    Map<String, dynamic> json) {
  final GetOrganizerStoreDetailBench getOrganizerStoreDetailBench =
      GetOrganizerStoreDetailBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getOrganizerStoreDetailBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getOrganizerStoreDetailBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getOrganizerStoreDetailBench.format = format;
  }
  return getOrganizerStoreDetailBench;
}

Map<String, dynamic> $GetOrganizerStoreDetailBenchToJson(
    GetOrganizerStoreDetailBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
