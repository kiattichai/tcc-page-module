import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/setting_vaccine_limit_response_entity.dart';

SettingVaccineLimitResponseEntity $SettingVaccineLimitResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SettingVaccineLimitResponseEntity settingVaccineLimitResponseEntity =
      SettingVaccineLimitResponseEntity();
  final SettingVaccineLimitResponseData? data =
      jsonConvert.convert<SettingVaccineLimitResponseData>(json['data']);
  if (data != null) {
    settingVaccineLimitResponseEntity.data = data;
  }
  final SettingVaccineLimitResponseBench? bench =
      jsonConvert.convert<SettingVaccineLimitResponseBench>(json['bench']);
  if (bench != null) {
    settingVaccineLimitResponseEntity.bench = bench;
  }
  return settingVaccineLimitResponseEntity;
}

Map<String, dynamic> $SettingVaccineLimitResponseEntityToJson(
    SettingVaccineLimitResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SettingVaccineLimitResponseData $SettingVaccineLimitResponseDataFromJson(
    Map<String, dynamic> json) {
  final SettingVaccineLimitResponseData settingVaccineLimitResponseData =
      SettingVaccineLimitResponseData();
  final List<SettingVaccineLimitResponseDataRecord>? record =
      jsonConvert.convertListNotNull<SettingVaccineLimitResponseDataRecord>(
          json['record']);
  if (record != null) {
    settingVaccineLimitResponseData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    settingVaccineLimitResponseData.cache = cache;
  }
  return settingVaccineLimitResponseData;
}

Map<String, dynamic> $SettingVaccineLimitResponseDataToJson(
    SettingVaccineLimitResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

SettingVaccineLimitResponseDataRecord
    $SettingVaccineLimitResponseDataRecordFromJson(Map<String, dynamic> json) {
  final SettingVaccineLimitResponseDataRecord
      settingVaccineLimitResponseDataRecord =
      SettingVaccineLimitResponseDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    settingVaccineLimitResponseDataRecord.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    settingVaccineLimitResponseDataRecord.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    settingVaccineLimitResponseDataRecord.key = key;
  }
  final int? value = jsonConvert.convert<int>(json['value']);
  if (value != null) {
    settingVaccineLimitResponseDataRecord.value = value;
  }
  final bool? serialized = jsonConvert.convert<bool>(json['serialized']);
  if (serialized != null) {
    settingVaccineLimitResponseDataRecord.serialized = serialized;
  }
  return settingVaccineLimitResponseDataRecord;
}

Map<String, dynamic> $SettingVaccineLimitResponseDataRecordToJson(
    SettingVaccineLimitResponseDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value;
  data['serialized'] = entity.serialized;
  return data;
}

SettingVaccineLimitResponseBench $SettingVaccineLimitResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SettingVaccineLimitResponseBench settingVaccineLimitResponseBench =
      SettingVaccineLimitResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    settingVaccineLimitResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    settingVaccineLimitResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    settingVaccineLimitResponseBench.format = format;
  }
  return settingVaccineLimitResponseBench;
}

Map<String, dynamic> $SettingVaccineLimitResponseBenchToJson(
    SettingVaccineLimitResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
