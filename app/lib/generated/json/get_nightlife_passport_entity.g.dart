import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/get_nightlife_passport_entity.dart';

GetNightlifePassportEntity $GetNightlifePassportEntityFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportEntity getNightlifePassportEntity =
      GetNightlifePassportEntity();
  final GetNightlifePassportData? data =
      jsonConvert.convert<GetNightlifePassportData>(json['data']);
  if (data != null) {
    getNightlifePassportEntity.data = data;
  }
  final GetNightlifePassportBench? bench =
      jsonConvert.convert<GetNightlifePassportBench>(json['bench']);
  if (bench != null) {
    getNightlifePassportEntity.bench = bench;
  }
  return getNightlifePassportEntity;
}

Map<String, dynamic> $GetNightlifePassportEntityToJson(
    GetNightlifePassportEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetNightlifePassportData $GetNightlifePassportDataFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportData getNightlifePassportData =
      GetNightlifePassportData();
  final List<GetNightlifePassportDataRecord>? record = jsonConvert
      .convertListNotNull<GetNightlifePassportDataRecord>(json['record']);
  if (record != null) {
    getNightlifePassportData.record = record;
  }
  final GetNightlifePassportDataUserData? userData =
      jsonConvert.convert<GetNightlifePassportDataUserData>(json['user_data']);
  if (userData != null) {
    getNightlifePassportData.userData = userData;
  }
  final GetNightlifePassportDataDocument? document =
      jsonConvert.convert<GetNightlifePassportDataDocument>(json['document']);
  if (document != null) {
    getNightlifePassportData.document = document;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getNightlifePassportData.cache = cache;
  }
  return getNightlifePassportData;
}

Map<String, dynamic> $GetNightlifePassportDataToJson(
    GetNightlifePassportData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['user_data'] = entity.userData?.toJson();
  data['document'] = entity.document?.toJson();
  data['cache'] = entity.cache;
  return data;
}

GetNightlifePassportDataRecord $GetNightlifePassportDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportDataRecord getNightlifePassportDataRecord =
      GetNightlifePassportDataRecord();
  final int? vaccineId = jsonConvert.convert<int>(json['vaccine_id']);
  if (vaccineId != null) {
    getNightlifePassportDataRecord.vaccineId = vaccineId;
  }
  final String? vaccineName = jsonConvert.convert<String>(json['vaccine_name']);
  if (vaccineName != null) {
    getNightlifePassportDataRecord.vaccineName = vaccineName;
  }
  final int? vaccinationOrder =
      jsonConvert.convert<int>(json['vaccination_order']);
  if (vaccinationOrder != null) {
    getNightlifePassportDataRecord.vaccinationOrder = vaccinationOrder;
  }
  return getNightlifePassportDataRecord;
}

Map<String, dynamic> $GetNightlifePassportDataRecordToJson(
    GetNightlifePassportDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['vaccine_id'] = entity.vaccineId;
  data['vaccine_name'] = entity.vaccineName;
  data['vaccination_order'] = entity.vaccinationOrder;
  return data;
}

GetNightlifePassportDataUserData $GetNightlifePassportDataUserDataFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportDataUserData getNightlifePassportDataUserData =
      GetNightlifePassportDataUserData();
  final int? userId = jsonConvert.convert<int>(json['user_id']);
  if (userId != null) {
    getNightlifePassportDataUserData.userId = userId;
  }
  final String? mobilePhone = jsonConvert.convert<String>(json['mobile_phone']);
  if (mobilePhone != null) {
    getNightlifePassportDataUserData.mobilePhone = mobilePhone;
  }
  final String? fullName = jsonConvert.convert<String>(json['full_name']);
  if (fullName != null) {
    getNightlifePassportDataUserData.fullName = fullName;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getNightlifePassportDataUserData.email = email;
  }
  final int? verifyStatus = jsonConvert.convert<int>(json['verify_status']);
  if (verifyStatus != null) {
    getNightlifePassportDataUserData.verifyStatus = verifyStatus;
  }
  final String? verifyRemark =
      jsonConvert.convert<String>(json['verify_remark']);
  if (verifyRemark != null) {
    getNightlifePassportDataUserData.verifyRemark = verifyRemark;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getNightlifePassportDataUserData.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getNightlifePassportDataUserData.updatedAt = updatedAt;
  }
  return getNightlifePassportDataUserData;
}

Map<String, dynamic> $GetNightlifePassportDataUserDataToJson(
    GetNightlifePassportDataUserData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['user_id'] = entity.userId;
  data['mobile_phone'] = entity.mobilePhone;
  data['full_name'] = entity.fullName;
  data['email'] = entity.email;
  data['verify_status'] = entity.verifyStatus;
  data['verify_remark'] = entity.verifyRemark;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetNightlifePassportDataDocument $GetNightlifePassportDataDocumentFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportDataDocument getNightlifePassportDataDocument =
      GetNightlifePassportDataDocument();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getNightlifePassportDataDocument.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getNightlifePassportDataDocument.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getNightlifePassportDataDocument.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getNightlifePassportDataDocument.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getNightlifePassportDataDocument.mime = mime;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getNightlifePassportDataDocument.url = url;
  }
  return getNightlifePassportDataDocument;
}

Map<String, dynamic> $GetNightlifePassportDataDocumentToJson(
    GetNightlifePassportDataDocument entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['url'] = entity.url;
  return data;
}

GetNightlifePassportBench $GetNightlifePassportBenchFromJson(
    Map<String, dynamic> json) {
  final GetNightlifePassportBench getNightlifePassportBench =
      GetNightlifePassportBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getNightlifePassportBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getNightlifePassportBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getNightlifePassportBench.format = format;
  }
  return getNightlifePassportBench;
}

Map<String, dynamic> $GetNightlifePassportBenchToJson(
    GetNightlifePassportBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
