import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/concert_agent_module/model/get_agent_sale_term_entity.dart';

GetAgentSaleTermEntity $GetAgentSaleTermEntityFromJson(
    Map<String, dynamic> json) {
  final GetAgentSaleTermEntity getAgentSaleTermEntity =
      GetAgentSaleTermEntity();
  final GetAgentSaleTermData? data =
      jsonConvert.convert<GetAgentSaleTermData>(json['data']);
  if (data != null) {
    getAgentSaleTermEntity.data = data;
  }
  final GetAgentSaleTermBench? bench =
      jsonConvert.convert<GetAgentSaleTermBench>(json['bench']);
  if (bench != null) {
    getAgentSaleTermEntity.bench = bench;
  }
  return getAgentSaleTermEntity;
}

Map<String, dynamic> $GetAgentSaleTermEntityToJson(
    GetAgentSaleTermEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetAgentSaleTermData $GetAgentSaleTermDataFromJson(Map<String, dynamic> json) {
  final GetAgentSaleTermData getAgentSaleTermData = GetAgentSaleTermData();
  final GetAgentSaleTermDataPagination? pagination =
      jsonConvert.convert<GetAgentSaleTermDataPagination>(json['pagination']);
  if (pagination != null) {
    getAgentSaleTermData.pagination = pagination;
  }
  final List<GetAgentSaleTermDataRecord>? record = jsonConvert
      .convertListNotNull<GetAgentSaleTermDataRecord>(json['record']);
  if (record != null) {
    getAgentSaleTermData.record = record;
  }
  return getAgentSaleTermData;
}

Map<String, dynamic> $GetAgentSaleTermDataToJson(GetAgentSaleTermData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetAgentSaleTermDataPagination $GetAgentSaleTermDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetAgentSaleTermDataPagination getAgentSaleTermDataPagination =
      GetAgentSaleTermDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getAgentSaleTermDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getAgentSaleTermDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getAgentSaleTermDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getAgentSaleTermDataPagination.total = total;
  }
  return getAgentSaleTermDataPagination;
}

Map<String, dynamic> $GetAgentSaleTermDataPaginationToJson(
    GetAgentSaleTermDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetAgentSaleTermDataRecord $GetAgentSaleTermDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetAgentSaleTermDataRecord getAgentSaleTermDataRecord =
      GetAgentSaleTermDataRecord();
  final List<GetAgentSaleTermDataRecordFields>? fields = jsonConvert
      .convertListNotNull<GetAgentSaleTermDataRecordFields>(json['fields']);
  if (fields != null) {
    getAgentSaleTermDataRecord.fields = fields;
  }
  return getAgentSaleTermDataRecord;
}

Map<String, dynamic> $GetAgentSaleTermDataRecordToJson(
    GetAgentSaleTermDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['fields'] = entity.fields?.map((v) => v.toJson()).toList();
  return data;
}

GetAgentSaleTermDataRecordFields $GetAgentSaleTermDataRecordFieldsFromJson(
    Map<String, dynamic> json) {
  final GetAgentSaleTermDataRecordFields getAgentSaleTermDataRecordFields =
      GetAgentSaleTermDataRecordFields();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getAgentSaleTermDataRecordFields.name = name;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getAgentSaleTermDataRecordFields.lang = lang;
  }
  return getAgentSaleTermDataRecordFields;
}

Map<String, dynamic> $GetAgentSaleTermDataRecordFieldsToJson(
    GetAgentSaleTermDataRecordFields entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['lang'] = entity.lang;
  return data;
}

GetAgentSaleTermBench $GetAgentSaleTermBenchFromJson(
    Map<String, dynamic> json) {
  final GetAgentSaleTermBench getAgentSaleTermBench = GetAgentSaleTermBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getAgentSaleTermBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getAgentSaleTermBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getAgentSaleTermBench.format = format;
  }
  return getAgentSaleTermBench;
}

Map<String, dynamic> $GetAgentSaleTermBenchToJson(
    GetAgentSaleTermBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
