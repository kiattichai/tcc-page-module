import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_my_page_entity.dart';

GetMyPageEntity $GetMyPageEntityFromJson(Map<String, dynamic> json) {
	final GetMyPageEntity getMyPageEntity = GetMyPageEntity();
	final GetMyPagePagination? pagination = jsonConvert.convert<GetMyPagePagination>(json['pagination']);
	if (pagination != null) {
		getMyPageEntity.pagination = pagination;
	}
	final List<GetMyPageData>? data = jsonConvert.convertListNotNull<GetMyPageData>(json['data']);
	if (data != null) {
		getMyPageEntity.data = data;
	}
	return getMyPageEntity;
}

Map<String, dynamic> $GetMyPageEntityToJson(GetMyPageEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['pagination'] = entity.pagination?.toJson();
	data['data'] =  entity.data?.map((v) => v.toJson()).toList();
	return data;
}

GetMyPagePagination $GetMyPagePaginationFromJson(Map<String, dynamic> json) {
	final GetMyPagePagination getMyPagePagination = GetMyPagePagination();
	final int? currentPage = jsonConvert.convert<int>(json['current_page']);
	if (currentPage != null) {
		getMyPagePagination.currentPage = currentPage;
	}
	final int? lastPage = jsonConvert.convert<int>(json['last_page']);
	if (lastPage != null) {
		getMyPagePagination.lastPage = lastPage;
	}
	final int? limitPage = jsonConvert.convert<int>(json['limit_page']);
	if (limitPage != null) {
		getMyPagePagination.limitPage = limitPage;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		getMyPagePagination.total = total;
	}
	return getMyPagePagination;
}

Map<String, dynamic> $GetMyPagePaginationToJson(GetMyPagePagination entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit_page'] = entity.limitPage;
	data['total'] = entity.total;
	return data;
}

GetMyPageData $GetMyPageDataFromJson(Map<String, dynamic> json) {
	final GetMyPageData getMyPageData = GetMyPageData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getMyPageData.id = id;
	}
	final String? displayName = jsonConvert.convert<String>(json['display_name']);
	if (displayName != null) {
		getMyPageData.displayName = displayName;
	}
	final dynamic? abouts = jsonConvert.convert<dynamic>(json['abouts']);
	if (abouts != null) {
		getMyPageData.abouts = abouts;
	}
	final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
	if (slug != null) {
		getMyPageData.slug = slug;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getMyPageData.storeId = storeId;
	}
	final String? basicId = jsonConvert.convert<String>(json['basic_id']);
	if (basicId != null) {
		getMyPageData.basicId = basicId;
	}
	final String? type = jsonConvert.convert<String>(json['type']);
	if (type != null) {
		getMyPageData.type = type;
	}
	final dynamic? userRef = jsonConvert.convert<dynamic>(json['user_ref']);
	if (userRef != null) {
		getMyPageData.userRef = userRef;
	}
	final dynamic? code = jsonConvert.convert<dynamic>(json['code']);
	if (code != null) {
		getMyPageData.code = code;
	}
	final dynamic? shopStatus = jsonConvert.convert<dynamic>(json['shop_status']);
	if (shopStatus != null) {
		getMyPageData.shopStatus = shopStatus;
	}
	final dynamic? shopStatusCode = jsonConvert.convert<dynamic>(json['shop_status_code']);
	if (shopStatusCode != null) {
		getMyPageData.shopStatusCode = shopStatusCode;
	}
	final dynamic? shopStatusText = jsonConvert.convert<dynamic>(json['shop_status_text']);
	if (shopStatusText != null) {
		getMyPageData.shopStatusText = shopStatusText;
	}
	final dynamic? dealerUrl = jsonConvert.convert<dynamic>(json['dealer_url']);
	if (dealerUrl != null) {
		getMyPageData.dealerUrl = dealerUrl;
	}
	final int? totalFollowers = jsonConvert.convert<int>(json['total_followers']);
	if (totalFollowers != null) {
		getMyPageData.totalFollowers = totalFollowers;
	}
	final int? totalPosts = jsonConvert.convert<int>(json['total_posts']);
	if (totalPosts != null) {
		getMyPageData.totalPosts = totalPosts;
	}
	final int? totalShares = jsonConvert.convert<int>(json['total_shares']);
	if (totalShares != null) {
		getMyPageData.totalShares = totalShares;
	}
	final int? totalViews = jsonConvert.convert<int>(json['total_views']);
	if (totalViews != null) {
		getMyPageData.totalViews = totalViews;
	}
	final String? timeAgo = jsonConvert.convert<String>(json['time_ago']);
	if (timeAgo != null) {
		getMyPageData.timeAgo = timeAgo;
	}
	final bool? isOwner = jsonConvert.convert<bool>(json['is_owner']);
	if (isOwner != null) {
		getMyPageData.isOwner = isOwner;
	}
	final bool? isFollow = jsonConvert.convert<bool>(json['is_follow']);
	if (isFollow != null) {
		getMyPageData.isFollow = isFollow;
	}
	final bool? isVerify = jsonConvert.convert<bool>(json['is_verify']);
	if (isVerify != null) {
		getMyPageData.isVerify = isVerify;
	}
	final bool? isBlocking = jsonConvert.convert<bool>(json['is_blocking']);
	if (isBlocking != null) {
		getMyPageData.isBlocking = isBlocking;
	}
	final dynamic? userFollow = jsonConvert.convert<dynamic>(json['user_follow']);
	if (userFollow != null) {
		getMyPageData.userFollow = userFollow;
	}
	final GetMyPageDataAvatar? avatar = jsonConvert.convert<GetMyPageDataAvatar>(json['avatar']);
	if (avatar != null) {
		getMyPageData.avatar = avatar;
	}
	final GetMyPageDataCover? cover = jsonConvert.convert<GetMyPageDataCover>(json['cover']);
	if (cover != null) {
		getMyPageData.cover = cover;
	}
	final List<dynamic>? category = jsonConvert.convertListNotNull<dynamic>(json['category']);
	if (category != null) {
		getMyPageData.category = category;
	}
	final GetMyPageDataUser? user = jsonConvert.convert<GetMyPageDataUser>(json['user']);
	if (user != null) {
		getMyPageData.user = user;
	}
	final dynamic? staff = jsonConvert.convert<dynamic>(json['staff']);
	if (staff != null) {
		getMyPageData.staff = staff;
	}
	final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
	if (remark != null) {
		getMyPageData.remark = remark;
	}
	final String? followUrl = jsonConvert.convert<String>(json['follow_url']);
	if (followUrl != null) {
		getMyPageData.followUrl = followUrl;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		getMyPageData.updatedAt = updatedAt;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		getMyPageData.createdAt = createdAt;
	}
	final String? deletedAt = jsonConvert.convert<String>(json['deleted_at']);
	if (deletedAt != null) {
		getMyPageData.deletedAt = deletedAt;
	}
	return getMyPageData;
}

Map<String, dynamic> $GetMyPageDataToJson(GetMyPageData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['display_name'] = entity.displayName;
	data['abouts'] = entity.abouts;
	data['slug'] = entity.slug;
	data['store_id'] = entity.storeId;
	data['basic_id'] = entity.basicId;
	data['type'] = entity.type;
	data['user_ref'] = entity.userRef;
	data['code'] = entity.code;
	data['shop_status'] = entity.shopStatus;
	data['shop_status_code'] = entity.shopStatusCode;
	data['shop_status_text'] = entity.shopStatusText;
	data['dealer_url'] = entity.dealerUrl;
	data['total_followers'] = entity.totalFollowers;
	data['total_posts'] = entity.totalPosts;
	data['total_shares'] = entity.totalShares;
	data['total_views'] = entity.totalViews;
	data['time_ago'] = entity.timeAgo;
	data['is_owner'] = entity.isOwner;
	data['is_follow'] = entity.isFollow;
	data['is_verify'] = entity.isVerify;
	data['is_blocking'] = entity.isBlocking;
	data['user_follow'] = entity.userFollow;
	data['avatar'] = entity.avatar?.toJson();
	data['cover'] = entity.cover?.toJson();
	data['category'] =  entity.category;
	data['user'] = entity.user?.toJson();
	data['staff'] = entity.staff;
	data['remark'] = entity.remark;
	data['follow_url'] = entity.followUrl;
	data['updated_at'] = entity.updatedAt;
	data['created_at'] = entity.createdAt;
	data['deleted_at'] = entity.deletedAt;
	return data;
}

GetMyPageDataAvatar $GetMyPageDataAvatarFromJson(Map<String, dynamic> json) {
	final GetMyPageDataAvatar getMyPageDataAvatar = GetMyPageDataAvatar();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getMyPageDataAvatar.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getMyPageDataAvatar.storeId = storeId;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getMyPageDataAvatar.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getMyPageDataAvatar.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getMyPageDataAvatar.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getMyPageDataAvatar.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getMyPageDataAvatar.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getMyPageDataAvatar.size = size;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		getMyPageDataAvatar.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		getMyPageDataAvatar.updatedAt = updatedAt;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getMyPageDataAvatar.tag = tag;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getMyPageDataAvatar.position = position;
	}
	return getMyPageDataAvatar;
}

Map<String, dynamic> $GetMyPageDataAvatarToJson(GetMyPageDataAvatar entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['tag'] = entity.tag;
	data['position'] = entity.position;
	return data;
}

GetMyPageDataCover $GetMyPageDataCoverFromJson(Map<String, dynamic> json) {
	final GetMyPageDataCover getMyPageDataCover = GetMyPageDataCover();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getMyPageDataCover.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getMyPageDataCover.storeId = storeId;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getMyPageDataCover.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getMyPageDataCover.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getMyPageDataCover.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getMyPageDataCover.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getMyPageDataCover.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getMyPageDataCover.size = size;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		getMyPageDataCover.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		getMyPageDataCover.updatedAt = updatedAt;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getMyPageDataCover.tag = tag;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getMyPageDataCover.position = position;
	}
	return getMyPageDataCover;
}

Map<String, dynamic> $GetMyPageDataCoverToJson(GetMyPageDataCover entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['tag'] = entity.tag;
	data['position'] = entity.position;
	return data;
}

GetMyPageDataUser $GetMyPageDataUserFromJson(Map<String, dynamic> json) {
	final GetMyPageDataUser getMyPageDataUser = GetMyPageDataUser();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getMyPageDataUser.id = id;
	}
	final dynamic? displayName = jsonConvert.convert<dynamic>(json['display_name']);
	if (displayName != null) {
		getMyPageDataUser.displayName = displayName;
	}
	final GetMyPageDataUserAvatar? avatar = jsonConvert.convert<GetMyPageDataUserAvatar>(json['avatar']);
	if (avatar != null) {
		getMyPageDataUser.avatar = avatar;
	}
	return getMyPageDataUser;
}

Map<String, dynamic> $GetMyPageDataUserToJson(GetMyPageDataUser entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['display_name'] = entity.displayName;
	data['avatar'] = entity.avatar?.toJson();
	return data;
}

GetMyPageDataUserAvatar $GetMyPageDataUserAvatarFromJson(Map<String, dynamic> json) {
	final GetMyPageDataUserAvatar getMyPageDataUserAvatar = GetMyPageDataUserAvatar();
	return getMyPageDataUserAvatar;
}

Map<String, dynamic> $GetMyPageDataUserAvatarToJson(GetMyPageDataUserAvatar entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	return data;
}