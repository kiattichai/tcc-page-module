import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/log_in_module/model/get_user_detail_entity.dart';

GetUserDetailEntity $GetUserDetailEntityFromJson(Map<String, dynamic> json) {
  final GetUserDetailEntity getUserDetailEntity = GetUserDetailEntity();
  final GetUserDetailData? data =
      jsonConvert.convert<GetUserDetailData>(json['data']);
  if (data != null) {
    getUserDetailEntity.data = data;
  }
  final GetUserDetailBench? bench =
      jsonConvert.convert<GetUserDetailBench>(json['bench']);
  if (bench != null) {
    getUserDetailEntity.bench = bench;
  }
  return getUserDetailEntity;
}

Map<String, dynamic> $GetUserDetailEntityToJson(GetUserDetailEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetUserDetailData $GetUserDetailDataFromJson(Map<String, dynamic> json) {
  final GetUserDetailData getUserDetailData = GetUserDetailData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserDetailData.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getUserDetailData.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getUserDetailData.countryCode = countryCode;
  }
  final dynamic? displayName =
      jsonConvert.convert<dynamic>(json['display_name']);
  if (displayName != null) {
    getUserDetailData.displayName = displayName;
  }
  final GetUserDetailDataAvatar? avatar =
      jsonConvert.convert<GetUserDetailDataAvatar>(json['avatar']);
  if (avatar != null) {
    getUserDetailData.avatar = avatar;
  }
  final String? gender = jsonConvert.convert<String>(json['gender']);
  if (gender != null) {
    getUserDetailData.gender = gender;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getUserDetailData.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getUserDetailData.lastName = lastName;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getUserDetailData.email = email;
  }
  final dynamic? idcard = jsonConvert.convert<dynamic>(json['idcard']);
  if (idcard != null) {
    getUserDetailData.idcard = idcard;
  }
  final String? birthday = jsonConvert.convert<String>(json['birthday']);
  if (birthday != null) {
    getUserDetailData.birthday = birthday;
  }
  final bool? termsAccepted = jsonConvert.convert<bool>(json['terms_accepted']);
  if (termsAccepted != null) {
    getUserDetailData.termsAccepted = termsAccepted;
  }
  final bool? activated = jsonConvert.convert<bool>(json['activated']);
  if (activated != null) {
    getUserDetailData.activated = activated;
  }
  final String? activatedAt = jsonConvert.convert<String>(json['activated_at']);
  if (activatedAt != null) {
    getUserDetailData.activatedAt = activatedAt;
  }
  final bool? blocked = jsonConvert.convert<bool>(json['blocked']);
  if (blocked != null) {
    getUserDetailData.blocked = blocked;
  }
  final int? profileScore = jsonConvert.convert<int>(json['profile_score']);
  if (profileScore != null) {
    getUserDetailData.profileScore = profileScore;
  }
  final String? lastLoginAt =
      jsonConvert.convert<String>(json['last_login_at']);
  if (lastLoginAt != null) {
    getUserDetailData.lastLoginAt = lastLoginAt;
  }
  final String? lastLoginIp =
      jsonConvert.convert<String>(json['last_login_ip']);
  if (lastLoginIp != null) {
    getUserDetailData.lastLoginIp = lastLoginIp;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getUserDetailData.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getUserDetailData.updatedAt = updatedAt;
  }
  final String? referral = jsonConvert.convert<String>(json['referral']);
  if (referral != null) {
    getUserDetailData.referral = referral;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getUserDetailData.lang = lang;
  }
  final GetUserDetailDataWallet? wallet =
      jsonConvert.convert<GetUserDetailDataWallet>(json['wallet']);
  if (wallet != null) {
    getUserDetailData.wallet = wallet;
  }
  final GetUserDetailDataGroup? group =
      jsonConvert.convert<GetUserDetailDataGroup>(json['group']);
  if (group != null) {
    getUserDetailData.group = group;
  }
  final GetUserDetailDataAgent? agent =
      jsonConvert.convert<GetUserDetailDataAgent>(json['agent']);
  if (agent != null) {
    getUserDetailData.agent = agent;
  }
  final List<GetUserDetailDataConnects>? connects = jsonConvert
      .convertListNotNull<GetUserDetailDataConnects>(json['connects']);
  if (connects != null) {
    getUserDetailData.connects = connects;
  }
  return getUserDetailData;
}

Map<String, dynamic> $GetUserDetailDataToJson(GetUserDetailData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['display_name'] = entity.displayName;
  data['avatar'] = entity.avatar?.toJson();
  data['gender'] = entity.gender;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['email'] = entity.email;
  data['idcard'] = entity.idcard;
  data['birthday'] = entity.birthday;
  data['terms_accepted'] = entity.termsAccepted;
  data['activated'] = entity.activated;
  data['activated_at'] = entity.activatedAt;
  data['blocked'] = entity.blocked;
  data['profile_score'] = entity.profileScore;
  data['last_login_at'] = entity.lastLoginAt;
  data['last_login_ip'] = entity.lastLoginIp;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['referral'] = entity.referral;
  data['lang'] = entity.lang;
  data['wallet'] = entity.wallet?.toJson();
  data['group'] = entity.group?.toJson();
  data['agent'] = entity.agent?.toJson();
  data['connects'] = entity.connects?.map((v) => v.toJson()).toList();
  return data;
}

GetUserDetailDataAvatar $GetUserDetailDataAvatarFromJson(
    Map<String, dynamic> json) {
  final GetUserDetailDataAvatar getUserDetailDataAvatar =
      GetUserDetailDataAvatar();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getUserDetailDataAvatar.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserDetailDataAvatar.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getUserDetailDataAvatar.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getUserDetailDataAvatar.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getUserDetailDataAvatar.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getUserDetailDataAvatar.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getUserDetailDataAvatar.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getUserDetailDataAvatar.resizeUrl = resizeUrl;
  }
  return getUserDetailDataAvatar;
}

Map<String, dynamic> $GetUserDetailDataAvatarToJson(
    GetUserDetailDataAvatar entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetUserDetailDataWallet $GetUserDetailDataWalletFromJson(
    Map<String, dynamic> json) {
  final GetUserDetailDataWallet getUserDetailDataWallet =
      GetUserDetailDataWallet();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserDetailDataWallet.id = id;
  }
  final int? amount = jsonConvert.convert<int>(json['amount']);
  if (amount != null) {
    getUserDetailDataWallet.amount = amount;
  }
  return getUserDetailDataWallet;
}

Map<String, dynamic> $GetUserDetailDataWalletToJson(
    GetUserDetailDataWallet entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['amount'] = entity.amount;
  return data;
}

GetUserDetailDataGroup $GetUserDetailDataGroupFromJson(
    Map<String, dynamic> json) {
  final GetUserDetailDataGroup getUserDetailDataGroup =
      GetUserDetailDataGroup();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserDetailDataGroup.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserDetailDataGroup.name = name;
  }
  return getUserDetailDataGroup;
}

Map<String, dynamic> $GetUserDetailDataGroupToJson(
    GetUserDetailDataGroup entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserDetailDataAgent $GetUserDetailDataAgentFromJson(
    Map<String, dynamic> json) {
  final GetUserDetailDataAgent getUserDetailDataAgent =
      GetUserDetailDataAgent();
  return getUserDetailDataAgent;
}

Map<String, dynamic> $GetUserDetailDataAgentToJson(
    GetUserDetailDataAgent entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetUserDetailDataConnects $GetUserDetailDataConnectsFromJson(
    Map<String, dynamic> json) {
  final GetUserDetailDataConnects getUserDetailDataConnects =
      GetUserDetailDataConnects();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserDetailDataConnects.id = id;
  }
  final String? service = jsonConvert.convert<String>(json['service']);
  if (service != null) {
    getUserDetailDataConnects.service = service;
  }
  final String? uid = jsonConvert.convert<String>(json['uid']);
  if (uid != null) {
    getUserDetailDataConnects.uid = uid;
  }
  final String? account = jsonConvert.convert<String>(json['account']);
  if (account != null) {
    getUserDetailDataConnects.account = account;
  }
  return getUserDetailDataConnects;
}

Map<String, dynamic> $GetUserDetailDataConnectsToJson(
    GetUserDetailDataConnects entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['service'] = entity.service;
  data['uid'] = entity.uid;
  data['account'] = entity.account;
  return data;
}

GetUserDetailBench $GetUserDetailBenchFromJson(Map<String, dynamic> json) {
  final GetUserDetailBench getUserDetailBench = GetUserDetailBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getUserDetailBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getUserDetailBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getUserDetailBench.format = format;
  }
  return getUserDetailBench;
}

Map<String, dynamic> $GetUserDetailBenchToJson(GetUserDetailBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
