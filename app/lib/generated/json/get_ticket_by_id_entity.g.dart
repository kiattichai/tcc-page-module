import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_ticket_by_id_entity.dart';

GetTicketByIdEntity $GetTicketByIdEntityFromJson(Map<String, dynamic> json) {
	final GetTicketByIdEntity getTicketByIdEntity = GetTicketByIdEntity();
	final GetTicketByIdData? data = jsonConvert.convert<GetTicketByIdData>(json['data']);
	if (data != null) {
		getTicketByIdEntity.data = data;
	}
	final GetTicketByIdBench? bench = jsonConvert.convert<GetTicketByIdBench>(json['bench']);
	if (bench != null) {
		getTicketByIdEntity.bench = bench;
	}
	return getTicketByIdEntity;
}

Map<String, dynamic> $GetTicketByIdEntityToJson(GetTicketByIdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetTicketByIdData $GetTicketByIdDataFromJson(Map<String, dynamic> json) {
	final GetTicketByIdData getTicketByIdData = GetTicketByIdData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getTicketByIdData.id = id;
	}
	final String? sku = jsonConvert.convert<String>(json['sku']);
	if (sku != null) {
		getTicketByIdData.sku = sku;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getTicketByIdData.name = name;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		getTicketByIdData.description = description;
	}
	final dynamic? group = jsonConvert.convert<dynamic>(json['group']);
	if (group != null) {
		getTicketByIdData.group = group;
	}
	final dynamic? zone = jsonConvert.convert<dynamic>(json['zone']);
	if (zone != null) {
		getTicketByIdData.zone = zone;
	}
	final int? costPrice = jsonConvert.convert<int>(json['cost_price']);
	if (costPrice != null) {
		getTicketByIdData.costPrice = costPrice;
	}
	final String? costPriceText = jsonConvert.convert<String>(json['cost_price_text']);
	if (costPriceText != null) {
		getTicketByIdData.costPriceText = costPriceText;
	}
	final int? price = jsonConvert.convert<int>(json['price']);
	if (price != null) {
		getTicketByIdData.price = price;
	}
	final String? priceText = jsonConvert.convert<String>(json['price_text']);
	if (priceText != null) {
		getTicketByIdData.priceText = priceText;
	}
	final int? compareAtPrice = jsonConvert.convert<int>(json['compare_at_price']);
	if (compareAtPrice != null) {
		getTicketByIdData.compareAtPrice = compareAtPrice;
	}
	final String? compareAtPriceText = jsonConvert.convert<String>(json['compare_at_price_text']);
	if (compareAtPriceText != null) {
		getTicketByIdData.compareAtPriceText = compareAtPriceText;
	}
	final bool? package = jsonConvert.convert<bool>(json['package']);
	if (package != null) {
		getTicketByIdData.package = package;
	}
	final int? perPackage = jsonConvert.convert<int>(json['per_package']);
	if (perPackage != null) {
		getTicketByIdData.perPackage = perPackage;
	}
	final int? stock = jsonConvert.convert<int>(json['stock']);
	if (stock != null) {
		getTicketByIdData.stock = stock;
	}
	final int? quantity = jsonConvert.convert<int>(json['quantity']);
	if (quantity != null) {
		getTicketByIdData.quantity = quantity;
	}
	final dynamic? orderQuantity = jsonConvert.convert<dynamic>(json['order_quantity']);
	if (orderQuantity != null) {
		getTicketByIdData.orderQuantity = orderQuantity;
	}
	final int? diffStock = jsonConvert.convert<int>(json['diff_stock']);
	if (diffStock != null) {
		getTicketByIdData.diffStock = diffStock;
	}
	final int? hold = jsonConvert.convert<int>(json['hold']);
	if (hold != null) {
		getTicketByIdData.hold = hold;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getTicketByIdData.position = position;
	}
	final int? points = jsonConvert.convert<int>(json['points']);
	if (points != null) {
		getTicketByIdData.points = points;
	}
	final int? weight = jsonConvert.convert<int>(json['weight']);
	if (weight != null) {
		getTicketByIdData.weight = weight;
	}
	final bool? status = jsonConvert.convert<bool>(json['status']);
	if (status != null) {
		getTicketByIdData.status = status;
	}
	final String? publishedStart = jsonConvert.convert<String>(json['published_start']);
	if (publishedStart != null) {
		getTicketByIdData.publishedStart = publishedStart;
	}
	final String? publishedEnd = jsonConvert.convert<String>(json['published_end']);
	if (publishedEnd != null) {
		getTicketByIdData.publishedEnd = publishedEnd;
	}
	final String? gateOpen = jsonConvert.convert<String>(json['gate_open']);
	if (gateOpen != null) {
		getTicketByIdData.gateOpen = gateOpen;
	}
	final String? gateClose = jsonConvert.convert<String>(json['gate_close']);
	if (gateClose != null) {
		getTicketByIdData.gateClose = gateClose;
	}
	final int? allowOrderMin = jsonConvert.convert<int>(json['allow_order_min']);
	if (allowOrderMin != null) {
		getTicketByIdData.allowOrderMin = allowOrderMin;
	}
	final int? allowOrderMax = jsonConvert.convert<int>(json['allow_order_max']);
	if (allowOrderMax != null) {
		getTicketByIdData.allowOrderMax = allowOrderMax;
	}
	final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
	if (remark != null) {
		getTicketByIdData.remark = remark;
	}
	final bool? specialOption = jsonConvert.convert<bool>(json['special_option']);
	if (specialOption != null) {
		getTicketByIdData.specialOption = specialOption;
	}
	final bool? serviceCharge = jsonConvert.convert<bool>(json['service_charge']);
	if (serviceCharge != null) {
		getTicketByIdData.serviceCharge = serviceCharge;
	}
	final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
	if (serviceFee != null) {
		getTicketByIdData.serviceFee = serviceFee;
	}
	final bool? hasTicket = jsonConvert.convert<bool>(json['has_ticket']);
	if (hasTicket != null) {
		getTicketByIdData.hasTicket = hasTicket;
	}
	final GetTicketByIdDataService? service = jsonConvert.convert<GetTicketByIdDataService>(json['service']);
	if (service != null) {
		getTicketByIdData.service = service;
	}
	final List<dynamic>? options = jsonConvert.convertListNotNull<dynamic>(json['options']);
	if (options != null) {
		getTicketByIdData.options = options;
	}
	final List<dynamic>? promotions = jsonConvert.convertListNotNull<dynamic>(json['promotions']);
	if (promotions != null) {
		getTicketByIdData.promotions = promotions;
	}
	final GetTicketByIdDataImage? image = jsonConvert.convert<GetTicketByIdDataImage>(json['image']);
	if (image != null) {
		getTicketByIdData.image = image;
	}
	final bool? onlineMeeting = jsonConvert.convert<bool>(json['online_meeting']);
	if (onlineMeeting != null) {
		getTicketByIdData.onlineMeeting = onlineMeeting;
	}
	final List<dynamic>? meetings = jsonConvert.convertListNotNull<dynamic>(json['meetings']);
	if (meetings != null) {
		getTicketByIdData.meetings = meetings;
	}
	final dynamic? meta = jsonConvert.convert<dynamic>(json['meta']);
	if (meta != null) {
		getTicketByIdData.meta = meta;
	}
	final bool? displayStatus = jsonConvert.convert<bool>(json['display_status']);
	if (displayStatus != null) {
		getTicketByIdData.displayStatus = displayStatus;
	}
	final String? displayText = jsonConvert.convert<String>(json['display_text']);
	if (displayText != null) {
		getTicketByIdData.displayText = displayText;
	}
	final bool? cache = jsonConvert.convert<bool>(json['cache']);
	if (cache != null) {
		getTicketByIdData.cache = cache;
	}
	return getTicketByIdData;
}

Map<String, dynamic> $GetTicketByIdDataToJson(GetTicketByIdData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['sku'] = entity.sku;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['group'] = entity.group;
	data['zone'] = entity.zone;
	data['cost_price'] = entity.costPrice;
	data['cost_price_text'] = entity.costPriceText;
	data['price'] = entity.price;
	data['price_text'] = entity.priceText;
	data['compare_at_price'] = entity.compareAtPrice;
	data['compare_at_price_text'] = entity.compareAtPriceText;
	data['package'] = entity.package;
	data['per_package'] = entity.perPackage;
	data['stock'] = entity.stock;
	data['quantity'] = entity.quantity;
	data['order_quantity'] = entity.orderQuantity;
	data['diff_stock'] = entity.diffStock;
	data['hold'] = entity.hold;
	data['position'] = entity.position;
	data['points'] = entity.points;
	data['weight'] = entity.weight;
	data['status'] = entity.status;
	data['published_start'] = entity.publishedStart;
	data['published_end'] = entity.publishedEnd;
	data['gate_open'] = entity.gateOpen;
	data['gate_close'] = entity.gateClose;
	data['allow_order_min'] = entity.allowOrderMin;
	data['allow_order_max'] = entity.allowOrderMax;
	data['remark'] = entity.remark;
	data['special_option'] = entity.specialOption;
	data['service_charge'] = entity.serviceCharge;
	data['service_fee'] = entity.serviceFee;
	data['has_ticket'] = entity.hasTicket;
	data['service'] = entity.service?.toJson();
	data['options'] =  entity.options;
	data['promotions'] =  entity.promotions;
	data['image'] = entity.image?.toJson();
	data['online_meeting'] = entity.onlineMeeting;
	data['meetings'] =  entity.meetings;
	data['meta'] = entity.meta;
	data['display_status'] = entity.displayStatus;
	data['display_text'] = entity.displayText;
	data['cache'] = entity.cache;
	return data;
}

GetTicketByIdDataService $GetTicketByIdDataServiceFromJson(Map<String, dynamic> json) {
	final GetTicketByIdDataService getTicketByIdDataService = GetTicketByIdDataService();
	final bool? charge = jsonConvert.convert<bool>(json['charge']);
	if (charge != null) {
		getTicketByIdDataService.charge = charge;
	}
	final int? feeValue = jsonConvert.convert<int>(json['fee_value']);
	if (feeValue != null) {
		getTicketByIdDataService.feeValue = feeValue;
	}
	final int? fee = jsonConvert.convert<int>(json['fee']);
	if (fee != null) {
		getTicketByIdDataService.fee = fee;
	}
	final String? feeText = jsonConvert.convert<String>(json['fee_text']);
	if (feeText != null) {
		getTicketByIdDataService.feeText = feeText;
	}
	return getTicketByIdDataService;
}

Map<String, dynamic> $GetTicketByIdDataServiceToJson(GetTicketByIdDataService entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['charge'] = entity.charge;
	data['fee_value'] = entity.feeValue;
	data['fee'] = entity.fee;
	data['fee_text'] = entity.feeText;
	return data;
}

GetTicketByIdDataImage $GetTicketByIdDataImageFromJson(Map<String, dynamic> json) {
	final GetTicketByIdDataImage getTicketByIdDataImage = GetTicketByIdDataImage();
	return getTicketByIdDataImage;
}

Map<String, dynamic> $GetTicketByIdDataImageToJson(GetTicketByIdDataImage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	return data;
}

GetTicketByIdBench $GetTicketByIdBenchFromJson(Map<String, dynamic> json) {
	final GetTicketByIdBench getTicketByIdBench = GetTicketByIdBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getTicketByIdBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getTicketByIdBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getTicketByIdBench.format = format;
	}
	return getTicketByIdBench;
}

Map<String, dynamic> $GetTicketByIdBenchToJson(GetTicketByIdBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}