import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/get_geocode_entity.dart';

GetGeocodeEntity $GetGeocodeEntityFromJson(Map<String, dynamic> json) {
  final GetGeocodeEntity getGeocodeEntity = GetGeocodeEntity();
  final GetGeocodePlusCode? plusCode =
      jsonConvert.convert<GetGeocodePlusCode>(json['plus_code']);
  if (plusCode != null) {
    getGeocodeEntity.plusCode = plusCode;
  }
  final List<GetGeocodeResults>? results =
      jsonConvert.convertListNotNull<GetGeocodeResults>(json['results']);
  if (results != null) {
    getGeocodeEntity.results = results;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    getGeocodeEntity.status = status;
  }
  return getGeocodeEntity;
}

Map<String, dynamic> $GetGeocodeEntityToJson(GetGeocodeEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['plus_code'] = entity.plusCode?.toJson();
  data['results'] = entity.results?.map((v) => v.toJson()).toList();
  data['status'] = entity.status;
  return data;
}

GetGeocodePlusCode $GetGeocodePlusCodeFromJson(Map<String, dynamic> json) {
  final GetGeocodePlusCode getGeocodePlusCode = GetGeocodePlusCode();
  final String? compoundCode =
      jsonConvert.convert<String>(json['compound_code']);
  if (compoundCode != null) {
    getGeocodePlusCode.compoundCode = compoundCode;
  }
  final String? globalCode = jsonConvert.convert<String>(json['global_code']);
  if (globalCode != null) {
    getGeocodePlusCode.globalCode = globalCode;
  }
  return getGeocodePlusCode;
}

Map<String, dynamic> $GetGeocodePlusCodeToJson(GetGeocodePlusCode entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['compound_code'] = entity.compoundCode;
  data['global_code'] = entity.globalCode;
  return data;
}

GetGeocodeResults $GetGeocodeResultsFromJson(Map<String, dynamic> json) {
  final GetGeocodeResults getGeocodeResults = GetGeocodeResults();
  final List<GetGeocodeResultsAddressComponents>? addressComponents =
      jsonConvert.convertListNotNull<GetGeocodeResultsAddressComponents>(
          json['address_components']);
  if (addressComponents != null) {
    getGeocodeResults.addressComponents = addressComponents;
  }
  final String? formattedAddress =
      jsonConvert.convert<String>(json['formatted_address']);
  if (formattedAddress != null) {
    getGeocodeResults.formattedAddress = formattedAddress;
  }
  final GetGeocodeResultsGeometry? geometry =
      jsonConvert.convert<GetGeocodeResultsGeometry>(json['geometry']);
  if (geometry != null) {
    getGeocodeResults.geometry = geometry;
  }
  final String? placeId = jsonConvert.convert<String>(json['place_id']);
  if (placeId != null) {
    getGeocodeResults.placeId = placeId;
  }
  final GetGeocodeResultsPlusCode? plusCode =
      jsonConvert.convert<GetGeocodeResultsPlusCode>(json['plus_code']);
  if (plusCode != null) {
    getGeocodeResults.plusCode = plusCode;
  }
  final List<String>? types =
      jsonConvert.convertListNotNull<String>(json['types']);
  if (types != null) {
    getGeocodeResults.types = types;
  }
  return getGeocodeResults;
}

Map<String, dynamic> $GetGeocodeResultsToJson(GetGeocodeResults entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['address_components'] =
      entity.addressComponents?.map((v) => v.toJson()).toList();
  data['formatted_address'] = entity.formattedAddress;
  data['geometry'] = entity.geometry?.toJson();
  data['place_id'] = entity.placeId;
  data['plus_code'] = entity.plusCode?.toJson();
  data['types'] = entity.types;
  return data;
}

GetGeocodeResultsAddressComponents $GetGeocodeResultsAddressComponentsFromJson(
    Map<String, dynamic> json) {
  final GetGeocodeResultsAddressComponents getGeocodeResultsAddressComponents =
      GetGeocodeResultsAddressComponents();
  final String? longName = jsonConvert.convert<String>(json['long_name']);
  if (longName != null) {
    getGeocodeResultsAddressComponents.longName = longName;
  }
  final String? shortName = jsonConvert.convert<String>(json['short_name']);
  if (shortName != null) {
    getGeocodeResultsAddressComponents.shortName = shortName;
  }
  final List<String>? types =
      jsonConvert.convertListNotNull<String>(json['types']);
  if (types != null) {
    getGeocodeResultsAddressComponents.types = types;
  }
  return getGeocodeResultsAddressComponents;
}

Map<String, dynamic> $GetGeocodeResultsAddressComponentsToJson(
    GetGeocodeResultsAddressComponents entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['long_name'] = entity.longName;
  data['short_name'] = entity.shortName;
  data['types'] = entity.types;
  return data;
}

GetGeocodeResultsGeometry $GetGeocodeResultsGeometryFromJson(
    Map<String, dynamic> json) {
  final GetGeocodeResultsGeometry getGeocodeResultsGeometry =
      GetGeocodeResultsGeometry();
  final GetGeocodeResultsGeometryLocation? location =
      jsonConvert.convert<GetGeocodeResultsGeometryLocation>(json['location']);
  if (location != null) {
    getGeocodeResultsGeometry.location = location;
  }
  final String? locationType =
      jsonConvert.convert<String>(json['location_type']);
  if (locationType != null) {
    getGeocodeResultsGeometry.locationType = locationType;
  }
  final GetGeocodeResultsGeometryViewport? viewport =
      jsonConvert.convert<GetGeocodeResultsGeometryViewport>(json['viewport']);
  if (viewport != null) {
    getGeocodeResultsGeometry.viewport = viewport;
  }
  return getGeocodeResultsGeometry;
}

Map<String, dynamic> $GetGeocodeResultsGeometryToJson(
    GetGeocodeResultsGeometry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['location'] = entity.location?.toJson();
  data['location_type'] = entity.locationType;
  data['viewport'] = entity.viewport?.toJson();
  return data;
}

GetGeocodeResultsGeometryLocation $GetGeocodeResultsGeometryLocationFromJson(
    Map<String, dynamic> json) {
  final GetGeocodeResultsGeometryLocation getGeocodeResultsGeometryLocation =
      GetGeocodeResultsGeometryLocation();
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getGeocodeResultsGeometryLocation.lat = lat;
  }
  final double? lng = jsonConvert.convert<double>(json['lng']);
  if (lng != null) {
    getGeocodeResultsGeometryLocation.lng = lng;
  }
  return getGeocodeResultsGeometryLocation;
}

Map<String, dynamic> $GetGeocodeResultsGeometryLocationToJson(
    GetGeocodeResultsGeometryLocation entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['lat'] = entity.lat;
  data['lng'] = entity.lng;
  return data;
}

GetGeocodeResultsGeometryViewport $GetGeocodeResultsGeometryViewportFromJson(
    Map<String, dynamic> json) {
  final GetGeocodeResultsGeometryViewport getGeocodeResultsGeometryViewport =
      GetGeocodeResultsGeometryViewport();
  final GetGeocodeResultsGeometryViewportNortheast? northeast = jsonConvert
      .convert<GetGeocodeResultsGeometryViewportNortheast>(json['northeast']);
  if (northeast != null) {
    getGeocodeResultsGeometryViewport.northeast = northeast;
  }
  final GetGeocodeResultsGeometryViewportSouthwest? southwest = jsonConvert
      .convert<GetGeocodeResultsGeometryViewportSouthwest>(json['southwest']);
  if (southwest != null) {
    getGeocodeResultsGeometryViewport.southwest = southwest;
  }
  return getGeocodeResultsGeometryViewport;
}

Map<String, dynamic> $GetGeocodeResultsGeometryViewportToJson(
    GetGeocodeResultsGeometryViewport entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['northeast'] = entity.northeast?.toJson();
  data['southwest'] = entity.southwest?.toJson();
  return data;
}

GetGeocodeResultsGeometryViewportNortheast
    $GetGeocodeResultsGeometryViewportNortheastFromJson(
        Map<String, dynamic> json) {
  final GetGeocodeResultsGeometryViewportNortheast
      getGeocodeResultsGeometryViewportNortheast =
      GetGeocodeResultsGeometryViewportNortheast();
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getGeocodeResultsGeometryViewportNortheast.lat = lat;
  }
  final double? lng = jsonConvert.convert<double>(json['lng']);
  if (lng != null) {
    getGeocodeResultsGeometryViewportNortheast.lng = lng;
  }
  return getGeocodeResultsGeometryViewportNortheast;
}

Map<String, dynamic> $GetGeocodeResultsGeometryViewportNortheastToJson(
    GetGeocodeResultsGeometryViewportNortheast entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['lat'] = entity.lat;
  data['lng'] = entity.lng;
  return data;
}

GetGeocodeResultsGeometryViewportSouthwest
    $GetGeocodeResultsGeometryViewportSouthwestFromJson(
        Map<String, dynamic> json) {
  final GetGeocodeResultsGeometryViewportSouthwest
      getGeocodeResultsGeometryViewportSouthwest =
      GetGeocodeResultsGeometryViewportSouthwest();
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getGeocodeResultsGeometryViewportSouthwest.lat = lat;
  }
  final double? lng = jsonConvert.convert<double>(json['lng']);
  if (lng != null) {
    getGeocodeResultsGeometryViewportSouthwest.lng = lng;
  }
  return getGeocodeResultsGeometryViewportSouthwest;
}

Map<String, dynamic> $GetGeocodeResultsGeometryViewportSouthwestToJson(
    GetGeocodeResultsGeometryViewportSouthwest entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['lat'] = entity.lat;
  data['lng'] = entity.lng;
  return data;
}

GetGeocodeResultsPlusCode $GetGeocodeResultsPlusCodeFromJson(
    Map<String, dynamic> json) {
  final GetGeocodeResultsPlusCode getGeocodeResultsPlusCode =
      GetGeocodeResultsPlusCode();
  final String? compoundCode =
      jsonConvert.convert<String>(json['compound_code']);
  if (compoundCode != null) {
    getGeocodeResultsPlusCode.compoundCode = compoundCode;
  }
  final String? globalCode = jsonConvert.convert<String>(json['global_code']);
  if (globalCode != null) {
    getGeocodeResultsPlusCode.globalCode = globalCode;
  }
  return getGeocodeResultsPlusCode;
}

Map<String, dynamic> $GetGeocodeResultsPlusCodeToJson(
    GetGeocodeResultsPlusCode entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['compound_code'] = entity.compoundCode;
  data['global_code'] = entity.globalCode;
  return data;
}
