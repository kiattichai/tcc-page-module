import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/delete_image_review_entity.dart';

DeleteImageReviewEntity $DeleteImageReviewEntityFromJson(
    Map<String, dynamic> json) {
  final DeleteImageReviewEntity deleteImageReviewEntity =
      DeleteImageReviewEntity();
  final List<DeleteImageReviewFiles>? files =
      jsonConvert.convertListNotNull<DeleteImageReviewFiles>(json['files']);
  if (files != null) {
    deleteImageReviewEntity.files = files;
  }
  return deleteImageReviewEntity;
}

Map<String, dynamic> $DeleteImageReviewEntityToJson(
    DeleteImageReviewEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['files'] = entity.files?.map((v) => v.toJson()).toList();
  return data;
}

DeleteImageReviewFiles $DeleteImageReviewFilesFromJson(
    Map<String, dynamic> json) {
  final DeleteImageReviewFiles deleteImageReviewFiles =
      DeleteImageReviewFiles();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    deleteImageReviewFiles.id = id;
  }
  return deleteImageReviewFiles;
}

Map<String, dynamic> $DeleteImageReviewFilesToJson(
    DeleteImageReviewFiles entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  return data;
}
