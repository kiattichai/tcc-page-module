import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_review_by_id_entity.dart';

GetReviewByIdEntity $GetReviewByIdEntityFromJson(Map<String, dynamic> json) {
	final GetReviewByIdEntity getReviewByIdEntity = GetReviewByIdEntity();
	final GetReviewByIdData? data = jsonConvert.convert<GetReviewByIdData>(json['data']);
	if (data != null) {
		getReviewByIdEntity.data = data;
	}
	final GetReviewByIdBench? bench = jsonConvert.convert<GetReviewByIdBench>(json['bench']);
	if (bench != null) {
		getReviewByIdEntity.bench = bench;
	}
	return getReviewByIdEntity;
}

Map<String, dynamic> $GetReviewByIdEntityToJson(GetReviewByIdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetReviewByIdData $GetReviewByIdDataFromJson(Map<String, dynamic> json) {
	final GetReviewByIdData getReviewByIdData = GetReviewByIdData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getReviewByIdData.id = id;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		getReviewByIdData.description = description;
	}
	final GetReviewByIdDataUser? user = jsonConvert.convert<GetReviewByIdDataUser>(json['user']);
	if (user != null) {
		getReviewByIdData.user = user;
	}
	final GetReviewByIdDataStore? store = jsonConvert.convert<GetReviewByIdDataStore>(json['store']);
	if (store != null) {
		getReviewByIdData.store = store;
	}
	final int? rating = jsonConvert.convert<int>(json['rating']);
	if (rating != null) {
		getReviewByIdData.rating = rating;
	}
	final List<GetReviewByIdDataImages>? images = jsonConvert.convertListNotNull<GetReviewByIdDataImages>(json['images']);
	if (images != null) {
		getReviewByIdData.images = images;
	}
	final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
	if (remark != null) {
		getReviewByIdData.remark = remark;
	}
	final GetReviewByIdDataCreatedAt? createdAt = jsonConvert.convert<GetReviewByIdDataCreatedAt>(json['created_at']);
	if (createdAt != null) {
		getReviewByIdData.createdAt = createdAt;
	}
	final GetReviewByIdDataUpdatedAt? updatedAt = jsonConvert.convert<GetReviewByIdDataUpdatedAt>(json['updated_at']);
	if (updatedAt != null) {
		getReviewByIdData.updatedAt = updatedAt;
	}
	final bool? cache = jsonConvert.convert<bool>(json['cache']);
	if (cache != null) {
		getReviewByIdData.cache = cache;
	}
	return getReviewByIdData;
}

Map<String, dynamic> $GetReviewByIdDataToJson(GetReviewByIdData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['description'] = entity.description;
	data['user'] = entity.user?.toJson();
	data['store'] = entity.store?.toJson();
	data['rating'] = entity.rating;
	data['images'] =  entity.images?.map((v) => v.toJson()).toList();
	data['remark'] = entity.remark;
	data['created_at'] = entity.createdAt?.toJson();
	data['updated_at'] = entity.updatedAt?.toJson();
	data['cache'] = entity.cache;
	return data;
}

GetReviewByIdDataUser $GetReviewByIdDataUserFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataUser getReviewByIdDataUser = GetReviewByIdDataUser();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		getReviewByIdDataUser.id = id;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		getReviewByIdDataUser.username = username;
	}
	final String? firstName = jsonConvert.convert<String>(json['first_name']);
	if (firstName != null) {
		getReviewByIdDataUser.firstName = firstName;
	}
	final String? lastName = jsonConvert.convert<String>(json['last_name']);
	if (lastName != null) {
		getReviewByIdDataUser.lastName = lastName;
	}
	final GetReviewByIdDataUserImage? image = jsonConvert.convert<GetReviewByIdDataUserImage>(json['image']);
	if (image != null) {
		getReviewByIdDataUser.image = image;
	}
	return getReviewByIdDataUser;
}

Map<String, dynamic> $GetReviewByIdDataUserToJson(GetReviewByIdDataUser entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['image'] = entity.image?.toJson();
	return data;
}

GetReviewByIdDataUserImage $GetReviewByIdDataUserImageFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataUserImage getReviewByIdDataUserImage = GetReviewByIdDataUserImage();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getReviewByIdDataUserImage.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getReviewByIdDataUserImage.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getReviewByIdDataUserImage.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getReviewByIdDataUserImage.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getReviewByIdDataUserImage.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getReviewByIdDataUserImage.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getReviewByIdDataUserImage.url = url;
	}
	final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
	if (resizeUrl != null) {
		getReviewByIdDataUserImage.resizeUrl = resizeUrl;
	}
	return getReviewByIdDataUserImage;
}

Map<String, dynamic> $GetReviewByIdDataUserImageToJson(GetReviewByIdDataUserImage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	return data;
}

GetReviewByIdDataStore $GetReviewByIdDataStoreFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataStore getReviewByIdDataStore = GetReviewByIdDataStore();
	return getReviewByIdDataStore;
}

Map<String, dynamic> $GetReviewByIdDataStoreToJson(GetReviewByIdDataStore entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	return data;
}

GetReviewByIdDataImages $GetReviewByIdDataImagesFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataImages getReviewByIdDataImages = GetReviewByIdDataImages();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		getReviewByIdDataImages.id = id;
	}
	final int? storeId = jsonConvert.convert<int>(json['store_id']);
	if (storeId != null) {
		getReviewByIdDataImages.storeId = storeId;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		getReviewByIdDataImages.tag = tag;
	}
	final int? albumId = jsonConvert.convert<int>(json['album_id']);
	if (albumId != null) {
		getReviewByIdDataImages.albumId = albumId;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		getReviewByIdDataImages.name = name;
	}
	final int? width = jsonConvert.convert<int>(json['width']);
	if (width != null) {
		getReviewByIdDataImages.width = width;
	}
	final int? height = jsonConvert.convert<int>(json['height']);
	if (height != null) {
		getReviewByIdDataImages.height = height;
	}
	final String? mime = jsonConvert.convert<String>(json['mime']);
	if (mime != null) {
		getReviewByIdDataImages.mime = mime;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		getReviewByIdDataImages.size = size;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		getReviewByIdDataImages.url = url;
	}
	final int? position = jsonConvert.convert<int>(json['position']);
	if (position != null) {
		getReviewByIdDataImages.position = position;
	}
	return getReviewByIdDataImages;
}

Map<String, dynamic> $GetReviewByIdDataImagesToJson(GetReviewByIdDataImages entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

GetReviewByIdDataCreatedAt $GetReviewByIdDataCreatedAtFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataCreatedAt getReviewByIdDataCreatedAt = GetReviewByIdDataCreatedAt();
	final String? value = jsonConvert.convert<String>(json['value']);
	if (value != null) {
		getReviewByIdDataCreatedAt.value = value;
	}
	final String? date = jsonConvert.convert<String>(json['date']);
	if (date != null) {
		getReviewByIdDataCreatedAt.date = date;
	}
	final String? time = jsonConvert.convert<String>(json['time']);
	if (time != null) {
		getReviewByIdDataCreatedAt.time = time;
	}
	return getReviewByIdDataCreatedAt;
}

Map<String, dynamic> $GetReviewByIdDataCreatedAtToJson(GetReviewByIdDataCreatedAt entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

GetReviewByIdDataUpdatedAt $GetReviewByIdDataUpdatedAtFromJson(Map<String, dynamic> json) {
	final GetReviewByIdDataUpdatedAt getReviewByIdDataUpdatedAt = GetReviewByIdDataUpdatedAt();
	final String? value = jsonConvert.convert<String>(json['value']);
	if (value != null) {
		getReviewByIdDataUpdatedAt.value = value;
	}
	final String? date = jsonConvert.convert<String>(json['date']);
	if (date != null) {
		getReviewByIdDataUpdatedAt.date = date;
	}
	final String? time = jsonConvert.convert<String>(json['time']);
	if (time != null) {
		getReviewByIdDataUpdatedAt.time = time;
	}
	return getReviewByIdDataUpdatedAt;
}

Map<String, dynamic> $GetReviewByIdDataUpdatedAtToJson(GetReviewByIdDataUpdatedAt entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

GetReviewByIdBench $GetReviewByIdBenchFromJson(Map<String, dynamic> json) {
	final GetReviewByIdBench getReviewByIdBench = GetReviewByIdBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getReviewByIdBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getReviewByIdBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getReviewByIdBench.format = format;
	}
	return getReviewByIdBench;
}

Map<String, dynamic> $GetReviewByIdBenchToJson(GetReviewByIdBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}