import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/log_in_module/model/get_user_authen_entity.dart';

GetUserAuthenEntity $GetUserAuthenEntityFromJson(Map<String, dynamic> json) {
  final GetUserAuthenEntity getUserAuthenEntity = GetUserAuthenEntity();
  final GetUserAuthenData? data =
      jsonConvert.convert<GetUserAuthenData>(json['data']);
  if (data != null) {
    getUserAuthenEntity.data = data;
  }
  final GetUserAuthenBench? bench =
      jsonConvert.convert<GetUserAuthenBench>(json['bench']);
  if (bench != null) {
    getUserAuthenEntity.bench = bench;
  }
  return getUserAuthenEntity;
}

Map<String, dynamic> $GetUserAuthenEntityToJson(GetUserAuthenEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetUserAuthenData $GetUserAuthenDataFromJson(Map<String, dynamic> json) {
  final GetUserAuthenData getUserAuthenData = GetUserAuthenData();
  final String? tokenType = jsonConvert.convert<String>(json['token_type']);
  if (tokenType != null) {
    getUserAuthenData.tokenType = tokenType;
  }
  final String? accessToken = jsonConvert.convert<String>(json['access_token']);
  if (accessToken != null) {
    getUserAuthenData.accessToken = accessToken;
  }
  final String? refreshToken =
      jsonConvert.convert<String>(json['refresh_token']);
  if (refreshToken != null) {
    getUserAuthenData.refreshToken = refreshToken;
  }
  return getUserAuthenData;
}

Map<String, dynamic> $GetUserAuthenDataToJson(GetUserAuthenData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['token_type'] = entity.tokenType;
  data['access_token'] = entity.accessToken;
  data['refresh_token'] = entity.refreshToken;
  return data;
}

GetUserAuthenBench $GetUserAuthenBenchFromJson(Map<String, dynamic> json) {
  final GetUserAuthenBench getUserAuthenBench = GetUserAuthenBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getUserAuthenBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getUserAuthenBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getUserAuthenBench.format = format;
  }
  return getUserAuthenBench;
}

Map<String, dynamic> $GetUserAuthenBenchToJson(GetUserAuthenBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
