import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/base_response_entity.dart';

BaseResponseEntity $BaseResponseEntityFromJson(Map<String, dynamic> json) {
  final BaseResponseEntity baseResponseEntity = BaseResponseEntity();
  final BaseResponseData? data =
      jsonConvert.convert<BaseResponseData>(json['data']);
  if (data != null) {
    baseResponseEntity.data = data;
  }
  return baseResponseEntity;
}

Map<String, dynamic> $BaseResponseEntityToJson(BaseResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  return data;
}

BaseResponseData $BaseResponseDataFromJson(Map<String, dynamic> json) {
  final BaseResponseData baseResponseData = BaseResponseData();
  final int? orderId = jsonConvert.convert<int>(json['order_id']);
  if (orderId != null) {
    baseResponseData.orderId = orderId;
  }
  final String? orderNo = jsonConvert.convert<String>(json['order_no']);
  if (orderNo != null) {
    baseResponseData.orderNo = orderNo;
  }
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    baseResponseData.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    baseResponseData.id = id;
  }
  final String? comment = jsonConvert.convert<String>(json['comment']);
  if (comment != null) {
    baseResponseData.comment = comment;
  }
  final List<DataUpload>? uploads =
      jsonConvert.convertListNotNull<DataUpload>(json['uploads']);
  if (uploads != null) {
    baseResponseData.uploads = uploads;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    baseResponseData.status = status;
  }
  return baseResponseData;
}

Map<String, dynamic> $BaseResponseDataToJson(BaseResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['order_id'] = entity.orderId;
  data['order_no'] = entity.orderNo;
  data['message'] = entity.message;
  data['id'] = entity.id;
  data['comment'] = entity.comment;
  data['uploads'] = entity.uploads?.map((v) => v.toJson()).toList();
  data['status'] = entity.status;
  return data;
}

DataUpload $DataUploadFromJson(Map<String, dynamic> json) {
  final DataUpload dataUpload = DataUpload();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    dataUpload.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    dataUpload.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    dataUpload.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    dataUpload.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    dataUpload.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    dataUpload.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    dataUpload.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    dataUpload.resizeUrl = resizeUrl;
  }
  return dataUpload;
}

Map<String, dynamic> $DataUploadToJson(DataUpload entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}
