import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_code_entity.dart';

GetCodeEntity $GetCodeEntityFromJson(Map<String, dynamic> json) {
  final GetCodeEntity getCodeEntity = GetCodeEntity();
  final GetCodeData? data = jsonConvert.convert<GetCodeData>(json['data']);
  if (data != null) {
    getCodeEntity.data = data;
  }
  final GetCodeBench? bench = jsonConvert.convert<GetCodeBench>(json['bench']);
  if (bench != null) {
    getCodeEntity.bench = bench;
  }
  return getCodeEntity;
}

Map<String, dynamic> $GetCodeEntityToJson(GetCodeEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetCodeData $GetCodeDataFromJson(Map<String, dynamic> json) {
  final GetCodeData getCodeData = GetCodeData();
  final GetCodeDataPagination? pagination =
      jsonConvert.convert<GetCodeDataPagination>(json['pagination']);
  if (pagination != null) {
    getCodeData.pagination = pagination;
  }
  final List<GetCodeDataRecord>? record =
      jsonConvert.convertListNotNull<GetCodeDataRecord>(json['record']);
  if (record != null) {
    getCodeData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getCodeData.cache = cache;
  }
  return getCodeData;
}

Map<String, dynamic> $GetCodeDataToJson(GetCodeData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetCodeDataPagination $GetCodeDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetCodeDataPagination getCodeDataPagination = GetCodeDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getCodeDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getCodeDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getCodeDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getCodeDataPagination.total = total;
  }
  return getCodeDataPagination;
}

Map<String, dynamic> $GetCodeDataPaginationToJson(
    GetCodeDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetCodeDataRecord $GetCodeDataRecordFromJson(Map<String, dynamic> json) {
  final GetCodeDataRecord getCodeDataRecord = GetCodeDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCodeDataRecord.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getCodeDataRecord.code = code;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getCodeDataRecord.status = status;
  }
  final int? used = jsonConvert.convert<int>(json['used']);
  if (used != null) {
    getCodeDataRecord.used = used;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getCodeDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getCodeDataRecord.updatedAt = updatedAt;
  }
  final int? sumDiscount = jsonConvert.convert<int>(json['sum_discount']);
  if (sumDiscount != null) {
    getCodeDataRecord.sumDiscount = sumDiscount;
  }
  return getCodeDataRecord;
}

Map<String, dynamic> $GetCodeDataRecordToJson(GetCodeDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['used'] = entity.used;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['sum_discount'] = entity.sumDiscount;
  return data;
}

GetCodeBench $GetCodeBenchFromJson(Map<String, dynamic> json) {
  final GetCodeBench getCodeBench = GetCodeBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getCodeBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getCodeBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getCodeBench.format = format;
  }
  return getCodeBench;
}

Map<String, dynamic> $GetCodeBenchToJson(GetCodeBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
