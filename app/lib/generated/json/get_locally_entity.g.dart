import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/create_page_module/model/get_locally_entity.dart';

GetLocallyEntity $GetLocallyEntityFromJson(Map<String, dynamic> json) {
	final GetLocallyEntity getLocallyEntity = GetLocallyEntity();
	final GetLocallyData? data = jsonConvert.convert<GetLocallyData>(json['data']);
	if (data != null) {
		getLocallyEntity.data = data;
	}
	final GetLocallyBench? bench = jsonConvert.convert<GetLocallyBench>(json['bench']);
	if (bench != null) {
		getLocallyEntity.bench = bench;
	}
	return getLocallyEntity;
}

Map<String, dynamic> $GetLocallyEntityToJson(GetLocallyEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

GetLocallyData $GetLocallyDataFromJson(Map<String, dynamic> json) {
	final GetLocallyData getLocallyData = GetLocallyData();
	final int? provinceId = jsonConvert.convert<int>(json['province_id']);
	if (provinceId != null) {
		getLocallyData.provinceId = provinceId;
	}
	final int? cityId = jsonConvert.convert<int>(json['city_id']);
	if (cityId != null) {
		getLocallyData.cityId = cityId;
	}
	final int? districtId = jsonConvert.convert<int>(json['district_id']);
	if (districtId != null) {
		getLocallyData.districtId = districtId;
	}
	final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
	if (zipCode != null) {
		getLocallyData.zipCode = zipCode;
	}
	final bool? cache = jsonConvert.convert<bool>(json['cache']);
	if (cache != null) {
		getLocallyData.cache = cache;
	}
	return getLocallyData;
}

Map<String, dynamic> $GetLocallyDataToJson(GetLocallyData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['province_id'] = entity.provinceId;
	data['city_id'] = entity.cityId;
	data['district_id'] = entity.districtId;
	data['zip_code'] = entity.zipCode;
	data['cache'] = entity.cache;
	return data;
}

GetLocallyBench $GetLocallyBenchFromJson(Map<String, dynamic> json) {
	final GetLocallyBench getLocallyBench = GetLocallyBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		getLocallyBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		getLocallyBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		getLocallyBench.format = format;
	}
	return getLocallyBench;
}

Map<String, dynamic> $GetLocallyBenchToJson(GetLocallyBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}