import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_review_response_entity.dart';

SaveReviewResponseEntity $SaveReviewResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SaveReviewResponseEntity saveReviewResponseEntity =
      SaveReviewResponseEntity();
  final SaveReviewResponseData? data =
      jsonConvert.convert<SaveReviewResponseData>(json['data']);
  if (data != null) {
    saveReviewResponseEntity.data = data;
  }
  final SaveReviewResponseBench? bench =
      jsonConvert.convert<SaveReviewResponseBench>(json['bench']);
  if (bench != null) {
    saveReviewResponseEntity.bench = bench;
  }
  return saveReviewResponseEntity;
}

Map<String, dynamic> $SaveReviewResponseEntityToJson(
    SaveReviewResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SaveReviewResponseData $SaveReviewResponseDataFromJson(
    Map<String, dynamic> json) {
  final SaveReviewResponseData saveReviewResponseData =
      SaveReviewResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    saveReviewResponseData.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    saveReviewResponseData.id = id;
  }
  return saveReviewResponseData;
}

Map<String, dynamic> $SaveReviewResponseDataToJson(
    SaveReviewResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  return data;
}

SaveReviewResponseBench $SaveReviewResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SaveReviewResponseBench saveReviewResponseBench =
      SaveReviewResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    saveReviewResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    saveReviewResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    saveReviewResponseBench.format = format;
  }
  return saveReviewResponseBench;
}

Map<String, dynamic> $SaveReviewResponseBenchToJson(
    SaveReviewResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
