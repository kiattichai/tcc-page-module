import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/create_page_entity.dart';

CreatePageEntity $CreatePageEntityFromJson(Map<String, dynamic> json) {
	final CreatePageEntity createPageEntity = CreatePageEntity();
	final String? displayName = jsonConvert.convert<String>(json['display_name']);
	if (displayName != null) {
		createPageEntity.displayName = displayName;
	}
	final String? category = jsonConvert.convert<String>(json['category']);
	if (category != null) {
		createPageEntity.category = category;
	}
	return createPageEntity;
}

Map<String, dynamic> $CreatePageEntityToJson(CreatePageEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['display_name'] = entity.displayName;
	data['category'] = entity.category;
	return data;
}