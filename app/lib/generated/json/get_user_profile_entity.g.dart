import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/agent_withdraw_module/model/get_user_profile_entity.dart';

GetUserProfileEntity $GetUserProfileEntityFromJson(Map<String, dynamic> json) {
  final GetUserProfileEntity getUserProfileEntity = GetUserProfileEntity();
  final GetUserProfileData? data =
      jsonConvert.convert<GetUserProfileData>(json['data']);
  if (data != null) {
    getUserProfileEntity.data = data;
  }
  final GetUserProfileBench? bench =
      jsonConvert.convert<GetUserProfileBench>(json['bench']);
  if (bench != null) {
    getUserProfileEntity.bench = bench;
  }
  return getUserProfileEntity;
}

Map<String, dynamic> $GetUserProfileEntityToJson(GetUserProfileEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetUserProfileData $GetUserProfileDataFromJson(Map<String, dynamic> json) {
  final GetUserProfileData getUserProfileData = GetUserProfileData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileData.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getUserProfileData.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getUserProfileData.countryCode = countryCode;
  }
  final dynamic? displayName =
      jsonConvert.convert<dynamic>(json['display_name']);
  if (displayName != null) {
    getUserProfileData.displayName = displayName;
  }
  final GetUserProfileDataAvatar? avatar =
      jsonConvert.convert<GetUserProfileDataAvatar>(json['avatar']);
  if (avatar != null) {
    getUserProfileData.avatar = avatar;
  }
  final String? gender = jsonConvert.convert<String>(json['gender']);
  if (gender != null) {
    getUserProfileData.gender = gender;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getUserProfileData.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getUserProfileData.lastName = lastName;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getUserProfileData.email = email;
  }
  final String? idcard = jsonConvert.convert<String>(json['idcard']);
  if (idcard != null) {
    getUserProfileData.idcard = idcard;
  }
  final String? birthday = jsonConvert.convert<String>(json['birthday']);
  if (birthday != null) {
    getUserProfileData.birthday = birthday;
  }
  final bool? termsAccepted = jsonConvert.convert<bool>(json['terms_accepted']);
  if (termsAccepted != null) {
    getUserProfileData.termsAccepted = termsAccepted;
  }
  final bool? activated = jsonConvert.convert<bool>(json['activated']);
  if (activated != null) {
    getUserProfileData.activated = activated;
  }
  final String? activatedAt = jsonConvert.convert<String>(json['activated_at']);
  if (activatedAt != null) {
    getUserProfileData.activatedAt = activatedAt;
  }
  final bool? blocked = jsonConvert.convert<bool>(json['blocked']);
  if (blocked != null) {
    getUserProfileData.blocked = blocked;
  }
  final int? profileScore = jsonConvert.convert<int>(json['profile_score']);
  if (profileScore != null) {
    getUserProfileData.profileScore = profileScore;
  }
  final String? lastLoginAt =
      jsonConvert.convert<String>(json['last_login_at']);
  if (lastLoginAt != null) {
    getUserProfileData.lastLoginAt = lastLoginAt;
  }
  final String? lastLoginIp =
      jsonConvert.convert<String>(json['last_login_ip']);
  if (lastLoginIp != null) {
    getUserProfileData.lastLoginIp = lastLoginIp;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getUserProfileData.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getUserProfileData.updatedAt = updatedAt;
  }
  final String? referral = jsonConvert.convert<String>(json['referral']);
  if (referral != null) {
    getUserProfileData.referral = referral;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getUserProfileData.lang = lang;
  }
  final GetUserProfileDataWallet? wallet =
      jsonConvert.convert<GetUserProfileDataWallet>(json['wallet']);
  if (wallet != null) {
    getUserProfileData.wallet = wallet;
  }
  final GetUserProfileDataGroup? group =
      jsonConvert.convert<GetUserProfileDataGroup>(json['group']);
  if (group != null) {
    getUserProfileData.group = group;
  }
  final GetUserProfileDataAgent? agent =
      jsonConvert.convert<GetUserProfileDataAgent>(json['agent']);
  if (agent != null) {
    getUserProfileData.agent = agent;
  }
  final List<GetUserProfileDataConnects>? connects = jsonConvert
      .convertListNotNull<GetUserProfileDataConnects>(json['connects']);
  if (connects != null) {
    getUserProfileData.connects = connects;
  }
  return getUserProfileData;
}

Map<String, dynamic> $GetUserProfileDataToJson(GetUserProfileData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['display_name'] = entity.displayName;
  data['avatar'] = entity.avatar?.toJson();
  data['gender'] = entity.gender;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['email'] = entity.email;
  data['idcard'] = entity.idcard;
  data['birthday'] = entity.birthday;
  data['terms_accepted'] = entity.termsAccepted;
  data['activated'] = entity.activated;
  data['activated_at'] = entity.activatedAt;
  data['blocked'] = entity.blocked;
  data['profile_score'] = entity.profileScore;
  data['last_login_at'] = entity.lastLoginAt;
  data['last_login_ip'] = entity.lastLoginIp;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['referral'] = entity.referral;
  data['lang'] = entity.lang;
  data['wallet'] = entity.wallet?.toJson();
  data['group'] = entity.group?.toJson();
  data['agent'] = entity.agent?.toJson();
  data['connects'] = entity.connects?.map((v) => v.toJson()).toList();
  return data;
}

GetUserProfileDataAvatar $GetUserProfileDataAvatarFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAvatar getUserProfileDataAvatar =
      GetUserProfileDataAvatar();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getUserProfileDataAvatar.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAvatar.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getUserProfileDataAvatar.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getUserProfileDataAvatar.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getUserProfileDataAvatar.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getUserProfileDataAvatar.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getUserProfileDataAvatar.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getUserProfileDataAvatar.resizeUrl = resizeUrl;
  }
  return getUserProfileDataAvatar;
}

Map<String, dynamic> $GetUserProfileDataAvatarToJson(
    GetUserProfileDataAvatar entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetUserProfileDataWallet $GetUserProfileDataWalletFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataWallet getUserProfileDataWallet =
      GetUserProfileDataWallet();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataWallet.id = id;
  }
  final dynamic? amount = jsonConvert.convert<dynamic>(json['amount']);
  if (amount != null) {
    getUserProfileDataWallet.amount = amount;
  }
  return getUserProfileDataWallet;
}

Map<String, dynamic> $GetUserProfileDataWalletToJson(
    GetUserProfileDataWallet entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['amount'] = entity.amount;
  return data;
}

GetUserProfileDataGroup $GetUserProfileDataGroupFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataGroup getUserProfileDataGroup =
      GetUserProfileDataGroup();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataGroup.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataGroup.name = name;
  }
  return getUserProfileDataGroup;
}

Map<String, dynamic> $GetUserProfileDataGroupToJson(
    GetUserProfileDataGroup entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserProfileDataAgent $GetUserProfileDataAgentFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgent getUserProfileDataAgent =
      GetUserProfileDataAgent();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgent.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getUserProfileDataAgent.code = code;
  }
  final String? status = jsonConvert.convert<String>(json['status']);
  if (status != null) {
    getUserProfileDataAgent.status = status;
  }
  final GetUserProfileDataAgentDocumentStatus? documentStatus = jsonConvert
      .convert<GetUserProfileDataAgentDocumentStatus>(json['document_status']);
  if (documentStatus != null) {
    getUserProfileDataAgent.documentStatus = documentStatus;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getUserProfileDataAgent.remark = remark;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getUserProfileDataAgent.address = address;
  }
  final String? zipCode = jsonConvert.convert<String>(json['zip_code']);
  if (zipCode != null) {
    getUserProfileDataAgent.zipCode = zipCode;
  }
  final GetUserProfileDataAgentBank? bank =
      jsonConvert.convert<GetUserProfileDataAgentBank>(json['bank']);
  if (bank != null) {
    getUserProfileDataAgent.bank = bank;
  }
  final GetUserProfileDataAgentCountry? country =
      jsonConvert.convert<GetUserProfileDataAgentCountry>(json['country']);
  if (country != null) {
    getUserProfileDataAgent.country = country;
  }
  final GetUserProfileDataAgentProvince? province =
      jsonConvert.convert<GetUserProfileDataAgentProvince>(json['province']);
  if (province != null) {
    getUserProfileDataAgent.province = province;
  }
  final GetUserProfileDataAgentCity? city =
      jsonConvert.convert<GetUserProfileDataAgentCity>(json['city']);
  if (city != null) {
    getUserProfileDataAgent.city = city;
  }
  final GetUserProfileDataAgentDistrict? district =
      jsonConvert.convert<GetUserProfileDataAgentDistrict>(json['district']);
  if (district != null) {
    getUserProfileDataAgent.district = district;
  }
  return getUserProfileDataAgent;
}

Map<String, dynamic> $GetUserProfileDataAgentToJson(
    GetUserProfileDataAgent entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['document_status'] = entity.documentStatus?.toJson();
  data['remark'] = entity.remark;
  data['address'] = entity.address;
  data['zip_code'] = entity.zipCode;
  data['bank'] = entity.bank?.toJson();
  data['country'] = entity.country?.toJson();
  data['province'] = entity.province?.toJson();
  data['city'] = entity.city?.toJson();
  data['district'] = entity.district?.toJson();
  return data;
}

GetUserProfileDataAgentDocumentStatus
    $GetUserProfileDataAgentDocumentStatusFromJson(Map<String, dynamic> json) {
  final GetUserProfileDataAgentDocumentStatus
      getUserProfileDataAgentDocumentStatus =
      GetUserProfileDataAgentDocumentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentDocumentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getUserProfileDataAgentDocumentStatus.text = text;
  }
  return getUserProfileDataAgentDocumentStatus;
}

Map<String, dynamic> $GetUserProfileDataAgentDocumentStatusToJson(
    GetUserProfileDataAgentDocumentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetUserProfileDataAgentBank $GetUserProfileDataAgentBankFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgentBank getUserProfileDataAgentBank =
      GetUserProfileDataAgentBank();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentBank.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAgentBank.name = name;
  }
  final String? accountNo = jsonConvert.convert<String>(json['account_no']);
  if (accountNo != null) {
    getUserProfileDataAgentBank.accountNo = accountNo;
  }
  final String? accountName = jsonConvert.convert<String>(json['account_name']);
  if (accountName != null) {
    getUserProfileDataAgentBank.accountName = accountName;
  }
  final dynamic? accountType =
      jsonConvert.convert<dynamic>(json['account_type']);
  if (accountType != null) {
    getUserProfileDataAgentBank.accountType = accountType;
  }
  final String? branch = jsonConvert.convert<String>(json['branch']);
  if (branch != null) {
    getUserProfileDataAgentBank.branch = branch;
  }
  final String? logo = jsonConvert.convert<String>(json['logo']);
  if (logo != null) {
    getUserProfileDataAgentBank.logo = logo;
  }
  return getUserProfileDataAgentBank;
}

Map<String, dynamic> $GetUserProfileDataAgentBankToJson(
    GetUserProfileDataAgentBank entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['account_no'] = entity.accountNo;
  data['account_name'] = entity.accountName;
  data['account_type'] = entity.accountType;
  data['branch'] = entity.branch;
  data['logo'] = entity.logo;
  return data;
}

GetUserProfileDataAgentCountry $GetUserProfileDataAgentCountryFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgentCountry getUserProfileDataAgentCountry =
      GetUserProfileDataAgentCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAgentCountry.name = name;
  }
  return getUserProfileDataAgentCountry;
}

Map<String, dynamic> $GetUserProfileDataAgentCountryToJson(
    GetUserProfileDataAgentCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserProfileDataAgentProvince $GetUserProfileDataAgentProvinceFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgentProvince getUserProfileDataAgentProvince =
      GetUserProfileDataAgentProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAgentProvince.name = name;
  }
  return getUserProfileDataAgentProvince;
}

Map<String, dynamic> $GetUserProfileDataAgentProvinceToJson(
    GetUserProfileDataAgentProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserProfileDataAgentCity $GetUserProfileDataAgentCityFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgentCity getUserProfileDataAgentCity =
      GetUserProfileDataAgentCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAgentCity.name = name;
  }
  return getUserProfileDataAgentCity;
}

Map<String, dynamic> $GetUserProfileDataAgentCityToJson(
    GetUserProfileDataAgentCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserProfileDataAgentDistrict $GetUserProfileDataAgentDistrictFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataAgentDistrict getUserProfileDataAgentDistrict =
      GetUserProfileDataAgentDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataAgentDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getUserProfileDataAgentDistrict.name = name;
  }
  return getUserProfileDataAgentDistrict;
}

Map<String, dynamic> $GetUserProfileDataAgentDistrictToJson(
    GetUserProfileDataAgentDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetUserProfileDataConnects $GetUserProfileDataConnectsFromJson(
    Map<String, dynamic> json) {
  final GetUserProfileDataConnects getUserProfileDataConnects =
      GetUserProfileDataConnects();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getUserProfileDataConnects.id = id;
  }
  final String? service = jsonConvert.convert<String>(json['service']);
  if (service != null) {
    getUserProfileDataConnects.service = service;
  }
  final String? uid = jsonConvert.convert<String>(json['uid']);
  if (uid != null) {
    getUserProfileDataConnects.uid = uid;
  }
  final String? account = jsonConvert.convert<String>(json['account']);
  if (account != null) {
    getUserProfileDataConnects.account = account;
  }
  return getUserProfileDataConnects;
}

Map<String, dynamic> $GetUserProfileDataConnectsToJson(
    GetUserProfileDataConnects entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['service'] = entity.service;
  data['uid'] = entity.uid;
  data['account'] = entity.account;
  return data;
}

GetUserProfileBench $GetUserProfileBenchFromJson(Map<String, dynamic> json) {
  final GetUserProfileBench getUserProfileBench = GetUserProfileBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getUserProfileBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getUserProfileBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getUserProfileBench.format = format;
  }
  return getUserProfileBench;
}

Map<String, dynamic> $GetUserProfileBenchToJson(GetUserProfileBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
