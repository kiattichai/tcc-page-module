import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_genre_attribute_entity.dart';

GetGenreAttributeEntity $GetGenreAttributeEntityFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeEntity getGenreAttributeEntity =
      GetGenreAttributeEntity();
  final GetGenreAttributeData? data =
      jsonConvert.convert<GetGenreAttributeData>(json['data']);
  if (data != null) {
    getGenreAttributeEntity.data = data;
  }
  final GetGenreAttributeBench? bench =
      jsonConvert.convert<GetGenreAttributeBench>(json['bench']);
  if (bench != null) {
    getGenreAttributeEntity.bench = bench;
  }
  return getGenreAttributeEntity;
}

Map<String, dynamic> $GetGenreAttributeEntityToJson(
    GetGenreAttributeEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetGenreAttributeData $GetGenreAttributeDataFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeData getGenreAttributeData = GetGenreAttributeData();
  final GetGenreAttributeDataPagination? pagination =
      jsonConvert.convert<GetGenreAttributeDataPagination>(json['pagination']);
  if (pagination != null) {
    getGenreAttributeData.pagination = pagination;
  }
  final List<GetGenreAttributeDataRecord>? record = jsonConvert
      .convertListNotNull<GetGenreAttributeDataRecord>(json['record']);
  if (record != null) {
    getGenreAttributeData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getGenreAttributeData.cache = cache;
  }
  return getGenreAttributeData;
}

Map<String, dynamic> $GetGenreAttributeDataToJson(
    GetGenreAttributeData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetGenreAttributeDataPagination $GetGenreAttributeDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeDataPagination getGenreAttributeDataPagination =
      GetGenreAttributeDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getGenreAttributeDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getGenreAttributeDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getGenreAttributeDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getGenreAttributeDataPagination.total = total;
  }
  return getGenreAttributeDataPagination;
}

Map<String, dynamic> $GetGenreAttributeDataPaginationToJson(
    GetGenreAttributeDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetGenreAttributeDataRecord $GetGenreAttributeDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeDataRecord getGenreAttributeDataRecord =
      GetGenreAttributeDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getGenreAttributeDataRecord.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getGenreAttributeDataRecord.code = code;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getGenreAttributeDataRecord.position = position;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getGenreAttributeDataRecord.status = status;
  }
  final bool? require = jsonConvert.convert<bool>(json['require']);
  if (require != null) {
    getGenreAttributeDataRecord.require = require;
  }
  final int? type = jsonConvert.convert<int>(json['type']);
  if (type != null) {
    getGenreAttributeDataRecord.type = type;
  }
  final bool? filter = jsonConvert.convert<bool>(json['filter']);
  if (filter != null) {
    getGenreAttributeDataRecord.filter = filter;
  }
  final dynamic? metafield = jsonConvert.convert<dynamic>(json['metafield']);
  if (metafield != null) {
    getGenreAttributeDataRecord.metafield = metafield;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getGenreAttributeDataRecord.name = name;
  }
  final List<GetGenreAttributeDataRecordValues>? values = jsonConvert
      .convertListNotNull<GetGenreAttributeDataRecordValues>(json['values']);
  if (values != null) {
    getGenreAttributeDataRecord.values = values;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getGenreAttributeDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getGenreAttributeDataRecord.updatedAt = updatedAt;
  }
  return getGenreAttributeDataRecord;
}

Map<String, dynamic> $GetGenreAttributeDataRecordToJson(
    GetGenreAttributeDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['position'] = entity.position;
  data['status'] = entity.status;
  data['require'] = entity.require;
  data['type'] = entity.type;
  data['filter'] = entity.filter;
  data['metafield'] = entity.metafield;
  data['name'] = entity.name;
  data['values'] = entity.values?.map((v) => v.toJson()).toList();
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetGenreAttributeDataRecordValues $GetGenreAttributeDataRecordValuesFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeDataRecordValues getGenreAttributeDataRecordValues =
      GetGenreAttributeDataRecordValues();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getGenreAttributeDataRecordValues.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getGenreAttributeDataRecordValues.position = position;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getGenreAttributeDataRecordValues.status = status;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getGenreAttributeDataRecordValues.name = name;
  }
  final dynamic? image = jsonConvert.convert<dynamic>(json['image']);
  if (image != null) {
    getGenreAttributeDataRecordValues.image = image;
  }
  return getGenreAttributeDataRecordValues;
}

Map<String, dynamic> $GetGenreAttributeDataRecordValuesToJson(
    GetGenreAttributeDataRecordValues entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['status'] = entity.status;
  data['name'] = entity.name;
  data['image'] = entity.image;
  return data;
}

GetGenreAttributeBench $GetGenreAttributeBenchFromJson(
    Map<String, dynamic> json) {
  final GetGenreAttributeBench getGenreAttributeBench =
      GetGenreAttributeBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getGenreAttributeBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getGenreAttributeBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getGenreAttributeBench.format = format;
  }
  return getGenreAttributeBench;
}

Map<String, dynamic> $GetGenreAttributeBenchToJson(
    GetGenreAttributeBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
