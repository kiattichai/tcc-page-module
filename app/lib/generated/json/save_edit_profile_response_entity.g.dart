import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_edit_profile_response_entity.dart';

SaveEditProfileResponseEntity $SaveEditProfileResponseEntityFromJson(Map<String, dynamic> json) {
	final SaveEditProfileResponseEntity saveEditProfileResponseEntity = SaveEditProfileResponseEntity();
	final SaveEditProfileResponseData? data = jsonConvert.convert<SaveEditProfileResponseData>(json['data']);
	if (data != null) {
		saveEditProfileResponseEntity.data = data;
	}
	final SaveEditProfileResponseBench? bench = jsonConvert.convert<SaveEditProfileResponseBench>(json['bench']);
	if (bench != null) {
		saveEditProfileResponseEntity.bench = bench;
	}
	return saveEditProfileResponseEntity;
}

Map<String, dynamic> $SaveEditProfileResponseEntityToJson(SaveEditProfileResponseEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] = entity.data?.toJson();
	data['bench'] = entity.bench?.toJson();
	return data;
}

SaveEditProfileResponseData $SaveEditProfileResponseDataFromJson(Map<String, dynamic> json) {
	final SaveEditProfileResponseData saveEditProfileResponseData = SaveEditProfileResponseData();
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		saveEditProfileResponseData.message = message;
	}
	return saveEditProfileResponseData;
}

Map<String, dynamic> $SaveEditProfileResponseDataToJson(SaveEditProfileResponseData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['message'] = entity.message;
	return data;
}

SaveEditProfileResponseBench $SaveEditProfileResponseBenchFromJson(Map<String, dynamic> json) {
	final SaveEditProfileResponseBench saveEditProfileResponseBench = SaveEditProfileResponseBench();
	final int? second = jsonConvert.convert<int>(json['second']);
	if (second != null) {
		saveEditProfileResponseBench.second = second;
	}
	final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
	if (millisecond != null) {
		saveEditProfileResponseBench.millisecond = millisecond;
	}
	final String? format = jsonConvert.convert<String>(json['format']);
	if (format != null) {
		saveEditProfileResponseBench.format = format;
	}
	return saveEditProfileResponseBench;
}

Map<String, dynamic> $SaveEditProfileResponseBenchToJson(SaveEditProfileResponseBench entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}