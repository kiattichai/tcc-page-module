import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_ticket_scan_entity.dart';

GetTicketScanEntity $GetTicketScanEntityFromJson(Map<String, dynamic> json) {
  final GetTicketScanEntity getTicketScanEntity = GetTicketScanEntity();
  final GetTicketScanData? data =
      jsonConvert.convert<GetTicketScanData>(json['data']);
  if (data != null) {
    getTicketScanEntity.data = data;
  }
  final GetTicketScanBench? bench =
      jsonConvert.convert<GetTicketScanBench>(json['bench']);
  if (bench != null) {
    getTicketScanEntity.bench = bench;
  }
  return getTicketScanEntity;
}

Map<String, dynamic> $GetTicketScanEntityToJson(GetTicketScanEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetTicketScanData $GetTicketScanDataFromJson(Map<String, dynamic> json) {
  final GetTicketScanData getTicketScanData = GetTicketScanData();
  final GetTicketScanDataPagination? pagination =
      jsonConvert.convert<GetTicketScanDataPagination>(json['pagination']);
  if (pagination != null) {
    getTicketScanData.pagination = pagination;
  }
  final List<GetTicketScanDataRecord>? record =
      jsonConvert.convertListNotNull<GetTicketScanDataRecord>(json['record']);
  if (record != null) {
    getTicketScanData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getTicketScanData.cache = cache;
  }
  return getTicketScanData;
}

Map<String, dynamic> $GetTicketScanDataToJson(GetTicketScanData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetTicketScanDataPagination $GetTicketScanDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetTicketScanDataPagination getTicketScanDataPagination =
      GetTicketScanDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getTicketScanDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getTicketScanDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getTicketScanDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getTicketScanDataPagination.total = total;
  }
  return getTicketScanDataPagination;
}

Map<String, dynamic> $GetTicketScanDataPaginationToJson(
    GetTicketScanDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetTicketScanDataRecord $GetTicketScanDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetTicketScanDataRecord getTicketScanDataRecord =
      GetTicketScanDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getTicketScanDataRecord.id = id;
  }
  final String? token = jsonConvert.convert<String>(json['token']);
  if (token != null) {
    getTicketScanDataRecord.token = token;
  }
  final String? note = jsonConvert.convert<String>(json['note']);
  if (note != null) {
    getTicketScanDataRecord.note = note;
  }
  final String? lastLoginAt =
      jsonConvert.convert<String>(json['last_login_at']);
  if (lastLoginAt != null) {
    getTicketScanDataRecord.lastLoginAt = lastLoginAt;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getTicketScanDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getTicketScanDataRecord.updatedAt = updatedAt;
  }
  return getTicketScanDataRecord;
}

Map<String, dynamic> $GetTicketScanDataRecordToJson(
    GetTicketScanDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['token'] = entity.token;
  data['note'] = entity.note;
  data['last_login_at'] = entity.lastLoginAt;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetTicketScanBench $GetTicketScanBenchFromJson(Map<String, dynamic> json) {
  final GetTicketScanBench getTicketScanBench = GetTicketScanBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getTicketScanBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getTicketScanBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getTicketScanBench.format = format;
  }
  return getTicketScanBench;
}

Map<String, dynamic> $GetTicketScanBenchToJson(GetTicketScanBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
