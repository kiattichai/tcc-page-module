import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_document_ticket_entity.dart';

GetDocumentTicketEntity $GetDocumentTicketEntityFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketEntity getDocumentTicketEntity =
      GetDocumentTicketEntity();
  final GetDocumentTicketData? data =
      jsonConvert.convert<GetDocumentTicketData>(json['data']);
  if (data != null) {
    getDocumentTicketEntity.data = data;
  }
  final GetDocumentTicketBench? bench =
      jsonConvert.convert<GetDocumentTicketBench>(json['bench']);
  if (bench != null) {
    getDocumentTicketEntity.bench = bench;
  }
  return getDocumentTicketEntity;
}

Map<String, dynamic> $GetDocumentTicketEntityToJson(
    GetDocumentTicketEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetDocumentTicketData $GetDocumentTicketDataFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketData getDocumentTicketData = GetDocumentTicketData();
  final List<GetDocumentTicketDataRecord>? record = jsonConvert
      .convertListNotNull<GetDocumentTicketDataRecord>(json['record']);
  if (record != null) {
    getDocumentTicketData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getDocumentTicketData.cache = cache;
  }
  return getDocumentTicketData;
}

Map<String, dynamic> $GetDocumentTicketDataToJson(
    GetDocumentTicketData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetDocumentTicketDataRecord $GetDocumentTicketDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketDataRecord getDocumentTicketDataRecord =
      GetDocumentTicketDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDocumentTicketDataRecord.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    getDocumentTicketDataRecord.storeId = storeId;
  }
  final String? title = jsonConvert.convert<String>(json['title']);
  if (title != null) {
    getDocumentTicketDataRecord.title = title;
  }
  final String? fileType = jsonConvert.convert<String>(json['file_type']);
  if (fileType != null) {
    getDocumentTicketDataRecord.fileType = fileType;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    getDocumentTicketDataRecord.type = type;
  }
  final GetDocumentTicketDataRecordStatus? status =
      jsonConvert.convert<GetDocumentTicketDataRecordStatus>(json['status']);
  if (status != null) {
    getDocumentTicketDataRecord.status = status;
  }
  final String? remark = jsonConvert.convert<String>(json['remark']);
  if (remark != null) {
    getDocumentTicketDataRecord.remark = remark;
  }
  final GetDocumentTicketDataRecordUser? user =
      jsonConvert.convert<GetDocumentTicketDataRecordUser>(json['user']);
  if (user != null) {
    getDocumentTicketDataRecord.user = user;
  }
  final GetDocumentTicketDataRecordStaff? staff =
      jsonConvert.convert<GetDocumentTicketDataRecordStaff>(json['staff']);
  if (staff != null) {
    getDocumentTicketDataRecord.staff = staff;
  }
  final List<GetDocumentTicketDataRecordImages>? images = jsonConvert
      .convertListNotNull<GetDocumentTicketDataRecordImages>(json['images']);
  if (images != null) {
    getDocumentTicketDataRecord.images = images;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getDocumentTicketDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getDocumentTicketDataRecord.updatedAt = updatedAt;
  }
  return getDocumentTicketDataRecord;
}

Map<String, dynamic> $GetDocumentTicketDataRecordToJson(
    GetDocumentTicketDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['title'] = entity.title;
  data['file_type'] = entity.fileType;
  data['type'] = entity.type;
  data['status'] = entity.status?.toJson();
  data['remark'] = entity.remark;
  data['user'] = entity.user?.toJson();
  data['staff'] = entity.staff?.toJson();
  data['images'] = entity.images?.map((v) => v.toJson()).toList();
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetDocumentTicketDataRecordStatus $GetDocumentTicketDataRecordStatusFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketDataRecordStatus getDocumentTicketDataRecordStatus =
      GetDocumentTicketDataRecordStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDocumentTicketDataRecordStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getDocumentTicketDataRecordStatus.text = text;
  }
  return getDocumentTicketDataRecordStatus;
}

Map<String, dynamic> $GetDocumentTicketDataRecordStatusToJson(
    GetDocumentTicketDataRecordStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetDocumentTicketDataRecordUser $GetDocumentTicketDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketDataRecordUser getDocumentTicketDataRecordUser =
      GetDocumentTicketDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getDocumentTicketDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getDocumentTicketDataRecordUser.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getDocumentTicketDataRecordUser.countryCode = countryCode;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getDocumentTicketDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getDocumentTicketDataRecordUser.lastName = lastName;
  }
  return getDocumentTicketDataRecordUser;
}

Map<String, dynamic> $GetDocumentTicketDataRecordUserToJson(
    GetDocumentTicketDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  return data;
}

GetDocumentTicketDataRecordStaff $GetDocumentTicketDataRecordStaffFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketDataRecordStaff getDocumentTicketDataRecordStaff =
      GetDocumentTicketDataRecordStaff();
  return getDocumentTicketDataRecordStaff;
}

Map<String, dynamic> $GetDocumentTicketDataRecordStaffToJson(
    GetDocumentTicketDataRecordStaff entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetDocumentTicketDataRecordImages $GetDocumentTicketDataRecordImagesFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketDataRecordImages getDocumentTicketDataRecordImages =
      GetDocumentTicketDataRecordImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getDocumentTicketDataRecordImages.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getDocumentTicketDataRecordImages.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getDocumentTicketDataRecordImages.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getDocumentTicketDataRecordImages.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getDocumentTicketDataRecordImages.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getDocumentTicketDataRecordImages.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getDocumentTicketDataRecordImages.url = url;
  }
  return getDocumentTicketDataRecordImages;
}

Map<String, dynamic> $GetDocumentTicketDataRecordImagesToJson(
    GetDocumentTicketDataRecordImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetDocumentTicketBench $GetDocumentTicketBenchFromJson(
    Map<String, dynamic> json) {
  final GetDocumentTicketBench getDocumentTicketBench =
      GetDocumentTicketBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getDocumentTicketBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getDocumentTicketBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getDocumentTicketBench.format = format;
  }
  return getDocumentTicketBench;
}

Map<String, dynamic> $GetDocumentTicketBenchToJson(
    GetDocumentTicketBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
