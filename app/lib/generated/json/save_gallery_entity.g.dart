import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_gallery_entity.dart';

SaveGalleryEntity $SaveGalleryEntityFromJson(Map<String, dynamic> json) {
  final SaveGalleryEntity saveGalleryEntity = SaveGalleryEntity();
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    saveGalleryEntity.description = description;
  }
  final String? fileData = jsonConvert.convert<String>(json['file_data']);
  if (fileData != null) {
    saveGalleryEntity.fileData = fileData;
  }
  final String? fileName = jsonConvert.convert<String>(json['file_name']);
  if (fileName != null) {
    saveGalleryEntity.fileName = fileName;
  }
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    saveGalleryEntity.storeId = storeId;
  }
  return saveGalleryEntity;
}

Map<String, dynamic> $SaveGalleryEntityToJson(SaveGalleryEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['description'] = entity.description;
  data['file_data'] = entity.fileData;
  data['file_name'] = entity.fileName;
  data['store_id'] = entity.storeId;
  return data;
}
