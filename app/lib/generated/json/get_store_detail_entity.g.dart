import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_store_detail_entity.dart';

GetStoreDetailEntity $GetStoreDetailEntityFromJson(Map<String, dynamic> json) {
  final GetStoreDetailEntity getStoreDetailEntity = GetStoreDetailEntity();
  final GetStoreDetailData? data =
      jsonConvert.convert<GetStoreDetailData>(json['data']);
  if (data != null) {
    getStoreDetailEntity.data = data;
  }
  final GetStoreDetailBench? bench =
      jsonConvert.convert<GetStoreDetailBench>(json['bench']);
  if (bench != null) {
    getStoreDetailEntity.bench = bench;
  }
  return getStoreDetailEntity;
}

Map<String, dynamic> $GetStoreDetailEntityToJson(GetStoreDetailEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetStoreDetailData $GetStoreDetailDataFromJson(Map<String, dynamic> json) {
  final GetStoreDetailData getStoreDetailData = GetStoreDetailData();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailData.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailData.name = name;
  }
  final String? slug = jsonConvert.convert<String>(json['slug']);
  if (slug != null) {
    getStoreDetailData.slug = slug;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getStoreDetailData.description = description;
  }
  final GetStoreDetailDataType? type =
      jsonConvert.convert<GetStoreDetailDataType>(json['type']);
  if (type != null) {
    getStoreDetailData.type = type;
  }
  final GetStoreDetailDataSection? section =
      jsonConvert.convert<GetStoreDetailDataSection>(json['section']);
  if (section != null) {
    getStoreDetailData.section = section;
  }
  final GetStoreDetailDataContact? contact =
      jsonConvert.convert<GetStoreDetailDataContact>(json['contact']);
  if (contact != null) {
    getStoreDetailData.contact = contact;
  }
  final GetStoreDetailDataCorporate? corporate =
      jsonConvert.convert<GetStoreDetailDataCorporate>(json['corporate']);
  if (corporate != null) {
    getStoreDetailData.corporate = corporate;
  }
  final GetStoreDetailDataBilling? billing =
      jsonConvert.convert<GetStoreDetailDataBilling>(json['billing']);
  if (billing != null) {
    getStoreDetailData.billing = billing;
  }
  final bool? acceptTerms = jsonConvert.convert<bool>(json['accept_terms']);
  if (acceptTerms != null) {
    getStoreDetailData.acceptTerms = acceptTerms;
  }
  final GetStoreDetailDataDocumentStatus? documentStatus = jsonConvert
      .convert<GetStoreDetailDataDocumentStatus>(json['document_status']);
  if (documentStatus != null) {
    getStoreDetailData.documentStatus = documentStatus;
  }
  final GetStoreDetailDataPaymentStatus? paymentStatus = jsonConvert
      .convert<GetStoreDetailDataPaymentStatus>(json['payment_status']);
  if (paymentStatus != null) {
    getStoreDetailData.paymentStatus = paymentStatus;
  }
  final GetStoreDetailDataPublishStatus? publishStatus = jsonConvert
      .convert<GetStoreDetailDataPublishStatus>(json['publish_status']);
  if (publishStatus != null) {
    getStoreDetailData.publishStatus = publishStatus;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getStoreDetailData.status = status;
  }
  final GetStoreDetailDataCreatedAt? createdAt =
      jsonConvert.convert<GetStoreDetailDataCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getStoreDetailData.createdAt = createdAt;
  }
  final GetStoreDetailDataUpdatedAt? updatedAt =
      jsonConvert.convert<GetStoreDetailDataUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getStoreDetailData.updatedAt = updatedAt;
  }
  final GetStoreDetailDataPublishedAt? publishedAt =
      jsonConvert.convert<GetStoreDetailDataPublishedAt>(json['published_at']);
  if (publishedAt != null) {
    getStoreDetailData.publishedAt = publishedAt;
  }
  final List<GetStoreDetailDataSettings>? settings = jsonConvert
      .convertListNotNull<GetStoreDetailDataSettings>(json['settings']);
  if (settings != null) {
    getStoreDetailData.settings = settings;
  }
  final GetStoreDetailDataImages? images =
      jsonConvert.convert<GetStoreDetailDataImages>(json['images']);
  if (images != null) {
    getStoreDetailData.images = images;
  }
  final List<GetStoreDetailDataConnects>? connects = jsonConvert
      .convertListNotNull<GetStoreDetailDataConnects>(json['connects']);
  if (connects != null) {
    getStoreDetailData.connects = connects;
  }
  final GetStoreDetailDataStaff? staff =
      jsonConvert.convert<GetStoreDetailDataStaff>(json['staff']);
  if (staff != null) {
    getStoreDetailData.staff = staff;
  }
  final GetStoreDetailDataVenue? venue =
      jsonConvert.convert<GetStoreDetailDataVenue>(json['venue']);
  if (venue != null) {
    getStoreDetailData.venue = venue;
  }
  final List<GetStoreDetailDataAttributes>? attributes = jsonConvert
      .convertListNotNull<GetStoreDetailDataAttributes>(json['attributes']);
  if (attributes != null) {
    getStoreDetailData.attributes = attributes;
  }
  final List<GetStoreDetailDataTimes>? times =
      jsonConvert.convertListNotNull<GetStoreDetailDataTimes>(json['times']);
  if (times != null) {
    getStoreDetailData.times = times;
  }
  final List<String>? timesDisplay =
      jsonConvert.convertListNotNull<String>(json['times_display']);
  if (timesDisplay != null) {
    getStoreDetailData.timesDisplay = timesDisplay;
  }
  final GetStoreDetailDataTimeOpen? timeOpen =
      jsonConvert.convert<GetStoreDetailDataTimeOpen>(json['time_open']);
  if (timeOpen != null) {
    getStoreDetailData.timeOpen = timeOpen;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getStoreDetailData.remark = remark;
  }
  final bool? updated = jsonConvert.convert<bool>(json['updated']);
  if (updated != null) {
    getStoreDetailData.updated = updated;
  }
  final bool? acceptFee = jsonConvert.convert<bool>(json['accept_fee']);
  if (acceptFee != null) {
    getStoreDetailData.acceptFee = acceptFee;
  }
  final GetStoreDetailDataAcceptFeeAt? acceptFeeAt =
      jsonConvert.convert<GetStoreDetailDataAcceptFeeAt>(json['accept_fee_at']);
  if (acceptFeeAt != null) {
    getStoreDetailData.acceptFeeAt = acceptFeeAt;
  }
  final int? averageRating = jsonConvert.convert<int>(json['average_rating']);
  if (averageRating != null) {
    getStoreDetailData.averageRating = averageRating;
  }
  final int? reviewTotal = jsonConvert.convert<int>(json['review_total']);
  if (reviewTotal != null) {
    getStoreDetailData.reviewTotal = reviewTotal;
  }
  final bool? covidVerify = jsonConvert.convert<bool>(json['covid_verify']);
  if (covidVerify != null) {
    getStoreDetailData.covidVerify = covidVerify;
  }
  final bool? isShaPlus = jsonConvert.convert<bool>(json['is_sha_plus']);
  if (isShaPlus != null) {
    getStoreDetailData.isShaPlus = isShaPlus;
  }
  final bool? isCovidFree = jsonConvert.convert<bool>(json['is_covid_free']);
  if (isCovidFree != null) {
    getStoreDetailData.isCovidFree = isCovidFree;
  }
  final GetStoreDetailDataParking? parking =
      jsonConvert.convert<GetStoreDetailDataParking>(json['parking']);
  if (parking != null) {
    getStoreDetailData.parking = parking;
  }
  final GetStoreDetailDataSeats? seats =
      jsonConvert.convert<GetStoreDetailDataSeats>(json['seats']);
  if (seats != null) {
    getStoreDetailData.seats = seats;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getStoreDetailData.cache = cache;
  }
  return getStoreDetailData;
}

Map<String, dynamic> $GetStoreDetailDataToJson(GetStoreDetailData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['slug'] = entity.slug;
  data['description'] = entity.description;
  data['type'] = entity.type?.toJson();
  data['section'] = entity.section?.toJson();
  data['contact'] = entity.contact?.toJson();
  data['corporate'] = entity.corporate?.toJson();
  data['billing'] = entity.billing?.toJson();
  data['accept_terms'] = entity.acceptTerms;
  data['document_status'] = entity.documentStatus?.toJson();
  data['payment_status'] = entity.paymentStatus?.toJson();
  data['publish_status'] = entity.publishStatus?.toJson();
  data['status'] = entity.status;
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  data['published_at'] = entity.publishedAt?.toJson();
  data['settings'] = entity.settings?.map((v) => v.toJson()).toList();
  data['images'] = entity.images?.toJson();
  data['connects'] = entity.connects?.map((v) => v.toJson()).toList();
  data['staff'] = entity.staff?.toJson();
  data['venue'] = entity.venue?.toJson();
  data['attributes'] = entity.attributes?.map((v) => v.toJson()).toList();
  data['times'] = entity.times?.map((v) => v.toJson()).toList();
  data['times_display'] = entity.timesDisplay;
  data['time_open'] = entity.timeOpen?.toJson();
  data['remark'] = entity.remark;
  data['updated'] = entity.updated;
  data['accept_fee'] = entity.acceptFee;
  data['accept_fee_at'] = entity.acceptFeeAt?.toJson();
  data['average_rating'] = entity.averageRating;
  data['review_total'] = entity.reviewTotal;
  data['covid_verify'] = entity.covidVerify;
  data['is_sha_plus'] = entity.isShaPlus;
  data['is_covid_free'] = entity.isCovidFree;
  data['parking'] = entity.parking?.toJson();
  data['seats'] = entity.seats?.toJson();
  data['cache'] = entity.cache;
  return data;
}

GetStoreDetailDataType $GetStoreDetailDataTypeFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataType getStoreDetailDataType =
      GetStoreDetailDataType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataType.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataType.text = text;
  }
  return getStoreDetailDataType;
}

Map<String, dynamic> $GetStoreDetailDataTypeToJson(
    GetStoreDetailDataType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataSection $GetStoreDetailDataSectionFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataSection getStoreDetailDataSection =
      GetStoreDetailDataSection();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataSection.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataSection.text = text;
  }
  final String? textFull = jsonConvert.convert<String>(json['text_full']);
  if (textFull != null) {
    getStoreDetailDataSection.textFull = textFull;
  }
  return getStoreDetailDataSection;
}

Map<String, dynamic> $GetStoreDetailDataSectionToJson(
    GetStoreDetailDataSection entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  data['text_full'] = entity.textFull;
  return data;
}

GetStoreDetailDataContact $GetStoreDetailDataContactFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataContact getStoreDetailDataContact =
      GetStoreDetailDataContact();
  final dynamic? firstName = jsonConvert.convert<dynamic>(json['first_name']);
  if (firstName != null) {
    getStoreDetailDataContact.firstName = firstName;
  }
  final dynamic? lastName = jsonConvert.convert<dynamic>(json['last_name']);
  if (lastName != null) {
    getStoreDetailDataContact.lastName = lastName;
  }
  final dynamic? citizenId = jsonConvert.convert<dynamic>(json['citizen_id']);
  if (citizenId != null) {
    getStoreDetailDataContact.citizenId = citizenId;
  }
  final String? phone = jsonConvert.convert<String>(json['phone']);
  if (phone != null) {
    getStoreDetailDataContact.phone = phone;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getStoreDetailDataContact.countryCode = countryCode;
  }
  final List<dynamic>? phoneOption =
      jsonConvert.convertListNotNull<dynamic>(json['phone_option']);
  if (phoneOption != null) {
    getStoreDetailDataContact.phoneOption = phoneOption;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    getStoreDetailDataContact.email = email;
  }
  return getStoreDetailDataContact;
}

Map<String, dynamic> $GetStoreDetailDataContactToJson(
    GetStoreDetailDataContact entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['citizen_id'] = entity.citizenId;
  data['phone'] = entity.phone;
  data['country_code'] = entity.countryCode;
  data['phone_option'] = entity.phoneOption;
  data['email'] = entity.email;
  return data;
}

GetStoreDetailDataCorporate $GetStoreDetailDataCorporateFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataCorporate getStoreDetailDataCorporate =
      GetStoreDetailDataCorporate();
  final dynamic? name = jsonConvert.convert<dynamic>(json['name']);
  if (name != null) {
    getStoreDetailDataCorporate.name = name;
  }
  final dynamic? branch = jsonConvert.convert<dynamic>(json['branch']);
  if (branch != null) {
    getStoreDetailDataCorporate.branch = branch;
  }
  final dynamic? phone = jsonConvert.convert<dynamic>(json['phone']);
  if (phone != null) {
    getStoreDetailDataCorporate.phone = phone;
  }
  final dynamic? taxId = jsonConvert.convert<dynamic>(json['tax_id']);
  if (taxId != null) {
    getStoreDetailDataCorporate.taxId = taxId;
  }
  return getStoreDetailDataCorporate;
}

Map<String, dynamic> $GetStoreDetailDataCorporateToJson(
    GetStoreDetailDataCorporate entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['branch'] = entity.branch;
  data['phone'] = entity.phone;
  data['tax_id'] = entity.taxId;
  return data;
}

GetStoreDetailDataBilling $GetStoreDetailDataBillingFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataBilling getStoreDetailDataBilling =
      GetStoreDetailDataBilling();
  final dynamic? address = jsonConvert.convert<dynamic>(json['address']);
  if (address != null) {
    getStoreDetailDataBilling.address = address;
  }
  final dynamic? district = jsonConvert.convert<dynamic>(json['district']);
  if (district != null) {
    getStoreDetailDataBilling.district = district;
  }
  final dynamic? city = jsonConvert.convert<dynamic>(json['city']);
  if (city != null) {
    getStoreDetailDataBilling.city = city;
  }
  final dynamic? province = jsonConvert.convert<dynamic>(json['province']);
  if (province != null) {
    getStoreDetailDataBilling.province = province;
  }
  final dynamic? country = jsonConvert.convert<dynamic>(json['country']);
  if (country != null) {
    getStoreDetailDataBilling.country = country;
  }
  final dynamic? zipcode = jsonConvert.convert<dynamic>(json['zipcode']);
  if (zipcode != null) {
    getStoreDetailDataBilling.zipcode = zipcode;
  }
  return getStoreDetailDataBilling;
}

Map<String, dynamic> $GetStoreDetailDataBillingToJson(
    GetStoreDetailDataBilling entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['address'] = entity.address;
  data['district'] = entity.district;
  data['city'] = entity.city;
  data['province'] = entity.province;
  data['country'] = entity.country;
  data['zipcode'] = entity.zipcode;
  return data;
}

GetStoreDetailDataDocumentStatus $GetStoreDetailDataDocumentStatusFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataDocumentStatus getStoreDetailDataDocumentStatus =
      GetStoreDetailDataDocumentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataDocumentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataDocumentStatus.text = text;
  }
  return getStoreDetailDataDocumentStatus;
}

Map<String, dynamic> $GetStoreDetailDataDocumentStatusToJson(
    GetStoreDetailDataDocumentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataPaymentStatus $GetStoreDetailDataPaymentStatusFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataPaymentStatus getStoreDetailDataPaymentStatus =
      GetStoreDetailDataPaymentStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataPaymentStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataPaymentStatus.text = text;
  }
  return getStoreDetailDataPaymentStatus;
}

Map<String, dynamic> $GetStoreDetailDataPaymentStatusToJson(
    GetStoreDetailDataPaymentStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataPublishStatus $GetStoreDetailDataPublishStatusFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataPublishStatus getStoreDetailDataPublishStatus =
      GetStoreDetailDataPublishStatus();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataPublishStatus.id = id;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataPublishStatus.text = text;
  }
  return getStoreDetailDataPublishStatus;
}

Map<String, dynamic> $GetStoreDetailDataPublishStatusToJson(
    GetStoreDetailDataPublishStatus entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataCreatedAt $GetStoreDetailDataCreatedAtFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataCreatedAt getStoreDetailDataCreatedAt =
      GetStoreDetailDataCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getStoreDetailDataCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getStoreDetailDataCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getStoreDetailDataCreatedAt.time = time;
  }
  return getStoreDetailDataCreatedAt;
}

Map<String, dynamic> $GetStoreDetailDataCreatedAtToJson(
    GetStoreDetailDataCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetStoreDetailDataUpdatedAt $GetStoreDetailDataUpdatedAtFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataUpdatedAt getStoreDetailDataUpdatedAt =
      GetStoreDetailDataUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getStoreDetailDataUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getStoreDetailDataUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getStoreDetailDataUpdatedAt.time = time;
  }
  final String? valueUtc = jsonConvert.convert<String>(json['value_utc']);
  if (valueUtc != null) {
    getStoreDetailDataUpdatedAt.valueUtc = valueUtc;
  }
  return getStoreDetailDataUpdatedAt;
}

Map<String, dynamic> $GetStoreDetailDataUpdatedAtToJson(
    GetStoreDetailDataUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  data['value_utc'] = entity.valueUtc;
  return data;
}

GetStoreDetailDataPublishedAt $GetStoreDetailDataPublishedAtFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataPublishedAt getStoreDetailDataPublishedAt =
      GetStoreDetailDataPublishedAt();
  return getStoreDetailDataPublishedAt;
}

Map<String, dynamic> $GetStoreDetailDataPublishedAtToJson(
    GetStoreDetailDataPublishedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetStoreDetailDataSettings $GetStoreDetailDataSettingsFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataSettings getStoreDetailDataSettings =
      GetStoreDetailDataSettings();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataSettings.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getStoreDetailDataSettings.code = code;
  }
  final String? key = jsonConvert.convert<String>(json['key']);
  if (key != null) {
    getStoreDetailDataSettings.key = key;
  }
  final GetStoreDetailDataSettingsValue? value =
      jsonConvert.convert<GetStoreDetailDataSettingsValue>(json['value']);
  if (value != null) {
    getStoreDetailDataSettings.value = value;
  }
  final bool? serialize = jsonConvert.convert<bool>(json['serialize']);
  if (serialize != null) {
    getStoreDetailDataSettings.serialize = serialize;
  }
  return getStoreDetailDataSettings;
}

Map<String, dynamic> $GetStoreDetailDataSettingsToJson(
    GetStoreDetailDataSettings entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['key'] = entity.key;
  data['value'] = entity.value?.toJson();
  data['serialize'] = entity.serialize;
  return data;
}

GetStoreDetailDataSettingsValue $GetStoreDetailDataSettingsValueFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataSettingsValue getStoreDetailDataSettingsValue =
      GetStoreDetailDataSettingsValue();
  final String? website = jsonConvert.convert<String>(json['website']);
  if (website != null) {
    getStoreDetailDataSettingsValue.website = website;
  }
  final String? facebook = jsonConvert.convert<String>(json['facebook']);
  if (facebook != null) {
    getStoreDetailDataSettingsValue.facebook = facebook;
  }
  final String? twitter = jsonConvert.convert<String>(json['twitter']);
  if (twitter != null) {
    getStoreDetailDataSettingsValue.twitter = twitter;
  }
  final String? line = jsonConvert.convert<String>(json['line']);
  if (line != null) {
    getStoreDetailDataSettingsValue.line = line;
  }
  final String? instagram = jsonConvert.convert<String>(json['instagram']);
  if (instagram != null) {
    getStoreDetailDataSettingsValue.instagram = instagram;
  }
  return getStoreDetailDataSettingsValue;
}

Map<String, dynamic> $GetStoreDetailDataSettingsValueToJson(
    GetStoreDetailDataSettingsValue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['website'] = entity.website;
  data['facebook'] = entity.facebook;
  data['twitter'] = entity.twitter;
  data['line'] = entity.line;
  data['instagram'] = entity.instagram;
  return data;
}

GetStoreDetailDataImages $GetStoreDetailDataImagesFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataImages getStoreDetailDataImages =
      GetStoreDetailDataImages();
  final GetStoreDetailDataImagesLogo? logo =
      jsonConvert.convert<GetStoreDetailDataImagesLogo>(json['logo']);
  if (logo != null) {
    getStoreDetailDataImages.logo = logo;
  }
  final List<GetStoreDetailDataImagesBanner>? banner = jsonConvert
      .convertListNotNull<GetStoreDetailDataImagesBanner>(json['banner']);
  if (banner != null) {
    getStoreDetailDataImages.banner = banner;
  }
  final dynamic? corporateLogo =
      jsonConvert.convert<dynamic>(json['corporate_logo']);
  if (corporateLogo != null) {
    getStoreDetailDataImages.corporateLogo = corporateLogo;
  }
  final dynamic? signature = jsonConvert.convert<dynamic>(json['signature']);
  if (signature != null) {
    getStoreDetailDataImages.signature = signature;
  }
  return getStoreDetailDataImages;
}

Map<String, dynamic> $GetStoreDetailDataImagesToJson(
    GetStoreDetailDataImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['logo'] = entity.logo?.toJson();
  data['banner'] = entity.banner?.map((v) => v.toJson()).toList();
  data['corporate_logo'] = entity.corporateLogo;
  data['signature'] = entity.signature;
  return data;
}

GetStoreDetailDataImagesLogo $GetStoreDetailDataImagesLogoFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataImagesLogo getStoreDetailDataImagesLogo =
      GetStoreDetailDataImagesLogo();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getStoreDetailDataImagesLogo.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getStoreDetailDataImagesLogo.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getStoreDetailDataImagesLogo.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getStoreDetailDataImagesLogo.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataImagesLogo.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getStoreDetailDataImagesLogo.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getStoreDetailDataImagesLogo.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getStoreDetailDataImagesLogo.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getStoreDetailDataImagesLogo.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getStoreDetailDataImagesLogo.url = url;
  }
  return getStoreDetailDataImagesLogo;
}

Map<String, dynamic> $GetStoreDetailDataImagesLogoToJson(
    GetStoreDetailDataImagesLogo entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetStoreDetailDataImagesBanner $GetStoreDetailDataImagesBannerFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataImagesBanner getStoreDetailDataImagesBanner =
      GetStoreDetailDataImagesBanner();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getStoreDetailDataImagesBanner.id = id;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getStoreDetailDataImagesBanner.position = position;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getStoreDetailDataImagesBanner.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    getStoreDetailDataImagesBanner.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataImagesBanner.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getStoreDetailDataImagesBanner.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getStoreDetailDataImagesBanner.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getStoreDetailDataImagesBanner.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getStoreDetailDataImagesBanner.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getStoreDetailDataImagesBanner.url = url;
  }
  return getStoreDetailDataImagesBanner;
}

Map<String, dynamic> $GetStoreDetailDataImagesBannerToJson(
    GetStoreDetailDataImagesBanner entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['position'] = entity.position;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  return data;
}

GetStoreDetailDataConnects $GetStoreDetailDataConnectsFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataConnects getStoreDetailDataConnects =
      GetStoreDetailDataConnects();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataConnects.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getStoreDetailDataConnects.username = username;
  }
  final String? countryCode = jsonConvert.convert<String>(json['country_code']);
  if (countryCode != null) {
    getStoreDetailDataConnects.countryCode = countryCode;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getStoreDetailDataConnects.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getStoreDetailDataConnects.lastName = lastName;
  }
  return getStoreDetailDataConnects;
}

Map<String, dynamic> $GetStoreDetailDataConnectsToJson(
    GetStoreDetailDataConnects entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['country_code'] = entity.countryCode;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  return data;
}

GetStoreDetailDataStaff $GetStoreDetailDataStaffFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataStaff getStoreDetailDataStaff =
      GetStoreDetailDataStaff();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataStaff.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getStoreDetailDataStaff.username = username;
  }
  final dynamic? phone = jsonConvert.convert<dynamic>(json['phone']);
  if (phone != null) {
    getStoreDetailDataStaff.phone = phone;
  }
  return getStoreDetailDataStaff;
}

Map<String, dynamic> $GetStoreDetailDataStaffToJson(
    GetStoreDetailDataStaff entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['phone'] = entity.phone;
  return data;
}

GetStoreDetailDataVenue $GetStoreDetailDataVenueFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataVenue getStoreDetailDataVenue =
      GetStoreDetailDataVenue();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataVenue.id = id;
  }
  final double? lat = jsonConvert.convert<double>(json['lat']);
  if (lat != null) {
    getStoreDetailDataVenue.lat = lat;
  }
  final double? long = jsonConvert.convert<double>(json['long']);
  if (long != null) {
    getStoreDetailDataVenue.long = long;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataVenue.name = name;
  }
  final String? address = jsonConvert.convert<String>(json['address']);
  if (address != null) {
    getStoreDetailDataVenue.address = address;
  }
  final GetStoreDetailDataVenueCountry? country =
      jsonConvert.convert<GetStoreDetailDataVenueCountry>(json['country']);
  if (country != null) {
    getStoreDetailDataVenue.country = country;
  }
  final GetStoreDetailDataVenueProvince? province =
      jsonConvert.convert<GetStoreDetailDataVenueProvince>(json['province']);
  if (province != null) {
    getStoreDetailDataVenue.province = province;
  }
  final GetStoreDetailDataVenueCity? city =
      jsonConvert.convert<GetStoreDetailDataVenueCity>(json['city']);
  if (city != null) {
    getStoreDetailDataVenue.city = city;
  }
  final GetStoreDetailDataVenueDistrict? district =
      jsonConvert.convert<GetStoreDetailDataVenueDistrict>(json['district']);
  if (district != null) {
    getStoreDetailDataVenue.district = district;
  }
  final int? zipCode = jsonConvert.convert<int>(json['zip_code']);
  if (zipCode != null) {
    getStoreDetailDataVenue.zipCode = zipCode;
  }
  return getStoreDetailDataVenue;
}

Map<String, dynamic> $GetStoreDetailDataVenueToJson(
    GetStoreDetailDataVenue entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['lat'] = entity.lat;
  data['long'] = entity.long;
  data['name'] = entity.name;
  data['address'] = entity.address;
  data['country'] = entity.country?.toJson();
  data['province'] = entity.province?.toJson();
  data['city'] = entity.city?.toJson();
  data['district'] = entity.district?.toJson();
  data['zip_code'] = entity.zipCode;
  return data;
}

GetStoreDetailDataVenueCountry $GetStoreDetailDataVenueCountryFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataVenueCountry getStoreDetailDataVenueCountry =
      GetStoreDetailDataVenueCountry();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataVenueCountry.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataVenueCountry.name = name;
  }
  return getStoreDetailDataVenueCountry;
}

Map<String, dynamic> $GetStoreDetailDataVenueCountryToJson(
    GetStoreDetailDataVenueCountry entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetStoreDetailDataVenueProvince $GetStoreDetailDataVenueProvinceFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataVenueProvince getStoreDetailDataVenueProvince =
      GetStoreDetailDataVenueProvince();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataVenueProvince.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataVenueProvince.name = name;
  }
  return getStoreDetailDataVenueProvince;
}

Map<String, dynamic> $GetStoreDetailDataVenueProvinceToJson(
    GetStoreDetailDataVenueProvince entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetStoreDetailDataVenueCity $GetStoreDetailDataVenueCityFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataVenueCity getStoreDetailDataVenueCity =
      GetStoreDetailDataVenueCity();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataVenueCity.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataVenueCity.name = name;
  }
  return getStoreDetailDataVenueCity;
}

Map<String, dynamic> $GetStoreDetailDataVenueCityToJson(
    GetStoreDetailDataVenueCity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetStoreDetailDataVenueDistrict $GetStoreDetailDataVenueDistrictFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataVenueDistrict getStoreDetailDataVenueDistrict =
      GetStoreDetailDataVenueDistrict();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataVenueDistrict.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataVenueDistrict.name = name;
  }
  return getStoreDetailDataVenueDistrict;
}

Map<String, dynamic> $GetStoreDetailDataVenueDistrictToJson(
    GetStoreDetailDataVenueDistrict entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  return data;
}

GetStoreDetailDataAttributes $GetStoreDetailDataAttributesFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataAttributes getStoreDetailDataAttributes =
      GetStoreDetailDataAttributes();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataAttributes.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getStoreDetailDataAttributes.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataAttributes.name = name;
  }
  final List<GetStoreDetailDataAttributesItems>? items = jsonConvert
      .convertListNotNull<GetStoreDetailDataAttributesItems>(json['items']);
  if (items != null) {
    getStoreDetailDataAttributes.items = items;
  }
  return getStoreDetailDataAttributes;
}

Map<String, dynamic> $GetStoreDetailDataAttributesToJson(
    GetStoreDetailDataAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  data['items'] = entity.items?.map((v) => v.toJson()).toList();
  return data;
}

GetStoreDetailDataAttributesItems $GetStoreDetailDataAttributesItemsFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataAttributesItems getStoreDetailDataAttributesItems =
      GetStoreDetailDataAttributesItems();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getStoreDetailDataAttributesItems.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getStoreDetailDataAttributesItems.name = name;
  }
  final dynamic? value = jsonConvert.convert<dynamic>(json['value']);
  if (value != null) {
    getStoreDetailDataAttributesItems.value = value;
  }
  return getStoreDetailDataAttributesItems;
}

Map<String, dynamic> $GetStoreDetailDataAttributesItemsToJson(
    GetStoreDetailDataAttributesItems entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['value'] = entity.value;
  return data;
}

GetStoreDetailDataTimes $GetStoreDetailDataTimesFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataTimes getStoreDetailDataTimes =
      GetStoreDetailDataTimes();
  final int? day = jsonConvert.convert<int>(json['day']);
  if (day != null) {
    getStoreDetailDataTimes.day = day;
  }
  final String? dayText = jsonConvert.convert<String>(json['day_text']);
  if (dayText != null) {
    getStoreDetailDataTimes.dayText = dayText;
  }
  final String? open = jsonConvert.convert<String>(json['open']);
  if (open != null) {
    getStoreDetailDataTimes.open = open;
  }
  final String? close = jsonConvert.convert<String>(json['close']);
  if (close != null) {
    getStoreDetailDataTimes.close = close;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getStoreDetailDataTimes.status = status;
  }
  return getStoreDetailDataTimes;
}

Map<String, dynamic> $GetStoreDetailDataTimesToJson(
    GetStoreDetailDataTimes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['day'] = entity.day;
  data['day_text'] = entity.dayText;
  data['open'] = entity.open;
  data['close'] = entity.close;
  data['status'] = entity.status;
  return data;
}

GetStoreDetailDataTimeOpen $GetStoreDetailDataTimeOpenFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataTimeOpen getStoreDetailDataTimeOpen =
      GetStoreDetailDataTimeOpen();
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getStoreDetailDataTimeOpen.status = status;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataTimeOpen.text = text;
  }
  return getStoreDetailDataTimeOpen;
}

Map<String, dynamic> $GetStoreDetailDataTimeOpenToJson(
    GetStoreDetailDataTimeOpen entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['status'] = entity.status;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataAcceptFeeAt $GetStoreDetailDataAcceptFeeAtFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataAcceptFeeAt getStoreDetailDataAcceptFeeAt =
      GetStoreDetailDataAcceptFeeAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getStoreDetailDataAcceptFeeAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getStoreDetailDataAcceptFeeAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getStoreDetailDataAcceptFeeAt.time = time;
  }
  return getStoreDetailDataAcceptFeeAt;
}

Map<String, dynamic> $GetStoreDetailDataAcceptFeeAtToJson(
    GetStoreDetailDataAcceptFeeAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetStoreDetailDataParking $GetStoreDetailDataParkingFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataParking getStoreDetailDataParking =
      GetStoreDetailDataParking();
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getStoreDetailDataParking.total = total;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataParking.text = text;
  }
  return getStoreDetailDataParking;
}

Map<String, dynamic> $GetStoreDetailDataParkingToJson(
    GetStoreDetailDataParking entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['total'] = entity.total;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailDataSeats $GetStoreDetailDataSeatsFromJson(
    Map<String, dynamic> json) {
  final GetStoreDetailDataSeats getStoreDetailDataSeats =
      GetStoreDetailDataSeats();
  final int? min = jsonConvert.convert<int>(json['min']);
  if (min != null) {
    getStoreDetailDataSeats.min = min;
  }
  final int? max = jsonConvert.convert<int>(json['max']);
  if (max != null) {
    getStoreDetailDataSeats.max = max;
  }
  final String? text = jsonConvert.convert<String>(json['text']);
  if (text != null) {
    getStoreDetailDataSeats.text = text;
  }
  return getStoreDetailDataSeats;
}

Map<String, dynamic> $GetStoreDetailDataSeatsToJson(
    GetStoreDetailDataSeats entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['min'] = entity.min;
  data['max'] = entity.max;
  data['text'] = entity.text;
  return data;
}

GetStoreDetailBench $GetStoreDetailBenchFromJson(Map<String, dynamic> json) {
  final GetStoreDetailBench getStoreDetailBench = GetStoreDetailBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getStoreDetailBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getStoreDetailBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getStoreDetailBench.format = format;
  }
  return getStoreDetailBench;
}

Map<String, dynamic> $GetStoreDetailBenchToJson(GetStoreDetailBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
