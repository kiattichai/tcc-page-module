import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/response_image_upload_entity.dart';

ResponseImageUploadEntity $ResponseImageUploadEntityFromJson(
    Map<String, dynamic> json) {
  final ResponseImageUploadEntity responseImageUploadEntity =
      ResponseImageUploadEntity();
  final ResponseImageUploadData? data =
      jsonConvert.convert<ResponseImageUploadData>(json['data']);
  if (data != null) {
    responseImageUploadEntity.data = data;
  }
  final ResponseImageUploadBench? bench =
      jsonConvert.convert<ResponseImageUploadBench>(json['bench']);
  if (bench != null) {
    responseImageUploadEntity.bench = bench;
  }
  return responseImageUploadEntity;
}

Map<String, dynamic> $ResponseImageUploadEntityToJson(
    ResponseImageUploadEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

ResponseImageUploadData $ResponseImageUploadDataFromJson(
    Map<String, dynamic> json) {
  final ResponseImageUploadData responseImageUploadData =
      ResponseImageUploadData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    responseImageUploadData.message = message;
  }
  return responseImageUploadData;
}

Map<String, dynamic> $ResponseImageUploadDataToJson(
    ResponseImageUploadData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

ResponseImageUploadBench $ResponseImageUploadBenchFromJson(
    Map<String, dynamic> json) {
  final ResponseImageUploadBench responseImageUploadBench =
      ResponseImageUploadBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    responseImageUploadBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    responseImageUploadBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    responseImageUploadBench.format = format;
  }
  return responseImageUploadBench;
}

Map<String, dynamic> $ResponseImageUploadBenchToJson(
    ResponseImageUploadBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
