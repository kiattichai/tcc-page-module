import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/images_entity.dart';

ImagesEntity $ImagesEntityFromJson(Map<String, dynamic> json) {
  final ImagesEntity imagesEntity = ImagesEntity();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    imagesEntity.id = id;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    imagesEntity.storeId = storeId;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    imagesEntity.tag = tag;
  }
  final int? albumId = jsonConvert.convert<int>(json['album_id']);
  if (albumId != null) {
    imagesEntity.albumId = albumId;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    imagesEntity.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    imagesEntity.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    imagesEntity.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    imagesEntity.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    imagesEntity.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    imagesEntity.url = url;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    imagesEntity.position = position;
  }
  return imagesEntity;
}

Map<String, dynamic> $ImagesEntityToJson(ImagesEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['store_id'] = entity.storeId;
  data['tag'] = entity.tag;
  data['album_id'] = entity.albumId;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['position'] = entity.position;
  return data;
}
