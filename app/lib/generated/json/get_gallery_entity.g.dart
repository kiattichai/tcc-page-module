import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_gallery_entity.dart';

GetGalleryEntity $GetGalleryEntityFromJson(Map<String, dynamic> json) {
  final GetGalleryEntity getGalleryEntity = GetGalleryEntity();
  final GetGalleryData? data =
      jsonConvert.convert<GetGalleryData>(json['data']);
  if (data != null) {
    getGalleryEntity.data = data;
  }
  final GetGalleryBench? bench =
      jsonConvert.convert<GetGalleryBench>(json['bench']);
  if (bench != null) {
    getGalleryEntity.bench = bench;
  }
  return getGalleryEntity;
}

Map<String, dynamic> $GetGalleryEntityToJson(GetGalleryEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetGalleryData $GetGalleryDataFromJson(Map<String, dynamic> json) {
  final GetGalleryData getGalleryData = GetGalleryData();
  final GetGalleryDataPagination? pagination =
      jsonConvert.convert<GetGalleryDataPagination>(json['pagination']);
  if (pagination != null) {
    getGalleryData.pagination = pagination;
  }
  final List<GetGalleryDataRecord>? record =
      jsonConvert.convertListNotNull<GetGalleryDataRecord>(json['record']);
  if (record != null) {
    getGalleryData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getGalleryData.cache = cache;
  }
  return getGalleryData;
}

Map<String, dynamic> $GetGalleryDataToJson(GetGalleryData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetGalleryDataPagination $GetGalleryDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetGalleryDataPagination getGalleryDataPagination =
      GetGalleryDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getGalleryDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getGalleryDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getGalleryDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getGalleryDataPagination.total = total;
  }
  return getGalleryDataPagination;
}

Map<String, dynamic> $GetGalleryDataPaginationToJson(
    GetGalleryDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetGalleryDataRecord $GetGalleryDataRecordFromJson(Map<String, dynamic> json) {
  final GetGalleryDataRecord getGalleryDataRecord = GetGalleryDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getGalleryDataRecord.id = id;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getGalleryDataRecord.description = description;
  }
  final GetGalleryDataRecordUser? user =
      jsonConvert.convert<GetGalleryDataRecordUser>(json['user']);
  if (user != null) {
    getGalleryDataRecord.user = user;
  }
  final GetGalleryDataRecordImage? image =
      jsonConvert.convert<GetGalleryDataRecordImage>(json['image']);
  if (image != null) {
    getGalleryDataRecord.image = image;
  }
  final GetGalleryDataRecordCreatedAt? createdAt =
      jsonConvert.convert<GetGalleryDataRecordCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getGalleryDataRecord.createdAt = createdAt;
  }
  final GetGalleryDataRecordUpdatedAt? updatedAt =
      jsonConvert.convert<GetGalleryDataRecordUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getGalleryDataRecord.updatedAt = updatedAt;
  }
  return getGalleryDataRecord;
}

Map<String, dynamic> $GetGalleryDataRecordToJson(GetGalleryDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['description'] = entity.description;
  data['user'] = entity.user?.toJson();
  data['image'] = entity.image?.toJson();
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

GetGalleryDataRecordUser $GetGalleryDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetGalleryDataRecordUser getGalleryDataRecordUser =
      GetGalleryDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getGalleryDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getGalleryDataRecordUser.username = username;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getGalleryDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getGalleryDataRecordUser.lastName = lastName;
  }
  return getGalleryDataRecordUser;
}

Map<String, dynamic> $GetGalleryDataRecordUserToJson(
    GetGalleryDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  return data;
}

GetGalleryDataRecordImage $GetGalleryDataRecordImageFromJson(
    Map<String, dynamic> json) {
  final GetGalleryDataRecordImage getGalleryDataRecordImage =
      GetGalleryDataRecordImage();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getGalleryDataRecordImage.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getGalleryDataRecordImage.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getGalleryDataRecordImage.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getGalleryDataRecordImage.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getGalleryDataRecordImage.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getGalleryDataRecordImage.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getGalleryDataRecordImage.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getGalleryDataRecordImage.resizeUrl = resizeUrl;
  }
  return getGalleryDataRecordImage;
}

Map<String, dynamic> $GetGalleryDataRecordImageToJson(
    GetGalleryDataRecordImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetGalleryDataRecordCreatedAt $GetGalleryDataRecordCreatedAtFromJson(
    Map<String, dynamic> json) {
  final GetGalleryDataRecordCreatedAt getGalleryDataRecordCreatedAt =
      GetGalleryDataRecordCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getGalleryDataRecordCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getGalleryDataRecordCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getGalleryDataRecordCreatedAt.time = time;
  }
  return getGalleryDataRecordCreatedAt;
}

Map<String, dynamic> $GetGalleryDataRecordCreatedAtToJson(
    GetGalleryDataRecordCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetGalleryDataRecordUpdatedAt $GetGalleryDataRecordUpdatedAtFromJson(
    Map<String, dynamic> json) {
  final GetGalleryDataRecordUpdatedAt getGalleryDataRecordUpdatedAt =
      GetGalleryDataRecordUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getGalleryDataRecordUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getGalleryDataRecordUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getGalleryDataRecordUpdatedAt.time = time;
  }
  return getGalleryDataRecordUpdatedAt;
}

Map<String, dynamic> $GetGalleryDataRecordUpdatedAtToJson(
    GetGalleryDataRecordUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetGalleryBench $GetGalleryBenchFromJson(Map<String, dynamic> json) {
  final GetGalleryBench getGalleryBench = GetGalleryBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getGalleryBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getGalleryBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getGalleryBench.format = format;
  }
  return getGalleryBench;
}

Map<String, dynamic> $GetGalleryBenchToJson(GetGalleryBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
