import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/module/nightlife_passport_module/model/get_vaccine_entity.dart';

GetVaccineEntity $GetVaccineEntityFromJson(Map<String, dynamic> json) {
  final GetVaccineEntity getVaccineEntity = GetVaccineEntity();
  final GetVaccineData? data =
      jsonConvert.convert<GetVaccineData>(json['data']);
  if (data != null) {
    getVaccineEntity.data = data;
  }
  final GetVaccineBench? bench =
      jsonConvert.convert<GetVaccineBench>(json['bench']);
  if (bench != null) {
    getVaccineEntity.bench = bench;
  }
  return getVaccineEntity;
}

Map<String, dynamic> $GetVaccineEntityToJson(GetVaccineEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetVaccineData $GetVaccineDataFromJson(Map<String, dynamic> json) {
  final GetVaccineData getVaccineData = GetVaccineData();
  final GetVaccineDataPagination? pagination =
      jsonConvert.convert<GetVaccineDataPagination>(json['pagination']);
  if (pagination != null) {
    getVaccineData.pagination = pagination;
  }
  final List<GetVaccineDataRecord>? record =
      jsonConvert.convertListNotNull<GetVaccineDataRecord>(json['record']);
  if (record != null) {
    getVaccineData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getVaccineData.cache = cache;
  }
  return getVaccineData;
}

Map<String, dynamic> $GetVaccineDataToJson(GetVaccineData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetVaccineDataPagination $GetVaccineDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataPagination getVaccineDataPagination =
      GetVaccineDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getVaccineDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getVaccineDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getVaccineDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getVaccineDataPagination.total = total;
  }
  return getVaccineDataPagination;
}

Map<String, dynamic> $GetVaccineDataPaginationToJson(
    GetVaccineDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetVaccineDataRecord $GetVaccineDataRecordFromJson(Map<String, dynamic> json) {
  final GetVaccineDataRecord getVaccineDataRecord = GetVaccineDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVaccineDataRecord.id = id;
  }
  final dynamic? slug = jsonConvert.convert<dynamic>(json['slug']);
  if (slug != null) {
    getVaccineDataRecord.slug = slug;
  }
  final GetVaccineDataRecordGroup? group =
      jsonConvert.convert<GetVaccineDataRecordGroup>(json['group']);
  if (group != null) {
    getVaccineDataRecord.group = group;
  }
  final GetVaccineDataRecordCategory? category =
      jsonConvert.convert<GetVaccineDataRecordCategory>(json['category']);
  if (category != null) {
    getVaccineDataRecord.category = category;
  }
  final List<GetVaccineDataRecordFields>? fields = jsonConvert
      .convertListNotNull<GetVaccineDataRecordFields>(json['fields']);
  if (fields != null) {
    getVaccineDataRecord.fields = fields;
  }
  final List<GetVaccineDataRecordImages>? images = jsonConvert
      .convertListNotNull<GetVaccineDataRecordImages>(json['images']);
  if (images != null) {
    getVaccineDataRecord.images = images;
  }
  final int? viewed = jsonConvert.convert<int>(json['viewed']);
  if (viewed != null) {
    getVaccineDataRecord.viewed = viewed;
  }
  final int? viewedText = jsonConvert.convert<int>(json['viewed_text']);
  if (viewedText != null) {
    getVaccineDataRecord.viewedText = viewedText;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getVaccineDataRecord.status = status;
  }
  final String? shareUrl = jsonConvert.convert<String>(json['share_url']);
  if (shareUrl != null) {
    getVaccineDataRecord.shareUrl = shareUrl;
  }
  final String? webviewUrl = jsonConvert.convert<String>(json['webview_url']);
  if (webviewUrl != null) {
    getVaccineDataRecord.webviewUrl = webviewUrl;
  }
  final int? totalUser = jsonConvert.convert<int>(json['total_user']);
  if (totalUser != null) {
    getVaccineDataRecord.totalUser = totalUser;
  }
  final String? publishedAt = jsonConvert.convert<String>(json['published_at']);
  if (publishedAt != null) {
    getVaccineDataRecord.publishedAt = publishedAt;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    getVaccineDataRecord.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    getVaccineDataRecord.updatedAt = updatedAt;
  }
  return getVaccineDataRecord;
}

Map<String, dynamic> $GetVaccineDataRecordToJson(GetVaccineDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['slug'] = entity.slug;
  data['group'] = entity.group?.toJson();
  data['category'] = entity.category?.toJson();
  data['fields'] = entity.fields?.map((v) => v.toJson()).toList();
  data['images'] = entity.images?.map((v) => v.toJson()).toList();
  data['viewed'] = entity.viewed;
  data['viewed_text'] = entity.viewedText;
  data['status'] = entity.status;
  data['share_url'] = entity.shareUrl;
  data['webview_url'] = entity.webviewUrl;
  data['total_user'] = entity.totalUser;
  data['published_at'] = entity.publishedAt;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  return data;
}

GetVaccineDataRecordGroup $GetVaccineDataRecordGroupFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordGroup getVaccineDataRecordGroup =
      GetVaccineDataRecordGroup();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVaccineDataRecordGroup.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getVaccineDataRecordGroup.code = code;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordGroup.name = name;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getVaccineDataRecordGroup.description = description;
  }
  return getVaccineDataRecordGroup;
}

Map<String, dynamic> $GetVaccineDataRecordGroupToJson(
    GetVaccineDataRecordGroup entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['name'] = entity.name;
  data['description'] = entity.description;
  return data;
}

GetVaccineDataRecordCategory $GetVaccineDataRecordCategoryFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordCategory getVaccineDataRecordCategory =
      GetVaccineDataRecordCategory();
  final GetVaccineDataRecordCategoryCurrent? current =
      jsonConvert.convert<GetVaccineDataRecordCategoryCurrent>(json['current']);
  if (current != null) {
    getVaccineDataRecordCategory.current = current;
  }
  final List<GetVaccineDataRecordCategoryItems>? items = jsonConvert
      .convertListNotNull<GetVaccineDataRecordCategoryItems>(json['items']);
  if (items != null) {
    getVaccineDataRecordCategory.items = items;
  }
  return getVaccineDataRecordCategory;
}

Map<String, dynamic> $GetVaccineDataRecordCategoryToJson(
    GetVaccineDataRecordCategory entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current'] = entity.current?.toJson();
  data['items'] = entity.items?.map((v) => v.toJson()).toList();
  return data;
}

GetVaccineDataRecordCategoryCurrent
    $GetVaccineDataRecordCategoryCurrentFromJson(Map<String, dynamic> json) {
  final GetVaccineDataRecordCategoryCurrent
      getVaccineDataRecordCategoryCurrent =
      GetVaccineDataRecordCategoryCurrent();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVaccineDataRecordCategoryCurrent.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getVaccineDataRecordCategoryCurrent.code = code;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getVaccineDataRecordCategoryCurrent.status = status;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getVaccineDataRecordCategoryCurrent.position = position;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordCategoryCurrent.name = name;
  }
  final dynamic? description =
      jsonConvert.convert<dynamic>(json['description']);
  if (description != null) {
    getVaccineDataRecordCategoryCurrent.description = description;
  }
  final dynamic? metaTitle = jsonConvert.convert<dynamic>(json['meta_title']);
  if (metaTitle != null) {
    getVaccineDataRecordCategoryCurrent.metaTitle = metaTitle;
  }
  final dynamic? metaDescription =
      jsonConvert.convert<dynamic>(json['meta_description']);
  if (metaDescription != null) {
    getVaccineDataRecordCategoryCurrent.metaDescription = metaDescription;
  }
  final dynamic? metaKeyword =
      jsonConvert.convert<dynamic>(json['meta_keyword']);
  if (metaKeyword != null) {
    getVaccineDataRecordCategoryCurrent.metaKeyword = metaKeyword;
  }
  return getVaccineDataRecordCategoryCurrent;
}

Map<String, dynamic> $GetVaccineDataRecordCategoryCurrentToJson(
    GetVaccineDataRecordCategoryCurrent entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['position'] = entity.position;
  data['name'] = entity.name;
  data['description'] = entity.description;
  data['meta_title'] = entity.metaTitle;
  data['meta_description'] = entity.metaDescription;
  data['meta_keyword'] = entity.metaKeyword;
  return data;
}

GetVaccineDataRecordCategoryItems $GetVaccineDataRecordCategoryItemsFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordCategoryItems getVaccineDataRecordCategoryItems =
      GetVaccineDataRecordCategoryItems();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVaccineDataRecordCategoryItems.id = id;
  }
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    getVaccineDataRecordCategoryItems.code = code;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getVaccineDataRecordCategoryItems.status = status;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getVaccineDataRecordCategoryItems.position = position;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordCategoryItems.name = name;
  }
  return getVaccineDataRecordCategoryItems;
}

Map<String, dynamic> $GetVaccineDataRecordCategoryItemsToJson(
    GetVaccineDataRecordCategoryItems entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['code'] = entity.code;
  data['status'] = entity.status;
  data['position'] = entity.position;
  data['name'] = entity.name;
  return data;
}

GetVaccineDataRecordFields $GetVaccineDataRecordFieldsFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordFields getVaccineDataRecordFields =
      GetVaccineDataRecordFields();
  final String? label = jsonConvert.convert<String>(json['label']);
  if (label != null) {
    getVaccineDataRecordFields.label = label;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordFields.name = name;
  }
  final GetVaccineDataRecordFieldsType? type =
      jsonConvert.convert<GetVaccineDataRecordFieldsType>(json['type']);
  if (type != null) {
    getVaccineDataRecordFields.type = type;
  }
  final bool? hasLang = jsonConvert.convert<bool>(json['has_lang']);
  if (hasLang != null) {
    getVaccineDataRecordFields.hasLang = hasLang;
  }
  final bool? require = jsonConvert.convert<bool>(json['require']);
  if (require != null) {
    getVaccineDataRecordFields.require = require;
  }
  final dynamic? options = jsonConvert.convert<dynamic>(json['options']);
  if (options != null) {
    getVaccineDataRecordFields.options = options;
  }
  final String? lang = jsonConvert.convert<String>(json['lang']);
  if (lang != null) {
    getVaccineDataRecordFields.lang = lang;
  }
  return getVaccineDataRecordFields;
}

Map<String, dynamic> $GetVaccineDataRecordFieldsToJson(
    GetVaccineDataRecordFields entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['label'] = entity.label;
  data['name'] = entity.name;
  data['type'] = entity.type?.toJson();
  data['has_lang'] = entity.hasLang;
  data['require'] = entity.require;
  data['options'] = entity.options;
  data['lang'] = entity.lang;
  return data;
}

GetVaccineDataRecordFieldsType $GetVaccineDataRecordFieldsTypeFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordFieldsType getVaccineDataRecordFieldsType =
      GetVaccineDataRecordFieldsType();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getVaccineDataRecordFieldsType.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordFieldsType.name = name;
  }
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getVaccineDataRecordFieldsType.value = value;
  }
  return getVaccineDataRecordFieldsType;
}

Map<String, dynamic> $GetVaccineDataRecordFieldsTypeToJson(
    GetVaccineDataRecordFieldsType entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['value'] = entity.value;
  return data;
}

GetVaccineDataRecordImages $GetVaccineDataRecordImagesFromJson(
    Map<String, dynamic> json) {
  final GetVaccineDataRecordImages getVaccineDataRecordImages =
      GetVaccineDataRecordImages();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getVaccineDataRecordImages.id = id;
  }
  final String? tag = jsonConvert.convert<String>(json['tag']);
  if (tag != null) {
    getVaccineDataRecordImages.tag = tag;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getVaccineDataRecordImages.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getVaccineDataRecordImages.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getVaccineDataRecordImages.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getVaccineDataRecordImages.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getVaccineDataRecordImages.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getVaccineDataRecordImages.url = url;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getVaccineDataRecordImages.position = position;
  }
  return getVaccineDataRecordImages;
}

Map<String, dynamic> $GetVaccineDataRecordImagesToJson(
    GetVaccineDataRecordImages entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['tag'] = entity.tag;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['position'] = entity.position;
  return data;
}

GetVaccineBench $GetVaccineBenchFromJson(Map<String, dynamic> json) {
  final GetVaccineBench getVaccineBench = GetVaccineBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getVaccineBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getVaccineBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getVaccineBench.format = format;
  }
  return getVaccineBench;
}

Map<String, dynamic> $GetVaccineBenchToJson(GetVaccineBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
