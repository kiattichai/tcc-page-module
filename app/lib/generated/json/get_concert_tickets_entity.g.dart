import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_concert_tickets_entity.dart';

GetConcertTicketsEntity $GetConcertTicketsEntityFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsEntity getConcertTicketsEntity =
      GetConcertTicketsEntity();
  final GetConcertTicketsData? data =
      jsonConvert.convert<GetConcertTicketsData>(json['data']);
  if (data != null) {
    getConcertTicketsEntity.data = data;
  }
  final GetConcertTicketsBench? bench =
      jsonConvert.convert<GetConcertTicketsBench>(json['bench']);
  if (bench != null) {
    getConcertTicketsEntity.bench = bench;
  }
  return getConcertTicketsEntity;
}

Map<String, dynamic> $GetConcertTicketsEntityToJson(
    GetConcertTicketsEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetConcertTicketsData $GetConcertTicketsDataFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsData getConcertTicketsData = GetConcertTicketsData();
  final List<GetConcertTicketsDataRecord>? record = jsonConvert
      .convertListNotNull<GetConcertTicketsDataRecord>(json['record']);
  if (record != null) {
    getConcertTicketsData.record = record;
  }
  final bool? cache = jsonConvert.convert<bool>(json['cache']);
  if (cache != null) {
    getConcertTicketsData.cache = cache;
  }
  return getConcertTicketsData;
}

Map<String, dynamic> $GetConcertTicketsDataToJson(
    GetConcertTicketsData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  data['cache'] = entity.cache;
  return data;
}

GetConcertTicketsDataRecord $GetConcertTicketsDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsDataRecord getConcertTicketsDataRecord =
      GetConcertTicketsDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getConcertTicketsDataRecord.id = id;
  }
  final String? sku = jsonConvert.convert<String>(json['sku']);
  if (sku != null) {
    getConcertTicketsDataRecord.sku = sku;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getConcertTicketsDataRecord.name = name;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    getConcertTicketsDataRecord.description = description;
  }
  final dynamic? group = jsonConvert.convert<dynamic>(json['group']);
  if (group != null) {
    getConcertTicketsDataRecord.group = group;
  }
  final dynamic? zone = jsonConvert.convert<dynamic>(json['zone']);
  if (zone != null) {
    getConcertTicketsDataRecord.zone = zone;
  }
  final int? costPrice = jsonConvert.convert<int>(json['cost_price']);
  if (costPrice != null) {
    getConcertTicketsDataRecord.costPrice = costPrice;
  }
  final String? costPriceText =
      jsonConvert.convert<String>(json['cost_price_text']);
  if (costPriceText != null) {
    getConcertTicketsDataRecord.costPriceText = costPriceText;
  }
  final int? price = jsonConvert.convert<int>(json['price']);
  if (price != null) {
    getConcertTicketsDataRecord.price = price;
  }
  final String? priceText = jsonConvert.convert<String>(json['price_text']);
  if (priceText != null) {
    getConcertTicketsDataRecord.priceText = priceText;
  }
  final int? compareAtPrice =
      jsonConvert.convert<int>(json['compare_at_price']);
  if (compareAtPrice != null) {
    getConcertTicketsDataRecord.compareAtPrice = compareAtPrice;
  }
  final String? compareAtPriceText =
      jsonConvert.convert<String>(json['compare_at_price_text']);
  if (compareAtPriceText != null) {
    getConcertTicketsDataRecord.compareAtPriceText = compareAtPriceText;
  }
  final bool? package = jsonConvert.convert<bool>(json['package']);
  if (package != null) {
    getConcertTicketsDataRecord.package = package;
  }
  final int? perPackage = jsonConvert.convert<int>(json['per_package']);
  if (perPackage != null) {
    getConcertTicketsDataRecord.perPackage = perPackage;
  }
  final int? stock = jsonConvert.convert<int>(json['stock']);
  if (stock != null) {
    getConcertTicketsDataRecord.stock = stock;
  }
  final int? quantity = jsonConvert.convert<int>(json['quantity']);
  if (quantity != null) {
    getConcertTicketsDataRecord.quantity = quantity;
  }
  final dynamic? orderQuantity =
      jsonConvert.convert<dynamic>(json['order_quantity']);
  if (orderQuantity != null) {
    getConcertTicketsDataRecord.orderQuantity = orderQuantity;
  }
  final int? diffStock = jsonConvert.convert<int>(json['diff_stock']);
  if (diffStock != null) {
    getConcertTicketsDataRecord.diffStock = diffStock;
  }
  final int? hold = jsonConvert.convert<int>(json['hold']);
  if (hold != null) {
    getConcertTicketsDataRecord.hold = hold;
  }
  final int? position = jsonConvert.convert<int>(json['position']);
  if (position != null) {
    getConcertTicketsDataRecord.position = position;
  }
  final int? points = jsonConvert.convert<int>(json['points']);
  if (points != null) {
    getConcertTicketsDataRecord.points = points;
  }
  final int? weight = jsonConvert.convert<int>(json['weight']);
  if (weight != null) {
    getConcertTicketsDataRecord.weight = weight;
  }
  final bool? status = jsonConvert.convert<bool>(json['status']);
  if (status != null) {
    getConcertTicketsDataRecord.status = status;
  }
  final String? publishedStart =
      jsonConvert.convert<String>(json['published_start']);
  if (publishedStart != null) {
    getConcertTicketsDataRecord.publishedStart = publishedStart;
  }
  final String? publishedEnd =
      jsonConvert.convert<String>(json['published_end']);
  if (publishedEnd != null) {
    getConcertTicketsDataRecord.publishedEnd = publishedEnd;
  }
  final String? gateOpen = jsonConvert.convert<String>(json['gate_open']);
  if (gateOpen != null) {
    getConcertTicketsDataRecord.gateOpen = gateOpen;
  }
  final String? gateClose = jsonConvert.convert<String>(json['gate_close']);
  if (gateClose != null) {
    getConcertTicketsDataRecord.gateClose = gateClose;
  }
  final int? allowOrderMin = jsonConvert.convert<int>(json['allow_order_min']);
  if (allowOrderMin != null) {
    getConcertTicketsDataRecord.allowOrderMin = allowOrderMin;
  }
  final int? allowOrderMax = jsonConvert.convert<int>(json['allow_order_max']);
  if (allowOrderMax != null) {
    getConcertTicketsDataRecord.allowOrderMax = allowOrderMax;
  }
  final dynamic? remark = jsonConvert.convert<dynamic>(json['remark']);
  if (remark != null) {
    getConcertTicketsDataRecord.remark = remark;
  }
  final bool? specialOption = jsonConvert.convert<bool>(json['special_option']);
  if (specialOption != null) {
    getConcertTicketsDataRecord.specialOption = specialOption;
  }
  final bool? serviceCharge = jsonConvert.convert<bool>(json['service_charge']);
  if (serviceCharge != null) {
    getConcertTicketsDataRecord.serviceCharge = serviceCharge;
  }
  final int? serviceFee = jsonConvert.convert<int>(json['service_fee']);
  if (serviceFee != null) {
    getConcertTicketsDataRecord.serviceFee = serviceFee;
  }
  final bool? hasTicket = jsonConvert.convert<bool>(json['has_ticket']);
  if (hasTicket != null) {
    getConcertTicketsDataRecord.hasTicket = hasTicket;
  }
  final GetConcertTicketsDataRecordService? service =
      jsonConvert.convert<GetConcertTicketsDataRecordService>(json['service']);
  if (service != null) {
    getConcertTicketsDataRecord.service = service;
  }
  final List<dynamic>? options =
      jsonConvert.convertListNotNull<dynamic>(json['options']);
  if (options != null) {
    getConcertTicketsDataRecord.options = options;
  }
  final List<dynamic>? promotions =
      jsonConvert.convertListNotNull<dynamic>(json['promotions']);
  if (promotions != null) {
    getConcertTicketsDataRecord.promotions = promotions;
  }
  final GetConcertTicketsDataRecordImage? image =
      jsonConvert.convert<GetConcertTicketsDataRecordImage>(json['image']);
  if (image != null) {
    getConcertTicketsDataRecord.image = image;
  }
  final bool? onlineMeeting = jsonConvert.convert<bool>(json['online_meeting']);
  if (onlineMeeting != null) {
    getConcertTicketsDataRecord.onlineMeeting = onlineMeeting;
  }
  final List<dynamic>? meetings =
      jsonConvert.convertListNotNull<dynamic>(json['meetings']);
  if (meetings != null) {
    getConcertTicketsDataRecord.meetings = meetings;
  }
  final dynamic? meta = jsonConvert.convert<dynamic>(json['meta']);
  if (meta != null) {
    getConcertTicketsDataRecord.meta = meta;
  }
  final bool? displayStatus = jsonConvert.convert<bool>(json['display_status']);
  if (displayStatus != null) {
    getConcertTicketsDataRecord.displayStatus = displayStatus;
  }
  final String? displayText = jsonConvert.convert<String>(json['display_text']);
  if (displayText != null) {
    getConcertTicketsDataRecord.displayText = displayText;
  }
  return getConcertTicketsDataRecord;
}

Map<String, dynamic> $GetConcertTicketsDataRecordToJson(
    GetConcertTicketsDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['sku'] = entity.sku;
  data['name'] = entity.name;
  data['description'] = entity.description;
  data['group'] = entity.group;
  data['zone'] = entity.zone;
  data['cost_price'] = entity.costPrice;
  data['cost_price_text'] = entity.costPriceText;
  data['price'] = entity.price;
  data['price_text'] = entity.priceText;
  data['compare_at_price'] = entity.compareAtPrice;
  data['compare_at_price_text'] = entity.compareAtPriceText;
  data['package'] = entity.package;
  data['per_package'] = entity.perPackage;
  data['stock'] = entity.stock;
  data['quantity'] = entity.quantity;
  data['order_quantity'] = entity.orderQuantity;
  data['diff_stock'] = entity.diffStock;
  data['hold'] = entity.hold;
  data['position'] = entity.position;
  data['points'] = entity.points;
  data['weight'] = entity.weight;
  data['status'] = entity.status;
  data['published_start'] = entity.publishedStart;
  data['published_end'] = entity.publishedEnd;
  data['gate_open'] = entity.gateOpen;
  data['gate_close'] = entity.gateClose;
  data['allow_order_min'] = entity.allowOrderMin;
  data['allow_order_max'] = entity.allowOrderMax;
  data['remark'] = entity.remark;
  data['special_option'] = entity.specialOption;
  data['service_charge'] = entity.serviceCharge;
  data['service_fee'] = entity.serviceFee;
  data['has_ticket'] = entity.hasTicket;
  data['service'] = entity.service?.toJson();
  data['options'] = entity.options;
  data['promotions'] = entity.promotions;
  data['image'] = entity.image?.toJson();
  data['online_meeting'] = entity.onlineMeeting;
  data['meetings'] = entity.meetings;
  data['meta'] = entity.meta;
  data['display_status'] = entity.displayStatus;
  data['display_text'] = entity.displayText;
  return data;
}

GetConcertTicketsDataRecordService $GetConcertTicketsDataRecordServiceFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsDataRecordService getConcertTicketsDataRecordService =
      GetConcertTicketsDataRecordService();
  final bool? charge = jsonConvert.convert<bool>(json['charge']);
  if (charge != null) {
    getConcertTicketsDataRecordService.charge = charge;
  }
  final int? feeValue = jsonConvert.convert<int>(json['fee_value']);
  if (feeValue != null) {
    getConcertTicketsDataRecordService.feeValue = feeValue;
  }
  final int? fee = jsonConvert.convert<int>(json['fee']);
  if (fee != null) {
    getConcertTicketsDataRecordService.fee = fee;
  }
  final String? feeText = jsonConvert.convert<String>(json['fee_text']);
  if (feeText != null) {
    getConcertTicketsDataRecordService.feeText = feeText;
  }
  return getConcertTicketsDataRecordService;
}

Map<String, dynamic> $GetConcertTicketsDataRecordServiceToJson(
    GetConcertTicketsDataRecordService entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['charge'] = entity.charge;
  data['fee_value'] = entity.feeValue;
  data['fee'] = entity.fee;
  data['fee_text'] = entity.feeText;
  return data;
}

GetConcertTicketsDataRecordImage $GetConcertTicketsDataRecordImageFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsDataRecordImage getConcertTicketsDataRecordImage =
      GetConcertTicketsDataRecordImage();
  return getConcertTicketsDataRecordImage;
}

Map<String, dynamic> $GetConcertTicketsDataRecordImageToJson(
    GetConcertTicketsDataRecordImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  return data;
}

GetConcertTicketsBench $GetConcertTicketsBenchFromJson(
    Map<String, dynamic> json) {
  final GetConcertTicketsBench getConcertTicketsBench =
      GetConcertTicketsBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getConcertTicketsBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getConcertTicketsBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getConcertTicketsBench.format = format;
  }
  return getConcertTicketsBench;
}

Map<String, dynamic> $GetConcertTicketsBenchToJson(
    GetConcertTicketsBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
