import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_ticket_scan_entity.dart';

SaveTicketScanEntity $SaveTicketScanEntityFromJson(Map<String, dynamic> json) {
  final SaveTicketScanEntity saveTicketScanEntity = SaveTicketScanEntity();
  final String? storeId = jsonConvert.convert<String>(json['store_id']);
  if (storeId != null) {
    saveTicketScanEntity.storeId = storeId;
  }
  final String? note = jsonConvert.convert<String>(json['note']);
  if (note != null) {
    saveTicketScanEntity.note = note;
  }
  return saveTicketScanEntity;
}

Map<String, dynamic> $SaveTicketScanEntityToJson(SaveTicketScanEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['store_id'] = entity.storeId;
  data['note'] = entity.note;
  return data;
}
