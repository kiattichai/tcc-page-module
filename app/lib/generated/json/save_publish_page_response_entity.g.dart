import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/save_publish_page_response_entity.dart';

SavePublishPageResponseEntity $SavePublishPageResponseEntityFromJson(
    Map<String, dynamic> json) {
  final SavePublishPageResponseEntity savePublishPageResponseEntity =
      SavePublishPageResponseEntity();
  final SavePublishPageResponseData? data =
      jsonConvert.convert<SavePublishPageResponseData>(json['data']);
  if (data != null) {
    savePublishPageResponseEntity.data = data;
  }
  final SavePublishPageResponseBench? bench =
      jsonConvert.convert<SavePublishPageResponseBench>(json['bench']);
  if (bench != null) {
    savePublishPageResponseEntity.bench = bench;
  }
  return savePublishPageResponseEntity;
}

Map<String, dynamic> $SavePublishPageResponseEntityToJson(
    SavePublishPageResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

SavePublishPageResponseData $SavePublishPageResponseDataFromJson(
    Map<String, dynamic> json) {
  final SavePublishPageResponseData savePublishPageResponseData =
      SavePublishPageResponseData();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    savePublishPageResponseData.message = message;
  }
  return savePublishPageResponseData;
}

Map<String, dynamic> $SavePublishPageResponseDataToJson(
    SavePublishPageResponseData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  return data;
}

SavePublishPageResponseBench $SavePublishPageResponseBenchFromJson(
    Map<String, dynamic> json) {
  final SavePublishPageResponseBench savePublishPageResponseBench =
      SavePublishPageResponseBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    savePublishPageResponseBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    savePublishPageResponseBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    savePublishPageResponseBench.format = format;
  }
  return savePublishPageResponseBench;
}

Map<String, dynamic> $SavePublishPageResponseBenchToJson(
    SavePublishPageResponseBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
