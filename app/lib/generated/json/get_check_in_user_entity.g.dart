import 'package:app/generated/json/base/json_convert_content.dart';
import 'package:app/model/get_check_in_user_entity.dart';

GetCheckInUserEntity $GetCheckInUserEntityFromJson(Map<String, dynamic> json) {
  final GetCheckInUserEntity getCheckInUserEntity = GetCheckInUserEntity();
  final GetCheckInUserData? data =
      jsonConvert.convert<GetCheckInUserData>(json['data']);
  if (data != null) {
    getCheckInUserEntity.data = data;
  }
  final GetCheckInUserBench? bench =
      jsonConvert.convert<GetCheckInUserBench>(json['bench']);
  if (bench != null) {
    getCheckInUserEntity.bench = bench;
  }
  return getCheckInUserEntity;
}

Map<String, dynamic> $GetCheckInUserEntityToJson(GetCheckInUserEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['data'] = entity.data?.toJson();
  data['bench'] = entity.bench?.toJson();
  return data;
}

GetCheckInUserData $GetCheckInUserDataFromJson(Map<String, dynamic> json) {
  final GetCheckInUserData getCheckInUserData = GetCheckInUserData();
  final GetCheckInUserDataMeta? meta =
      jsonConvert.convert<GetCheckInUserDataMeta>(json['meta']);
  if (meta != null) {
    getCheckInUserData.meta = meta;
  }
  final GetCheckInUserDataPagination? pagination =
      jsonConvert.convert<GetCheckInUserDataPagination>(json['pagination']);
  if (pagination != null) {
    getCheckInUserData.pagination = pagination;
  }
  final List<GetCheckInUserDataRecord>? record =
      jsonConvert.convertListNotNull<GetCheckInUserDataRecord>(json['record']);
  if (record != null) {
    getCheckInUserData.record = record;
  }
  return getCheckInUserData;
}

Map<String, dynamic> $GetCheckInUserDataToJson(GetCheckInUserData entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['meta'] = entity.meta?.toJson();
  data['pagination'] = entity.pagination?.toJson();
  data['record'] = entity.record?.map((v) => v.toJson()).toList();
  return data;
}

GetCheckInUserDataMeta $GetCheckInUserDataMetaFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataMeta getCheckInUserDataMeta =
      GetCheckInUserDataMeta();
  final int? checkinTotal = jsonConvert.convert<int>(json['checkin_total']);
  if (checkinTotal != null) {
    getCheckInUserDataMeta.checkinTotal = checkinTotal;
  }
  final int? userTotal = jsonConvert.convert<int>(json['user_total']);
  if (userTotal != null) {
    getCheckInUserDataMeta.userTotal = userTotal;
  }
  return getCheckInUserDataMeta;
}

Map<String, dynamic> $GetCheckInUserDataMetaToJson(
    GetCheckInUserDataMeta entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['checkin_total'] = entity.checkinTotal;
  data['user_total'] = entity.userTotal;
  return data;
}

GetCheckInUserDataPagination $GetCheckInUserDataPaginationFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataPagination getCheckInUserDataPagination =
      GetCheckInUserDataPagination();
  final int? currentPage = jsonConvert.convert<int>(json['current_page']);
  if (currentPage != null) {
    getCheckInUserDataPagination.currentPage = currentPage;
  }
  final int? lastPage = jsonConvert.convert<int>(json['last_page']);
  if (lastPage != null) {
    getCheckInUserDataPagination.lastPage = lastPage;
  }
  final int? limit = jsonConvert.convert<int>(json['limit']);
  if (limit != null) {
    getCheckInUserDataPagination.limit = limit;
  }
  final int? total = jsonConvert.convert<int>(json['total']);
  if (total != null) {
    getCheckInUserDataPagination.total = total;
  }
  return getCheckInUserDataPagination;
}

Map<String, dynamic> $GetCheckInUserDataPaginationToJson(
    GetCheckInUserDataPagination entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['current_page'] = entity.currentPage;
  data['last_page'] = entity.lastPage;
  data['limit'] = entity.limit;
  data['total'] = entity.total;
  return data;
}

GetCheckInUserDataRecord $GetCheckInUserDataRecordFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataRecord getCheckInUserDataRecord =
      GetCheckInUserDataRecord();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCheckInUserDataRecord.id = id;
  }
  final GetCheckInUserDataRecordUser? user =
      jsonConvert.convert<GetCheckInUserDataRecordUser>(json['user']);
  if (user != null) {
    getCheckInUserDataRecord.user = user;
  }
  final GetCheckInUserDataRecordCreatedAt? createdAt = jsonConvert
      .convert<GetCheckInUserDataRecordCreatedAt>(json['created_at']);
  if (createdAt != null) {
    getCheckInUserDataRecord.createdAt = createdAt;
  }
  final GetCheckInUserDataRecordUpdatedAt? updatedAt = jsonConvert
      .convert<GetCheckInUserDataRecordUpdatedAt>(json['updated_at']);
  if (updatedAt != null) {
    getCheckInUserDataRecord.updatedAt = updatedAt;
  }
  return getCheckInUserDataRecord;
}

Map<String, dynamic> $GetCheckInUserDataRecordToJson(
    GetCheckInUserDataRecord entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['user'] = entity.user?.toJson();
  data['created_at'] = entity.createdAt?.toJson();
  data['updated_at'] = entity.updatedAt?.toJson();
  return data;
}

GetCheckInUserDataRecordUser $GetCheckInUserDataRecordUserFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataRecordUser getCheckInUserDataRecordUser =
      GetCheckInUserDataRecordUser();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    getCheckInUserDataRecordUser.id = id;
  }
  final String? username = jsonConvert.convert<String>(json['username']);
  if (username != null) {
    getCheckInUserDataRecordUser.username = username;
  }
  final String? firstName = jsonConvert.convert<String>(json['first_name']);
  if (firstName != null) {
    getCheckInUserDataRecordUser.firstName = firstName;
  }
  final String? lastName = jsonConvert.convert<String>(json['last_name']);
  if (lastName != null) {
    getCheckInUserDataRecordUser.lastName = lastName;
  }
  final GetCheckInUserDataRecordUserImage? image =
      jsonConvert.convert<GetCheckInUserDataRecordUserImage>(json['image']);
  if (image != null) {
    getCheckInUserDataRecordUser.image = image;
  }
  return getCheckInUserDataRecordUser;
}

Map<String, dynamic> $GetCheckInUserDataRecordUserToJson(
    GetCheckInUserDataRecordUser entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['username'] = entity.username;
  data['first_name'] = entity.firstName;
  data['last_name'] = entity.lastName;
  data['image'] = entity.image?.toJson();
  return data;
}

GetCheckInUserDataRecordUserImage $GetCheckInUserDataRecordUserImageFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataRecordUserImage getCheckInUserDataRecordUserImage =
      GetCheckInUserDataRecordUserImage();
  final String? id = jsonConvert.convert<String>(json['id']);
  if (id != null) {
    getCheckInUserDataRecordUserImage.id = id;
  }
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    getCheckInUserDataRecordUserImage.name = name;
  }
  final int? width = jsonConvert.convert<int>(json['width']);
  if (width != null) {
    getCheckInUserDataRecordUserImage.width = width;
  }
  final int? height = jsonConvert.convert<int>(json['height']);
  if (height != null) {
    getCheckInUserDataRecordUserImage.height = height;
  }
  final String? mime = jsonConvert.convert<String>(json['mime']);
  if (mime != null) {
    getCheckInUserDataRecordUserImage.mime = mime;
  }
  final int? size = jsonConvert.convert<int>(json['size']);
  if (size != null) {
    getCheckInUserDataRecordUserImage.size = size;
  }
  final String? url = jsonConvert.convert<String>(json['url']);
  if (url != null) {
    getCheckInUserDataRecordUserImage.url = url;
  }
  final String? resizeUrl = jsonConvert.convert<String>(json['resize_url']);
  if (resizeUrl != null) {
    getCheckInUserDataRecordUserImage.resizeUrl = resizeUrl;
  }
  return getCheckInUserDataRecordUserImage;
}

Map<String, dynamic> $GetCheckInUserDataRecordUserImageToJson(
    GetCheckInUserDataRecordUserImage entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['name'] = entity.name;
  data['width'] = entity.width;
  data['height'] = entity.height;
  data['mime'] = entity.mime;
  data['size'] = entity.size;
  data['url'] = entity.url;
  data['resize_url'] = entity.resizeUrl;
  return data;
}

GetCheckInUserDataRecordCreatedAt $GetCheckInUserDataRecordCreatedAtFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataRecordCreatedAt getCheckInUserDataRecordCreatedAt =
      GetCheckInUserDataRecordCreatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getCheckInUserDataRecordCreatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getCheckInUserDataRecordCreatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getCheckInUserDataRecordCreatedAt.time = time;
  }
  return getCheckInUserDataRecordCreatedAt;
}

Map<String, dynamic> $GetCheckInUserDataRecordCreatedAtToJson(
    GetCheckInUserDataRecordCreatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetCheckInUserDataRecordUpdatedAt $GetCheckInUserDataRecordUpdatedAtFromJson(
    Map<String, dynamic> json) {
  final GetCheckInUserDataRecordUpdatedAt getCheckInUserDataRecordUpdatedAt =
      GetCheckInUserDataRecordUpdatedAt();
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    getCheckInUserDataRecordUpdatedAt.value = value;
  }
  final String? date = jsonConvert.convert<String>(json['date']);
  if (date != null) {
    getCheckInUserDataRecordUpdatedAt.date = date;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    getCheckInUserDataRecordUpdatedAt.time = time;
  }
  return getCheckInUserDataRecordUpdatedAt;
}

Map<String, dynamic> $GetCheckInUserDataRecordUpdatedAtToJson(
    GetCheckInUserDataRecordUpdatedAt entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['value'] = entity.value;
  data['date'] = entity.date;
  data['time'] = entity.time;
  return data;
}

GetCheckInUserBench $GetCheckInUserBenchFromJson(Map<String, dynamic> json) {
  final GetCheckInUserBench getCheckInUserBench = GetCheckInUserBench();
  final int? second = jsonConvert.convert<int>(json['second']);
  if (second != null) {
    getCheckInUserBench.second = second;
  }
  final double? millisecond = jsonConvert.convert<double>(json['millisecond']);
  if (millisecond != null) {
    getCheckInUserBench.millisecond = millisecond;
  }
  final String? format = jsonConvert.convert<String>(json['format']);
  if (format != null) {
    getCheckInUserBench.format = format;
  }
  return getCheckInUserBench;
}

Map<String, dynamic> $GetCheckInUserBenchToJson(GetCheckInUserBench entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['second'] = entity.second;
  data['millisecond'] = entity.millisecond;
  data['format'] = entity.format;
  return data;
}
